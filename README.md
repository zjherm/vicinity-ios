# VICINITY #

### Server ###

*API Base URLs:*

DEV:    http://45.55.134.111:3007/api

STAGE:  http://45.55.134.111:3006/api

PROD:   http://45.55.15.229:80/api

*Swagger:*

DEV:    http://45.55.134.111:3007

STAGE:  http://45.55.134.111:3006

PROD:   http://45.55.15.229:3005

(info@getvicinityapp.com/$WAGGER)

*Admin panel:*

DEV:    http://45.55.134.111:9002/admin (info@getvicinityapp.com/zande!2015) 

STAGE:  http://45.55.134.111:9001/admin (info@getvicinityapp.com/Zande!2015)

PROD:   http://45.55.15.229:9000/admin (info@getvicinityapp.com/zande!2015)

*SSH:*

DEV:    ssh root@45.55.134.111 (videv98powRt)

STAGE:  ssh root@45.55.134.111 (videv98powRt)

PROD:   ssh root@45.55.15.229 (vi98powRt)

*Digital Ocean:*

https://cloud.digitalocean.com (info@getvicinityapp.com/Zande!2015)

### Branch IO ###

https://branch.io (info@getvicinityapp.com/zande2015)

### Mailchimp ###

http://mailchimp.com (vicinitylocalnetwork/ZandE!2015)