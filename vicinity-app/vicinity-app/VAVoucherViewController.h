//
//  VAVoucherViewController.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 04.08.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VADeal.h"

@interface VAVoucherViewController : UIViewController
@property (assign, nonatomic) BOOL isViewVoucherMode;                 //mode for displaying one voucher from Vouchers screen (default is NO)
@property (strong, nonatomic) VADeal *deal;
@end
