//
//  VAPhotoTool.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 29.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol VAPhotoToolDelegate;

@interface VAPhotoTool : NSObject
@property (weak, nonatomic) id <VAPhotoToolDelegate> delegate;

+ (VAPhotoTool *)defaultPhoto;

- (void)showPhotoActionViewOnController:(UIViewController *)controller;

- (void)openPhotoBrowserFor:(UIImage *)image;

@end

@protocol VAPhotoToolDelegate <NSObject>
@required
- (void)photoHasBeenEdited:(UIImage *)photo landscape:(BOOL)isLandscape;

@optional
- (void)photoActionViewHasBeenDismissed;
@end