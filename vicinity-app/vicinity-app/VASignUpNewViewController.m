//
//  VASignUpNewViewController.m
//  vicinity-app
//
//  Created by vadzim vasileuski on 3/25/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import "VASignUpNewViewController.h"
#import "VAUnderlinableView.h"
#import "VAAutoCompleteTextField.h"
#import "VAConfirmEmailViewController.h"
#import "VACreateAccountViewController.h"

#import "VAControlTool.h"
#import "VANotificationMessage.h"

#import "VAUserApiManager.h"
#import "VALoginResponse.h"
#import "VAUserDefaultsHelper.h"

#import "VAAutoCompleteObject.h"
#import "VAAutocompleteDataSource.h"

@interface VASignUpNewViewController () <UITextFieldDelegate, VAConfirmEmailDelegate, MLPAutoCompleteTextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *backgroundView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

//input text fields
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet VAAutoCompleteTextField *countryField;
@property (weak, nonatomic) IBOutlet UITextField *cityField;
@property (weak, nonatomic) IBOutlet VAAutoCompleteTextField *stateField;

//only for fontsize setup
@property (weak, nonatomic) IBOutlet UIButton *letsGoButton;

//views to make translucent effect
@property (weak, nonatomic) IBOutlet UIView *privacyView;
@property (weak, nonatomic) IBOutlet VAUnderlinableView *nameFieldView;
@property (weak, nonatomic) IBOutlet VAUnderlinableView *emailFieldView;
@property (weak, nonatomic) IBOutlet VAUnderlinableView *countryFieldView;
@property (weak, nonatomic) IBOutlet VAUnderlinableView *cityFieldView;
@property (weak, nonatomic) IBOutlet VAUnderlinableView *stateFieldView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

//proportional layouting
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topMargin;

//keyboard management
@property (strong, nonatomic) UITextField *activeField;

@property (nonatomic, strong) VAAutocompleteDataSource *countriesDataSource;
@property (nonatomic, strong) VAAutocompleteDataSource *statesDataSource;

@property (nonatomic, strong) NSArray *stateViewShowConstraints;
@property (nonatomic, strong) NSArray *stateViewHideConstraints;

//actions
- (IBAction)termsButtonTapped:(id)sender;
- (IBAction)privacyButtonTapped:(id)sender;
- (IBAction)backButtonTapped:(id)sender;
- (IBAction)letsGoButtonTapped:(id)sender;

//for red underlining
@property (nonatomic) BOOL isNameDidEndEditing;
@property (nonatomic) BOOL isEmailDidEndEditing;
@property (nonatomic) BOOL isCountryDidEndEditing;
@property (nonatomic) BOOL isCityDidEndEditing;
@property (nonatomic) BOOL isStateDidEndEditing;

@end

@implementation VASignUpNewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpVerticalSpacingConstraints];
    [self setUpFontSizes];
    [self setUpViewsAlpha];
    [self setUpGestureRecognisers];
    [self setUpPlaceholders];
    [self disableLetsGoButton];
    // Do any additional setup after loading the view.
    
    [self setupAutocompleteTextFields];
    [self setupStateViewConstraints];
    [self hideStateView:NO];
    
    if (self.pendingUser) {
        [self setupFieldsForPendingUser];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setUpKeyboardNotifications];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - setup methods
- (void)setUpViewsAlpha {
    //Need to set dynamically because Interface Builder sets alpha channel also to subviews
    self.nameFieldView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.27];
    self.emailFieldView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.27];
    self.countryFieldView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.27];
    self.cityFieldView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.27];
    self.stateFieldView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.27];
    self.privacyView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.38];
}

- (void)setUpVerticalSpacingConstraints {
    CGFloat mainViewHeight = self.view.frame.size.height;
    self.topMargin.constant = 0.18f * mainViewHeight;
}

- (void)setUpFontSizes {
    if (self.view.frame.size.height < 660) {
        //iphone 5 and smaller
        CGFloat fontSizeDiff = 2.0f;
        if (self.view.frame.size.height < 560) {
            //iphone 4 and smaller
            fontSizeDiff = 4.0f;
        }
        self.letsGoButton.titleLabel.font = [self.letsGoButton.titleLabel.font fontWithSize:(self.letsGoButton.titleLabel.font.pointSize - fontSizeDiff)];
        self.nameField.font = [self.nameField.font fontWithSize:(self.nameField.font.pointSize - fontSizeDiff)];
        self.emailField.font = [self.emailField.font fontWithSize:(self.emailField.font.pointSize - fontSizeDiff)];
        self.countryField.font = [self.countryField.font fontWithSize:(self.countryField.font.pointSize - fontSizeDiff)];
        self.cityField.font = [self.cityField.font fontWithSize:(self.cityField.font.pointSize - fontSizeDiff)];
        self.stateField.font = [self.stateField.font fontWithSize:(self.stateField.font.pointSize - fontSizeDiff)];
    }
}

- (void)setUpKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)setUpGestureRecognisers {
    //tap on background to hide keyboard
    UITapGestureRecognizer *tapBackground = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
   [self.backgroundView addGestureRecognizer:tapBackground];
}

- (void)setUpPlaceholders {
    [self setUpPlaceholderForTextField:self.nameField];
    [self setUpPlaceholderForTextField:self.emailField];
    [self setUpPlaceholderForTextField:self.countryField];
    [self setUpPlaceholderForTextField:self.cityField];
    [self setUpPlaceholderForTextField:self.stateField];
}

- (void)setUpPlaceholderForTextField:(UITextField *)textField{
    NSAttributedString *placeholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{ NSForegroundColorAttributeName : [[UIColor whiteColor] colorWithAlphaComponent:1.0f]}];
    textField.attributedPlaceholder = placeholder;
}

- (void)setupFieldsForPendingUser {
    self.nameField.text = self.pendingUser.fullname;
    if (self.nameField.text.length > 0) {
        self.isNameDidEndEditing = YES;
    }
    self.emailField.text = self.pendingUser.email;
    if (self.emailField.text.length > 0) {
        self.isEmailDidEndEditing = YES;
    }
    self.cityField.text = self.pendingUser.city;
    if (self.cityField.text.length > 0) {
        self.isCityDidEndEditing = YES;
    }

    if ([self.pendingUser.country isEqualToString:@"United States"]) {
        [self showStateView];
        self.stateField.text = self.pendingUser.state;
        if (self.stateField.text.length > 0) {
            self.isStateDidEndEditing = YES;
        }
    }
    
    self.countryField.text = self.pendingUser.country;
    if (self.countryField.text.length > 0) {
        self.isCountryDidEndEditing = YES;
    }
    
    [self validateAllFields];
    if ([self allFieldsAreValid]) {
        [self enableLetsGoButton];
    } else {
        [self disableLetsGoButton];
    }
}


- (void)setupAutocompleteTextFields {
    self.countriesDataSource = [[VAAutocompleteDataSource alloc] initWithNames:[VAAutocompleteDataSource countries]];
    [self setupAutoCompleteTextField:self.countryField];
    self.countryField.autoCompleteDataSource = self.countriesDataSource;
    self.countryField.autoCompleteDelegate = self;
    
    self.statesDataSource = [[VAAutocompleteDataSource alloc] initWithNames:[VAAutocompleteDataSource states]];
    [self setupAutoCompleteTextField:self.stateField];
    self.stateField.autoCompleteDataSource = self.statesDataSource;
    self.stateField.autoCompleteDelegate = self;
}

- (void)setupAutoCompleteTextField:(VAAutoCompleteTextField *)textField {
    textField.maximumNumberOfAutoCompleteRows = 3;
    textField.applyBoldEffectToAutoCompleteSuggestions = YES;
    textField.reverseAutoCompleteSuggestionsBoldEffect = YES;
    textField.showTextFieldDropShadowWhenAutoCompleteTableIsOpen = NO;
    textField.autoCompleteTableOriginOffset = CGSizeMake(0, 0);
    textField.autoCompleteTableCornerRadius = 1;
    textField.autoCompleteContentInsets = UIEdgeInsetsMake(0, -8.0f, 0, 0);
    textField.autoCompleteTableBorderWidth = 0;
    textField.autoCompleteRowHeight = 28.0f;
    textField.autoCompleteFontSize = 16.0f;
    textField.autoCompleteBoldFontName = @"Avenir-Heavy";
    textField.autoCompleteRegularFontName = @"Avenir-Book";
    textField.autoCompleteTableCellTextColor = [[UIColor blackColor] colorWithAlphaComponent:0.87f];
    textField.autoCompleteTableBackgroundColor = [UIColor whiteColor];
    textField.autoCompleteTableCellBackgroundColor = [UIColor clearColor];
    textField.autoCompleteTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [textField setAutoCompleteTableAppearsAsKeyboardAccessory:NO];
}

#pragma mark - keyboard management
- (void)hideKeyboard {
    [self.activeField resignFirstResponder];
}

- (void)willKeyboardShow:(NSNotification*)notification {
    NSDictionary* info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;

    CGPoint originInSuperview = [self.scrollView convertPoint:CGPointZero fromView:self.letsGoButton];
    CGFloat yOffset = (originInSuperview.y + self.letsGoButton.frame.size.height + 15) - (self.scrollView.frame.size.height - kbSize.height);
    
    if (yOffset > 0) {
        self.scrollView.contentOffset = CGPointMake(0, yOffset);
    }
}

- (void)willKeyboardHide:(NSNotification*)notification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    self.scrollView.contentOffset = CGPointZero;
}

#pragma mark - UITextFieldDelegate methods
- (void)textFieldDidBeginEditing:(UITextField *)textField {
   [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(UITextFieldTextDidChange:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:textField];
    
    
    self.activeField = textField;
    
    
    ///From old View Controller
    if ([textField isEqual:self.emailField]) {
        if (![[VAControlTool defaultControl] validEmailInTextField:self.emailField] && self.emailFieldView.isUnderlinded) {
            [VANotificationMessage showFillInEmailMessageOnController:self withDuration:5.f];
        }
    } else if ([textField isEqual:self.nameField] && self.nameFieldView.isUnderlinded) {
        if (self.nameField.text.length < 3) {
            [VANotificationMessage showFillInNameMessageOnController:self withDuration:5.f];
        }
    } else if ([textField isEqual:self.cityField] && self.cityFieldView.isUnderlinded) {
        if (textField.text.length < 3) {
            [VANotificationMessage showFillInCityMessageOnController:self withDuration:5.f];
        }
    } else if ([textField isEqual:self.countryField] && self.countryFieldView.isUnderlinded) {
        if (![self isValidCountry]) {
            [VANotificationMessage showFillInCountryMessageOnController:self withDuration:5.f];
        }
    }

}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UITextFieldTextDidChangeNotification
                                                  object:textField];
    
    if ([textField isEqual:self.countryField]) {
        if ([self.countryField.text isEqualToString:@"United States"]) {
            [self showStateView];
        } else {
            [self hideStateView:YES];
        }
        if (textField.text.length > 0) {
            self.isCountryDidEndEditing = YES;
        }        
    }
    
    if ([textField isEqual:self.nameField] && (textField.text.length > 0)) {
        self.isNameDidEndEditing = YES;
    }
    if ([textField isEqual:self.emailField] && (textField.text.length > 0)) {
        self.isEmailDidEndEditing = YES;
    }
    if ([textField isEqual:self.cityField] && (textField.text.length > 0)) {
        self.isCityDidEndEditing = YES;
    }
    if ([textField isEqual:self.stateField] && (textField.text.length > 0)) {
        self.isStateDidEndEditing = YES;
    }
    [self validateAllFields];
    
    
    if ([self allFieldsAreValid]) {
        [self enableLetsGoButton];
    } else {
        [self disableLetsGoButton];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([textField isEqual:self.emailField]) {
        return [[VAControlTool defaultControl] setEmailMaskForTextField:textField InRange:range replacementString:string];
    } else if ([textField isEqual:self.nameField] || [textField isEqual:self.cityField] || [textField isEqual:self.countryField]) {
        return [[VAControlTool defaultControl] setNameAndSurnameMaskForTextField:textField InRange:range replacementString:string];
    } else {
        return YES;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    UITextField *nextField = [self nextTextFieldFrom:textField];
    if (nextField != nil) {
        [nextField becomeFirstResponder];
    } else {
        [self hideKeyboard];
         if ([self allFieldsAreValid]) {
             [self sendCreateUserRequest];
         }
    }
    
    return YES;
}
- (BOOL)textFieldShouldClear:(UITextField *)textField {
    [self disableLetsGoButton];
    return YES;
}

- (void) UITextFieldTextDidChange:(NSNotification*)notification
{
    [self validateAllFields];
    if ([self allFieldsAreValid]) {
        [self enableLetsGoButton];
    } else {
        [self disableLetsGoButton];
    }
    [self.activeField reloadInputViews];
}

#pragma mark - text field validation
- (void)drawUnderlineForView:(VAUnderlinableView *)view {
    [view drawUnderlineWithColor:[UIColor colorWithRed:208.0f/255 green:2.0f/255 blue:27.0f/255 alpha:1.0f] width:1.5];
}

- (void)disableLetsGoButton {
    self.letsGoButton.enabled = NO;
    self.letsGoButton.backgroundColor = [self.letsGoButton.backgroundColor colorWithAlphaComponent:0.5];
    
    /*self.nameField.returnKeyType = UIReturnKeyNext;
    self.emailField.returnKeyType = UIReturnKeyNext;
    self.cityField.returnKeyType = UIReturnKeyNext;
    self.countryField.returnKeyType = UIReturnKeyNext;
    self.stateField.returnKeyType = UIReturnKeyNext;*/
}

- (void)enableLetsGoButton {
    self.letsGoButton.enabled = YES;
    self.letsGoButton.backgroundColor = [self.letsGoButton.backgroundColor colorWithAlphaComponent:1.0];
    
    /*self.nameField.returnKeyType = UIReturnKeyGo;
    self.emailField.returnKeyType = UIReturnKeyGo;
    self.cityField.returnKeyType = UIReturnKeyGo;
    self.countryField.returnKeyType = UIReturnKeyGo;
    self.stateField.returnKeyType = UIReturnKeyGo;*/
}

- (BOOL)allFieldsAreValid {
    if (self.nameField.text.length < 3) {
        return NO;
    } else if (self.cityField.text.length < 3) {
        return NO;
    } else if (![self isValidCountry]) {
        return NO;
    } else if ([self.countryField.text isEqualToString:@"United States"] && ![self isValidState]) {
        return NO;
    }  else if (![[VAControlTool defaultControl] validEmailInTextField:self.emailField]) {
        return NO;
    } else {
        return YES;
    }
}

- (void)validateAllFields {
    if ( self.nameField.text.length < 3 ) {
        if (self.isNameDidEndEditing) {
            [self drawUnderlineForView:self.nameFieldView];
        }
    } else {
        [self.nameFieldView removeUnderline];
    }

    if (self.cityField.text.length < 3){
        if (self.isCityDidEndEditing) {
            [self drawUnderlineForView:self.cityFieldView];
        }
    } else {
        [self.cityFieldView removeUnderline];
    }

    if (![self isValidCountry]){
        if (self.isCountryDidEndEditing) {
            [self drawUnderlineForView:self.countryFieldView];
        }
    } else {
        [self.countryFieldView removeUnderline];
    }

    if (![self isValidState]){
        if (self.isStateDidEndEditing) {
            [self drawUnderlineForView:self.stateFieldView];
        }
    } else {
        [self.stateFieldView removeUnderline];
    }

    if ( ![[VAControlTool defaultControl] validEmailInTextField:self.emailField] ){
        if (self.isEmailDidEndEditing) {
            [self drawUnderlineForView:self.emailFieldView];
        }
    } else {
        [self.emailFieldView removeUnderline];
    }
}

- (BOOL)isValidCountry {
    if ([self.countryField.text length]) {
        NSArray *countries = [VAAutocompleteDataSource countries];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self == %@", self.countryField.text];
        NSString *country = [[countries filteredArrayUsingPredicate:predicate] firstObject];
        if (country) {
            return YES;
        }
        return NO;
    }
    return NO;
}

- (BOOL)isValidState {
    if ([self.stateField.text length]) {
        NSArray *states = [VAAutocompleteDataSource states];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self == %@", self.stateField.text];
        NSString *state = [[states filteredArrayUsingPredicate:predicate] firstObject];
        if (state) {
            return YES;
        }
        return NO;
    }
    return NO;
}

- (UITextField *)nextTextFieldFrom:(UITextField *)textField {
    //button go avaliable
    /*if ([self allFieldsAreValid]) {
        return nil;
    }*/
    
    //next text field
    NSMutableArray *fields = [NSMutableArray arrayWithObjects:self.nameField, self.emailField, self.countryField, self.cityField, nil];
    if ([self.countryField.text isEqualToString:@"United States"]) {
        [fields addObject:self.stateField];
    }
    
    NSInteger currentFieldIndex = 0;
    for (UITextField *field in fields) {
        if ([field isEqual:textField]) {
            break;
        }
        currentFieldIndex++;
    }
    
    NSInteger nextFiledIndex = currentFieldIndex + 1;
    if (nextFiledIndex >= [fields count]) {
        nextFiledIndex = 0;
        return nil;
    }

    return fields[nextFiledIndex];
    
}

#pragma mark - actions
- (IBAction)termsButtonTapped:(id)sender {
    NSString *urlString = @"http://getvicinityapp.com/terms/";
    NSURL *url = [NSURL URLWithString:urlString];
    [[UIApplication sharedApplication] openURL:url];
}

- (IBAction)privacyButtonTapped:(id)sender {
    NSString *urlString = @"http://getvicinityapp.com/policy/";
    NSURL *url = [NSURL URLWithString:urlString];
    [[UIApplication sharedApplication] openURL:url];
}

- (IBAction)backButtonTapped:(id)sender {
    [self hideKeyboard];
    
    NSArray *viewControllers = self.navigationController.viewControllers;
    VACreateAccountViewController *createAccountVC = viewControllers[viewControllers.count - 2];
    if (!self.pendingUser) {
        self.pendingUser = [[VAUser alloc] init];
    }
    self.pendingUser.fullname = self.nameField.text;
    self.pendingUser.country = self.countryField.text;
    self.pendingUser.city = self.cityField.text;
    self.pendingUser.state = self.stateField.text;
    self.pendingUser.email = self.emailField.text;
    createAccountVC.pendingUser = self.pendingUser;
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)letsGoButtonTapped:(id)sender {
    //TODO add activity
    [self hideKeyboard];
    [self sendCreateUserRequest];
}


#pragma mark - server requests

- (void)sendCreateUserRequest {
    [self showActivityIndicator];
    VAUserApiManager *manager = [VAUserApiManager new];
    [manager postCreateUserWithEmail:self.emailField.text
                            password:self.password
                            fullname:self.nameField.text
                            username:self.username
                                city:self.cityField.text
                               state:self.stateField.text
                             country:self.countryField.text
                              gender:nil//self.genderTypeView.genderType == VAGenderTypeFemale ? @"female" : @"male"
                         avatarImage:self.avatarImage
                       andCompletion:^(NSError *error, VAModel *model) {
                           if (model) {
                               VALoginResponse *response = (VALoginResponse *)model;
                               VAUser *user = response.loggedUser;
                               [VAUserDefaultsHelper saveModel:user forKey:VASaveKeys_CurrentUser];
                               [VAUserDefaultsHelper setAuthToken:response.sessionId];
                               
                               VAConfirmEmailViewController *vc = [[UIStoryboard storyboardWithName:@"Vicinity" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"confirmEmailViewController"];
                               vc.isEmailUpdated = NO;
                               vc.delegate = self;
                               [self.navigationController pushViewController:vc animated:YES];
                               
                           } else if (error) {
                               
                               if ([error.localizedDescription isEqual:@(100001)]) {
                                   [VANotificationMessage showEmailInUseMessageOnController:self forEmail:self.emailField.text withDuration:5.f];
                                   [self drawUnderlineForView:self.emailFieldView];
                               } else if ([error.localizedDescription isEqual:@(100002)]) {
                                   [VANotificationMessage showUsernameInUseMessageOnController:self forUsername:self.username withDuration:5.f];
//                                   [self.usernameIcon setImage:[UIImage imageNamed:@"profile-username-icon-red"]];
                               } else {
                                   [VANotificationMessage showCreateAccountErrorMessageOnController:self withDuration:5.f];
                               }
                           }
                           [self hideActivityIndicator];
                       }];
}
#pragma mark - Confirm email delegate

- (void)didRestoreOriginalEmailForUser:(VAUser *)user {
    self.pendingUser = user;
}

#pragma mark - MLPAutoCompleteTextField Delegate

- (BOOL)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
          shouldConfigureCell:(UITableViewCell *)cell
       withAutoCompleteString:(NSString *)autocompleteString
         withAttributedString:(NSAttributedString *)boldedString
        forAutoCompleteObject:(id<MLPAutoCompletionObject>)autocompleteObject
            forRowAtIndexPath:(NSIndexPath *)indexPath;
{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return YES;
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
  didSelectAutoCompleteString:(NSString *)selectedString
       withAutoCompleteObject:(id<MLPAutoCompletionObject>)selectedObject
            forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(selectedObject){
        NSLog(@"selected object from autocomplete menu %@ with string %@", selectedObject, [selectedObject autocompleteString]);
    } else {
        NSLog(@"selected string '%@' from autocomplete menu", selectedString);
        [[NSNotificationCenter defaultCenter] postNotificationName:UITextFieldTextDidChangeNotification object:textField];
    }
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField willHideAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    NSLog(@"Autocomplete table view will be removed from the view hierarchy");
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField willShowAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    NSLog(@"Autocomplete table view will be added to the view hierarchy");
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField didHideAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    NSLog(@"Autocomplete table view ws removed from the view hierarchy");
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField didShowAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    NSLog(@"Autocomplete table view was added to the view hierarchy");
}

#pragma mark - State view

- (void)setupStateViewConstraints {
    self.stateViewShowConstraints = @[[NSLayoutConstraint constraintWithItem:self.cityFieldView
                                                                   attribute:NSLayoutAttributeRight
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:self.stateFieldView
                                                                   attribute:NSLayoutAttributeLeft
                                                                  multiplier:1
                                                                    constant:-8.0f],
                                      [NSLayoutConstraint constraintWithItem:self.stateFieldView
                                                                   attribute:NSLayoutAttributeTrailing
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:self.contentView
                                                                   attribute:NSLayoutAttributeTrailing
                                                                  multiplier:1
                                                                    constant:-20.0f]];
    self.stateViewHideConstraints = @[[NSLayoutConstraint constraintWithItem:self.cityFieldView
                                                                   attribute:NSLayoutAttributeRight
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:self.stateFieldView
                                                                   attribute:NSLayoutAttributeLeft
                                                                  multiplier:1
                                                                    constant:-20.0f],
                                      [NSLayoutConstraint constraintWithItem:self.stateFieldView
                                                                   attribute:NSLayoutAttributeLeading
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:self.contentView
                                                                   attribute:NSLayoutAttributeTrailing
                                                                  multiplier:1
                                                                    constant:0]];
}

- (void)showStateView {
    [self.contentView removeConstraints:self.stateViewHideConstraints];
    [self.contentView addConstraints:self.stateViewShowConstraints];
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
    
    self.cityField.returnKeyType = UIReturnKeyNext;
    self.stateField.returnKeyType = UIReturnKeyGo;
}

- (void)hideStateView:(BOOL)animated {
    [self.contentView removeConstraints:self.stateViewShowConstraints];
    [self.contentView addConstraints:self.stateViewHideConstraints];
    if (animated) {
        [UIView animateWithDuration:0.3f animations:^{
            [self.view layoutIfNeeded];
        }];
    }
    
    self.cityField.returnKeyType = UIReturnKeyGo;
    self.stateField.returnKeyType = UIReturnKeyNext;
}

- (void)showActivityIndicator {
    self.view.userInteractionEnabled = NO;
    [self.letsGoButton setTitle:@"" forState:UIControlStateNormal];
    [self.activityIndicator startAnimating];
}

- (void)hideActivityIndicator {
    [self.activityIndicator stopAnimating];
    [self.letsGoButton setTitle:@"LET'S GO!" forState:UIControlStateNormal];
    self.view.userInteractionEnabled = YES;
}

@end
