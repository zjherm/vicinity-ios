//
//  VABugViewController.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 16.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VABugViewController.h"
#import "REFrostedViewController.h"
#import "VAControlTool.h"
#import "VANotificationMessage.h"
#import "VAUserApiManager.h"
#import "VAServerResponse.h"
#import "DEMONavigationController.h"
#import "UIBarButtonItem+Badge.h"
#import "VAUserDefaultsHelper.h"
#import "VANotificationTool.h"
#import "VALoaderView.h"
#import "VADesignTool.h"
#import <Google/Analytics.h>

typedef NS_ENUM(NSUInteger, VAReportType) {
    VAReportTypeRequest = 0,
    VAReportTypeBugReport,
    VAReportTypeGeneral
};

static NSString *kRequestString = @"request";
static NSString *kBugReportString = @"bugReport";
static NSString *kGeneralString = @"General";

@interface VABugViewController ()
@property (weak, nonatomic) IBOutlet UITextView *infoTextView;
@property (weak, nonatomic) IBOutlet UIView *requestView;
@property (weak, nonatomic) IBOutlet UIView *bugReportView;
@property (weak, nonatomic) IBOutlet UIView *generalView;

@property (weak, nonatomic) IBOutlet UILabel *requestLabel;
@property (weak, nonatomic) IBOutlet UILabel *bugReportLabel;
@property (weak, nonatomic) IBOutlet UILabel *generalLabel;

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UILabel *placeholderLabel;
@property (weak, nonatomic) IBOutlet UIView *segmentedView;
@property (weak, nonatomic) IBOutlet UIView *stateView;
@property (weak, nonatomic) IBOutlet UIView *fieldsView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomScrollViewConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewHeightConstraint;

// Constraints
@property (nonatomic, strong) NSArray *requestStateViewConstraints;
@property (nonatomic, strong) NSArray *bugReportStateViewConstraints;
@property (nonatomic, strong) NSArray *generalStateViewConstraints;

@property (assign, nonatomic) VAReportType reportType;
@property (strong, nonatomic) id activeField;
@property (strong, nonatomic) UIView *backgroundView;
@property (strong, nonatomic) UIBarButtonItem *leftItem;
@property (strong, nonatomic) VALoaderView *loader;

- (IBAction)actionClear:(id)sender;
- (IBAction)actionSubmit:(id)sender;
@end

@implementation VABugViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupConstraints];
    
    [(DEMONavigationController *)self.navigationController setDefaultNavigationBar];
    
    UITapGestureRecognizer *requestTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectRequestType:)];
    [self.requestView addGestureRecognizer:requestTap];
    UITapGestureRecognizer *bugReportTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectBugReportType:)];
    [self.bugReportView addGestureRecognizer:bugReportTap];
    UITapGestureRecognizer *generalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectGeneralType:)];
    [self.generalView addGestureRecognizer:generalTap];
    
    UITapGestureRecognizer *resignKeyboardTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:resignKeyboardTap];
    
    self.reportType = VAReportTypeRequest;
    
    self.textView.textContainerInset = UIEdgeInsetsMake(15.5, 8.0, 15.5, 8.0);
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Menu Icon"] style:UIBarButtonItemStylePlain target:self.navigationController action:@selector(showMenu)];
    self.navigationItem.leftBarButtonItem = leftItem;
    self.leftItem = leftItem;
    
    self.activeField = nil;
    self.loader = nil;
    self.backgroundView = nil;
    
    self.fieldsView.layer.cornerRadius = 5.f;
    self.fieldsView.clipsToBounds = YES;

    self.textView.layer.cornerRadius = 5.f;
    self.textView.layer.borderWidth = 1.f;
    self.textView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.textView.clipsToBounds = YES;
    
    self.clearButton.layer.cornerRadius = 5.f;
    self.clearButton.clipsToBounds = YES;
    
    self.submitButton.layer.cornerRadius = 5.f;
    self.clearButton.clipsToBounds = YES;
    
    self.submitButton.backgroundColor = [UIColor lightGrayColor];
    self.submitButton.enabled = NO;
    
    self.clearButton.backgroundColor = [UIColor lightGrayColor];
    self.clearButton.enabled = NO;
    
    self.containerViewHeightConstraint.constant = CGRectGetHeight(self.view.bounds) - 64.f;        //this screen has navigation bar
    
    self.infoTextView.text = @"We appreciate any feedback, bug submissions or any sort of general comment, positive or negative. \nOur community is what keeps this app fun. \nThank you so much for your time.";
    self.infoTextView.textColor = [UIColor lightGrayColor];
    self.infoTextView.textAlignment = NSTextAlignmentCenter;
    self.infoTextView.font = [[VADesignTool defaultDesign] vicinityRegularFontOfSize:10.f];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    [self addBadgeObservers];
    if ([VAUserDefaultsHelper shouldShowNotificationBadge]) {
        [self showButtonBadge];
    }
    
    self.navigationController.title = @"Feedback / Bug Report";
    
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Feedback screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];  
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Setup UI

- (void)setupConstraints {
    self.requestStateViewConstraints = @[[NSLayoutConstraint constraintWithItem:self.stateView
                                                                      attribute:NSLayoutAttributeLeading
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.requestLabel
                                                                      attribute:NSLayoutAttributeLeading
                                                                     multiplier:1
                                                                       constant:0],
                                         [NSLayoutConstraint constraintWithItem:self.stateView
                                                                      attribute:NSLayoutAttributeTrailing
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.requestLabel
                                                                      attribute:NSLayoutAttributeTrailing
                                                                     multiplier:1
                                                                       constant:0],
                                         [NSLayoutConstraint constraintWithItem:self.stateView
                                                                      attribute:NSLayoutAttributeTop
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.requestLabel
                                                                      attribute:NSLayoutAttributeBottom
                                                                     multiplier:1
                                                                       constant:6.0f]];
    
    self.bugReportStateViewConstraints = @[[NSLayoutConstraint constraintWithItem:self.stateView
                                                                      attribute:NSLayoutAttributeLeading
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.bugReportLabel
                                                                      attribute:NSLayoutAttributeLeading
                                                                     multiplier:1
                                                                       constant:0],
                                         [NSLayoutConstraint constraintWithItem:self.stateView
                                                                      attribute:NSLayoutAttributeTrailing
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.bugReportLabel
                                                                      attribute:NSLayoutAttributeTrailing
                                                                     multiplier:1
                                                                       constant:0],
                                         [NSLayoutConstraint constraintWithItem:self.stateView
                                                                      attribute:NSLayoutAttributeTop
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.bugReportLabel
                                                                      attribute:NSLayoutAttributeBottom
                                                                     multiplier:1
                                                                       constant:6.0f]];
    
    self.generalStateViewConstraints = @[[NSLayoutConstraint constraintWithItem:self.stateView
                                                                      attribute:NSLayoutAttributeLeading
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.generalLabel
                                                                      attribute:NSLayoutAttributeLeading
                                                                     multiplier:1
                                                                       constant:0],
                                         [NSLayoutConstraint constraintWithItem:self.stateView
                                                                      attribute:NSLayoutAttributeTrailing
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.generalLabel
                                                                      attribute:NSLayoutAttributeTrailing
                                                                     multiplier:1
                                                                       constant:0],
                                         [NSLayoutConstraint constraintWithItem:self.stateView
                                                                      attribute:NSLayoutAttributeTop
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.generalLabel
                                                                      attribute:NSLayoutAttributeBottom
                                                                     multiplier:1
                                                                       constant:6.0f]];
    
    [self.segmentedView addConstraints:self.requestStateViewConstraints];
}

#pragma mark - Properties

- (void)setReportType:(VAReportType)reportType {
    _reportType = reportType;
}

#pragma mark - Taps

- (void)didSelectRequestType:(id)sender {
    self.reportType = VAReportTypeRequest;
    
    [self.segmentedView removeConstraints:self.bugReportStateViewConstraints];
    [self.segmentedView removeConstraints:self.generalStateViewConstraints];
    [self.segmentedView addConstraints:self.requestStateViewConstraints];
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.segmentedView layoutIfNeeded];
    }];
}

- (void)didSelectBugReportType:(id)sender {
    self.reportType = VAReportTypeBugReport;
    
    [self.segmentedView removeConstraints:self.requestStateViewConstraints];
    [self.segmentedView removeConstraints:self.generalStateViewConstraints];
    [self.segmentedView addConstraints:self.bugReportStateViewConstraints];
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.segmentedView layoutIfNeeded];
    }];
}

- (void)didSelectGeneralType:(id)sender {
    self.reportType = VAReportTypeGeneral;
    
    [self.segmentedView removeConstraints:self.requestStateViewConstraints];
    [self.segmentedView removeConstraints:self.bugReportStateViewConstraints];
    [self.segmentedView addConstraints:self.generalStateViewConstraints];
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.segmentedView layoutIfNeeded];
    }];
}

#pragma mark - Keyboard actions

- (void)willKeyboardShow:(NSNotification*)notification {
    
    NSDictionary* info = [notification userInfo];
    CGRect keyRect;
    [[info objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyRect];
    CGFloat height = keyRect.size.height;
    
    self.bottomScrollViewConstraint.constant = height;
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
    
//    CGRect rc = [self.activeField bounds];
//    rc = [self.textView convertRect:rc toView:self.scrollView];
//    [self.scrollView scrollRectToVisible:rc animated:YES];
}

- (void)willKeyboardHide:(NSNotification*)notification {
    
    self.bottomScrollViewConstraint.constant = 0.0;
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    self.activeField = textView;
    [self setButtonsAttributes];
}

- (void)textViewDidChange:(UITextView *)textView {
    [self setButtonsAttributes];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    self.activeField = textView;
    return YES;
}

#pragma mark - Methods

- (void)setButtonsAttributes {
    if ([self.textView.text isEqualToString:@""]) {
        [self.placeholderLabel setHidden:NO];
        self.submitButton.enabled = NO;
        self.clearButton.enabled = NO;
        self.submitButton.backgroundColor = [UIColor lightGrayColor];
        self.clearButton.backgroundColor = [UIColor lightGrayColor];
    } else {
        [self.placeholderLabel setHidden:YES];
        self.submitButton.enabled = YES;
        self.clearButton.enabled = YES;
        self.clearButton.backgroundColor = [[VADesignTool defaultDesign] clearGray];
        self.submitButton.backgroundColor = [[VADesignTool defaultDesign] vicinityBlue];
    }
}

- (void)hideKeyboard {
    [self.activeField resignFirstResponder];
}

- (NSString *)stringValueForReportType:(VAReportType)reportType {
    switch (reportType) {
        case VAReportTypeBugReport:
            return kBugReportString;
        case VAReportTypeGeneral:
            return kGeneralString;
        case VAReportTypeRequest:
            return kRequestString;
        default:
            return nil;
    }
}

- (void)darkenBackground {
    UIView *backgroundView = [[UIView alloc] initWithFrame:self.view.bounds];
    backgroundView.backgroundColor = [UIColor blackColor];
    backgroundView.alpha = 0.f;
    self.backgroundView = backgroundView;
    [self.view addSubview:backgroundView];
    [UIView animateWithDuration:0.3f animations:^{
        backgroundView.alpha = 0.7f;
    }];
}

- (void)lightenBackground {
    [UIView animateWithDuration:0.3f animations:^{
        self.backgroundView.alpha = 0.0f;
    }];
    [self.backgroundView removeFromSuperview];
}

- (void)showActivityIndicator {
    VALoaderView *loader = [VALoaderView initWhiteLoader];
    loader.frame = CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), kLoaderHeight);
    [loader setCenter:CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds))];
    [self.backgroundView addSubview:loader];
    self.loader = loader;
    [loader startAnimating];
}

- (void)hideActivityIndicator {
    if (self.loader) {
        [self.loader stopAnimating];
        [self.loader removeFromSuperview];
        self.loader = nil;
    }
}

#pragma mark - Menu button badge

- (void)showButtonBadge {
    self.navigationItem.leftBarButtonItem = [self getMenuButtonWithBadge:YES];
}

- (void)hideButtonBadge {
    self.navigationItem.leftBarButtonItem = [self getMenuButtonWithBadge:NO];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kShouldShowNotificationBadge];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}
        
#pragma mark - Actions

- (IBAction)actionClear:(id)sender {
    self.textView.text = @"";
    [self setButtonsAttributes];
}

- (IBAction)actionSubmit:(id)sender {
    [self.textView resignFirstResponder];
    [self darkenBackground];
    [self showActivityIndicator];
    [[VAUserApiManager alloc] postReportWithText:self.textView.text
                                      deviceType:[[VAControlTool defaultControl] platformString]
                                 operationSystem:[NSString stringWithFormat:@"%@ %@", [[UIDevice currentDevice] systemName], [[UIDevice currentDevice] systemVersion]]
                              applicationVersion:[NSString stringWithFormat:@"%@ (%@)", [VAControlTool appVersion], [VAControlTool build]]
                                      reportType:[self stringValueForReportType:self.reportType]
                                  withCompletion:^(NSError *error, VAModel *model) {
                                      [self lightenBackground];
                                      [self hideActivityIndicator];
                                      UIViewController *vc = [VAControlTool topScreenController];
                                      if (error) {
                                          [VANotificationMessage showErrorFeedbackMessageOnController:vc withDuration:5.f];
                                      } else if (model) {
                                          VAServerResponse *response = (VAServerResponse *)model;
                                          if ([response.status isEqualToString:kSuccessResponseStatus]) {
                                              [self actionClear:nil];
                                              [VANotificationMessage showSuccessFeedbackMessageOnController:vc withDuration:5.f];
                                          } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                              [VANotificationMessage showErrorFeedbackMessageOnController:vc withDuration:5.f];
                                          }
                                      }
                                  }];
}

- (IBAction)rightSwipeRecognized:(id)sender {
    switch (self.reportType) {
        case VAReportTypeRequest:
            break;
            
        case VAReportTypeBugReport:
            [self didSelectRequestType:nil];
            break;
            
        case VAReportTypeGeneral:
            [self didSelectBugReportType:nil];
            break;
            
        default:
            break;
    }
}
- (IBAction)leftSwipeRecognized:(id)sender {
    switch (self.reportType) {
        case VAReportTypeRequest:
            [self didSelectBugReportType:nil];
            break;
            
        case VAReportTypeBugReport:
            [self didSelectGeneralType:nil];
            break;
            
        case VAReportTypeGeneral:
            break;
            
        default:
            break;
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

@end
