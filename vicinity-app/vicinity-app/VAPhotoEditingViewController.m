//
//  VAPhotoEditingViewController.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 26.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAPhotoEditingViewController.h"
#import "VAUserTool.h"
#import "VAProfileViewController.h"
#import "VAEventsViewController.h"
#import "DEMONavigationController.h"
#import "UIImage+VAFixOrientation.h"

@interface VAPhotoEditingViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *centerView;
@property (weak, nonatomic) IBOutlet UISwitch *modeSwitch;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cropZoneHeightConstraint;

@property (weak, nonatomic) IBOutlet UIView *landscapeView;
@property (weak, nonatomic) IBOutlet UIView *squareView;

@property (nonatomic, weak) IBOutlet UIView *modeView;

@property (strong, nonatomic) UIImageView *imageView;

@property (nonatomic, strong) NSLayoutConstraint *squareHeightConstraint;
@property (nonatomic, strong) NSLayoutConstraint *landscapeHeightConstraint;

@property (nonatomic, readonly) BOOL isAllowLandscape;

- (IBAction)actionGoBack:(id)sender;
- (IBAction)actionDone:(id)sender;
@end

@implementation VAPhotoEditingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication] setStatusBarHidden:YES];

    self.cropZoneHeightConstraint.constant = self.view.bounds.size.width;
    
    UIImageView *imageView = [UIImageView new];
    imageView.frame = self.view.frame;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.imageView = imageView;
    imageView.image = self.photo;
    self.scrollView.contentSize = CGSizeMake(CGRectGetWidth(imageView.frame), CGRectGetHeight(imageView.frame));
    self.scrollView.maximumZoomScale = 5.0;
    self.scrollView.minimumZoomScale = 1.0;
    self.scrollView.clipsToBounds = YES;
    [self.scrollView addSubview:imageView];
    
    CGFloat koeff = self.isAllowLandscape && !self.modeSwitch.isOn ? 16.0f/9.0f : 1.0f;
    if (self.photo.size.width/koeff > self.photo.size.height) {         //landscape photo
        CGFloat multiplier = self.photo.size.width / self.photo.size.height;
        self.scrollView.zoomScale = multiplier;
        self.scrollView.minimumZoomScale = multiplier;
    }
    
    CGFloat photoHeight = self.photo.size.height / (self.photo.size.width / CGRectGetWidth(self.view.frame));
    photoHeight = photoHeight * self.scrollView.zoomScale;
    
    CGFloat inset = (CGRectGetHeight(self.imageView.frame) - photoHeight) / 2;
    inset -= (CGRectGetHeight(self.view.bounds) - CGRectGetWidth(self.view.bounds)/koeff) / 2;            //width of self.view = height of center square view (difference is top and bottom bars height)
    [self.scrollView setContentInset:UIEdgeInsetsMake(-inset, 0, -inset, 0)];

    CGFloat xOffset = (CGRectGetWidth(self.view.bounds)*self.scrollView.zoomScale - CGRectGetWidth(self.view.bounds))/2;
    CGFloat yOffset = -self.scrollView.contentInset.top;
    yOffset = yOffset < 0 ? 0 : yOffset;
    self.scrollView.contentOffset = CGPointMake(xOffset, yOffset);
    
    if (self.isAllowLandscape) {
        self.modeView.hidden = NO;
        [self setupModeIcons];
        self.modeSwitch.layer.borderColor = [UIColor whiteColor].CGColor;
        self.modeSwitch.layer.borderWidth = 1.0f;
        self.modeSwitch.layer.cornerRadius = 15.5f;
        self.modeSwitch.clipsToBounds = YES;
    } else {
        self.modeView.hidden = YES;
    }
}

- (void)updateInsets {
    CGFloat prevZoomScale = self.scrollView.zoomScale;
    CGFloat koeff = self.isAllowLandscape && !self.modeSwitch.isOn ? 16.0f/9.0f : 1.0f;
    if (self.photo.size.width/koeff > self.photo.size.height) {         //landscape photo
        CGFloat multiplier = self.photo.size.width / self.photo.size.height;
        self.scrollView.minimumZoomScale = multiplier;
        self.scrollView.zoomScale = multiplier;
    } else {
        self.scrollView.minimumZoomScale = 1.0;
        self.scrollView.zoomScale = 1.0;
    }
    
    if (prevZoomScale == self.scrollView.zoomScale) {
        [self scrollViewDidZoom:self.scrollView];
    }
    
    CGFloat xOffset = (CGRectGetWidth(self.view.bounds)*self.scrollView.zoomScale - CGRectGetWidth(self.view.bounds))/2;
    CGFloat yOffset = -self.scrollView.contentInset.top;
    yOffset = yOffset < 0 ? 0 : yOffset;
    self.scrollView.contentOffset = CGPointMake(xOffset, yOffset);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setupConstraints];
}

- (void)setupConstraints {
    self.squareHeightConstraint = [NSLayoutConstraint constraintWithItem:self.centerView
                                                               attribute:NSLayoutAttributeWidth
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.centerView
                                                               attribute:NSLayoutAttributeHeight
                                                              multiplier:1.0f
                                                                constant:0];
    self.landscapeHeightConstraint = [NSLayoutConstraint constraintWithItem:self.centerView
                                                                  attribute:NSLayoutAttributeWidth
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:self.centerView
                                                                  attribute:NSLayoutAttributeHeight
                                                                 multiplier:16.0f/9.0f
                                                                   constant:0];
    if (self.isAllowLandscape) {
        [self.centerView addConstraint:self.landscapeHeightConstraint];
    } else {
        [self.centerView addConstraint:self.squareHeightConstraint];
    }
}

- (void)setupModeIcons {
    [self setupModeIcon:self.landscapeView];
    [self setupModeIcon:self.squareView];
}

- (void)setupModeIcon:(UIView *)modeIcon {
    modeIcon.layer.borderColor = [UIColor whiteColor].CGColor;
    modeIcon.layer.borderWidth = 2.5f;
    modeIcon.layer.cornerRadius = 2.5f;
    modeIcon.clipsToBounds = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)isAllowLandscape {
    return [self.flowController isKindOfClass:[VAEventsViewController class]];
}

#pragma mark - Actions

- (IBAction)actionGoBack:(id)sender {
    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)actionDone:(id)sender {
    UIImage *cropped = [self cropImageFromImageView:self.imageView withCropZone:self.centerView.frame];
    
    [self.delegate photoHasBeenTaken:cropped landscape:self.isAllowLandscape && !self.modeSwitch.isOn];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)modeSwitchValueChanged:(id)sender {
    if (self.modeSwitch.isOn) {
        [self.centerView removeConstraint:self.landscapeHeightConstraint];
        [self.centerView addConstraint:self.squareHeightConstraint];
    } else {
        [self.centerView removeConstraint:self.squareHeightConstraint];
        [self.centerView addConstraint:self.landscapeHeightConstraint];
    }
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
        [self updateInsets];
    }];
}

- (UIImage *)cropImageFromImageView:(UIImageView *)imageView withCropZone:(CGRect)cropRect {

    imageView.image = [imageView.image fixOrientation];
    
    CGFloat multiplier;
    
    double oriImgWid = imageView.image.size.width;
    double oriImgHgt = imageView.image.size.height;

    //MAGIC Moment
    BOOL portrait = [self isPortraitOrientation:oriImgHgt width:oriImgWid landscapeEditMode:[self isLandscapeEditMode]];
    if (oriImgWid < CGRectGetWidth(cropRect)) {
        multiplier = oriImgWid / CGRectGetWidth(cropRect);//stretch in width narrow pictures
    } else if (oriImgHgt < CGRectGetHeight(cropRect)) {
        multiplier = oriImgHgt / CGRectGetHeight(cropRect);//stretch in height short pictures
    } else if (oriImgWid > CGRectGetWidth(cropRect) && oriImgHgt > oriImgWid) {
        multiplier = oriImgWid / CGRectGetWidth(cropRect); //scale in width very height pictures
    } else if (oriImgHgt > CGRectGetHeight(cropRect) && oriImgWid > oriImgHgt) {
        multiplier = oriImgWid / CGRectGetWidth(cropRect); //scale in width very height pictures
    } else {
        if(portrait) {
            multiplier = oriImgHgt / CGRectGetHeight(cropRect); //scale in heigth where width > height but 16*height < 9*width
        } else {
            multiplier = oriImgWid / CGRectGetWidth(cropRect); //scale in width wide pictures
        }
    }

    
    multiplier /= self.scrollView.zoomScale;
    CGSize oriCropSize = CGSizeMake(CGRectGetWidth(cropRect) * multiplier, CGRectGetHeight(cropRect) * multiplier);
    
    CGPoint offset = self.scrollView.contentOffset;
    UIEdgeInsets inset = self.scrollView.contentInset;
    
    NSInteger topY = offset.y - (-inset.top);      //inset is < 0
    NSInteger leftX = offset.x;
    
    NSInteger originalX = leftX * multiplier;
    NSInteger originalY = topY * multiplier;
    
    CGRect oriCropRect = CGRectMake(originalX, originalY, oriCropSize.width, oriCropSize.height);
    oriCropRect = CGRectIntegral(oriCropRect);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([imageView.image CGImage], oriCropRect);
    
    UIImage *cropped = [UIImage imageWithCGImage:imageRef scale:1.0 orientation:imageView.image.imageOrientation];
    CGImageRelease(imageRef);
    return cropped;
}

- (BOOL)isLandscapeEditMode {
    return self.isAllowLandscape && !self.modeSwitch.isOn;
}

- (BOOL)isPortraitOrientation:(double)height width:(double)width landscapeEditMode:(BOOL)isLandscape{
    if (isLandscape){
        return height/width > 9.0f/16.0f;
    } else {
        return height > width;
    }
}

#pragma mark - UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.imageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    CGFloat photoHeight = self.photo.size.height / (self.photo.size.width / CGRectGetWidth(self.view.frame));
    photoHeight = photoHeight * scrollView.zoomScale;
    
    CGFloat inset = (CGRectGetHeight(self.imageView.frame) - photoHeight) / 2;
    inset -= (CGRectGetHeight(self.view.bounds) - CGRectGetHeight(self.centerView.bounds)) / 2;
    [scrollView setContentInset:UIEdgeInsetsMake(-inset, 0, -inset, 0)];
}

@end