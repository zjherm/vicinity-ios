//
//  VAPushNotificationView.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 10.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VAPushNotificationView : UIView
@property (strong, nonatomic) NSString *postID;
@property (strong, nonatomic) NSString *userNickname;

- (void)showPushMessageWithText:(NSString *)text;
@end
