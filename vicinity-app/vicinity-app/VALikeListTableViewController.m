//
//  VALikeListTableViewController.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 27.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VALikeListTableViewController.h"
#import "VALikeTableViewCell.h"
#import "VAUserApiManager.h"
#import "VALikesResponse.h"
#import "VALoaderView.h"
#import "VAUser.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "VANotificationMessage.h"
#import "VAProfileViewController.h"
#import "DEMONavigationController.h"
#import "VAControlTool.h"
#import <Google/Analytics.h>

@interface VALikeListTableViewController ()
@property (strong, nonatomic) NSArray *likeListArray;
@property (strong, nonatomic) VALoaderView *headerLoader;
@property (strong, nonatomic) VALoaderView *refresh;
@property (assign, nonatomic) BOOL isRefreshing;
@end

@implementation VALikeListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackButton"] style:UIBarButtonItemStylePlain target:self.navigationController action:@selector(popViewControllerAnimated:)];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    self.likeListArray = [NSArray array];
    
    if (!self.postID) {
        self.postID = @"";
    }
    VALoaderView *loader = [VALoaderView initLoader];
    loader.frame = CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), kLoaderHeight);
    [loader startAnimating];
    self.tableView.tableHeaderView = loader;
    self.headerLoader = loader;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self getLikesToPostWithID:self.postID];
    
    self.isRefreshing = NO;
    [self setupRefreshControl];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.title = @"Likes";
    
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Likes list screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [(DEMONavigationController *)self.navigationController setDefaultNavigationBar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.likeListArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"VALikeTableViewCell";
    VALikeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[VALikeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    VAUser *user = [self.likeListArray objectAtIndex:indexPath.row];
    [self showProfileForUserWithAlias:user.nickname];
}

#pragma mark - Methods

- (void)configureCell:(VALikeTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    VAUser *user = [self.likeListArray objectAtIndex:indexPath.row];
    
    if (user.avatarURL) {
        NSURL *imageURL = [NSURL URLWithString:user.avatarURL];
        
        [cell.avatarView sd_setImageWithURL:imageURL
                           placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                      if (image) {
                                          cell.avatarView.image = image;
                                      }
                                      if (error) {
                                          [VANotificationMessage showAvatarErrorMessageOnController:self withDuration:5.f];
                                      }
                                  }];
    } else {
        cell.avatarView.image = [UIImage imageNamed:@"placeholder.png"];
    }
    CGFloat imageDiametr = 36.f;
    cell.avatarView.layer.cornerRadius = imageDiametr / 2;
    cell.avatarView.clipsToBounds = YES;
    cell.nameLabel.text = user.nickname;
    
    if (indexPath.row == [self.likeListArray count] - 1) {
        [cell.separatorView setHidden:YES];
    } else {
        [cell.separatorView setHidden:NO];
    }
}

- (void)getLikesToPostWithID:(NSString *)postID {
    self.tableView.scrollEnabled = NO;
    VAUserApiManager *manager = [VAUserApiManager new];
    [manager getPostLikesForPostWithID:postID withCompletion:^(NSError *error, VAModel *model) {
        self.tableView.scrollEnabled = YES;
        if (!self.isRefreshing) {
            [self.headerLoader stopAnimating];
        } else {
            [self.refresh stopRefreshing];
            self.isRefreshing = NO;
        }
        
        self.tableView.tableHeaderView = nil;

        if (error) {
            [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
        } else if (model) {
            [[self mutableArrayValueForKey:@"likeListArray"] removeAllObjects];
            
            VALikesResponse *response = (VALikesResponse *)model;
            if ([response.status isEqualToString:kSuccessResponseStatus]) {
                [[self mutableArrayValueForKey:@"likeListArray"] addObjectsFromArray:response.usersWhoLikes];
                
                [self.tableView reloadData];
            } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
            }
        }
    }];
}

- (void)showProfileForUserWithAlias:(NSString *)alias {
    VAProfileViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"profileViewController"];
    vc.searchAlias = alias;
    vc.isUsersAccountMode = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)setupRefreshControl {
    VALoaderView *refresh = [VALoaderView initLoader];
    self.refresh = refresh;
    [refresh setFrame:[self frameForCustomRefreshControl]];
    refresh.alpha = 0.f;
    
    [refresh addTarget:self
                action:@selector(refreshData)
      forControlEvents:UIControlEventValueChanged];
    
    [self.tableView addSubview:refresh];
}

- (CGRect)frameForCustomRefreshControl {
    CGFloat customRefreshControlHeight = kLoaderHeight;
    CGFloat customRefreshControlWidth = CGRectGetWidth([self.tableView frame]);
    CGRect customRefreshControlFrame = CGRectMake(0.0f,
                                                  -customRefreshControlHeight,
                                                  customRefreshControlWidth,
                                                  customRefreshControlHeight);
    return customRefreshControlFrame;
}

- (void)refreshData {
    self.isRefreshing = YES;
    [self.refresh startRefreshing];
    [self getLikesToPostWithID:self.postID];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self.refresh containingScrollViewDidEndDragging:scrollView];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.refresh containingScrollViewDidScroll:scrollView];
}

@end
