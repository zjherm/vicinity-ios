//
//  VAPhotoPreviewCollectionViewCell.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 07.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VAPhotoPreviewCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@end
