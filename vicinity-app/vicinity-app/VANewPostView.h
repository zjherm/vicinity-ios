//
//  VANewPostView.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 29.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VANewPostActionsProtocol;

@interface VANewPostView : UIView
@property (weak, nonatomic) id <VANewPostActionsProtocol> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *avatarView;
@property (weak, nonatomic) IBOutlet UIImageView *postImageView;
@property (weak, nonatomic) IBOutlet UIImageView *facebookImageView;
@property (weak, nonatomic) IBOutlet UIImageView *popoverArrow;
@property (weak, nonatomic) IBOutlet UIButton *postButton;
@property (weak, nonatomic) IBOutlet UIButton *locationButton;
@property (weak, nonatomic) IBOutlet UIButton *photoButton;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UISwitch *facebookSwitch;
@property (weak, nonatomic) IBOutlet UIView *boundsView;
@property (weak, nonatomic) IBOutlet UILabel *placeholderLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageContainerHeight;

@property (assign, nonatomic) BOOL isLocationSet;
@property (assign, nonatomic) BOOL isPhotoSet;
@property (assign, nonatomic) BOOL isLandscapePhoto;

@end

@protocol VANewPostActionsProtocol <NSObject>
- (void)locationButtonPushed;
- (void)photoButtonPushed;
- (void)postButtonPushed;
- (void)showLocationActions;
- (void)showPhotoActions;
@end
