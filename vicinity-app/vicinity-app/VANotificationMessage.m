//
//  VANotificationMessage.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 22.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VANotificationMessage.h"
#import "AFNetworkReachabilityManager.h"
#import "VAControlTool.h"

@interface VANotificationMessage ()
@property (strong, nonatomic) UILabel *label;
@property (strong, nonatomic) NSArray* errorsToBeReplacedIfNoInternet;
@property (assign, nonatomic) BOOL isMessageShown;
@end

CGFloat horizontalPadding = 16.f;
CGFloat verticalPadding = 10.f;
CGFloat statusBarHeight = 20.f;

@implementation VANotificationMessage

+ (instancetype)currentMessageController {
    static VANotificationMessage *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [VANotificationMessage new];
        shared.isMessageShown = NO;
        [shared setupErrorsToBeReplacedIfNoInternet];
    });
    return shared;
}

- (void)setupErrorsToBeReplacedIfNoInternet{
    NSMutableArray* array = [NSMutableArray array];
    
    [array addObject:NSLocalizedString(@"update.account.error", nil)];
    [array addObject:NSLocalizedString(@"load.profile.error", nil)];
    [array addObject:NSLocalizedString(@"add.commet.error", nil)];
    [array addObject:NSLocalizedString(@"update.like.error", nil)];
    [array addObject:NSLocalizedString(@"load.avatar.error", nil)];
    [array addObject:NSLocalizedString(@"connection.error", nil)];
    [array addObject:NSLocalizedString(@"login.error", nil)];
    [array addObject:NSLocalizedString(@"load.data.error", nil)];
    [array addObject:NSLocalizedString(@"add.post.error", nil)];
    [array addObject:NSLocalizedString(@"create.user.error", nil)];
    [array addObject:NSLocalizedString(@"add.feedback.error", nil)];
    [array addObject:NSLocalizedString(@"error.delete.post", nil)];
    [array addObject:NSLocalizedString(@"error.update.post", nil)];
    [array addObject:NSLocalizedString(@"error.delete.comment", nil)];
    [array addObject:NSLocalizedString(@"error.update.comment", nil)];
    [array addObject:NSLocalizedString(@"error.no.verification", nil)];
    [array addObject:NSLocalizedString(@"error.incorrect.usernameOrEmail", nil)];
    [array addObject:NSLocalizedString(@"error.username.not.found", nil)];
    [array addObject:NSLocalizedString(@"error.email.not.found", nil)];
    [array addObject:NSLocalizedString(@"error.incorrect.password", nil)];
    [array addObject:NSLocalizedString(@"error.incorrect.data", nil)];
    [array addObject:NSLocalizedString(@"email.in.use", nil)];
    [array addObject:NSLocalizedString(@"username.in.use", nil)];
    [array addObject:NSLocalizedString(@"error.incorrect.old.password", nil)];
    
    self.errorsToBeReplacedIfNoInternet = [array copy];
}

+ (void)showShareSuccessMessageOnController:(UIViewController *)controller {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"Invite sent!", nil) color:[[VADesignTool defaultDesign] successMessageGreen] onController:controller andHideAfterDelay:5.0];
}

+ (void)showSharePostSuccessMessageOnController:(UIViewController *)controller {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"Post shared successfully.", nil) color:[[VADesignTool defaultDesign] successMessageGreen] onController:controller andHideAfterDelay:5.0];
}

+ (void)showSuccessMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"update.account.success", nil) color:[[VADesignTool defaultDesign] successMessageGreen] onController:controller andHideAfterDelay:duration];
}

+ (void)showUpdateErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"update.account.error", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showAliasExistsErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"alias.already.exists.error", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showLoadUserErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"load.profile.error", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showUserDoesntExistMessageOnController:(UIViewController *)controller forUserAlias:(NSString *)alias withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:[NSString stringWithFormat:NSLocalizedString(@"user.doesnt.exist", nil), alias] onController:controller andHideAfterDelay:duration];
}

+ (void)showAddCommentErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"add.commet.error", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showLikeErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"update.like.error", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showAvatarErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"load.avatar.error", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showFillInNameMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"name.validation", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showFillInCityMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"city.validation", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showFillInCountryMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"country.validation", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showFillInStateMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"state.validation", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showFillInAliasMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"nickname.validation", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showUsernameCannotEndWithPeriodMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"nickname.validation.endWithPeriod", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showFillInEmailMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"email.validation", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showConnectionErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"connection.error", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showCreateAccountErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"create.user.error", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showLoginCancelledMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"login.error", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showLoadErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"load.data.error", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showAddingPostErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"add.post.error", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showTurnOnLocationMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"location.permissions", nil) onController:controller andHideAfterDelay:duration];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapNotification:)];
    [[self currentMessageController] addGestureRecognizer:tapGesture];
    [[self currentMessageController] setIsLocationMessage:YES];
}

+ (void)didTapNotification:(id)sender {
    if ([VANotificationMessage currentMessageController].isLocationMessage) {
        [[VAControlTool defaultControl] openAppSettings];
    }
}

+ (void)showTurnOnNotificationsMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"notifications.permissions", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showSuccessFeedbackMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"add.feedback.success", nil) color:[[VADesignTool defaultDesign] successMessageGreen] onController:controller andHideAfterDelay:duration];
}

+ (void)showErrorFeedbackMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"add.feedback.error", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showCantUpdateLocationMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:@"Can't Detect Your Location" onController:controller andHideAfterDelay:duration];
}

+ (void)showDeletePostErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"error.delete.post", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showEditPostErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"error.update.post", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showDeleteCommentErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"error.delete.comment", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showEditCommentErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"error.update.comment", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showEnterUsernameMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"error.enter.username", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showEnterPasswordMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"error.enter.password", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showNoVerificationMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"error.no.verification", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showCheckNewPasswordMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"check.email.password", nil) color:[[VADesignTool defaultDesign] successMessageGreen] onController:controller andHideAfterDelay:duration];
}

+ (void)showIncorrectUsernameOrEmailMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"error.incorrect.usernameOrEmail", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showUsernameNotFoundMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"error.username.not.found", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showEmailNotFoundMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"error.email.not.found", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showIncorrectPasswordMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"error.incorrect.password", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showIncorrectDataMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"error.incorrect.data", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showEmailInUseMessageOnController:(UIViewController *)controller forEmail:(NSString *)email withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:[NSString stringWithFormat:NSLocalizedString(@"email.in.use", nil), email] onController:controller andHideAfterDelay:duration];
}

+ (void)showUsernameInUseMessageOnController:(UIViewController *)controller forUsername:(NSString *)username withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:[NSString stringWithFormat:NSLocalizedString(@"username.in.use", nil), username] onController:controller andHideAfterDelay:duration];
}

+ (void)showIncorrectOldPasswordMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"error.incorrect.old.password", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showSuccessUpdatePasswordMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"update.password.success", nil) color:[[VADesignTool defaultDesign] successMessageGreen] onController:controller andHideAfterDelay:duration];
}

+ (void)showSetAvatarMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"error.set.avatar", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showInvalidPasswordMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"error.password.validation", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showFacebookInUseMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"error.facebook.isUsed", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showNetworkErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:NSLocalizedString(@"network.error", nil) onController:controller andHideAfterDelay:duration];
}

+ (void)showBlockUserSuccessMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration nickname:(NSString *)nickname {
    [[self currentMessageController] showTopNotificationMessageWithTitle:[NSString stringWithFormat:@"%@ succesfully blocked", nickname] color:[[VADesignTool defaultDesign] successMessageGreen] onController:controller andHideAfterDelay:duration];
}

+ (void)showUnblockUserSuccessMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration nickname:(NSString *)nickname {
    [[self currentMessageController] showTopNotificationMessageWithTitle:[NSString stringWithFormat:@"%@ succesfully unblocked", nickname] color:[[VADesignTool defaultDesign] successMessageGreen] onController:controller andHideAfterDelay:duration];
}

+ (void)showReportSuccessMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration {
    [[self currentMessageController] showTopNotificationMessageWithTitle:@"Report submitted succesfully" color:[[VADesignTool defaultDesign] successMessageGreen] onController:controller andHideAfterDelay:duration];
}

+ (void)showUserNotFoundMessageOnController:(UIViewController *)controller {
    [[self currentMessageController] showTopNotificationMessageWithTitle:@"User not found" color:[UIColor colorWithRed:155.0f/255.0f green:155.0f/255.0f blue:155.0f/255.0f alpha:1] onController:controller];
}

+ (void)showUserDeletedMessageOnController:(UIViewController *)controller {
    [[self currentMessageController] showTopNotificationMessageWithTitle:@"User has been deleted" color:[UIColor colorWithRed:155.0f/255.0f green:155.0f/255.0f blue:155.0f/255.0f alpha:1] onController:controller];
}

+ (void)showUserBlockedMessageOnController:(UIViewController *)controller {
    [[self currentMessageController] showTopNotificationMessageWithTitle:@"User has been blocked" color:[UIColor colorWithRed:155.0f/255.0f green:155.0f/255.0f blue:155.0f/255.0f alpha:1] onController:controller];
}

+ (void)showYouBlockedUserMessageOnController:(UIViewController *)controller {
    [[self currentMessageController] showTopNotificationMessageWithTitle:@"You have blocked this user" color:[[VADesignTool defaultDesign] errorMessageRed] onController:controller];
}

+ (void)showConnectUserSuccessMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration nickname:(NSString *)nickname {
    [[self currentMessageController] showTopNotificationMessageWithTitle:[NSString stringWithFormat:@"You're now connected to %@", nickname] color:[[VADesignTool defaultDesign] successMessageGreen] onController:controller andHideAfterDelay:duration];
}

- (void)showCenterNotificationMessageWithTitle:(NSString *)title onController:(UIViewController *)controller {
    UIFont *font = [[VADesignTool defaultDesign] notificationMessageFontOfSize:17.f];
    CGFloat height = [self heightForLabelWithWidth:CGRectGetWidth([UIScreen mainScreen].bounds) - horizontalPadding * 2 font:font andTitile:title];
    [self showNotificationMessageWithTitle:title backgroundColor:[[VADesignTool defaultDesign] notificationMessageBlue] fontColor:[UIColor whiteColor] font:font andStartYPoint:CGRectGetMidY(controller.view.bounds) - height / 2 andHeight:height onController:controller];
}

- (void)showTopNotificationMessageWithTitle:(NSString *)title onController:(UIViewController *)controller {
    [self showTopNotificationMessageWithTitle:title color:[[VADesignTool defaultDesign] errorMessageRed] onController:controller];
}

- (void)showTopNotificationMessageWithTitle:(NSString *)title color:(UIColor*)color onController:(UIViewController *)controller {
    UIFont *font = [[VADesignTool defaultDesign] notificationMessageFontOfSize:14.f];
    CGFloat height = [self heightForLabelWithWidth:CGRectGetWidth([UIScreen mainScreen].bounds) - horizontalPadding * 2 font:font andTitile:title];
    CGFloat startY = 0.f;

    NSString* newTitle = [self checkInternetConnectionAndChangeTitleOfErrorIfNoInternet:(NSString*)title];
    [self showNotificationMessageWithTitle:newTitle backgroundColor:color fontColor:[UIColor whiteColor] font:font andStartYPoint:startY andHeight:height onController:controller];
}

- (void)showNotificationMessageWithTitle:(NSString *)title backgroundColor:(UIColor *)color fontColor:(UIColor *)fontColor font:(UIFont *)font andStartYPoint:(CGFloat)y andHeight:(CGFloat)height onController:(UIViewController *)controller {
    self.backgroundColor = color;
    UILabel *label = [UILabel new];
    if ([self shouldFillStatusBarOfController:controller]) {
        [self setFrame:CGRectMake(0, y, CGRectGetWidth([UIScreen mainScreen].bounds), height + statusBarHeight + verticalPadding * 2)];     //frame for view
        [label setFrame:CGRectMake(horizontalPadding, statusBarHeight + verticalPadding, CGRectGetWidth(self.bounds) - horizontalPadding * 2, height)];      //frame for label
    } else {
        if (controller.navigationController.navigationBar.translucent) {
            y += 64.f;     //navBar height
        }
        [self setFrame:CGRectMake(0, y, CGRectGetWidth([UIScreen mainScreen].bounds), height + verticalPadding * 2)];       //frame for view
        [label setFrame:CGRectMake(horizontalPadding, verticalPadding, CGRectGetWidth(self.bounds) - horizontalPadding * 2, height)];      //frame for label
    }
    self.label = label;
    label.text = title;
    [label setFont:font];
    [label setTextColor:fontColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    if ([self.subviews count] > 0) {
        for (id subview in self.subviews) {
            [subview removeFromSuperview];
        }
    }
    [self addSubview:label];
    [self setFrame:CGRectMake(CGRectGetMinX(self.frame), y - CGRectGetHeight(self.frame), CGRectGetWidth(self.frame), CGRectGetHeight(self.frame))];
    [controller.view addSubview:self];
    [UIView animateWithDuration:0.3f animations:^{
        [self setFrame:CGRectMake(CGRectGetMinX(self.frame), y, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame))];
    }];
    self.isMessageShown = YES;
}

- (void)showTopNotificationMessageWithTitle:(NSString *)title onController:(UIViewController *)controller andHideAfterDelay:(CGFloat)delay {
    if ([self notificationMessageIsShown]) {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideCurrentMessage) object:nil];
        [self hideCurrentMessage];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self showTopNotificationMessageWithTitle:title onController:controller];
            [self hideCurrentMessageAfterDelay:delay];
        });
    } else {
        [self showTopNotificationMessageWithTitle:title onController:controller];
        [self hideCurrentMessageAfterDelay:delay];
    }
}

- (void)showTopNotificationMessageWithTitle:(NSString *)title color:(UIColor *)color onController:(UIViewController *)controller andHideAfterDelay:(CGFloat)delay {
    if ([self notificationMessageIsShown]) {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideCurrentMessage) object:nil];
        [self hideCurrentMessage];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self showTopNotificationMessageWithTitle:title color:color onController:controller];
            [self hideCurrentMessageAfterDelay:delay];
        });
    } else {
        [self showTopNotificationMessageWithTitle:title color:color onController:controller];
        [self hideCurrentMessageAfterDelay:delay];
    }
}

- (void)hideCurrentMessageAfterDelay:(CGFloat)delay {
    if (delay > 0.f) {
        [self performSelector:@selector(hideCurrentMessage) withObject:nil afterDelay:delay];
    }
}

- (void)hideCurrentMessage {
    [[VANotificationMessage currentMessageController] setIsLocationMessage:NO];
    [UIView animateWithDuration:0.3f animations:^{
        [self setFrame:CGRectMake(CGRectGetMinX(self.frame), -CGRectGetHeight(self.frame), CGRectGetWidth(self.frame), CGRectGetHeight(self.frame))];
    } completion:^(BOOL finished) {
        for (id subview in self.subviews) {
            [subview removeFromSuperview];
        }
        [self removeFromSuperview];
        self.isMessageShown = NO;
    }];
}

- (BOOL)notificationMessageIsShown {
    return self.isMessageShown;
}

- (CGFloat)heightForLabelWithWidth:(CGFloat)width font:(UIFont *)font andTitile:(NSString *)title {
    UILabel *sizeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, CGFLOAT_MAX)];
    [sizeLabel setText:title];
    [sizeLabel setFont:font];
    sizeLabel.textAlignment = NSTextAlignmentCenter;
    sizeLabel.numberOfLines = 0;
    sizeLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize titleSize = [sizeLabel sizeThatFits:CGSizeMake(width, CGFLOAT_MAX)];
    
    return titleSize.height;
}

- (BOOL)shouldFillStatusBarOfController:(UIViewController *)controller {
    if (!controller.navigationController || controller.navigationController.navigationBarHidden) {
        return YES;
    } else {
        return NO;
    }
}

-(NSString*)checkInternetConnectionAndChangeTitleOfErrorIfNoInternet:(NSString*)title{
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) {
        if ([self.errorsToBeReplacedIfNoInternet containsObject:title]) {
            return NSLocalizedString(@"error.noInternetConnection", nil);
        } else {
            return title;
        }
    } else {
        return title;
    }
}

+ (void)showAlertWithText:(NSString *)text {
    [[[UIAlertView alloc] initWithTitle:@"Log Alert"
                                message:text
                               delegate:self
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
    
}

@end