//
//  VANotificationTool.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 22.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VANotificationTool.h"

@implementation VANotificationTool
NSString *const kDidChangeLocationStatus = @"kDidChangeLocationStatus";
NSString *const kDidChangeUserAvatar = @"kDidChangeUserAvatar";
NSString *const kDidUpdateLocaion = @"kDidUpdateLocaion";
NSString *const kUserDidRegisterForPushNotifications = @"kUserDidRegisterForPushNotifications";
NSString *const kUserDidCreateAlias = @"kUserDidCreateAlias";
NSString *const kShowBadgeForMenuIcon = @"kShowBadgeForMenuIcon";
NSString *const kHideBadgeForMenuIcon = @"kHideBadgeForMenuIcon";
NSString *const kShowInviteScreen = @"kShowInviteScreen";
NSString *const kDidDisableLocationServices = @"kDidDisableLocationServices";

@end
