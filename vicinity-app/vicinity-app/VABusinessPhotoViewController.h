//
//  VABusinessPhotoViewController.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 07.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VABusinessPhotoViewController : UIViewController
@property (strong, nonatomic) NSArray *photoURLsArray;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
@end
