//
//  VAUnderlinableView.m
//  vicinity-app
//
//  Created by vadzim vasileuski on 3/28/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import "VAUnderlinableView.h"

@implementation VAUnderlinableView {
    CALayer *_bottomBorder;
    CGFloat _width;
}

- (void)drawUnderlineWithColor:(UIColor *)color width:(CGFloat)width
{
    self.clipsToBounds = NO;
    if (_bottomBorder) {
        [self removeUnderline];
    }
    
    _width = width;
    _bottomBorder = [CALayer layer];
    _bottomBorder.borderColor = color.CGColor;
    _bottomBorder.borderWidth = width;
    _bottomBorder.frame = CGRectMake(0, CGRectGetHeight(self.frame) - width, CGRectGetWidth(self.frame), width);
    
    [self.layer addSublayer:_bottomBorder];
}

- (void)removeUnderline
{
    if (_bottomBorder) {
        [_bottomBorder removeFromSuperlayer];
        _bottomBorder = nil;
    }
}

- (BOOL)isUnderlinded
{
    return (_bottomBorder != nil);
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    _bottomBorder.frame = CGRectMake(0, CGRectGetHeight(self.frame) - _width, CGRectGetWidth(self.frame), _width);
}

@end
