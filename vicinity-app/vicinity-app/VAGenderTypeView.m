//
//  VAGenderTypeView.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/8/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAGenderTypeView.h"
#import "VADesignTool.h"

@interface VAGenderTypeView ()

@property (nonatomic, weak) IBOutlet UIView *femaleView;
@property (nonatomic, weak) IBOutlet UIView *maleView;
@property (nonatomic, weak) IBOutlet UIImageView *femaleStateImageView;
@property (nonatomic, weak) IBOutlet UIImageView *maleStateImageView;
@end

@implementation VAGenderTypeView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.femaleStateImageView.layer.cornerRadius = 5.f;
    self.femaleStateImageView.clipsToBounds = YES;
    
    self.maleStateImageView.layer.cornerRadius = 5.f;
    self.maleStateImageView.clipsToBounds = YES;
    
    UITapGestureRecognizer *femaleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectFemaleGender:)];
    [self.femaleView addGestureRecognizer:femaleTap];
    UITapGestureRecognizer *maleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectMaleGender:)];
    [self.maleView addGestureRecognizer:maleTap];
    
    self.genderType = VAGenderTypeFemale;
}

#pragma mark - Properties

- (void)setGenderType:(VAGenderType)genderType {
    
    _genderType = genderType;
    
    if (genderType == VAGenderTypeFemale) {
        self.femaleStateImageView.image = nil;
        self.femaleStateImageView.backgroundColor = [[VADesignTool defaultDesign] vicinityBlue];
        self.maleStateImageView.backgroundColor = [UIColor clearColor];
        self.maleStateImageView.image = [UIImage imageNamed:@"profile-gender-inactive"];
    } else if (genderType == VAGenderTypeMale) {
        self.maleStateImageView.image = nil;
        self.maleStateImageView.backgroundColor = [[VADesignTool defaultDesign] vicinityBlue];
        self.femaleStateImageView.backgroundColor = [UIColor clearColor];
        self.femaleStateImageView.image = [UIImage imageNamed:@"profile-gender-inactive"];
    } else {
        self.maleStateImageView.backgroundColor = [UIColor clearColor];
        self.maleStateImageView.image = [UIImage imageNamed:@"profile-gender-inactive"];
        self.femaleStateImageView.backgroundColor = [UIColor clearColor];
        self.femaleStateImageView.image = [UIImage imageNamed:@"profile-gender-inactive"];
    }
}

#pragma mark - Taps

- (void)didSelectFemaleGender:(id)sender {
    if (self.genderType != VAGenderTypeFemale) {
        [self.delegate userDidChangeGenderType:VAGenderTypeFemale];
    }
    self.genderType = VAGenderTypeFemale;
}

- (void)didSelectMaleGender:(id)sender {
    if (self.genderType != VAGenderTypeMale) {
        [self.delegate userDidChangeGenderType:VAGenderTypeMale];
    }
    self.genderType = VAGenderTypeMale;

}

@end
