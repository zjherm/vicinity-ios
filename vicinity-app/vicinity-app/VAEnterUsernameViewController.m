//
//  VAEnterUsernameViewController.m
//  vicinity-app
//
//  Created by vadzim vasileuski on 3/30/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import "VAEnterUsernameViewController.h"

#import "VAUnderlinableView.h"
#import "DEMONavigationController.h"
#import "VAEventsViewController.h"
#import "VAMenuTableViewController.h"
#import "VAWelcomeViewController.h"

#import "VAPhotoTool.h"
#import "VANotificationMessage.h"
#import "VAControlTool.h"

#import "VAModel.h"
#import "VAUser.h"
#import "VALoginResponse.h"
#import "VAUserDefaultsHelper.h"
#import "VAUserApiManager.h"
#import "VANotificationTool.h"

#import <SDWebImage/UIImageView+WebCache.h>


static NSString *const kAliasAlreadyExist = @"NICKNAME_ALREADY_EXISTS";

@interface VAEnterUsernameViewController () <UITextFieldDelegate, VAPhotoToolDelegate>

//avatar group
@property (weak, nonatomic) IBOutlet UIView *imageBoundaryView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *editLabel;

//views to make translucent effect
@property (weak, nonatomic) IBOutlet VAUnderlinableView *usernameFieldView;

//
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

//avatar image
@property (strong, nonatomic) UIImage *avatarImage;
@property (nonatomic) BOOL imageChanged;

//actions
- (IBAction)continueButtonTapped:(id)sender;

//for red underlining
@property (nonatomic) BOOL isUsernameDidEndEditing;


@end

@implementation VAEnterUsernameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.imageChanged = false;
    [self setUpViewsAlpha];
    [self setUpFontSizes];
    [self setUpGestureRecognisers];
    [self setupNavigationBar];
    [self setUpPlaceholders];
    [self loadAvatarForUser];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setUpKeyboardNotifications];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - setup methods
- (void)setUpViewsAlpha {
    //Need to set dynamically because Interface Builder sets alpha channel also to subviews
    self.usernameFieldView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.27];
}

- (void)setUpFontSizes {
    if (self.view.frame.size.height < 660) {
        //iphone 5 and smaller
        CGFloat fontSizeDiff = 2.0f;
        if (self.view.frame.size.height < 560) {
            //iphone 4 and smaller
            fontSizeDiff = 4.0f;
        }
        self.continueButton.titleLabel.font = [self.continueButton.titleLabel.font fontWithSize:(self.continueButton.titleLabel.font.pointSize - fontSizeDiff)];
        self.usernameField.font = [self.usernameField.font fontWithSize:(self.usernameField.font.pointSize - fontSizeDiff)];
        self.editLabel.font = [self.editLabel.font fontWithSize:(self.editLabel.font.pointSize - fontSizeDiff)];
    }
}

- (void)setUpKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)setUpGestureRecognisers {
    //tap on imageView to choose avatar image
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeAvatar)];
    [self.avatarImageView addGestureRecognizer:tapRecognizer];
    
    //tap on background to hide keyboard
    UITapGestureRecognizer *tapBackground = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.scrollView addGestureRecognizer:tapBackground];
}

- (void)setupNavigationBar {
    [(DEMONavigationController *)self.navigationController setDefaultNavigationBar];
    [(DEMONavigationController *)self.navigationController setTitle:@"Enter Username"];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:67.0/255.0 green:207.0/255.0 blue:206.0/255.0 alpha:1.0];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    //[self.navigationController.navigationBar setTitleVerticalPositionAdjustment:4.0f forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"Avenir-Book" size:21.0f]}];
    
    UIImage *backImage = [UIImage imageNamed:@"BackButton"];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:backImage style:UIBarButtonItemStylePlain target:self action:@selector(goBackToLoginScreen:)];
    self.navigationItem.leftBarButtonItem = leftItem;
}

- (void)setUpPlaceholders {
    [self setUpPlaceholderForTextField:self.usernameField];
}

- (void)setUpPlaceholderForTextField:(UITextField *)textField{
    NSAttributedString *placeholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{ NSForegroundColorAttributeName : [[UIColor whiteColor] colorWithAlphaComponent:1.0f]}];
    textField.attributedPlaceholder = placeholder;
}


#pragma mark - actions
- (void)changeAvatar {
    [self hideKeyboard];
    [VAPhotoTool defaultPhoto].delegate = self;
    [[VAPhotoTool defaultPhoto] showPhotoActionViewOnController:self];
}

- (void)goBackToLoginScreen:(id)sender {
    [self hideKeyboard];
    [self.delegate userDidGoBackToLoginScreen];
    [[VAControlTool defaultControl] logout];
    //[self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)continueButtonTapped:(id)sender {
    if ([self allFieldsAreValid]) {
        [self showActivityIndicator];
        NSData *imageData = nil;
        if (self.imageChanged) {
            imageData = UIImageJPEGRepresentation(self.avatarImage, 0.1);
        }
        //UIImagePNGRepresentation(self.avatarImage);
        
        VAUser *user = [VAUserDefaultsHelper me];
        NSDictionary *params = @{
                                 @"fullname": user.fullname != nil? user.fullname :@"",
                                 @"nickname": [self.usernameField.text lowercaseString],
                                 @"email": user.email != nil? user.email :@"",
                                 @"city": user.city  != nil? user.city :@"",
                                 @"state": user.state != nil? user.state :@"",
                                 @"country": user.country != nil? user.country :@"",
                                 @"gender": user.gender != nil? user.gender :@"",
                                 @"bio": user.bio != nil? user.bio :@""
                                 };
        [[VAUserApiManager alloc] postUpdateCurrentUserWithParams:params imageData:imageData withCompletion:^(NSError *error, VAModel *model) {
            if (model) {
                VALoginResponse *response = (VALoginResponse *)model;
                VAUser *user = response.loggedUser;
                [VAUserDefaultsHelper saveModel:user forKey:VASaveKeys_CurrentUser];
                [self goToEventsViewController];
            } else {
                if ([error.localizedDescription isKindOfClass:[NSString class]] && [error.localizedDescription isEqualToString:kAliasAlreadyExist]) {
                    [VANotificationMessage showAliasExistsErrorMessageOnController:self withDuration:5.f];
                } else {
                    [VANotificationMessage showUpdateErrorMessageOnController:self withDuration:5.f];
                }
            }
            [self hideActivityIndicator];
        }];
    }
}

- (void)goToEventsViewController {
    [self hideKeyboard];
    
    [[VALocationTool sharedInstance] requestInUseAuthorization];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    VAEventsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"VAEventsViewController"];
    DEMONavigationController *nav = (DEMONavigationController*)[storyboard instantiateViewControllerWithIdentifier:@"MyVicinityNavigationController"];
    [nav setViewControllers:@[vc] animated:NO];
    VAMenuTableViewController *leftMenuController = [storyboard instantiateViewControllerWithIdentifier:@"VAMenuTableViewController"];;
    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:nav
                                                    leftMenuViewController:leftMenuController
                                                    rightMenuViewController:nil];
    container.leftMenuWidth = 125.f;
    container.menuAnimationMaxDuration = container.menuAnimationDefaultDuration;
    leftMenuController.containerViewController = container;
    nav.containerViewController = container;
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    window.rootViewController = container;
    [window makeKeyAndVisible];
    
}



#pragma mark - keyboard management
- (void)hideKeyboard {
    [self.usernameField resignFirstResponder];
}

- (void)willKeyboardShow:(NSNotification*)notification {
    NSDictionary* info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    CGPoint originInSuperview = [self.scrollView convertPoint:CGPointZero fromView:self.continueButton];
    CGFloat yOffset = (originInSuperview.y + self.continueButton.frame.size.height + 15) - (self.scrollView.frame.size.height - kbSize.height);
    if (yOffset > 0) {
        self.scrollView.contentOffset = CGPointMake(0, yOffset);
    }
}

- (void)willKeyboardHide:(NSNotification*)notification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    self.scrollView.contentOffset = CGPointZero;
}


#pragma mark - text field validation
- (void)drawUnderlineForView:(VAUnderlinableView *)view {
    [view drawUnderlineWithColor:[UIColor colorWithRed:208.0f/255 green:2.0f/255 blue:27.0f/255 alpha:1.0f] width:1.5];
}

- (BOOL)allFieldsAreValid {
    return (self.usernameField.text.length > 2) &&  ![self.usernameField.text hasSuffix:@"."] && (self.avatarImage != nil);
}

#pragma mark - disable/enable button
- (void)disableContinueButton {
    self.continueButton.enabled = NO;
    self.continueButton.backgroundColor = [self.continueButton.backgroundColor colorWithAlphaComponent:0.5];
}

- (void)enableContinueButton {
    self.continueButton.enabled = YES;
    self.continueButton.backgroundColor = [self.continueButton.backgroundColor colorWithAlphaComponent:1.0];
}

- (void)validateContinueButtonEnable {
    
    if (self.usernameField.text.length < 3  || [self.usernameField.text hasSuffix:@"."]) {
        if (self.isUsernameDidEndEditing) {
            [self drawUnderlineForView:self.usernameFieldView];
        }
    }  else {
        [self.usernameFieldView removeUnderline];
    }
    
    if ([self allFieldsAreValid]) {
        [self enableContinueButton];
    } else {
        [self disableContinueButton];
    }
}

#pragma mark - VAPhotoToolDelegate
- (void)photoHasBeenEdited:(UIImage *)photo landscape:(BOOL)isLandscape {

    [self setAvatarImage:photo];
    self.imageChanged = true;
    [self validateContinueButtonEnable];
}

- (void)setAvatarImage:(UIImage *)avatarImage
{
    _avatarImage = avatarImage;
    self.avatarImageView.image = avatarImage;
    self.editLabel.hidden = NO;
    self.imageBoundaryView.layer.cornerRadius = self.avatarImageView.frame.size.width / 2;
    self.imageBoundaryView.clipsToBounds = YES;
    self.imageBoundaryView.layer.borderWidth = 2.0f;
    self.imageBoundaryView.layer.borderColor = [UIColor whiteColor].CGColor;
}

#pragma mark - UITextFieldDelegate methods
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (!self.avatarImage) {
        self.avatarImageView.image = [UIImage imageNamed:@"photo-placeholder-red"];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(UITextFieldTextDidChange:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:textField];
    
    
    
    if (self.usernameFieldView.isUnderlinded) {
        if (self.usernameField.text.length < 3) {
            [VANotificationMessage showFillInAliasMessageOnController:self withDuration:5.f];
        }
        if ([self.usernameField.text hasSuffix:@"."]) {
            [VANotificationMessage showUsernameCannotEndWithPeriodMessageOnController:self withDuration:5.f];
        }
    }
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UITextFieldTextDidChangeNotification
                                                  object:textField];
    if (textField.text.length > 0) {
        self.isUsernameDidEndEditing = YES;
    }
    
    [self validateContinueButtonEnable];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    BOOL change = [[VAControlTool defaultControl] setAliasMaskForTextField:textField InRange:range replacementString:string];
    return change;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self hideKeyboard];
    if ([self allFieldsAreValid]) {
        [self continueButtonTapped:self.continueButton];
    }
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    [self disableContinueButton];
    return YES;
}

- (void) UITextFieldTextDidChange:(NSNotification*)notification
{
    [self validateContinueButtonEnable];
}


#pragma mark - load avatar
- (void)loadAvatarForUser {
    VAUser *user = [VAUserDefaultsHelper me];
    if (user.avatarURL) {
        NSURL *imageURL = [NSURL URLWithString:user.avatarURL];
        
       [self.avatarImageView sd_setImageWithURL:imageURL
                                placeholderImage:[UIImage imageNamed:@"photo-placeholder.png"]
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                           if (image) {
                                               [self setAvatarImage:image];
                                           }
                                           if (error) {
                                               [VANotificationMessage showAvatarErrorMessageOnController:self withDuration:5.f];
                                           }
                                       }];
        
    } else {
        self.avatarImageView.image = [UIImage imageNamed:@"photo-placeholder-red.png"];
    }
}

#pragma mark - activity indicator

- (void)showActivityIndicator {
    self.view.userInteractionEnabled = NO;
    [self.continueButton setTitle:@"" forState:UIControlStateNormal];
    [self.activityIndicator startAnimating];
}

- (void)hideActivityIndicator {
    [self.activityIndicator stopAnimating];
    [self.continueButton setTitle:@"CONTINUE" forState:UIControlStateNormal];
    self.view.userInteractionEnabled = YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
@end
