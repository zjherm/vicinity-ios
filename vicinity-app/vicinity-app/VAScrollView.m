//
//  VAScrollView.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/11/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAScrollView.h"

@interface VAScrollView ()

@end

@implementation VAScrollView

- (void)awakeFromNib {
    self.avoidScrolling = NO;
    self.scrollingOffset = 0.f;
}

- (void)scrollRectToVisible:(CGRect)rect animated:(BOOL)animated {
    if (!self.avoidScrolling) {
        CGRect newRect = CGRectMake(CGRectGetMinX(rect), CGRectGetMinY(rect) + 15 + self.scrollingOffset, CGRectGetWidth(rect), CGRectGetHeight(rect));
        [super scrollRectToVisible:newRect animated:animated];
    }
}


@end
