//
//  VAAllPhotosViewController.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 07.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAAllPhotosViewController.h"
#import "VAPhotoPreviewCollectionViewCell.h"
#import "VABusinessPhotoViewController.h"
#import "DEMONavigationController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "VANotificationMessage.h"
#import "VAControlTool.h"
#import <Google/Analytics.h>

#import "VAGooglePlacesApiManager.h"

@interface VAAllPhotosViewController ()

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation VAAllPhotosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [(DEMONavigationController *)self.navigationController setDefaultNavigationBar];
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackButton"] style:UIBarButtonItemStylePlain target:self.navigationController action:@selector(popViewControllerAnimated:)];
    self.navigationItem.leftBarButtonItem = leftItem;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.title = @"Photos";
    
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Business photo preview screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.photoURLsArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Photo";
    VAPhotoPreviewCollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];

    [cell.indicator startAnimating];
    
    cell.imageView.alpha = 0.f;
    __weak VAPhotoPreviewCollectionViewCell *weakCell = cell;
    [weakCell.imageView sd_setImageWithURL:[[VAGooglePlacesApiManager sharedManager] downloadUrlForPhoto:self.photoURLsArray[indexPath.row]]
                          placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                   options:SDWebImageRetryFailed | SDWebImageContinueInBackground | SDWebImageHandleCookies | SDWebImageHighPriority | SDWebImageTransformAnimatedImage | SDWebImageAvoidAutoSetImage
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                     if (image) {
                                         [cell.indicator stopAnimating];
                                         
                                         weakCell.imageView.image = image;
                                         [UIView animateWithDuration:0.3f animations:^{
                                             weakCell.imageView.alpha = 1.0;
                                         }];
                                     } else if (error) {
                                         [VANotificationMessage showLoadErrorMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
                                     }
                                 }];
    [cell layoutIfNeeded];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    VABusinessPhotoViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"VABusinessPhotoViewController"];
    vc.selectedIndexPath = indexPath;
    vc.photoURLsArray = self.photoURLsArray;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat inset = 5.f;
    NSInteger cellsInARow = 3;
    CGFloat cellSide = (CGRectGetWidth(self.view.bounds) - inset - (cellsInARow - 1) * inset - inset) / 3;      //left inset, insets betwwen cells and right inset
    CGSize size = CGSizeMake(cellSide, cellSide);
    return size;
}


@end
