//
//  VAGoogleLocation.h
//  vicinity-app
//
//  Created by Panda Systems on 10/28/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAGoogleApiModel.h"

@interface VAGoogleLocation : VAGoogleApiModel

@property (strong, nonatomic) NSNumber *latitude;
@property (strong, nonatomic) NSNumber *longitude;

@end
