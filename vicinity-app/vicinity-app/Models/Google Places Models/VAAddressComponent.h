//
//  VAAddressComponent.h
//  vicinity-app
//
//  Created by Panda Systems on 10/27/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAGoogleApiModel.h"

typedef NS_ENUM(NSUInteger, VAAddressComponentType) {
    VAAddressComponentTypeStreetNumber,
    VAAddressComponentTypeStreet,
    VAAddressComponentTypeCity,
    VAAddressComponentTypeState,
    VAAddressComponentTypeCountry,
    VAAddressComponentTypePostalCode,
    VAAddressComponentTypeUnknown
};

@interface VAAddressComponent : VAGoogleApiModel

@property (nonatomic, strong) NSString *longName;
@property (nonatomic, strong) NSString *shortName;
@property (nonatomic, strong) NSArray *types;

@property (nonatomic, readonly) VAAddressComponentType type;

@end
