//
//  VAPlaceDetails.m
//  vicinity-app
//
//  Created by Panda Systems on 10/27/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAPlaceDetails.h"

#import "VAPlacePhoto.h"
#import "VADay.h"
#import "VAGoogleLocation.h"
#import "VAAddressComponent.h"

@implementation VAPlaceDetails

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"placeId": @"place_id",
             @"addressComponents": @"address_components",
             @"formattedAddress": @"formatted_address",
             @"formattedPhoneNumber": @"formatted_phone_number",
             @"location": @"geometry.location",
             @"internationalPhoneNumber": @"international_phone_number",
             @"name": @"name",
             @"openingHours": @"opening_hours.periods",
             @"photos": @"photos",
             @"priceLevel": @"price_level",
             @"rating": @"rating",
             @"userRatingsTotal": @"user_ratings_total",
             @"website": @"website",
             @"vicinity": @"vicinity",
             @"types": @"types"
             };
}

+ (NSValueTransformer *)photosJSONTransformer {
    return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:[VAPlacePhoto class]];
}

+ (NSValueTransformer *)openingHoursJSONTransformer {
    return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:[VADay class]];
}

+ (NSValueTransformer *)addressComponentsJSONTransformer {
    return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:[VAAddressComponent class]];
}

+ (NSValueTransformer *)locationJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:[VAGoogleLocation class]];
}

- (NSString *)shortAddressStreet {
    NSMutableString *addressString = [[NSMutableString alloc] initWithString:@""];
    if (self.streetNumber) {
        [addressString appendString:self.streetNumber];
    }
    if (self.street) {
        if ([addressString length] > 0) {
            [addressString appendString:@" "];
        }
        [addressString appendString:self.street];
    }
    if (self.city) {
        if ([addressString length] > 0) {
            [addressString appendString:@"\n"];
        }
        [addressString appendString:self.city];
    }
    if (self.state) {
        if ([addressString length] > 0) {
            [addressString appendString:@", "];
        }
        [addressString appendString:self.state];
    }
    if (self.postalCode) {
        if ([addressString length] > 0) {
            [addressString appendString:@", "];
        }
        [addressString appendString:self.postalCode];
    }
    
    return [addressString length] > 0 ? addressString : self.formattedAddress;
}

- (NSString *)streetNumber {
    VAAddressComponent *streetNumber = [self addressComponentWithType:VAAddressComponentTypeStreetNumber];
    return streetNumber.longName;
}

- (NSString *)street {
    VAAddressComponent *street = [self addressComponentWithType:VAAddressComponentTypeStreet];
    return street.shortName;
}

- (NSString *)city {
    VAAddressComponent *city = [self addressComponentWithType:VAAddressComponentTypeCity];
    return city.longName;
}

- (NSString *)state {
    VAAddressComponent *state = [self addressComponentWithType:VAAddressComponentTypeState];
    return state.shortName;
}

- (NSString *)country {
    VAAddressComponent *country = [self addressComponentWithType:VAAddressComponentTypeCountry];
    return country.shortName;
}

- (NSString *)postalCode {
    VAAddressComponent *postalCode = [self addressComponentWithType:VAAddressComponentTypePostalCode];
    return postalCode.longName;
}

- (VAAddressComponent *)addressComponentWithType:(VAAddressComponentType)type {
    for (VAAddressComponent *component in self.addressComponents) {
        if (component.type == type) {
            return component;
        }
    }
    return nil;
}

@end
