//
//  VAGoogleApiModel.m
//  vicinity-app
//
//  Created by Panda Systems on 10/27/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAGoogleApiModel.h"

@implementation VAGoogleApiModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    NSAssert(NO, @"VAGoogleApiModel does not provide concrete implementation, child classes must implement protocol");
    return nil;
}

@end
