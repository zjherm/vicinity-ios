//
//  VAGoogleLocation.m
//  vicinity-app
//
//  Created by Panda Systems on 10/28/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAGoogleLocation.h"

@implementation VAGoogleLocation

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"latitude": @"lat",
             @"longitude": @"lng",
             };
}

@end
