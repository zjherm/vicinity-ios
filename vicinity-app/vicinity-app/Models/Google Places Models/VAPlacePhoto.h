//
//  VAPlacePhoto.h
//  vicinity-app
//
//  Created by Panda Systems on 10/27/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAGoogleApiModel.h"

@interface VAPlacePhoto : VAGoogleApiModel

@property (nonatomic, strong) NSNumber *height;
@property (nonatomic, strong) NSNumber *width;
@property (nonatomic, strong) NSString *photoReference;

@end
