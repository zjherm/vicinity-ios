//
//  VAPlacePhoto.m
//  vicinity-app
//
//  Created by Panda Systems on 10/27/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAPlacePhoto.h"

@implementation VAPlacePhoto

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"height": @"height",
             @"width": @"width",
             @"photoReference": @"photo_reference"
             };
}

@end
