//
//  VAAddressComponent.m
//  vicinity-app
//
//  Created by Panda Systems on 10/27/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAAddressComponent.h"

static NSString *const kStreetNumberKey = @"street_number";
static NSString *const kStreetKey = @"route";
static NSString *const kCityKey = @"locality";
static NSString *const kStateKey = @"administrative_area_level_1";
static NSString *const kCountryKey = @"country";
static NSString *const kPostalCodeKey = @"postal_code";

@implementation VAAddressComponent

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"longName": @"long_name",
             @"shortName": @"short_name",
             @"types": @"types"
             };
}

- (VAAddressComponentType)type {
    for (NSString *typeString in self.types) {
        if ([typeString isEqualToString:kStreetNumberKey]) {
            return VAAddressComponentTypeStreetNumber;
        } else if ([typeString isEqualToString:kStreetKey]) {
            return VAAddressComponentTypeStreet;
        } else if ([typeString isEqualToString:kCityKey]) {
            return VAAddressComponentTypeCity;
        } else if ([typeString isEqualToString:kStateKey]) {
            return VAAddressComponentTypeState;
        } else if ([typeString isEqualToString:kCountryKey]) {
            return VAAddressComponentTypeCountry;
        } else if ([typeString isEqualToString:kPostalCodeKey]) {
            return VAAddressComponentTypePostalCode;
        }
    }
    
    return VAAddressComponentTypeUnknown;
}

@end
