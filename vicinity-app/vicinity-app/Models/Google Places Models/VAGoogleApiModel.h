//
//  VAGoogleApiModel.h
//  vicinity-app
//
//  Created by Panda Systems on 10/27/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface VAGoogleApiModel : MTLModel<MTLJSONSerializing>

@end
