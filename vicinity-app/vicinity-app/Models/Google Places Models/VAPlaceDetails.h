//
//  VAPlaceDetails.h
//  vicinity-app
//
//  Created by Panda Systems on 10/27/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAGoogleApiModel.h"

@class VAGoogleLocation;

@interface VAPlaceDetails : VAGoogleApiModel

@property (nonatomic, strong) NSString *placeId;
@property (nonatomic, strong) NSArray *addressComponents;
@property (nonatomic, strong) NSString *formattedAddress;
@property (nonatomic, strong) NSString *formattedPhoneNumber;
@property (nonatomic, strong) VAGoogleLocation *location;
@property (nonatomic, strong) NSString *internationalPhoneNumber;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSArray *openingHours;
@property (nonatomic, strong) NSArray *photos;
@property (nonatomic, strong) NSNumber *priceLevel;
@property (nonatomic, strong) NSNumber *rating;
@property (nonatomic, strong) NSNumber *userRatingsTotal;
@property (nonatomic, strong) NSString *website;
@property (nonatomic, strong) NSString *vicinity;
@property (nonatomic, strong) NSArray *types;

@property (nonatomic, strong) NSNumber *distance;

@property (nonatomic, strong, readonly) NSString *shortAddressStreet;

@property (nonatomic, strong, readonly) NSString *streetNumber;
@property (nonatomic, strong, readonly) NSString *street;
@property (nonatomic, strong, readonly) NSString *city;
@property (nonatomic, strong, readonly) NSString *state;
@property (nonatomic, strong, readonly) NSString *country;
@property (nonatomic, strong, readonly) NSString *postalCode;

@end
