//
//  UIViewController+VAMenuButton.h
//  vicinity-app
//
//  Created by Panda Systems on 2/22/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (VAMenuButton)

- (UIBarButtonItem *)getMenuButtonWithBadge:(BOOL)showBadge;
- (void)addBadgeObservers;
- (void)removeBadgeObservers;

@end
