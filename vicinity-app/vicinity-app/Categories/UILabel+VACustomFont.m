//
//  UILabel+VACustomFont.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 25.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "UILabel+VACustomFont.h"
#import "VAComment.h"
#import "VAUser.h"
#import "VAPost.h"
#import "VALocation.h"
#import <TTTAttributedLabel.h>
#import "TTTAttributedLabel+VALinks.h"
#import "VANotification.h"

NSString *const kHashtag = @"Hashtag";
NSString *const kMention = @"Mention";
NSString *const kCommentWord = @"CommentWord";
NSString *const kAlias = @"Alias";

NSString *const kType = @"type";
NSString *const kWord = @"word";

static NSString *const kURLRegExpPattern = @"(http:\\/\\/|https\\:\\/\\/)?(([a-z0-9][a-z0-9\\-]*\\.)+[a-z0-9][a-z0-9\\-]+)(\\/[a-z0-9\\-]*)*(\\.[a-z]+)?";

@implementation UILabel (VACustomFont)

- (void)setLetterSpacingWithMultiplier:(CGFloat)multiplier {
    [self setLetterSpacingWithMultiplier:multiplier forTitle:self.text];
}

- (void)setLetterSpacingWithMultiplier:(CGFloat)multiplier forTitle:(NSString *)title {
    if (title) {
        NSMutableAttributedString* attrStr = [[NSMutableAttributedString alloc] initWithString:title];
        [attrStr addAttribute:NSKernAttributeName value:@(multiplier) range:NSMakeRange(0, attrStr.length)];
        self.attributedText = attrStr;
    }
}

- (void)setLineSpacing:(CGFloat)spacing {
    [self setLineSpacing:spacing forTitle:self.text];
}

- (void)setLineSpacing:(CGFloat)spacing forTitle:(NSString *)title {
    if (title) {
        NSMutableAttributedString* attrStr = [[NSMutableAttributedString alloc] initWithString:title];
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        [style setLineSpacing:spacing];
        [attrStr addAttribute:NSParagraphStyleAttributeName
                        value:style
                        range:NSMakeRange(0, attrStr.length)];
        self.attributedText = attrStr;
    }
}

- (void)setLetterSpacingWithMultiplier:(CGFloat)multiplier lineSpacing:(CGFloat)spacing forTitle:(NSString *)title {
    if (title) {
        NSMutableAttributedString* attrStr = [[NSMutableAttributedString alloc] initWithString:title];
        [attrStr addAttribute:NSKernAttributeName value:@(multiplier) range:NSMakeRange(0, attrStr.length)];
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        [style setLineSpacing:spacing];
        [attrStr addAttribute:NSParagraphStyleAttributeName
                        value:style
                        range:NSMakeRange(0, attrStr.length)];
        self.attributedText = attrStr;
    }
}

- (void)addPostAtrributes {
    
    ((TTTAttributedLabel *)self).activeLinkAttributes = @{NSForegroundColorAttributeName: [[VADesignTool defaultDesign] vicinityDarkBlue]};
    ((TTTAttributedLabel *)self).linkAttributes = @{NSForegroundColorAttributeName: [[VADesignTool defaultDesign] vicinityDarkBlue]};
    
    [((TTTAttributedLabel *)self) setLinkModels:[NSArray array]];
    
    NSAttributedString *text = self.attributedText;
    self.attributedText = [[NSAttributedString alloc] initWithString:@""];
    self.attributedText = text;
    
    NSMutableCharacterSet *separatorSet = [NSMutableCharacterSet whitespaceAndNewlineCharacterSet];
    [separatorSet formUnionWithCharacterSet:[NSCharacterSet punctuationCharacterSet]];
    [separatorSet removeCharactersInString:@"@#.:/_'"];
    NSArray *postWords = [self.text componentsSeparatedByCharactersInSet:separatorSet];
    
    NSInteger wordStartIndex = 0;
    
    for (NSInteger i = 0; i < [postWords count]; i++) {
        NSString *word = postWords[i];
        if ([word rangeOfString:@"@"].location != NSNotFound) {
            NSString *aliasWord = [[word componentsSeparatedByString:@"@"] lastObject];
            if (aliasWord.length) {
                NSString *fullAlias = [@"@" stringByAppendingString:aliasWord];
                NSRange fullMentionRange = [self.text rangeOfString:fullAlias options:NSCaseInsensitiveSearch range:NSMakeRange(wordStartIndex, self.text.length - wordStartIndex)];                        //this is range of word with @
                [(TTTAttributedLabel *)self addTTTAttribute:TTTAttributeTypeMention forWord:fullAlias inRange:fullMentionRange];
            }
        } else if ([word rangeOfString:@"#"].location != NSNotFound) {
            NSString *hashtagWord = [[word componentsSeparatedByString:@"#"] lastObject];
            if (hashtagWord.length) {
                NSString *fullHashtag = [@"#" stringByAppendingString:hashtagWord];
                NSRange fullMentionRange = [self.text rangeOfString:fullHashtag options:NSCaseInsensitiveSearch range:NSMakeRange(wordStartIndex, self.text.length - wordStartIndex)];                        //this is range of word with #
                [(TTTAttributedLabel *)self addTTTAttribute:TTTAttributeTypeHashtah forWord:fullHashtag inRange:fullMentionRange];
            }
        }
        wordStartIndex += word.length + 1;     //+1 is for separator
    }
    
    
    //find url links
    if (self.text) {
        NSRegularExpression *urlRegExp = [NSRegularExpression regularExpressionWithPattern:kURLRegExpPattern
                                                                                   options:NSRegularExpressionCaseInsensitive
                                                                                     error:nil];
        NSArray *urlMatches = [urlRegExp matchesInString:self.text
                                                 options:0
                                                   range:NSMakeRange(0, self.text.length)];
        for (NSTextCheckingResult *match in urlMatches) {
            NSRange linkRange = [match rangeAtIndex:0];
            NSURL *url = [self urlWithMatchedString:[self.text substringWithRange:linkRange]];
            [(TTTAttributedLabel *)self addLinkToURL:url withRange:linkRange];
        }
    }
    
}

- (void)addCommentAtrributes {
    [self addPostAtrributes];
    
    NSArray *commentWords = [self.text componentsSeparatedByString:@" "];
    NSString *alias = [commentWords firstObject];
    NSRange aliasRange = [self.text rangeOfString:alias];
    
    [(TTTAttributedLabel *)self addTTTAttribute:TTTAttributeTypeAlias forWord:alias inRange:aliasRange];

    
}

- (CGFloat)heightForPostLabelWithWidth:(CGFloat)width {
    NSString *text = self.text;
    NSMutableAttributedString *postString = [[NSMutableAttributedString alloc] initWithString:text
                                                                                   attributes:@{
                                                                                                NSFontAttributeName : [[VADesignTool defaultDesign] vicinityRegularFontOfSize:15.f],
                                                                                                NSForegroundColorAttributeName: [UIColor blackColor]
                                                                                                }];
    
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)postString);
    CGSize targetSize = CGSizeMake(width, CGFLOAT_MAX);
    CGSize fitSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0, [postString length]), NULL, targetSize, NULL);
    CFRelease(framesetter);
    return fitSize.height + 1;
}

- (CGFloat)heightForCommentLabelWithWidth:(CGFloat)width {
    NSString *text = self.text;
    NSMutableAttributedString *postString = [[NSMutableAttributedString alloc] initWithString:text
                                                                                   attributes:@{
                                                                                                NSFontAttributeName : [[VADesignTool defaultDesign] vicinityRegularFontOfSize:14.f],
                                                                                                NSForegroundColorAttributeName: [UIColor blackColor]
                                                                                                }];
    
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)postString);
    CGSize targetSize = CGSizeMake(width, CGFLOAT_MAX);
    CGSize fitSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0, [postString length]), NULL, targetSize, NULL);
    CFRelease(framesetter);
    return fitSize.height + 1;
}

- (CGFloat)heightForLabelWithWidth:(CGFloat)width font:(UIFont *)font andTitile:(NSString *)title {
    UILabel *sizeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, CGFLOAT_MAX)];
    [sizeLabel setText:title];
    [sizeLabel setFont:font];
    sizeLabel.textAlignment = NSTextAlignmentLeft;
    sizeLabel.numberOfLines = 0;
    sizeLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize titleSize = [sizeLabel sizeThatFits:CGSizeMake(width, CGFLOAT_MAX)];
    
    return titleSize.height;
}

- (CGFloat)heightForCommentLabelWithWidth:(CGFloat)width andComment:(VAComment *)comment {
    NSString *commentText = comment.text;
    NSString *userNickname = comment.user.nickname;
    NSString *fullComment = [NSString stringWithFormat:@"%@ %@",userNickname, commentText];
    NSMutableAttributedString *commentAttrString = [[NSMutableAttributedString alloc] initWithString:fullComment attributes:@{NSFontAttributeName : [[VADesignTool defaultDesign] vicinityRegularFontOfSize:13.5f]}];
    
    NSRange nameBoldedRange = NSMakeRange(0, userNickname.length);
    
    [commentAttrString addAttributes:@{
                                       NSFontAttributeName: [[VADesignTool defaultDesign] vicinityBoldFontOfSize:13.5f],
                                       NSForegroundColorAttributeName: [[VADesignTool defaultDesign] vicinityDarkBlue]
                                       }
                               range:nameBoldedRange];
//    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, 45.0)];
//    label.numberOfLines = 2;
//    label.attributedText = commentAttrString;
//    [label sizeToFit];
//    return label.frame.size.height;
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)commentAttrString);
    CGSize targetSize = CGSizeMake(width + 5.0, 45.f);     //max height is 42 because we need maximum 2 lines in comments
    CGSize fitSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0, [commentAttrString length]), NULL, targetSize, NULL);
    CFRelease(framesetter);
    return fitSize.height;
}

- (CGFloat)heightForNameAndCheckInLabelWithWidth:(CGFloat)width andPost:(VAPost *)post {
    VAUser *user = post.user;
    VALocation *location = post.checkIn;
    
    NSString *resultString = nil;
    
    if (post.checkIn.address) {
        resultString = [NSString stringWithFormat:@"%@ checked in at %@", user.nickname, location.name];
    } else {
        resultString = user.nickname;
    }
    
    NSMutableAttributedString *checkInAttrString = nil;
    
    if (resultString) {
        NSRange nameBoldedRange = NSMakeRange(0, user.nickname.length);
        checkInAttrString = [[NSMutableAttributedString alloc] initWithString:resultString
                                                                                              attributes:@{
                                                                                                           NSFontAttributeName : [[VADesignTool defaultDesign] vicinityRegularFontOfSize:13.f],
                                                                                                           NSForegroundColorAttributeName: [UIColor blackColor]
                                                                                                           }];
        [checkInAttrString addAttributes:@{
                                           NSFontAttributeName: [[VADesignTool defaultDesign] vicinityBoldFontOfSize:15.f],
                                           NSForegroundColorAttributeName: [[VADesignTool defaultDesign] vicinityDarkBlue]
                                           }
                                   range:nameBoldedRange];
    }
    
    if (post.checkIn.address) {
        NSInteger preCheckInLength = [[NSString stringWithFormat:@"%@ checked in at ", user.nickname] length];
        NSRange checkInRange = NSMakeRange(preCheckInLength, location.name.length);
        
        [checkInAttrString addAttributes:@{
                                           NSFontAttributeName: [[VADesignTool defaultDesign] vicinityRegularFontOfSize:13.f],
                                           NSForegroundColorAttributeName: [[VADesignTool defaultDesign] vicinityDarkBlue]
                                           }
                                   range:checkInRange];
    }

    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)checkInAttrString);
    CGSize targetSize = CGSizeMake(width, CGFLOAT_MAX);
    CGSize fitSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0, [checkInAttrString length]), NULL, targetSize, NULL);
    CFRelease(framesetter);
    
    return fitSize.height;
}

- (CGFloat)heightForNotificationEventLabellWithWidth:(CGFloat)width andNotification:(VANotification *)notification {
    VAUser *user = notification.user;
    NSString *text = notification.eventText;
    NSString *alias = user.nickname;
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:text
                                                                                attributes:@{
                                                                                             NSFontAttributeName : [[VADesignTool defaultDesign] vicinityRegularFontOfSize:12.f],
                                                                                             NSForegroundColorAttributeName: [UIColor blackColor]
                                                                                             }];
    
    [attrStr addAttributes:@{
                             NSFontAttributeName: [[VADesignTool defaultDesign] vicinityBoldFontOfSize:12.f],
                             NSForegroundColorAttributeName: [[VADesignTool defaultDesign] vicinityDarkBlue]
                             }
                     range:[text rangeOfString:alias]];
    
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)attrStr);
    CGSize targetSize = CGSizeMake(width, CGFLOAT_MAX);
    CGSize fitSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0, [attrStr length]), NULL, targetSize, NULL);
    CFRelease(framesetter);
    
    return fitSize.height;
}

- (CGFloat)heightForNotificationTextLabelWithWidth:(CGFloat)width andNotification:(VANotification *)notification {
    
    if ([notification.eventType isEqualToString:@"mention.post"] || [notification.eventType isEqualToString:@"mention.comment"] || [notification.eventType isEqualToString:@"connection"]) {
        notification.text = @"";
    }
    
    NSString *showString = [NSString stringWithFormat:@"\"%@\"", notification.text ? notification.text : @""];
    
    if ([showString isEqualToString:@"\"\""]) {
        return 0.f;
    }

    NSMutableAttributedString *detailStr = [[NSMutableAttributedString alloc] initWithString:showString
                                                                                  attributes:@{
                                                                                               NSFontAttributeName : [[VADesignTool defaultDesign] vicinityRegularFontOfSize:12.f],
                                                                                               NSForegroundColorAttributeName: [UIColor blackColor]
                                                                                               }];

    
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)detailStr);
    CGSize targetSize = CGSizeMake(width, 20.f);                        //1 line max
    CGSize fitSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0, [detailStr length]), NULL, targetSize, NULL);
    CFRelease(framesetter);
    
    return fitSize.height;
}

- (void)addTTTAttribute:(TTTAttributeType)type forWord:(NSString *)word inRange:(NSRange)range {
    NSString *typeString = nil;
    switch (type) {
        case TTTAttributeTypeAlias:
            typeString = kAlias;
            break;
        case TTTAttributeTypeCommentWord:
            typeString = kCommentWord;
            break;
        case TTTAttributeTypeHashtah:
            typeString = kHashtag;
            break;
        case TTTAttributeTypeMention:
            typeString = kMention;
            break;
        default:
            break;
    }
    [(TTTAttributedLabel *)self addLinkToTransitInformation:@{
                                                              @"type": typeString,
                                                              @"word": word,
                                                              }
                                                  withRange:range];
}

#pragma mark - url link 
- (NSURL *) urlWithMatchedString:(NSString *)urlString
{
    NSString * resultURLString = urlString.lowercaseString;
    if ([resultURLString hasPrefix:@"http"]) {
        return [NSURL URLWithString:resultURLString];
    }
    NSRange rangeOf = [resultURLString rangeOfString:@"/"];
    NSString *host = @"";
    NSString *path = @"/";
    if (rangeOf.location != NSNotFound) {
        host = [resultURLString substringToIndex:rangeOf.location];
        path = [resultURLString substringFromIndex:rangeOf.location];
        
    }
    else {
        host = resultURLString;
    }
    NSURL *url = [[NSURL alloc] initWithScheme:@"http" host:host path:path];
    return url;
}

@end
