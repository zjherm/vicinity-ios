//
//  UITableViewCell+VAParentCell.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 01.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (VAParentCell)
+ (UITableViewCell *)findParentCellOfView:(UIView *)view;
@end
