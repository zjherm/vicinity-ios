//
//  UILabel+VACustomFont.h
//  vicinity-app
//
//  Created by Evgeny Dedovets on 02/06/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

@interface NSMutableDictionary (Params)

- (void)addNotNill:(id)obj forKey:(id <NSCopying>)key;

@end
