//
//  UIViewController+VAMenuButton.m
//  vicinity-app
//
//  Created by Panda Systems on 2/22/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import "UIViewController+VAMenuButton.h"

#import "VANotificationTool.h"

@implementation UIViewController (VAMenuButton)

- (UIBarButtonItem *)getMenuButtonWithBadge:(BOOL)showBadge {
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    menuButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [menuButton setImage:[UIImage imageNamed:@"Menu Icon"] forState:UIControlStateNormal];
    [menuButton addTarget:self.navigationController action:@selector(showMenu) forControlEvents:UIControlEventTouchUpInside];
    [customView addSubview:menuButton];
    if (showBadge) {
        UIView *badgeView = [[UIView alloc] initWithFrame:CGRectMake(10, 7, 12.5, 12.5)];
        badgeView.layer.cornerRadius = 6.25f;
        badgeView.clipsToBounds = YES;
        badgeView.backgroundColor = [[VADesignTool defaultDesign] vicinityOrange];
        [customView addSubview:badgeView];
    }
    return [[UIBarButtonItem alloc] initWithCustomView:customView];
}

- (void)addBadgeObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showButtonBadge) name:kShowBadgeForMenuIcon object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideButtonBadge) name:kHideBadgeForMenuIcon object:nil];
}

- (void)removeBadgeObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kShowBadgeForMenuIcon object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kHideBadgeForMenuIcon object:nil];
}

@end
