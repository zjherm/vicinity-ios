//
//  NSNumber+VAMilesDistance.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 15.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (VAMilesDistance)

- (NSString *)milesStringFromNSNumberMeters:(BOOL)highAccuracy;

@end
