//
//  NSDate+VAString.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 10.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (VAString)

- (NSString *)messageTimeForDate;

@end
