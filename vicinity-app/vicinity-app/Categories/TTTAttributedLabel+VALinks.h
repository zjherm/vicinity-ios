//
//  TTTAttributedLabel+VALinks.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 8/19/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "TTTAttributedLabel.h"

@interface TTTAttributedLabel (VALinks)

- (void)setLinkModels:(NSArray *)linkModels;

@end
