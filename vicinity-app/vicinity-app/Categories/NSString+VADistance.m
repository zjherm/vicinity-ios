//
//  NSString+VADistance.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 8/31/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "NSString+VADistance.h"

@implementation NSString (VADistance)

- (CGFloat)metersFromMilesString {
    CGFloat miles;
    CGFloat kilometersInMile = 1.609344f;
    if ([self isEqualToString:@"Few blocks"]) {
        miles = 0.1f;
    } else if ([self isEqualToString:@"1/4 mile"]) {
        miles = 0.25f;
    } else if ([self isEqualToString:@"1/2 mile"]) {
        miles = 0.5f;
    } else if ([self isEqualToString:@"1 mile"]) {
        miles = 1.f;
    } else if ([self isEqualToString:@"2 miles"]) {
        miles = 2.f;
    } else if ([self isEqualToString:@"5 miles"]) {
        miles = 5.f;
    } else if ([self isEqualToString:@"10 miles"]) {
        miles = 10.f;
    } else if ([self isEqualToString:@"25 miles"]) {
        miles = 25.f;
    } else if ([self isEqualToString:@"50 miles"]) {
        miles = 50.f;
    }
    
    CGFloat meters = miles * kilometersInMile * 1000.f;
    return meters;
}

@end
