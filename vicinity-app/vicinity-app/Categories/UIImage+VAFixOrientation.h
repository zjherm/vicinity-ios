//
//  UIImage+VAFixOrientation.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 29.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (VAFixOrientation)
- (UIImage *)fixOrientation;
@end
