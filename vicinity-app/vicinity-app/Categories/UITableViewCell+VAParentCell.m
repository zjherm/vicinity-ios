//
//  UITableViewCell+VAParentCell.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 01.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "UITableViewCell+VAParentCell.h"

@implementation UITableViewCell (VAParentCell)

+ (UITableViewCell *)findParentCellOfView:(UIView *)view {
    if (view == nil || [view isKindOfClass:[UITableViewCell class]]) {
        return (UITableViewCell *)view;
    }
    return [self findParentCellOfView:[view superview]];
}

@end
