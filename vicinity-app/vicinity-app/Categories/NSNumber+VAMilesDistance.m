//
//  NSNumber+VAMilesDistance.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 15.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "NSNumber+VAMilesDistance.h"

@implementation NSNumber (VAMilesDistance)

- (double)milesFromMeters {
    double miles = [self floatValue] * 0.000621371192;
    return miles;
}

- (NSString *)milesStringFromNSNumberMeters:(BOOL)highAccuracy {
    double miles = [self milesFromMeters];
    
    if (!highAccuracy && miles < 0.5f) {
        return @"< 0.5 mi";
    }
    
    NSInteger feetsInMile = 5280;
    if (miles < 0.1f) {
        double feets = miles * feetsInMile;
        return [NSString stringWithFormat:@"%ld ft", (long)feets];
    }
    
    NSInteger integralPart = (NSInteger)miles;
    double fractionalPart = miles - integralPart;
    CGFloat breaker = 0.95f;

    while (fractionalPart < breaker) {
        breaker -= 0.1f;
    }

    if (breaker < 0) {
        fractionalPart = 0;
    } else {
        fractionalPart = breaker + 0.05f;
    }
    if (fractionalPart == 1) {
        fractionalPart = 0;
        integralPart += 1;
    }
    return [NSString stringWithFormat:@"%.1f mi", integralPart + fractionalPart];
}

@end
