//
//  NSDate+VAString.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 10.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "NSDate+VAString.h"

@implementation NSDate (VAString)

- (NSString *)messageTimeForDate {
    NSString *time = nil;
    NSInteger gap = [[NSDate dateWithTimeIntervalSinceNow:0] timeIntervalSinceDate:self];
    
    switch (gap) {
        case 0 ... 59:
            time = [NSString stringWithFormat:@"%lds", gap];
            break;
        case 60 ... 60 * 60 - 1:
            time = [NSString stringWithFormat:@"%ldm", gap / 60];
            break;
        case 3600 ... (3600 * 24 - 1):
            time = [NSString stringWithFormat:@"%ldh", gap / 60 / 60];
            break;
        case (3600 * 24) ... (3600 * 24 * 7 - 1):
            time = [self dayStringDependingOnWeekDaysForGap:gap];
            break;
        default:
            time = [NSString stringWithFormat:@"%ldw", gap / 60 / 60 / 24 / 7];
            break;
    }
    
    if (gap < 0) {
        time = @"0s";
    }
    return time;
}

- (NSString *)dayStringDependingOnWeekDaysForGap:(NSInteger)gap {
    
    static NSArray *weekDays = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        weekDays = @[@"Monday", @"Tuesday", @"Wednesday", @"Thursday", @"Friday", @"Saturday", @"Sunday"];
    });
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat:@"EEEE"];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"US"]];
    NSString *today = [dateFormatter stringFromDate:[NSDate date]];
    NSInteger todaysIndex = [weekDays indexOfObject:today];
    
    NSString *eventDay = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSinceNow:-gap]];
    NSInteger eventsDayIndex = [weekDays indexOfObject:eventDay];
    
    if (todaysIndex > eventsDayIndex) {
        return [NSString stringWithFormat:@"%ldd", todaysIndex - eventsDayIndex];
    } else {
        return [NSString stringWithFormat:@"%ldd", (7 + todaysIndex - eventsDayIndex) == 7 ? 6 : 7 + todaysIndex - eventsDayIndex];
    }
}
@end
