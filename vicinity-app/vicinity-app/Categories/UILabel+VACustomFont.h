//
//  UILabel+VACustomFont.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 25.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, TTTAttributeType) {
    TTTAttributeTypeAlias = 0,
    TTTAttributeTypeHashtah,
    TTTAttributeTypeMention,
    TTTAttributeTypeCommentWord
};

extern NSString *const kHashtag;
extern NSString *const kMention;
extern NSString *const kAlias;
extern NSString *const kCommentWord;

extern NSString *const kType;
extern NSString *const kWord;

@class VAComment;
@class VAPost;
@class VANotification;

@interface UILabel (VACustomFont)

- (void)setLetterSpacingWithMultiplier:(CGFloat)multiplier;

- (void)setLetterSpacingWithMultiplier:(CGFloat)multiplier forTitle:(NSString *)title;

- (void)setLineSpacing:(CGFloat)spacing;

- (void)setLineSpacing:(CGFloat)spacing forTitle:(NSString *)title;

- (void)setLetterSpacingWithMultiplier:(CGFloat)multiplier lineSpacing:(CGFloat)spacing forTitle:(NSString *)title;

- (void)addPostAtrributes;

- (void)addCommentAtrributes;

- (CGFloat)heightForPostLabelWithWidth:(CGFloat)width;

- (CGFloat)heightForCommentLabelWithWidth:(CGFloat)width;

- (CGFloat)heightForLabelWithWidth:(CGFloat)width font:(UIFont *)font andTitile:(NSString *)title;

- (CGFloat)heightForCommentLabelWithWidth:(CGFloat)width andComment:(VAComment *)comment;

- (CGFloat)heightForNameAndCheckInLabelWithWidth:(CGFloat)width andPost:(VAPost *)post;

- (CGFloat)heightForNotificationEventLabellWithWidth:(CGFloat)width andNotification:(VANotification *)notification;

- (CGFloat)heightForNotificationTextLabelWithWidth:(CGFloat)width andNotification:(VANotification *)notification;

@end
