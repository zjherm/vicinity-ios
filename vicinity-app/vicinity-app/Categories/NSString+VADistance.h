//
//  NSString+VADistance.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 8/31/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (VADistance)

- (CGFloat)metersFromMilesString;

@end
