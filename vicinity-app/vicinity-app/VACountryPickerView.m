//
//  VACountryPickerView.m
//  vicinity-app
//
//  Created by Panda Systems on 2/25/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import "VACountryPickerView.h"

@interface VACountryPickerView () <UIPickerViewDataSource, UIPickerViewDelegate>
@property (nonatomic, strong) NSArray *countries;
@end

@implementation VACountryPickerView

- (instancetype)init {
    self = [super init];
    if (self) {
        self.tintColor = [[VADesignTool defaultDesign] middleGrayColor];
        self.backgroundColor = [UIColor whiteColor];
        self.dataSource = self;
        self.delegate = self;
        [self selectRow:5 inComponent:0 animated:NO];
        
        _countries = [self countriesArray];
    }
    return self;
}

- (void)setCurrentCountry:(NSString *)currentCountry {
    _currentCountry = currentCountry;
    
    NSInteger index = [self.countries indexOfObject:currentCountry];
    if (index != NSNotFound) {
        [self reloadAllComponents];
        [self selectRow:index inComponent:0 animated:NO];
    } else {
        [self reloadAllComponents];
        [self selectRow:0 inComponent:0 animated:YES];
        _currentCountry = _countries[0];
    }
}

#pragma mark - Data source

+ (NSArray *)countryNames
{
    static NSArray *_countryNames = nil;
    if (!_countryNames)
    {
        _countryNames = [[[[self countryNamesByCode] allValues] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] copy];
    }
    return _countryNames;
}

+ (NSArray *)countryCodes
{
    static NSArray *_countryCodes = nil;
    if (!_countryCodes)
    {
        _countryCodes = [[[self countryCodesByName] objectsForKeys:[self countryNames] notFoundMarker:@""] copy];
    }
    return _countryCodes;
}

+ (NSDictionary *)countryNamesByCode
{
    static NSDictionary *_countryNamesByCode = nil;
    if (!_countryNamesByCode)
    {
        NSMutableDictionary *namesByCode = [NSMutableDictionary dictionary];
        for (NSString *code in [NSLocale ISOCountryCodes])
        {
            NSString *countryName = [[NSLocale currentLocale] displayNameForKey:NSLocaleCountryCode value:code];
            
            //workaround for simulator bug
            if (!countryName)
            {
                countryName = [[NSLocale localeWithLocaleIdentifier:@"en_US"] displayNameForKey:NSLocaleCountryCode value:code];
            }
            
            namesByCode[code] = countryName ?: code;
        }
        _countryNamesByCode = [namesByCode copy];
    }
    return _countryNamesByCode;
}

+ (NSDictionary *)countryCodesByName
{
    static NSDictionary *_countryCodesByName = nil;
    if (!_countryCodesByName)
    {
        NSDictionary *countryNamesByCode = [self countryNamesByCode];
        NSMutableDictionary *codesByName = [NSMutableDictionary dictionary];
        for (NSString *code in countryNamesByCode)
        {
            codesByName[countryNamesByCode[code]] = code;
        }
        _countryCodesByName = [codesByName copy];
    }
    return _countryCodesByName;
}

- (NSArray *)countriesArray {
    return @[@"United States", @"Afghanistan", @"Åland Islands", @"Albania", @"Algeria", @"American Samoa", @"Andorra", @"Angola",
             @"Anguilla", @"Antarctica", @"Antigua & Barbuda", @"Argentina", @"Armenia", @"Aruba", @"Australia", @"Austria",
             @"Azerbaijan", @"Bahamas", @"Bahrain", @"Bangladesh", @"Barbados", @"Belarus", @"Belgium", @"Belize", @"Benin",
             @"Bermuda", @"Bhutan", @"Bolivia", @"Bosnia & Herzegovina", @"Botswana", @"Bouvet Island", @"Brazil", @"British Indian Ocean Territory",
             @"British Virgin Islands", @"Brunei", @"Bulgaria", @"Burkina Faso", @"Burundi", @"Cambodia", @"Cameroon", @"Canada", @"Cape Verde",
             @"Caribbean Netherlands", @"Cayman Islands", @"Central African Republic", @"Chad", @"Chile", @"China", @"Christmas Island",
             @"Cocos (Keeling) Islands", @"Colombia", @"Comoros", @"Congo - Brazzaville", @"Congo - Kinshasa", @"Cook Islands", @"Costa Rica",
             @"Côte d’Ivoire", @"Croatia", @"Cuba", @"Curaçao", @"Cyprus", @"Czech Republic", @"Denmark", @"Djibouti", @"Dominica", @"Dominican Republic",
             @"Ecuador", @"Egypt", @"El Salvador", @"Equatorial Guinea", @"Eritrea", @"Estonia", @"Ethiopia", @"Falkland Islands", @"Faroe Islands",
             @"Fiji", @"Finland", @"France", @"French Guiana", @"French Polynesia", @"French Southern Territories", @"Gabon", @"Gambia", @"Georgia",
             @"Germany", @"Ghana", @"Gibraltar", @"Greece", @"Greenland", @"Grenada", @"Guadeloupe", @"Guam", @"Guatemala", @"Guernsey", @"Guinea",
             @"Guinea-Bissau", @"Guyana", @"Haiti", @"Heard & McDonald Islands", @"Honduras", @"Hong Kong SAR China", @"Hungary", @"Iceland", @"India",
             @"Indonesia", @"Iran", @"Iraq", @"Ireland", @"Isle of Man", @"Israel", @"Italy", @"Jamaica", @"Japan", @"Jersey", @"Jordan", @"Kazakhstan",
             @"Kenya", @"Kiribati", @"Kuwait", @"Kyrgyzstan", @"Laos", @"Latvia", @"Lebanon", @"Lesotho", @"Liberia", @"Libya", @"Liechtenstein",
             @"Lithuania", @"Luxembourg", @"Macau SAR China", @"Macedonia", @"Madagascar", @"Malawi", @"Malaysia", @"Maldives", @"Mali", @"Malta",
             @"Marshall Islands", @"Martinique", @"Mauritania", @"Mauritius", @"Mayotte", @"Mexico", @"Micronesia", @"Moldova", @"Monaco", @"Mongolia",
             @"Montenegro", @"Montserrat", @"Morocco", @"Mozambique", @"Myanmar (Burma)", @"Namibia", @"Nauru", @"Nepal", @"Netherlands",
             @"New Caledonia", @"New Zealand", @"Nicaragua", @"Niger", @"Nigeria", @"Niue", @"Norfolk Island", @"North Korea", @"Northern Mariana Islands",
             @"Norway", @"Oman", @"Pakistan", @"Palau", @"Palestinian Territories", @"Panama", @"Papua New Guinea", @"Paraguay", @"Peru", @"Philippines",
             @"Pitcairn Islands", @"Poland", @"Portugal", @"Puerto Rico", @"Qatar", @"Réunion", @"Romania", @"Russia", @"Rwanda", @"Samoa", @"San Marino",
             @"São Tomé & Príncipe", @"Saudi Arabia", @"Senegal", @"Serbia", @"Seychelles", @"Sierra Leone", @"Singapore", @"Sint Maarten", @"Slovakia",
             @"Slovenia", @"So. Georgia & So. Sandwich Isl.", @"Solomon Islands", @"Somalia", @"South Africa", @"South Korea", @"South Sudan", @"Spain",
             @"Sri Lanka", @"St. Barthélemy", @"St. Helena", @"St. Kitts & Nevis", @"St. Lucia", @"St. Martin", @"St. Pierre & Miquelon",
             @"St. Vincent & Grenadines", @"Sudan", @"Suriname", @"Svalbard & Jan Mayen", @"Swaziland", @"Sweden", @"Switzerland", @"Syria",
             @"Taiwan", @"Tajikistan", @"Tanzania", @"Thailand", @"Timor-Leste", @"Togo", @"Tokelau", @"Tonga", @"Trinidad & Tobago", @"Tunisia",
             @"Turkey", @"Turkmenistan", @"Turks & Caicos Islands", @"Tuvalu", @"U.S. Outlying Islands", @"U.S. Virgin Islands", @"Uganda",
             @"Ukraine", @"United Arab Emirates", @"United Kingdom", @"Uruguay", @"Uzbekistan", @"Vanuatu", @"Vatican City", @"Venezuela",
             @"Vietnam", @"Wallis & Futuna", @"Western Sahara", @"Yemen", @"Zambia", @"Zimbabwe"];
//    NSMutableArray *countries = [[VACountryPickerView countryNames] mutableCopy];
//    [countries removeObject:@"United States"];
//    [countries insertObject:@"United States" atIndex:0];
//    return countries;
}

#pragma mark - UIPickerViewDelegate methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.countries.count;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    UILabel *tView = (UILabel*)view;
    if (!tView) {
        tView = [[UILabel alloc] init];
        [tView setFont:[[VADesignTool defaultDesign] vicinityRegularFontOfSize:14.f]];
        [tView setTextAlignment:NSTextAlignmentCenter];
        tView.numberOfLines = 3;
    }
    
    tView.text = [NSString stringWithFormat:@"%@", self.countries[row]];
    return tView;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    _currentCountry = self.countries[row];
    if (self.countryDelegate) {
        [self.countryDelegate didSelectCountry:self.countries[row]];
    }
}

@end
