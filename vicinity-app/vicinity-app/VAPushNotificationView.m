//
//  VAPushNotificationView.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 10.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAPushNotificationView.h"
#import "VAControlTool.h"
#import "VACommentsViewController.h"
#import "VAProfileViewController.h"
#import "DEMONavigationController.h"
#import "VANotificationTool.h"
#import "VAUserDefaultsHelper.h"
#import "VALoginViewController.h"
#import <MFSideMenu.h>

@interface VAPushNotificationView ()
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *appNameLabel;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UILabel *notificationLabel;

@property (assign, nonatomic) BOOL notificationMessageIsShown;
@property (strong, nonatomic) UIVisualEffectView *effectView;
@property (strong, nonatomic) UIVisualEffectView *vibrantView;
@end

@implementation VAPushNotificationView

- (void)awakeFromNib {
    self.iconImageView.layer.cornerRadius = 5.f;
    self.iconImageView.clipsToBounds = YES;
    
    self.appNameLabel.text = @"Vicinity";
    
//    [self.notificationLabel sizeToFit];
    
    self.frame = CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), 90.f);
    
    self.backgroundColor = [UIColor clearColor];
    
    if (IOS8 || IOS9) {
        self.backgroundView.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVibrancyEffect *vibrancy = [UIVibrancyEffect effectForBlurEffect:blur];
        
        UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
        effectView.frame = self.frame;
        
        UIVisualEffectView *vibrantView = [[UIVisualEffectView alloc]initWithEffect:vibrancy];
        vibrantView.frame = self.frame;
        
        self.effectView = effectView;
        self.vibrantView = vibrantView;
        
        [self.backgroundView addSubview:effectView];
        [self.backgroundView addSubview:vibrantView];
    } else if (IOS7) {
        self.backgroundView.backgroundColor = [UIColor blackColor];
        self.backgroundView.alpha = 0.85f;
    }
    
    self.notificationMessageIsShown = NO;
    
    self.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapMessage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bannerTap:)];
    [tapMessage setNumberOfTapsRequired:1];
    [self addGestureRecognizer:tapMessage];
}

#pragma mark - Methods

- (void)showPushMessageWithText:(NSString *)text {
//    [self layoutIfNeeded];
    
    self.notificationLabel.text = text;
    
    if (self.notificationMessageIsShown) {
        [self hideCurrentMessage];
    }
    [self setFrame:CGRectMake(CGRectGetMinX(self.frame), -CGRectGetHeight(self.frame), CGRectGetWidth(self.frame), CGRectGetHeight(self.frame))];
    UIViewController *vc = [[[UIApplication sharedApplication] keyWindow] rootViewController];     //if we skip login rootVC is settingsViewController, which is container for all another controllers
    if ([vc isKindOfClass:[VALoginViewController class]]) {                   //if we don't skip login, vc is loginViewController. But we should show view on settingsViewController
        UIViewController *topVC = [[[[UIApplication sharedApplication]  keyWindow] rootViewController] presentedViewController];
        if ([topVC isKindOfClass:[MFSideMenuContainerViewController class]]) {
            vc = topVC;
        }
    }
    [vc.view addSubview:self];
    [UIView animateWithDuration:0.3f animations:^{
        [self setFrame:CGRectMake(CGRectGetMinX(self.frame), 0, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame))];
    } completion:^(BOOL finished) {
        self.notificationMessageIsShown = YES;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self hideCurrentMessage];
        });
    }];
}

- (void)hideCurrentMessage {
    [UIView animateWithDuration:0.3f animations:^{
        [self setFrame:CGRectMake(CGRectGetMinX(self.frame), -CGRectGetHeight(self.frame), CGRectGetWidth(self.frame), CGRectGetHeight(self.frame))];
    } completion:^(BOOL finished) {
        for (id subview in self.subviews) {
            [subview removeFromSuperview];
        }
        [self removeFromSuperview];
        self.notificationMessageIsShown = NO;
    }];
}

- (void)bannerTap:(id)sender {
    if (self.postID) {
        NSLog(@"Show Post: %@", self.postID);
        [self showPost];
    } else if (self.userNickname) {
        NSLog(@"Show Profile: %@", self.userNickname);
        [self showProfile];
    }
}

- (void)showPost {
    if (self.postID) {
        VACommentsViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"VACommentsViewController"];
        vc.postID = self.postID;
        UIViewController *rootVC = [[[UIApplication sharedApplication] keyWindow] rootViewController];
        MFSideMenuContainerViewController *container = nil;
        if ([rootVC isKindOfClass:[MFSideMenuContainerViewController class]]) {
            container = (MFSideMenuContainerViewController *)rootVC;
        } else if ([rootVC isKindOfClass:[VALoginViewController class]]) {
            container = (MFSideMenuContainerViewController *)rootVC.presentedViewController;
        }
        if (container) {
            DEMONavigationController *nav = (DEMONavigationController *)[container centerViewController];
            [container setMenuState:MFSideMenuStateClosed];
            [nav pushViewController:vc animated:YES];
            
            [self hideCurrentMessage];
            if ([UIApplication sharedApplication].applicationIconBadgeNumber == 1) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kHideBadgeForMenuIcon object:nil];
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kShouldShowNotificationBadge];
            } else {
                [UIApplication sharedApplication].applicationIconBadgeNumber -= 1;
            }
        }
    }
}

- (void)showProfile {
    if (self.userNickname) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        VAProfileViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"profileViewController"];
        vc.searchAlias = self.userNickname;
        vc.isUsersAccountMode = YES;
        
        UIViewController *rootVC = [[[UIApplication sharedApplication] keyWindow] rootViewController];
        MFSideMenuContainerViewController *container = nil;
        if ([rootVC isKindOfClass:[MFSideMenuContainerViewController class]]) {
            container = (MFSideMenuContainerViewController *)rootVC;
        } else if ([rootVC isKindOfClass:[VALoginViewController class]]) {
            container = (MFSideMenuContainerViewController *)rootVC.presentedViewController;
        }
        if (container) {
            DEMONavigationController *nav = (DEMONavigationController *)[container centerViewController];
            [container setMenuState:MFSideMenuStateClosed];
            [nav pushViewController:vc animated:YES];
            
            [self hideCurrentMessage];
            if ([UIApplication sharedApplication].applicationIconBadgeNumber == 1) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kHideBadgeForMenuIcon object:nil];
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kShouldShowNotificationBadge];
            } else {
                [UIApplication sharedApplication].applicationIconBadgeNumber -= 1;
            }
        }
    }
}

@end
