//
//  VAAppDelegate.m
//  vicinity-app
//
//  Created by Panda Systems on 5/18/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAAppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <TSMessages/TSMessageView.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <Google/Analytics.h>
#import "VAUserTool.h"
#import "VAUserDefaultsHelper.h"
#import "VAEventsViewController.h"
#import "VAProfileViewController.h"
#import "VACommentsViewController.h"
#import "DEMONavigationController.h"
#import "VANotificationMessage.h"
#import "VANotificationTool.h"
#import "VAControlTool.h"
#import "VAPushNotificationView.h"
#include <AudioToolbox/AudioToolbox.h>
#import <OpenInGoogleMapsController.h>
#import "VAUserApiManager.h"
#import "VAMenuTableViewController.h"
#import "VAUser.h"
#import <GoogleMaps/GoogleMaps.h>
#import "VAChooseNewPasswordViewController.h"
#import "AFNetworkReachabilityManager.h"
#import "VAWelcomeViewController.h"
#import "VAControlTool.h"
#import <LaunchKit/LaunchKit.h>
#import "MSDPreventImagePickerCrashOn3DTouch.h"
#import <HockeySDK/HockeySDK.h>
#import "VAConfirmEmailViewController.h"
#import "Branch.h"

@interface VAAppDelegate ()

@end

@implementation VAAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
    
    [Fabric with:@[CrashlyticsKit]];
    
    [self setUpGoogleMapsInstance];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kUserDataLoaded];
    
    VAUser *user = (VAUser *)[VAUserDefaultsHelper getModelForKey:VASaveKeys_CurrentUser];
    [LaunchKit launchWithToken:@"_Vl_ehjhlXiv2FJrI-FJNhdwQ3qXmhGJsuOC8TnGG948"];

    if ([VAUserDefaultsHelper getAuthToken] && ![user.nickname isEqualToString:@""] && [user.isEmailConfirmed boolValue]) {
        if (user.userId && user.nickname && user.email) {
            [[LaunchKit sharedInstance] setUserIdentifier:user.userId
                                                    email:user.email
                                                     name:user.nickname];
        }
        [self goToCorrectScreen:launchOptions];
    } else if ([VAUserDefaultsHelper getAuthToken]) {
        // Go to welcome screen
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Vicinity" bundle:nil];
        
        VAWelcomeViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"welcomeViewController"];
        self.window.rootViewController = vc;
        [self.window makeKeyAndVisible];
    }
    [GMSServices provideAPIKey:@"AIzaSyBmc-f8xb6Mhvnpc0uX5O_pfxKBC734pJo"];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [[VAImageCache sharedImageCache] setCroppedImageSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,
                                                                    [UIScreen mainScreen].bounds.size.width)];
    MSDPreventImagePickerCrashOn3DTouch();
    
#ifdef VICINITY
    NSError *configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
#endif
    
    // HockeySDK
    [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:@"dce294d6f6d54cf282623cd736f4dcec"];
#ifdef DEBUG
    [[BITHockeyManager sharedHockeyManager] setDisableUpdateManager:YES];
#endif
    [[BITHockeyManager sharedHockeyManager] startManager];
    [[BITHockeyManager sharedHockeyManager].authenticator authenticateInstallation];
    
    // Branch
    Branch *branch = [Branch getInstance];
    [branch initSessionWithLaunchOptions:launchOptions andRegisterDeepLinkHandler:^(NSDictionary *params, NSError *error) {
        NSLog(@"Deep link data: %@", [params description]);
    }];

    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
//    if ([[VALocationTool sharedInstance] isServicesEnabled]) {
        [[VALocationTool sharedInstance] getCurrentDeviceLocationWithComplition:^(NSError *error, CLLocation *location) {
            if (error) {
                if (![[VALocationTool sharedInstance] isServicesEnabled]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:kDidDisableLocationServices object:nil];
                }
//                UIViewController *vc = [VAControlTool topScreenController];
//                [[VANotificationMessage currentMessage] showTopNotificationMessageWithTitle:error.localizedDescription onController:vc];
//                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                    [[VANotificationMessage currentMessage] hideCurrentMessage];
//
//
//                });

            } else if (location) {
                double lat = location.coordinate.latitude;
                double lon = location.coordinate.longitude;
                [VAUserDefaultsHelper savelatitude:lat andLongitude:lon];
                [[NSNotificationCenter defaultCenter] postNotificationName:kDidUpdateLocaion object:nil];
            }
        }];
//    } else {
//        [[NSNotificationCenter defaultCenter] postNotificationName:kDidDisableLocationServices object:nil];
//    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    NSInteger badgeCount = application.applicationIconBadgeNumber;

    if (badgeCount > 0) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kShouldShowNotificationBadge];
        [[NSNotificationCenter defaultCenter] postNotificationName:kShowBadgeForMenuIcon object:nil];
    }
    
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    [[Branch getInstance] handleDeepLink:url];
    
    NSString * q = [url query];
    NSArray * pairs = [q componentsSeparatedByString:@"&"];
    NSMutableDictionary * kvPairs = [NSMutableDictionary dictionary];
    for (NSString * pair in pairs) {
        NSArray * bits = [pair componentsSeparatedByString:@"="];
        NSString * key = [[bits objectAtIndex:0] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString * value = [[bits objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [kvPairs setObject:value forKey:key];
    }
    NSString *token = kvPairs[@"token"];
    NSString *email = kvPairs[@"login"];
    if (![VAUserDefaultsHelper getAuthToken] && email && token) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        VAChooseNewPasswordViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"VAChooseNewPasswordViewController"];
        vc.token = token;
        vc.email = email;
        DEMONavigationController *nav = (DEMONavigationController*)[storyboard instantiateViewControllerWithIdentifier:@"MyVicinityNavigationController"];
        [[VAControlTool defaultControl] setEmailLoginMethod];
        [nav setViewControllers:@[vc] animated:NO];
        self.window.rootViewController = nav;
    }
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray * _Nullable))restorationHandler {
    BOOL handledByBranch = [[Branch getInstance] continueUserActivity:userActivity];
    
    return handledByBranch;
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kUserHasSeenNotificationAlert];
    
    
    UIUserNotificationType types = notificationSettings.types;
    if ( types != 0 ) {
        [application registerForRemoteNotifications];
    } else {
#if TARGET_IPHONE_SIMULATOR
        NSLog(@"Push notifications unavailable in simulator.");
#else
        [VANotificationMessage showTurnOnNotificationsMessageOnController:[VAControlTool topScreenController] withDuration:0];
#endif

    }
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
        
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kUserHasSeenNotificationAlert];

    NSString* dt = [[[[deviceToken description]
                      stringByReplacingOccurrencesOfString: @"<" withString: @""]
                     stringByReplacingOccurrencesOfString: @">" withString: @""]
                    stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    [VAUserDefaultsHelper setDeviceToken:dt];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kUserDidRegisterForPushNotifications object:nil];
    
    
    if (dt) {
        [[VAUserApiManager alloc] postDeviceToken:dt withCompletion:^(NSError *error, VAModel *model) {
            
        }];
    }
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kUserHasSeenNotificationAlert];
    
    if (error) {
#if TARGET_IPHONE_SIMULATOR
        NSLog(@"Push notifications unavailable in simulator.");
#else
        UIViewController *vc = [VAControlTool topScreenController];
        [[VANotificationMessage currentMessageController] showTopNotificationMessageWithTitle:error.localizedDescription onController:vc];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[VANotificationMessage currentMessageController] hideCurrentMessage];
        });
#endif
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kShouldShowNotificationBadge];
    [[NSNotificationCenter defaultCenter] postNotificationName:kShowBadgeForMenuIcon object:nil];
    
    application.applicationIconBadgeNumber = [[userInfo valueForKey:@"badge"] integerValue];
    
    UIApplicationState state = [application applicationState];
    
    if (state == UIApplicationStateActive) {
        if ([[userInfo objectForKey:@"type"] isEqualToString:@"user.block"]) {
            [[VAControlTool defaultControl] logoutForBlockedUser];
        } else if ([[userInfo objectForKey:@"type"] isEqualToString:@"user.delete"]) {
            [[VAControlTool defaultControl] logout];
        } else if ([[userInfo objectForKey:@"type"] isEqualToString:@"user.confirm_email"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"kUserEmailConfirmed" object:nil];
        } else {
            VAPushNotificationView *view = [[[NSBundle mainBundle] loadNibNamed:@"VAPushNotificationView" owner:self options:nil] lastObject];
            NSLog(@"PUSH USER INFO: %@", userInfo);
            view.postID = [userInfo valueForKey:@"postId"];
            NSString *friendId = userInfo[@"friendId"];
            NSString *actorNickname = userInfo[@"actor"][@"nickname"];
            if (friendId && actorNickname) {
                NSLog(@"SET NICKNAME: %@", actorNickname);
                view.userNickname = actorNickname;
            }
            [view showPushMessageWithText:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]];
            
            AudioServicesPlaySystemSound(1002);
            AudioServicesPlaySystemSound (kSystemSoundID_Vibrate);
        }
    } else {
        if ([[userInfo objectForKey:@"type"] isEqualToString:@"user.block"]) {
            [[VAControlTool defaultControl] logoutForBlockedUser];
        } else if ([[userInfo objectForKey:@"type"] isEqualToString:@"user.delete"]) {
            [[VAControlTool defaultControl] logout];
        } else if ([[userInfo objectForKey:@"type"] isEqualToString:@"user.confirm_email"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"kUserEmailConfirmed" object:nil];
        } else {
            [self goToCorrectScreen:@{UIApplicationLaunchOptionsRemoteNotificationKey: userInfo}];
        }
    }
    
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)goToCorrectScreen:(NSDictionary *)launchOptions {
    NSDictionary *pushOptions = launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey];
    if (pushOptions) {
        NSLog(@"Push Options: %@", pushOptions);
        NSString *actorNickname = pushOptions[@"actor"][@"nickname"];
        NSString *friendId = pushOptions[@"friendId"];
        NSString *postId = pushOptions[@"postId"];
        if (friendId && actorNickname) {
            [self goToProfileViewControllerWithUserNickname:actorNickname];
        } else if (postId) {
            [self goToPostViewControllerWithPostId:postId];
        } else {
            [self goToEventsViewController];
        }
    } else {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Vicinity" bundle:nil];
        
        VAWelcomeViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"welcomeViewController"];
        self.window.rootViewController = vc;
        [self.window makeKeyAndVisible];
    }
}

- (void)goToProfileViewControllerWithUserNickname:(NSString *)userNickname {
    [[VALocationTool sharedInstance] requestInUseAuthorization];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    VAEventsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"VAEventsViewController"];
    VAProfileViewController *profileVC = [storyboard instantiateViewControllerWithIdentifier:@"profileViewController"];
    profileVC.searchAlias = userNickname;
    profileVC.isUsersAccountMode = YES;
    DEMONavigationController *nav = (DEMONavigationController*)[storyboard instantiateViewControllerWithIdentifier:@"MyVicinityNavigationController"];
    [nav setViewControllers:@[vc, profileVC] animated:NO];
    VAMenuTableViewController *leftMenuController = [storyboard instantiateViewControllerWithIdentifier:@"VAMenuTableViewController"];;
    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:nav
                                                    leftMenuViewController:leftMenuController
                                                    rightMenuViewController:nil];
    container.leftMenuWidth = 125.f;
    container.menuAnimationMaxDuration = container.menuAnimationDefaultDuration;
    leftMenuController.containerViewController = container;
    nav.containerViewController = container;
    
    self.window.rootViewController = container;
    [self.window makeKeyAndVisible];
    
}

- (void)goToPostViewControllerWithPostId:(NSString *)postId {
    [[VALocationTool sharedInstance] requestInUseAuthorization];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    VAEventsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"VAEventsViewController"];
    VACommentsViewController *commentsVC = [storyboard instantiateViewControllerWithIdentifier:@"VACommentsViewController"];
    commentsVC.postID = postId;
    commentsVC.delegate = vc;
    
    DEMONavigationController *nav = (DEMONavigationController*)[storyboard instantiateViewControllerWithIdentifier:@"MyVicinityNavigationController"];
    [nav setViewControllers:@[vc, commentsVC] animated:NO];
    VAMenuTableViewController *leftMenuController = [storyboard instantiateViewControllerWithIdentifier:@"VAMenuTableViewController"];;
    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:nav
                                                    leftMenuViewController:leftMenuController
                                                    rightMenuViewController:nil];
    container.leftMenuWidth = 125.f;
    container.menuAnimationMaxDuration = container.menuAnimationDefaultDuration;
    leftMenuController.containerViewController = container;
    nav.containerViewController = container;
    
    self.window.rootViewController = container;
    [self.window makeKeyAndVisible];
    
}

- (void)goToEventsViewController {
    [[VALocationTool sharedInstance] requestInUseAuthorization];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    VAEventsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"VAEventsViewController"];
    DEMONavigationController *nav = (DEMONavigationController*)[storyboard instantiateViewControllerWithIdentifier:@"MyVicinityNavigationController"];
    [nav setViewControllers:@[vc] animated:NO];
    VAMenuTableViewController *leftMenuController = [storyboard instantiateViewControllerWithIdentifier:@"VAMenuTableViewController"];;
    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:nav
                                                    leftMenuViewController:leftMenuController
                                                    rightMenuViewController:nil];
    container.leftMenuWidth = 125.f;
    container.menuAnimationMaxDuration = container.menuAnimationDefaultDuration;
    leftMenuController.containerViewController = container;
    nav.containerViewController = container;
    
    self.window.rootViewController = container;
    [self.window makeKeyAndVisible];

}

- (void)setUpGoogleMapsInstance {
    [OpenInGoogleMapsController sharedInstance].fallbackStrategy = kGoogleMapsFallbackAppleMaps;
}

@end
