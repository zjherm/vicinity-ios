//
//  VANotificationTableViewCell.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 17.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TTTAttributedLabel.h>

@class VAUser;

@interface VANotificationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *avatarView;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *generalInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIView *separatorView;

@property (strong, nonatomic) VAUser *user;
@end
 