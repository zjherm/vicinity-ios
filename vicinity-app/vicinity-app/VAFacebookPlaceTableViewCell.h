//
//  VAFacebookPlaceTableViewCell.h
//  vicinity-app
//
//  Created by Panda Systems on 10/12/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VAFacebookPlaceTableViewCell : UITableViewCell

- (void)configureWithFacebookPlace:(NSDictionary *)place;

- (void)setDefaultProperties;
- (void)setSelectedProperties;

@end
