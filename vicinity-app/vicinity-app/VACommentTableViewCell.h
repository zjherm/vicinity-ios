//
//  VACommentTableViewCell.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 28.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TTTAttributedLabel.h>

@class VAUser;
@class VAPost;
@protocol VACommentToProfileDelegate;

@interface VACommentTableViewCell : UITableViewCell <TTTAttributedLabelDelegate>
@property (weak, nonatomic) id <VACommentToProfileDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *nameLabel;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *postAndCommentLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *postImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageContainerHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textContainerHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *likesViewContainerHeight;
@property (weak, nonatomic) IBOutlet UIView *separatorView;
@property (weak, nonatomic) IBOutlet UILabel *likesCountLabel;
@property (weak, nonatomic) IBOutlet UIView *likesContainer;
@property (weak, nonatomic) IBOutlet UIButton *showLikesButton;
@property (weak, nonatomic) IBOutlet UIButton *editButton;

@property (strong, nonatomic) VAUser *user;
@property (strong, nonatomic) VAPost *post;

- (CGFloat)heightForCellWithPost:(VAPost *)post;
- (CGFloat)heightForPostContainerWithPost:(VAPost *)post;
@end

@protocol VACommentToProfileDelegate <NSObject>

- (void)shouldShowProfileForUser:(VAUser *)user;

@end