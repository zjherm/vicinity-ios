//
//  VAPhotoEditingViewController.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 26.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VAPhotoProtocol;

@interface VAPhotoEditingViewController : UIViewController
@property (strong, nonatomic) UIViewController *flowController;

@property (strong, nonatomic) UIImage *photo;
@property (weak, nonatomic) id <VAPhotoProtocol> delegate;

@end

@protocol VAPhotoProtocol <NSObject>
- (void)photoHasBeenTaken:(UIImage *)photo landscape:(BOOL)isLandscape;
@end
