//
//  VALiveDealsViewController.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 01.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VALiveDealsViewController.h"
#import "DEMONavigationController.h"
#import "VAFilterViewController.h"
#import "VADealTableViewCell.h"
#import "VADeal.h"
#import "VALocation.h"
#import "VABusiness.h"
#import "VAComment.h"
#import "VAUser.h"
#import "NSDate+VAString.h"
#import "NSNumber+VAMilesDistance.h"
#import "VAControlTool.h"
#import "UITableViewCell+VAParentCell.h"
#import "VALikeListTableViewController.h"
#import "VACommentsViewController.h"
#import "VABusinessProfileViewController.h"
#import "VAUserDefaultsHelper.h"
#import "UIBarButtonItem+Badge.h"
#import "VANotificationTool.h"
#import "VALoaderView.h"
#import "VAVoucherViewController.h"
#import <Google/Analytics.h>
#import "VANotificationMessage.h"
#import "VAUserDefaultsHelper.h"
#import "VAUserApiManager.h"
#import "VAFiltersTool.h"
#import "NSString+VADistance.h"
#import "VADealsResponse.h"

#define dealsLimit 20

typedef NS_ENUM(NSInteger, VALikeAction) {
    VALikeActionLike = 0,
    VALikeActionUnlike
};

@interface VALiveDealsViewController () <VAFilterDelegate, VADealDelegate>
@property (strong, nonatomic) NSArray *dealsArray;
@property (assign, nonatomic) VALikeAction currentLikeAction;
@property (strong, nonatomic) VALoaderView *loader;              //activity indicator for data loading
@property (strong, nonatomic) VALoaderView *refresh;             //loader for refresh control
@property (strong, nonatomic) VALoaderView *footerLoader;        //loader in footer for loading more deals
@property (strong, nonatomic) UIView *navigationAlphaView;
@property (strong, nonatomic) UIView *navigationBlurView;
@property (assign, nonatomic) BOOL isRefreshing;                 //refreshing all posts after pull to refresh
@property (assign, nonatomic) BOOL isLoadingNewDealsBatch;        //loading another batch of posts
@property (assign, nonatomic) BOOL firstBatchHasBeenLoaded;
@property (assign, nonatomic) BOOL allDealsHasBeenLoaded;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menuButton;
@end

@implementation VALiveDealsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dealsArray = [NSArray new];
    
    self.isRefreshing = NO;
    self.isLoadingNewDealsBatch = NO;
    self.firstBatchHasBeenLoaded = NO;
    self.allDealsHasBeenLoaded = NO;
    
    if (!IOS7) {
        [self addBlurNavigationViews];
    }
    
    [self setupRefreshControl];
    
    self.tableView.clipsToBounds = NO;
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(7.f, 0.f, 0.f, -8.f);       //7 is an top inset for heaeder height = 7.f
    
    [self showActivityIndicator];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showButtonBadge) name:kShowBadgeForMenuIcon object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideButtonBadge) name:kHideBadgeForMenuIcon object:nil];

//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [self createDeals];
//        [self hideActivityIndicator];
//        [self.tableView reloadData];
//    });
    
    [self reloadDealsInfo];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.title = @"Live Deals";
    if ([VAUserDefaultsHelper shouldShowNotificationBadge]) {
        [self.menuButton showBadge];
    }
    
    if (!IOS7) {
        [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                      forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [UIImage new];
        self.navigationController.navigationBar.translucent = YES;
    } else {
        [(DEMONavigationController *)self.navigationController setDefaultNavigationBar];
    }
    
//    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName value:@"Live deals screen"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    [self.view bringSubviewToFront:self.navigationBlurView];
    [self.view bringSubviewToFront:self.navigationAlphaView];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([[VALocationTool sharedInstance] isServicesEnabled]) {
        return [self.dealsArray count];
    } else {
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Deal";
    
    VADealTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier];
    VADeal *deal = nil;
    if ([self.dealsArray count] > 0) {
        deal = [self.dealsArray objectAtIndex:indexPath.section];
    }
    
    cell.path = indexPath;
    [cell configureCellWithDeal:deal];
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 7.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Deal";
    
    static VADealTableViewCell *dealCell = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dealCell = [self.tableView dequeueReusableCellWithIdentifier:identifier];
    });
    dealCell.path = indexPath;
    
    VADeal *deal = [self.dealsArray objectAtIndex:indexPath.section];
    
    return [dealCell heightForCellWithDeal:deal];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 7)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//    NSIndexPath *startRequestIndexPath = [NSIndexPath indexPathForRow:0 inSection:[self.postsArray count] - 5];
//    
//    if ([indexPath isEqual:startRequestIndexPath]  && self.firstBatchHasBeenLoaded && !self.allPostsHasBeenLoaded) {
//        self.isLoadingNewPostBatch = YES;
//        [self makePostRequest];
//    }
//}

//- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath  {
//    self.firstBatchHasBeenLoaded = YES;
//}

#pragma mark - Actions

- (IBAction)showMenu:(id)sender {
    [((DEMONavigationController*)self.navigationController) showMenu];
}

- (IBAction)showFilters:(UIBarButtonItem *)sender {
    VAFilterViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"VAFilterViewController"];
    vc.hideCategories = NO;
    vc.delegate = self;
    if (IOS7) {
        vc.modalPresentationStyle = UIModalPresentationCurrentContext;
    } else if (IOS8 || IOS9) {
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    }
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)getDeal:(UIButton *)sender {
    VADealTableViewCell *cell = (VADealTableViewCell *)[UITableViewCell findParentCellOfView:sender];
    VADeal *deal = [self dealForCellWithPushedButton:sender];
    if (deal.dealType == VADealTypePaid && deal.isPurchased.boolValue == NO) {
        deal.isInConfirmPurchaseMode = YES;
        [self showPurchaseConfirmationForCell:cell];
    } else if (deal.dealType == VADealTypePaid && deal.isPurchased.boolValue == YES) {
        [self showVoucherForDeal:deal];
    } else if (deal.dealType == VADealTypeFree && deal.isPurchased.boolValue == NO) {
        [sender setTitle:@"DEAL SAVED - VIEW" forState:UIControlStateNormal];
        deal.isPurchased = [NSNumber numberWithBool:YES];
    } else if (deal.dealType == VADealTypeFree && deal.isPurchased.boolValue == YES) {
        [self showVoucherForDeal:deal];
    }

//    [self.tableView scrollToRowAtIndexPath:[self.tableView indexPathForCell:cell] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

- (IBAction)cancelPurchase:(UIButton *)sender {
    VADealTableViewCell *cell = (VADealTableViewCell *)[UITableViewCell findParentCellOfView:sender];
    VADeal *deal = [self dealForCellWithPushedButton:sender];
    deal.isInConfirmPurchaseMode = NO;
    [self.view removeConstraint:cell.bottomConfirmationViewConstraint];
    deal.isInConfirmPurchaseMode = NO;
    [self cancelPurchaseForCell:cell];
}

- (IBAction)confirmPurchase:(UIButton *)sender {
    VADealTableViewCell *cell = (VADealTableViewCell *)[UITableViewCell findParentCellOfView:sender];
    VADeal *deal = [self dealForCellWithPushedButton:sender];
    [cell.getDealButton setTitle:@"PURCHASED - VIEW DEAL" forState:UIControlStateNormal];
    deal.isInConfirmPurchaseMode = NO;
    deal.isPurchased = [NSNumber numberWithBool:YES];
    [self.view removeConstraint:cell.bottomConfirmationViewConstraint];
    [self finishPurchaseForCell:cell];
}

#pragma mark - Deal Actions

- (IBAction)showLikesToDealWithButton:(UIButton *)sender {
    VADeal *deal = [self dealForCellWithPushedButton:sender];
    
    VALikeListTableViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"VALikeListTableViewController"];
    vc.postID = deal.dealID;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)showCommentsToDealWithButton:(UIButton *)sender {
    VADeal *deal = [self dealForCellWithPushedButton:sender];
    
    VACommentsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"VACommentsViewController"];
//    vc.post = deal;
//    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)likeThisDeal:(UIButton *)sender {
    VADeal *deal = [self dealForCellWithPushedButton:sender];
    VADealTableViewCell *cell = (VADealTableViewCell *)[UITableViewCell findParentCellOfView:sender];
    
//    VAUserApiManager *manager = [VAUserApiManager new];
    
    if ([deal.isLiked boolValue] == NO) {
        self.currentLikeAction = VALikeActionLike;
        deal.isLiked = [NSNumber numberWithBool:YES];
        deal.likesAmount = @([deal.likesAmount integerValue] + 1);
        [self performLikeForCell:cell andDeal:deal];

//        [manager postLikePostWithID:post.postID withCompletion:^(NSError *error, VAModel *model) {
//            if (error) {
//                [self performUnlikeForCell:cell andPost:post];
//            } else if (model) {
//                VAServerResponse *response = (VAServerResponse *)model;
//                if ([response.status isEqualToString:kSuccessResponseStatus]) {
//                    post.isLiked = [NSNumber numberWithBool:YES];
//                    post.likesAmount = [NSNumber numberWithInteger:[cell.likeLabel.text integerValue]];
//                } else if ([response.status isEqualToString:kErrorResponseStatus]) {
//                    [self performUnlikeForCell:cell andPost:post];
//                }
//            }
//        }];
    } else {
        self.currentLikeAction = VALikeActionUnlike;
        deal.isLiked = [NSNumber numberWithBool:NO];
        deal.likesAmount = @([deal.likesAmount integerValue] - 1);
        [self performUnlikeForCell:cell andDeal:deal];
//        [manager deleteLikePostWithID:post.postID withCompletion:^(NSError *error, VAModel *model) {
//            if (error) {
//                [self performLikeForCell:cell andPost:post];
//            } else if (model) {
//                VAServerResponse *response = (VAServerResponse *)model;
//                if ([response.status isEqualToString:kSuccessResponseStatus]) {
//                    post.isLiked = [NSNumber numberWithBool:NO];
//                    post.likesAmount = [NSNumber numberWithInteger:[cell.likeLabel.text integerValue]];
//                } else if ([response.status isEqualToString:kErrorResponseStatus]) {
//                    [self performLikeForCell:cell andPost:post];
//                }
//            }
//        }];
    }
    
}

#pragma mark - Like and Unlike Methods

- (void)performLikeForCell:(VADealTableViewCell *)cell andDeal:(VADeal *)deal {
    cell.likeImageView.image = [UIImage imageNamed:@"isLiked-red.png"];
    cell.likeLabel.text = [NSString stringWithFormat:@"%ld", (long)[deal.likesAmount integerValue]];
    if (self.currentLikeAction == VALikeActionUnlike) {
//        [VANotificationMessage showLikeErrorMessageOnController:self withDuration:5.f];
    }
    
    cell.likesAndCommentsContainerHeight.constant = 24.f;
    [cell.likeView setHidden:NO];
    [UIView animateWithDuration:0.3f animations:^{
        [cell layoutIfNeeded];
    }];
    
    if ([deal.likesAmount integerValue] == 1) {          //it's ammount after our's like, before like it was 0
        [self.tableView beginUpdates];
        [self.tableView endUpdates];
    }
}

- (void)performUnlikeForCell:(VADealTableViewCell *)cell andDeal:(VADeal *)deal{
    cell.likeImageView.image = [UIImage imageNamed:@"isUnliked.png"];
    cell.likeLabel.text = [NSString stringWithFormat:@"%ld", (long)[deal.likesAmount integerValue]];
    if (self.currentLikeAction == VALikeActionLike) {
//        [VANotificationMessage showLikeErrorMessageOnController:self withDuration:5.f];
    }
    
    if ([deal.likesAmount integerValue] == 0) {
        [cell.likeView setHidden:YES];
    }
    
    if ([deal.likesAmount integerValue] == 0 && [deal.commentsAmount integerValue] < 3) {
        cell.likesAndCommentsContainerHeight.constant = 0.f;
        
        [UIView animateWithDuration:0.3f animations:^{
            [cell layoutIfNeeded];
        }];
        
        [self.tableView beginUpdates];
        [self.tableView endUpdates];
    }
}

#pragma mark - Methods

- (void)createDeals {
    
    VADeal *deal1 = [VADeal new];
    VALocation *location1 = [VALocation new];
    location1.cityName = @"La Jolla";
    location1.distance = [NSNumber numberWithInteger:160];
//    deal1.location = location1;
    VABusiness *business1 = [VABusiness new];
    business1.name = @"Bonfide Restaurant";
    business1.avatarURL = @"deal0";
    business1.businessID = @"55e453848ebf29144de80cb4";
    deal1.business = business1;
    deal1.text = @"Dinner is served. $20 for $45 dollars tonight. Grib deal fast!";
    deal1.shortDescription = @"$20 for $45 at Bonfide Restaurant";
    deal1.date = [NSDate dateWithTimeIntervalSinceNow:-25 * 60];
    deal1.photoURL = @"profile-avatar";
    deal1.likesAmount = [NSNumber numberWithInteger:0];
    deal1.commentsAmount = [NSNumber numberWithInteger:0];
    deal1.commentsArray = @[];
    deal1.isLiked = [NSNumber numberWithBool:NO];
    deal1.isPurchased = [NSNumber numberWithBool:NO];
    deal1.dealType = VADealTypePaid;
    deal1.serialNumber = @"32872-01";
    deal1.expirationDate = [NSDate dateWithTimeIntervalSinceNow:4 * 24 * 60 * 60];
    
    VADeal *deal2 = [VADeal new];
    VALocation *location2 = [VALocation new];
    location2.cityName = @"La Jolla";
    location2.distance = [NSNumber numberWithInteger:800];
//    deal2.location = location2;
    VABusiness *business2 = [VABusiness new];
    business2.name = @"Estanica Hotel $ Spa";
    business2.businessID = @"55d6fcb95aedfde873f341fd";
    deal2.business = business2;
    deal2.text = @"We've got walk in openings at our Spa for an amazing";
    deal2.date = [NSDate dateWithTimeIntervalSinceNow:-2 * 60 * 60];
    deal2.photoURL = @"111";
    deal2.likesAmount = [NSNumber numberWithInteger:3];
    deal2.commentsAmount = [NSNumber numberWithInteger:5];
    VAComment *comment1 = [VAComment new];
    VAUser *user1 = [VAUser new];
    user1.nickname = @"mathew.J";
    comment1.user = user1;
    comment1.text = @"Love this! Awesome!";
    VAComment *comment2 = [VAComment new];
    VAUser *user2 = [VAUser new];
    user2.nickname = @"carrie_D";
    comment2.user = user2;
    comment2.text = @"Nice deals, guys!";
    deal2.commentsArray = @[comment1, comment2];
    deal2.isLiked = [NSNumber numberWithBool:NO];
    deal2.isPurchased = [NSNumber numberWithBool:NO];
    deal2.dealType = VADealTypeAd;
    
    VADeal *deal3 = [VADeal new];
    VALocation *location3 = [VALocation new];
    location3.cityName = @"La Jolla";
    location3.distance = [NSNumber numberWithInteger:1050];
//    deal3.location = location3;
    VABusiness *business3 = [VABusiness new];
    business3.name = @"Joe's Bar";
    business3.avatarURL = @"deal2";
    deal3.business = business3;
    deal3.text = @"Come stop by! For the next two hours we are doing half off all";
    deal3.date = [NSDate dateWithTimeIntervalSinceNow:-5 * 60 * 60];
    deal3.photoURL = @"image2";
    deal3.likesAmount = [NSNumber numberWithInteger:22];
    deal3.commentsAmount = [NSNumber numberWithInteger:0];
    deal3.commentsArray = @[];
    deal3.isLiked = [NSNumber numberWithBool:YES];
    deal3.isPurchased = [NSNumber numberWithBool:NO];
    deal3.dealType = VADealTypeFree;
    deal3.shortDescription = @"$20 for 30 minutes massage until 4pm";
    deal3.serialNumber = @"12467-09";
    deal3.expirationDate = [NSDate dateWithTimeIntervalSinceNow:2 * 24 * 60 * 60];
    
//    for (int i = 0; i < 22; i++) {
//        [[self mutableArrayValueForKey:@"dealsArray"] addObjectsFromArray:@[deal1, deal2, deal3]];
//
//    }
    [[self mutableArrayValueForKey:@"dealsArray"] addObjectsFromArray:@[deal1, deal2, deal3]];

}

- (VADeal *)dealForCellWithPushedButton:(UIButton *)button {
    VADealTableViewCell *cell = (VADealTableViewCell *)[UITableViewCell findParentCellOfView:button];
    NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
    VADeal *deal = [self.dealsArray objectAtIndex:cellIndexPath.section];
    return deal;
}

- (void)showPurchaseConfirmationForCell:(VADealTableViewCell *)cell {
    [self addBottomConfirmationViewConstraintForCell:cell];
    
    [self.tableView beginUpdates];
    cell.confirmationViewHeight.priority = 250.f;
    cell.confirmationView.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        [cell layoutIfNeeded];
    }];
    [self.tableView endUpdates];
}

- (void)finishPurchaseForCell:(VADealTableViewCell *)cell {
    [self.tableView beginUpdates];
    cell.confirmationViewHeight.priority = 900.f;
    cell.confirmationView.hidden = YES;
    [UIView animateWithDuration:0.3f animations:^{
        [cell layoutIfNeeded];
    }];
    [self.tableView endUpdates];
}

- (void)cancelPurchaseForCell:(VADealTableViewCell *)cell {
    [self.tableView beginUpdates];
    cell.confirmationViewHeight.priority = 900.f;
    cell.confirmationView.hidden = YES;
    [UIView animateWithDuration:0.3f animations:^{
        [cell layoutIfNeeded];
    }];
    [self.tableView endUpdates];
}

- (void)showButtonBadge {
    [self.menuButton showBadge];
}

- (void)hideButtonBadge {
    [self.menuButton hideBadge];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kShouldShowNotificationBadge];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void)showActivityIndicator {
    if (self.loader) {
        [self hideActivityIndicator];
    }
    VALoaderView *loader = [VALoaderView initLoader];
    loader.frame = CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), kLoaderHeight);
    [loader startAnimating];
    self.tableView.tableHeaderView = loader;
    self.loader = loader;
    self.tableView.userInteractionEnabled = NO;
}

- (void)hideActivityIndicator {
    [self.loader stopAnimating];
    self.tableView.tableHeaderView = nil;
    self.loader = nil;
    self.tableView.userInteractionEnabled = YES;
}

- (void)addBottomConfirmationViewConstraintForCell:(VADealTableViewCell *)cell {
    NSLayoutConstraint *bottomConstraint =[NSLayoutConstraint
                                           constraintWithItem:cell
                                           attribute:NSLayoutAttributeBottom
                                           relatedBy:NSLayoutRelationEqual
                                           toItem:cell.confirmationView
                                           attribute:NSLayoutAttributeBottom
                                           multiplier:1.0
                                           constant:0];
    
    cell.bottomConfirmationViewConstraint = bottomConstraint;
    
    [self.view addConstraint:bottomConstraint];
}

- (void)showVoucherForDeal:(VADeal *)deal {
    VAVoucherViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"VAVoucherViewController"];
    vc.isViewVoucherMode = YES;
    vc.deal = deal;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)addBlurNavigationViews {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 64)];
    view.backgroundColor = [[VADesignTool defaultDesign] vicinityBlue];
    [self.view addSubview:view];
    self.navigationAlphaView = view;
    
    UIView *blurView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 64)];
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
    effectView.frame = blurView.bounds;
    [blurView addSubview:effectView];
    
    self.navigationBlurView = blurView;
    
    [self.view addSubview:blurView];
}

- (void)reloadDealsInfo {
    self.tableView.scrollEnabled = YES;
    if ([[VANotificationMessage currentMessageController] notificationMessageIsShown]) {
        [[VANotificationMessage currentMessageController] hideCurrentMessage];
    }
    if (![[VALocationTool sharedInstance] isServicesEnabled]) {
        self.tableView.scrollEnabled = NO;
        [self showNoLocationMessage];
    } else if (![VAUserDefaultsHelper locationHaveBeenSaved]){
        [[VALocationTool sharedInstance] getCurrentDeviceLocationWithComplition:^(NSError *error, CLLocation *location) {
            if (error) {
                [[VANotificationMessage currentMessageController] showTopNotificationMessageWithTitle:error.localizedDescription onController:self];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [[VANotificationMessage currentMessageController] hideCurrentMessage];
                });
            } else if (location) {
                double lat = location.coordinate.latitude;
                double lon = location.coordinate.longitude;
                [VAUserDefaultsHelper savelatitude:lat andLongitude:lon];
                
                [self makeDealsRequest];
            }
        }];
    } else {
        [self makeDealsRequest];
    }
}

- (void)makeDealsRequest {
    
    self.tableView.scrollEnabled = YES;
    
    if (!self.isRefreshing && !self.isLoadingNewDealsBatch) {      //first post batch loading
        [self showActivityIndicator];
    } else if (self.isLoadingNewDealsBatch) {
        VALoaderView *loader = [VALoaderView initLoader];
        self.footerLoader = loader;
        loader.frame = CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), kLoaderHeight);
        [loader startAnimating];
        self.tableView.tableFooterView = loader;
    }
    
    VAUserApiManager *manager = [VAUserApiManager new];
    NSString *milesString = [[VAFiltersTool currentFilters] loadDistance];
    if (!milesString) {
        milesString = @"10 miles";
    }
    CGFloat distance = [milesString metersFromMilesString];
    
    NSNumber *batchOffset = nil;
    if (self.isRefreshing) {
        batchOffset = [NSNumber numberWithInteger:0];
    } else {
        batchOffset = [NSNumber numberWithInteger:[self.dealsArray count]];
    }
    
    NSNumber *limit = [NSNumber numberWithInteger:dealsLimit];
    
    NSNumber *lat = ((NSNumber *)[[NSUserDefaults standardUserDefaults] valueForKey:kLatValue]);
    NSNumber *lon = ((NSNumber *)[[NSUserDefaults standardUserDefaults] valueForKey:kLonValue]);
    
    [manager getDealsWithLimit:limit
                        offset:batchOffset
                      latitude:lat
                     longitude:lon
                      distance:[NSNumber numberWithFloat:distance]
                 andCompletion:^(NSError *error, VAModel *model) {
                     if (model) {
                         if (!self.isRefreshing && !self.isLoadingNewDealsBatch) {           //first batch loading
                             [self hideActivityIndicator];
                         } else if (self.isLoadingNewDealsBatch) {                           //second, third etc... batch loading
                             [self.footerLoader stopAnimating];
                             self.tableView.tableFooterView = nil;
                             self.isLoadingNewDealsBatch = NO;
                         } else {                                                           //pull to refresh have been used
                             [self.refresh stopRefreshing];
                             self.isRefreshing = NO;
                             [[self mutableArrayValueForKey:@"dealsArray"] removeAllObjects];
                             self.allDealsHasBeenLoaded = NO;
                         }
                         VADealsResponse *response = (VADealsResponse *)model;
                         if ([response.status isEqualToString:kErrorResponseStatus]) {
                             
                         } else {
                             [[self mutableArrayValueForKey:@"dealsArray"] addObjectsFromArray:response.deals];
                             [self.tableView reloadData];
                             
                             if ([self.dealsArray count] == 0) {
                                 [self showNoDealsView];
                             } else {
                                 [self hideNoDealsView];
                             }
                         }
                         
                         if ([response.deals count] < dealsLimit) {                          //all post have been loaded
                             self.allDealsHasBeenLoaded = YES;
                         }
                     } else if (error) {
                         if (!self.isRefreshing && !self.isLoadingNewDealsBatch) {
                             [self hideActivityIndicator];
                         } else if (self.isLoadingNewDealsBatch) {
                             [self.footerLoader stopAnimating];
                             self.tableView.tableFooterView = nil;
                             self.isLoadingNewDealsBatch = NO;
                         } else {
                             [self.refresh stopRefreshing];
                             self.isRefreshing = NO;
                         }
                         
                         [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
                     }
                 }];
}

- (void)showNoLocationMessage {
    [VANotificationMessage showTurnOnLocationMessageOnController:self withDuration:0.f];
}

- (void)showNoDealsView {
    
}

- (void)hideNoDealsView {
    
}

#pragma mark - Refresh Control

- (void)setupRefreshControl {
    VALoaderView *refresh = [VALoaderView initLoader];
    self.refresh = refresh;
    [refresh setFrame:[self frameForCustomRefreshControl]];
    refresh.alpha = 0.f;
    
    [refresh addTarget:self
                action:@selector(refreshData)
      forControlEvents:UIControlEventValueChanged];
    
    [self.tableView addSubview:refresh];
}

- (CGRect)frameForCustomRefreshControl {
    CGFloat customRefreshControlHeight = kLoaderHeight;
    CGFloat customRefreshControlWidth = CGRectGetWidth([self.tableView frame]);
    CGRect customRefreshControlFrame = CGRectMake(0.0f,
                                                  -customRefreshControlHeight,
                                                  customRefreshControlWidth,
                                                  customRefreshControlHeight);
    return customRefreshControlFrame;
}

- (void)refreshData {
    [self.refresh startRefreshing];
//    self.isRefreshing = YES;
//    [self reloadPostInfo];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.refresh stopRefreshing];
    });
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self.refresh containingScrollViewDidEndDragging:scrollView];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.refresh containingScrollViewDidScroll:scrollView];
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    if (scrollView.contentOffset.y > 0) {
        [UIView animateWithDuration:0.5f animations:^{
            self.navigationAlphaView.alpha = 0.55f;
        }];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [UIView animateWithDuration:0.5f animations:^{
        self.navigationAlphaView.alpha = 1.f;
    }];
    
}


#pragma mark - VAFilterDelegate

- (void)userDidChangeFilters {
    
}

#pragma mark - VADealDelegate

- (void)shouldShowProfileForBusiness:(VABusiness *)business {
    VABusinessProfileViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"VABusinessProfileViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
