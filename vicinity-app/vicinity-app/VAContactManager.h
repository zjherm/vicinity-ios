//
//  VAContactManager.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/23/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>

@interface VAContactManager : NSObject

+ (NSMutableArray *)getAllContacts;

+ (void)askContactsPermissionsWithComplition:(void (^)(BOOL granted))comlition;

@end
