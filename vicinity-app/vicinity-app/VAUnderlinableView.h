//
//  VAUnderlinableView.h
//  vicinity-app
//
//  Created by vadzim vasileuski on 3/28/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VAUnderlinableView : UIView

- (void)drawUnderlineWithColor:(UIColor *)color width:(CGFloat)width;
- (void)removeUnderline;

- (BOOL)isUnderlinded;

@end
