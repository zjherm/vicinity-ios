//
//  VABlockedUserViewController.m
//  vicinity-app
//
//  Created by Panda Systems on 11/25/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VABlockedUserViewController.h"

#import "DEMONavigationController.h"
#import "VALoginViewController.h"

@interface VABlockedUserViewController ()

@end

@implementation VABlockedUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [(DEMONavigationController *)self.navigationController setDefaultNavigationBar];
    [(DEMONavigationController *)self.navigationController setTitle:@"Blocked"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actionss

- (IBAction)backButtonTouchUp:(id)sender {
    [self showLoginScreen];
}

#pragma mark - Navigation

- (void)showLoginScreen {
    VALoginViewController *vc = [[UIStoryboard storyboardWithName:@"LoginFlow" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"VALoginViewController"];
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:vc animated:YES completion:nil];
}

@end
