//
//  VAConfirmEmailViewController.m
//  vicinity-app
//
//  Created by Panda Systems on 2/10/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import "VAConfirmEmailViewController.h"

#import "DEMONavigationController.h"
#import "VAEventsViewController.h"
#import "VAMenuTableViewController.h"

#import "VAUserDefaultsHelper.h"
#import "VANotificationMessage.h"
#import "VAUserApiManager.h"
#import "VALoginResponse.h"
#import "VAUser.h"

NSString *const kSignUpMessageText = @"Check your email to confirm address. Once confirmed you will be automatically signed in.";
NSString *const kUpdateEmailMessageText = @"Please check your email to confirm email address. Once confirmed you will be automatically returned to your profile.";
NSString *const kSignUpInfoText = @"If incorrect you may go back to edit your email address.";
NSString *const kUpdateEmailInfoText = @"If this is incorrect you may go back to revert to your original email address. You can reattempt to change your email address after going back.";


@interface VAConfirmEmailViewController ()

@property (nonatomic, weak) IBOutlet UILabel *messageLabel;
@property (nonatomic, weak) IBOutlet UILabel *noteLabel;
@property (nonatomic, weak) IBOutlet UILabel *infoLabel;

@end

@implementation VAConfirmEmailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavigationBar];
    [self setupLabels];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //[self setupNavigationBar];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUserProfile) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUserProfile) name:@"kUserEmailConfirmed" object:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup UI

- (void)setupNavigationBar {
    self.navigationItem.title = @"Verify Info";
    UIImage *backImage = [UIImage imageNamed:@"BackButton"];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:backImage style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonPressed:)];
    leftItem.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = leftItem;
}

- (void)setupLabels {
    self.messageLabel.text = self.isEmailUpdated ? kUpdateEmailMessageText : kSignUpMessageText;
    self.noteLabel.attributedText = [self noteString];
    self.infoLabel.text = self.isEmailUpdated ? kUpdateEmailInfoText : kSignUpInfoText;
}

#pragma mark - Actions

- (void)cancelButtonPressed:(id)sender {
    // TODO: show loader
    VAUserApiManager *manager = [[VAUserApiManager alloc] init];
    [manager getUserProfileWithCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            [VANotificationMessage showNetworkErrorMessageOnController:self withDuration:5.0f];
        } else {
            VAUser *user = (VAUser *)model;
            [VAUserDefaultsHelper saveModel:user forKey:VASaveKeys_CurrentUser];
            if ([user.isEmailConfirmed boolValue]) {
                if (self.isEmailUpdated) {
                    if (self.delegate) {
                        [self.delegate didConfirmEmailForUser:user];
                    }
                    [self.navigationController popViewControllerAnimated:YES];
                } else {
                    [self showMyVicinityScreen];
                }
            } else {
                if (self.isEmailUpdated) {
                    VAUserApiManager *manager = [[VAUserApiManager alloc] init];
                    [manager restoreOriginalEmailWithCompletion:^(NSError *error, VAModel *model) {
                        if (error) {
                            [VANotificationMessage showNetworkErrorMessageOnController:self withDuration:5.0f];
                        } else {
                            VALoginResponse *response = (VALoginResponse *)model;
                            VAUser *user = response.loggedUser;
                            [VAUserDefaultsHelper saveModel:user forKey:VASaveKeys_CurrentUser];
                            if (self.delegate) {
                                [self.delegate didRestoreOriginalEmailForUser:user];
                            }
                            [self.navigationController popViewControllerAnimated:YES];
                        }
                    }];
                } else {
                    VAUserApiManager *manager = [[VAUserApiManager alloc] init];
                    VAUser *me = [[VAUserDefaultsHelper me] copy];
                    [manager removeUserWithId:me.userId withCompletion:^(NSError *error, VAModel *model) {
                        if (error) {
                            [VANotificationMessage showNetworkErrorMessageOnController:self withDuration:5.0f];
                        } else {
                            [VAUserDefaultsHelper saveModel:nil forKey:VASaveKeys_CurrentUser];
                            if (self.delegate) {
                                [self.delegate didRestoreOriginalEmailForUser:me];
                            }
                            [self.navigationController popViewControllerAnimated:YES];
                        }
                    }];
                }
            }
        }
    }];
}

#pragma mark - Helpers

- (void)getUserProfile {
    VAUserApiManager *manager = [[VAUserApiManager alloc] init];
    [manager getUserProfileWithCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            [VANotificationMessage showNetworkErrorMessageOnController:self withDuration:5.0f];
        } else {
            VAUser *user = (VAUser *)model;
            [VAUserDefaultsHelper saveModel:user forKey:VASaveKeys_CurrentUser];
            if ([user.isEmailConfirmed boolValue]) {
                if (self.isEmailUpdated) {
                    if (self.delegate) {
                        [self.delegate didConfirmEmailForUser:user];
                    }
                    [self.navigationController popViewControllerAnimated:YES];
                } else {
                    [self showMyVicinityScreen];
                }
            }
        }
    }];
}

- (NSAttributedString *)noteString {
    NSMutableAttributedString *noteString = [[NSMutableAttributedString alloc] initWithString:@"Note: You've entered "
                                                                                   attributes:@{
                                                                                                NSForegroundColorAttributeName: [UIColor whiteColor],
                                                                                                NSFontAttributeName: [UIFont fontWithName:@"Avenir-light" size:13.0f]
                                                                                                }];
    [noteString appendAttributedString:[[NSAttributedString alloc] initWithString:self.isEmailUpdated ? [VAUserDefaultsHelper me].pendingEmail : [VAUserDefaultsHelper me].email
                                                                       attributes:@{
                                                                                    NSForegroundColorAttributeName: [UIColor whiteColor],
                                                                                    NSFontAttributeName: [UIFont fontWithName:@"Avenir-Heavy" size:13.0f]
                                                                                    }]];
    return noteString;
}

- (void)showMyVicinityScreen {
    [[VALocationTool sharedInstance] requestInUseAuthorization];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    VAEventsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"VAEventsViewController"];
    DEMONavigationController *nav = (DEMONavigationController*)[storyboard instantiateViewControllerWithIdentifier:@"MyVicinityNavigationController"];
    [nav setViewControllers:@[vc] animated:NO];
    VAMenuTableViewController *leftMenuController = [storyboard instantiateViewControllerWithIdentifier:@"VAMenuTableViewController"];;
    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:nav
                                                    leftMenuViewController:leftMenuController
                                                    rightMenuViewController:nil];
    container.leftMenuWidth = 125.f;
    container.menuAnimationMaxDuration = container.menuAnimationDefaultDuration;
    leftMenuController.containerViewController = container;
    nav.containerViewController = container;
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    window.rootViewController = container;
    [window makeKeyAndVisible];
}

@end
