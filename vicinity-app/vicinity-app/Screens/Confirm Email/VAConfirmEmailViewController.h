//
//  VAConfirmEmailViewController.h
//  vicinity-app
//
//  Created by Panda Systems on 2/10/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VAUser;

@protocol VAConfirmEmailDelegate <NSObject>
@optional
- (void)didRestoreOriginalEmailForUser:(VAUser *)user;
- (void)didConfirmEmailForUser:(VAUser *)user;
@end

@interface VAConfirmEmailViewController : UIViewController

@property (nonatomic, weak) id<VAConfirmEmailDelegate> delegate;
@property (nonatomic) BOOL isEmailUpdated;

@end
