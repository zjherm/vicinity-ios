//
//  VAProfileActionsView.m
//  vicinity-app
//
//  Created by Panda Systems on 11/11/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAProfileActionsView.h"

#import "VAUser.h"

@interface VAProfileActionsView ()

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *topSeparatorViewHeightConstraint;

@property (nonatomic, weak) IBOutlet UIView *connectView;
@property (nonatomic, weak) IBOutlet UIView *disconnectView;
@property (nonatomic, weak) IBOutlet UIView *messageView;
@property (nonatomic, weak) IBOutlet UIView *reportView;
@property (nonatomic, weak) IBOutlet UIView *blockView;
@property (nonatomic, weak) IBOutlet UIView *blockedReportView;
@property (nonatomic, weak) IBOutlet UIView *unblockView;

@end

@implementation VAProfileActionsView

- (void)awakeFromNib {
    self.topSeparatorViewHeightConstraint.constant = 0.5f;
}

#pragma mark - Properties

- (void)setUser:(VAUser *)user {
    _user = user;
    
    [self updateUI];
}

#pragma mark - Update UI

- (void)updateUI {
    self.connectView.hidden = [self.user.isFriend boolValue] || [self.user.isBlocked boolValue];
    self.disconnectView.hidden = ![self.user.isFriend boolValue] || [self.user.isBlocked boolValue];
    
    self.messageView.hidden = [self.user.isBlocked boolValue];
    self.reportView.hidden = [self.user.isBlocked boolValue];
    self.blockedReportView.hidden = ![self.user.isBlocked boolValue];
    
    self.blockView.hidden = [self.user.isBlocked boolValue];
    self.unblockView.hidden = ![self.user.isBlocked boolValue];
}

#pragma mark - Actions

- (IBAction)connectButtonTouchUp:(id)sender {
    if (self.delegate) {
        [self.delegate connectUserSelected:self.user];
    }
}

- (IBAction)disconnectButtonTouchUp:(id)sender {
    if (self.delegate) {
        [self.delegate disconnectUserSelected:self.user];
    }
}

- (IBAction)messageButtonTouchUp:(id)sender {
    if (self.delegate) {
        [self.delegate messageToUserSelected:self.user];
    }
}

- (IBAction)reportButtonTouchUp:(id)sender {
    if (self.delegate) {
        [self.delegate reportAboutUserSelected:self.user];
    }
}

- (IBAction)blockButtonTouchUp:(id)sender {
    if (self.delegate) {
        [self.delegate blockUserSelected:self.user];
    }
}

- (IBAction)unblockButtonTouchUp:(id)sender {
    if (self.delegate) {
        [self.delegate unblockUserSelected:self.user];
    }
}

@end
