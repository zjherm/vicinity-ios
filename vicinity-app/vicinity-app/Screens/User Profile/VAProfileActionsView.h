//
//  VAProfileActionsView.h
//  vicinity-app
//
//  Created by Panda Systems on 11/11/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VAUser;

@protocol VAUserActionsDelegate <NSObject>

- (void)connectUserSelected:(VAUser *)user;
- (void)disconnectUserSelected:(VAUser *)user;
- (void)messageToUserSelected:(VAUser *)user;
- (void)reportAboutUserSelected:(VAUser *)user;
- (void)blockUserSelected:(VAUser *)user;
- (void)unblockUserSelected:(VAUser *)user;

@end

@interface VAProfileActionsView : UIView

@property (nonatomic, weak) id <VAUserActionsDelegate> delegate;

@property (nonatomic, strong) VAUser *user;

@end
