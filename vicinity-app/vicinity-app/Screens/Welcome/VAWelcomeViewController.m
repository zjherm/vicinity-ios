//
//  VAWelcomeViewController.m
//  vicinity-app
//
//  Created by Panda Systems on 11/25/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAWelcomeViewController.h"

#import "VAUserDefaultsHelper.h"
#import "VAUserApiManager.h"
#import "VAUser.h"
#import "VAUserProfileResponse.h"
#import "VALocationTool.h"
#import "VAControlTool.h"

#import "DEMONavigationController.h"
#import "VAMenuTableViewController.h"
#import "VALoginViewController.h"
#import "VAEventsViewController.h"
#import "VABlockedUserViewController.h"
#import "VAProfileViewController.h"
#import "VASignUpViewController.h"
#import "VAConfirmEmailViewController.h"
#import "VASignUpNewViewController.h"
#import "VACreateAccountViewController.h"
#import "VAEnterUsernameViewController.h"

@interface VAWelcomeViewController ()
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;
@end

@implementation VAWelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadUserData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Load data

- (void)loadUserData {
    [self.activityIndicator startAnimating];
    
    [[VAUserApiManager alloc] getUserProfileWithCompletion:^(NSError *error, VAModel *model) {
        [self .activityIndicator stopAnimating];
        if (model) {
            VAUser *user = (VAUser *)model;
            if (user) {
                [VAUserDefaultsHelper saveModel:user forKey:VASaveKeys_CurrentUser];
                [self showScreenForUser:user];
            } else {
                [self showLoginScreen];
            }
        } else if (error) {
            if ([error.userInfo[NSLocalizedDescriptionKey] integerValue] == 100019) {
//                [self showBlockedUserScreen];
            } else if ([error.userInfo[NSLocalizedDescriptionKey] integerValue] == 100020) {
                [self showLockedProfileScreen];
            } else {
                [self showLoginScreen];
            }
        }
    }];
}

#pragma mark - Navigation

- (void)showScreenForUser:(VAUser *)user {
    switch (user.status) {
        case VAUserStatusNormal:
            if (![user.isEmailConfirmed boolValue] && [user.pendingEmail length]) {
                [self showConfirmEmailAfterUpdateViewController];
            } else if (![user.isEmailConfirmed boolValue]) {
                [self showConfirmEmailAfterSignUpViewControllerWithUser:user];
            } else {
                if (user.nickname.length < 3) {
                    [self showEnterUsernameScreen];
                } else {
                    [self showMyVicinityScreen];
                }
            }
            break;
        case VAUserStatusDeleted:
            [self showLoginScreen];
            break;
        case VAUserStatusBlocked:
            [self showBlockedUserScreen];
            break;
        case VAUserStatusLocked:
            [self showLockedProfileScreen];
            break;
        default:
            break;
    }
}

- (void)showLoginScreen {
    VALoginViewController *vc = [[UIStoryboard storyboardWithName:@"LoginFlow" bundle:[NSBundle mainBundle]] instantiateInitialViewController];
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)showEnterUsernameScreen {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"LoginFlow" bundle:nil];
    VALoginViewController *loginVC =[storyboard instantiateViewControllerWithIdentifier:@"VALoginViewController"];
    VAEnterUsernameViewController *vc = [[UIStoryboard storyboardWithName:@"LoginFlow" bundle:nil] instantiateViewControllerWithIdentifier:@"VAEnterUsernameViewController"];

    vc.delegate = loginVC;
    
    DEMONavigationController *nav = (DEMONavigationController*)[[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"MyVicinityNavigationController"];
    nav.navigationBar.barTintColor = [UIColor colorWithRed:67.0/255.0 green:207.0/255.0 blue:206.0/255.0 alpha:1.0];
    [nav.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [nav.navigationBar setShadowImage:[UIImage new]];
    //[nav.navigationBar setTitleVerticalPositionAdjustment:4.0f forBarMetrics:UIBarMetricsDefault];
    [nav.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"Avenir-Book" size:21.0f]}];
    [nav setViewControllers:@[loginVC, vc] animated:NO];
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    window.rootViewController = nav;
    window.backgroundColor = [UIColor colorWithRed:67.0/255.0 green:207.0/255.0 blue:206.0/255.0 alpha:1.0f];
    [window makeKeyAndVisible];
    
}

- (void)showMyVicinityScreen {
    [[VALocationTool sharedInstance] requestInUseAuthorization];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    VAEventsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"VAEventsViewController"];
    DEMONavigationController *nav = (DEMONavigationController*)[storyboard instantiateViewControllerWithIdentifier:@"MyVicinityNavigationController"];
    [nav setViewControllers:@[vc] animated:NO];
    VAMenuTableViewController *leftMenuController = [storyboard instantiateViewControllerWithIdentifier:@"VAMenuTableViewController"];;
    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:nav
                                                    leftMenuViewController:leftMenuController
                                                    rightMenuViewController:nil];
    container.leftMenuWidth = 125.f;
    container.menuAnimationMaxDuration = container.menuAnimationDefaultDuration;
    leftMenuController.containerViewController = container;
    nav.containerViewController = container;
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    window.rootViewController = container;
    [window makeKeyAndVisible];
}

- (void)showBlockedUserScreen {
    [[VAControlTool defaultControl] logoutForBlockedUser];
}

- (void)showLockedProfileScreen {
    VAProfileViewController *profileVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"profileViewController"];
    DEMONavigationController *nav = (DEMONavigationController*)[[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"MyVicinityNavigationController"];
    [nav setViewControllers:@[profileVC] animated:NO];

    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    window.rootViewController = nav;
    [window makeKeyAndVisible];
}

- (void)showConfirmEmailAfterUpdateViewController {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    VAProfileViewController *profileVC = [storyboard instantiateViewControllerWithIdentifier:@"profileViewController"];
    VAConfirmEmailViewController *confirmEmailVC = [[UIStoryboard storyboardWithName:@"Vicinity" bundle:nil] instantiateViewControllerWithIdentifier:@"confirmEmailViewController"];
    confirmEmailVC.isEmailUpdated = YES;
    confirmEmailVC.delegate = (id<VAConfirmEmailDelegate>)profileVC;
    DEMONavigationController *nav = (DEMONavigationController*)[storyboard instantiateViewControllerWithIdentifier:@"MyVicinityNavigationController"];
    [nav setViewControllers:@[profileVC, confirmEmailVC] animated:NO];
    VAMenuTableViewController *leftMenuController = [storyboard instantiateViewControllerWithIdentifier:@"VAMenuTableViewController"];;
    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:nav
                                                    leftMenuViewController:leftMenuController
                                                    rightMenuViewController:nil];
    container.leftMenuWidth = 125.f;
    container.menuAnimationMaxDuration = container.menuAnimationDefaultDuration;
    leftMenuController.containerViewController = container;
    nav.containerViewController = container;
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    window.rootViewController = container;
    [window makeKeyAndVisible];
}

- (void)showConfirmEmailAfterSignUpViewControllerWithUser:(VAUser *)user {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"LoginFlow" bundle:nil];

    VALoginViewController *loginVC =[storyboard instantiateViewControllerWithIdentifier:@"VALoginViewController"];
    VACreateAccountViewController *createAccountVC = [storyboard instantiateViewControllerWithIdentifier:@"VACreateAccountViewController"];
    VASignUpNewViewController *signUpVC = [storyboard instantiateViewControllerWithIdentifier:@"VASignUpNewViewController"];
    signUpVC.pendingUser = user;
    VAConfirmEmailViewController *confirmEmailVC = [[UIStoryboard storyboardWithName:@"Vicinity" bundle:nil] instantiateViewControllerWithIdentifier:@"confirmEmailViewController"];
    confirmEmailVC.isEmailUpdated = NO;
    confirmEmailVC.delegate = (id<VAConfirmEmailDelegate>)signUpVC;
    
    DEMONavigationController *nav = (DEMONavigationController*)[[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"MyVicinityNavigationController"];
    nav.navigationBar.barTintColor = [UIColor colorWithRed:67.0/255.0 green:207.0/255.0 blue:206.0/255.0 alpha:1.0];
    [nav.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [nav.navigationBar setShadowImage:[UIImage new]];
    //[nav.navigationBar setTitleVerticalPositionAdjustment:4.0f forBarMetrics:UIBarMetricsDefault];
    [nav.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"Avenir-Book" size:21.0f]}];
    [nav setViewControllers:@[loginVC, createAccountVC, signUpVC, confirmEmailVC] animated:NO];

    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    window.rootViewController = nav;
    window.backgroundColor = [UIColor colorWithRed:67.0/255.0 green:207.0/255.0 blue:206.0/255.0 alpha:1.0f];
    [window makeKeyAndVisible];
}

@end
