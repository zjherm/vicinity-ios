//
//  VAConnectionUserTableViewCell.h
//  vicinity-app
//
//  Created by Panda Systems on 11/10/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VAConnection;

@protocol VAConnectionUserCellDelegate <NSObject>

- (void)toggleActionsForConnection:(VAConnection *)connection;

@end

@interface VAConnectionUserTableViewCell : UITableViewCell

@property (nonatomic, weak) id <VAConnectionUserCellDelegate> delegate;

- (void)configureCellWithConnection:(VAConnection *)connection
                        showActions:(BOOL)showActions;

@end
