//
//  VAConnectionActionsTableViewCell.h
//  vicinity-app
//
//  Created by Panda Systems on 11/10/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VAConnection;

@protocol VAConnectionActionsDelegate <NSObject>

- (void)disconnectConnectionSelected:(VAConnection *)connection;
- (void)messageToConnectionSelected:(VAConnection *)connection;
- (void)reportAboutConnectionSelected:(VAConnection *)connection;
- (void)blockConnectionSelected:(VAConnection *)connection;

@end

@interface VAConnectionActionsTableViewCell : UITableViewCell

@property (nonatomic, weak) id <VAConnectionActionsDelegate> delegate;

- (void)configureCellWithConnection:(VAConnection *)connection;

@end
