//
//  VAConnectionActionsTableViewCell.m
//  vicinity-app
//
//  Created by Panda Systems on 11/10/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAConnectionActionsTableViewCell.h"

#import "VAConnection.h"

@interface VAConnectionActionsTableViewCell ()

@property (nonatomic, weak) IBOutlet UIView *borderView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *borderViewHeightConstraint;

@property (nonatomic, strong) VAConnection *connection;

@end

@implementation VAConnectionActionsTableViewCell

- (void)awakeFromNib {
    [self setupBorderView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Setup UI

- (void)setupBorderView {
    self.borderView.layer.borderColor = [UIColor colorWithRed:151.0f/255.0f
                                                        green:151.0f/255.0f
                                                         blue:151.0f/255.0f
                                                        alpha:1].CGColor;
    self.borderView.layer.borderWidth = 0.5f;
    
    if ([UIScreen mainScreen].bounds.size.width < 350.0f) {
        self.borderViewHeightConstraint.constant = 42.0f;
    } else if ([UIScreen mainScreen].bounds.size.width < 400.0f) {
        self.borderViewHeightConstraint.constant = 49.0f;
    } else {
        self.borderViewHeightConstraint.constant = 54.0f;
    }
}

#pragma mark - Configure cell

- (void)configureCellWithConnection:(VAConnection *)connection {
    self.connection = connection;
}

#pragma mark - Actions

- (IBAction)disconnectButtonTouchUp:(id)sender {
    if (self.delegate) {
        [self.delegate disconnectConnectionSelected:self.connection];
    }
}

- (IBAction)messageButtonTouchUp:(id)sender {
    if (self.delegate) {
        [self.delegate messageToConnectionSelected:self.connection];
    }
}

- (IBAction)reportButtonTouchUp:(id)sender {
    if (self.delegate) {
        [self.delegate reportAboutConnectionSelected:self.connection];
    }
}

- (IBAction)blockButtonTouchUp:(id)sender {
    if (self.delegate) {
        [self.delegate blockConnectionSelected:self.connection];
    }
}

@end
