//
//  VAYourConnectionsTabsView.m
//  vicinity-app
//
//  Created by Panda Systems on 11/9/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAYourConnectionsTabsView.h"

@interface VAYourConnectionsTabsView ()

@property (nonatomic, weak) IBOutlet UIView *borderView;

@property (nonatomic, weak) IBOutlet UIView *yourConnectionsView;
@property (nonatomic, weak) IBOutlet UILabel *yourConnectionsLabel;

@property (nonatomic, weak) IBOutlet UIView *connectedToYouView;
@property (nonatomic, weak) IBOutlet UILabel *connectedToYouLabel;

@property (nonatomic, weak) IBOutlet UIView *everyoneView;
@property (nonatomic, weak) IBOutlet UILabel *everyoneLabel;

@property (nonatomic, weak) IBOutlet UIView *activeTabView;

// Constraints
@property (nonatomic, strong) NSArray *yourConnectionsActiveTabConstraints;
@property (nonatomic, strong) NSArray *connectedToYouActiveTabConstraints;
@property (nonatomic, strong) NSArray *everyoneActiveTabConstraints;

@end

@implementation VAYourConnectionsTabsView

- (void)awakeFromNib {
    [self setupBorderView];
    [self setupTextFonts];
    [self setupGestures];
    [self setupConstraints];
    
    self.selectedTab = YourConnectionsTabsYourConnections;
}

#pragma mark - Setup UI

- (void)setupBorderView {
    self.borderView.layer.borderColor = [UIColor colorWithRed:151.0f/255.0f
                                                        green:151.0f/255.0f
                                                         blue:151.0f/255.0f
                                                        alpha:1].CGColor;
    self.borderView.layer.borderWidth = 0.5f;
}

- (void)setupTextFonts {
    self.yourConnectionsLabel.font = [self selectedTabTextFont];
    self.connectedToYouLabel.font = [self normalTabTextFont];
    self.everyoneLabel.font = [self normalTabTextFont];
}

- (void)setupGestures {
    UITapGestureRecognizer *yourConnectionsTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(yourConnectionsViewTap:)];
    [self.yourConnectionsView addGestureRecognizer:yourConnectionsTap];
    
    UITapGestureRecognizer *connectedToYouTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(connectedToYouViewTap:)];
    [self.connectedToYouView addGestureRecognizer:connectedToYouTap];
    
    UITapGestureRecognizer *everyoneTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(everyoneViewTap:)];
    [self.everyoneView addGestureRecognizer:everyoneTap];
}

- (void)setupConstraints {
    self.yourConnectionsActiveTabConstraints = @[[NSLayoutConstraint constraintWithItem:self.activeTabView
                                                                             attribute:NSLayoutAttributeLeading
                                                                             relatedBy:NSLayoutRelationEqual
                                                                                toItem:self.yourConnectionsView
                                                                             attribute:NSLayoutAttributeLeading
                                                                            multiplier:1
                                                                              constant:0],
                                                [NSLayoutConstraint constraintWithItem:self.activeTabView
                                                                             attribute:NSLayoutAttributeTrailing
                                                                             relatedBy:NSLayoutRelationEqual
                                                                                toItem:self.yourConnectionsView
                                                                             attribute:NSLayoutAttributeTrailing
                                                                            multiplier:1
                                                                              constant:0]];
    
    self.connectedToYouActiveTabConstraints = @[[NSLayoutConstraint constraintWithItem:self.activeTabView
                                                                             attribute:NSLayoutAttributeLeading
                                                                             relatedBy:NSLayoutRelationEqual
                                                                                toItem:self.connectedToYouView
                                                                             attribute:NSLayoutAttributeLeading
                                                                            multiplier:1
                                                                              constant:0],
                                                [NSLayoutConstraint constraintWithItem:self.activeTabView
                                                                             attribute:NSLayoutAttributeTrailing
                                                                             relatedBy:NSLayoutRelationEqual
                                                                                toItem:self.connectedToYouView
                                                                             attribute:NSLayoutAttributeTrailing
                                                                            multiplier:1
                                                                              constant:0]];
    
    self.everyoneActiveTabConstraints = @[[NSLayoutConstraint constraintWithItem:self.activeTabView
                                                                             attribute:NSLayoutAttributeLeading
                                                                             relatedBy:NSLayoutRelationEqual
                                                                                toItem:self.everyoneView
                                                                             attribute:NSLayoutAttributeLeading
                                                                            multiplier:1
                                                                              constant:0],
                                                [NSLayoutConstraint constraintWithItem:self.activeTabView
                                                                             attribute:NSLayoutAttributeTrailing
                                                                             relatedBy:NSLayoutRelationEqual
                                                                                toItem:self.everyoneView
                                                                             attribute:NSLayoutAttributeTrailing
                                                                            multiplier:1
                                                                              constant:0]];

    [self addConstraints:self.yourConnectionsActiveTabConstraints];
}

#pragma mark - Tabs selection

- (void)selectYourConnectionsTab {
    [self selectedTabLabel].textColor = [self normalTabTextColor];
    [self selectedTabLabel].font = [self normalTabTextFont];
    self.selectedTab = YourConnectionsTabsYourConnections;
    [self selectedTabLabel].textColor = [self selectedTabTextColor];
    [self selectedTabLabel].font = [self selectedTabTextFont];
    
    [self removeConstraints:self.connectedToYouActiveTabConstraints];
    [self removeConstraints:self.everyoneActiveTabConstraints];
    [self addConstraints:self.yourConnectionsActiveTabConstraints];
}

- (void)selectConnectedToYouTab {
    [self selectedTabLabel].textColor = [self normalTabTextColor];
    [self selectedTabLabel].font = [self normalTabTextFont];
    self.selectedTab = YourConnectionsTabsConnectedToYou;
    [self selectedTabLabel].textColor = [self selectedTabTextColor];
    [self selectedTabLabel].font = [self selectedTabTextFont];
    
    [self removeConstraints:self.yourConnectionsActiveTabConstraints];
    [self removeConstraints:self.everyoneActiveTabConstraints];
    [self addConstraints:self.connectedToYouActiveTabConstraints];
}

- (void)selectEveryoneTab {
    [self selectedTabLabel].textColor = [self normalTabTextColor];
    [self selectedTabLabel].font = [self normalTabTextFont];
    self.selectedTab = YourConnectionsTabsEveryone;
    [self selectedTabLabel].textColor = [self selectedTabTextColor];
    [self selectedTabLabel].font = [self selectedTabTextFont];
    
    [self removeConstraints:self.yourConnectionsActiveTabConstraints];
    [self removeConstraints:self.connectedToYouActiveTabConstraints];
    [self addConstraints:self.everyoneActiveTabConstraints];
}

#pragma mark - Actions

- (void)yourConnectionsViewTap:(id)sender {
    if (self.delegate) {
        [self.delegate selectedTabChanged:YourConnectionsTabsYourConnections];
    }
}

- (void)connectedToYouViewTap:(id)sender {
    if (self.delegate) {
        [self.delegate selectedTabChanged:YourConnectionsTabsConnectedToYou];
    }
}

- (void)everyoneViewTap:(id)sender {
    if (self.delegate) {
        [self.delegate selectedTabChanged:YourConnectionsTabsEveryone];
    }
}

#pragma mark - Helpers

- (UIColor *)normalTabTextColor {
    return [UIColor colorWithRed:74.0f/255.0f
                           green:74.0f/255.0f
                            blue:74.0f/255.0f
                           alpha:1];
}

- (UIColor *)selectedTabTextColor {
    return [UIColor colorWithRed:16.0f/255.0f
                           green:112.0f/255.0f
                            blue:179.0f/255.0f
                           alpha:1];
}

- (UIFont *)normalTabTextFont {
//    if ([UIScreen mainScreen].bounds.size.width < 350.0f) {
//        return [UIFont fontWithName:@"Avenir-Book" size:8.5f];
//    } else if ([UIScreen mainScreen].bounds.size.width < 400.0f) {
        return [UIFont fontWithName:@"Avenir-Book" size:10.0f];
//    } else {
//        return [UIFont fontWithName:@"Avenir-Book" size:11.0f];
//    }
}

- (UIFont *)selectedTabTextFont {
//    if ([UIScreen mainScreen].bounds.size.width < 350.0f) {
//        return [UIFont fontWithName:@"Avenir-Heavy" size:8.5f];
//    } else if ([UIScreen mainScreen].bounds.size.width < 400.0f) {
        return [UIFont fontWithName:@"Avenir-Heavy" size:10.0f];
//    } else {
//        return [UIFont fontWithName:@"Avenir-Heavy" size:11.0f];
//    }
}

- (UILabel *)selectedTabLabel {
    switch (self.selectedTab) {
        case YourConnectionsTabsYourConnections:
            return self.yourConnectionsLabel;
        case YourConnectionsTabsConnectedToYou:
            return self.connectedToYouLabel;
        case YourConnectionsTabsEveryone:
            return self.everyoneLabel;
    }
    return nil;
}

@end
