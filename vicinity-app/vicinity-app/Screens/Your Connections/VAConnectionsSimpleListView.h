//
//  VAConnectionsSimpleListView.h
//  vicinity-app
//
//  Created by Panda Systems on 11/16/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VAConnectionsListView.h"

@interface VAConnectionsSimpleListView : VAConnectionsListView

- (void)updateConnectionInList:(VAConnection *)connection;

@end
