//
//  VAConnectionUserTableViewCell.m
//  vicinity-app
//
//  Created by Panda Systems on 11/10/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAConnectionUserTableViewCell.h"

#import "VAConnection.h"

@interface VAConnectionUserTableViewCell ()

@property (nonatomic, weak) IBOutlet UIImageView *avatarImageView;
@property (nonatomic, weak) IBOutlet UILabel *nicknameLabel;
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;

@property (nonatomic, weak) IBOutlet UIView *userInfoBottomSeparatorView;

@property (nonatomic, strong) IBOutletCollection(NSLayoutConstraint) NSArray *separatorHeightConstraints;

@property (nonatomic, strong) VAConnection *connection;
@property (nonatomic) BOOL showActions;

@end

@implementation VAConnectionUserTableViewCell

- (void)awakeFromNib {
    [self setupAvatarImageView];
    [self setupSeparatorLines];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

#pragma mark - Setup

- (void)setupAvatarImageView {
    self.avatarImageView.layer.borderWidth = 0.5f;
    self.avatarImageView.layer.borderColor = [UIColor colorWithRed:239.0f/255.0f
                                                             green:239.0f/255.0f
                                                              blue:239.0f/255.0f
                                                             alpha:1].CGColor;
    self.avatarImageView.layer.cornerRadius = 45.0f/2;
}

- (void)setupSeparatorLines {
    for (NSLayoutConstraint *constraint in self.separatorHeightConstraints) {
        constraint.constant = 0.5f;
    }
}

#pragma mark - Properties

- (void)setShowActions:(BOOL)showActions {
    _showActions = showActions;
    
    self.userInfoBottomSeparatorView.hidden = showActions;
}

#pragma mark - Configure cell

- (void)configureCellWithConnection:(VAConnection *)connection
                        showActions:(BOOL)showActions {
    self.connection = connection;
    self.showActions = showActions;
    
    self.avatarImageView.image = [UIImage imageNamed:@"avatar-placeholder"];
    [[VAImageCache sharedImageCache] getImageWithURL:connection.avatarURL success:^(UIImage *image, NSString *imageURL) {
        if ([self.connection.avatarURL isEqualToString:imageURL]) {
            self.avatarImageView.image = image;
        }
    } failure:^(NSError *error) {
        
    }];
    self.nicknameLabel.text = connection.nickname;
    self.nameLabel.text = connection.fullname;
}

#pragma mark - Actions

- (IBAction)moreButtonTouchUp:(id)sender {
    self.showActions = !self.showActions;
    if (self.delegate) {
        [self.delegate toggleActionsForConnection:self.connection];
    }
}

@end
