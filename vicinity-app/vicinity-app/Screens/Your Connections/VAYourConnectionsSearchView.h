//
//  VAYourConnectionsSearchView.h
//  vicinity-app
//
//  Created by Panda Systems on 11/9/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ConnectionsSearchDelegate <NSObject>

- (void)searchTextChanged:(NSString *)searchText;

@end

@interface VAYourConnectionsSearchView : UIView
@property (nonatomic, weak) id <ConnectionsSearchDelegate> delegate;
@end
