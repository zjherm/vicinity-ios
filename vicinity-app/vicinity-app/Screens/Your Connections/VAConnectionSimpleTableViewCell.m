//
//  VAConnectionsSimpleTableViewCell.m
//  vicinity-app
//
//  Created by Panda Systems on 11/16/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAConnectionSimpleTableViewCell.h"

#import "VAConnection.h"

@interface VAConnectionSimpleTableViewCell ()

@property (nonatomic, weak) IBOutlet UIImageView *avatarImageView;
@property (nonatomic, weak) IBOutlet UILabel *nicknameLabel;
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;

@property (nonatomic, weak) IBOutlet UIButton *addConnectionButton;
@property (nonatomic, weak) IBOutlet UIButton *removeConnectionButton;

@property (nonatomic, strong) IBOutletCollection(NSLayoutConstraint) NSArray *separatorHeightConstraints;

@property (nonatomic, strong) VAConnection *connection;

@end


@implementation VAConnectionSimpleTableViewCell

- (void)awakeFromNib {
    [self setupAvatarImageView];
    [self setupSeparatorLines];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Setup

- (void)setupAvatarImageView {
    self.avatarImageView.layer.borderWidth = 0.5f;
    self.avatarImageView.layer.borderColor = [UIColor colorWithRed:239.0f/255.0f
                                                             green:239.0f/255.0f
                                                              blue:239.0f/255.0f
                                                             alpha:1].CGColor;
    
    self.avatarImageView.layer.cornerRadius = 45.0f/2;
}

- (void)setupSeparatorLines {
    for (NSLayoutConstraint *constraint in self.separatorHeightConstraints) {
        constraint.constant = 1/[UIScreen mainScreen].scale;
    }
}

#pragma mark - Configure cell

- (void)configureCellWithConnection:(VAConnection *)connection {
    self.connection = connection;
    
    self.avatarImageView.image = [UIImage imageNamed:@"avatar-placeholder"];
    [[VAImageCache sharedImageCache] getImageWithURL:connection.avatarURL success:^(UIImage *image, NSString *imageURL) {
        if ([self.connection.avatarURL isEqualToString:imageURL]) {
            self.avatarImageView.image = image;
        }
    } failure:^(NSError *error) {
        
    }];
    self.nicknameLabel.text = connection.nickname;
    self.nameLabel.text = connection.fullname;
    
    self.addConnectionButton.hidden = [connection.isFriend boolValue] || [connection.isBlocked boolValue];
    self.removeConnectionButton.hidden = ![connection.isFriend boolValue] || [connection.isBlocked boolValue];
}

#pragma mark - Actions

- (IBAction)addConnectionButtonTouchUp:(id)sender {
    if (self.delegate) {
        [self.delegate addConnectionSelected:self.connection];
    }
}

- (IBAction)removeConnectionButtonTouchUp:(id)sender {
    if (self.delegate) {
        [self.delegate removeConnectionSelected:self.connection];
    }
}

@end
