//
//  VAYourConnectionsSearchView.m
//  vicinity-app
//
//  Created by Panda Systems on 11/9/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAYourConnectionsSearchView.h"

@interface VAYourConnectionsSearchView () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITextField *searchTextField;
@property (nonatomic, weak) IBOutlet UIButton *cancelButton;

@end

@implementation VAYourConnectionsSearchView

- (void)awakeFromNib {
    if ([UIScreen mainScreen].bounds.size.width < 350.0f) {
        self.searchTextField.font = [UIFont fontWithName:@"Montserrat-Regular" size:12.0f];
        self.cancelButton.titleLabel.font = [UIFont fontWithName:@"Montserrat-Regular" size:12.0f];
    } else if ([UIScreen mainScreen].bounds.size.width < 400.0f) {
        self.searchTextField.font = [UIFont fontWithName:@"Montserrat-Regular" size:14.0f];
        self.cancelButton.titleLabel.font = [UIFont fontWithName:@"Montserrat-Regular" size:14.0f];
    } else {
        self.searchTextField.font = [UIFont fontWithName:@"Montserrat-Regular" size:15.0f];
        self.cancelButton.titleLabel.font = [UIFont fontWithName:@"Montserrat-Regular" size:15.0f];
    }
    
    self.cancelButton.hidden = YES;
}

#pragma mark - Actions

- (IBAction)cancelButtonTouchUp:(UIButton *)sender {
    [self.searchTextField resignFirstResponder];
}

- (IBAction)searchTextFieldEditingChanged:(UITextField *)sender {
    if (self.delegate) {
        [self.delegate searchTextChanged:sender.text];
    }
}

#pragma mark - Text Field delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.cancelButton.hidden = NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.cancelButton.hidden = YES;
}

@end
