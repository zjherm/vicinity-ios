//
//  VAConnectionsSimpleListView.m
//  vicinity-app
//
//  Created by Panda Systems on 11/16/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAConnectionsSimpleListView.h"

#import "VAConnectionSimpleTableViewCell.h"

#import "VAUserApiManager.h"
#import "VAConnection.h"
#import "VANotificationMessage.h"
#import "VAControlTool.h"

@interface VAConnectionsSimpleListView () <VAConnectionSimpleCellDelegate>


@end

@implementation VAConnectionsSimpleListView

- (void)awakeFromNib {
    _connections = [NSMutableArray array];
    
    [self registerNibsForCells];
    [self setupTextFonts];
    [self setupSeparatorLine];
}

#pragma mark - Properties

- (NSMutableArray *)connections {
    return _connections;
}

- (void)setConnections:(NSMutableArray *)connections {
    _connections = connections;
    
    [self updateUI];
}

#pragma mark - Setup

- (void)registerNibsForCells {
    UINib *userCellNib = [UINib nibWithNibName:@"VAConnectionSimpleTableViewCell" bundle:[NSBundle mainBundle]];
    [self.connectionsTableView registerNib:userCellNib forCellReuseIdentifier:@"ConnectionCell"];
}

- (void)setupTextFonts {
    if ([UIScreen mainScreen].bounds.size.width < 350.0f) {
        self.connectionsCountLabel.font = [UIFont fontWithName:@"Montserrat-Regular" size:10.0f];
    } else if ([UIScreen mainScreen].bounds.size.width < 400.0f) {
        self.connectionsCountLabel.font = [UIFont fontWithName:@"Montserrat-Regular" size:12.0f];
    } else {
        self.connectionsCountLabel.font = [UIFont fontWithName:@"Montserrat-Regular" size:13.0f];
    }
}

- (void)setupSeparatorLine {
    self.separatorLineViewHeightConstraint.constant = 1/[UIScreen mainScreen].scale;
}

#pragma mark - Table View data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.connections.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VAConnection *connection = self.connections[indexPath.row];
    
    VAConnectionSimpleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ConnectionCell" forIndexPath:indexPath];
    cell.delegate = self;
    [cell configureCellWithConnection:connection];
    return cell;
}

#pragma mark - Table View delegate

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.delegate) {
        [self.delegate connectionSelected:self.connections[indexPath.row]];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - VAConnectionSimpleCellDelegate

- (void)addConnectionSelected:(VAConnection *)connection {
    [self connectConnection:connection];
}

- (void)removeConnectionSelected:(VAConnection *)connection {
    [self disconnectConnection:connection];
}

#pragma mark - Helpers

- (void)updateConnectionInList:(VAConnection *)connection {
    NSInteger index = [self.connections indexOfObject:connection];
    if (index != NSNotFound) {
        NSArray *indexPaths = @[[NSIndexPath indexPathForRow:index inSection:0]];
        [self.connections replaceObjectAtIndex:index withObject:connection];
        
        [self.connectionsTableView beginUpdates];
        [self.connectionsTableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
        [self.connectionsTableView endUpdates];
    }
}

- (void)connectConnection:(VAConnection *)connection {
    connection.isFriend = @(YES);
    [self updateConnectionInList:connection];
    
    [[VAUserApiManager new] postAddFriendWithID:connection.userId andCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            connection.isFriend = @(NO);
            [self updateConnectionInList:connection];
            
            [VANotificationMessage showNetworkErrorMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
        } else {
            [VANotificationsService didConnectUser:connection];
        }
    }];
}

- (void)disconnectConnection:(VAConnection *)connection {
    connection.isFriend = @(NO);
    [self updateConnectionInList:connection];
    
    [[VAUserApiManager new] deleteConnectionWithID:connection.userId withCompletion:^(NSError *error, VAModel *model) {
         if (error) {
             connection.isFriend = @(YES);
             [self updateConnectionInList:connection];
             
             [VANotificationMessage showNetworkErrorMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
         } else {
             [VANotificationsService didDisconnectUser:connection];
         }
     }];
}

@end
