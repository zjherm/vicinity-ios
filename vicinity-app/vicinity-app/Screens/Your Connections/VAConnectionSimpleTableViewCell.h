//
//  VAConnectionsSimpleTableViewCell.h
//  vicinity-app
//
//  Created by Panda Systems on 11/16/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VAConnection;

@protocol VAConnectionSimpleCellDelegate <NSObject>

- (void)addConnectionSelected:(VAConnection *)connection;
- (void)removeConnectionSelected:(VAConnection *)connection;

@end

@interface VAConnectionSimpleTableViewCell : UITableViewCell

@property (nonatomic, weak) id <VAConnectionSimpleCellDelegate> delegate;

- (void)configureCellWithConnection:(VAConnection *)connection;

@end
