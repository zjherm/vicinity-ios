//
//  VAYourConnectionsViewController.m
//  vicinity-app
//
//  Created by Panda Systems on 11/9/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAYourConnectionsViewController.h"

#import "DEMONavigationController.h"
#import "VAUserApiManager.h"
#import "VANotificationMessage.h"
#import "VAProfileViewController.h"
#import "VAReportViewController.h"
#import "VAConnection.h"
#import "VAConnectionsResponse.h"
#import "VAUserSearchResponse.h"
#import "VANotificationMessage.h"
#import "VAUserDefaultsHelper.h"
#import <Google/Analytics.h>

#import "VAYourConnectionsSearchView.h"
#import "VAYourConnectionsTabsView.h"
#import "VAConnectionsListView.h"
#import "VAConnectionsSimpleListView.h"

@interface VAYourConnectionsViewController () <ConnectionsSearchDelegate, ConnectionsTabsDelegate, VAConnectionsListDelegate, VAReportSendingDelegate>

@property (nonatomic, weak) IBOutlet VAYourConnectionsSearchView *searchView;
@property (nonatomic, weak) IBOutlet VAYourConnectionsTabsView *tabsView;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic, weak) IBOutlet VAConnectionsListView *yourConnectionsListView;
@property (nonatomic, weak) IBOutlet VAConnectionsSimpleListView *connectedToYouListView;
@property (nonatomic, weak) IBOutlet VAConnectionsSimpleListView *everyoneListView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *viewBottomConstraint;

@property (nonatomic, strong) NSMutableArray *yourConnections;
@property (nonatomic, strong) NSMutableArray *connectedToYouConnections;
@property (nonatomic, strong) NSMutableArray *everyoneConnections;

@property (nonatomic, strong) NSTimer *searchTimer;
@property (nonatomic, strong) NSString *searchText;

@end

@implementation VAYourConnectionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [(DEMONavigationController *)self.navigationController setDefaultNavigationBar];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Menu Icon"] style:UIBarButtonItemStylePlain target:self.navigationController action:@selector(showMenu)];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    self.searchView.delegate = self;
    self.tabsView.delegate = self;
    self.yourConnectionsListView.delegate = self;
    self.connectedToYouListView.delegate = self;
    self.everyoneListView.delegate = self;
    
    [self setupObservers];
    
    [self loadYourConnections];
    [self loadConnectedToYou];
    [self loadEveryone];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [(DEMONavigationController *)self.navigationController setTitle:@"Connections"];
    
    switch (self.tabsView.selectedTab) {
        case YourConnectionsTabsYourConnections:
            self.scrollView.contentOffset = CGPointMake(0, 0);
            break;
        case YourConnectionsTabsConnectedToYou:
            self.scrollView.contentOffset = CGPointMake(self.scrollView.frame.size.width, 0);
            break;
        case YourConnectionsTabsEveryone:
            self.scrollView.contentOffset = CGPointMake(self.scrollView.frame.size.width*2, 0);
            break;
        default:
            break;
    }
    
    [self addKeyboardObservers];
    
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Connections screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self removeKeyboardObservers];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Setup

- (void)setupObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didUserConnected:)
                                                 name:VAUserDidConnectedNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didUserDisconnected:)
                                                 name:VAUserDidDisconnectedNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didUserDisconnected:)
                                                 name:VAUserDidBlockedNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didUserUnblocked:)
                                                 name:VAUserDidUnblockedNotification
                                               object:nil];
}

- (void)addKeyboardObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)removeKeyboardObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

#pragma mark - Tabs selection

- (void)selectYourConnectionsTab {
    [self.tabsView selectYourConnectionsTab];
    
    [UIView animateWithDuration:0.3f animations:^{
        self.scrollView.contentOffset = CGPointMake(0, 0);
        [self.tabsView layoutIfNeeded];
    }];
}

- (void)selectConnectedToYouTab {
    [self.tabsView selectConnectedToYouTab];
    
    [UIView animateWithDuration:0.3f animations:^{
        self.scrollView.contentOffset = CGPointMake(self.scrollView.frame.size.width, 0);
        [self.tabsView layoutIfNeeded];
    }];
}

- (void)selectEveryoneTab {
    [self.tabsView selectEveryoneTab];
    
    [UIView animateWithDuration:0.3f animations:^{
        self.scrollView.contentOffset = CGPointMake(self.scrollView.frame.size.width*2, 0);
        [self.tabsView layoutIfNeeded];
    }];
}

#pragma mark - Actions

- (IBAction)rightSwipeRecognized:(id)sender {
    switch (self.tabsView.selectedTab) {
        case YourConnectionsTabsYourConnections:
            break;
        case YourConnectionsTabsConnectedToYou:
            [self selectYourConnectionsTab];
            break;
        case YourConnectionsTabsEveryone:
            [self selectConnectedToYouTab];
            break;
        default:
            break;
    }
}

- (IBAction)leftSwipeRecognized:(id)sender {
    switch (self.tabsView.selectedTab) {
        case YourConnectionsTabsYourConnections:
            [self selectConnectedToYouTab];
            break;
        case YourConnectionsTabsConnectedToYou:
            [self selectEveryoneTab];
            break;
        case YourConnectionsTabsEveryone:
            break;
        default:
            break;
    }
}

#pragma mark - Connections Search delegate

- (void)searchTextChanged:(NSString *)searchText {
    self.searchText = searchText;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"fullname CONTAINS[cd] %@ OR nickname CONTAINS[cd] %@ ", searchText, searchText];
    self.yourConnectionsListView.connections = [searchText length] ? [[self.yourConnections filteredArrayUsingPredicate:predicate] mutableCopy] : [self.yourConnections mutableCopy];
    self.connectedToYouListView.connections = [searchText length] ? [[self.connectedToYouConnections filteredArrayUsingPredicate:predicate] mutableCopy] : [self.connectedToYouConnections mutableCopy];

    if (self.searchTimer) {
        [self.searchTimer invalidate];
        self.searchTimer = nil;
    }
    
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                      target:self
                                                    selector:@selector(loadEveryone)
                                                    userInfo:nil
                                                     repeats:NO];
    self.searchTimer = timer;
}

#pragma mark - Connections Tabs delegate

- (void)selectedTabChanged:(YourConnectionsTabs)tab {
    switch (tab) {
        case YourConnectionsTabsYourConnections:
            [self selectYourConnectionsTab];
            break;
        case YourConnectionsTabsConnectedToYou:
            [self selectConnectedToYouTab];
            break;
        case YourConnectionsTabsEveryone:
            [self selectEveryoneTab];
            break;
        default:
            break;
    }
}

#pragma mark - Connections List delegate

- (void)connectionSelected:(VAConnection *)connection {
    [self showProfileForUserWithAlias:connection.nickname];
}

- (void)reportSelectedForConnection:(VAConnection *)connection {
    [self showReportScreenWithConnection:connection];
}

#pragma mark - Helpers

- (void)loadYourConnections {
    VAUserApiManager *manager = [VAUserApiManager new];
    [manager getConnectionsWithCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
        } else if (model) {
            VAConnectionsResponse *response = (VAConnectionsResponse *)model;
            
            NSSortDescriptor *desriptor = [[NSSortDescriptor alloc] initWithKey:@"fullname" ascending:YES];
            NSArray *sorted = [response.connections sortedArrayUsingDescriptors:@[desriptor]];
            
            self.yourConnections = [sorted mutableCopy];
            self.yourConnectionsListView.connections = [self.yourConnections mutableCopy];
        }
    }];
}

- (void)loadConnectedToYou {
    VAUserApiManager *manager = [VAUserApiManager new];
    [manager getFollowersWithCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
        } else if (model) {
            VAConnectionsResponse *response = (VAConnectionsResponse *)model;
            
            NSSortDescriptor *desriptor = [[NSSortDescriptor alloc] initWithKey:@"fullname" ascending:YES];
            NSArray *sorted = [response.connections sortedArrayUsingDescriptors:@[desriptor]];
            
            self.connectedToYouConnections = [sorted mutableCopy];
            self.connectedToYouListView.connections = [self.connectedToYouConnections mutableCopy];
        }
    }];
}

- (void)loadEveryone {
    self.searchTimer = nil;
    VAUserApiManager *manager = [VAUserApiManager new];
    if ([self.searchText length]) {
        [manager getUserByAliasSearch:self.searchText withCompletion:^(NSError *error, VAModel *model) {
            if (error) {
                [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
            } else if (model) {
                VAUserSearchResponse *response = (VAUserSearchResponse *)model;
                
                NSSortDescriptor *desriptor = [[NSSortDescriptor alloc] initWithKey:@"fullname" ascending:YES];
                NSArray *sorted = [response.users sortedArrayUsingDescriptors:@[desriptor]];
                
                VAUser *user = (VAUser *)[VAUserDefaultsHelper getModelForKey:VASaveKeys_CurrentUser];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId != %@", user.userId];
                
                self.everyoneConnections = [[sorted filteredArrayUsingPredicate:predicate] mutableCopy];
                self.everyoneListView.connections = self.everyoneConnections;
            }
        }];
    } else {
        [manager getViewedUsersWithCompletion:^(NSError *error, VAModel *model) {
            if (error) {
                [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
            } else if (model) {
                VAConnectionsResponse *response = (VAConnectionsResponse *)model;
                
                NSSortDescriptor *desriptor = [[NSSortDescriptor alloc] initWithKey:@"fullname" ascending:YES];
                NSArray *sorted = [response.connections sortedArrayUsingDescriptors:@[desriptor]];
                
                VAUser *user = (VAUser *)[VAUserDefaultsHelper getModelForKey:VASaveKeys_CurrentUser];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId != %@", user.userId];
                
                self.everyoneConnections = [[sorted filteredArrayUsingPredicate:predicate] mutableCopy];
                self.everyoneListView.connections = self.everyoneConnections;
            }
        }];
    }
}

- (void)showProfileForUserWithAlias:(NSString *)alias {
    VAProfileViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"profileViewController"];
    vc.searchAlias = alias;
    vc.isUsersAccountMode = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showReportScreenWithConnection:(VAConnection *)connection {
    VAReportViewController *vc = [[UIStoryboard storyboardWithName:@"Vicinity" bundle:nil] instantiateViewControllerWithIdentifier:@"reportViewController"];
    vc.reportedUser = connection;
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Report Sending delegate

- (void)didSendReportAboutUser:(VAUser *)user {
    [VANotificationMessage showReportSuccessMessageOnController:self withDuration:5.f];
}

#pragma mark - Observers

- (void)didUserConnected:(NSNotification *)notification {
    VAConnection *user = notification.userInfo[@"user"];
    
    // Your connections
    [self.yourConnections addObject:user];
    NSSortDescriptor *desriptor = [[NSSortDescriptor alloc] initWithKey:@"fullname" ascending:YES];
    self.yourConnections = [[self.yourConnections sortedArrayUsingDescriptors:@[desriptor]] mutableCopy];
    [self.yourConnectionsListView addConnectionToList:user];
    
    // Connected To You
    NSInteger connectedToYouIndex = [self.connectedToYouConnections indexOfObject:user];
    if (connectedToYouIndex != NSNotFound) {
        [self.connectedToYouConnections replaceObjectAtIndex:connectedToYouIndex withObject:user];
        [self.connectedToYouListView updateConnectionInList:user];
    }
    
    // Everyone
    NSInteger everyoneIndex = [self.everyoneConnections indexOfObject:user];
    if (everyoneIndex != NSNotFound) {
        [self.everyoneConnections replaceObjectAtIndex:everyoneIndex withObject:user];
        [self.everyoneListView updateConnectionInList:user];
    }

    // Show notification banner
    [VANotificationMessage showConnectUserSuccessMessageOnController:self withDuration:5.0f nickname:user.nickname];

}

- (void)didUserDisconnected:(NSNotification *)notification {
    VAConnection *user = notification.userInfo[@"user"];

    // Your connections
    [self.yourConnections removeObject:user];
    [self.yourConnectionsListView removeConnectionFromList:user];
    
    // Conected To You
    NSInteger connectedToYouIndex = [self.connectedToYouConnections indexOfObject:user];
    if (connectedToYouIndex != NSNotFound) {
        [self.connectedToYouConnections replaceObjectAtIndex:connectedToYouIndex withObject:user];
        [self.connectedToYouListView updateConnectionInList:user];
    }
    
    // Everyone
    NSInteger everyoneIndex = [self.everyoneConnections indexOfObject:user];
    if (everyoneIndex != NSNotFound) {
        [self.everyoneConnections replaceObjectAtIndex:everyoneIndex withObject:user];
        [self.everyoneListView updateConnectionInList:user];
    }
}

- (void)didUserUnblocked:(NSNotification *)notification {
    VAConnection *user = notification.userInfo[@"user"];
    
    // Your connections
    if ([user.isFriend boolValue]) {
        [self.yourConnections addObject:user];
        NSSortDescriptor *desriptor = [[NSSortDescriptor alloc] initWithKey:@"fullname" ascending:YES];
        self.yourConnections = [[self.yourConnections sortedArrayUsingDescriptors:@[desriptor]] mutableCopy];
        [self.yourConnectionsListView addConnectionToList:user];
    }
    
    // Conected To You
    NSInteger connectedToYouIndex = [self.connectedToYouConnections indexOfObject:user];
    if (connectedToYouIndex != NSNotFound) {
        [self.connectedToYouConnections replaceObjectAtIndex:connectedToYouIndex withObject:user];
        [self.connectedToYouListView updateConnectionInList:user];
    }
    
    // Everyone
    NSInteger everyoneIndex = [self.everyoneConnections indexOfObject:user];
    if (everyoneIndex != NSNotFound) {
        [self.everyoneConnections replaceObjectAtIndex:everyoneIndex withObject:user];
        [self.everyoneListView updateConnectionInList:user];
    }
}

#pragma mark - Keyboard observers

- (void)willKeyboardShow:(NSNotification*)notification {
    
    NSDictionary* info = [notification userInfo];
    CGRect keyRect;
    [[info objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyRect];
    CGFloat height = keyRect.size.height;
    
    self.viewBottomConstraint.constant = height;
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
        switch (self.tabsView.selectedTab) {
            case YourConnectionsTabsYourConnections:
                self.scrollView.contentOffset = CGPointMake(0, 0);
                break;
            case YourConnectionsTabsConnectedToYou:
                self.scrollView.contentOffset = CGPointMake(self.scrollView.frame.size.width, 0);
                break;
            case YourConnectionsTabsEveryone:
                self.scrollView.contentOffset = CGPointMake(self.scrollView.frame.size.width*2, 0);
                break;
            default:
                break;
        }
    }];
}

- (void)willKeyboardHide:(NSNotification*)notification {
    self.viewBottomConstraint.constant = 0.0f;
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
        switch (self.tabsView.selectedTab) {
            case YourConnectionsTabsYourConnections:
                self.scrollView.contentOffset = CGPointMake(0, 0);
                break;
            case YourConnectionsTabsConnectedToYou:
                self.scrollView.contentOffset = CGPointMake(self.scrollView.frame.size.width, 0);
                break;
            case YourConnectionsTabsEveryone:
                self.scrollView.contentOffset = CGPointMake(self.scrollView.frame.size.width*2, 0);
                break;
            default:
                break;
        }
    }];
}

#pragma mark - Gesture recognizer handling

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

@end
