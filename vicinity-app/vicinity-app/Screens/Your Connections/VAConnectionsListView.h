//
//  VAConnectionsListView.h
//  vicinity-app
//
//  Created by Panda Systems on 11/10/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VAConnection;

@protocol VAConnectionsListDelegate <NSObject>

@required
- (void)connectionSelected:(VAConnection *)connection;

@optional
- (void)reportSelectedForConnection:(VAConnection *)connection;
- (void)connectSelectedForConnection:(VAConnection *)connection;
- (void)disconnectSelectedForConnection:(VAConnection *)connection;

@end

@interface VAConnectionsListView : UIView <UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray *_connections;
}

// Outlets
@property (nonatomic, weak) IBOutlet UILabel *connectionsCountLabel;
@property (nonatomic, weak) IBOutlet UITableView *connectionsTableView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *separatorLineViewHeightConstraint;

// Delegates
@property (nonatomic, weak) id <VAConnectionsListDelegate> delegate;

@property (nonatomic, strong) NSMutableArray *connections;

- (void)updateUI;

- (void)addConnectionToList:(VAConnection *)connection;
- (void)removeConnectionFromList:(VAConnection *)connection;

@end
