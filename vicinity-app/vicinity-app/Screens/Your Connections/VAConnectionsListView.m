//
//  VAConnectionsListView.m
//  vicinity-app
//
//  Created by Panda Systems on 11/10/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAConnectionsListView.h"

#import "VAConnectionUserTableViewCell.h"
#import "VAConnectionActionsTableViewCell.h"

#import "VAConnection.h"
#import "VAUserApiManager.h"
#import "VAControlTool.h"
#import "VANotificationMessage.h"

@interface VAConnectionsListView () <VAConnectionUserCellDelegate, VAConnectionActionsDelegate>

@property (nonatomic, strong) NSMutableDictionary *shownActions;
@property (nonatomic) VAConnection *activeConnection;

@end

@implementation VAConnectionsListView

- (void)awakeFromNib {
    _connections = [NSMutableArray array];
    _shownActions = [NSMutableDictionary dictionary];
    _activeConnection = nil;
    
    [self registerNibsForCells];
    [self setupTextFonts];
    [self setupSeparatorLine];
}

#pragma mark - Properties

- (NSMutableArray *)connections {
    return _connections;
}

- (void)setConnections:(NSMutableArray *)connections {
    _connections = connections;
    
    [self.shownActions removeAllObjects];
    for (VAConnection *c in connections) {
        [self.shownActions setObject:@(NO) forKey:c.userId];
    }
    
    [self updateUI];
}


#pragma mark - Setup

- (void)registerNibsForCells {
    UINib *userCellNib = [UINib nibWithNibName:@"VAConnectionUserTableViewCell" bundle:[NSBundle mainBundle]];
    [self.connectionsTableView registerNib:userCellNib forCellReuseIdentifier:@"ConnectionCell"];
    
    UINib *actionsCellNib = [UINib nibWithNibName:@"VAConnectionActionsTableViewCell" bundle:[NSBundle mainBundle]];
    [self.connectionsTableView registerNib:actionsCellNib forCellReuseIdentifier:@"ActionsCell"];
}

- (void)setupTextFonts {
    if ([UIScreen mainScreen].bounds.size.width < 350.0f) {
        self.connectionsCountLabel.font = [UIFont fontWithName:@"Montserrat-Regular" size:10.0f];
    } else if ([UIScreen mainScreen].bounds.size.width < 400.0f) {
        self.connectionsCountLabel.font = [UIFont fontWithName:@"Montserrat-Regular" size:12.0f];
    } else {
        self.connectionsCountLabel.font = [UIFont fontWithName:@"Montserrat-Regular" size:13.0f];
    }
}

- (void)setupSeparatorLine {
    self.separatorLineViewHeightConstraint.constant = 1/[UIScreen mainScreen].scale;
}

#pragma mark - Update UI

- (void)updateUI {
    [self updateConnectionsCount];
    [self.connectionsTableView reloadData];
}

- (void)updateConnectionsCount {
    self.connectionsCountLabel.text = [NSString stringWithFormat:@"Count: %lu", (unsigned long)self.connections.count];
}

#pragma mark - Table View data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.connections.count*2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VAConnection *connection = self.connections[indexPath.row/2];

    if (indexPath.row % 2 == 0) {
        VAConnectionUserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ConnectionCell" forIndexPath:indexPath];
        cell.delegate = self;
        [cell configureCellWithConnection:connection
                              showActions:[self.shownActions[connection.userId] boolValue]];
        return cell;
    }
    
    VAConnectionActionsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActionsCell" forIndexPath:indexPath];
    cell.delegate = self;
    [cell configureCellWithConnection:connection];
    return cell;
}

#pragma mark - Table View delegate

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row % 2 == 0) {
        return 55.0f;
    } else {
        return 49.0f;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row % 2 == 0) {
        return UITableViewAutomaticDimension;
    } else {
        VAConnection *connection = self.connections[indexPath.row/2];
        return [self.shownActions[connection.userId] boolValue] ? UITableViewAutomaticDimension : 0.0f;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row % 2 == 0) {
        if (self.delegate) {
            [self.delegate connectionSelected:self.connections[indexPath.row/2]];
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Connection User Cell delegate

- (void)toggleActionsForConnection:(VAConnection *)connection {
    if ([self.shownActions[connection.userId] boolValue]) {
        [self hideActionsForConnection:connection];
        self.activeConnection = nil;
    } else {
        [self showActionsForConnection:connection];
        self.activeConnection = connection;
    }
}

#pragma mark - Connection Actions delegate

- (void)disconnectConnectionSelected:(VAConnection *)connection {
    [self disconnectConnection:connection];
}

- (void)messageToConnectionSelected:(VAConnection *)connection {
    
}

- (void)reportAboutConnectionSelected:(VAConnection *)connection {
    if (self.delegate) {
        [self.delegate reportSelectedForConnection:connection];
    }
}

- (void)blockConnectionSelected:(VAConnection *)connection {
    [self askConfirmationToBlockConnection:connection];
}

#pragma mark - Helpers

- (void)removeConnectionFromList:(VAConnection *)connection {
    NSInteger index = [self.connections indexOfObject:connection];
    if (index != NSNotFound) {
        NSArray *indexPaths = @[[NSIndexPath indexPathForRow:index*2 inSection:0],
                                [NSIndexPath indexPathForRow:index*2 + 1 inSection:0]];
        [self.connections removeObject:connection];
        [self.shownActions removeObjectForKey:connection.userId];
        
        [self updateConnectionsCount];
        
        [self.connectionsTableView beginUpdates];
        [self.connectionsTableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
        [self.connectionsTableView endUpdates];
    }
}

- (void)addConnectionToList:(VAConnection *)connection {
    NSInteger index = [self.connections indexOfObject:connection];
    if (index == NSNotFound) {
        [self.connections addObject:connection];
        [self.shownActions setObject:@(NO) forKey:connection.userId];
        
        NSSortDescriptor *desriptor = [[NSSortDescriptor alloc] initWithKey:@"fullname" ascending:YES];
        _connections = [[self.connections sortedArrayUsingDescriptors:@[desriptor]] mutableCopy];
        
        NSInteger index = [self.connections indexOfObject:connection];
        if (index != NSNotFound) {
            NSArray *indexPaths = @[[NSIndexPath indexPathForRow:index*2 inSection:0],
                                    [NSIndexPath indexPathForRow:index*2 + 1 inSection:0]];
            [self updateConnectionsCount];
            
            [self.connectionsTableView beginUpdates];
            [self.connectionsTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
            [self.connectionsTableView endUpdates];
        }
    }
}

- (void)disconnectConnection:(VAConnection *)connection {
    connection.isFriend = @(NO);
    [self removeConnectionFromList:connection];
    
    [[VAUserApiManager new] deleteConnectionWithID:connection.userId
                                    withCompletion:^(NSError *error, VAModel *model)
    {
        if (error) {
            connection.isFriend = @(YES);
            [VANotificationMessage showNetworkErrorMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
        } else {
            [VANotificationsService didDisconnectUser:connection];
        }
    }];
}

- (void)askConfirmationToBlockConnection:(VAConnection *)connection {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"Block %@", connection.fullname]
                                                                             message:[NSString stringWithFormat:@"Are you sure you want to block %@?", connection.fullname]
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"No"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * _Nonnull action) { }];
    [alertController addAction:noAction];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"Yes"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * _Nonnull action) {
                                                          [self blockConnection:connection];
                                                      }];
    [alertController addAction:yesAction];
    
    [[VAControlTool topScreenController] presentViewController:alertController animated:YES completion:nil];
}

- (void)blockConnection:(VAConnection *)connection {
    [self removeConnectionFromList:connection];
    
    connection.isBlocked = @(YES);

    [[VAUserApiManager new] blockUserWithID:connection.userId andCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            connection.isBlocked = @(NO);
            [VANotificationMessage showNetworkErrorMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
        } else {
            [VANotificationsService didBlockUser:connection];
        }
    }];
}

- (void)showActionsForConnection:(VAConnection *)connection {
    NSMutableArray *cellsToReload = [NSMutableArray array];
    if (self.activeConnection) {
        [self.shownActions setObject:@(NO) forKey:self.activeConnection.userId];
        NSInteger index = [self.connections indexOfObject:self.activeConnection];
        if (index != NSNotFound) {
            [cellsToReload addObject:[NSIndexPath indexPathForRow:index*2 inSection:0]];
        }
    }
    
    [self.shownActions setObject:@(YES) forKey:connection.userId];
    [self.connectionsTableView beginUpdates];
    [self.connectionsTableView reloadRowsAtIndexPaths:cellsToReload withRowAnimation:UITableViewRowAnimationNone];
    [self.connectionsTableView endUpdates];
}

- (void)hideActionsForConnection:(VAConnection *)connection {
    NSMutableArray *cellsToReload = [NSMutableArray array];
    if (self.activeConnection) {
        [self.shownActions setObject:@(NO) forKey:self.activeConnection.userId];
        NSInteger index = [self.connections indexOfObject:self.activeConnection];
        if (index != NSNotFound) {
            [cellsToReload addObject:[NSIndexPath indexPathForRow:index*2 inSection:0]];
        }
    }
    [self.shownActions setObject:@(NO) forKey:connection.userId];
    [self.connectionsTableView beginUpdates];
    [self.connectionsTableView reloadRowsAtIndexPaths:cellsToReload withRowAnimation:UITableViewRowAnimationNone];
    [self.connectionsTableView endUpdates];
}

@end
