//
//  VAYourConnectionsTabsView.h
//  vicinity-app
//
//  Created by Panda Systems on 11/9/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, YourConnectionsTabs) {
    YourConnectionsTabsYourConnections,
    YourConnectionsTabsConnectedToYou,
    YourConnectionsTabsEveryone
};

@protocol ConnectionsTabsDelegate <NSObject>

- (void)selectedTabChanged:(YourConnectionsTabs)tab;

@end

@interface VAYourConnectionsTabsView : UIView

@property (nonatomic, weak) id <ConnectionsTabsDelegate> delegate;
@property (nonatomic) YourConnectionsTabs selectedTab;

- (void)selectYourConnectionsTab;
- (void)selectConnectedToYouTab;
- (void)selectEveryoneTab;

@end
