//
//  VAPostActionsView.m
//  vicinity-app
//
//  Created by Panda Systems on 11/12/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAPostActionsView.h"

#import "VAPostActionsService.h"

@interface VAPostActionsView ()

@property (nonatomic, strong) UIView *upArrowView;
@property (nonatomic, strong) UIView *actionsContainerView;

@end

@implementation VAPostActionsView

- (instancetype)init {
    self = [super init];
    if (self) {
        _upArrowView = [[UIView alloc] init];
        _upArrowView.backgroundColor = [UIColor clearColor];
        [self addSubview:_upArrowView];
        [self setupArrowView];
        _actionsContainerView = [[UIView alloc] init];
        _actionsContainerView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_actionsContainerView];
    }
    return self;
}

- (void)awakeFromNib {
    [self setupArrowView];
    [self setupActionsContainerView];
}

#pragma mark - Setup

- (void)setupArrowView {
    UIBezierPath* trianglePath = [UIBezierPath bezierPath];
    [trianglePath moveToPoint:CGPointMake(0.0f, 4.0f)];
    [trianglePath addLineToPoint:CGPointMake(6.0f, 0.0f)];
    [trianglePath addLineToPoint:CGPointMake(12.0f, 3.0f)];
    [trianglePath closePath];
    
    CAShapeLayer *triangleMaskLayer = [CAShapeLayer layer];
    [triangleMaskLayer setPath:trianglePath.CGPath];
    triangleMaskLayer.fillColor = [UIColor whiteColor].CGColor;

    [self.upArrowView.layer addSublayer:triangleMaskLayer];
}

- (void)setupActionsContainerView {
    self.actionsContainerView.layer.borderColor = [UIColor colorWithRed:239.0f/255.0f
                                                                  green:239.0f/255.0f
                                                                   blue:239.0f/255.0f
                                                                  alpha:1].CGColor;
    self.actionsContainerView.layer.borderWidth = 0.5f;
    self.actionsContainerView.layer.shadowColor = [UIColor colorWithRed:239.0f/255.0f
                                                                  green:239.0f/255.0f
                                                                   blue:239.0f/255.0f
                                                                  alpha:1].CGColor;
    self.actionsContainerView.layer.shadowRadius = 1.0f;
    self.actionsContainerView.layer.shadowOpacity = 1.0f;
    self.actionsContainerView.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    self.actionsContainerView.layer.masksToBounds = NO;
}

#pragma mark - Properties

- (void)setPostActions:(NSArray *)postActions {
    _postActions = postActions;
    
    [self updateView];
}

#pragma mark - Configure view

- (void)updateView {
    for (UIView *v in self.actionsContainerView.subviews) {
        [v removeFromSuperview];
    }
    
    self.upArrowView.frame = CGRectMake(self.frame.size.width - 26.0f, 0, 12.0f, 4.0f);
    self.actionsContainerView.frame = CGRectMake(0, 2, self.frame.size.width, self.postActions.count*37.0f);
    
    for (int i = 0; i < self.postActions.count; i++) {
        VAPostActions action = [self.postActions[i] integerValue];
        UIView *actionView = [VAPostActionsService viewForPostAction:action withWidth:self.frame.size.width];
        actionView.frame = CGRectMake(0, actionView.frame.size.height*i, actionView.frame.size.width, actionView.frame.size.height);
        [self.actionsContainerView addSubview:actionView];
        
        SEL selector;
        switch (action) {
            case VAPostActionsEditPost:
                selector = @selector(editPostTap:);
                break;
            case VAPostActionsDeletePost:
                selector = @selector(deletePostTap:);
                break;
            case VAPostActionsBlockUser:
                selector = @selector(blockUserTap:);
                break;
            case VAPostActionsReportUser:
                selector = @selector(reportTap:);
                break;
            case VAPostActionsTurnOffNotifications:
                selector = @selector(turnOffNotificationsTap:);
                break;
            case VAPostActionsCopyPost:
                selector = @selector(copyPostTap:);
                break;
            case VAPostActionsReportPost:
                selector = @selector(reportPostTap:);
                break;
            case VAPostActionsTurnOnNotifications:
                selector = @selector(turnOnNotificationsTap:);
                break;
            case VAPostActionsSharePost:
                selector = @selector(sharePostTap:);
                break;
            default:
                break;
        }
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:selector];
        [actionView addGestureRecognizer:tap];
        
        if (i < self.postActions.count - 1) {
            UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(actionView.frame.origin.x, actionView.frame.origin.y + actionView.frame.size.height - 0.5f, actionView.frame.size.width, 0.5f)];
            separator.backgroundColor = [UIColor colorWithRed:239.0f/255.0f
                                                        green:239.0f/255.0f
                                                         blue:239.0f/255.0f
                                                        alpha:1];
            [self.actionsContainerView addSubview:separator];
        }
    }
}

#pragma mark - Actions

- (void)editPostTap:(id)sender {
    if (self.delegate) {
        [self.delegate editPostSelected];
    }
}

- (void)deletePostTap:(id)sender {
    if (self.delegate) {
        [self.delegate deletePostSelected];
    }
}

- (void)blockUserTap:(id)sender {
    if (self.delegate) {
        [self.delegate blockUserSelected];
    }
}

- (void)reportTap:(id)sender {
    if (self.delegate) {
        [self.delegate reportUserSelected];
    }
}

- (void)turnOffNotificationsTap:(id)sender {
    if (self.delegate) {
        [self.delegate turnOffNotificationsSelected];
    }
}

- (void)copyPostTap:(id)sender {
    if (self.delegate) {
        [self.delegate copyPostSelected];
    }
}

- (void)reportPostTap:(id)sender {
    if (self.delegate) {
        [self.delegate reportPostSelected];
    }
}

- (void)turnOnNotificationsTap:(id)sender {
    if (self.delegate) {
        [self.delegate turnOnNotificationsSelected];
    }
}

- (void)sharePostTap:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(sharePostSelected)]) {
        [self.delegate sharePostSelected];
    }
}

@end
