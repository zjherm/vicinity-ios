//
//  VAPostsTableViewCell.h
//  vicinity-app
//
//  Created by Panda Systems on 11/27/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VAPost;

@interface VAPostsTableViewCell : UITableViewCell

- (void)configureCellWithPost:(VAPost *)post
               postNameString:(NSAttributedString *)postNameString
               postTextString:(NSAttributedString *)postTextString;

@end
