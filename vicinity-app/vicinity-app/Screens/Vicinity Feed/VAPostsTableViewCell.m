//
//  VAPostsTableViewCell.m
//  vicinity-app
//
//  Created by Panda Systems on 11/27/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAPostsTableViewCell.h"

#import "VAPost.h"
#import "VAUser.h"

#import "NSDate+VAString.h"
#import "NSNumber+VAMilesDistance.h"

@interface VAPostsTableViewCell ()

@property (nonatomic, weak) IBOutlet UIView *containerView;

@property (nonatomic, weak) IBOutlet UIImageView *avatarImageView;
@property (nonatomic, weak) IBOutlet UITextView *nameTextView;
@property (nonatomic, weak) IBOutlet UILabel *timeLabel;
@property (nonatomic, weak) IBOutlet UILabel *distanceLabel;
@property (nonatomic, weak) IBOutlet UILabel *locationLabel;

@property (nonatomic, weak) IBOutlet UITextView *postTextView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *postTextViewHeightConstraint;

//@property (weak, nonatomic) IBOutlet UIImageView *locationImage;
//@property (weak, nonatomic) IBOutlet UIImageView *dateImage;
//@property (weak, nonatomic) IBOutlet UITextView *postTextView;
//@property (weak, nonatomic) IBOutlet UIImageView *likeImageView;
//@property (weak, nonatomic) IBOutlet UILabel *likeLabel;
//@property (weak, nonatomic) IBOutlet UIButton *viewAllCommentsButton;
//@property (weak, nonatomic) IBOutlet UITextView *firstCommentTextView;
//@property (weak, nonatomic) IBOutlet UITextView *secondCommentTextView;
//@property (weak, nonatomic) IBOutlet UIButton *likeButton;
//@property (weak, nonatomic) IBOutlet UILabel *likeButtonLabel;
//@property (weak, nonatomic) IBOutlet UILabel *commentButtonLabel;
//@property (weak, nonatomic) IBOutlet UIButton *commentButton;
//@property (weak, nonatomic) IBOutlet UIImageView *postImageView;
//@property (weak, nonatomic) IBOutlet UIView *likeView;
//@property (weak, nonatomic) IBOutlet UIButton *editButton;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *likesAndCommentsContainerHeight;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageContainerHeight;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textContainerHeight;

@property (nonatomic, strong) VAPost *post;

@end

@implementation VAPostsTableViewCell

- (void)awakeFromNib {
    [self setupContainerView];
    [self setupAvatarImageView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Setup

- (void)setupContainerView {
    self.containerView.layer.cornerRadius = 4.0f;
    self.containerView.clipsToBounds = YES;
}

- (void)setupAvatarImageView {
    self.avatarImageView.layer.borderWidth = 1.5/[UIScreen mainScreen].scale;
    self.avatarImageView.layer.borderColor = [UIColor colorWithRed:151.0f/255.0f
                                                             green:151.0f/255.0f
                                                              blue:151.0f/255.0f
                                                             alpha:1].CGColor;
    self.avatarImageView.layer.cornerRadius = 22.5f;
    self.avatarImageView.clipsToBounds = YES;
}

#pragma mark - Configure cell

- (void)configureCellWithPost:(VAPost *)post
               postNameString:(NSAttributedString *)postNameString
               postTextString:(NSAttributedString *)postTextString {
    self.post = post;
    
    self.avatarImageView.image = [UIImage imageNamed:@"avatar-placeholder"];
    [[VAImageCache sharedImageCache] getImageWithURL:post.user.avatarURL success:^(UIImage *image, NSString *imageURL) {
        if ([self.post.user.avatarURL isEqualToString:imageURL]) {
            self.avatarImageView.image = image;
        }
    } failure:nil];
    self.nameTextView.attributedText = postNameString;//[self decoratedNameStringForPost:post];
    self.timeLabel.text = [post.date messageTimeForDate];
    self.distanceLabel.text = post.checkIn.distance ? [post.checkIn.distance milesStringFromNSNumberMeters:NO] : @"< 0.5 mi";
    self.locationLabel.text = post.checkIn.cityName;
    self.postTextView.attributedText = postTextString;//[self decoratedPostStringFromString:post.text];
    self.postTextViewHeightConstraint.active = post.text && post.text.length ? NO : YES;
}

#pragma mark - Helpers

- (NSMutableAttributedString *)decoratedNameStringForPost:(VAPost *)post {
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] init];
    NSAttributedString *nameAttrString = [[NSAttributedString alloc] initWithString:post.user.nickname
                                                                         attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:58.0f/255.0f
                                                                                                                                      green:110.0f/255.0f
                                                                                                                                       blue:162.0f/255.0f
                                                                                                                                      alpha:1],
                                                                                      @"Author": @(YES)}];
    [attrString appendAttributedString:nameAttrString];
    
    if (post.checkIn.address) {
        NSAttributedString *checkInAttrString = [[NSAttributedString alloc] initWithString:@" checked in at "
                                                                                attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:74.0f/255.0f
                                                                                                                                             green:74.0f/255.0f
                                                                                                                                              blue:74.0f/255.0f
                                                                                                                                             alpha:1]}];
        [attrString appendAttributedString:checkInAttrString];
        
        NSAttributedString *locationAttrString = [[NSAttributedString alloc] initWithString:post.checkIn.name
                                                                                 attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:58.0f/255.0f
                                                                                                                                              green:110.0f/255.0f
                                                                                                                                               blue:162.0f/255.0f
                                                                                                                                              alpha:1],
                                                                                              @"CheckIn": @(YES)}];
        [attrString appendAttributedString:locationAttrString];
    }
    
    [attrString addAttribute:NSFontAttributeName
                       value:[UIFont fontWithName:@"Montserrat-Light" size:15.0f]
                       range:NSMakeRange(0, attrString.length)];
    
    return attrString;
}

- (NSMutableAttributedString *)decoratedPostStringFromString:(NSString *)stringWithTags {
    if (!stringWithTags) {
        return [[NSMutableAttributedString alloc] initWithString:@""];
    }
    NSError *error = nil;
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[#@](\\w+)" options:0 error:&error];
    
    NSArray *matches = [regex matchesInString:stringWithTags
                                      options:0
                                        range:NSMakeRange(0, stringWithTags.length)];
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:stringWithTags];
    [attString addAttribute:NSForegroundColorAttributeName
                      value:[UIColor colorWithRed:74.0f/255.0f
                                            green:74.0f/255.0f
                                             blue:74.0f/255.0f
                                            alpha:1]
                      range:NSMakeRange(0, stringWithTags.length)];
    [attString addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:@"Montserrat-Light" size:15.0f]
                      range:NSMakeRange(0, stringWithTags.length)];
    
    for (NSTextCheckingResult *match in matches) {
        NSRange wordRange = [match rangeAtIndex:0];
        
        //Set Foreground Color
        UIColor *foregroundColor = [UIColor colorWithRed:58.0f/255.0f
                                                   green:110.0f/255.0f
                                                    blue:162.0f/255.0f
                                                   alpha:1];
        [attString addAttribute:NSForegroundColorAttributeName
                          value:foregroundColor
                          range:wordRange];
        [attString addAttribute:@"Highlighted"
                          value:@(YES)
                          range:wordRange];
    }
    
    return attString;
}

@end
