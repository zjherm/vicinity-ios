//
//  VAPostActionsView.h
//  vicinity-app
//
//  Created by Panda Systems on 11/12/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol VAPostActionsDelegate <NSObject>

- (void)editPostSelected;
- (void)deletePostSelected;
- (void)blockUserSelected;
- (void)reportUserSelected;
- (void)turnOffNotificationsSelected;
- (void)copyPostSelected;
- (void)reportPostSelected;
- (void)turnOnNotificationsSelected;
- (void)sharePostSelected;
@end

@interface VAPostActionsView : UIView

@property (nonatomic, weak) id <VAPostActionsDelegate> delegate;

@property (nonatomic, strong) NSArray *postActions;

@end
