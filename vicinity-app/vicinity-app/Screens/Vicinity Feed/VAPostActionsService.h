//
//  VAPostActionsService.h
//  vicinity-app
//
//  Created by Panda Systems on 11/12/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, VAPostActions) {
    VAPostActionsEditPost,
    VAPostActionsDeletePost,
    VAPostActionsBlockUser,
    VAPostActionsReportUser,
    VAPostActionsTurnOffNotifications,
    VAPostActionsCopyPost,
    VAPostActionsReportPost,
    VAPostActionsTurnOnNotifications,
    VAPostActionsSharePost
};

@class VAPost;

@interface VAPostActionsService : NSObject

+ (NSArray *)postActionsForPost:(VAPost *)post;
+ (UIView *)viewForPostAction:(VAPostActions)postAction withWidth:(CGFloat)width;

@end
