//
//  VAPostActionsService.m
//  vicinity-app
//
//  Created by Panda Systems on 11/12/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAPostActionsService.h"

#import "VAUser.h"
#import "VAPost.h"
#import "VAUserDefaultsHelper.h"
#import "VADesignTool.h"

@implementation VAPostActionsService

+ (NSArray *)postActionsForPost:(VAPost *)post {
    NSMutableArray *actions = [NSMutableArray array];
    VAUser *me = (VAUser *)[VAUserDefaultsHelper getModelForKey:VASaveKeys_CurrentUser];
    [actions addObject:@(VAPostActionsSharePost)];
    
    if ([post.user.userId isEqualToString:me.userId]) {
        [actions addObject:@(VAPostActionsEditPost)];
        [actions addObject:@(VAPostActionsDeletePost)];
    } else {
        [actions addObject:@(VAPostActionsCopyPost)];
        [actions addObject:@(VAPostActionsReportPost)];
    }
    
    if ([post.isSubscribed boolValue] && post.user.status == VAUserStatusNormal) {
        [actions addObject:@(VAPostActionsTurnOffNotifications)];
    } else {
        [actions addObject:@(VAPostActionsTurnOnNotifications)];
    }
    
    if (![post.user.userId isEqualToString:me.userId]) {
        [actions addObject:@(VAPostActionsBlockUser)];
        [actions addObject:@(VAPostActionsReportUser)];
    }
    
    return actions;
}

+ (UIView *)viewForPostAction:(VAPostActions)postAction withWidth:(CGFloat)width {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 37.0f)];
    view.backgroundColor = [UIColor whiteColor];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40.0f, 37.0f)];
    imageView.contentMode = UIViewContentModeCenter;
    imageView.image = [VAPostActionsService imageForPostAction:postAction];
    [view addSubview:imageView];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(40.0f, 0.0f, width - 40.0f, 37.0f)];
    label.textColor = [UIColor colorWithRed:74.0f/255.0f
                                      green:74.0f/255.0f
                                       blue:74.0f/255.0f
                                      alpha:1];
    label.font = [[VADesignTool defaultDesign] vicinityRegularFontOfSize:12.f];
    label.textAlignment = NSTextAlignmentLeft;
    label.text = [VAPostActionsService textForPostAction:postAction];
    [view addSubview:label];
    
    return view;
}

+ (UIImage *)imageForPostAction:(VAPostActions)postAction {
    switch (postAction) {
        case VAPostActionsEditPost:
            return [UIImage imageNamed:@"postActionsEditPost"];
        case VAPostActionsDeletePost:
            return [UIImage imageNamed:@"postActionsDeletePost"];
        case VAPostActionsBlockUser:
            return [UIImage imageNamed:@"postActionsBlockUser"];
        case VAPostActionsReportUser:
            return [UIImage imageNamed:@"postActionsReport"];
        case VAPostActionsTurnOffNotifications:
            return [UIImage imageNamed:@"postActionsNotificationsOff"];
        case VAPostActionsCopyPost:
            return [UIImage imageNamed:@"postActionsCopy"];
        case VAPostActionsReportPost:
            return [UIImage imageNamed:@"postActionsReportPost"];
        case VAPostActionsTurnOnNotifications:
            return [UIImage imageNamed:@"postActionsNotificationsOn"];
        case VAPostActionsSharePost:
            return [UIImage imageNamed:@"sharePost"];
        default:
            break;
    }
    return nil;
}

+ (NSString *)textForPostAction:(VAPostActions)postAction {
    switch (postAction) {
        case VAPostActionsEditPost:
            return @"Edit Post";
        case VAPostActionsDeletePost:
            return @"Delete Post";
        case VAPostActionsBlockUser:
            return @"Block User";
        case VAPostActionsReportUser:
            return @"Report User";
        case VAPostActionsTurnOffNotifications:
            return @"Turn off notifications for this post";
        case VAPostActionsCopyPost:
            return @"Copy Post";
        case VAPostActionsReportPost:
            return @"Report Post";
        case VAPostActionsTurnOnNotifications:
            return @"Turn on notifications for this post";
        case VAPostActionsSharePost:
            return @"Share Post";
        default:
            break;
    }
    return nil;
}

@end
