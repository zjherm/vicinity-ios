//
//  VACommentsTableViewCell.h
//  vicinity-app
//
//  Created by Panda Systems on 11/26/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"

@class VAComment;

@protocol VACommentsInteractionDelegate <NSObject>

@optional
- (void)didSelectAuthorForComment:(VAComment *)comment;
- (void)didSelectHashtag:(NSString *)hashtag;
- (void)didSelectMention:(NSString *)mention;
- (void)didSelectReportComment:(VAComment *)comment;
- (void)didSelectCopyComment:(VAComment *)comment;
- (void)didSelectDeleteComment:(VAComment *)comment;
- (void)didSelectEditComment:(VAComment *)comment;

@end

@interface VACommentsTableViewCell : MGSwipeTableCell

@property (nonatomic, weak) id <VACommentsInteractionDelegate> commentDelegate;

- (void)configureCellWithComment:(VAComment *)comment;
- (CGFloat)getCellHeight;

@end
