//
//  VACommentsTableViewCell.m
//  vicinity-app
//
//  Created by Panda Systems on 11/26/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VACommentsTableViewCell.h"

#import "VAComment.h"
#import "VAUser.h"
#import "VAUserDefaultsHelper.h"

#import "NSDate+VAString.h"


static NSString *const kURLRegExpPattern = @"(http:\\/\\/|https\\:\\/\\/)?(([a-z0-9][a-z0-9\\-]*\\.)+[a-z0-9][a-z0-9\\-]+)(\\/[a-z0-9\\-]*)*(\\.[a-z]+)?";

@interface VACommentsTableViewCell ()

@property (nonatomic, weak) IBOutlet UIView *shadowView;
@property (nonatomic, weak) IBOutlet UIImageView *avatarImageView;
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UITextView *commentTextView;
@property (nonatomic, weak) IBOutlet UILabel *timeLabel;

@property (nonatomic, strong) VAComment *comment;

@end

@implementation VACommentsTableViewCell

- (void)awakeFromNib {
    [self setupAvatarImageView];
    [self addShadow];
    [self setupTaps];
    self.commentTextView.selectable = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Setup

- (void)setupAvatarImageView {
    self.avatarImageView.layer.borderWidth = 1.5/[UIScreen mainScreen].scale;
    self.avatarImageView.layer.borderColor = [UIColor colorWithRed:151.0f/255.0f
                                                             green:151.0f/255.0f
                                                              blue:151.0f/255.0f
                                                             alpha:1].CGColor;
    self.avatarImageView.layer.cornerRadius = 17.0f;
    self.avatarImageView.clipsToBounds = YES;
}

- (void)addBorderForAvatar {
    self.avatarImageView.layer.borderWidth = 1.5/[UIScreen mainScreen].scale;
}

- (void)removeBorderForAvatar {
    self.avatarImageView.layer.borderWidth = 0;
}

- (void)addShadow {
    self.shadowView.layer.shadowColor = [UIColor colorWithRed:239.0f/255.0f
                                                        green:239.0f/255.0f
                                                         blue:239.0f/255.0f
                                                        alpha:1].CGColor;
    self.shadowView.layer.shadowOpacity = 1.0f;
    self.shadowView.layer.shadowRadius = 0.5f;
    self.shadowView.layer.shadowOffset = CGSizeMake(0, 0.5f);
    self.shadowView.layer.masksToBounds = NO;
}

- (void)setupTaps {
    UITapGestureRecognizer *authorNameTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(authorNameTapped:)];
    [self.nameLabel addGestureRecognizer:authorNameTap];
    
    UITapGestureRecognizer *textTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(textTapped:)];
    [self.commentTextView addGestureRecognizer:textTap];

    UITapGestureRecognizer *cellTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cellTapped:)];
    [self.contentView addGestureRecognizer:cellTap];
}

#pragma mark - Configure cell

- (void)configureCellWithComment:(VAComment *)comment {
    self.comment = comment;
    
    self.avatarImageView.image = [UIImage imageNamed:@"commentDefaultAvatar"];
    if (comment.commentStatus != VACommentStatusDeleted) {
        [[VAImageCache sharedImageCache] getImageWithURL:comment.user.avatarURL success:^(UIImage *image, NSString *imageURL) {
            if ([self.comment.user.avatarURL isEqualToString:imageURL]) {
                self.avatarImageView.image = image;
            }
        } failure:nil];
        self.nameLabel.text = comment.user.nickname;
    }
    switch (comment.commentStatus) {
        case VACommentStatusActive:
            self.nameLabel.alpha = 1;
            self.commentTextView.attributedText = [self decoratedStringFromString:comment.text];
            [self addBorderForAvatar];
            break;
        case VACommentStatusRemoved:
            self.nameLabel.alpha = 1;
            self.commentTextView.attributedText = [self decoratedStringForDeletedComment];
            [self addBorderForAvatar];
            break;
        case VACommentStatusDeleted:
            self.nameLabel.text = @"Removed User";
            self.nameLabel.alpha = 0.5f;
            self.commentTextView.attributedText = [self decoratedStringForDeletedComment];
            [self removeBorderForAvatar];
            break;
        default:
            break;
    }
    
    self.timeLabel.text = [comment.date messageTimeForDate];

    self.rightButtons = [self getRightButtonsForComment:comment];
    self.rightSwipeSettings.transition = MGSwipeTransitionStatic;
}

#pragma mark - Height calculation

// Conffigure cell before call this method
- (CGFloat)getCellHeight {
    CGFloat verticalMargins = 21.0f;
    CGFloat textViewMargins = -13.0f;
    CGFloat nameLabelHeight = [self.nameLabel sizeThatFits:CGSizeMake(CGRectGetWidth(self.nameLabel.frame), CGFLOAT_MAX)].height;
    CGFloat commentTextViewHeight = [self.commentTextView sizeThatFits:CGSizeMake(CGRectGetWidth(self.commentTextView.frame), CGFLOAT_MAX)].height;
    CGFloat cellHeight = verticalMargins + textViewMargins + nameLabelHeight + commentTextViewHeight;
    return cellHeight;
}

#pragma mark - Actions

- (void)textTapped:(UITapGestureRecognizer *)recognizer {
    UITextView *textView = (UITextView *)recognizer.view;
    
    // Location of the tap in text-container coordinates
    
    NSLayoutManager *layoutManager = textView.layoutManager;
    CGPoint location = [recognizer locationInView:textView];
    location.x -= textView.textContainerInset.left;
    location.y -= textView.textContainerInset.top;
    
    // Find the character that's been tapped on
    
    NSUInteger characterIndex;
    characterIndex = [layoutManager characterIndexForPoint:location
                                           inTextContainer:textView.textContainer
                  fractionOfDistanceBetweenInsertionPoints:NULL];
    
    if (characterIndex < textView.textStorage.length) {
        
        NSRange range;
        id value = [textView.attributedText attribute:@"Highlighted" atIndex:characterIndex effectiveRange:&range];
        
        if (value) {
            NSString *tappedWord = [textView.text substringWithRange:range];
            if ([tappedWord length] && self.commentDelegate) {
                if ([[tappedWord substringToIndex:1] isEqualToString:@"#"]) {
                    [self.commentDelegate didSelectHashtag:[tappedWord substringFromIndex:1]];
                } else if ([[tappedWord substringToIndex:1] isEqualToString:@"@"]) {
                    [self.commentDelegate didSelectMention:[tappedWord substringFromIndex:1]];
                } else {
                    NSRegularExpression *urlRegExp = [NSRegularExpression regularExpressionWithPattern:kURLRegExpPattern
                                                                                               options:NSRegularExpressionCaseInsensitive
                                                                                                error:nil];
                    NSRange matchRange = [urlRegExp rangeOfFirstMatchInString:tappedWord options:0 range:NSMakeRange(0, tappedWord.length)];
                    if (matchRange.location != NSNotFound){
                        NSURL *url = [self urlWithMatchedString:tappedWord];
                        
                        if ([[UIApplication sharedApplication] canOpenURL:url]){
                            [[UIApplication sharedApplication] openURL:url];
                        }
                    }
                }
            }
        } else {
            [self cellTapped:recognizer];
        }
    }
}

- (void)authorNameTapped:(UITapGestureRecognizer *)recognizer {
    if (self.commentDelegate) {
        [self.commentDelegate didSelectAuthorForComment:self.comment];
    }
}

- (void)cellTapped:(UITapGestureRecognizer *)recognizer{
    MGSwipeAnimation* animation = [[MGSwipeAnimation alloc] init];
    animation.duration = 0.1;
    animation.easingFunction = MGSwipeEasingFunctionQuadOut;
    __weak MGSwipeTableCell* weakself = self;
    [self setSwipeOffset:-10.0 animation:animation completion:^(BOOL finished){
        if (finished) {
            animation.easingFunction = MGSwipeEasingFunctionQuadIn;
            [weakself setSwipeOffset:0.0 animation:animation completion:^(BOOL finished){

            }];
        }
    }];
}

#pragma mark - Helpers

- (NSURL *) urlWithMatchedString:(NSString *)urlString
{
    NSString * resultURLString = urlString.lowercaseString;
    if ([resultURLString hasPrefix:@"http"]) {
        return [NSURL URLWithString:resultURLString];
    }
    NSRange rangeOf = [resultURLString rangeOfString:@"/"];
    NSString *host = @"";
    NSString *path = @"/";
    if (rangeOf.location != NSNotFound) {
        host = [resultURLString substringToIndex:rangeOf.location];
        path = [resultURLString substringFromIndex:rangeOf.location];
        
    }
    else {
        host = resultURLString;
    }
    NSURL *url = [[NSURL alloc] initWithScheme:@"http" host:host path:path];
    return url;
}

- (NSMutableAttributedString *)decoratedStringFromString:(NSString *)stringWithTags {
    NSError *error = nil;
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[#@](\\w+)" options:0 error:&error];
    
    NSArray *matches = [regex matchesInString:stringWithTags
                                      options:0
                                        range:NSMakeRange(0, stringWithTags.length)];
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:stringWithTags];
    [attString addAttribute:NSForegroundColorAttributeName
                      value:[UIColor colorWithRed:74.0f/255.0f
                                            green:74.0f/255.0f
                                             blue:74.0f/255.0f
                                            alpha:1]
                      range:NSMakeRange(0, stringWithTags.length)];
    [attString addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:@"Montserrat-Light" size:12.0f]
                      range:NSMakeRange(0, stringWithTags.length)];
    
    for (NSTextCheckingResult *match in matches) {
        NSRange wordRange = [match rangeAtIndex:0];
        
        //Set Foreground Color
        UIColor *foregroundColor = [UIColor colorWithRed:58.0f/255.0f
                                                   green:110.0f/255.0f
                                                    blue:162.0f/255.0f
                                                   alpha:1];
        [attString addAttribute:NSForegroundColorAttributeName
                          value:foregroundColor
                          range:wordRange];
        [attString addAttribute:@"Highlighted"
                          value:@(YES)
                          range:wordRange];
    }
    
    //find url links
    NSRegularExpression *urlRegExp = [NSRegularExpression regularExpressionWithPattern:kURLRegExpPattern
                                                                               options:NSRegularExpressionCaseInsensitive
                                                                                 error:nil];
    NSArray *urlMatches = [urlRegExp matchesInString:stringWithTags
                                          options:0
                                            range:NSMakeRange(0, stringWithTags.length)];
    for (NSTextCheckingResult *match in urlMatches) {
        NSRange wordRange = [match rangeAtIndex:0];
        
        //Set Foreground Color
        UIColor *foregroundColor = [UIColor colorWithRed:58.0f/255.0f
                                                   green:110.0f/255.0f
                                                    blue:162.0f/255.0f
                                                   alpha:1];
        [attString addAttribute:NSForegroundColorAttributeName
                          value:foregroundColor
                          range:wordRange];
        [attString addAttribute:@"Highlighted"
                          value:@(YES)
                          range:wordRange];
    }

    return attString;
}



- (NSAttributedString *)decoratedStringForDeletedComment {
    return [[NSAttributedString alloc] initWithString:@"Comment has been removed" attributes:@{NSForegroundColorAttributeName:[self inactiveColor], NSFontAttributeName: [UIFont fontWithName:@"Montserrat-Light" size:12.0f]}];
}

- (UIColor *)inactiveColor {
    return [UIColor colorWithRed:155.0f/255.0f
                           green:155.0f/255.0f
                            blue:155.0f/255.0f
                           alpha:1];
}

- (NSArray *)getRightButtonsForComment:(VAComment *)comment {
    if (comment.commentStatus == VACommentStatusActive) {
        if ([self.comment.user.userId isEqualToString:[VAUserDefaultsHelper me].userId]) {
            return @[[self copySwipeButton],
                     [self deleteSwipeButton],
                     [self editSwipeButton]];
        } else {
            return @[[self copySwipeButton],
                     [self reportSwipeButton]];
        }
    } else {
        return @[];
    }
}

- (MGSwipeButton *)copySwipeButton {
    return [MGSwipeButton buttonWithTitle:@""
                                     icon:[UIImage imageNamed:@"commentCopy"]
                          backgroundColor:[UIColor colorWithRed:16.0f/255.0f
                                                          green:112.0f/255.0f
                                                           blue:179.0f/255.0f
                                                          alpha:1]
                                 callback:^BOOL(MGSwipeTableCell *sender)
            {
                if (self.commentDelegate) {
                    [self.commentDelegate didSelectCopyComment:self.comment];
                }
                return YES;
            }];
}

- (MGSwipeButton *)reportSwipeButton {
    return [MGSwipeButton buttonWithTitle:@""
                                     icon:[UIImage imageNamed:@"commentReport"]
                          backgroundColor:[UIColor colorWithRed:252.0f/255.0f
                                                          green:77.0f/255.0f
                                                           blue:83.0f/255.0f
                                                          alpha:1]
                                 callback:^BOOL(MGSwipeTableCell *sender)
            {
                if (self.commentDelegate) {
                    [self.commentDelegate didSelectReportComment:self.comment];
                }
                return YES;
            }];
}

- (MGSwipeButton *)deleteSwipeButton {
    return [MGSwipeButton buttonWithTitle:@""
                                     icon:[UIImage imageNamed:@"commentDelete"]
                          backgroundColor:[UIColor colorWithRed:252.0f/255.0f
                                                          green:77.0f/255.0f
                                                           blue:83.0f/255.0f
                                                          alpha:1]
                                 callback:^BOOL(MGSwipeTableCell *sender)
            {
                if (self.commentDelegate) {
                    [self.commentDelegate didSelectDeleteComment:self.comment];
                }
                return YES;
            }];
}

- (MGSwipeButton *)editSwipeButton {
    return [MGSwipeButton buttonWithTitle:@""
                                     icon:[UIImage imageNamed:@"editComment"]
                          backgroundColor:[UIColor colorWithRed:74.0f/255.0f
                                                          green:74.0f/255.0f
                                                           blue:74.0f/255.0f
                                                          alpha:1]
                                 callback:^BOOL(MGSwipeTableCell *sender)
            {
                if (self.commentDelegate) {
                    [self.commentDelegate didSelectEditComment:self.comment];
                }
                return YES;
            }];
}

@end
