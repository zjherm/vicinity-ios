//
//  VAReportTypeTableViewCell.h
//  vicinity-app
//
//  Created by Panda Systems on 11/11/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VAReportTypeTableViewCell : UITableViewCell

- (void)configureCellWithTitle:(NSString *)title
                      selected:(BOOL)selected
                    showShadow:(BOOL)showShadow;

@end
