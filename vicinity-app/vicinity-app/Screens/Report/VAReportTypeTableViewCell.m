//
//  VAReportTypeTableViewCell.m
//  vicinity-app
//
//  Created by Panda Systems on 11/11/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAReportTypeTableViewCell.h"

@interface VAReportTypeTableViewCell ()

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *separatorViewHeightConstraint;

@property (nonatomic, weak) IBOutlet UIView *shadowView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIImageView *selectedImageView;

@end

@implementation VAReportTypeTableViewCell

- (void)awakeFromNib {
    self.separatorViewHeightConstraint.constant = 0.5f;
    
    [self addShadow];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    self.selectedImageView.hidden = !selected;
}

#pragma mark - Setup

- (void)addShadow {
    self.shadowView.layer.shadowColor = [UIColor colorWithRed:155.0f/255.0f
                                                         green:155.0f/255.0f
                                                          blue:155.0f/255.0f
                                                         alpha:1].CGColor;
    self.shadowView.layer.shadowOpacity = 1.0f;
    self.shadowView.layer.shadowRadius = 0.5f;
    self.shadowView.layer.shadowOffset = CGSizeMake(0, 0.5f);
    self.shadowView.layer.masksToBounds = NO;
}

#pragma mark - Configure cell

- (void)configureCellWithTitle:(NSString *)title
                      selected:(BOOL)selected
                    showShadow:(BOOL)showShadow {
    self.titleLabel.text = title;
    self.selectedImageView.hidden = !selected;
    self.shadowView.hidden = !showShadow;
}

@end
