//
//  VAReportViewController.h
//  vicinity-app
//
//  Created by Panda Systems on 11/11/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VAUser;
@class VAPost;
@class VAComment;

@protocol VAReportSendingDelegate <NSObject>

- (void)didSendReportAboutUser:(VAUser *)user;

@end

@interface VAReportViewController : UIViewController

@property (nonatomic, weak) id <VAReportSendingDelegate> delegate;

@property (nonatomic, strong) VAUser *reportedUser;
@property (nonatomic, strong) VAPost *reportedPost;
@property (nonatomic, strong) VAComment *reportedComment;

@end
