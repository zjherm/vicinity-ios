//
//  VAReportManager.h
//  vicinity-app
//
//  Created by Panda Systems on 11/11/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, VAReportType) {
    VAReportTypeInappropriateContent,
    VAReportTypeRudeOrAbusive,
    VAReportTypeSendingSpam,
    VAReportTypeScammer,
    VAReportTypeSpamScam,
    VAReportTypeExplicitContent,
    VAReportTypeAbusive
};

@interface VAReportManager : NSObject

+ (NSArray *)userReportTypes;
+ (NSArray *)postReportTypes;
+ (NSArray *)commentReportTypes;
+ (NSString *)titleForReportType:(VAReportType)reportType;
+ (NSString *)stringForReportType:(VAReportType)reportType;

@end
