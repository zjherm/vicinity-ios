//
//  VAReportViewController.m
//  vicinity-app
//
//  Created by Panda Systems on 11/11/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAReportViewController.h"

#import "DEMONavigationController.h"
#import "VAUser.h"
#import "VAPost.h"
#import "VAComment.h"
#import "VAReportManager.h"
#import "VAUserApiManager.h"
#import "VANotificationMessage.h"

#import "VAReportTypeTableViewCell.h"

@interface VAReportViewController () <UITableViewDataSource, UITabBarDelegate>

@property (nonatomic, weak) IBOutlet UIBarButtonItem *cancelButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *sendButton;

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSArray *reportTypes;
@property (nonatomic) NSInteger selectedIndex;

@property (nonatomic, strong, readonly) NSString *target;
@property (nonatomic, strong, readonly) NSString *userId;
@property (nonatomic, strong, readonly) NSString *postId;
@property (nonatomic, strong, readonly) NSString *commentId;

@end

@implementation VAReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setCurrentReportTypes];
    self.selectedIndex = -1;
    
    [self setupNavigationBar];
    [self setupTitleLabel];
    [self registerNibsForCells];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup UI

- (void)setupNavigationBar {
    [(DEMONavigationController *)self.navigationController setDefaultNavigationBar];
    [(DEMONavigationController *)self.navigationController setTitle:@"Report"];
    [self customizeBarButtonItem:self.cancelButton];
    [self customizeBarButtonItem:self.sendButton];
}

- (void)customizeBarButtonItem:(UIBarButtonItem *)item {
    [item setTitleTextAttributes:@{
                                   NSFontAttributeName: [[VADesignTool defaultDesign] vicinityRegularFontOfSize:12.f],
                                   NSForegroundColorAttributeName: [UIColor whiteColor]
                                   }
                        forState:UIControlStateNormal];
    [item setTitleTextAttributes:@{
                                   NSFontAttributeName: [[VADesignTool defaultDesign] vicinityRegularFontOfSize:12.f],
                                   NSForegroundColorAttributeName: [UIColor colorWithWhite:1 alpha:0.5f]
                                   }
                        forState:UIControlStateDisabled];
}

- (void)setupTitleLabel {
    self.titleLabel.text = [NSString stringWithFormat:@"Why are you reporting this %@?", [self targetString]];
}

- (void)registerNibsForCells {
    UINib *reportCellNib = [UINib nibWithNibName:@"VAReportTypeTableViewCell" bundle:[NSBundle mainBundle]];
    [self.tableView registerNib:reportCellNib forCellReuseIdentifier:@"ReportCell"];
}

#pragma mark - Properties

- (void)setSelectedIndex:(NSInteger)selectedIndex {
    _selectedIndex = selectedIndex;
    
    self.sendButton.enabled = selectedIndex >= 0;
}

- (NSString *)target {
    NSString *target = @"user";
    if (self.reportedComment) {
        target = @"comment";
    } else if (self.reportedPost) {
        target = @"post";
    } else if (self.reportedUser) {
        target = @"user";
    }
    return target;
}

- (NSString *)targetString {
    NSString *target = @"account";
    if (self.reportedComment) {
        target = @"comment";
    } else if (self.reportedPost) {
        target = @"post";
    } else if (self.reportedUser) {
        target = @"account";
    }
    return target;
}

- (NSString *)userId {
    if (self.reportedComment) {
        return self.reportedComment.user.userId;
    } else if (self.reportedPost) {
        return self.reportedPost.user.userId;
    } else if (self.reportedUser) {
        return self.reportedUser.userId;
    }
    return nil;
}

- (NSString *)postId {
    if (self.reportedPost) {
        return self.reportedPost.postID;
    }
    return nil;
}

- (NSString *)commentId {
    if (self.reportedComment) {
        return self.reportedComment.commentID;
    }
    return nil;
}

#pragma mark - Actions

- (IBAction)cancelButtonTouchUp:(id)sender {
    [self dismissController];
}

- (IBAction)sendButtonTouchUp:(id)sender {
    if (self.userId) {
        [[VAUserApiManager new] sendReportAboutUserWithID:self.userId
                                            andReportType:[VAReportManager stringForReportType:[self.reportTypes[self.selectedIndex] integerValue]]
                                                andTarget:self.target
                                                andPostId:self.postId
                                             andCommentId:self.commentId
                                            andCompletion:^(NSError *error, VAModel *model)
        {
            if (error) {
                [VANotificationMessage showNetworkErrorMessageOnController:self withDuration:5.f];
            } else {
                if (self.delegate) {
                    [self.delegate didSendReportAboutUser:self.reportedUser];
                }
                [self dismissController];
            }
        }];
    } else {
        [VANotificationMessage showNetworkErrorMessageOnController:self withDuration:5.f];
    }
}

#pragma mark - Table View data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.reportTypes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VAReportTypeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ReportCell" forIndexPath:indexPath];
    [cell configureCellWithTitle:[VAReportManager titleForReportType:[self.reportTypes[indexPath.row] integerValue]]
                        selected:self.selectedIndex == indexPath.row
                      showShadow:indexPath.row != 0];
    return cell;
}

#pragma mark - Table View delegate

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.selectedIndex == indexPath.row) {
        self.selectedIndex = -1;
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    } else {
        self.selectedIndex = indexPath.row;
    }
}

#pragma mark - Helpers

- (void)dismissController {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setCurrentReportTypes {
    if (self.reportedComment) {
        self.reportTypes = [VAReportManager commentReportTypes];
    } else if (self.reportedPost) {
        self.reportTypes = [VAReportManager postReportTypes];
    } else if (self.reportedUser) {
        self.reportTypes = [VAReportManager userReportTypes];
    } else {
        self.reportTypes = [NSArray array];
    }
}

@end
