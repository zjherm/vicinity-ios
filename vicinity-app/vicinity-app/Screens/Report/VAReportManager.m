//
//  VAReportManager.m
//  vicinity-app
//
//  Created by Panda Systems on 11/11/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAReportManager.h"

@implementation VAReportManager

+ (NSArray *)userReportTypes {
    return @[@(VAReportTypeInappropriateContent),
             @(VAReportTypeRudeOrAbusive),
             @(VAReportTypeSendingSpam),
             @(VAReportTypeScammer)];
}

+ (NSArray *)postReportTypes {
    return @[@(VAReportTypeSpamScam),
             @(VAReportTypeExplicitContent),
             @(VAReportTypeAbusive)];
}

+ (NSArray *)commentReportTypes {
    return @[@(VAReportTypeSpamScam),
             @(VAReportTypeExplicitContent),
             @(VAReportTypeAbusive)];
}

+ (NSString *)titleForReportType:(VAReportType)reportType {
    switch (reportType) {
        case VAReportTypeInappropriateContent:
            return @"Inappropriate Content";
        case VAReportTypeRudeOrAbusive:
            return @"Rude or abusive";
        case VAReportTypeSendingSpam:
            return @"Sending Spam";
        case VAReportTypeScammer:
            return @"Scammer";
        case VAReportTypeSpamScam:
            return @"Spam/Scam";
        case VAReportTypeExplicitContent:
            return @"Explicit Content";
        case VAReportTypeAbusive:
            return @"Abusive";
        default:
            break;
    }
    return nil;
}

+ (NSString *)stringForReportType:(VAReportType)reportType {
    switch (reportType) {
        case VAReportTypeInappropriateContent:
            return @"InappropriateContent";
        case VAReportTypeRudeOrAbusive:
            return @"RudeOrAbusive";
        case VAReportTypeSendingSpam:
            return @"SendingSpam";
        case VAReportTypeScammer:
            return @"Scammer";
        case VAReportTypeSpamScam:
            return @"SpamScam";
        case VAReportTypeExplicitContent:
            return @"ExplicitContent";
        case VAReportTypeAbusive:
            return @"Abusive";
        default:
            break;
    }
    return nil;
}

@end
