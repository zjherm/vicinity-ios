//
//  VAConnectViewController.m
//  vicinity-app
//
//  Created by Panda Systems on 1/28/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import "VAConnectViewController.h"

#import "DEMONavigationController.h"
#import "VAFriendsViewController.h"
#import "VAConnectionsTabViewController.h"
#import "VABaseConnectionsListViewController.h"
#import "VAExploreTabViewController.h"
#import "VAAddFriendsViewController.h"
#import "VAUserDefaultsHelper.h"
#import "VAYourConnectionsTabsView.h"

//TemporaryHide
#import "VAInviteTabViewController.h"

@interface VAConnectViewController () <ConnectionsTabsDelegate>

@property (nonatomic, weak) IBOutlet VAYourConnectionsTabsView *tabsView;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;

@property (nonatomic, strong) VAConnectionsTabViewController *connectionsTabViewController;
@property (nonatomic, strong) VAExploreTabViewController *exploreTabViewController;

@end

@implementation VAConnectViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.tabsView.delegate = self;
    if (self.isInviteTabDefault) {
        [self.tabsView selectConnectedToYouTab];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setupNavigationBar];
    [self updateScrollView];
    [self addBadgeObservers];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self updateScrollView];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self removeBadgeObservers];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup UI

- (void)setupNavigationBar {
    [(DEMONavigationController *)self.navigationController setDefaultNavigationBar];
    [(DEMONavigationController *)self.navigationController setTitle:@"Connect"];
    self.navigationItem.leftBarButtonItem = [self getMenuButtonWithBadge:[VAUserDefaultsHelper shouldShowNotificationBadge]];
}

- (void)updateScrollView {
    switch (self.tabsView.selectedTab) {
        case YourConnectionsTabsYourConnections:
            self.scrollView.contentOffset = CGPointMake(0, 0);
            break;
        case YourConnectionsTabsConnectedToYou:
            self.scrollView.contentOffset = CGPointMake(self.scrollView.frame.size.width, 0);
            break;
        case YourConnectionsTabsEveryone:
            self.scrollView.contentOffset = CGPointMake(self.scrollView.frame.size.width*2, 0);
            break;
        default:
            break;
    }
}

#pragma mark - Connections Tabs delegate

- (void)selectedTabChanged:(YourConnectionsTabs)tab {
    switch (tab) {
        case YourConnectionsTabsYourConnections:
            [self selectYourConnectionsTab];
            break;
        case YourConnectionsTabsConnectedToYou:
            [self selectConnectedToYouTab];
            break;
        case YourConnectionsTabsEveryone:
            [self selectEveryoneTab];
            break;
        default:
            break;
    }
}

#pragma mark - Tabs selection

- (void)selectYourConnectionsTab {
    [self.view endEditing:YES];
    [self.tabsView selectYourConnectionsTab];
    
    [UIView animateWithDuration:0.3f animations:^{
        self.scrollView.contentOffset = CGPointMake(0, 0);
        [self.tabsView layoutIfNeeded];
    }];
}

- (void)selectConnectedToYouTab {
    [self.view endEditing:YES];
    [self.tabsView selectConnectedToYouTab];
    
    [UIView animateWithDuration:0.3f animations:^{
        self.scrollView.contentOffset = CGPointMake(self.scrollView.frame.size.width, 0);
        [self.tabsView layoutIfNeeded];
    }];
    
    BOOL TemporaryHide = true;
    if (TemporaryHide) {
        VAInviteTabViewController  *inviteVC = (VAInviteTabViewController *)self.childViewControllers[1];
        [inviteVC showShareSheet];
    }
    
}

- (void)selectEveryoneTab {
    [self.view endEditing:YES];
    [self.tabsView selectEveryoneTab];
    
    [UIView animateWithDuration:0.3f animations:^{
        self.scrollView.contentOffset = CGPointMake(self.scrollView.frame.size.width*2, 0);
        [self.tabsView layoutIfNeeded];
    }];
}

#pragma mark - Actions

- (IBAction)rightSwipeRecognized:(id)sender {
    CGPoint point = [(UISwipeGestureRecognizer *)sender locationOfTouch:0 inView:self.exploreTabViewController.view];
    switch (self.tabsView.selectedTab) {
        case YourConnectionsTabsYourConnections:
            break;
        case YourConnectionsTabsConnectedToYou:
            [self selectYourConnectionsTab];
            break;
        case YourConnectionsTabsEveryone:
            // TODO: get view frame instead of hadcoding values
            if (point.y < 72.0f || point.y > 173.0f) {
                [self selectConnectedToYouTab];
            } else if (([self.exploreTabViewController.recentlyViewedConnections count] + 1)*kConnectionCellWidth < [UIScreen mainScreen].bounds.size.width) {
                [self selectConnectedToYouTab];
            }
            break;
        default:
            break;
    }
}

- (IBAction)leftSwipeRecognized:(id)sender {
    CGPoint point = [(UISwipeGestureRecognizer *)sender locationOfTouch:0 inView:self.connectionsTabViewController.view];
    NSLog(@"Point: %f", point.y);
    switch (self.tabsView.selectedTab) {
        case YourConnectionsTabsYourConnections:
            // TODO: get view frame instead of hadcoding values
            if (point.y < 28.0f || (point.y > 129.0f && point.y < 161.0f) || point.y > 262.0f) {
                [self selectConnectedToYouTab];
            } else if (point.y > 27.0f && point.y < 130.0f && [self.connectionsTabViewController.yourConnections count]*kConnectionCellWidth < [UIScreen mainScreen].bounds.size.width) {
                [self selectConnectedToYouTab];
            } else if (point.y > 160.0f && point.y < 263.0f && [self.connectionsTabViewController.connectedToYouConnections count]*kConnectionCellWidth < [UIScreen mainScreen].bounds.size.width) {
                [self selectConnectedToYouTab];
            }
            break;
        case YourConnectionsTabsConnectedToYou:
            [self selectEveryoneTab];
            break;
        case YourConnectionsTabsEveryone:
            break;
        default:
            break;
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showFriends"]) {
        VAFriendsViewController *friendsVC = segue.destinationViewController;
        friendsVC.friends = [self.connectionsTabViewController.yourConnections mutableCopy];
    } else if ([segue.identifier isEqualToString:@"showConnectedToYou"]) {
        VABaseConnectionsListViewController *connectedToYouVC = segue.destinationViewController;
        connectedToYouVC.connections = [self.connectionsTabViewController.connectedToYouConnections mutableCopy];
    } else if ([segue.identifier isEqualToString:@"showFacebookFriends"]) {
        VABaseConnectionsListViewController *facebookFriendsVC = segue.destinationViewController;
        facebookFriendsVC.connections = [self.connectionsTabViewController.addFriendsViewController.facebookFriends mutableCopy];
    } else if ([segue.identifier isEqualToString:@"showContacts"]) {
        VABaseConnectionsListViewController *contactsVC = segue.destinationViewController;
        contactsVC.connections = [self.connectionsTabViewController.addFriendsViewController.contacts mutableCopy];
    } else if ([segue.identifier isEqualToString:@"showRecentlyViewed"]) {
        VABaseConnectionsListViewController *contactsVC = segue.destinationViewController;
        NSArray *recentlyViewed = self.exploreTabViewController.recentlyViewedConnections.count > 0 ? [[self.exploreTabViewController.recentlyViewedConnections subarrayWithRange:NSMakeRange(0, self.exploreTabViewController.recentlyViewedConnections.count - 1)] mutableCopy] : [NSArray array];
        contactsVC.connections = [recentlyViewed mutableCopy];
    } else if ([segue.identifier isEqualToString:@"embedConnections"]) {
        self.connectionsTabViewController = segue.destinationViewController;
    } else if ([segue.identifier isEqualToString:@"embedExplore"]) {
        self.exploreTabViewController = segue.destinationViewController;
    }
}

#pragma mark - Menu button badge

- (void)showButtonBadge {
    self.navigationItem.leftBarButtonItem = [self getMenuButtonWithBadge:YES];
}

- (void)hideButtonBadge {
    self.navigationItem.leftBarButtonItem = [self getMenuButtonWithBadge:NO];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kShouldShowNotificationBadge];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

#pragma mark - Gesture recognizer handling

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

@end
