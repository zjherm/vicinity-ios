//
//  VAConnectionsCollectionView.m
//  vicinity-app
//
//  Created by Panda Systems on 1/28/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import "VAConnectionsCollectionView.h"

#import "VAConnectionCollectionViewCell.h"

@implementation VAConnectionsCollectionView

- (id)initWithCoder:(NSCoder *)aDecoder {
    if( self = [super initWithCoder:aDecoder]) {
        self.delegate = self;
        self.dataSource = self;
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        [flowLayout setItemSize:CGSizeMake(65.0f, 101.0f)];
        [flowLayout setSectionInset:UIEdgeInsetsMake(0, 3.0f, 0, 3.0f)];
        [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        flowLayout.minimumLineSpacing = 0;
        flowLayout.minimumInteritemSpacing = 0;
        [self setCollectionViewLayout:flowLayout];
        
        self.backgroundColor = [UIColor clearColor];
        
        self.bounces = YES;
        self.alwaysBounceHorizontal = NO;
        self.alwaysBounceVertical = NO;
        self.clipsToBounds = YES;
        
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator = NO;
        
        [self registerNib:[UINib nibWithNibName:@"VAConnectionCollectionViewCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"ConnectionCellIdentifier"];
    }
    return self;
}

#pragma mark - Properties

- (void)setConnections:(NSMutableArray *)connections {
    _connections = connections;
    
    [self reloadData];
}

#pragma mark - Collection View data source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.connections count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    VAConnectionCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ConnectionCellIdentifier" forIndexPath:indexPath];
    [cell configureCellWithUser:self.connections[indexPath.row]];
    return cell;
}

#pragma mark - Collection View delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.listDelegate) {
        [self.listDelegate collectionView:self didSelectUser:self.connections[indexPath.row]];
    }
}

@end
