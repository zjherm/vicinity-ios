//
//  VAAddFriendsViewController.m
//  vicinity-app
//
//  Created by Panda Systems on 2/1/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import "VAAddFriendsViewController.h"

#import "VAUserDefaultsHelper.h"
#import "VAUser.h"
#import "VAContact.h"
#import "VAUserSearchResponse.h"
#import "VALoginResponse.h"

#import "VAContactManager.h"
#import "VAUserApiManager.h"
#import "VANotificationMessage.h"
#import "VAControlTool.h"

#import <FBSDKLoginKit.h>
#import <FBSDKCoreKit.h>
#import <AddressBook/AddressBook.h>

static NSString *const kFindFacebookFriendsText = @"Find Facebook Friends";
static NSString *const kFacebookFriendsOnVicinityText = @"%lu Facebook Friends on Vicinity";
static NSString *const kFacebookOneFriendOnVicinityText = @"%lu Facebook Friend on Vicinity";
static NSString *const kFindTwitterFriendsText = @"Find Twitter Friends";
static NSString *const kTwitterFriendsOnVicinityText = @"%lu Twitter Friends on Vicinity";
static NSString *const kTwitterOneFriendOnVicinityText = @"%lu Twitter Friend on Vicinity";
static NSString *const kFindContactsText = @"Find Contacts";
static NSString *const kContactsOnVicinityText = @"%lu Contacts on Vicinity";
static NSString *const kOneContactOnVicinityText = @"%lu Contact on Vicinity";

@interface VAAddFriendsViewController ()

@property (nonatomic, weak) IBOutlet UILabel *facebookFriendsLabel;
@property (nonatomic, weak) IBOutlet UILabel *twitterFriendsLabel;
@property (nonatomic, weak) IBOutlet UILabel *contactsLabel;

@end

@implementation VAAddFriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadContactsContent];
    [self loadFacebookFriendsContent];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup UI

- (void)updateLabels {
    
}

#pragma mark - Actions

- (IBAction)facebookFriendsViewTap:(id)sender {
    if ([self hasFacebookFriendsPermissions]) {
        [self.parentViewController.parentViewController performSegueWithIdentifier:@"showFacebookFriends" sender:nil];
    } else if ([self hasLinkedFacebookAccount]) {
        [self askFacebookFriendsPermissions];
    } else {
        [self connectAccountToFacebook];
    }
}

- (IBAction)twitterFriendsViewTap:(id)sender {
    // TODO: implement
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:@"This feature is not implemented yet."
                                                   delegate:self
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
}

- (IBAction)contactsViewTap:(id)sender {
    if ([self hasContactsAccess]) {
        [self.parentViewController.parentViewController performSegueWithIdentifier:@"showContacts" sender:nil];
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied || ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusRestricted) {
        [self showGoToSettingsMessage];
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        [self askContactsPermissions];
    }
}

#pragma mark - Helpers

- (BOOL)hasLinkedFacebookAccount {
    VAUser *user = (VAUser *)[VAUserDefaultsHelper getModelForKey:VASaveKeys_CurrentUser];
    return user.facebookID ? YES : NO;
}

- (BOOL)hasFacebookFriendsPermissions {
    FBSDKAccessToken *accessToken = [FBSDKAccessToken currentAccessToken];
    
    if (![self hasLinkedFacebookAccount]) {
        return NO;
    } else if (!accessToken || ![[accessToken permissions] containsObject:@"user_friends"]) {
        return NO;
    }
    return YES;
}

- (BOOL)hasContactsAccess {
    return ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized;
}

- (void)loadFacebookFriendsContent {
    if (![self hasFacebookFriendsPermissions]) {
        self.facebookFriendsLabel.text = kFindFacebookFriendsText;
    } else {
        [self getFacebookFriends];
    }
}

- (void)loadContactsContent {
    if ([self hasContactsAccess]) {
        [self loadAllContacts];
    } else {
        self.contactsLabel.text = kFindContactsText;
    }
}

- (void)loadAllContacts {
    _contacts = [NSArray array];
    NSArray *contacts = [VAContactManager getAllContacts];
    contacts = [self sortContacts:contacts];
    
    NSMutableArray *temp = [NSMutableArray array];
    for (NSDictionary *userInfo in contacts) {
        VAContact *contact = [[VAContact alloc] initWithDict:userInfo];
        [temp addObject:contact];
    }
    contacts = temp;
    
    NSArray *emails = [contacts valueForKeyPath:@"@distinctUnionOfArrays.emails"];
    [self performSelectorOnMainThread:@selector(sendGetVicinityUsersRequestForEmailsArray:) withObject:emails waitUntilDone:NO];
}

- (NSArray *)sortContacts:(NSArray *)contacts {
    ABPersonSortOrdering sortOrder = ABPersonGetSortOrdering();
    if (sortOrder == kABPersonSortByFirstName) {
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"firstName" ascending:YES selector:@selector(caseInsensitiveCompare:)];
        return [contacts sortedArrayUsingDescriptors:@[sortDescriptor]];
    } else {
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"lastName" ascending:YES selector:@selector(caseInsensitiveCompare:)];
        return [contacts sortedArrayUsingDescriptors:@[sortDescriptor]];
    }
}

- (void)sendGetVicinityUsersRequestForEmailsArray:(NSArray *)emails {
    NSString *emailsList = [emails componentsJoinedByString:@", "];
    
    [[VAUserApiManager new] getUsersByEmailListSearch:emailsList
                                       withCompletion:^(NSError *error, VAModel *model) {
                                           if (error) {
                                               self.contactsLabel.text = kFindContactsText;
                                           } else if (model) {
                                               VAUser *user = (VAUser *)[VAUserDefaultsHelper getModelForKey:VASaveKeys_CurrentUser];
                                               NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId != %@", user.userId];
                                               VAUserSearchResponse *response = (VAUserSearchResponse *)model;
                                               self.contacts = [response.users filteredArrayUsingPredicate:predicate];
                                               self.contactsLabel.text = [NSString stringWithFormat:self.contacts.count == 1? kOneContactOnVicinityText : kContactsOnVicinityText, (unsigned long)self.contacts.count];
                                           }
                                       }];
}

- (void)showGoToSettingsMessage {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Allow access"
                                                                             message:NSLocalizedString(@"contacts.permissions.denied", nil)
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }];
    [alertController addAction:actionOk];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)askContactsPermissions {
    [VAContactManager askContactsPermissionsWithComplition:^(BOOL granted) {
        if (granted) {
            [self performSelectorOnMainThread:@selector(loadAllContacts) withObject:nil waitUntilDone:NO];
        }
    }];
}

- (void)askFacebookFriendsPermissions {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    NSArray *permissions = @[@"user_friends"];
    [login logInWithReadPermissions:permissions handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            [VANotificationMessage showLoginCancelledMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
        } else {
            if ([[result declinedPermissions] containsObject:@"user_friends"]) {
                
            } else {
                [self getFacebookFriends];
            }
        }
    }];
}

- (void)connectAccountToFacebook {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    NSArray *permissions = @[@"public_profile", @"user_location", @"email", @"user_friends"];
    [login logInWithReadPermissions:permissions handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            [VANotificationMessage showLoginCancelledMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
        } else {
            if ([[result declinedPermissions] containsObject:@"user_friends"]) {
                
            } else {
                FBSDKAccessToken *accessToken = [FBSDKAccessToken currentAccessToken];
                NSString *tokenString = [accessToken tokenString];
                
                [[VAUserApiManager new] postCurrentSessionAddLinkToFacebookToken:tokenString
                                                                  withCompletion:^(NSError *error, VAModel *model) {
                                                                      if (model) {
                                                                          VALoginResponse *response = (VALoginResponse *)model;
                                                                          VAUser *user = response.loggedUser;
                                                                          [VAUserDefaultsHelper saveModel:user forKey:VASaveKeys_CurrentUser];
                                                                          [VAUserDefaultsHelper setAuthToken:response.sessionId];
                                                                          
                                                                          [self getFacebookFriends];
                                                                      } else if ([error.localizedDescription isEqual:@(100015)]) {
                                                                          [VANotificationMessage showFacebookInUseMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
                                                                      }
                                                                  }];
            }
        }
    }];
}

- (void)getFacebookFriends {
    VAUserApiManager *manager = [VAUserApiManager new];
    [manager getFacebookFriendsWithCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            self.facebookFriendsLabel.text = kFindFacebookFriendsText;
            [VANotificationMessage showLoadErrorMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
        } else if (model) {
            VAUserSearchResponse *response = (VAUserSearchResponse *)model;
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userStatus == %@", @"Normal"];
            self.facebookFriends = [response.users filteredArrayUsingPredicate:predicate];
            
            self.facebookFriendsLabel.text = [NSString stringWithFormat:self.facebookFriends.count == 1 ? kFacebookOneFriendOnVicinityText : kFacebookFriendsOnVicinityText, (unsigned long)self.facebookFriends.count];
        }
    }];
}

@end
