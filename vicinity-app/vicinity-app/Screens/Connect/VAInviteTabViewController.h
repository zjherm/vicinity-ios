//
//  VAInviteTabViewController.h
//  vicinity-app
//
//  Created by Panda Systems on 2/1/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VAInviteTabViewController : UIViewController


//TemporaryHide
- (void)showShareSheet;

@end
