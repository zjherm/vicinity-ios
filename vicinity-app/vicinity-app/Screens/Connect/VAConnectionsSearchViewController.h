//
//  VAConnectionsSearchViewController.h
//  vicinity-app
//
//  Created by Panda Systems on 1/28/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VAConnectionsSearchDelegate <NSObject>
- (void)searchTextChanged:(NSString *)searchText;
@optional
- (void)didPressCancelButton;
- (void)didSearchTextFieldBecomeActive:(UITextField *)searchTextField;
@end

@interface VAConnectionsSearchViewController : UIViewController

@property (nonatomic, weak) id<VAConnectionsSearchDelegate> delegate;

- (void)clearTextField;

@end
