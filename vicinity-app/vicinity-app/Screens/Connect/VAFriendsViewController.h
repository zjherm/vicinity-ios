//
//  VAFriendsViewController.h
//  vicinity-app
//
//  Created by Panda Systems on 1/28/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VAFriendsViewController : UIViewController
@property (nonatomic, strong) NSMutableArray *friends;
@end
