//
//  VAConnectedToYouViewController.h
//  vicinity-app
//
//  Created by Panda Systems on 1/29/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VABaseConnectionsListViewController.h"

@interface VAConnectedToYouViewController : VABaseConnectionsListViewController

@end
