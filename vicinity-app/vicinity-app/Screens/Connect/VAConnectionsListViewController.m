//
//  VAConnectionsListViewController.m
//  vicinity-app
//
//  Created by Panda Systems on 1/29/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import "VAConnectionsListViewController.h"

#import "VAProfileViewController.h"

#import "VAConnectionSimpleTableViewCell.h"

#import "VAConnection.h"

static NSString *const kConnectionsCountText = @"Count: %lu";

@interface VAConnectionsListViewController () <UITableViewDataSource, UITableViewDelegate, VAConnectionSimpleCellDelegate>

@property (nonatomic, weak) IBOutlet UILabel *countLabel;
@property (nonatomic, weak) IBOutlet UIButton *connectToAllButton;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation VAConnectionsListViewController

@synthesize connections = _connections;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerNibsForCells];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup

- (void)registerNibsForCells {
    UINib *userCellNib = [UINib nibWithNibName:@"VAConnectionSimpleTableViewCell" bundle:[NSBundle mainBundle]];
    [self.tableView registerNib:userCellNib forCellReuseIdentifier:@"ConnectionCell"];
}

- (void)updateUI {
    [self updateCountLabel];
    self.connectToAllButton.hidden = self.showConnectToAllButton ? [[self disconnectedConnections] count] > 0 ? NO : YES : YES;
}

- (void)updateCountLabel {
    self.countLabel.text = [NSString stringWithFormat:kConnectionsCountText, (unsigned long)[self.connections count]];
}

#pragma mark - Actions

- (IBAction)connectToAllButtonPressed:(id)sender {
    if (self.delegate) {
        [self.delegate connectToAllSelectedForUsers:[self disconnectedConnections]];
    }
}

#pragma mark - Properties

- (void)setConnections:(NSMutableArray *)connections {
    _connections = connections;
    
    [self.tableView reloadData];
    [self updateUI];
}

- (NSMutableArray *)connections {
    if (!_connections) {
        _connections = [NSMutableArray array];
    }
    return _connections;
}

#pragma mark - Table View data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.connections.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VAConnection *connection = self.connections[indexPath.row];
    
    VAConnectionSimpleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ConnectionCell" forIndexPath:indexPath];
    cell.delegate = self;
    [cell configureCellWithConnection:connection];
    return cell;
}

#pragma mark - Table View delegate

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self showProfileForUserWithAlias:((VAConnection *)self.connections[indexPath.row]).nickname];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - VAConnectionSimpleCellDelegate

- (void)addConnectionSelected:(VAConnection *)connection {
    if (self.delegate) {
        [self.delegate connectConnectionSelected:connection];
    }
}

- (void)removeConnectionSelected:(VAConnection *)connection {
    if (self.delegate) {
        [self.delegate disconnectConnectionSelected:connection];
    }
}

#pragma mark - Helpers

- (NSArray *)disconnectedConnections {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isFriend == NO"];
    return [self.connections filteredArrayUsingPredicate:predicate];
}

- (void)showProfileForUserWithAlias:(NSString *)alias {
    VAProfileViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"profileViewController"];
    vc.searchAlias = alias;
    vc.isUsersAccountMode = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Public API

- (void)updateConnectionInList:(VAConnection *)connection {
    NSInteger index = [self.connections indexOfObject:connection];
    if (index != NSNotFound) {
        NSArray *indexPaths = @[[NSIndexPath indexPathForRow:index inSection:0]];
        [self.connections replaceObjectAtIndex:index withObject:connection];
        
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView endUpdates];
        
        [self updateUI];
    }
}

@end
