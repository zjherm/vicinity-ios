//
//  VARecentlyViewedViewController.m
//  vicinity-app
//
//  Created by Panda Systems on 2/11/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import "VARecentlyViewedViewController.h"

@interface VARecentlyViewedViewController ()

@end

@implementation VARecentlyViewedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self updateUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setupNavigationBarWithTitle:@"Recently Viewed"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup UI

- (void)updateUI {
    self.listViewController.connections = [[self filteredConnections] mutableCopy];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];
}

@end
