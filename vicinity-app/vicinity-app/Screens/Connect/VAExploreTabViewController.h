//
//  VAExploreTabViewController.h
//  vicinity-app
//
//  Created by Panda Systems on 2/2/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VABaseConnectionsListViewController.h"

@interface VAExploreTabViewController : VABaseConnectionsListViewController

@property (nonatomic, strong, readonly) NSArray *recentlyViewedConnections;

@end
