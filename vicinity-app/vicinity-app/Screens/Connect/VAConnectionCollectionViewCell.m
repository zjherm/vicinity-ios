//
//  VAConnectionCollectionViewCell.m
//  vicinity-app
//
//  Created by Panda Systems on 1/28/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import "VAConnectionCollectionViewCell.h"

#import "VAUser.h"

@interface VAConnectionCollectionViewCell ()

@property (nonatomic, weak) IBOutlet UIImageView *avatarImageView;
@property (nonatomic, weak) IBOutlet UILabel *nicknameLabel;
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;

@property (nonatomic, strong) VAUser *user;

@end

@implementation VAConnectionCollectionViewCell

- (void)awakeFromNib {
    [self setupAvatarImageView];
}

#pragma mark - Setup

- (void)setupAvatarImageView {
    self.avatarImageView.layer.borderWidth = 1.5/[UIScreen mainScreen].scale;
    self.avatarImageView.layer.borderColor = [UIColor colorWithRed:151.0f/255.0f
                                                             green:151.0f/255.0f
                                                              blue:151.0f/255.0f
                                                             alpha:1].CGColor;
    self.avatarImageView.layer.cornerRadius = 22.5f;
    self.avatarImageView.clipsToBounds = YES;
}

#pragma mark - Configure cell

- (void)configureCellWithUser:(VAUser *)user {
    self.user = user;
    
    self.avatarImageView.image = [user.nickname length] ? [UIImage imageNamed:@"commentDefaultAvatar"] : [UIImage imageNamed:@"clearRecent"];
    [[VAImageCache sharedImageCache] getImageWithURL:user.avatarURL success:^(UIImage *image, NSString *imageURL) {
        if ([self.user.avatarURL isEqualToString:imageURL]) {
            self.avatarImageView.image = image;
        }
    } failure:nil];
    self.nicknameLabel.text = user.nickname;
    self.nameLabel.text = [user.fullname stringByReplacingOccurrencesOfString:@" " withString:@"\n"];
}

@end
