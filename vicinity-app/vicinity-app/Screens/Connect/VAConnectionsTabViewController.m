//
//  VAConnectionsTabViewController.m
//  vicinity-app
//
//  Created by Panda Systems on 1/28/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import "VAConnectionsTabViewController.h"

#import "VAProfileViewController.h"
#import "VAAddFriendsViewController.h"

#import "VAConnectionsCollectionView.h"

#import "VAUserApiManager.h"
#import "VAControlTool.h"
#import "VANotificationMessage.h"
#import "VAConnectionsResponse.h"
#import "VAConnection.h"

static NSString *const kYourConnectionsCountText = @"Your Connections: %lu";
static NSString *const kConnectedToYouCountText = @"Connected To You: %lu";
CGFloat const kConnectionCellWidth = 65.0f;

@interface VAConnectionsTabViewController () <VAConnectionsListViewDelegate>
// Outlets
@property (nonatomic, weak) IBOutlet UILabel *connectionsCountLabel;
@property (nonatomic, weak) IBOutlet UILabel *connectedToYouCountLabel;
@property (nonatomic, weak) IBOutlet UIButton *seeAllConnectionsButton;
@property (nonatomic, weak) IBOutlet UIButton *seeAllConnectedToYouButton;
@property (nonatomic, weak) IBOutlet VAConnectionsCollectionView *yourConnectionsCollectionView;
@property (nonatomic, weak) IBOutlet VAConnectionsCollectionView *connectedToYouCollectionView;
@property (nonatomic, weak) IBOutlet UIView *noConnectionsView;
@property (nonatomic, weak) IBOutlet UIView *noConnectedToYouView;

@property (nonatomic, strong, readwrite) NSMutableArray *yourConnections;
@property (nonatomic, strong, readwrite) NSMutableArray *connectedToYouConnections;

@end

@implementation VAConnectionsTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.yourConnectionsCollectionView.listDelegate = self;
    self.connectedToYouCollectionView.listDelegate = self;
    
    [self setupObservers];

    [self loadYourConnections];
    [self loadConnectedToYou];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Setup UI

- (void)setupObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didUserConnected:)
                                                 name:VAUserDidConnectedNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didUserDisconnected:)
                                                 name:VAUserDidDisconnectedNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didUserDisconnected:)
                                                 name:VAUserDidBlockedNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didUserUnblocked:)
                                                 name:VAUserDidUnblockedNotification
                                               object:nil];
}

- (void)updateUI {
    self.connectionsCountLabel.text = [NSString stringWithFormat:kYourConnectionsCountText, (unsigned long)[self.yourConnections count]];
    self.connectedToYouCountLabel.text = [NSString stringWithFormat:kConnectedToYouCountText, (unsigned long)[self.connectedToYouConnections count]];
    
    self.yourConnectionsCollectionView.hidden = !([self.yourConnections count] > 0);
    self.connectedToYouCollectionView.hidden = !([self.connectedToYouConnections count] > 0);
    
    self.noConnectionsView.hidden = [self.yourConnections count] > 0;
    self.noConnectedToYouView.hidden = [self.connectedToYouConnections count] > 0;
    
    self.seeAllConnectionsButton.hidden = [self.yourConnections count]*kConnectionCellWidth < [UIScreen mainScreen].bounds.size.width;
    self.seeAllConnectedToYouButton.hidden = [self.connectedToYouConnections count]*kConnectionCellWidth < [UIScreen mainScreen].bounds.size.width;
}

#pragma mark - Actions

- (IBAction)seeAllConnectionsButtonPressed:(id)sender {
    [self.parentViewController performSegueWithIdentifier:@"showFriends" sender:nil];
}

- (IBAction)seeAllConnectedToYouButtonPressed:(id)sender {
    [self.parentViewController performSegueWithIdentifier:@"showConnectedToYou" sender:nil];
}

#pragma mark - Helpers

- (void)loadYourConnections {
    VAUserApiManager *manager = [VAUserApiManager new];
    [manager getConnectionsWithCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            [VANotificationMessage showLoadErrorMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
        } else if (model) {
            VAConnectionsResponse *response = (VAConnectionsResponse *)model;
            
            NSSortDescriptor *desriptor = [[NSSortDescriptor alloc] initWithKey:@"fullname" ascending:YES];
            NSArray *sorted = [response.connections sortedArrayUsingDescriptors:@[desriptor]];
            
            self.yourConnections = [sorted mutableCopy];
            self.yourConnectionsCollectionView.connections = [self.yourConnections mutableCopy];
            [self updateUI];
        }
    }];
}

- (void)loadConnectedToYou {
    VAUserApiManager *manager = [VAUserApiManager new];
    [manager getFollowersWithCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            [VANotificationMessage showLoadErrorMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
        } else if (model) {
            VAConnectionsResponse *response = (VAConnectionsResponse *)model;
            
            NSSortDescriptor *desriptor = [[NSSortDescriptor alloc] initWithKey:@"fullname" ascending:YES];
            NSArray *sorted = [response.connections sortedArrayUsingDescriptors:@[desriptor]];
            
            self.connectedToYouConnections = [sorted mutableCopy];
            self.connectedToYouCollectionView.connections = [self.connectedToYouConnections mutableCopy];
            
            [self updateUI];
        }
    }];
}

- (void)showProfileForUserWithAlias:(NSString *)alias {
    VAProfileViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"profileViewController"];
    vc.searchAlias = alias;
    vc.isUsersAccountMode = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Connections List View delegate

- (void)collectionView:(VAConnectionsCollectionView *)collectionView didSelectUser:(VAUser *)user {
    [self showProfileForUserWithAlias:user.nickname];
}

#pragma mark - Observers

- (void)didUserConnected:(NSNotification *)notification {
    VAConnection *user = notification.userInfo[@"user"];
    
    // Your connections
    [self.yourConnections addObject:user];
    NSSortDescriptor *desriptor = [[NSSortDescriptor alloc] initWithKey:@"fullname" ascending:YES];
    self.yourConnections = [[self.yourConnections sortedArrayUsingDescriptors:@[desriptor]] mutableCopy];
    self.yourConnectionsCollectionView.connections = [self.yourConnections mutableCopy];
    
    // Connected To You
    NSInteger connectedToYouIndex = [self.connectedToYouConnections indexOfObject:user];
    if (connectedToYouIndex != NSNotFound) {
        [self.connectedToYouConnections replaceObjectAtIndex:connectedToYouIndex withObject:user];
        self.connectedToYouCollectionView.connections = [self.connectedToYouConnections mutableCopy];
    }
    
    [self updateUI];
}

- (void)didUserDisconnected:(NSNotification *)notification {
    VAConnection *user = notification.userInfo[@"user"];
    
    // Your connections
    [self.yourConnections removeObject:user];
    self.yourConnectionsCollectionView.connections = [self.yourConnections mutableCopy];
    
    // Conected To You
    NSInteger connectedToYouIndex = [self.connectedToYouConnections indexOfObject:user];
    if (connectedToYouIndex != NSNotFound) {
        [self.connectedToYouConnections replaceObjectAtIndex:connectedToYouIndex withObject:user];
        self.connectedToYouCollectionView.connections = [self.connectedToYouConnections mutableCopy];
    }
    
    [self updateUI];
}

- (void)didUserUnblocked:(NSNotification *)notification {
    VAConnection *user = notification.userInfo[@"user"];
    
    // Your connections
    if ([user.isFriend boolValue]) {
        [self.yourConnections addObject:user];
        NSSortDescriptor *desriptor = [[NSSortDescriptor alloc] initWithKey:@"fullname" ascending:YES];
        self.yourConnections = [[self.yourConnections sortedArrayUsingDescriptors:@[desriptor]] mutableCopy];
        self.yourConnectionsCollectionView.connections = [self.yourConnections mutableCopy];
    }
    
    // Conected To You
    NSInteger connectedToYouIndex = [self.connectedToYouConnections indexOfObject:user];
    if (connectedToYouIndex != NSNotFound) {
        [self.connectedToYouConnections replaceObjectAtIndex:connectedToYouIndex withObject:user];
        self.connectedToYouCollectionView.connections = [self.connectedToYouConnections mutableCopy];
    }
    
    [self updateUI];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"embedAddFriends"]) {
        self.addFriendsViewController = segue.destinationViewController;
    }
}

@end
