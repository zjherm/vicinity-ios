//
//  VAConnectionsSearchViewController.m
//  vicinity-app
//
//  Created by Panda Systems on 1/28/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import "VAConnectionsSearchViewController.h"

@interface VAConnectionsSearchViewController () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITextField *searchTextField;
@property (nonatomic, weak) IBOutlet UIButton *cancelButton;

@end

@implementation VAConnectionsSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)cancelButtonTouchUp:(UIButton *)sender {
    [self.searchTextField resignFirstResponder];
    [self clearTextField];
    if (self.delegate && [self.delegate respondsToSelector:@selector(didPressCancelButton)]) {
        [self.delegate didPressCancelButton];
    }
}

- (IBAction)searchTextFieldEditingChanged:(UITextField *)sender {
    if (self.delegate) {
        [self.delegate searchTextChanged:sender.text];
    }
}

#pragma mark - Text Field delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.cancelButton.hidden = NO;
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSearchTextFieldBecomeActive:)]) {
        [self.delegate didSearchTextFieldBecomeActive:textField];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.cancelButton.hidden = YES;
}

#pragma mark - Public methods

- (void)clearTextField {
    self.searchTextField.text = @"";
    if (self.delegate) {
        [self.delegate searchTextChanged:self.searchTextField.text];
    }
}

@end
