//
//  VABaseConnectionsListViewController.m
//  vicinity-app
//
//  Created by Panda Systems on 1/28/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import "VABaseConnectionsListViewController.h"

#import "DEMONavigationController.h"

#import "VAControlTool.h"
#import "VANotificationTool.h"

#import "VAUserApiManager.h"
#import "VANotificationMessage.h"
#import "VAConnection.h"
#import "VAConnectionsResponse.h"

@interface VABaseConnectionsListViewController ()

@end

@implementation VABaseConnectionsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupObservers];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self addKeyboardObservers];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self removeKeyboardObservers];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Setup

- (void)setupObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didUserUpdated:)
                                                 name:VAUserDidConnectedNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didUserUpdated:)
                                                 name:VAUserDidDisconnectedNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didUserUpdated:)
                                                 name:VAUserDidBlockedNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didUserUpdated:)
                                                 name:VAUserDidUnblockedNotification
                                               object:nil];
}

- (void)addKeyboardObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)removeKeyboardObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - Properties

- (NSArray *)filteredConnections {
    if ([self.searchText length]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"fullname CONTAINS[cd] %@ OR nickname CONTAINS[cd] %@ ", self.searchText, self.searchText];
        return [self.connections filteredArrayUsingPredicate:predicate];
    }
    return self.connections;
}

#pragma mark - Actions

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)inviteFriendsButtonPressed:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:kShowInviteScreen object:nil];
}

#pragma mark - Public API

- (void)setupNavigationBarWithTitle:(NSString *)title {
    [(DEMONavigationController *)self.navigationController setDefaultNavigationBar];
    [(DEMONavigationController *)self.navigationController setTitle:title];
}

- (void)updateUI { }

#pragma mark - Connections Search delegate

- (void)searchTextChanged:(NSString *)searchText {
    self.searchText = searchText;
    
    [self updateUI];
}

#pragma mark - Connections List Controller delegate

- (void)connectConnectionSelected:(VAConnection *)connection {
    [self connectConnection:connection];
}

- (void)disconnectConnectionSelected:(VAConnection *)connection {
    [self disconnectConnection:connection];
}

- (void)connectToAllSelectedForUsers:(NSArray *)users {
    [self connectToUsers:users];
}

#pragma mark - Helpers

- (void)updateConnectionInList:(VAConnection *)connection {
    NSInteger index = [self.connections indexOfObject:connection];
    if (index != NSNotFound) {
        [self.connections replaceObjectAtIndex:index withObject:connection];
        [self.listViewController updateConnectionInList:connection];
    }
}

- (void)connectConnection:(VAConnection *)connection {
    connection.isFriend = @(YES);
    [self updateConnectionInList:connection];
    
    [[VAUserApiManager new] postAddFriendWithID:connection.userId andCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            connection.isFriend = @(NO);
            [self updateConnectionInList:connection];
            
            [VANotificationMessage showNetworkErrorMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
        } else {
            [VANotificationsService didConnectUser:connection];
            [VANotificationMessage showConnectUserSuccessMessageOnController:[VAControlTool topScreenController] withDuration:5.0f nickname:connection.nickname];
        }
    }];
}

- (void)disconnectConnection:(VAConnection *)connection {
    connection.isFriend = @(NO);
    [self updateConnectionInList:connection];
    
    [[VAUserApiManager new] deleteConnectionWithID:connection.userId withCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            connection.isFriend = @(YES);
            [self updateConnectionInList:connection];
            
            [VANotificationMessage showNetworkErrorMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
        } else {
            [VANotificationsService didDisconnectUser:connection];
        }
    }];
}

- (void)connectToUsers:(NSArray *)users {
    NSArray *emails = [users  valueForKeyPath:@"@distinctUnionOfObjects.email"];
    NSString *emailsList = [emails componentsJoinedByString:@","];
    
    for (VAConnection *user in users) {
        user.isFriend = @(YES);
        [self updateConnectionInList:user];
    }
    
    [[VAUserApiManager new] addFriendsFromEmailList:emailsList withCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            [VANotificationMessage showLoadErrorMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
            for (VAConnection *user in users) {
                user.isFriend = @(NO);
                [self updateConnectionInList:user];
            }
        } else if (model) {
            for (VAConnection *user in users) {
                [VANotificationsService didConnectUser:user];
            }
        }
    }];
}

#pragma mark - Observers

- (void)didUserUpdated:(NSNotification *)notification {
    VAConnection *user = notification.userInfo[@"user"];
    [self updateConnectionInList:user];
}

#pragma mark - Keyboard observers

- (void)willKeyboardShow:(NSNotification*)notification {
    NSDictionary* info = [notification userInfo];
    CGRect keyRect;
    [[info objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyRect];
    CGFloat height = keyRect.size.height;
    
    self.viewBottomConstraint.constant = height;
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)willKeyboardHide:(NSNotification*)notification {
    self.viewBottomConstraint.constant = 0.0f;
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"embedSearchView"]) {
        self.searchViewController = (VAConnectionsSearchViewController *)segue.destinationViewController;
        ((VAConnectionsSearchViewController *)segue.destinationViewController).delegate = self;
    } else if ([segue.identifier isEqualToString:@"embedConnections"]) {
        self.listViewController = (VAConnectionsListViewController *)segue.destinationViewController;
        self.listViewController.showConnectToAllButton = YES;
        self.listViewController.delegate = self;
    }
}

@end
