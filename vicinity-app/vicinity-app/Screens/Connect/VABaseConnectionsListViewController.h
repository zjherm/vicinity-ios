//
//  VABaseConnectionsListViewController.h
//  vicinity-app
//
//  Created by Panda Systems on 1/28/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VAConnectionsSearchViewController.h"
#import "VAConnectionsListViewController.h"

@interface VABaseConnectionsListViewController : UIViewController <VAConnectionsSearchDelegate, VAConnectionsListControllerDelegate>

@property (nonatomic, weak) IBOutlet UIView *inviteFriendsView;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *viewBottomConstraint;

@property (nonatomic, strong) VAConnectionsListViewController *listViewController;
@property (nonatomic, strong) VAConnectionsSearchViewController *searchViewController;

@property (nonatomic, strong) NSMutableArray *connections;
@property (nonatomic, strong, readonly) NSArray *filteredConnections;

@property (nonatomic, strong) NSString *searchText;

- (void)setupNavigationBarWithTitle:(NSString *)title;
- (void)updateUI;
- (void)connectToUsers:(NSArray *)users;

@end
