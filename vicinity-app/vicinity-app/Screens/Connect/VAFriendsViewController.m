//
//  VAFriendsViewController.m
//  vicinity-app
//
//  Created by Panda Systems on 1/28/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import "VAFriendsViewController.h"

#import "DEMONavigationController.h"
#import "VAConnectionsSearchViewController.h"
#import "VAProfileViewController.h"
#import "VAReportViewController.h"

#import "VAConnectionUserTableViewCell.h"
#import "VAConnectionActionsTableViewCell.h"

#import "VANotificationMessage.h"
#import "VAUserApiManager.h"
#import "VAConnection.h"

static NSString *const kConnectionsCountText = @"Count: %lu";

@interface VAFriendsViewController () <VAConnectionsSearchDelegate, UITableViewDataSource, UITableViewDelegate, VAConnectionUserCellDelegate, VAConnectionActionsDelegate, VAReportSendingDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UILabel *connectionsCountLabel;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *viewBottomConstraint;

@property (nonatomic, strong, readonly) NSArray *filteredFriends;
@property (nonatomic, strong) NSString *searchText;

@property (nonatomic) VAConnection *activeConnection;

@end

@implementation VAFriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self registerNibsForCells];
    [self setupObservers];
    
    [self updateUI];
    [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setupNavigationBar];
    [self addKeyboardObservers];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self removeKeyboardObservers];
    if (self.activeConnection) {
        [self hideActionsForConnection:self.activeConnection];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Properties

- (NSArray *)filteredFriends {
    if ([self.searchText length]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"fullname CONTAINS[cd] %@ OR nickname CONTAINS[cd] %@ ", self.searchText, self.searchText];
        return [self.friends filteredArrayUsingPredicate:predicate];
    }
    return self.friends;
}

#pragma mark - Setup UI

- (void)setupObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didUserConnected:)
                                                 name:VAUserDidConnectedNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didUserDisconnected:)
                                                 name:VAUserDidDisconnectedNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didUserDisconnected:)
                                                 name:VAUserDidBlockedNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didUserUnblocked:)
                                                 name:VAUserDidUnblockedNotification
                                               object:nil];
}

- (void)registerNibsForCells {
    UINib *userCellNib = [UINib nibWithNibName:@"VAConnectionUserTableViewCell" bundle:[NSBundle mainBundle]];
    [self.tableView registerNib:userCellNib forCellReuseIdentifier:@"ConnectionCell"];
    
    UINib *actionsCellNib = [UINib nibWithNibName:@"VAConnectionActionsTableViewCell" bundle:[NSBundle mainBundle]];
    [self.tableView registerNib:actionsCellNib forCellReuseIdentifier:@"ActionsCell"];
}

- (void)setupNavigationBar {
    [(DEMONavigationController *)self.navigationController setDefaultNavigationBar];
    [(DEMONavigationController *)self.navigationController setTitle:@"Your Connections"];
}

- (void)addKeyboardObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)removeKeyboardObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)updateUI {
    self.connectionsCountLabel.text = [NSString stringWithFormat:kConnectionsCountText, (unsigned long)[self.filteredFriends count]];
}

#pragma mark - Actions

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)emptySpaceTapped:(id)sender {
    if ([((UITapGestureRecognizer *)sender).view isKindOfClass:[UITableView class]]) {
        CGPoint location = [sender locationInView:self.tableView];
        NSIndexPath *path = [self.tableView indexPathForRowAtPoint:location];
        
        if (path) {
            // tap was on existing row
            return;
        }
    }
    
    if (self.activeConnection) {
        [self hideActionsForConnection:self.activeConnection];
    }
}

#pragma mark - Connections Search delegate

- (void)searchTextChanged:(NSString *)searchText {
    self.searchText = searchText;
    
    [self updateUI];
    self.activeConnection = nil;
    [self.tableView reloadData];
}

- (void)didSearchTextFieldBecomeActive:(UITextField *)searchTextField {
    if (self.activeConnection) {
        [self hideActionsForConnection:self.activeConnection];
    }
}

#pragma mark - Table View data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.filteredFriends.count*2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VAConnection *connection = self.filteredFriends[indexPath.row/2];
    
    if (indexPath.row % 2 == 0) {
        VAConnectionUserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ConnectionCell" forIndexPath:indexPath];
        cell.delegate = self;
        [cell configureCellWithConnection:connection
                              showActions:self.activeConnection != nil && [connection isEqual:self.activeConnection]];
        return cell;
    }
    
    VAConnectionActionsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActionsCell" forIndexPath:indexPath];
    cell.delegate = self;
    [cell configureCellWithConnection:connection];
    return cell;
}

#pragma mark - Table View delegate

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row % 2 == 0) {
        return 55.0f;
    } else {
        return 49.0f;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row % 2 == 0) {
        return UITableViewAutomaticDimension;
    } else {
        VAConnection *connection = self.filteredFriends[indexPath.row/2];
        return self.activeConnection != nil && [connection isEqual:self.activeConnection] ? UITableViewAutomaticDimension : 0.0f;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row % 2 == 0) {
        [self showProfileForUserWithAlias:((VAConnection *)self.filteredFriends[indexPath.row/2]).nickname];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Connection User Cell delegate

- (void)toggleActionsForConnection:(VAConnection *)connection {
    if (self.activeConnection != nil && [connection isEqual:self.activeConnection]) {
        [self hideActionsForConnection:connection];
    } else {
        [self showActionsForConnection:connection];
    }
}

#pragma mark - Connection Actions delegate

- (void)disconnectConnectionSelected:(VAConnection *)connection {
    [self disconnectConnection:connection];
}

- (void)messageToConnectionSelected:(VAConnection *)connection {
    
}

- (void)reportAboutConnectionSelected:(VAConnection *)connection {
    [self showReportScreenWithConnection:connection];
}

- (void)blockConnectionSelected:(VAConnection *)connection {
    [self askConfirmationToBlockConnection:connection];
}

#pragma mark - Report Sending delegate

- (void)didSendReportAboutUser:(VAUser *)user {
    [VANotificationMessage showReportSuccessMessageOnController:self withDuration:5.f];
}

#pragma mark - Helpers

- (void)removeConnectionFromList:(VAConnection *)connection {
    NSInteger index = [self.filteredFriends indexOfObject:connection];
    [self.friends removeObject:connection];
    if (index != NSNotFound) {
        NSArray *indexPaths = @[[NSIndexPath indexPathForRow:index*2 inSection:0],
                                [NSIndexPath indexPathForRow:index*2 + 1 inSection:0]];
        if ([self.activeConnection isEqual:connection]) {
            self.activeConnection = nil;
        }
        
        [self updateUI];
        
        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
        [self.tableView endUpdates];
    }
}

- (void)addConnectionToList:(VAConnection *)connection {
    NSInteger index = [self.friends indexOfObject:connection];
    if (index == NSNotFound) {
        [self.friends addObject:connection];
        
        NSSortDescriptor *desriptor = [[NSSortDescriptor alloc] initWithKey:@"fullname" ascending:YES];
        _friends = [[self.friends sortedArrayUsingDescriptors:@[desriptor]] mutableCopy];
        
        NSInteger index = [self.filteredFriends indexOfObject:connection];
        if (index != NSNotFound) {
            NSArray *indexPaths = @[[NSIndexPath indexPathForRow:index*2 inSection:0],
                                    [NSIndexPath indexPathForRow:index*2 + 1 inSection:0]];
            [self updateUI];
            
            [self.tableView beginUpdates];
            [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
            [self.tableView endUpdates];
        }
    }
}

- (void)disconnectConnection:(VAConnection *)connection {
    connection.isFriend = @(NO);
    [self removeConnectionFromList:connection];
    
    [[VAUserApiManager new] deleteConnectionWithID:connection.userId
                                    withCompletion:^(NSError *error, VAModel *model)
     {
         if (error) {
             connection.isFriend = @(YES);
             [VANotificationMessage showNetworkErrorMessageOnController:self withDuration:5.f];
         } else {
             [VANotificationsService didDisconnectUser:connection];
         }
     }];
}

- (void)askConfirmationToBlockConnection:(VAConnection *)connection {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"Block %@", connection.fullname]
                                                                             message:[NSString stringWithFormat:@"Are you sure you want to block %@?", connection.fullname]
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"No"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * _Nonnull action) { }];
    [alertController addAction:noAction];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"Yes"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * _Nonnull action) {
                                                          [self blockConnection:connection];
                                                      }];
    [alertController addAction:yesAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)blockConnection:(VAConnection *)connection {
    [self removeConnectionFromList:connection];
    
    connection.isBlocked = @(YES);
    
    [[VAUserApiManager new] blockUserWithID:connection.userId andCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            connection.isBlocked = @(NO);
            [VANotificationMessage showNetworkErrorMessageOnController:self withDuration:5.f];
        } else {
            [VANotificationsService didBlockUser:connection];
        }
    }];
}

- (void)showActionsForConnection:(VAConnection *)connection {
    NSMutableArray *cellsToReload = [NSMutableArray array];
    if (self.activeConnection) {
        NSInteger index = [self.filteredFriends indexOfObject:self.activeConnection];
        if (index != NSNotFound) {
            [cellsToReload addObject:[NSIndexPath indexPathForRow:index*2 inSection:0]];
        }
        
    }
    self.activeConnection = connection;
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:cellsToReload withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
}

- (void)hideActionsForConnection:(VAConnection *)connection {
    NSMutableArray *cellsToReload = [NSMutableArray array];
    if (self.activeConnection) {
        NSInteger index = [self.filteredFriends indexOfObject:self.activeConnection];
        if (index != NSNotFound) {
            [cellsToReload addObject:[NSIndexPath indexPathForRow:index*2 inSection:0]];
        }
    }
    self.activeConnection = nil;
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:cellsToReload withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
}

- (void)showProfileForUserWithAlias:(NSString *)alias {
    VAProfileViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"profileViewController"];
    vc.searchAlias = alias;
    vc.isUsersAccountMode = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showReportScreenWithConnection:(VAConnection *)connection {
    VAReportViewController *vc = [[UIStoryboard storyboardWithName:@"Vicinity" bundle:nil] instantiateViewControllerWithIdentifier:@"reportViewController"];
    vc.reportedUser = connection;
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Observers

- (void)didUserConnected:(NSNotification *)notification {
    VAConnection *user = notification.userInfo[@"user"];
    [self addConnectionToList:user];
}

- (void)didUserDisconnected:(NSNotification *)notification {
    VAConnection *user = notification.userInfo[@"user"];
    [self removeConnectionFromList:user];
}

- (void)didUserUnblocked:(NSNotification *)notification {
    VAConnection *user = notification.userInfo[@"user"];
    
    if ([user.isFriend boolValue]) {
        [self addConnectionToList:user];
    }
}

#pragma mark - Keyboard observers

- (void)willKeyboardShow:(NSNotification*)notification {
    NSDictionary* info = [notification userInfo];
    CGRect keyRect;
    [[info objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyRect];
    CGFloat height = keyRect.size.height;
    
    self.viewBottomConstraint.constant = height;
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)willKeyboardHide:(NSNotification*)notification {
    self.viewBottomConstraint.constant = 0.0f;
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"embedSearchView"]) {
        ((VAConnectionsSearchViewController *)segue.destinationViewController).delegate = self;
    }
}

@end
