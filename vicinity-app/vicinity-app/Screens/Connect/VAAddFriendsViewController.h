//
//  VAAddFriendsViewController.h
//  vicinity-app
//
//  Created by Panda Systems on 2/1/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VAAddFriendsViewController : UIViewController

@property (nonatomic, strong) NSArray *facebookFriends;
@property (nonatomic, strong) NSArray *twitterFriends;
@property (nonatomic, strong) NSArray *contacts;

@end
