//
//  VAConnectionsCollectionView.h
//  vicinity-app
//
//  Created by Panda Systems on 1/28/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VAUser;
@protocol VAConnectionsListViewDelegate;

@interface VAConnectionsCollectionView : UICollectionView <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, weak) id<VAConnectionsListViewDelegate> listDelegate;
@property (nonatomic, strong) NSMutableArray *connections;

@end

@protocol VAConnectionsListViewDelegate <NSObject>

- (void)collectionView:(VAConnectionsCollectionView *)collectionView didSelectUser:(VAUser *)user;

@end
