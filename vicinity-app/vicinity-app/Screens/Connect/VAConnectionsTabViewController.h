//
//  VAConnectionsTabViewController.h
//  vicinity-app
//
//  Created by Panda Systems on 1/28/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

extern CGFloat const kConnectionCellWidth;

@class VAAddFriendsViewController;

@interface VAConnectionsTabViewController : UIViewController
@property (nonatomic, strong, readonly) NSMutableArray *yourConnections;
@property (nonatomic, strong, readonly) NSMutableArray *connectedToYouConnections;

@property (nonatomic, strong) VAAddFriendsViewController *addFriendsViewController;

@end
