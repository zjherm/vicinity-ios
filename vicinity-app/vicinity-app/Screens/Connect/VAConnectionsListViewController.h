//
//  VAConnectionsListViewController.h
//  vicinity-app
//
//  Created by Panda Systems on 1/29/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VAConnection;

@protocol VAConnectionsListControllerDelegate <NSObject>

- (void)connectConnectionSelected:(VAConnection *)connection;
- (void)disconnectConnectionSelected:(VAConnection *)connection;
- (void)connectToAllSelectedForUsers:(NSArray *)users;

@end

@interface VAConnectionsListViewController : UIViewController

@property (nonatomic, weak) id<VAConnectionsListControllerDelegate> delegate;

@property (nonatomic, strong) NSMutableArray *connections;

- (void)updateConnectionInList:(VAConnection *)connection;

@property (nonatomic) BOOL showConnectToAllButton;

@end
