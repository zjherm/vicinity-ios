//
//  VAConnectedToYouViewController.m
//  vicinity-app
//
//  Created by Panda Systems on 1/29/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import "VAConnectedToYouViewController.h"

@interface VAConnectedToYouViewController ()

@end

@implementation VAConnectedToYouViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self updateUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setupNavigationBarWithTitle:@"Connected To You"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup UI

- (void)updateUI {
    self.listViewController.connections = [[self filteredConnections] mutableCopy];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];
}

@end
