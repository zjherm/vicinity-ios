//
//  VAInviteTabViewController.m
//  vicinity-app
//
//  Created by Panda Systems on 2/1/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import "VAInviteTabViewController.h"

#import "VAControlTool.h"
#import "VANotificationMessage.h"
#import <FBSDKShareKit.h>

@interface VAInviteTabViewController () <FBSDKAppInviteDialogDelegate>

@property (nonatomic) BOOL TemporaryHide;
@property (weak, nonatomic) IBOutlet UIButton *facebookBtn;
@property (weak, nonatomic) IBOutlet UIButton *twitterBtn;
@property (weak, nonatomic) IBOutlet UIButton *shareBtn;
@property (weak, nonatomic) IBOutlet UILabel *inviteVia;

@end

@implementation VAInviteTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.TemporaryHide = true;
    if (self.TemporaryHide) {
        self.facebookBtn.hidden = YES;
        self.twitterBtn.hidden = YES;
        self.shareBtn.hidden = YES;
        self.inviteVia.hidden = YES;
    }
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)facebookButtonPressed:(id)sender {
    [self inviteFacebookFriends];
}

- (IBAction)twitterButtonPressed:(id)sender {
    // TODO: implement
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:@"This feature is not implemented yet."
                                                   delegate:self
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
}

- (IBAction)shareButtonPressed:(id)sender {
    [self showShareSheet];
}


#pragma mark - Helpers

- (void)inviteFacebookFriends {
    FBSDKAppInviteContent *content = [[FBSDKAppInviteContent alloc] init];
    content.appLinkURL = [NSURL URLWithString:@"https://fb.me/1642207479328843"];
    
    [FBSDKAppInviteDialog showWithContent:content delegate:self];
}

- (void)showShareSheet {
    [[VAControlTool defaultControl] showShareSheetWithCompletion:^(NSString * _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError) {
        if (completed) {
            [VANotificationMessage showShareSuccessMessageOnController:[VAControlTool topScreenController]];
        }
    }];
}

#pragma mark - FB SDK App Invite Dialog delegate

- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didCompleteWithResults:(NSDictionary *)results {
    if (results && [results[@"didComplete"] intValue] == 1 && ![results[@"completionGesture"] isEqualToString:@"cancel"]) {
        [VANotificationMessage showShareSuccessMessageOnController:[VAControlTool topScreenController]];
    }
}

- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didFailWithError:(NSError *)error {
    NSLog(@"Fail invite friends via Facebook");
}

@end
