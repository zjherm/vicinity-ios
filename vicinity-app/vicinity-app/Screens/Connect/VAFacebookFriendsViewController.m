//
//  VAFacebookFriendsViewController.m
//  vicinity-app
//
//  Created by Panda Systems on 2/2/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import "VAFacebookFriendsViewController.h"

@interface VAFacebookFriendsViewController ()

@end

@implementation VAFacebookFriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self updateUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setupNavigationBarWithTitle:@"Facebook Friends"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup UI

- (void)updateUI {
    self.listViewController.connections = [[self filteredConnections] mutableCopy];
    self.inviteFriendsView.hidden = [[self filteredConnections] count] > 0;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];
}

@end
