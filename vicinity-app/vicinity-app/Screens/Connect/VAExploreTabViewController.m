//
//  VAExploreTabViewController.m
//  vicinity-app
//
//  Created by Panda Systems on 2/2/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import "VAExploreTabViewController.h"

#import "VAProfileViewController.h"
#import "VAConnectionsCollectionView.h"

#import "VAUserApiManager.h"
#import "VANotificationMessage.h"
#import "VAUserDefaultsHelper.h"
#import "VAControlTool.h"
#import "VANotificationTool.h"

#import "VAUserSearchResponse.h"
#import "VAConnectionsResponse.h"
#import "VAUser.h"

static CGFloat const kConnectionCellWidth = 65.0f;

@interface VAExploreTabViewController () <VAConnectionsListViewDelegate>

@property (nonatomic, weak) IBOutlet UIView *searchResultsView;
@property (nonatomic, weak) IBOutlet UIView *recentlyViewedView;
@property (nonatomic, weak) IBOutlet UIView *bottomInviteFriendsView;
@property (nonatomic, weak) IBOutlet UIButton *seeAllRecentlyViewedButton;
@property (nonatomic, weak) IBOutlet VAConnectionsCollectionView *recentlyViewedCollectionView;

@property (nonatomic, strong) NSTimer *searchTimer;
@property (nonatomic, strong, readwrite) NSArray *recentlyViewedConnections;

@end

@implementation VAExploreTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.connections = [NSMutableArray array];
    
    self.recentlyViewedCollectionView.listDelegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadEveryone];
    [self addKeyboardObservers];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self removeKeyboardObservers];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup UI

- (void)updateUI {
    if (self.searchTimer) {
        [self.searchTimer invalidate];
        self.searchTimer = nil;
    }
    
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:0.5f
                                                      target:self
                                                    selector:@selector(loadEveryone)
                                                    userInfo:nil
                                                     repeats:NO];
    self.searchTimer = timer;
}

- (void)addKeyboardObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)removeKeyboardObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - Actions

- (IBAction)seeAllConnectionsButtonPressed:(id)sender {
    [self.parentViewController performSegueWithIdentifier:@"showRecentlyViewed" sender:nil];
}

- (IBAction)inviteFriendsButtonPressed:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:kShowInviteScreen object:nil];
}

#pragma mark - Connections List View delegate

- (void)collectionView:(VAConnectionsCollectionView *)collectionView didSelectUser:(VAUser *)user {
    if ([user.nickname length]) {
        [self showProfileForUserWithAlias:user.nickname];
    } else {
        [self clearViewedUsers];
    }
}

#pragma mark - Helpers

- (void)loadEveryone {
    self.searchTimer = nil;
    VAUserApiManager *manager = [VAUserApiManager new];
    if ([self.searchText length]) {
        self.searchResultsView.hidden = NO;
        self.recentlyViewedView.hidden = YES;
        self.inviteFriendsView.hidden = YES;
        self.bottomInviteFriendsView.hidden = YES;
        [manager getUserByAliasSearch:self.searchText withCompletion:^(NSError *error, VAModel *model) {
            if (error) {
                [VANotificationMessage showLoadErrorMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
            } else if (model) {
                VAUserSearchResponse *response = (VAUserSearchResponse *)model;
                
                NSSortDescriptor *desriptor = [[NSSortDescriptor alloc] initWithKey:@"fullname" ascending:YES];
                NSArray *sorted = [response.users sortedArrayUsingDescriptors:@[desriptor]];
                
                VAUser *user = (VAUser *)[VAUserDefaultsHelper getModelForKey:VASaveKeys_CurrentUser];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId != %@", user.userId];
                
                self.connections = [[sorted filteredArrayUsingPredicate:predicate] mutableCopy];
                self.listViewController.connections = [self.connections mutableCopy];
            }
        }];
    } else {
        self.searchResultsView.hidden = YES;
        self.recentlyViewedView.hidden = YES;
        self.inviteFriendsView.hidden = YES;
        self.bottomInviteFriendsView.hidden = YES;
        [manager getViewedUsersWithCompletion:^(NSError *error, VAModel *model) {
            if (error) {
                [VANotificationMessage showLoadErrorMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
            } else if (model) {
                VAConnectionsResponse *response = (VAConnectionsResponse *)model;
                
                NSArray *sorted = response.connections;
                
                VAUser *user = (VAUser *)[VAUserDefaultsHelper getModelForKey:VASaveKeys_CurrentUser];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId != %@", user.userId];
                
                VAUser *clearRecent = [[VAUser alloc] init];
                clearRecent.nickname = @"";
                clearRecent.fullname = @"CLEAR RECENT";
                
                self.recentlyViewedConnections = [[[[sorted filteredArrayUsingPredicate:predicate] mutableCopy] arrayByAddingObject:clearRecent] mutableCopy];
                if (![self.searchText length]) {
                    self.recentlyViewedView.hidden = self.recentlyViewedConnections.count > 1 ? NO : YES;
                    self.inviteFriendsView.hidden = self.recentlyViewedConnections.count > 1 ? YES : NO;
                    self.bottomInviteFriendsView.hidden = self.recentlyViewedConnections.count > 1 ? NO : YES;
                    
                    self.recentlyViewedCollectionView.connections = [self.recentlyViewedConnections mutableCopy];
                    self.seeAllRecentlyViewedButton.hidden = [self.recentlyViewedConnections count]*kConnectionCellWidth < [UIScreen mainScreen].bounds.size.width;
                }
            }
        }];
    }
}

- (void)clearViewedUsers {
    VAUserApiManager *manager = [VAUserApiManager new];
    [manager clearViewedUsersWithCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            [VANotificationMessage showLoadErrorMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
        } else {
            self.recentlyViewedConnections = [NSArray array];
            self.recentlyViewedView.hidden = self.recentlyViewedConnections.count > 1 ? NO : YES;
            self.inviteFriendsView.hidden = self.recentlyViewedConnections.count > 1 ? YES : NO;
            self.bottomInviteFriendsView.hidden = self.recentlyViewedConnections.count > 1 ? NO : YES;
            
            self.recentlyViewedCollectionView.connections = [self.recentlyViewedConnections mutableCopy];
            self.seeAllRecentlyViewedButton.hidden = [self.recentlyViewedConnections count]*kConnectionCellWidth < [UIScreen mainScreen].bounds.size.width;
        }
    }];
}

- (void)showProfileForUserWithAlias:(NSString *)alias {
    VAProfileViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"profileViewController"];
    vc.searchAlias = alias;
    vc.isUsersAccountMode = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Keyboard observers

- (void)willKeyboardShow:(NSNotification*)notification {
    NSDictionary* info = [notification userInfo];
    CGRect keyRect;
    [[info objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyRect];
    CGFloat height = keyRect.size.height;
    
    self.viewBottomConstraint.constant = height;
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)willKeyboardHide:(NSNotification*)notification {
    self.viewBottomConstraint.constant = 0.0f;
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"embedConnections"]) {
        self.listViewController = (VAConnectionsListViewController *)segue.destinationViewController;
        self.listViewController.showConnectToAllButton = NO;
        self.listViewController.delegate = self;
    } else {
        [super prepareForSegue:segue sender:sender];
    }
}

@end
