//
//  VAConnectionCollectionViewCell.h
//  vicinity-app
//
//  Created by Panda Systems on 1/28/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VAUser;

@interface VAConnectionCollectionViewCell : UICollectionViewCell

- (void)configureCellWithUser:(VAUser *)user;

@end
