//
//  VAFilterTableViewFooter.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 25.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAFilterTableViewFooterView.h"
#import "DEMONavigationController.h"
#import "VAFilterViewController.h"

@interface VAFilterTableViewFooterView ()
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *applyButton;

- (IBAction)cancelButtonPushed:(id)sender;
- (IBAction)applyButtonPushed:(id)sender;
@end

@implementation VAFilterTableViewFooterView

- (void)awakeFromNib {
    self.cancelButton.layer.cornerRadius = 5.f;
    self.cancelButton.clipsToBounds = YES;
    
    self.applyButton.layer.cornerRadius = 5.f;
    self.applyButton.clipsToBounds = YES;
}

#pragma mark - Actions

- (IBAction)cancelButtonPushed:(id)sender {
    [self.delegate cancelFilters];
}

- (IBAction)applyButtonPushed:(id)sender {
    [self.delegate applyFilters];
}

@end
