//
//  VAProfileSegmentedView.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 8/27/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAProfileSegmentedView.h"

@interface VAProfileSegmentedView ()
@property (nonatomic, weak) IBOutlet UIView *generalView;
@property (nonatomic, weak) IBOutlet UIView *postsView;
@property (nonatomic, weak) IBOutlet UIView *likesView;

@property (nonatomic, weak) IBOutlet UIView *stateView;

@property (nonatomic, weak) IBOutlet UILabel *generalLabel;
@property (nonatomic, weak) IBOutlet UILabel *postsLabel;
@property (nonatomic, weak) IBOutlet UILabel *likesLabel;
@end

@implementation VAProfileSegmentedView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setupGestures];
    [self setupConstraints];
    
    _profileInfoType = VAProfileInfoTypeGeneral;
}

#pragma mark - Setup UI

- (void)setupGestures {
    UITapGestureRecognizer *generalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectGeneralInfo)];
    [self.generalView addGestureRecognizer:generalTap];
    UITapGestureRecognizer *postsTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectPostsInfo)];
    [self.postsView addGestureRecognizer:postsTap];
    UITapGestureRecognizer *likesTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectLikesInfo)];
    [self.likesView addGestureRecognizer:likesTap];
}

- (void)setupConstraints {
    self.generalStateViewConstraints = @[[NSLayoutConstraint constraintWithItem:self.stateView
                                                                      attribute:NSLayoutAttributeLeading
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.generalView
                                                                      attribute:NSLayoutAttributeLeading
                                                                     multiplier:1
                                                                       constant:0],
                                         [NSLayoutConstraint constraintWithItem:self.stateView
                                                                      attribute:NSLayoutAttributeTrailing
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.generalView
                                                                      attribute:NSLayoutAttributeTrailing
                                                                     multiplier:1
                                                                       constant:0]];
    
    self.postsStateViewConstraints = @[[NSLayoutConstraint constraintWithItem:self.stateView
                                                                      attribute:NSLayoutAttributeLeading
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.postsView
                                                                      attribute:NSLayoutAttributeLeading
                                                                     multiplier:1
                                                                       constant:0],
                                         [NSLayoutConstraint constraintWithItem:self.stateView
                                                                      attribute:NSLayoutAttributeTrailing
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.postsView
                                                                      attribute:NSLayoutAttributeTrailing
                                                                     multiplier:1
                                                                       constant:0]];
    
    self.likesStateViewConstraints = @[[NSLayoutConstraint constraintWithItem:self.stateView
                                                                      attribute:NSLayoutAttributeLeading
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.likesView
                                                                      attribute:NSLayoutAttributeLeading
                                                                     multiplier:1
                                                                       constant:0],
                                         [NSLayoutConstraint constraintWithItem:self.stateView
                                                                      attribute:NSLayoutAttributeTrailing
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.likesView
                                                                      attribute:NSLayoutAttributeTrailing
                                                                     multiplier:1
                                                                       constant:0]];
    
    self.centralStateViewConstraints = @[[NSLayoutConstraint constraintWithItem:self.stateView
                                                                    attribute:NSLayoutAttributeWidth
                                                                    relatedBy:NSLayoutRelationEqual
                                                                       toItem:self.generalView
                                                                    attribute:NSLayoutAttributeWidth
                                                                   multiplier:1.0f/3.0f
                                                                     constant:0],
                                       [NSLayoutConstraint constraintWithItem:self.stateView
                                                                    attribute:NSLayoutAttributeCenterX
                                                                    relatedBy:NSLayoutRelationEqual
                                                                       toItem:self.generalView
                                                                    attribute:NSLayoutAttributeCenterX
                                                                   multiplier:1
                                                                     constant:0]];
    
    [self addConstraints:self.generalStateViewConstraints];
}

#pragma mark - Properties

- (void)setProfileInfoType:(VAProfileInfoType)profileInfoType {
    _profileInfoType = profileInfoType;
    
    self.generalLabel.textColor = profileInfoType == VAProfileInfoTypeGeneral ? [self selectedTabTextColor] : [self unselectedTabTextColor];
    self.generalLabel.font = profileInfoType == VAProfileInfoTypeGeneral ? [self selectedTabTextFont] : [self unselectedTabTextFont];
    self.postsLabel.textColor = profileInfoType == VAProfileInfoTypePosts ? [self selectedTabTextColor] : [self unselectedTabTextColor];
    self.postsLabel.font = profileInfoType == VAProfileInfoTypePosts ? [self selectedTabTextFont] : [self unselectedTabTextFont];
    self.likesLabel.textColor = profileInfoType == VAProfileInfoTypeLikes ? [self selectedTabTextColor] : [self unselectedTabTextColor];
    self.likesLabel.font = profileInfoType == VAProfileInfoTypeLikes ? [self selectedTabTextFont] : [self unselectedTabTextFont];
    
    profileInfoType == VAProfileInfoTypeGeneral ? [self.delegate didSelectGeneralInfo] : nil;
    profileInfoType == VAProfileInfoTypePosts ? [self.delegate didSelectPostsInfo] : nil;
    profileInfoType == VAProfileInfoTypeLikes ? [self.delegate didSelectLikesInfo] : nil;
}

#pragma mark - Taps

- (void)didSelectGeneralInfo {
    if (self.profileInfoType != VAProfileInfoTypeGeneral) {
        self.profileInfoType = VAProfileInfoTypeGeneral;
    }
}

- (void)didSelectPostsInfo {
    if (self.profileInfoType != VAProfileInfoTypePosts) {
        self.profileInfoType = VAProfileInfoTypePosts;
    }
}

- (void)didSelectLikesInfo {
    if (self.profileInfoType != VAProfileInfoTypeLikes) {
        self.profileInfoType = VAProfileInfoTypeLikes;
    }
}

#pragma mark - Helpers

- (UIColor *)selectedTabTextColor {
    return [UIColor colorWithRed:16.0f/255.0f
                           green:112.0f/255.0f
                            blue:179.0f/255.0f
                           alpha:1];
}

- (UIColor *)unselectedTabTextColor {
    return [UIColor colorWithRed:74.0f/255.0f
                           green:74.0f/255.0f
                            blue:74.0f/255.0f
                           alpha:1];
}

- (UIFont *)selectedTabTextFont {
    return [UIFont fontWithName:@"Avenir-Heavy" size:15.0f];
}

- (UIFont *)unselectedTabTextFont {
    return [UIFont fontWithName:@"Avenir-Book" size:15.0f];
}

@end
