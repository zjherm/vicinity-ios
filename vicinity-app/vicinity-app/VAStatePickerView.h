//
//  VAStatePickerView.h
//  vicinity-app
//
//  Created by Panda Systems on 10/21/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VAStatePickerDelegate <NSObject>

- (void)didSelectState:(NSString *)state withAbbreviation:(NSString *)abbreviation;

@end

@interface VAStatePickerView : UIPickerView
@property (nonatomic, weak) id<VAStatePickerDelegate> stateDelegate;
@property (nonatomic) NSString *currentState;
@end
