//
//  VAEnterUsernameViewController.h
//  vicinity-app
//
//  Created by vadzim vasileuski on 3/30/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

//VAProfileProtocol
#import "VAProfileViewController.h"

@interface VAEnterUsernameViewController : UIViewController

@property (weak, nonatomic) id <VAProfileProtocol> delegate;

@end
