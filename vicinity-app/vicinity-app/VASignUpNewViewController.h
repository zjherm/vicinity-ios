//
//  VASignUpNewViewController.h
//  vicinity-app
//
//  Created by vadzim vasileuski on 3/25/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VAUser;

@interface VASignUpNewViewController : UIViewController

@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, strong) UIImage *avatarImage;


@property (nonatomic, strong) VAUser *pendingUser;


@end
