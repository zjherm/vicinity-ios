//
//  VAControlTool.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 02.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAControlTool.h"
#include <sys/types.h>
#include <sys/sysctl.h>
#import "DEMONavigationController.h"
#import <MFSideMenu.h>
#import "VAUserDefaultsHelper.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "VAUserApiManager.h"
#import "VALoginViewController.h"
#import "VAWelcomeViewController.h"
#import "VAPost.h"
#import "VAPostStore.h"
#import "VAUserTool.h"


#import "BranchUniversalObject.h"
#import "BranchLinkProperties.h"

NSString *const kFacebookLoginMethod = @"kFacebookLoginMethod";

@implementation VAControlTool

+ (VAControlTool *)defaultControl {
    static VAControlTool *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [VAControlTool new];
    });
    return shared;
}

- (BOOL)setNameAndSurnameMaskForTextField:(UITextField *)textField InRange:(NSRange)range replacementString:(NSString *)string {
    
    NSCharacterSet *validationSet = [NSCharacterSet characterSetWithCharactersInString:@"1234567890@#$%&*+/=?^_`{|}~;[]/\\\":±§<>!().,€·£•¥"];
    NSArray *components = [string componentsSeparatedByCharactersInSet:validationSet];
    
    if ([components count] > 1) {
        return NO;
    }
    
    if ([textField.text rangeOfString:@"."].location != NSNotFound) {
        if ([string isEqualToString:@"."]) {
            return NO;
        }
    }
    
    if ([textField.text length] == 0 && ([string isEqualToString:@"."] || [string isEqualToString:@"'"])) {
        return NO;
    }
    
    NSString *lastSymbol = nil;
    if (textField.text.length > 0) {
        lastSymbol = [textField.text substringWithRange:NSMakeRange([textField.text length] - 1, 1)];
    }
    
    if ([lastSymbol isEqualToString:@"."] || [lastSymbol isEqualToString:@"'"]) {
        if ([string isEqualToString:@"."] || [string isEqualToString:@"'"]) {
            return NO;
        }
    }
    
    return YES;
}

- (BOOL)setPasswordMaskForTextField:(UITextField *)textField InRange:(NSRange)range replacementString:(NSString *)string {
    
    NSCharacterSet *validationSet = [NSCharacterSet characterSetWithCharactersInString:@" "];
    NSArray *components = [string componentsSeparatedByCharactersInSet:validationSet];
    
    if ([components count] > 1) {
        return NO;
    }
    
    return YES;
}

- (BOOL)setAliasMaskForTextField:(UITextField *)textField InRange:(NSRange)range replacementString:(NSString *)string {
    
    NSMutableCharacterSet *validationSet = [NSMutableCharacterSet characterSetWithCharactersInString:@" '@-#$%&*+/=?^`{|}~;[]/\\\":±§<>!(),€·£•¥"];
    
    //for working with SwiftKey keyboard
    NSString *trimmedString = string;
    if ([string hasSuffix:@" "]){
        trimmedString = [string stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    }
    
    NSArray *components = [trimmedString componentsSeparatedByCharactersInSet:validationSet];
    
    if ([components count] > 1) {
        return NO;
    }
    
    if ([textField.text length] == 0 && ([string isEqualToString:@"."] || [string isEqualToString:@"_"])) {
        return NO;
    }
    
    NSString *lastSymbol = nil;
    if (textField.text.length > 0) {
        lastSymbol = [textField.text substringWithRange:NSMakeRange([textField.text length] - 1, 1)];
    }
    
    if ([lastSymbol isEqualToString:@"."] || [lastSymbol isEqualToString:@"_"]) {
        if ([string isEqualToString:@"."] || [string isEqualToString:@"_"]) {
            return NO;
        }
    }
    
    if ([textField.text hasSuffix:@"."]) {
        if ([string isEqualToString:@"."]) {
            return NO;
        }
    }
    
    if ([textField.text hasSuffix:@"_"]) {
        if ([string isEqualToString:@"_"]) {
            return NO;
        }
    }
    
    UITextPosition *beginning = textField.beginningOfDocument;
    UITextPosition *cursorLocation = [textField positionFromPosition:beginning offset:(range.location + trimmedString.length)];
    
    textField.text = [[textField.text stringByReplacingCharactersInRange:range withString:trimmedString] lowercaseString];
    [[NSNotificationCenter defaultCenter] postNotificationName:UITextFieldTextDidChangeNotification object:textField];
    // cursorLocation will be (null) if you're inputting text at the end of the string
    // if already at the end, no need to change location as it will default to end anyway
    if (cursorLocation)
    {
        // set start/end location to same spot so that nothing is highlighted
        [textField setSelectedTextRange:[textField textRangeFromPosition:cursorLocation toPosition:cursorLocation]];
    }

    return NO;
}

- (BOOL)setEmailMaskForTextField:(UITextField *)textField InRange:(NSRange)range replacementString:(NSString *)string {
    
    NSCharacterSet *validationSet = [NSCharacterSet characterSetWithCharactersInString:@"#$%&*/=?^{|}~[]/\\\":±§<>'!(),€·£•¥"];
    NSArray *components = [string componentsSeparatedByCharactersInSet:validationSet];
    
    if ([components count] > 1) {
        return NO;
    }
    
    // Check if correct email address
    NSArray *spaceSeparatedComponents = [string componentsSeparatedByString:@" "];
    NSMutableString *stringWithoutSpaces = [NSMutableString string];
    for (NSString *c in spaceSeparatedComponents) {
        [stringWithoutSpaces appendString:c];
    }
    NSString *potentialResultString = [textField.text stringByReplacingCharactersInRange:range withString:stringWithoutSpaces];
    if ([potentialResultString rangeOfString:@"@"].location != NSNotFound) {
        if ([self isValidEmailString:potentialResultString]) {
            textField.text = potentialResultString;
            //cursor positioning
            UITextPosition *beginning = textField.beginningOfDocument;
            UITextPosition *cursorLocation = [textField positionFromPosition:beginning offset:(range.location + string.length)];
            if (cursorLocation)
            {
                // set start/end location to same spot so that nothing is highlighted
                [textField setSelectedTextRange:[textField textRangeFromPosition:cursorLocation toPosition:cursorLocation]];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:UITextFieldTextDidChangeNotification object:textField];
            return NO;
        }
    }
    
    /*username@hostname.domain*/
    
    NSMutableString *resultString = [NSMutableString string];
    
    if ([string isEqualToString:@""]) {
        resultString = (NSMutableString *)[textField.text stringByReplacingCharactersInRange:range withString:string];
    } else {
        if ([textField.text containsString:@"@"]) {      //hostname + domain
            NSRange rangeOfAt = [textField.text rangeOfString:@"@"];
            NSString *username = [textField.text substringToIndex:rangeOfAt.location];
            
            NSMutableCharacterSet *legalSetWithDot = [NSMutableCharacterSet characterSetWithCharactersInString:@".-+"];
            [legalSetWithDot formUnionWithCharacterSet:[NSCharacterSet letterCharacterSet]];
            NSCharacterSet *illigalSet = [legalSetWithDot invertedSet];
            NSArray *components = [string componentsSeparatedByCharactersInSet:illigalSet];
            if ([components count] > 1) {
                return NO;
            }
            
            NSMutableString *hostname = (NSMutableString *)[textField.text substringFromIndex:rangeOfAt.location + 1];
            
            hostname = (NSMutableString *)[hostname stringByReplacingCharactersInRange:NSMakeRange(range.location - rangeOfAt.location - 1, 0) withString:string];
            NSArray *validComponents = [hostname componentsSeparatedByCharactersInSet:illigalSet];
            hostname = (NSMutableString *)[validComponents componentsJoinedByString:@""];
            
            if ([hostname length] == 1) {
                if ([string isEqualToString:@"."] || [string isEqualToString:@"-"]) {
                    return NO;
                }
            } else if ([hostname length] > 63) {
                return NO;
            }
            
            if ([[hostname substringWithRange:NSMakeRange(0, [hostname length] - 1)] hasSuffix:@"."]) {
                if ([[string substringWithRange:NSMakeRange([string length] - 1, 1)] isEqualToString:@"."]) {
                    return NO;
                }
            }
            
            [resultString appendString:username];
            [resultString appendString:@"@"];
            [resultString appendString:hostname];
            
        } else {                                         //username
            NSMutableCharacterSet *legalSetWithDot = [NSMutableCharacterSet characterSetWithCharactersInString:@"+.@#$%&'*—/=?^_`{|}~!()€·£•¥"];
            [legalSetWithDot formUnionWithCharacterSet:[NSCharacterSet alphanumericCharacterSet]];
            NSCharacterSet *illigalSet = [legalSetWithDot invertedSet];
            
            //for working with SwiftKey keyboard
            NSString *trimmedString = string;
            if ([string hasSuffix:@" "]){
                trimmedString = [string stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
            }
            
            NSArray *components = [trimmedString componentsSeparatedByCharactersInSet:illigalSet];
            if ([components count] > 1) {
                return NO;
            }
            NSString *username = [textField.text stringByReplacingCharactersInRange:range withString:trimmedString];
            NSArray *validComponents = [username componentsSeparatedByCharactersInSet:illigalSet];
            username = [validComponents componentsJoinedByString:@""];
            
            if ([username length] == 1) {
                if ([string isEqualToString:@"."] || [string isEqualToString:@"@"]) {
                    return NO;
                }
            } else if ([username length] > 20) {
                return NO;
            }
            
            if ([textField.text hasSuffix:@"."]) {
                if ([string  isEqualToString:@"."]) {
                    return NO;
                }
            }
            
            if ([string isEqualToString:@"@"]) {
                if ([textField.text hasSuffix:@"."]) {
                    return NO;
                }
            }
            
            [resultString appendString:username];
        }
        
    }
    

    
    textField.text = resultString;
    //cursor positioning
    UITextPosition *beginning = textField.beginningOfDocument;
    UITextPosition *cursorLocation = [textField positionFromPosition:beginning offset:(range.location + string.length)];
    if (cursorLocation)
    {
        // set start/end location to same spot so that nothing is highlighted
        [textField setSelectedTextRange:[textField textRangeFromPosition:cursorLocation toPosition:cursorLocation]];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:UITextFieldTextDidChangeNotification object:textField];
    
    return NO;
}

- (BOOL)validEmailInTextField:(UITextField *)textField {
    if ([textField.text rangeOfString:@"@"].location != NSNotFound) {
        NSRange rangeOfAt = [textField.text rangeOfString:@"@"];
        NSString *hostname = [textField.text substringFromIndex:rangeOfAt.location + 1];
        if ([hostname rangeOfString:@"."].location == NSNotFound) {
            return NO;
        } else if ([[textField.text substringWithRange:NSMakeRange([textField.text length] - 1, 1)] isEqualToString:@"."]) {
            return NO;
        } else {
            return YES;
        }
    } else {
        return NO;
    }
}

- (BOOL)isValidEmailString:(NSString *)string {
    if ([string rangeOfString:@"@"].location != NSNotFound) {
        NSRange rangeOfAt = [string rangeOfString:@"@"];
        NSString *hostname = [string substringFromIndex:rangeOfAt.location + 1];
        if ([hostname rangeOfString:@"."].location == NSNotFound) {
            return NO;
        } else if ([[string substringWithRange:NSMakeRange([string length] - 1, 1)] isEqualToString:@"."]) {
            return NO;
        } else {
            return YES;
        }
    } else {
        return NO;
    }
}

- (BOOL)validPasswordInTextField:(UITextField *)textField {
    
    NSRange range;
    
    range = [textField.text rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]];
    if (!range.length) {
        return NO;  // no letter
    }
    
    range = [textField.text rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]];
    if (!range.length) {
        return NO;  // no number
    }
    
    if ([textField.text length] < 6) {
        return NO;
    } else if ([textField.text length] > 20) {
        return NO;
    } else {
        return YES;
    }
}

- (NSString *)platform {
    size_t size;
    sysctlbyname(
                 "hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    
    sysctlbyname(
                 "hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithUTF8String:machine];
    
    free(machine);
    return platform;
}

- (NSString *)platformString {
    NSString *platform = [self platform];
    if ([platform isEqualToString:@"iPhone1,1"])
        return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])
        return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])
        return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])
        return @"iPhone 4 (GSM)";
    if ([platform isEqualToString:@"iPhone3,3"])
        return @"iPhone 4 (CDMA)";
    if ([platform isEqualToString:@"iPhone4,1"])
        return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])
        return @"iPhone 5 (GSM)";
    if ([platform isEqualToString:@"iPhone5,2"])
        return @"iPhone 5 (CDMA)";
    if ([platform isEqualToString:@"iPhone5,3"])
        return @"iPhone 5C";
    if ([platform isEqualToString:@"iPhone5,4"])
        return @"iPhone 5C";
    if ([platform isEqualToString:@"iPhone6,1"])
        return @"iPhone 5S";
    if ([platform isEqualToString:@"iPhone6,2"])
        return @"iPhone 5S";
    if ([platform isEqualToString:@"iPhone7,2"])
        return @"iPhone 6";
    if ([platform isEqualToString:@"iPhone7,1"])
        return @"iPhone 6 Plus";
    
    if ([platform isEqualToString:@"iPod1,1"])
        return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])
        return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])
        return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])
        return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPod5,1"])
        return @"iPod Touch 5G";
    
    if ([platform isEqualToString:@"iPad1,1"])
        return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])
        return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])
        return @"iPad 2 (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])
        return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"iPad2,5"])
        return @"iPad Mini (WiFi)";
    if ([platform isEqualToString:@"iPad2,6"])
        return @"iPad Mini (GSM)";
    if ([platform isEqualToString:@"iPad2,7"])
        return @"iPad Mini (CDMA)";
    if ([platform isEqualToString:@"iPad3,1"])
        return @"iPad 3 (WiFi)";
    if ([platform isEqualToString:@"iPad3,2"])
        return @"iPad 3 (CDMA)";
    if ([platform isEqualToString:@"iPad3,3"])
        return @"iPad 3 (GSM)";
    if ([platform isEqualToString:@"iPad3,4"])
        return @"iPad 4 (WiFi)";
    if ([platform isEqualToString:@"iPad3,5"])
        return @"iPad 4 (GSM)";
    if ([platform isEqualToString:@"iPad3,6"])
        return @"iPad 4 (CDMA)";
    
    if ([platform isEqualToString:@"iPad4,1"])
        return @"iPad Air (WiFi)";
    if ([platform isEqualToString:@"iPad4,2"])
        return @"iPad Air (GSM)";
    if ([platform isEqualToString:@"iPad4,3"])
        return @"iPad Air (CDMA)";
    if ([platform isEqualToString:@"iPad4,4"])
        return @"iPad Mini Retina (WiFi)";
    if ([platform isEqualToString:@"iPad4,5"])
        return @"iPad Mini Retina (CDMA)";
    
    if ([platform isEqualToString:@"i386"])
        return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])
        return @"Simulator";
    return platform;
}

+ (NSString *)appVersion {
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
}

+ (NSString *)build {
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
}

+ (UIViewController *)topScreenController {
    UIViewController *rootVC = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    MFSideMenuContainerViewController *container = nil;
    if ([rootVC isKindOfClass:[MFSideMenuContainerViewController class]]) {
        container = (MFSideMenuContainerViewController *)rootVC;
    } else {
        container = (MFSideMenuContainerViewController *)rootVC.presentedViewController;
    }
    if (![container isKindOfClass:[MFSideMenuContainerViewController class]]) {
        if (container == nil) {
            return [UIApplication sharedApplication].keyWindow.rootViewController;
        }
        return container;
    }
    DEMONavigationController *nav = (DEMONavigationController *)[container centerViewController];
    UIViewController *vc = [nav topViewController];
    if (vc == nil) {
        vc = [UIApplication sharedApplication].keyWindow.rootViewController;
    }
    return vc;
}

- (BOOL)userHasGivenNotificationsPermission {
    if (IOS8 || IOS9) {
        return [[[UIApplication sharedApplication] currentUserNotificationSettings] types] != UIUserNotificationTypeNone;
    } else if (IOS7) {
        return [[UIApplication sharedApplication] enabledRemoteNotificationTypes] != UIRemoteNotificationTypeNone;
    } else {
        return NO;
    }
}

- (void)registerApplicationNotificationSettings {
    
    if (IOS8 || IOS9) {
        UIUserNotificationType types = UIUserNotificationTypeBadge |
        UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
        
        UIUserNotificationSettings *mySettings =
        [UIUserNotificationSettings settingsForTypes:types categories:nil];
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
    } else if (IOS7) {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
}

- (void)showShareSheet {
    [self showShareSheetWithCompletion:nil];
}

- (void)showShareSheetWithCompletion:(UIActivityViewControllerCompletionWithItemsHandler)handler {
    /*NSString *textToShare = @"Vicinity is a social network that allows you to interact with people around you. Download for free at:";
    NSURL *myWebsite = [NSURL URLWithString:@"http://vcnty.co/app"];
    
    NSArray *objectsToShare = @[textToShare, myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    if (handler) {
        activityVC.completionWithItemsHandler = handler;
    }
    activityVC.excludedActivityTypes = excludeActivities;
    UIViewController *topVC = [[self class] topScreenController];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [topVC presentViewController:activityVC animated:YES completion:nil];
    });*/
    [self generateUrl:^(NSString *urlString) {
        NSLog(@"url string %@", urlString);
        NSString *textToShare = [NSString stringWithFormat:@"Vicinity is a social network that allows you to interact with people around you. Click here to download for free!"];
        NSURL *myWebsite = [NSURL URLWithString:urlString];
        //UIImage *image = [UIImage imageNamed:@"image-for-sharing"];
        
        NSArray *objectsToShare = @[/*image, */textToShare, myWebsite];
        
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
        
        NSArray *excludeActivities = @[UIActivityTypePrint,
                                       UIActivityTypeAssignToContact,
                                       UIActivityTypeSaveToCameraRoll,
                                       UIActivityTypeAddToReadingList,
                                       UIActivityTypePostToFlickr,
                                       UIActivityTypePostToVimeo];
        if (handler) {
            activityVC.completionWithItemsHandler = handler;
        }
        activityVC.excludedActivityTypes = excludeActivities;
        UIViewController *topVC = [[self class] topScreenController];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [topVC presentViewController:activityVC animated:YES completion:nil];
        });
    }];
}

- (void)showSharePostSheetWithPost:(VAPost*)post image:(UIImage*)image completion:(UIActivityViewControllerCompletionWithItemsHandler)handler {
    NSString *textToShare;
    if (post.text.length) {
        textToShare = [NSString stringWithFormat:@"%@ - via Vicinity - The Local Network. Download at http://vcnty.co/app", post.text];
    } else {
        textToShare = @"via Vicinity - The Local Network. Download at http://vcnty.co/app";
    }
    NSMutableArray* objectsToShare = [NSMutableArray array];
    if (textToShare) {
        [objectsToShare addObject:textToShare];
    }
    if (image) {
        [objectsToShare addObject:image];
    }

    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];

    NSArray *excludeActivities = @[UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    if (handler) {
        activityVC.completionWithItemsHandler = handler;
    }
    activityVC.excludedActivityTypes = excludeActivities;
    UIViewController *topVC = [[self class] topScreenController];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [topVC presentViewController:activityVC animated:YES completion:nil];
    });
}

- (void)generateUrl:(void(^)(NSString *urlString))completion {
    BranchUniversalObject *branchUniversalObject = [[BranchUniversalObject alloc] initWithCanonicalIdentifier:@"http://vcnty.co/app"];
    branchUniversalObject.title = @"Vicinity";
    branchUniversalObject.imageUrl = @"http://getvicinityapp.com/vicinity-share.png";
    branchUniversalObject.contentDescription = [NSString stringWithFormat:@"Vicinity is a social network that allows you to interact with people around you. Click here to download for free!"];
    [branchUniversalObject addMetadataKey:@"username" value:[VAUserDefaultsHelper me].nickname];
    
    BranchLinkProperties *linkProperties = [[BranchLinkProperties alloc] init];
    linkProperties.feature = @"invite";
    linkProperties.tags = @[[VAUserDefaultsHelper me].nickname];
    NSLog(@"User ID %@", [VAUserDefaultsHelper me].nickname);
    [linkProperties addControlParam:@"$desktop_url" withValue:@"http://vcnty.co/app"];
    [linkProperties addControlParam:@"$ios_url" withValue:@"http://vcnty.co/app"];
    [branchUniversalObject getShortUrlWithLinkProperties:linkProperties andCallback:^(NSString *url, NSError *error) {
        if (!error) {
            completion(url);
        }
    }];
}

- (void)setFacebookLoginMethod {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kFacebookLoginMethod];
}

- (void)setEmailLoginMethod {
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kFacebookLoginMethod];
}

- (VALoginMethod)currentLoginMethod {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:kFacebookLoginMethod]) {
        return VALoginMethodFacebook;
    } else {
        return VALoginMethodEmail;
    }
}

- (void)logout {
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kShouldShowNotificationBadge];
    
    [[VAPostStore sharedPostStore] clearCache];
        
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    
    [VAUserDefaultsHelper setAuthToken:nil];
    
    VALoginViewController *vc = [[UIStoryboard storyboardWithName:@"LoginFlow" bundle:nil] instantiateInitialViewController];
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    UIViewController *rootVC = window.rootViewController;
    [UIView transitionWithView:window
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{
                        window.rootViewController = vc;
                    }
                    completion:nil];
    for (UIView *subview in window.subviews) {
        if ([subview isKindOfClass:NSClassFromString(@"UITransitionView")]) {
            [subview removeFromSuperview];
        }
    }
    [rootVC dismissViewControllerAnimated:NO completion:^{
        [rootVC.view removeFromSuperview];
    }];
}

- (void)logoutForBlockedUser {
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kShouldShowNotificationBadge];
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    
    [VAUserDefaultsHelper setAuthToken:nil];
    
    VALoginViewController *vc = [[UIStoryboard storyboardWithName:@"LoginFlow" bundle:nil] instantiateInitialViewController];
    vc.showUserBlockedMessage = YES;
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    UIViewController *rootVC = window.rootViewController;
    [UIView transitionWithView:window
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{
                        window.rootViewController = vc;
                    }
                    completion:nil];
    for (UIView *subview in window.subviews) {
        if ([subview isKindOfClass:NSClassFromString(@"UITransitionView")]) {
            [subview removeFromSuperview];
        }
    }
    [rootVC dismissViewControllerAnimated:NO completion:^{
        [rootVC.view removeFromSuperview];
    }];
}

- (void)showWelcomeScreen {
    if (![[[[UIApplication sharedApplication] keyWindow] rootViewController] isKindOfClass:[VAWelcomeViewController class]]) {
        VAWelcomeViewController *welcomeVC = [[UIStoryboard storyboardWithName:@"Vicinity" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"welcomeViewController"];

        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        UIViewController *rootVC = window.rootViewController;
        [UIView transitionWithView:window
                          duration:0.5
                           options:UIViewAnimationOptionTransitionFlipFromLeft
                        animations:^{
                            window.rootViewController = welcomeVC;
                        }
                        completion:nil];
        for (UIView *subview in window.subviews) {
            if ([subview isKindOfClass:NSClassFromString(@"UITransitionView")]) {
                [subview removeFromSuperview];
            }
        }
        [rootVC dismissViewControllerAnimated:NO completion:^{
            [rootVC.view removeFromSuperview];
        }];
    }
}

- (void)clearDeviceTokenOnServer {
    [[VAUserApiManager alloc] getLogoutWithDeviceToken:[VAUserDefaultsHelper getDeviceToken] andCompletion:^(NSError *error, VAModel *model) {
        
    }];
}

- (void)openAppSettings {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", UIApplicationOpenSettingsURLString, [[NSBundle mainBundle] bundleIdentifier]]];
    [[UIApplication sharedApplication] openURL:url];
}

@end
