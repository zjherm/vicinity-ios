//
//  VAChooseLocationViewController.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 04.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAChooseLocationViewController.h"
#import "VALocationTableViewCell.h"
#import "VAUserApiManager.h"
#import "VANotificationMessage.h"
#import "VALocationResponse.h"
#import "VALocation.h"
#import "VALoaderView.h"
#import "NSNumber+VAMilesDistance.h"
#import "DEMONavigationController.h"
#import <Google/Analytics.h>

#import "VAGooglePlacesApiManager.h"
#import "VAPlaceDetails.h"
#import "VAGoogleLocation.h"

@interface VAChooseLocationViewController ()
// Data source
@property (strong, nonatomic) NSArray *locationsArray;
@property (strong, nonatomic) NSArray *searchResultsArray;

@property (nonatomic, getter=isSearchMode) BOOL searchMode;

@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
@property (strong, nonatomic) NSNumber *latitude;
@property (strong, nonatomic) NSNumber *longitude;

@property (strong, nonatomic) VALoaderView *loader;
@property (strong, nonatomic) VALoaderView *refresh;
@property (strong, nonatomic) NSTimer *searchTimer;
@property (strong, nonatomic) NSString *searchString;
@property (strong, nonatomic) UIBarButtonItem *rightItem;

@property (weak, nonatomic) IBOutlet UIView *searchBar;
@property (weak, nonatomic) IBOutlet UITextField *searchField;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomTableViewConstraint;

- (IBAction)searchFieldDidChange:(UITextField *)textField;

@property (nonatomic, strong) NSString *pageToken;
@end

CGFloat bottomConstraintConst = 9.f;
BOOL twoSecondsLeft = NO;

@implementation VAChooseLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [(DEMONavigationController *)self.navigationController setDefaultNavigationBar];
    
    self.locationsArray = [NSArray array];
    self.searchResultsArray = [NSArray array];
    
    self.searchMode = NO;
    
    self.loader = nil;
    self.searchTimer = nil;
    self.searchString = @"";
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self setupCurrentLocation];

    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.rightBarButtonItem = nil;
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackButton"] style:UIBarButtonItemStylePlain target:self.navigationController action:@selector(popViewControllerAnimated:)];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"VerifyButton"] style:UIBarButtonItemStylePlain target:self action:@selector(actionUpdateLocation)];
    self.rightItem = rightItem;
    
    self.searchBar.layer.cornerRadius = 5.f;
    self.searchBar.clipsToBounds = YES;
    self.searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search Location" attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
    
    self.tableView.layer.cornerRadius = 5.f;
    self.tableView.clipsToBounds = YES;
    
    self.selectedIndexPath = nil;
    
    [self setupRefreshControl];
    
    self.pageToken = nil;
    
    [self showActivityIndicator];
    [self loadAllAvailablePlaces];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationController.title = @"Choose Location";

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Check-in search screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Setup

- (void)setupCurrentLocation {
    double lat = [[NSUserDefaults standardUserDefaults] doubleForKey:kLatValue];
    double lon = [[NSUserDefaults standardUserDefaults] doubleForKey:kLonValue];
    self.latitude = [NSNumber numberWithDouble:lat];
    self.longitude = [NSNumber numberWithDouble:lon];
}

#pragma mark - Actions

- (IBAction)searchFieldDidChange:(UITextField *)textField {
    self.searchMode = YES;
    if (self.searchTimer) {
        [self.searchTimer invalidate];
    }
    [[self mutableArrayValueForKey:@"searchResultsArray"] removeAllObjects];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self showActivityIndicator];
    [self.tableView reloadData];
    
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:1.2f
                                                          target:self
                                                        selector:@selector(makeSearchRequest)
                                                        userInfo:nil
                                                         repeats:NO];
    self.searchTimer = timer;

    NSString *searchString = textField.text;
    self.searchString = searchString;
}

- (void)actionUpdateLocation {
    VACoordinate *fromLocation = [[VACoordinate alloc] init];
    fromLocation.latitude = self.currentLocation.location.latitude;
    fromLocation.longitude = self.currentLocation.location.longitude;
    VACoordinate *toLocation = [[VACoordinate alloc] init];
    toLocation.latitude = self.latitude;
    toLocation.longitude = self.longitude;
    VALocation *location = [VALocation locationFromPlaceDetails:self.currentLocation withDistance:[self distanceFromLocation:fromLocation toLocation:toLocation]];
    [self.delegate userDidChooseLocation:location];
    [self.navigationController popViewControllerAnimated:YES];
    
    if (self.currentLocation) {
        //code for google analytics
        id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Check In"
                                                              action:@"User changed check in"
                                                               label:nil
                                                               value:nil] build]];
    } else {
        //code for google analytics
        id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Check In"
                                                              action:@"User checked in"
                                                               label:nil
                                                               value:nil] build]];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.isSearchMode ? [self.searchResultsArray count] : [self.locationsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self basicCellForIndexPath:indexPath];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self dismissKeyboard:nil];
    VAPlaceDetails *location = self.isSearchMode ? [self.searchResultsArray objectAtIndex:indexPath.row] : [self.locationsArray objectAtIndex:indexPath.row];
    self.currentLocation = location;
    if (self.selectedIndexPath) {
        VALocationTableViewCell *oldCell = (VALocationTableViewCell *)[tableView cellForRowAtIndexPath:self.selectedIndexPath];
        [self setDefaultPropertiesForCell:oldCell];
    }
    VALocationTableViewCell *newCell = (VALocationTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    [self setSelectedPropertiesForCell:newCell];
    self.selectedIndexPath = indexPath;
    
    if (!self.navigationItem.rightBarButtonItem) {
        self.navigationItem.rightBarButtonItem = self.rightItem;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSIndexPath *startRequestIndexPath = [NSIndexPath indexPathForRow:[self.locationsArray count] - 5 inSection:0];
    
    if ([indexPath isEqual:startRequestIndexPath] && self.pageToken && [self.searchField.text isEqualToString:@""]) {
        VALoaderView *loader = [VALoaderView initLoader];
        loader.frame = CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), kLoaderHeight);
        [loader startAnimating];
        self.tableView.tableFooterView = loader;
        
        CGFloat delay;
        if (!twoSecondsLeft) {
            delay = 1.f;
        } else {
            delay = 0.f;
        }
        twoSecondsLeft = NO;

        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self loadAllAvailablePlaces];
        });
    }
}


#pragma mark - Keyboard actions

- (void)willKeyboardShow:(NSNotification*)notification {
    NSDictionary* info = [notification userInfo];
    CGRect keyRect;
    [[info objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyRect];
    CGFloat height = keyRect.size.height;
    NSIndexPath *indexPath = [[self.tableView indexPathsForVisibleRows] lastObject];
    self.bottomTableViewConstraint.constant = bottomConstraintConst + height;
    
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.tableView setFrame:CGRectMake(CGRectGetMinX(self.tableView.frame), CGRectGetMinY(self.tableView.frame), CGRectGetWidth(self.tableView.frame), CGRectGetHeight(self.tableView.frame) - height)];
    }];
    
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}

- (void)willKeyboardHide:(NSNotification*)notification {
    NSDictionary* info = [notification userInfo];
    CGRect keyRect;
    [[info objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyRect];
    CGFloat height = keyRect.size.height;
    NSIndexPath *indexPath = [[self.tableView indexPathsForVisibleRows] lastObject];
    self.bottomTableViewConstraint.constant = bottomConstraintConst;
    [UIView animateWithDuration:0.3f animations:^{
        [self.tableView setFrame:CGRectMake(CGRectGetMinX(self.tableView.frame), CGRectGetMinY(self.tableView.frame), CGRectGetWidth(self.tableView.frame), CGRectGetHeight(self.tableView.frame) + height)];
    }];
    
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}

#pragma mark - Methods

- (void)dismissKeyboard:(id)sender {
    [self.searchField resignFirstResponder];
}

- (VALocationTableViewCell *)basicCellForIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = @"Location";
    
    VALocationTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier];
        
    [self configureGoogleCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureGoogleCell:(VALocationTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    VAPlaceDetails *location = nil;
    if (!self.isSearchMode && [self.locationsArray count] > 0) {
        location = [self.locationsArray objectAtIndex:indexPath.row];
    } else if (self.isSearchMode && [self.searchResultsArray count] > 0) {
        location = [self.searchResultsArray objectAtIndex:indexPath.row];
    }
    if ([location.placeId isEqualToString:self.currentLocation.placeId]) {
        [self setSelectedPropertiesForCell:cell];
        self.selectedIndexPath = indexPath;
    } else {
        [self setDefaultPropertiesForCell:cell];
    }
    
    if (self.selectedIndexPath) {
        self.navigationItem.rightBarButtonItem = self.rightItem;
    }
    
    cell.locationNameLabel.text = location.name;
    VACoordinate *fromLocation = [[VACoordinate alloc] init];
    fromLocation.latitude = location.location.latitude;
    fromLocation.longitude = location.location.longitude;
    VACoordinate *toLocation = [[VACoordinate alloc] init];
    toLocation.latitude = self.latitude;
    toLocation.longitude = self.longitude;
    cell.distanceLabel.text = [[self distanceFromLocation:fromLocation toLocation:toLocation] milesStringFromNSNumberMeters:YES];
    cell.addressLabel.text = location.vicinity;
    [self showStarsForRating:[location.rating floatValue] inCell:cell];
    [cell.checkmarkView setHidden:YES];
}

- (void)configureCell:(VALocationTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    VALocation *location = nil;
    if ([self.locationsArray count] > 0) {
        location = [self.locationsArray objectAtIndex:indexPath.row];
    }
    if ([location.placeID isEqualToString:self.currentLocation.placeId]) {
        [self setSelectedPropertiesForCell:cell];
        self.selectedIndexPath = indexPath;
    } else {
        [self setDefaultPropertiesForCell:cell];
    }
    
    if (self.selectedIndexPath) {
        self.navigationItem.rightBarButtonItem = self.rightItem;
    }
    
    cell.locationNameLabel.text = location.name;
    cell.distanceLabel.text = [location.distance milesStringFromNSNumberMeters:YES];
    cell.addressLabel.text = location.address;
    [self showStarsForRating:[location.rating floatValue] inCell:cell];
    [cell.checkmarkView setHidden:YES];
}

- (void)showStarsForRating:(CGFloat)rating inCell:(VALocationTableViewCell *)cell {
    NSInteger integralPart = (NSInteger)rating;
    double fractionalPart = rating - integralPart;
    if (fractionalPart > 0.5) {
        integralPart +=1;
    }
    for (NSInteger j = 0; j < integralPart; j++) {
        UIImageView *star = [cell.starsArray objectAtIndex:j];
        star.image = [UIImage imageNamed:@"star-selected.png"];
    }
}

- (void)showActivityIndicator {
    if (self.loader) {
        [self hideActivityIndicator];
    }
    VALoaderView *loader = [VALoaderView initLoader];
    loader.frame = CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), kLoaderHeight);
    [loader startAnimating];
    self.tableView.tableHeaderView = loader;
    self.loader = loader;
}

- (void)hideActivityIndicator {
    [self.loader stopAnimating];
    self.tableView.tableHeaderView = nil;
}

- (void)setDefaultPropertiesForCell:(VALocationTableViewCell *)cell {
    cell.backgroundColor = [UIColor whiteColor];
    cell.locationNameLabel.textColor = [UIColor blackColor];
    cell.addressLabel.textColor = [[VADesignTool defaultDesign] middleGrayColor];
    cell.distanceLabel.textColor = [UIColor lightGrayColor];
    [cell.distanceLabel setHidden:NO];
    [cell.checkmarkView setHidden:YES];
}

- (void)setSelectedPropertiesForCell:(VALocationTableViewCell *)cell {
    cell.backgroundColor = [[VADesignTool defaultDesign] vicinityBlue];
    cell.locationNameLabel.textColor = [UIColor whiteColor];
    cell.addressLabel.textColor = [[VADesignTool defaultDesign] veryLightGrayColor];
    cell.distanceLabel.textColor = [UIColor whiteColor];
    [cell.distanceLabel setHidden:NO];
    [cell.checkmarkView setHidden:YES];
    [cell.checkmarkView setImage:[UIImage imageNamed:@"location-checkmark"]];
}

- (void)twoSecondsLeft {
    twoSecondsLeft = YES;
}

- (void)fireTimer {
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:2.f
                                                      target:self
                                                    selector:@selector(twoSecondsLeft)
                                                    userInfo:nil
                                                     repeats:NO];
}

- (void)setupRefreshControl {
    VALoaderView *refresh = [VALoaderView initLoader];
    self.refresh = refresh;
    [refresh setFrame:[self frameForCustomRefreshControl]];
    refresh.alpha = 0.f;
    
    [refresh addTarget:self
                action:@selector(refreshData)
      forControlEvents:UIControlEventValueChanged];
    
    [self.tableView addSubview:refresh];
}

- (CGRect)frameForCustomRefreshControl {
    CGFloat customRefreshControlHeight = kLoaderHeight;
    CGFloat customRefreshControlWidth = CGRectGetWidth([self.tableView frame]);
    CGRect customRefreshControlFrame = CGRectMake(0.0f,
                                                  -customRefreshControlHeight,
                                                  customRefreshControlWidth,
                                                  customRefreshControlHeight);
    return customRefreshControlFrame;
}

- (void)refreshData {
    [self.refresh startRefreshing];
    NSLog(@"GetPlacesNextPage started.");
    [[VAGooglePlacesApiManager sharedManager] getPlacesInBoundsWithCenterLatitude:self.latitude
                                                                  centerLongitude:self.longitude
                                                                           radius:@(5000)
                                                                       completion:^(NSError *error, id responseObject, NSString *nextPageToken)
    {
        NSLog(@"GetPlacesNextPage completed.");
        [self.refresh stopRefreshing];
        if (!error) {
            self.pageToken = nextPageToken;
            [[self mutableArrayValueForKey:@"locationsArray"] removeAllObjects];
            [[self mutableArrayValueForKey:@"locationsArray"] addObjectsFromArray:responseObject];
            [self.tableView reloadData];
        } else {
            self.pageToken = nil;
        }
    }];
}

- (void)makeSearchRequest {
    self.searchTimer = nil;
    if ([self.searchString isEqualToString:@""]) {
        self.searchMode = NO;
        self.pageToken = nil;
        NSLog(@"GetPlacesInBounds started.");
        [[VAGooglePlacesApiManager sharedManager] getPlacesInBoundsWithCenterLatitude:self.latitude
                                                                      centerLongitude:self.longitude
                                                                               radius:@(5000)
                                                                           completion:^(NSError *error, id responseObject, NSString *nextPageToken)
        {
            NSLog(@"GetPlacesInBounds completed.");
            if (!error) {
                self.pageToken = nextPageToken;
                [[self mutableArrayValueForKey:@"locationsArray"] removeAllObjects];
                [[self mutableArrayValueForKey:@"locationsArray"] addObjectsFromArray:responseObject];
                self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
                [self hideActivityIndicator];
                [self.tableView reloadData];
            } else {
                [self hideActivityIndicator];
                [VANotificationMessage showLoadErrorMessageOnController:self
                                                           withDuration:5.f];
            }
        }];
    } else {
        self.searchMode = YES;
        NSLog(@"GetPlacesTips started.");
        [[VAGooglePlacesApiManager sharedManager] getPlacesTipsWithCenterLatitude:self.latitude
                                                                  centerLongitude:self.longitude
                                                                           radius:@(1000)
                                                                          keyword:self.searchString
                                                                       completion:^(NSError *error, id responseObject)
        {
            NSLog(@"GetPlacesTips completed.");
            if (!error) {
                [[self mutableArrayValueForKey:@"searchResultsArray"] removeAllObjects];
                [[self mutableArrayValueForKey:@"searchResultsArray"] addObjectsFromArray:responseObject];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", self.searchString];
                NSArray *matchedResults = [self.locationsArray filteredArrayUsingPredicate:predicate];
                for (VAPlaceDetails *place in matchedResults) {
                    NSPredicate *idPredicate = [NSPredicate predicateWithFormat:@"placeId == %@", place.placeId];
                    NSArray* uniqueValues = [self.searchResultsArray filteredArrayUsingPredicate:idPredicate];
                    if (![uniqueValues count]) {
                        [[self mutableArrayValueForKey:@"searchResultsArray"] addObject:place];
                    }
                }
                for (VAPlaceDetails *place in responseObject) {
                    [self setDistanceForPlaceDetails:place];
                }
                NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"distance" ascending:YES];
                [[self mutableArrayValueForKey:@"searchResultsArray"] sortUsingDescriptors:@[sortDescriptor]];
                self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
                [self hideActivityIndicator];
                [self.tableView reloadData];
            } else {
                [self hideActivityIndicator];
                [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
            }
        }];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    return [textField resignFirstResponder];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self.refresh containingScrollViewDidEndDragging:scrollView];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.refresh containingScrollViewDidScroll:scrollView];
}

#pragma mark - Helpers

- (NSNumber *)distanceFromLocation:(VACoordinate *)oldCoordinate
                        toLocation:(VACoordinate *)newCoordinate {
    CLLocation *oldLocation = [[CLLocation alloc] initWithLatitude:[oldCoordinate.latitude doubleValue] longitude:[oldCoordinate.longitude doubleValue]];
    CLLocation *newLocation = [[CLLocation alloc] initWithLatitude:[newCoordinate.latitude doubleValue] longitude:[newCoordinate.longitude doubleValue]];
    CLLocationDistance distance = [newLocation distanceFromLocation:oldLocation];
    return @(distance);
}

- (void)setDistanceForPlaceDetails:(VAPlaceDetails *)placeDetails {
    VACoordinate *fromLocation = [[VACoordinate alloc] init];
    fromLocation.latitude = placeDetails.location.latitude;
    fromLocation.longitude = placeDetails.location.longitude;
    VACoordinate *toLocation = [[VACoordinate alloc] init];
    toLocation.latitude = self.latitude;
    toLocation.longitude = self.longitude;
    placeDetails.distance = [self distanceFromLocation:fromLocation toLocation:toLocation];
}

#pragma mark - Load data

- (void)loadAllAvailablePlaces {
    if (self.pageToken) {
        NSLog(@"GetPlacesNextPage started.");
        [[VAGooglePlacesApiManager sharedManager] getPlacesWithPageToken:self.pageToken completion:^(NSError *error, id responseObject, NSString *nextPageToken) {
            NSLog(@"GetPlacesNextPage completed.");
            if (!error) {
                self.pageToken = nextPageToken;
                self.tableView.tableFooterView = nil;
                self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
                [[self mutableArrayValueForKey:@"locationsArray"] addObjectsFromArray:responseObject];
                [self.tableView reloadData];
                [self fireTimer];
            } else {
                [self hideActivityIndicator];
                [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
            }
        }];
    } else {
        NSLog(@"GetPlacesInBounds started.");
        [[VAGooglePlacesApiManager sharedManager] getPlacesInBoundsWithCenterLatitude:self.latitude
                                                                      centerLongitude:self.longitude
                                                                               radius:@(5000)
                                                                           completion:^(NSError *error, id responseObject, NSString *nextPageToken)
        {
            NSLog(@"GetPlacesInBounds completed.");
            if (!error) {
                self.pageToken = nextPageToken;
                [self hideActivityIndicator];
                self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
                [[self mutableArrayValueForKey:@"locationsArray"] addObjectsFromArray:responseObject];
                [self.tableView reloadData];
                [self fireTimer];
            } else {
                [self hideActivityIndicator];
                [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
            }
        }];
    }
}

@end
