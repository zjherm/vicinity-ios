//
//  VAUserTool.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 24.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString *kUserFullName = @"kUserFullName";
static NSString *kUserID = @"kUserID";
static NSString *kUserPersonalToken = @"kUserPersonalToken";
static NSString *kUserGender = @"kUserGender";
static NSString *kUserImageURL = @"kUserImageURL";
static NSString *kUserEmail = @"kUserEmail";
static NSString *kUserDataLoaded = @"kUserDataLoaded";
static NSString *kUserAvatarImage = @"kUserAvatarImage";

@interface VAUserTool : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *userID;
@property (strong, nonatomic) NSString *personalToken;
@property (strong, nonatomic) NSString *gender;
@property (strong, nonatomic) NSString *imageURL;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) UIImage *avatarImage;

+ (VAUserTool *)currentUser;

- (void)save;
- (void)load;

@end
