//
//  VAContactsView.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/22/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAContactsView.h"
#import "VAContactManager.h"
#import "VAFriendsTableViewCell.h"
#import "VAContactTableViewCell.h"
#import "VAUser.h"
#import "UITableViewCell+VAParentCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "VANotificationMessage.h"
#import "VAControlTool.h"
#import "VAProfileViewController.h"
#import "VAUserApiManager.h"
#import "VALoaderView.h"
#import "VAContact.h"
#import "VAUserSearchResponse.h"
#import <MessageUI/MessageUI.h>
#import "VAUserDefaultsHelper.h"

@interface VAContactsView () <VAFacebookFriendsDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>
@property (strong, nonatomic) NSMutableArray *contactsArray;
@property (strong, nonatomic) NSArray *vicinityUsersArray;
@property (strong, nonatomic) VALoaderView *headerLoader;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation VAContactsView

- (void)awakeFromNib {
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.vicinityUsersArray = [NSArray array];
    self.contactsArray = [NSMutableArray array];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return [self.vicinityUsersArray count];
    } else {
        return [self.contactsArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *friendsIdentifier = @"VicinityFriends";
    static NSString *contactsIdentifier = @"ContactsFriends";
    NSString *identifier = nil;
    
    if (indexPath.section == 0) {
        identifier = friendsIdentifier;
    } else {
        identifier = contactsIdentifier;
    }
    
    if (indexPath.section == 0) {
        VAFriendsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.delegate = self;
        [self configureFriendsCell:cell atIndexPath:indexPath];
        return cell;
    } else {
        VAContactTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        [self configureContactsCell:cell atIndexPath:indexPath];
        return cell;
    }
    return nil;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.f;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        if ([self.vicinityUsersArray count] > 0) {
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.bounds), 35)];
            view.backgroundColor = [UIColor whiteColor];
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, CGRectGetWidth(self.bounds) - 16 * 2, 35)];
            [label setFont:[[VADesignTool defaultDesign] vicinityRegularFontOfSize:12.f]];
            [label setText:@"Contacts on Vicinity"];
            [label setTextColor:[UIColor lightGrayColor]];
            [view addSubview:label];
            UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(0, 34.f, CGRectGetWidth(view.bounds), 1.f)];
            separator.backgroundColor = [[[VADesignTool defaultDesign] clearGray] colorWithAlphaComponent:0.5f];
            [view addSubview:separator];
            return view;
        }
    } else {
        if ([self.contactsArray count] > 0) {

            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.bounds), 35)];
            view.backgroundColor = [UIColor whiteColor];
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, CGRectGetWidth(self.bounds) - 16 * 2, 35)];
            [label setFont:[[VADesignTool defaultDesign] vicinityRegularFontOfSize:12.f]];
            [label setText:@"Invite contacts to Vicinity"];
            [label setTextColor:[UIColor lightGrayColor]];
            [view addSubview:label];
            UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(0, 34.f, CGRectGetWidth(view.bounds), 1.f)];
            separator.backgroundColor = [[[VADesignTool defaultDesign] clearGray] colorWithAlphaComponent:0.5f];
            [view addSubview:separator];
            return view;
        }
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if ([self.vicinityUsersArray count] > 0 && section == 0) {
        return 35;
    } else if ([self.contactsArray count] > 0 && section == 1) {
        return 35;
    }
    return 0;
}

#pragma mark - Actions

- (IBAction)addFriendButtonPushed:(UIButton *)sender {
    VAUser *user = [self userForCellWithPushedButton:sender];
    VAFriendsTableViewCell *cell = (VAFriendsTableViewCell *)[UITableViewCell findParentCellOfView:sender];
    
    if ([user.isFriend boolValue] == 1) {
        [cell.addButton setImage:[UIImage imageNamed:@"add-contact-icon"] forState:UIControlStateNormal];
        user.isFriend = @(NO);
        [self deleteFriend:user];
    } else {
        [cell.addButton setImage:[UIImage imageNamed:@"Checkmark"] forState:UIControlStateNormal];
        user.isFriend = @(YES);
        [self addFriend:user];
    }
}

- (IBAction)sendEmailButtonPushed:(UIButton *)sender {
    VAContact *contact = [self contactForCellWithPushedButton:sender];
    if ([contact.emails count] == 1) {
        [self sendEmailToAddress:[contact.emails firstObject]];
    } else {
        UIAlertController *ac = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        for (NSString *email in contact.emails) {
            UIAlertAction *action = [UIAlertAction actionWithTitle:email style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                [self sendEmailToAddress:email];
            }];
            [ac addAction:action];
        }
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        [ac addAction:cancel];
        [[VAControlTool topScreenController] presentViewController:ac animated:YES completion:nil];
    }
}

- (IBAction)sendTextButtonPushed:(UIButton *)sender {
    VAContact *contact = [self contactForCellWithPushedButton:sender];
    if ([contact.phones count] == 1) {
        [self sendTextToPhoneNumber:[contact.phones firstObject]];
    } else {
        UIAlertController *ac = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        for (NSString *phone in contact.phones) {
            UIAlertAction *action = [UIAlertAction actionWithTitle:phone style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                [self sendTextToPhoneNumber:phone];
            }];
            [ac addAction:action];
        }
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        [ac addAction:cancel];
        [[VAControlTool topScreenController] presentViewController:ac animated:YES completion:nil];
    }
}

#pragma mark - Methods

- (void)configureFriendsCell:(VAFriendsTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.indexPath = indexPath;
    
    VAUser *user = [self.vicinityUsersArray objectAtIndex:indexPath.row];
    
    if (user.avatarURL) {
        NSURL *imageURL = [NSURL URLWithString:user.avatarURL];
        
        [cell.avatarView sd_setImageWithURL:imageURL
                           placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                      if (image) {
                                          cell.avatarView.image = image;
                                      }
                                      if (error) {
                                          [VANotificationMessage showAvatarErrorMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
                                      }
                                  }];
    } else {
        cell.avatarView.image = [UIImage imageNamed:@"placeholder.png"];
    }
    
    cell.nameLabel.text = user.fullname;
    cell.nicknameLabel.text = user.nickname;
    
    if (indexPath.row == [self.vicinityUsersArray count] - 1) {
        [cell.separatorView setHidden:YES];
    } else {
        [cell.separatorView setHidden:NO];
    }
    
    if ([user.isFriend boolValue] == 1) {
        [cell.addButton setImage:[UIImage imageNamed:@"Checkmark"] forState:UIControlStateNormal];
    } else {
        [cell.addButton setImage:[UIImage imageNamed:@"add-contact-icon"] forState:UIControlStateNormal];
    }
}

- (void)configureContactsCell:(VAContactTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    VAContact *contact = [self.contactsArray objectAtIndex:indexPath.row];
    
    if (contact.imageData) {
        UIImage *image = [[UIImage alloc]initWithData:contact.imageData];
        cell.avatarView.hidden = NO;
        cell.avatarPlaceholderLabel.hidden = YES;
        cell.avatarView.image = image;
    } else {
        cell.avatarView.hidden = YES;
        cell.avatarPlaceholderLabel.hidden = NO;
        NSMutableString* initials = [@"" mutableCopy];
        if ([contact.firstName length] > 0) {
            [initials appendString:[[contact.firstName substringToIndex:1] uppercaseString]];
        }
        if ([contact.lastName length] > 0) {
            [initials appendString:[[contact.lastName substringToIndex:1] uppercaseString]];
        }
        cell.avatarPlaceholderLabel.text = initials;
    }
    
    cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@", contact.firstName ? contact.firstName : @"", contact.lastName ? contact.lastName : @""];
    
    if (indexPath.row == [self.contactsArray count] - 1) {
        [cell.separatorView setHidden:YES];
    } else {
        [cell.separatorView setHidden:NO];
    }
    
    if ([contact.emails count] > 0) {
        [cell.emailButton setImage:[UIImage imageNamed:@"email-icon-active"] forState:UIControlStateNormal];
        cell.emailButton.userInteractionEnabled = YES;
    } else {
        [cell.emailButton setImage:[UIImage imageNamed:@"email-icon-inactive"] forState:UIControlStateNormal];
        cell.emailButton.userInteractionEnabled = NO;
    }
    
    if ([contact.phones count] > 0) {
        [cell.textButton setImage:[UIImage imageNamed:@"text-icon-active"] forState:UIControlStateNormal];
        cell.textButton.userInteractionEnabled = YES;
    } else {
        [cell.textButton setImage:[UIImage imageNamed:@"text-icon-inactive"] forState:UIControlStateNormal];
        cell.textButton.userInteractionEnabled = NO;
    }
}

- (void)showProfileForUserWithAlias:(NSString *)alias {
    VAProfileViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"profileViewController"];
    vc.searchAlias = alias;
    vc.isUsersAccountMode = YES;
    UIViewController *topController = [VAControlTool topScreenController];
    [topController.navigationController pushViewController:vc animated:YES];
}

- (void)sortAllContacts {
    [self.contactsArray removeAllObjects];
    [[self mutableArrayValueForKey:@"vicinityUsersArray"] removeAllObjects];
    [self.tableView reloadData];
    [self showLoader];
    
    self.contactsArray = [VAContactManager getAllContacts];
    [self sortContacts];
    
    NSMutableArray *temp = [NSMutableArray array];
    for (NSDictionary *userInfo in self.contactsArray) {
        VAContact *contact = [[VAContact alloc] initWithDict:userInfo];
        [temp addObject:contact];
    }
    self.contactsArray = temp;
    
    NSArray *emails = [self.contactsArray valueForKeyPath:@"@distinctUnionOfArrays.emails"];
    
    [self performSelectorOnMainThread:@selector(sendGetVicinityUsersRequestForEmailsArray:) withObject:emails waitUntilDone:NO];
}

- (void)sendGetVicinityUsersRequestForEmailsArray:(NSArray *)emails {
    NSString *emailsList = [emails componentsJoinedByString:@", "];
    
    [[VAUserApiManager new] getUsersByEmailListSearch:emailsList
                                       withCompletion:^(NSError *error, VAModel *model) {
                                           if (error) {
                                               [self hideLoader];
                                           } else if (model) {
                                               VAUser *user = (VAUser *)[VAUserDefaultsHelper getModelForKey:VASaveKeys_CurrentUser];
                                               NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId != %@", user.userId];
                                               VAUserSearchResponse *response = (VAUserSearchResponse *)model;
                                               [[self mutableArrayValueForKey:@"vicinityUsersArray"] addObjectsFromArray:[response.users filteredArrayUsingPredicate:predicate]];
                                               
                                               [self removeUsersFromContacts];
                                               [self hideLoader];
                                               [self.tableView reloadData];
                                           }
                                       }];
}

- (void)showLoader {
    VALoaderView *loader = [VALoaderView initLoader];
    loader.frame = CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), kLoaderHeight);
    [loader startAnimating];
    self.tableView.tableHeaderView = loader;
    self.headerLoader = loader;
}

- (void)hideLoader {
    self.tableView.tableHeaderView = nil;
}

- (VAUser *)userForCellWithPushedButton:(UIButton *)button {
    VAFriendsTableViewCell *cell = (VAFriendsTableViewCell *)[UITableViewCell findParentCellOfView:button];
    NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
    VAUser *user = [self.vicinityUsersArray objectAtIndex:cellIndexPath.row];
    return user;
}

- (VAContact *)contactForCellWithPushedButton:(UIButton *)button {
    VAContactTableViewCell *cell = (VAContactTableViewCell *)[UITableViewCell findParentCellOfView:button];
    NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
    VAContact *contact = [self.contactsArray objectAtIndex:cellIndexPath.row];
    return contact;
}

- (void)addFriend:(VAUser *)user {
    [[VAUserApiManager new] postAddFriendWithID:user.userId andCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self.vicinityUsersArray indexOfObject:user] inSection:0];
            VAFriendsTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            [cell.addButton setImage:[UIImage imageNamed:@"add-contact-icon"] forState:UIControlStateNormal];
            user.isFriend = @(NO);
        } else {
            [self.delegate didUpdateContacts];
            [self.delegate didAddUserAsConnection:user];
        }
    }];
}

- (void)deleteFriend:(VAUser *)user {
    [[VAUserApiManager new] deleteConnectionWithID:user.userId withCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self.vicinityUsersArray indexOfObject:user] inSection:0];
            VAFriendsTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            [cell.addButton setImage:[UIImage imageNamed:@"Checkmark"] forState:UIControlStateNormal];
            user.isFriend = @(YES);
        } else {
            [self.delegate didUpdateContacts];
        }
    }];
}

- (void)sortContacts {
    ABPersonSortOrdering sortOrder = ABPersonGetSortOrdering();
    if (sortOrder == kABPersonSortByFirstName) {
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"firstName" ascending:YES selector:@selector(caseInsensitiveCompare:)];
        [self.contactsArray sortUsingDescriptors:@[sortDescriptor]];
        
    }
    else {
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"lastName" ascending:YES selector:@selector(caseInsensitiveCompare:)];
        [self.contactsArray sortUsingDescriptors:@[sortDescriptor]];
        
    }
    nil;
}

- (void)removeUsersFromContacts {
    NSArray *usersEmails = [self.vicinityUsersArray valueForKeyPath:@"@distinctUnionOfObjects.email"];
    for (NSInteger i = [self.contactsArray count] - 1; i >= 0; i--) {
        VAContact *contact = self.contactsArray[i];
        for (NSString *email in usersEmails) {
            if ([contact.emails containsObject:email]) {
                [[self mutableArrayValueForKey:@"contactsArray"] removeObject:contact];
            }
        }
    }
}

- (void)sendEmailToAddress:(NSString *)emailAddress {
    NSString *emailTitle = @"Join Vicinity!";
    
    NSString *text = @"Vicinity is a social network that allows you to interact with people around you. Download for free at: http://vcnty.co/app";
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:text isHTML:NO];
    [mc setToRecipients:@[emailAddress]];
    
    if ([MFMailComposeViewController canSendMail]) {
        // Present mail view controller on screen
        [[VAControlTool topScreenController] presentViewController:mc animated:YES completion:NULL];
    }
}

- (void)sendTextToPhoneNumber:(NSString *)phoneNumber {
    NSArray *recipents = @[phoneNumber];
    NSString *message = @"Vicinity is a social network that allows you to interact with people around you. Download for free at: http://vcnty.co/app";
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setRecipients:recipents];
    [messageController setBody:message];
    
    if ([MFMessageComposeViewController canSendText]) {
        // Present message view controller on screen
        [[VAControlTool topScreenController] presentViewController:messageController animated:YES completion:nil];
    }
}

#pragma mark - VAFacebookFriendsDelegate

- (void)userDidTapCellAtIndexPath:(NSIndexPath *)path {
    VAUser *user = [self.vicinityUsersArray objectAtIndex:path.row];
    [self showProfileForUserWithAlias:user.nickname];
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            [controller dismissViewControllerAnimated:YES completion:nil];
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            [controller dismissViewControllerAnimated:YES completion:nil];
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            [self.delegate didInviteFriends];
            [controller dismissViewControllerAnimated:YES completion:nil];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            [controller dismissViewControllerAnimated:YES completion:nil];
            break;
        default:
            break;
    }
}

#pragma mark - MFMessageComposeViewControllerDelegate

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    switch (result) {
        case MessageComposeResultCancelled:
            [controller dismissViewControllerAnimated:YES completion:nil];
            break;
        case MessageComposeResultSent:
            [self.delegate didInviteFriends];
            [controller dismissViewControllerAnimated:YES completion:nil];
            break;
        case MessageComposeResultFailed:
            [controller dismissViewControllerAnimated:YES completion:nil];
            break;
        default:
            break;
    }
}

@end
