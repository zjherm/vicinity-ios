//
//  VACreateAccountViewController.m
//  vicinity-app
//
//  Created by vadzim vasileuski on 3/25/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import "VACreateAccountViewController.h"

#import "VAScrollView.h"
#import "VAControlTool.h"
#import "VAPhotoTool.h"
#import "VASignUpNewViewController.h"
#import "VAUnderlinableView.h"
#import "VANotificationMessage.h"

#import <SDWebImage/UIImageView+WebCache.h>

#import "VAUserApiManager.h"
#import "VALoginResponse.h"
#import "VAUserDefaultsHelper.h"


@interface VACreateAccountViewController () <UITextFieldDelegate, VAPhotoToolDelegate>

@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet VAScrollView *scrollView;

//avatar group
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIView *imageBoundaryView;
@property (weak, nonatomic) IBOutlet UILabel *editLabel;

//only for fontsize setup
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property (weak, nonatomic) IBOutlet UIButton *signInButton;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;
@property (weak, nonatomic) IBOutlet UIButton *hideButton;

//views to make translucent effect
@property (weak, nonatomic) IBOutlet VAUnderlinableView *loginFieldView;
@property (weak, nonatomic) IBOutlet VAUnderlinableView *passwordFieldView;
@property (weak, nonatomic) IBOutlet UIView *signUpView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

//proportional layouting
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topMarginToLogoViewConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoAndFieldsVSpaceConstraint;

//pop animation
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *barViewHeight;

//actions
- (IBAction)backBarButtonTaped:(id)sender;
- (IBAction)signInButtonTaped:(id)sender;
- (IBAction)continueButtonTaped:(id)sender;
- (IBAction)hideButtonTapped:(id)sender;

//keyboard management
@property (strong, nonatomic) UITextField *activeField;

//avatar image
@property (strong, nonatomic) UIImage *avatarImage;

//for red underlining
@property (nonatomic) BOOL isUsernameDidEndEditing;
@property (nonatomic) BOOL isPasswordDidEndEditing;

@end

@implementation VACreateAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];  
    [self setUpFontSizes];
    [self setUpVerticalSpacingConstraints];
    [self setUpViewsAlpha];
    [self setUpGestureRecognisers];
    [self disableContinueButton];
    
    [self setUpNavigationBar];
    [self setUpPlaceholders];
    if (self.pendingUser) {
        [self setupFieldsForPendingUser];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setUpKeyboardNotifications];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - setup methods
- (void)setUpViewsAlpha {
    //Need to set dynamically because Interface Builder sets alpha channel also to subviews
    self.loginFieldView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.27];
    self.passwordFieldView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.27];
    self.signUpView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.38];
}

- (void)setUpVerticalSpacingConstraints {
    CGFloat mainViewHeight = self.view.frame.size.height;
    self.topMarginToLogoViewConstraint.constant = 0.0685f * mainViewHeight;
    self.logoAndFieldsVSpaceConstraint.constant = 0.06f * mainViewHeight;
}

- (void)setUpFontSizes {
    if (self.view.frame.size.height < 660) {
        //iphone 5 and smaller
        CGFloat fontSizeDiff = 2.0f;
        if (self.view.frame.size.height < 560) {
            //iphone 4 and smaller
            fontSizeDiff = 4.0f;
        }
        
        self.continueButton.titleLabel.font = [self.continueButton.titleLabel.font fontWithSize:(self.continueButton.titleLabel.font.pointSize - fontSizeDiff)];
        self.usernameField.font = [self.usernameField.font fontWithSize:(self.usernameField.font.pointSize - fontSizeDiff)];
        self.passwordField.font = [self.passwordField.font fontWithSize:(self.passwordField.font.pointSize - fontSizeDiff)];
        self.signInButton.titleLabel.font = [self.signInButton.titleLabel.font fontWithSize:(self.signInButton.titleLabel.font.pointSize - fontSizeDiff)];
        self.bottomLabel.font = [self.bottomLabel.font fontWithSize:(self.bottomLabel.font.pointSize - fontSizeDiff)];
        self.editLabel.font = [self.editLabel.font fontWithSize:(self.editLabel.font.pointSize - fontSizeDiff)];
        self.hideButton.titleLabel.font = [self.hideButton.titleLabel.font fontWithSize:(self.hideButton.titleLabel.font.pointSize - fontSizeDiff)];
    }
}

- (void)setUpKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)setUpGestureRecognisers {
    //tap on imageView to choose avatar image
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeAvatar)];
    [self.imageView addGestureRecognizer:tapRecognizer];
    
    //tap on background to hide keyboard
    UITapGestureRecognizer *tapBackground = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.scrollView addGestureRecognizer:tapBackground];
}



- (void)setUpPlaceholders {
    [self setUpPlaceholderForTextField:self.usernameField];
    [self setUpPlaceholderForTextField:self.passwordField];

}

- (void)setUpPlaceholderForTextField:(UITextField *)textField{
    NSAttributedString *placeholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{ NSForegroundColorAttributeName : [[UIColor whiteColor] colorWithAlphaComponent:1.0f]}];
    textField.attributedPlaceholder = placeholder;
}

- (void)setUpNavigationBar {
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    //[self.navigationController.navigationBar setTitleVerticalPositionAdjustment:4.0f forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"Avenir-Book" size:21.0f]}];
}

- (void)setupFieldsForPendingUser {
    self.usernameField.text = self.pendingUser.nickname;

    if ([self.pendingUser.avatarURL length]) {
        [self.imageView sd_setImageWithURL:[NSURL URLWithString:self.pendingUser.avatarURL]
                                placeholderImage:[UIImage imageNamed:@"photo-placeholder-red"]
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                           if (image) {
                                               [self photoHasBeenEdited:image landscape:NO];
                                           }
                                       }];
    }
}

#pragma mark - actions
- (void)changeAvatar {
    [self hideKeyboard];
    [VAPhotoTool defaultPhoto].delegate = self;
    [[VAPhotoTool defaultPhoto] showPhotoActionViewOnController:self];
}

- (void)goBack {
    [self hideKeyboard];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    self.barViewHeight.constant = 44.0;
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)backBarButtonTaped:(id)sender {
    [self goBack];
}

- (IBAction)signInButtonTaped:(id)sender {
    [self goBack];
}

- (IBAction)continueButtonTaped:(id)sender {
    [self showActivityIndicator];
    [[VAUserApiManager new] checkUsername:self.usernameField.text withCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            if ([error.localizedDescription isEqual:@(100002)]) {
                [VANotificationMessage showUsernameInUseMessageOnController:self forUsername:self.usernameField.text withDuration:5.f];
                [self drawUnderlineForView:self.loginFieldView];
            } else {
                [VANotificationMessage showCreateAccountErrorMessageOnController:self withDuration:5.f];
            }
        } else {
            [self pushSignUpViewController];
        }
        [self hideActivityIndicator];
    }];
}

- (IBAction)hideButtonTapped:(id)sender {
    if (self.passwordField.isEditing) {
        [self.passwordField resignFirstResponder];
    }
    if (self.passwordField.secureTextEntry) {
        self.passwordField.secureTextEntry = NO;
        [self.hideButton setTitle:@"HIDE" forState:UIControlStateNormal];
    } else {
        self.passwordField.secureTextEntry = YES;
        [self.hideButton setTitle:@"SHOW" forState:UIControlStateNormal];
    }
}

- (void)pushSignUpViewController {
    [self performSegueWithIdentifier:@"CreateAccountToSignUpSegue"
                              sender:self];
}

#pragma mark - keyboard management
- (void)hideKeyboard {
    [self.activeField resignFirstResponder];
}

- (void)willKeyboardShow:(NSNotification*)notification {
    NSDictionary* info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    CGPoint originInSuperview = [self.scrollView convertPoint:CGPointZero fromView:self.continueButton];
    CGFloat yOffset = (originInSuperview.y + self.continueButton.frame.size.height + 15) - (self.scrollView.frame.size.height - kbSize.height);
    if (yOffset > 0) {
        self.scrollView.contentOffset = CGPointMake(0, yOffset);
    }
}

- (void)willKeyboardHide:(NSNotification*)notification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    self.scrollView.contentOffset = CGPointZero;
}



#pragma mark - UITextFieldDelegate methods
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (!self.avatarImage) {
        self.imageView.image = [UIImage imageNamed:@"photo-placeholder-red"];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(UITextFieldTextDidChange:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:textField];
    
    self.activeField = textField;
    
    
    if ([textField isEqual:self.passwordField]) {
        if (![[VAControlTool defaultControl] validPasswordInTextField:self.passwordField] && self.passwordFieldView.isUnderlinded) {
            [VANotificationMessage showIncorrectPasswordMessageOnController:self withDuration:5.f];
        }
    } else if ([textField isEqual:self.usernameField] && self.loginFieldView.isUnderlinded) {
        if (self.usernameField.text.length < 3) {
            [VANotificationMessage showFillInAliasMessageOnController:self withDuration:5.f];
        }
        if ([self.usernameField.text hasSuffix:@"."]) {
            [VANotificationMessage showUsernameCannotEndWithPeriodMessageOnController:self withDuration:5.f];
        }
    }
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UITextFieldTextDidChangeNotification
                                                  object:textField];
    if ([textField isEqual:self.usernameField] && (textField.text.length > 0)) {
        self.isUsernameDidEndEditing = YES;
    }
    if ([textField isEqual:self.passwordField] && (textField.text.length > 0)) {
        self.isPasswordDidEndEditing = YES;
    }
    [self validateAllFields];
    
    if ([self allFieldsAreValid]) {
        [self enableContinueButton];
    } else {
        [self disableContinueButton];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField isEqual:self.usernameField]) {        
        BOOL change = [[VAControlTool defaultControl] setAliasMaskForTextField:textField InRange:range replacementString:string];
        return change ;
    } else if ([textField isEqual:self.passwordField]) {
        BOOL sould = [[VAControlTool defaultControl] setPasswordMaskForTextField:textField InRange:range replacementString:string];
        return sould;
    } else {
        return YES;
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.usernameField]) {
        [self.passwordField becomeFirstResponder];
    } else if ([textField isEqual:self.passwordField]) {
        [self hideKeyboard];
        if (self.continueButton.enabled) {
            [self pushSignUpViewController];
        }
    }
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    [self disableContinueButton];
    return YES;
}

- (void) UITextFieldTextDidChange:(NSNotification*)notification
{
    [self validateAllFields];
    
    if ([self allFieldsAreValid]) {
        [self enableContinueButton];
    } else {
        [self disableContinueButton];
    }
}

#pragma mark - text field validation
- (void)drawUnderlineForView:(VAUnderlinableView *)view {
    [view drawUnderlineWithColor:[UIColor colorWithRed:208.0f/255 green:2.0f/255 blue:27.0f/255 alpha:1.0f] width:1.5];
}

- (void)disableContinueButton {
    self.continueButton.enabled = NO;
    self.continueButton.backgroundColor = [self.continueButton.backgroundColor colorWithAlphaComponent:0.5];
}

- (void)enableContinueButton {
    self.continueButton.enabled = YES;
    self.continueButton.backgroundColor = [self.continueButton.backgroundColor colorWithAlphaComponent:1.0];
}

- (BOOL)allFieldsAreValid {
    if ( (self.usernameField.text.length < 3) || [self.usernameField.text hasSuffix:@"."]) {
        return NO;
    } else if (![[VAControlTool defaultControl] validPasswordInTextField:self.passwordField]){
        return NO;
    } else if (!self.avatarImage) {
        return NO;
    }
    else {
        return YES;
    }
}

- (void)validateAllFields {
    if ([self.activeField isEqual:self.usernameField]) {
        if ( (self.usernameField.text.length < 3) || [self.usernameField.text hasSuffix:@"."]) {
            if (self.isUsernameDidEndEditing) {
                [self drawUnderlineForView:self.loginFieldView];
            }
        } else {
            [self.loginFieldView removeUnderline];
        }
    }
    

    if ([self.activeField isEqual:self.passwordField]) {
        if (![[VAControlTool defaultControl] validPasswordInTextField:self.passwordField]){
            if (self.isPasswordDidEndEditing) {
                [self drawUnderlineForView:self.passwordFieldView];
            }
        } else {
            [self.passwordFieldView removeUnderline];
        }
    }
}


#pragma mark - VAPhotoToolDelegate
- (void)photoHasBeenEdited:(UIImage *)photo landscape:(BOOL)isLandscape {
    self.imageView.image = photo;
    self.avatarImage = photo;
    self.editLabel.hidden = NO;
    self.imageBoundaryView.layer.cornerRadius = self.imageView.frame.size.width / 2;
    self.imageBoundaryView.clipsToBounds = YES;
    self.imageBoundaryView.layer.borderWidth = 2.0f;
    self.imageBoundaryView.layer.borderColor = [UIColor whiteColor].CGColor;
    
    if ([self allFieldsAreValid]) {
        [self enableContinueButton];
    } else {
        [self disableContinueButton];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"CreateAccountToSignUpSegue"]) {
        VASignUpNewViewController *signUpViewController = segue.destinationViewController;
        signUpViewController.username = self.usernameField.text;
        signUpViewController.password = self.passwordField.text;
        signUpViewController.avatarImage = self.avatarImage;
        signUpViewController.pendingUser = self.pendingUser;
    }
    [self hideKeyboard];
}

- (void)showActivityIndicator {
    self.view.userInteractionEnabled = NO;
    [self.continueButton setTitle:@"" forState:UIControlStateNormal];
    [self.activityIndicator startAnimating];
}

- (void)hideActivityIndicator {
    [self.activityIndicator stopAnimating];
    [self.continueButton setTitle:@"CONTINUE" forState:UIControlStateNormal];
    self.view.userInteractionEnabled = YES;
}

@end
