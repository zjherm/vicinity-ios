//
//  VAVoucherView.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 01.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAVoucherView.h"

@interface VAVoucherView ()
@property (strong, nonatomic) CAShapeLayer *border;
@end

@implementation VAVoucherView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _border = [CAShapeLayer layer];
    _border.strokeColor = [[VADesignTool defaultDesign] vicinityBlue].CGColor;
    _border.fillColor = nil;
    _border.lineWidth = 3.f;
    _border.lineDashPattern = @[@7, @3];
    [self.layer addSublayer:_border];
//    self.layer.cornerRadius = 5.f;
//    self.clipsToBounds = YES;
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    _border.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(CGRectGetMinX(self.bounds) + 3.f, CGRectGetMinY(self.bounds), CGRectGetWidth(self.bounds) - 6.f, CGRectGetHeight(self.bounds) - 3.f) cornerRadius:5.f].CGPath;
    _border.frame = self.bounds;
}

@end
