//
//  VADealTableViewCell.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 01.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VADealTableViewCell.h"
#import "UILabel+VACustomFont.h"
#import "UIButton+VACustomFont.h"
#import "VADeal.h"
#import "VABusiness.h"
#import "VALocation.h"
#import "VAComment.h"
#import "VAUser.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "VALiveDealsViewController.h"
#import "VANotificationMessage.h"
#import "NSDate+VAString.h"
#import "NSNumber+VAMilesDistance.h"
#import "VABusinessProfileViewController.h"
#import "VAControlTool.h"
#import <OpenInGoogleMapsController.h>
#import "VAProfileViewController.h"
#import "VAEventsViewController.h"

@interface VADealTableViewCell () <TTTAttributedLabelDelegate>
@property (strong, nonatomic) VADeal *deal;
@property (strong, nonatomic) VABusiness *business;
@end

CGFloat dealCellWidth;

@implementation VADealTableViewCell

- (void)awakeFromNib {
    self.imageContainerHeight.constant = 0.f;
    self.textContainerHeight.constant = 0.f;
    self.bottomCommentConstraint.constant = 0.f;
    self.getDealHeightConstraint.constant = 0.f;
    self.confirmationViewHeight.constant = 0.f;
    self.descriptionContainerHeightConstraint.constant = 0.f;
    self.commentBlockHeightConstraint.constant = 0.f;
    self.commentBlockHeightConstraint.priority = 250.f;
    
    UITapGestureRecognizer* tapAvatar = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showProfileForBusiness)];
    [tapAvatar setNumberOfTapsRequired:1];
    [self.avatarView addGestureRecognizer:tapAvatar];
    
    UITapGestureRecognizer* tapNameLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showProfileForBusiness)];
    [self.nameLabel addGestureRecognizer:tapNameLabel];
    self.nameLabel.userInteractionEnabled = 1;
    
    UITapGestureRecognizer* tapLocationLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showCurrentLocation)];
    [self.locationLabel addGestureRecognizer:tapLocationLabel];
    self.locationLabel.userInteractionEnabled = YES;
    
    self.getDealButton.hidden = YES;
    self.confirmationView.hidden = YES;
    
    self.cellBlock.layer.cornerRadius = 5.f;
    self.cellBlock.clipsToBounds = YES;
    
    self.avatarView.layer.cornerRadius = 24.5f;
    self.avatarView.clipsToBounds = YES;
    self.avatarView.userInteractionEnabled = YES;
    
    self.editCardButton.layer.cornerRadius = 3.f;
    self.editCardButton.clipsToBounds = YES;
    
    [self.commentButtonLabel setLetterSpacingWithMultiplier:1.1f];
    [self.likeButtonLabel setLetterSpacingWithMultiplier:1.2f];
    [self.getDealButton setLetterSpacingWithMultiplier:1.2f];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Methods

- (void)showProfileForBusiness {
    [self showProfileForBusinessWithID:self.business.businessID];
}

- (void)configureCellWithDeal:(VADeal *)deal {
    VABusiness *business = deal.business;
    
    self.business = business;
    self.deal = deal;
    
    [self setAvatarForBusiness:business];
    
    [self setNameForBusiness:business];
    
    [self setDealTextForDeal:deal];
    
    [self setDistanceForDeal:deal];
    
    [self setDateTextForDeal:deal];
    
    [self setLocationTextForDeal:deal];
    
    [self setImageForDeal:deal];
    
    [self setLikesCountForDeal:deal];
    
    [self setCommentsButtonForDeal:deal];
    
    [self setCommentsForDeal:deal];
    
    [self setLikeButtonForDeal:deal];
    
    [self setGetDealButtonForDeal:deal];
    
    [self setConfirmationViewForDeal:deal];

}

- (void)setAvatarForBusiness:(VABusiness *)business {
    if (business.avatarURL) {
        NSURL *imageURL = [NSURL URLWithString:business.avatarURL];
        
        [self.avatarView sd_setImageWithURL:imageURL
                           placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                      if (image) {
                                          self.avatarView.image = image;
                                      }
                                      if (error) {
                                          VALiveDealsViewController *currentVC = (VALiveDealsViewController *)[VAControlTool topScreenController];
                                          [VANotificationMessage showAvatarErrorMessageOnController:currentVC withDuration:5.f];
                                      }
                                  }];
    } else {
        self.avatarView.image = [UIImage imageNamed:@"placeholder.png"];
    }
}

- (void)setNameForBusiness:(VABusiness *)business {
    self.nameLabel.text = business.name;
}

- (void)setDealTextForDeal:(VADeal *)deal {
    
    self.dealLabel.attributedText = [self attributedStringForDeal:deal];
    
    self.dealLabel.delegate = self;
    
    if (deal.text) {
        self.textContainerHeight.priority = 250.f;
    } else {
        self.textContainerHeight.priority = 900.f;
    }
    
    [self.dealLabel addPostAtrributes];
}

- (void)setDateTextForDeal:(VADeal *)deal {
    self.dateLabel.text = [deal.date messageTimeForDate];
}

- (void)setDistanceForDeal:(VADeal *)deal {
    if (deal.distance) {
        self.distanceLabel.text = [deal.distance milesStringFromNSNumberMeters:NO];
    } else {
        self.distanceLabel.text = @"< 0.5 mi";
    }
}

- (void)setLocationTextForDeal:(VADeal *)deal {
    self.locationLabel.text = deal.business.regionName;
}

- (void)setImageForDeal:(VADeal *)deal {
    if (deal.photoURL) {
        NSURL *imageURL = [NSURL URLWithString:deal.photoURL];
        
        [self.dealImageView sd_setImageWithURL:imageURL
                              placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                         if (image) {
                                             self.dealImageView.image = image;
                                         }
                                         if (error) {
                                             VALiveDealsViewController *currentVC = (VALiveDealsViewController *)[VAControlTool topScreenController];
                                             [VANotificationMessage showAvatarErrorMessageOnController:currentVC withDuration:5.f];
                                         }
                                     }];
        
        self.imageContainerHeight.priority = 250.f;
    } else {
        self.dealImageView.image = nil;
        self.imageContainerHeight.priority = 900.f;
    }
}

- (void)setLikesCountForDeal:(VADeal *)deal {
    self.likeLabel.text = [NSString stringWithFormat:@"%ld", (long)[deal.likesAmount integerValue]];
    if ([deal.likesAmount integerValue] == 0) {
        self.likesAndCommentsContainerHeight.constant = 0.f;
        [self.likeView setHidden:YES];
    } else {
        self.likesAndCommentsContainerHeight.constant = 24.f;
        [self.likeView setHidden:NO];
    }
}

- (void)setCommentsButtonForDeal:(VADeal *)deal {
    if ([deal.commentsAmount integerValue] > 2) {
        [self.viewAllCommentsButton setHidden:NO];
        [self.viewAllCommentsButton setTitle:[NSString stringWithFormat:@"View all %ld comments", (unsigned long)[deal.commentsAmount integerValue]] forState:UIControlStateNormal];
        self.viewAllCommentsButton.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
        self.likesAndCommentsContainerHeight.constant = 24.f;
    } else {
        [self.viewAllCommentsButton setHidden:YES];
    }
}

- (void)setCommentsForDeal:(VADeal *)deal {
    self.firstCommentLabel.text = @"";
    self.secondCommentLabel.text = @"";
    
    self.firstCommentLabel.delegate = self;
    self.secondCommentLabel.delegate = self;
    
    if ([deal.commentsArray count] > 0) {
        for (int i = 0; i < [deal.commentsArray count]; i++) {
            VAComment *comment = [deal.commentsArray objectAtIndex:i];
            
            if (i == 0) {
                self.firstCommentLabel.attributedText = [self attributedStringForComment:comment];
                [self.firstCommentLabel addCommentAtrributes];
            } else if (i == 1) {
                self.secondCommentLabel.attributedText = [self attributedStringForComment:comment];
                [self.secondCommentLabel addCommentAtrributes];
            }
        }
        self.bottomCommentConstraint.constant = 7.f;
        self.bottomCommentConstraint.priority = 899;
        
    } else {
        self.bottomCommentConstraint.constant = 0.f;
        self.bottomCommentConstraint.priority = 900;
    }
}

- (void)setLikeButtonForDeal:(VADeal *)deal {
    if ([deal.isLiked boolValue] == YES) {
        self.likeImageView.image = [UIImage imageNamed:@"isLiked-red.png"];
    } else {
        self.likeImageView.image = [UIImage imageNamed:@"isUnliked.png"];
    }
}

- (void)setGetDealButtonForDeal:(VADeal *)deal {
    if (deal.dealType != VADealTypeAd) {
        self.getDealHeightConstraint.constant = 30.f;
        self.getDealButton.hidden = NO;
    } else {
        self.getDealHeightConstraint.constant = 0.f;
        self.getDealButton.hidden = YES;
    }
    
    if (deal.dealType == VADealTypeFree && deal.isPurchased.boolValue == NO) {
        [self.getDealButton setTitle:@"GET DEAL" forState:UIControlStateNormal];
    } else if (deal.dealType == VADealTypeFree && deal.isPurchased.boolValue == YES) {
        [self.getDealButton setTitle:@"DEAL SAVED - VIEW" forState:UIControlStateNormal];
    } else if (deal.dealType == VADealTypePaid && deal.isPurchased.boolValue == NO) {
        [self.getDealButton setTitle:@"BUY DEAL" forState:UIControlStateNormal];
    } else if (deal.dealType == VADealTypePaid && deal.isPurchased.boolValue == YES) {
        [self.getDealButton setTitle:@"PURCHASED - VIEW DEAL" forState:UIControlStateNormal];
    }
    
    self.getDealButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
}

- (void)setConfirmationViewForDeal:(VADeal *)deal {
    VALiveDealsViewController *vc = (VALiveDealsViewController *)[VAControlTool topScreenController];

    if (deal.isInConfirmPurchaseMode) {
        [self.confirmationView setHidden:NO];
        [vc.view addConstraint:self.bottomConfirmationViewConstraint];
    } else {
        [self.confirmationView setHidden:YES];
        [vc.view removeConstraint:self.bottomConfirmationViewConstraint];
    }
    
    if (deal.shortDescription && ![deal.shortDescription isEqualToString:@""]) {
        self.descriptionContainerHeightConstraint.constant = [self heightForDescriptionContainerForDeal:deal];
    } else {
        self.descriptionContainerHeightConstraint.constant = 0.f;
    }
}

#pragma mark - Attributes

- (NSMutableAttributedString *)attributedTextForUserName:(NSString *)name andLocation:(NSString *)location {
    NSRange nameBoldedRange = NSMakeRange(0, name.length);
    NSMutableAttributedString *checkInAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ checked in at %@", name, location]
                                                                                          attributes:@{
                                                                                                       NSFontAttributeName : [[VADesignTool defaultDesign] vicinityRegularFontOfSize:13.f],
                                                                                                       NSForegroundColorAttributeName: [UIColor blackColor]
                                                                                                       }];
    [checkInAttrString addAttributes:@{
                                       NSFontAttributeName: [[VADesignTool defaultDesign] vicinityBoldFontOfSize:15.f],
                                       NSForegroundColorAttributeName: [[VADesignTool defaultDesign] vicinityDarkBlue]
                                       }
                               range:nameBoldedRange];
    
    NSInteger preCheckInLength = [[NSString stringWithFormat:@"%@ checked in at ", name] length];
    NSRange checkInRange = NSMakeRange(preCheckInLength, location.length);
    
    [checkInAttrString addAttributes:@{
                                       NSFontAttributeName: [[VADesignTool defaultDesign] vicinityRegularFontOfSize:13.f],
                                       NSForegroundColorAttributeName: [[VADesignTool defaultDesign] vicinityDarkBlue]
                                       }
                               range:checkInRange];
    return checkInAttrString;
}

- (NSMutableAttributedString *)attributedStringForComment:(VAComment *)comment {
    NSString *commentText = comment.text;
    NSString *userNickname = comment.user.nickname;
    NSString *fullComment = [NSString stringWithFormat:@"%@ %@",userNickname, commentText];
    NSMutableAttributedString *commentAttrString = [[NSMutableAttributedString alloc] initWithString:fullComment attributes:@{NSFontAttributeName : [[VADesignTool defaultDesign] vicinityRegularFontOfSize:13.5f]}];
    
    NSRange nameBoldedRange = NSMakeRange(0, userNickname.length);
    
    
    [commentAttrString addAttributes:@{
                                       NSFontAttributeName: [[VADesignTool defaultDesign] vicinityBoldFontOfSize:13.5f],
                                       NSForegroundColorAttributeName: [[VADesignTool defaultDesign] vicinityDarkBlue]
                                       }
                               range:nameBoldedRange];
    
    return commentAttrString;
}

- (NSMutableAttributedString *)attributedStringForDeal:(VADeal *)deal {
    NSMutableAttributedString *dealString = nil;
    
    if (deal.text) {
        dealString = [[NSMutableAttributedString alloc] initWithString:deal.text
                                                            attributes:@{
                                                                         NSFontAttributeName : [[VADesignTool defaultDesign] vicinityRegularFontOfSize:15.f],
                                                                         NSForegroundColorAttributeName: [UIColor blackColor]
                                                                         }];
    }
    
    return dealString;
    
}

#pragma mark - Methods

- (void)showProfileForBusinessWithID:(NSString *)businessID {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VABusinessProfileViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"VABusinessProfileViewController"];
    vc.isBusinessSearchModeMode = YES;
    vc.businessID = businessID;
    VALiveDealsViewController *currentVC = (VALiveDealsViewController *)[VAControlTool topScreenController];
    [currentVC.navigationController pushViewController:vc animated:YES];
}

- (void)showProfileForUserWithAlias:(NSString *)alias {
    VAProfileViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"profileViewController"];
    vc.searchAlias = alias;
    vc.isUsersAccountMode = YES;
    VALiveDealsViewController *currentVC = (VALiveDealsViewController *)[VAControlTool topScreenController];
    [currentVC.navigationController pushViewController:vc animated:YES];
}

- (void)showCurrentLocation {
    [self showLocationForDeal:self.deal];
}

- (void)showLocationForDeal:(VADeal *)deal {
    GoogleDirectionsDefinition *definition = [[GoogleDirectionsDefinition alloc] init];
    double lat = [deal.coordinates.latitude doubleValue];
    double lon = [deal.coordinates.longitude doubleValue];
    definition.destinationPoint = [GoogleDirectionsWaypoint waypointWithLocation:CLLocationCoordinate2DMake(lat, lon)];
    [[OpenInGoogleMapsController sharedInstance] openDirections:definition];
}

- (void)showProfileForFirstCommentAuthor {
    VAComment *firstComment = [self.deal.commentsArray firstObject];
    [self showProfileForUserWithAlias:firstComment.user.nickname];
}

- (void)showProfileForSecondCommentAuthor {
    VAComment *secondComment = [self.deal.commentsArray lastObject];
    [self showProfileForUserWithAlias:secondComment.user.nickname];}

#pragma mark - TTTAttributedLabelDelegate

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithTransitInformation:(NSDictionary *)components {
    [self performActionToWordType:components[kType] wordString:components[kWord]];
}

#pragma mark - TTTAttributedLabel Actions

- (void)performActionToWordType:(NSString *)wordType wordString:(NSString *)string {
    if ([wordType isEqualToString:kHashtag]) {
        NSString *hashtag = [string substringFromIndex:1];              //string here shows #hashtag with #
        
        VAEventsViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"VAEventsViewController"];
        vc.isHashTagMode = YES;
        vc.hashtagString = hashtag;
        VALiveDealsViewController *currentVC = (VALiveDealsViewController *)[VAControlTool topScreenController];
        [currentVC.navigationController pushViewController:vc animated:YES];
    } else if ([wordType isEqualToString:kMention]) {
        NSString *nickname = [string substringFromIndex:1];
        [self showProfileForUserWithAlias:nickname];
    } else if ([wordType isEqualToString:kAlias]) {
        [self showProfileForUserWithAlias:string];
    } else if ([wordType isEqualToString:kCommentWord]) {
        
    }
}

#pragma mark - Height methods

- (CGFloat)heightForCellWithDeal:(VADeal *)deal {
    dealCellWidth = CGRectGetWidth([[UIScreen mainScreen] bounds]) - 2 * 16.f;
    
    CGFloat baseHeight = 111.f;
    CGFloat baseConfirmationHeight = 63.f;
    CGFloat postContainerHeight = [self heightForPostContainerWithDeal:deal];
    CGFloat imageContainerHeight = [self heightForImageContainerWithURLString:deal.photoURL];
    
    CGFloat commentsContainerHeight = [self heightForCommentsContainerWithComments:deal.commentsArray];
    CGFloat likeViewHeight = [self heightForLikeViewForDeal:deal];
    CGFloat getDealButtonHeight = [self heightForGetDealButtonForDeal:deal];
    
    CGFloat confirmationViewHeight = [self heightForConfirmationViewForDeal:deal];
    
    if (deal.isInConfirmPurchaseMode == NO) {
        return baseHeight + postContainerHeight + imageContainerHeight + commentsContainerHeight + likeViewHeight + getDealButtonHeight;
    } else {
        return baseConfirmationHeight + postContainerHeight + imageContainerHeight + confirmationViewHeight;
    }
}

- (CGFloat)heightForPostContainerWithDeal:(VADeal *)deal {
    CGFloat postLabelLeadingConstraint = 79.f;
    CGFloat postLabelTrailingConstraint = 14.f;
    CGFloat bottomPostLabelContraint = 10.f;
    
    if (!deal.text) {
        return 0;
    } else {
        self.dealLabel.text = deal.text;
        return [self.dealLabel heightForPostLabelWithWidth:dealCellWidth - postLabelLeadingConstraint - postLabelTrailingConstraint] + bottomPostLabelContraint;
    }
}

- (CGFloat)heightForImageContainerWithURLString:(NSString *)urlString {
    if (!urlString) {
        return 0;
    } else {
        return dealCellWidth;        //imageView is always square, so height equals cell's width
    };
}

- (CGFloat)heightForCommentsContainerWithComments:(NSArray *)commentsArray {
    CGFloat commentsVerticalConstraint = 1.f;               //between first and second comment
    CGFloat bottomCommentsContraint = 7.f;
    
    CGFloat commentLabelLeadingConstraint = 18.f;
    CGFloat commentLabelTrailngConstraint = 11.f;
    
    VAComment *firstComment = [commentsArray count] > 0 ? [commentsArray objectAtIndex:0] : nil;
    VAComment *secondComment = [commentsArray count] > 1 ? [commentsArray objectAtIndex:1] : nil;
    
    self.firstCommentLabel.text = firstComment.text;
    self.secondCommentLabel.text = secondComment.text;
    
    CGFloat firstCommentHeight = firstComment ? [self.firstCommentLabel heightForCommentLabelWithWidth:dealCellWidth - commentLabelTrailngConstraint - commentLabelLeadingConstraint andComment:firstComment] : 0.f;
    CGFloat secondCommentHeight = secondComment ? [self.secondCommentLabel heightForCommentLabelWithWidth:dealCellWidth - commentLabelTrailngConstraint - commentLabelLeadingConstraint andComment:secondComment] : 0.f;
    
    if ([commentsArray count] == 0) {
        return commentsVerticalConstraint;
    } else {
        return  firstCommentHeight + commentsVerticalConstraint + secondCommentHeight + bottomCommentsContraint;
    }
}

- (CGFloat)heightForLikeViewForDeal:(VADeal *)deal {
    if ([deal.likesAmount integerValue] == 0 && [deal.commentsAmount integerValue] < 3) {
        return 0.f;
    } else {
        return 24.f;
    }
}

- (CGFloat)heightForGetDealButtonForDeal:(VADeal *)deal {
    if (deal.dealType != VADealTypeAd) {
        return 30.f;
    } else {
        return 0.f;
    }
}

- (CGFloat)heightForConfirmationViewForDeal:(VADeal *)deal {
    CGFloat baseHeight = 171.f;
    
    CGFloat descriptionContainerHeight = [self heightForDescriptionContainerForDeal:deal];
    
    return baseHeight + descriptionContainerHeight;
}

- (CGFloat)heightForDescriptionContainerForDeal:(VADeal *)deal {
    CGFloat descriptionLabelIndent = 20.f;
    CGFloat descriptionLabelBottomConstraint = 25.f;

    if (deal.shortDescription) {
        self.shortDealDescriptionLabel.text = deal.shortDescription;
        return [self.shortDealDescriptionLabel heightForPostLabelWithWidth:dealCellWidth - 2 * descriptionLabelIndent] + descriptionLabelBottomConstraint;
    } else {
        return 0;
    }
}

@end
