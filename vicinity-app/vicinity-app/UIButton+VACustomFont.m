//
//  UIButton+VACustomFont.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 10.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "UIButton+VACustomFont.h"

@implementation UIButton (BTCustomFont)

- (NSString *)fontName {
    return self.titleLabel.font.fontName;
}

- (void)setFontName:(NSString *)fontName {
    self.titleLabel.font = [UIFont fontWithName:fontName size:self.titleLabel.font.pointSize];
}

- (void)setLetterSpacingWithMultiplier:(CGFloat)multiplier {
    [self setLetterSpacingWithMultiplier:multiplier forTitle:self.titleLabel.text];
}

- (void)setLetterSpacingWithMultiplier:(CGFloat)multiplier forTitle:(NSString *)title {
    NSMutableAttributedString* attrStr = [[NSMutableAttributedString alloc] initWithString:title];
    [attrStr addAttribute:NSKernAttributeName value:@(multiplier) range:NSMakeRange(0, attrStr.length)];
    self.titleLabel.attributedText = attrStr;
    [self.titleLabel sizeToFit];
}

- (void)setUnderlinedText:(NSString *)text withColor:(UIColor *)color {
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:text];
    [attributeString addAttribute:NSUnderlineStyleAttributeName
                            value:[NSNumber numberWithInt:1]
                            range:(NSRange){0,[attributeString length]}];
    
    [attributeString addAttributes:@{
                                     NSForegroundColorAttributeName: color
                                     }
                             range:(NSRange){0,[attributeString length]}];
    
    [self setAttributedTitle:attributeString forState:UIControlStateNormal];
}

@end
