//
//  VAMenuTableViewCell.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 30.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAMenuTableViewCell.h"
#import "VAUser.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "VANotificationMessage.h"
#import "VAControlTool.h"

@implementation VAMenuTableViewCell

- (void)awakeFromNib {
    self.countView.layer.cornerRadius = 10.f;
    self.countView.clipsToBounds = YES;
    
    self.avatarImageView.layer.cornerRadius = 25.f;
    self.avatarImageView.clipsToBounds = YES;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellSelected:(BOOL)selected {
    self.iconContainer.backgroundColor = selected ? [[[VADesignTool defaultDesign] vicinityBlue] colorWithAlphaComponent:0.5f] : [UIColor clearColor];
    self.rightArrowImageView.hidden = !selected;
}

- (void)setNotificationsCount:(NSInteger)count {
    if (count > 0) {
        [self.countView setHidden:NO];
        [self.countLabel setText:[NSString stringWithFormat:@"%ld", (long)count]];
    } else {
        [self.countView setHidden:YES];
    }
}

- (void)loadAvatarForUser:(VAUser *)user {
    if (user.avatarURL) {
        
        NSURL *imageURL = [NSURL URLWithString:user.avatarURL];
        
        __weak VAMenuTableViewCell *weakSelf = self;
        [self.avatarImageView sd_setImageWithURL:imageURL
                                placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                           if (image) {
                                               weakSelf.avatarImageView.image = image;
                                           }
                                           if (error) {
                                               [VANotificationMessage showAvatarErrorMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
                                           }
                                       }];
        
    } else {
        self.avatarImageView.image = [UIImage imageNamed:@"avatar-placeholder.png"];
    }
}


@end
