//
//  VAFilterViewController.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 25.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VAFilterDelegate;

@interface VAFilterViewController : UIViewController
@property (weak, nonatomic) id <VAFilterDelegate> delegate;

@property (assign, nonatomic) BOOL hideCategories;                         //default is NO (if YES only radius view will be shown)
@end

@protocol VAFilterDelegate <NSObject>
- (void)userDidChangeFilters;
@end
