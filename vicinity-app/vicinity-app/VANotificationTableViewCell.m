//
//  VANotificationTableViewCell.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 17.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VANotificationTableViewCell.h"
#import "VAProfileViewController.h"
#import "VANotificationsTableViewController.h"
#import "VAControlTool.h"
#import "VAUser.h"
#import "UILabel+VACustomFont.h"
#import "VAEventsViewController.h"

@implementation VANotificationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UITapGestureRecognizer* tapAvatar = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showProfileForUser)];
    [self.avatarView addGestureRecognizer:tapAvatar];
    self.avatarView.userInteractionEnabled = 1;
    
    self.generalInfoLabel.userInteractionEnabled = 1;
    
    [self.detailInfoLabel addPostAtrributes];
}

#pragma maek - Actions

- (void)showProfileForUser {
    [self showProfileForUserWithAlias:self.user.nickname];
}

- (void)showProfileForUserWithAlias:(NSString *)alias {
    VAProfileViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"profileViewController"];
    vc.searchAlias = alias;
    vc.isUsersAccountMode = YES;
    VANotificationsTableViewController *currentVC = (VANotificationsTableViewController *)[VAControlTool topScreenController];
    [currentVC.navigationController pushViewController:vc animated:YES];
}

#pragma mark - TTTAttributedLabelDelegate

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    if ([url.absoluteString isEqualToString:@"name"]) {
        [self showProfileForUserWithAlias:self.user.nickname];
    } else if ([url.absoluteString rangeOfString:@"http://"].location != NSNotFound) {
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithTransitInformation:(NSDictionary *)components {
    [self performActionToWordType:components[kType] wordString:components[kWord]];
}

#pragma mark - TTTAttributedLabel Actions

- (void)performActionToWordType:(NSString *)wordType wordString:(NSString *)string {
    if ([wordType isEqualToString:kHashtag]) {
        NSString *hashtag = [string substringFromIndex:1];              //string here shows #hashtag with #
        
        VAEventsViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"VAEventsViewController"];
        vc.isHashTagMode = YES;
        vc.hashtagString = hashtag;
        VANotificationsTableViewController *currentVC = (VANotificationsTableViewController *)[VAControlTool topScreenController];
        [currentVC.navigationController pushViewController:vc animated:YES];
    } else if ([wordType isEqualToString:kMention]) {
        NSString *nickname = [string substringFromIndex:1];
        [self showProfileForUserWithAlias:nickname];
    } else if ([wordType isEqualToString:kAlias]) {
        [self showProfileForUserWithAlias:string];
    }
}


@end
