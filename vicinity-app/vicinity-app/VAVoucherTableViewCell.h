//
//  VAVoucherTableViewCell.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 04.08.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VADeal;

@interface VAVoucherTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *dealImageView;
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *voucherNumberLabel;
@property (nonatomic, weak) IBOutlet UILabel *expirationLabel;

@property (strong, nonatomic) NSIndexPath *path;

- (void)configureCellWithDeal:(VADeal *)deal;
- (CGFloat)heightForCellWithDeal:(VADeal *)deal;
@end
