//
//  VAVerificationViewController.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/8/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VAVerificationDelegate;

@interface VAVerificationViewController : UIViewController
@property (weak, nonatomic) id <VAVerificationDelegate> delegate;
@end

@protocol VAVerificationDelegate <NSObject>
- (void)userDidGoBackToLoginScreenFromVerification;
@end