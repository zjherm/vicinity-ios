//
//  VAPostTableActions.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 8/28/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAPostTableActions.h"
#import "VAControlTool.h"
#import "VALikeListTableViewController.h"
#import "VACommentsViewController.h"
#import "VAProfileViewController.h"
#import "VAEventsViewController.h"
#import "VAPostTableViewCell.h"
#import "VAUserApiManager.h"
#import "VANotificationMessage.h"
#import <Google/Analytics.h>
#import "VAPost.h"
#import <LGAlertView.h>

typedef NS_ENUM(NSInteger, VALikeAction) {
    VALikeActionLike = 0,
    VALikeActionUnlike
};

#define kEditTag 998

VALikeAction currentLikeAction;
UITableView *tableViewToEdit;
VAPostTableActions *currentAction;
VAPost *postToEdit;

@interface VAPostTableActions () <UIActionSheetDelegate>

@end

@implementation VAPostTableActions

#pragma mark - Main actions

- (void)tableView:(UITableView *)tableView performLikePost:(VAPost *)post inCell:(VAPostTableViewCell *)cell {
    tableViewToEdit = tableView;
    currentAction = self;
    
    VAUserApiManager *manager = [VAUserApiManager new];
    
    if ([post.isLiked boolValue] == NO) {
        currentLikeAction = VALikeActionLike;
        post.isLiked = [NSNumber numberWithBool:YES];
        post.likesAmount = @([post.likesAmount integerValue] + 1);
        [self performLikeForCell:cell andPost:post];
        [manager postLikePostWithID:post.postID withCompletion:^(NSError *error, VAModel *model) {
            if (error) {
                [self performUnlikeForCell:cell andPost:post];
            } else if (model) {
                post.isLiked = [NSNumber numberWithBool:YES];
                post.likesAmount = [NSNumber numberWithInteger:[cell.likeLabel.text integerValue]];
            }
        }];
    } else {
        currentLikeAction = VALikeActionUnlike;
        post.isLiked = [NSNumber numberWithBool:NO];
        post.likesAmount = @([post.likesAmount integerValue] - 1);
        [self performUnlikeForCell:cell andPost:post];
        [manager deleteLikePostWithID:post.postID withCompletion:^(NSError *error, VAModel *model) {
            if (error) {
                [self performLikeForCell:cell andPost:post];
            } else if (model) {
                post.isLiked = [NSNumber numberWithBool:NO];
                post.likesAmount = [NSNumber numberWithInteger:[cell.likeLabel.text integerValue]];
            }
        }];
    }
}

- (void)tableView:(UITableView *)tableView showLikesForPost:(VAPost *)post {
    tableViewToEdit = tableView;
    currentAction = self;

    VALikeListTableViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"VALikeListTableViewController"];
    vc.postID = post.postID;
    UIViewController *topVC = [VAControlTool topScreenController];
    [topVC.navigationController pushViewController:vc animated:YES];
}

- (void)tableView:(UITableView *)tableView showCommentsForPost:(VAPost *)post {
    tableViewToEdit = tableView;
    currentAction = self;

    VACommentsViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"VACommentsViewController"];
    vc.post = post;
    UIViewController *topVC = [VAControlTool topScreenController];
    if ([topVC isKindOfClass:[VAEventsViewController class]] || [topVC isKindOfClass:[VAProfileViewController class]]) {
        vc.delegate = (VAEventsViewController *)topVC;
    }
    [topVC.navigationController pushViewController:vc animated:YES];
}

- (void)tableView:(UITableView *)tableView editPost:(VAPost *)post {
    tableViewToEdit = tableView;
    postToEdit = post;
    currentAction = self;
    [self showEditActionSheetForPost:post];
}

- (void)showEditViewForPost:(VAPost *)post inTableView:(UITableView *)tableView {
    tableViewToEdit = tableView;
    postToEdit = post;
    currentAction = self;
    [self showEditViewForPost:post];
}
- (void)showDeleteConfirmationAlertViewForPost:(VAPost *)post inTableView:(UITableView *)tableView {
    tableViewToEdit = tableView;
    postToEdit = post;
    currentAction = self;
    [self showDeleteConfirmationAlertViewForPost:post];
}

#pragma mark - Like and Unlike Methods

- (void)performLikeForCell:(VAPostTableViewCell *)cell andPost:(VAPost *)post {
    UIViewController *topVC = [VAControlTool topScreenController];

    cell.likeImageView.image = [UIImage imageNamed:@"isLiked-red.png"];
    cell.likeLabel.text = [NSString stringWithFormat:@"%ld", (long)[post.likesAmount integerValue]];
    if (currentLikeAction == VALikeActionUnlike) {
        [VANotificationMessage showLikeErrorMessageOnController:topVC withDuration:5.f];
    }
    
    cell.likesAndCommentsContainerHeight.constant = 24.f;
    [cell.likeView setHidden:NO];
    [UIView animateWithDuration:0.3f animations:^{
        [cell layoutIfNeeded];
    }];
    
    if ([post.likesAmount integerValue] == 1) {          //it's ammount after our's like, before like it was 0
        [tableViewToEdit beginUpdates];
        [tableViewToEdit endUpdates];
    }
    
    //code for google analytics
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Likes"
                                                          action:@"Perform like"
                                                           label:@"Post like"
                                                           value:@1] build]];
}

- (void)performUnlikeForCell:(VAPostTableViewCell *)cell andPost:(VAPost *)post{
    UIViewController *topVC = [VAControlTool topScreenController];

    cell.likeImageView.image = [UIImage imageNamed:@"isUnliked.png"];
    cell.likeLabel.text = [NSString stringWithFormat:@"%ld", (long)[post.likesAmount integerValue]];
    if (currentLikeAction == VALikeActionLike) {
        [VANotificationMessage showLikeErrorMessageOnController:topVC withDuration:5.f];
    }
    
    if ([post.likesAmount integerValue] == 0) {
        [cell.likeView setHidden:YES];
    }
    
    if ([post.likesAmount integerValue] == 0 && [post.commentsAmount integerValue] < 3) {
        cell.likesAndCommentsContainerHeight.constant = 0.f;
        
        [UIView animateWithDuration:0.3f animations:^{
            [cell layoutIfNeeded];
        }];
        
        [tableViewToEdit beginUpdates];
        [tableViewToEdit endUpdates];
    }
    
    //code for google analytics
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Likes"
                                                          action:@"Perform unlike"
                                                           label:@"Post unlike"
                                                           value:@1] build]];
}

#pragma mark - Post editing methods

- (void)showEditActionSheetForPost:(VAPost *)post {
    UIViewController *topVC = [VAControlTool topScreenController];
    
    if(IOS8 || IOS9) {
        UIAlertController *ac = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        if (post.text && ![post.text isEqualToString:@""]) {
            UIAlertAction *edit = [UIAlertAction actionWithTitle:@"Edit Post" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [currentAction showEditViewForPost:post];
            }];
            [ac addAction:edit];
            
        }
        UIAlertAction *delete = [UIAlertAction actionWithTitle:@"Delete Post" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
            [self showDeleteConfirmationAlertViewForPost:post];
        }];
        [ac addAction:delete];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        [ac addAction:cancel];
        [topVC presentViewController:ac animated:YES completion:nil];
    } else if (IOS7) {
        NSString *editTitle = nil;
        if (![post.text isEqualToString:@""] && post.text) {
            editTitle = @"Edit Post";
        }
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:currentAction cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Delete Post" otherButtonTitles:editTitle, nil];
        actionSheet.tag = kEditTag;
        [actionSheet showInView:topVC.view];
    }
}

- (void)showEditViewForPost:(VAPost *)post {
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 276.f, 80.f)];
    textView.backgroundColor = [UIColor clearColor];
    
    [textView setFont:[[VADesignTool defaultDesign] vicinityRegularFontOfSize:14.f]];
    
    textView.text = post.text;
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithViewStyleWithTitle:@"Edit Post"
                                                                     message:nil
                                                                        view:textView
                                                                buttonTitles:@[@"Save"]
                                                           cancelButtonTitle:nil
                                                      destructiveButtonTitle:@"Cancel"
                                                               actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                   if ([title isEqualToString:@"Save"]) {
                                                                       post.text = textView.text;
                                                                       
                                                                       [currentAction sendEditPostRequestForPost:post];
                                                                       
                                                                       [tableViewToEdit reloadData];
                                                                       
                                                                       //code for google analytics
                                                                       id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                                                                       [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Posts"
                                                                                                                             action:@"Edit post"
                                                                                                                              label:@"From Vicinity screen"
                                                                                                                              value:nil] build]];
                                                                   }
                                                               }
                                                               cancelHandler:nil
                                                          destructiveHandler:nil];
    alertView.cancelOnTouch = NO;
    alertView.destructiveButtonTitleColor = [UIColor blackColor];
    alertView.destructiveButtonTitleColorHighlighted = [UIColor blackColor];
    alertView.destructiveButtonBackgroundColorHighlighted = [UIColor clearColor];
    alertView.buttonsFont = [UIFont boldSystemFontOfSize:18.f];
    [alertView showAnimated:YES completionHandler:nil];
    [textView becomeFirstResponder];
    
}

- (void)showDeleteConfirmationAlertViewForPost:(VAPost *)post {
    UIViewController *topVC = [VAControlTool topScreenController];

    if (IOS8 || IOS9) {
        UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"Delete Post" message:[NSString stringWithFormat:NSLocalizedString(@"alert.delete.text", nil), @"post"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionDelete = [UIAlertAction actionWithTitle:NSLocalizedString(@"alert.delete.button", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [currentAction deletePost:post];
        }];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"alert.cancel.button", nil) style:UIAlertActionStyleCancel handler:nil];
        [ac addAction:actionDelete];
        [ac addAction:cancel];
        [topVC presentViewController:ac animated:YES completion:nil];
    } else if (IOS7) {
        [[[UIAlertView alloc] initWithTitle:@"Delete Post"
                                    message:[NSString stringWithFormat:NSLocalizedString(@"alert.delete.text", nil), @"post"]
                                   delegate:currentAction
                          cancelButtonTitle:NSLocalizedString(@"alert.cancel.button", nil)
                          otherButtonTitles:NSLocalizedString(@"alert.delete.button", nil), nil] show];
    }
}

- (void)deletePost:(VAPost *)post {
    UIViewController *topVC = [VAControlTool topScreenController];

    VAUserApiManager *manager = [VAUserApiManager new];
    [manager deletePostWithID:post.postID withCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            [VANotificationMessage showDeletePostErrorMessageOnController:topVC withDuration:5.f];
        } else if (model) {
            
        }
    }];
    
    [self.delegate didPerformDeleteActionForPost:post];
    
    //code for google analytics
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Posts"
                                                          action:@"Delete post"
                                                           label:@"From Vicinity screen"
                                                           value:nil] build]];
}

- (void)sendEditPostRequestForPost:(VAPost *)post {
    UIViewController *topVC = [VAControlTool topScreenController];

    VAUserApiManager *manager = [VAUserApiManager new];
    [manager putEditPostWithID:post.postID
                andNewPostText:post.text
                           lat:post.location.latitude
                           lon:post.location.longitude
                       placeID:post.checkIn.placeID
                      postToFB:[post.isPostedOnFacebook boolValue]
                     imageData:nil
             shouldUpdatePhoto:NO
                withCompletion:^(NSError *error, VAModel *model) {
                    if (error) {
                        [VANotificationMessage showEditPostErrorMessageOnController:topVC withDuration:5.f];
                    } else if (model) {
                        
                    }
                }];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [currentAction deletePost:postToEdit];
    } else if (buttonIndex == 1) {
        [currentAction showEditViewForPost:postToEdit];
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [self deletePost:postToEdit];
    }
}

@end
