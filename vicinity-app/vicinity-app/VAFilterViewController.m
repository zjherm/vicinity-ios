//
//  VAFilterViewController.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 25.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAFilterViewController.h"
#import "DEMONavigationController.h"
#import "VAFilterTableViewCell.h"
#import "VAFilterTableViewFooterView.h"
#import "VAFilterTableViewHeaderView.h"
#import "VAFiltersTool.h"
#import "VAControlTool.h"

#define checkmarkImageViewHeight 34.f

@interface VAFilterViewController () <VADistanceFilterProtocol>
@property (strong, nonatomic) NSString *currentDistanceFilter;
@property (strong, nonatomic) NSArray *currentCategoriesFilter;
@property (assign, nonatomic) BOOL breakFlag;

- (IBAction)actionCancel:(id)sender;
- (IBAction)actionApplyFilters:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@end

@implementation VAFilterViewController

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        _hideCategories = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.currentDistanceFilter = nil;
    self.breakFlag = NO;
    self.currentCategoriesFilter = [NSArray array];
    [self loadFilters];
    
    if (self.hideCategories) {
        self.titleLabel.text = @"Filter";
    } else {
        self.titleLabel.text = @"Filters";
    }
    
    if (IOS8 || IOS9) {
        UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        
        UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
        effectView.frame = self.view.frame;
        
        [self.view insertSubview:effectView atIndex:0];
    } else if (IOS7) {
        self.backgroundView.backgroundColor = [UIColor blackColor];
        self.backgroundView.alpha = 0.85f;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.hideCategories) {
        return 0;
    } else {
        return 6;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"FilterCell";
    VAFilterTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[VAFilterTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    VAFilterTableViewCell *cell = (VAFilterTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.row == 0 && !cell.hasCheckmark) {
        [self addCheckmarksToAllCellsOfTableView:tableView];
    } else if (indexPath.row == 0 && cell.hasCheckmark) {
        [self removeCheckmarksFromAllCellsOfTableView:tableView];
    } else if (cell.hasCheckmark) {
        [self removeCheckmarkFromCell:cell];
    } else {
        [self addCheckmarkToCell:cell];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    VAFilterTableViewHeaderView *headerView = [[[NSBundle mainBundle] loadNibNamed:@"VAFilterTableViewHeaderView" owner:self options:nil] firstObject];
    
    if (self.hideCategories) {
        headerView.hideCategories = YES;
    }
    
    if (SMALL_HEIGHT_DEVICE && !self.hideCategories) {           //if categories are hidden there is no sense to decrease headerView height
        headerView.topContraint.constant = 10.f;
    }
    
    headerView.delegate = self;
    if (self.currentDistanceFilter) {
        headerView.currentDistance = self.currentDistanceFilter;
        [headerView updateText];
        [headerView checkButtonsAvailability];
    }
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (SMALL_HEIGHT_DEVICE && !self.hideCategories) {
        return 165.f;
    } else {
        return 179.f;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (SMALL_HEIGHT_DEVICE) {
        return 39.f;
    } else {
        return 50.f;
    }
}

#pragma mark - Methods

- (void)configureCell:(VAFilterTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    NSArray *categories = @[@"SHOW ALL", @"FOOD & DRINK", @"THINGS TO DO", @"SERVICES", @"SHOPPING", @"HOSPITALITY"];
    cell.categoryLabel.text = [categories objectAtIndex:indexPath.row];
    cell.checkmarkImageView.layer.cornerRadius = checkmarkImageViewHeight / 2;
    cell.checkmarkImageView.clipsToBounds = YES;
    cell.checkmarkImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    cell.checkmarkImageView.layer.borderWidth = 1.f;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.hasCheckmark = NO;
    
    if (self.currentCategoriesFilter) {
        for (NSNumber *i in self.currentCategoriesFilter) {
            if ([i integerValue] == indexPath.row) {
                [self addCheckmarkToCell:cell];
            }
        }
    }
}

- (void)addCheckmarkToCell:(VAFilterTableViewCell *)cell {
    cell.checkmarkImageView.image = [UIImage imageNamed:@"Checkmark.png"];
    cell.hasCheckmark = YES;
    cell.checkmarkImageView.layer.borderWidth = 0.f;
    
    for (int i = 1; i < [self.tableView numberOfRowsInSection:0]; i++) {         //checking if all cells has checkmarks (exept "show all" cell)
        VAFilterTableViewCell *cell = (VAFilterTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        if (!cell.hasCheckmark) {
            self.breakFlag = YES;
            break;
        }
    }
    VAFilterTableViewCell *firstCell = (VAFilterTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if (!self.breakFlag && !firstCell.hasCheckmark) {              // self.breakFlag = NO means that all cells have checkmark
        [self addCheckmarkToCell:firstCell];
    }
    
    self.breakFlag = NO;
}

- (void)removeCheckmarkFromCell:(VAFilterTableViewCell *)cell {
    cell.checkmarkImageView.image = nil;
    cell.hasCheckmark = NO;
    cell.checkmarkImageView.layer.borderWidth = 1.f;
    
    VAFilterTableViewCell *showAllCell = (VAFilterTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if (showAllCell.hasCheckmark) {
        [self removeCheckmarkFromCell:showAllCell];
    }
}

- (void)addCheckmarksToAllCellsOfTableView:(UITableView *)tableView {
    for (int i = 0; i < [tableView numberOfRowsInSection:0]; i++) {
        VAFilterTableViewCell *cell = (VAFilterTableViewCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        [self addCheckmarkToCell:cell];
    }
}

- (void)removeCheckmarksFromAllCellsOfTableView:(UITableView *)tableView {
    for (int i = 0; i < [tableView numberOfRowsInSection:0]; i++) {
        VAFilterTableViewCell *cell = (VAFilterTableViewCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        [self removeCheckmarkFromCell:cell];
    }
}

- (void)hideFilterViewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)saveFilters {
    if (self.currentDistanceFilter) {
        [[VAFiltersTool currentFilters] saveDistance:self.currentDistanceFilter];
    }
    
    [[self mutableArrayValueForKey:@"currentCategoriesFilter"] removeAllObjects];
    for (int i = 0; i < [self.tableView numberOfRowsInSection:0]; i++) {
        VAFilterTableViewCell *cell = (VAFilterTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        if (cell.hasCheckmark) {
            [[self mutableArrayValueForKey:@"currentCategoriesFilter"] addObject:@(i)];
        }
    }
    if (self.currentCategoriesFilter) {
        [[VAFiltersTool currentFilters] saveCategories:self.currentCategoriesFilter];
    }
    [self.delegate userDidChangeFilters];
}

- (void)loadFilters {
    NSString *distance = [[VAFiltersTool currentFilters] loadDistance];
    if (distance) {
        self.currentDistanceFilter = distance;
    }
    
    NSArray *categories = [[VAFiltersTool currentFilters] loadCategories];
    if (categories) {
        self.currentCategoriesFilter = categories;
    }
}

#pragma mark - Filters

- (IBAction)actionCancel:(id)sender {
    [self hideFilterViewController];
}

- (IBAction)actionApplyFilters:(id)sender {
    [self saveFilters];
    [self hideFilterViewController];
}


#pragma mark - VADistanceFilterProtocol

- (void)distanceFilterChangedToDistance:(NSString *)distance {
    self.currentDistanceFilter = distance;
}

@end
