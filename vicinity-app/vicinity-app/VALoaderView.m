//
//  VALoader.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 09.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VALoaderView.h"

CGFloat const kLoaderHeight = 60.f;

@interface VALoaderView ()
@property (weak, nonatomic) IBOutlet UIImageView *loaderView;
@property (weak, nonatomic) IBOutlet UILabel *loaderLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *labelWidthConstraint;

@property (assign, nonatomic) CGAffineTransform rotation;
@property (strong, nonatomic) NSTimer *timer;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) NSDate *startDate;
@end

BOOL isRefreshing;
BOOL endRotation;
NSInteger count = 1;

@implementation VALoaderView

+ (VALoaderView *)initLoader {
    VALoaderView *loader = [[[NSBundle mainBundle] loadNibNamed:@"VALoaderView" owner:self options:nil] firstObject];
    [loader setFrame:CGRectMake(0, 0, 73.f, kLoaderHeight)];
    return loader;
}

+ (VALoaderView *)initWhiteLoader {
    VALoaderView *loader = [[[NSBundle mainBundle] loadNibNamed:@"VALoaderView" owner:self options:nil] firstObject];
    loader.loaderView.image = [UIImage imageNamed:@"Loader_white.png"];
    loader.loaderLabel.textColor = [UIColor whiteColor];
    [loader setFrame:CGRectMake(0, 0, 73.f, kLoaderHeight)];
    return loader;
}

- (void)awakeFromNib {
    self.rotation = CGAffineTransformMakeRotation(0);
    self.hidesWhenStopped = YES;
    if (!self.text) {
        self.text = @"LOADING";
    }
    
    endRotation = NO;
    isRefreshing = NO;
    self.scrollView = nil;
}

- (void)startAnimating {
    [self defaultValues];
    [self animateImage];
    [self animateLabel];
}

- (void)stopAnimating {
    [self stopLabelAnimation];
    [self stopImageAnimation];
}

- (void)startRefreshing {

    isRefreshing = YES;
    [self startAnimating];
}

- (void)stopRefreshing {
    
    isRefreshing = NO;
    [self stopLabelAnimation];
    [self stopImageAnimation];
    [UIView animateWithDuration:0.3f animations:^{
        [self.scrollView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }];
}

- (void)animateImage {
    
    if (endRotation) {
        return;
    }
    [UIView animateWithDuration:0.5f
                          delay:0.f
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.rotation = CGAffineTransformMakeRotation(M_PI);
                         self.loaderView.transform = self.rotation;
                     }
                     completion:^(BOOL finished) {
                         self.loaderView.transform = CGAffineTransformMakeRotation(M_PI);
                         [UIView animateWithDuration:0.5f
                                               delay:0.f
                                             options:UIViewAnimationOptionCurveLinear
                                          animations:^{
                                              self.rotation = CGAffineTransformMakeRotation(0);
                                              self.loaderView.transform = self.rotation;
                                          }
                                          completion:^(BOOL finished) {
                                              [self animateImage];
                                          }];
                     }];
}

- (void)animateLabel {
    if (self.timer.isValid) {
        [self.timer invalidate];
    }
    
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:0.33f
                                                        target:self
                                                      selector:@selector(updateLabel)
                                                      userInfo:nil
                                                       repeats:YES];
    self.timer = timer;
}

- (void)stopImageAnimation {
    endRotation = YES;
}

- (void)stopLabelAnimation {
    [self.timer invalidate];
}

#pragma mark - Help methods

- (void)defaultValues {
    count = 1;
    endRotation = NO;
    self.loaderLabel.text = [NSString stringWithFormat:@"%@.", self.text];
    self.rotation = CGAffineTransformMakeRotation(0);
    
    if ([self.text isEqualToString:@"LOADING"]) {
        self.labelWidthConstraint.constant = 56.f;
    } else {
        self.labelWidthConstraint.constant = 64.f;
    }
}

- (void)updateLabel {
    if(count == 1) {
        self.loaderLabel.text = [NSString stringWithFormat:@"%@.", self.text];
        count = 2;
    } else if(count == 2) {
        self.loaderLabel.text = [NSString stringWithFormat:@"%@..", self.text];
        count = 3;
    } else if(count == 3) {
        self.loaderLabel.text = [NSString stringWithFormat:@"%@...", self.text];
        count = 1;
    }
}

- (void)containingScrollViewDidEndDragging:(UIScrollView *)containingScrollView {
    self.scrollView = containingScrollView;
    
    CGFloat minOffsetToTriggerRefresh = kLoaderHeight;
    if (containingScrollView.contentOffset.y <= -minOffsetToTriggerRefresh) {
        [containingScrollView setContentInset:UIEdgeInsetsMake(-containingScrollView.contentOffset.y, 0, 0, 0)];
        [containingScrollView setContentOffset:CGPointMake(0, -kLoaderHeight) animated:YES];
        [UIView animateWithDuration:0.3f animations:^{
            [containingScrollView setContentInset:UIEdgeInsetsMake(kLoaderHeight, 0, 0, 0)];
        }];
        
        if (!isRefreshing) {
            [self sendActionsForControlEvents:UIControlEventValueChanged];
        }
    }
}

- (void)containingScrollViewDidScroll:(UIScrollView *)containingScrollView {
    CGFloat lenth = containingScrollView.contentOffset.y;
    if (lenth < 0) {
        lenth = -lenth;
        CGFloat phase = lenth / kLoaderHeight;
        if (phase > 1) {
            phase = 1;
        }
        self.alpha = phase;
    }
}

@end
