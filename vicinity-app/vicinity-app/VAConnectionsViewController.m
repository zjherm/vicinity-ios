//
//  VAConnectionsViewController.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/22/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAConnectionsViewController.h"
#import "DEMONavigationController.h"
#import "VANotificationTool.h"
#import <Google/Analytics.h>
#import "UIBarButtonItem+Badge.h"
#import "VAUserDefaultsHelper.h"
#import "VAConnectionTableViewCell.h"
#import "VAConnection.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "VANotificationMessage.h"
#import "VAControlTool.h"
#import "VAProfileViewController.h"
#import "VAUserApiManager.h"
#import "VALoaderView.h"
#import "VAConnectionsResponse.h"
#import "UITableViewCell+VAParentCell.h"

@interface VAConnectionsViewController () <VAConnectionsProfileDelegate>
@property (strong, nonatomic) UIBarButtonItem *leftItem;
@property (strong, nonatomic) NSArray *connectionsArray;
@property (strong, nonatomic) NSArray *searchFullArray;
@property (assign, nonatomic) BOOL isRefreshing;
@property (strong, nonatomic) VALoaderView *headerLoader;
@property (strong, nonatomic) VALoaderView *refresh;
@property (strong, nonatomic) VAConnection *lastConnection;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *searchField;
@property (weak, nonatomic) IBOutlet UILabel *connectionsCountLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomTableViewConstraint;
@end

@implementation VAConnectionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [(DEMONavigationController *)self.navigationController setDefaultNavigationBar];
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Menu Icon"] style:UIBarButtonItemStylePlain target:self.navigationController action:@selector(showMenu)];
    self.navigationItem.leftBarButtonItem = leftItem;
    self.leftItem = leftItem;
    
    self.connectionsArray = [NSArray array];
    self.searchFullArray = nil;
    self.isRefreshing = NO;
    self.connectionsCountLabel.text = @"No Connections";
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UITapGestureRecognizer *resignKeyboardTap = [[UITapGestureRecognizer alloc] initWithTarget:self.searchField action:@selector(resignFirstResponder)];
    [self.view addGestureRecognizer:resignKeyboardTap];
    
    [self setupRefreshControl];
    
    [self getConnections];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationController.title = @"Connections";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showButtonBadge) name:kShowBadgeForMenuIcon object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideButtonBadge) name:kHideBadgeForMenuIcon object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Connections screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.connectionsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Connections";
    VAConnectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell.delegate = self;
    
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    VAConnection *connection = nil;
    if ([self.connectionsArray count] > 0) {
        connection = self.connectionsArray[indexPath.row];
    }
    
    if (connection.isInDisconnectMode) {
        return 105.f;
    } else {
        return 60.f;
    }
}

#pragma mark - Keyboard actions

- (void)willKeyboardShow:(NSNotification*)notification {
    NSDictionary* info = [notification userInfo];
    CGRect keyRect;
    [[info objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyRect];
    CGFloat height = keyRect.size.height;
    self.bottomTableViewConstraint.constant = height;
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)willKeyboardHide:(NSNotification*)notification {
    NSDictionary* info = [notification userInfo];
    CGRect keyRect;
    [[info objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyRect];

    self.bottomTableViewConstraint.constant = 0.f;
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - Actions

- (IBAction)userDidPushEditButton:(UIButton *)sender {
    VAConnection *connection = [self connectionForCellWithPushedButton:sender];
    if (connection.isInDisconnectMode) {
        [self hideDeleteButtonToConnection:connection];
    } else {
        [self showDeleteButtonToConnection:connection];
    }
}

- (IBAction)userDidPushDisconnectButton:(UIButton *)sender {
    VAConnection *connection = [self connectionForCellWithPushedButton:sender];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self.connectionsArray indexOfObject:connection] inSection:0];
    
    [self disconnectConnection:connection];
    
    connection.isInDisconnectMode = NO;
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.31f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{     //delay is to end hide disconnect button animation
        [self.tableView beginUpdates];
        [[self mutableArrayValueForKey:@"connectionsArray"] removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        self.connectionsCountLabel.text = [NSString stringWithFormat:@"%@ Connections", [self.connectionsArray count] == 0 ? @"No" : @([self.connectionsArray count])];
        [self.tableView endUpdates];
    });
}

#pragma mark - Methods

- (void)configureCell:(VAConnectionTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    cell.indexPath = indexPath;
    
    VAConnection *connection = [self.connectionsArray objectAtIndex:indexPath.row];
    
    if (connection.avatarURL) {
        NSURL *imageURL = [NSURL URLWithString:connection.avatarURL];
        
        [cell.avatarView sd_setImageWithURL:imageURL
                           placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                      if (image) {
                                          cell.avatarView.image = image;
                                      }
                                      if (error) {
                                          [VANotificationMessage showAvatarErrorMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
                                      }
                                  }];
    } else {
        cell.avatarView.image = [UIImage imageNamed:@"placeholder.png"];
    }
    
    cell.nameLabel.text = connection.fullname;
    cell.nicknameLabel.text = connection.nickname;
    
    if (indexPath.row == [self.connectionsArray count] - 1) {
        [cell.separatorView setHidden:YES];
    } else {
        [cell.separatorView setHidden:NO];
    }
}

- (void)showProfileForUserWithAlias:(NSString *)alias {
    VAProfileViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"profileViewController"];
    vc.searchAlias = alias;
    vc.isUsersAccountMode = YES;
    UIViewController *topController = [VAControlTool topScreenController];
    [topController.navigationController pushViewController:vc animated:YES];
}

- (void)showButtonBadge {
    [self.leftItem showBadge];
}

- (void)hideButtonBadge {
    [self.leftItem hideBadge];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kShouldShowNotificationBadge];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void)getConnections {
    self.tableView.scrollEnabled = NO;
    if (!self.isRefreshing) {
        [self showLoader];
    }
    VAUserApiManager *manager = [VAUserApiManager new];
    [manager getConnectionsWithCompletion:^(NSError *error, VAModel *model) {
        self.tableView.scrollEnabled = YES;
        if (!self.isRefreshing) {
            [self.headerLoader stopAnimating];
        } else {
            [self.refresh stopRefreshing];
            self.isRefreshing = NO;
        }
        
        [self hideLoader];
        
        if (error) {
            [VANotificationMessage showLoadErrorMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
        } else if (model) {
            [[self mutableArrayValueForKey:@"connectionsArray"] removeAllObjects];
            
            VAConnectionsResponse *response = (VAConnectionsResponse *)model;
            
            NSSortDescriptor *desriptor = [[NSSortDescriptor alloc] initWithKey:@"fullname" ascending:YES];
            
            NSArray *sorted = [response.connections sortedArrayUsingDescriptors:@[desriptor]];
            
            [[self mutableArrayValueForKey:@"connectionsArray"] addObjectsFromArray:sorted];
            
            self.searchFullArray = self.connectionsArray;
            
            self.connectionsCountLabel.text = [NSString stringWithFormat:@"%@ Connections", [self.connectionsArray count] == 0 ? @"No" : @([self.connectionsArray count])];
            
            [self.tableView reloadData];
        }
    }];
}

- (void)showLoader {
    VALoaderView *loader = [VALoaderView initLoader];
    loader.frame = CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), kLoaderHeight);
    [loader startAnimating];
    self.tableView.tableHeaderView = loader;
    self.headerLoader = loader;
}

- (void)hideLoader {
    self.tableView.tableHeaderView = nil;
}

- (void)setupRefreshControl {
    VALoaderView *refresh = [VALoaderView initLoader];
    self.refresh = refresh;
    [refresh setFrame:[self frameForCustomRefreshControl]];
    refresh.alpha = 0.f;
    
    [refresh addTarget:self
                action:@selector(refreshData)
      forControlEvents:UIControlEventValueChanged];
    
    [self.tableView addSubview:refresh];
}

- (CGRect)frameForCustomRefreshControl {
    CGFloat customRefreshControlHeight = kLoaderHeight;
    CGFloat customRefreshControlWidth = CGRectGetWidth(self.tableView.bounds);
    CGRect customRefreshControlFrame = CGRectMake(0.0f,
                                                  -customRefreshControlHeight,
                                                  customRefreshControlWidth,
                                                  customRefreshControlHeight);
    return customRefreshControlFrame;
}

- (void)refreshData {
    self.isRefreshing = YES;
    [self.refresh startRefreshing];
    [self getConnections];
}

- (void)addConnections {
    for (int j = 0; j < 4; j++) {
        NSArray *names = @[@"Roman Jouravkov", @"Eugen Dedovets", @"Vadzom Marozau"];
        NSArray *nicknames = @[@"roman", @"eugen", @"vadzim"];
        for (int i = 0; i < 3; i++) {
            VAConnection *connection = [VAConnection new];
            connection.fullname = names[i];
            connection.nickname = nicknames[i];
            [[self mutableArrayValueForKey:@"connectionsArray"] addObject:connection];
        }
    }
}

- (VAConnection *)connectionForCellWithPushedButton:(UIButton *)button {
    VAConnectionTableViewCell *cell = (VAConnectionTableViewCell *)[UITableViewCell findParentCellOfView:button];
    NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
    VAConnection *connection = [self.connectionsArray objectAtIndex:cellIndexPath.row];
    return connection;
}

- (void)disconnectConnection:(VAConnection *)connection {
    [[VAUserApiManager new] deleteConnectionWithID:connection.userId withCompletion:^(NSError *error, VAModel *model) {

    }];
}

- (NSArray *)recentConnectionsArray {
    return [self.searchFullArray mutableCopy];
}

- (void)showDeleteButtonToConnection:(VAConnection *)connection {
    connection.isInDisconnectMode = YES;
    
    [self hideDeleteButtonToConnection:self.lastConnection];
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    self.lastConnection = connection;
}

- (void)hideDeleteButtonToConnection:(VAConnection *)connection {
    connection.isInDisconnectMode = NO;
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self.refresh containingScrollViewDidEndDragging:scrollView];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.refresh containingScrollViewDidScroll:scrollView];
}

#pragma mark - UITextFieldDelegate

- (IBAction)textFieldTextDidChange:(UITextField *)textField {
    NSString *filterString = textField.text;
    
    if ([filterString isEqualToString:@""]) {
        self.connectionsArray = [self recentConnectionsArray];
        [self.tableView reloadData];
        return;
    }
    
    NSMutableArray *recentArray = [NSMutableArray arrayWithArray:[self recentConnectionsArray]];
    
    for (NSInteger i = [recentArray count] - 1; i >= 0; i--) {
        VAConnection *connection = recentArray[i];
        if ([[connection.fullname lowercaseString] rangeOfString:[filterString lowercaseString]].location == NSNotFound &&
            [[connection.nickname lowercaseString] rangeOfString:[filterString lowercaseString]].location == NSNotFound) {
            [recentArray removeObjectAtIndex:i];
        }
    }
    self.connectionsArray = recentArray;
    [self.tableView reloadData];
}

#pragma mark - VAConnectionsProfileDelegate

- (void)userDidTapCellAtIndexPath:(NSIndexPath *)path {
    VAConnection *connection = [self.connectionsArray objectAtIndex:path.row];
    [self showProfileForUserWithAlias:connection.nickname];
}

@end
