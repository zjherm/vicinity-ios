//
//  VAEmptyFriendsView.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/1/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAEmptyFriendsView.h"

@interface VAEmptyFriendsView ()
@property (weak, nonatomic) IBOutlet UIButton *inviteButton;
@end

@implementation VAEmptyFriendsView

- (void)awakeFromNib {
    self.inviteButton.layer.cornerRadius = 5.f;
    self.inviteButton.clipsToBounds = YES;
}

#pragma mark - Actions

- (IBAction)inviteButtonPushed:(UIButton *)sender {
    [self.delegate userDidPushInviteFacebookFriendsButton:sender];
}

@end

