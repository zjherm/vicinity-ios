//
//  VAFacebookInviteView.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/21/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAFacebookInviteView.h"

@interface VAFacebookInviteView ()
@property (weak, nonatomic) IBOutlet UIButton *allowButton;
@end

@implementation VAFacebookInviteView

- (void)awakeFromNib {
    self.allowButton.layer.cornerRadius = 5.f;
    self.allowButton.clipsToBounds = YES;
}

#pragma mark - Actions

- (IBAction)actionAllowButtonDidPush:(UIButton *)sender {
    [self.delegate userDidPushAllowButton:sender];
}

@end
