//
//  UIButton+VACustomFont.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 10.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (BTCustomFont)
@property (nonatomic, copy) NSString* fontName;
- (void)setLetterSpacingWithMultiplier:(CGFloat)multiplier;
- (void)setLetterSpacingWithMultiplier:(CGFloat)multiplier forTitle:(NSString *)title;
- (void)setUnderlinedText:(NSString *)text withColor:(UIColor *)color;
@end
