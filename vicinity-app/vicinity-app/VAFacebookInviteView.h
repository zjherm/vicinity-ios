//
//  VAFacebookInviteView.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/21/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VAFacebookInviteDelegate;

@interface VAFacebookInviteView : UIView
@property (weak, nonatomic) id <VAFacebookInviteDelegate> delegate;
@end

@protocol VAFacebookInviteDelegate <NSObject>
- (void)userDidPushAllowButton:(UIButton *)button;
@end