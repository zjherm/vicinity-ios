//
//  VAInviteTypeView.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/21/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VAInviteDelegate;

typedef NS_ENUM(NSUInteger, VAInviteType) {
    VAInviteTypeFacebook = 0,
    VAInviteTypeContacts,
    VAInviteTypeOther
};

@interface VAInviteTypeView : UIView
@property (weak, nonatomic) id <VAInviteDelegate> delegate;
@property (assign, nonatomic) VAInviteType inviteType;
-(void)selectType:(VAInviteType)type;

// Constraints
@property (nonatomic, strong) NSArray *facebookStateViewConstraints;
@property (nonatomic, strong) NSArray *contactsStateViewConstraints;
@property (nonatomic, strong) NSArray *otherStateViewConstraints;
@end

@protocol VAInviteDelegate <NSObject>
-(void)userDidSelectInviteType:(VAInviteType)inviteType;
@end