//
//  VAConnectFacebookView.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/9/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VAFacebookFriendsViewDelegate;

@interface VAConnectFacebookView : UIView
@property (weak, nonatomic) id <VAFacebookFriendsViewDelegate> delegate;
@end

@protocol VAFacebookFriendsViewDelegate <NSObject>
- (void)userDidPushFbButton:(UIButton *)button;
@end