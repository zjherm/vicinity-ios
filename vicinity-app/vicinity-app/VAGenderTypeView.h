//
//  VAGenderTypeView.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/8/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VAGenderChangeDelegate;

typedef NS_ENUM(NSUInteger, VAGenderType) {
    VAGenderTypeMale,
    VAGenderTypeFemale,
    VAGenderTypeUnknown
};

@interface VAGenderTypeView : UIView
@property (weak, nonatomic) id <VAGenderChangeDelegate> delegate;
@property (assign, nonatomic) VAGenderType genderType;
@end

@protocol VAGenderChangeDelegate <NSObject>

- (void)userDidChangeGenderType:(VAGenderType)genderType;

@end