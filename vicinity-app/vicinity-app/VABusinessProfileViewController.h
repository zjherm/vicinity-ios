//
//  VABusinessProfileViewController.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 07.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VABusiness;

@interface VABusinessProfileViewController : UIViewController
@property (strong, nonatomic) VABusiness *business;                          //for businessSearch mode OFF
@property (strong, nonatomic) NSString *businessID;                          //for businessSearch mode ON
@property (strong, nonatomic) NSString *googlePlaceID;                       //for businessSearch mode ON
@property (assign, nonatomic) BOOL isBusinessSearchModeMode;                 //mode for displaying business profile searched by id or place id (default is NO)

@end
