//
//  VAEmptyVicinityView.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 12.08.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAEmptyVicinityView.h"

@interface VAEmptyVicinityView ()
@property (weak, nonatomic) IBOutlet UIButton *inviteButton;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UILabel *filterLabel;

- (IBAction)filterButtonPushed:(UIButton *)sender;
- (IBAction)inviteButtonPushed:(UIButton *)sender;
@end

@implementation VAEmptyVicinityView

- (void)awakeFromNib {
    self.backgroundView.layer.cornerRadius = 4.f;
    self.backgroundView.clipsToBounds = YES;
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(filterButtonPushed:)];
    [self.filterLabel addGestureRecognizer: tap];
    NSMutableAttributedString* str = [self.filterLabel.attributedText mutableCopy];
    [str addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0, str.length)];
    self.filterLabel.attributedText = str;
}

#pragma mark - Actions

- (void)filterButtonPushed:(UIButton *)sender {
    [self.delegate shouldShowFilters];
}

- (IBAction)inviteButtonPushed:(UIButton *)sender {
    [self.delegate shouldShowShareSheet];
}

@end
