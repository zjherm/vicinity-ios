//
//  VAUserTool.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 24.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAUserTool.h"

@implementation VAUserTool

+ (VAUserTool *)currentUser {
    static VAUserTool *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [VAUserTool new];
    });
    if (![[NSUserDefaults standardUserDefaults] boolForKey:kUserDataLoaded]) {
        [shared load];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kUserDataLoaded];
    }
    return shared;
}

- (void)save {
    [[NSUserDefaults standardUserDefaults] setObject:self.name forKey:kUserFullName];
    [[NSUserDefaults standardUserDefaults] setObject:self.personalToken forKey:kUserPersonalToken];
    [[NSUserDefaults standardUserDefaults] setObject:self.gender forKey:kUserGender];
    [[NSUserDefaults standardUserDefaults] setObject:self.userID forKey:kUserID];
    [[NSUserDefaults standardUserDefaults] setObject:self.imageURL forKey:kUserImageURL];
    [[NSUserDefaults standardUserDefaults] setObject:self.email forKey:kUserEmail];
    [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(self.avatarImage) forKey:kUserAvatarImage];
}

- (void)load {
    self.name = [[NSUserDefaults standardUserDefaults] objectForKey:kUserFullName];
    self.personalToken = [[NSUserDefaults standardUserDefaults] objectForKey:kUserPersonalToken];
    self.gender = [[NSUserDefaults standardUserDefaults] objectForKey:kUserGender];
    self.userID = [[NSUserDefaults standardUserDefaults] objectForKey:kUserID];
    self.imageURL = [[NSUserDefaults standardUserDefaults] objectForKey:kUserImageURL];
    self.email = [[NSUserDefaults standardUserDefaults] objectForKey:kUserEmail];
    self.avatarImage = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults] objectForKey:kUserAvatarImage]];
}

@end
