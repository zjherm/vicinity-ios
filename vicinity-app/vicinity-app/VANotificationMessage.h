//
//  VANotificationMessage.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 22.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VANotificationMessage : UIView

@property BOOL isLocationMessage;

+ (instancetype)currentMessageController;

+ (void)showShareSuccessMessageOnController:(UIViewController *)controller;

+ (void)showSharePostSuccessMessageOnController:(UIViewController *)controller;

+ (void)showSuccessMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showUpdateErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showAliasExistsErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showLoadUserErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showUserDoesntExistMessageOnController:(UIViewController *)controller forUserAlias:(NSString *)alias withDuration:(CGFloat)duration;

+ (void)showAddCommentErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showLikeErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showAvatarErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showFillInNameMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showFillInCityMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showFillInCountryMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showFillInStateMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showFillInAliasMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showFillInEmailMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showConnectionErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showCreateAccountErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showLoginCancelledMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showLoadErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showAddingPostErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showTurnOnLocationMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showTurnOnNotificationsMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showErrorFeedbackMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showSuccessFeedbackMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showCantUpdateLocationMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showDeletePostErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showEditPostErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showDeleteCommentErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showEditCommentErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showEnterUsernameMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showEnterPasswordMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showNoVerificationMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showCheckNewPasswordMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showIncorrectUsernameOrEmailMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showUsernameNotFoundMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showUsernameCannotEndWithPeriodMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration ;

+ (void)showEmailNotFoundMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showIncorrectPasswordMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showIncorrectDataMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showEmailInUseMessageOnController:(UIViewController *)controller forEmail:(NSString *)email withDuration:(CGFloat)duration;

+ (void)showUsernameInUseMessageOnController:(UIViewController *)controller forUsername:(NSString *)username withDuration:(CGFloat)duration;

+ (void)showIncorrectOldPasswordMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showSuccessUpdatePasswordMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showSetAvatarMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showInvalidPasswordMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showFacebookInUseMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showNetworkErrorMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showBlockUserSuccessMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration nickname:(NSString *)nickname;

+ (void)showUnblockUserSuccessMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration nickname:(NSString *)nickname;

+ (void)showReportSuccessMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration;

+ (void)showUserNotFoundMessageOnController:(UIViewController *)controller;
+ (void)showUserDeletedMessageOnController:(UIViewController *)controller;
+ (void)showUserBlockedMessageOnController:(UIViewController *)controller;
+ (void)showYouBlockedUserMessageOnController:(UIViewController *)controller;
+ (void)showConnectUserSuccessMessageOnController:(UIViewController *)controller withDuration:(CGFloat)duration nickname:(NSString *)nickname;

- (void)showTopNotificationMessageWithTitle:(NSString *)title onController:(UIViewController *)controller;

- (void)showNotificationMessageWithTitle:(NSString *)title backgroundColor:(UIColor *)color fontColor:(UIColor *)fontColor font:(UIFont *)font andStartYPoint:(CGFloat)y andHeight:(CGFloat)height onController:(UIViewController *)controller;

- (void)hideCurrentMessage;

- (BOOL)notificationMessageIsShown;

+ (void)showAlertWithText:(NSString *)text;

@end
