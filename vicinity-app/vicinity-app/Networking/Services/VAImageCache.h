//
//  VAImageCache.h
//  vicinity-app
//
//  Created by Panda Systems on 11/9/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VAImageCache : NSObject

@property (nonatomic) CGSize croppedImageSize;

+ (instancetype)sharedImageCache;

- (void)getImageWithURL:(NSString *)imageURL
                success:(void (^)(UIImage *image, NSString *imageURL))success
                failure:(void (^)(NSError *error))failure;

@end
