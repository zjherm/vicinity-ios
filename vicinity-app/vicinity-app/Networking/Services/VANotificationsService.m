//
//  VANotificationsService.m
//  vicinity-app
//
//  Created by Panda Systems on 11/12/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VANotificationsService.h"

#import "VAUser.h"

NSString * const VAUserDidConnectedNotification = @"userDidConnected";
NSString * const VAUserDidDisconnectedNotification = @"userDidDisconnected";

NSString * const VAUserDidBlockedNotification = @"userDidBlocked";
NSString * const VAUserDidUnblockedNotification = @"userDidUnblocked";

@implementation VANotificationsService

+ (void)didConnectUser:(VAUser *)user {
    [[NSNotificationCenter defaultCenter] postNotificationName:VAUserDidConnectedNotification
                                                        object:nil
                                                      userInfo:@{@"user": user}];
}

+ (void)didDisconnectUser:(VAUser *)user {
    [[NSNotificationCenter defaultCenter] postNotificationName:VAUserDidDisconnectedNotification
                                                        object:nil
                                                      userInfo:@{@"user": user}];
}

+ (void)didBlockUser:(VAUser *)user {
    [[NSNotificationCenter defaultCenter] postNotificationName:VAUserDidBlockedNotification
                                                        object:nil
                                                      userInfo:@{@"user": user}];
}

+ (void)didUnblockUser:(VAUser *)user {
    [[NSNotificationCenter defaultCenter] postNotificationName:VAUserDidUnblockedNotification
                                                        object:nil
                                                      userInfo:@{@"user": user}];
}

@end
