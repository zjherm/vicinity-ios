//
//  VANotificationsService.h
//  vicinity-app
//
//  Created by Panda Systems on 11/12/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const VAUserDidConnectedNotification;
extern NSString * const VAUserDidDisconnectedNotification;

extern NSString * const VAUserDidBlockedNotification;
extern NSString * const VAUserDidUnblockedNotification;

@class VAUser;

@interface VANotificationsService : NSObject

+ (void)didConnectUser:(VAUser *)user;
+ (void)didDisconnectUser:(VAUser *)user;

+ (void)didBlockUser:(VAUser *)user;
+ (void)didUnblockUser:(VAUser *)user;

@end
