//
//  VAPostStore.m
//  vicinity-app
//
//  Created by Panda Systems on 2/17/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import "VAPostStore.h"

#import "VAPost.h"

@interface VAPostStore ()

@property (nonatomic, strong) NSArray *feedPosts;
@property (nonatomic, strong) NSArray *profilePosts;
@property (nonatomic, strong) NSArray *likedPosts;

@end

@implementation VAPostStore

+ (instancetype)sharedPostStore {
    static dispatch_once_t pred;
    static VAPostStore *sharedInstance = nil;
    
    dispatch_once(&pred, ^{
        sharedInstance = [[VAPostStore alloc] init];
    });
    
    return sharedInstance;
}

#pragma mark - Public API

- (void)saveFeedPosts:(NSArray *)posts {
    self.feedPosts = posts;
    [self savePosts:posts
           filePath:[self feedPostsFilePath]];
}

- (void)readFeedPostsWithSuccess:(void (^)(NSArray *posts))success
                         failure:(void (^)(NSError *error))failure {
    if (self.feedPosts && success) {
        success(self.feedPosts);
        return;
    }
    [self readPostsWithFilePath:[self feedPostsFilePath]
                        success:^(NSArray *posts) {
                            self.feedPosts = posts;
                            success(posts);
                        }
                        failure:failure];
}

- (void)saveProfilePosts:(NSArray *)posts {
    self.profilePosts = posts;
    [self savePosts:posts
           filePath:[self profileOwnPostsFilePath]];
}

- (void)readProfilePostsWithSuccess:(void (^)(NSArray *posts))success
                            failure:(void (^)(NSError *error))failure {
    if (self.profilePosts && success) {
        success(self.profilePosts);
        return;
    }
    [self readPostsWithFilePath:[self profileOwnPostsFilePath]
                        success:^(NSArray *posts) {
                            self.profilePosts = posts;
                            success(posts);
                        }
                        failure:failure];
}

- (void)saveLikedPosts:(NSArray *)posts {
    self.likedPosts = posts;
    [self savePosts:posts
           filePath:[self profileLikedPostsFilePath]];
}

- (void)readLikedPostsWithSuccess:(void (^)(NSArray *posts))success
                          failure:(void (^)(NSError *error))failure {
    if (self.likedPosts && success) {
        success(self.likedPosts);
        return;
    }
    [self readPostsWithFilePath:[self profileLikedPostsFilePath]
                        success:^(NSArray *posts) {
                            self.likedPosts = posts;
                            success(posts);
                        }
                        failure:failure];
}

- (void)clearCache {
    NSError *error;
    [[NSFileManager defaultManager] removeItemAtPath:[self feedPostsFilePath] error:&error];
    [[NSFileManager defaultManager] removeItemAtPath:[self profileOwnPostsFilePath] error:&error];
    [[NSFileManager defaultManager] removeItemAtPath:[self profileLikedPostsFilePath] error:&error];
}

#pragma mark - Helpers

- (void)savePosts:(NSArray *)posts filePath:(NSString *)filePath {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSArray *postsJSON = [MTLJSONAdapter JSONArrayFromModels:posts];
        
        NSError *error = nil;
        NSData *postsData = [NSJSONSerialization dataWithJSONObject:postsJSON options:NSJSONWritingPrettyPrinted error:&error];
        
        if (!error) {
            [postsData writeToFile:filePath atomically:YES];
        }
    });
}

- (void)readPostsWithFilePath:(NSString *)filePath
                      success:(void (^)(NSArray *posts))success
                     failure:(void (^)(NSError *error))failure {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData *postsData = [NSData dataWithContentsOfFile:filePath];
        
        if (postsData) {
            NSError *error = nil;
            NSArray *postsJSON = [NSJSONSerialization JSONObjectWithData:postsData options:NSJSONReadingMutableContainers error:&error];
            
            if (error && failure) {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    failure(error);
                });
            } else if (success) {
                NSArray *posts = [MTLJSONAdapter modelsOfClass:[VAPost class] fromJSONArray:postsJSON error:&error];
                if (error && failure) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        failure(error);
                    });
                } else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        success(posts);
                    });
                }
            }
        } else {
            dispatch_sync(dispatch_get_main_queue(), ^{
                failure(nil);
            });
        }
    });
}

#pragma mark - File paths

- (NSString *)feedPostsFilePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"feedPosts.json"];
    return filePath;
}

- (NSString *)profileOwnPostsFilePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"profilePosts.json"];
    return filePath;
}

- (NSString *)profileLikedPostsFilePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"likedPosts.json"];
    return filePath;
}

@end
