//
//  UILabel+VACustomFont.h
//  vicinity-app
//
//  Created by Evgeny Dedovets on 02/06/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const VASaveKeys_CurrentUser;
extern NSString * const kUserDefaultsAuthTokenKey;
extern NSString * const kUserHasSeenNotificationAlert;
extern NSString * const kUserHasUpdatedAlias;
extern NSString * const kUserDefaultsDeviceToken;
extern NSString * const kShouldShowNotificationBadge;


@class VAModel;
@interface VAUserDefaultsHelper : NSObject

+ (void)setAuthToken:(NSString *)authToken;
+ (NSString *)getAuthToken;

+ (void)saveModel:(VAModel*)model forKey:(NSString*)key;
+ (VAModel*)getModelForKey:(NSString*)key;

+(void)savelatitude:(double)lat andLongitude:(double)lon;
+(BOOL)locationHaveBeenSaved;

+ (BOOL)hasPromptedForUserNotification;

+ (void)setDeviceToken:(NSString *)deviceToken;

+ (NSString *)getDeviceToken;

+ (BOOL)shouldShowNotificationBadge;

+ (VAUser *)me;

@end
