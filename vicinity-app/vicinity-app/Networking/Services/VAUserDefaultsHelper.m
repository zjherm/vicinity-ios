//
//  UILabel+VACustomFont.h
//  vicinity-app
//
//  Created by Evgeny Dedovets on 02/06/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAUserDefaultsHelper.h"
#import "VAModel.h"
#import "VAUser.h"
#import <LaunchKit/LaunchKit.h>

NSString * const kUserDefaultsAuthTokenKey = @"VASaveKeys_AuthTokenKey";
NSString * const VASaveKeys_CurrentUser = @"VASaveKeys_CurrentUser";
NSString * const kUserHasSeenNotificationAlert = @"VASaveKeys_UserHasSeenNotificationAlert";
NSString * const kUserHasUpdatedAlias = @"VASaveKeys_UserHasUpdatedAlias";
NSString * const kUserDefaultsDeviceToken = @"VASaveKeys_DeviceTokenKey";
NSString * const kShouldShowNotificationBadge = @"VASaveKeys_ShouldShowNotificationBadge";

@implementation VAUserDefaultsHelper

+ (void)setAuthToken:(NSString *)authToken {
    [[NSUserDefaults standardUserDefaults] setObject:authToken forKey:(NSString *)kUserDefaultsAuthTokenKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)getAuthToken {
    return [[NSUserDefaults standardUserDefaults] objectForKey:(NSString *)kUserDefaultsAuthTokenKey];
}

+ (void)saveModel:(VAModel*)model forKey:(NSString*)key {
    if (key == VASaveKeys_CurrentUser && [model isKindOfClass:[VAUser class]]) {
        VAUser* user = (VAUser*)model;
        if (user.userId && user.nickname && user.email) {
            [[LaunchKit sharedInstance] setUserIdentifier:user.userId
                                                    email:user.email
                                                     name:user.nickname];
        }
    }
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:model];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (VAModel*)getModelForKey:(NSString*)key {
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    VAModel *model = (VAModel *)[NSKeyedUnarchiver unarchiveObjectWithData:data];
    return model;
}

+ (void)savelatitude:(double)lat andLongitude:(double)lon {
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithDouble:lat] forKey:kLatValue];
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithDouble:lon] forKey:kLonValue];
}

+ (BOOL)locationHaveBeenSaved {
    NSNumber *lat = [[NSUserDefaults standardUserDefaults] valueForKey:kLatValue];
    NSNumber *lon = [[NSUserDefaults standardUserDefaults] valueForKey:kLonValue];
    return lat && lon;
}

+ (BOOL)hasPromptedForUserNotification {
    return [[NSUserDefaults standardUserDefaults] boolForKey:kUserHasSeenNotificationAlert];
}

+ (void)setDeviceToken:(NSString *)deviceToken {
    [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:(NSString *)kUserDefaultsDeviceToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)getDeviceToken {
    return [[NSUserDefaults standardUserDefaults] objectForKey:(NSString *)kUserDefaultsDeviceToken];
}

+ (BOOL)shouldShowNotificationBadge {
    return [[NSUserDefaults standardUserDefaults] boolForKey:kShouldShowNotificationBadge];
}

+ (VAUser *)me {
    return (VAUser *)[VAUserDefaultsHelper getModelForKey:VASaveKeys_CurrentUser];
}

@end
