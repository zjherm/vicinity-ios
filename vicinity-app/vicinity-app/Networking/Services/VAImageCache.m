//
//  VAImageCache.m
//  vicinity-app
//
//  Created by Panda Systems on 11/9/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAImageCache.h"

#import <SDWebImage/UIImageView+WebCache.h>

@implementation VAImageCache

+ (instancetype)sharedImageCache {
    static dispatch_once_t pred;
    static VAImageCache *sharedInstance = nil;
    
    dispatch_once(&pred, ^{
        sharedInstance = [[VAImageCache alloc] init];
        sharedInstance.croppedImageSize = CGSizeMake([UIScreen mainScreen].bounds.size.width  * [UIScreen mainScreen].scale,
                                                     [UIScreen mainScreen].bounds.size.width  * [UIScreen mainScreen].scale);
    });
    
    return sharedInstance;
}

- (void)getImageWithURL:(NSString *)imageURL
                success:(void (^)(UIImage *image, NSString *imageURL))success
                failure:(void (^)(NSError *error))failure {
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache queryDiskCacheForKey:imageURL done:^(UIImage *image, SDImageCacheType cacheType) {
        if (image) {
            success(image, imageURL);
//            NSLog(@"Image is loaded from cache.");
        } else {
            SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
            [downloader downloadImageWithURL:[NSURL URLWithString:imageURL]
                                     options:0
                                    progress:^(NSInteger receivedSize, NSInteger expectedSize) { }
                                   completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished)
             {
                 if (image && finished) {
                     if (error) {
                         failure(error);
                     } else {
                         UIImage *croppedImage;
                         if (image.size.width > self.croppedImageSize.width) {
                             CGSize cropSize = CGSizeMake(self.croppedImageSize.width, self.croppedImageSize.width * (image.size.height / image.size.width));
                             NSLog(@"Image cropped to size (%f x %f).", cropSize.width, cropSize.height);
                             croppedImage = [self imageWithImage:image scaledToSize:cropSize];
                         } else {
                             croppedImage = image;
                         }
                         
                         [imageCache storeImage:croppedImage forKey:imageURL];
                         dispatch_sync(dispatch_get_main_queue(), ^{
                             success(image, imageURL);
                         });
                     }
                 }
             }];
        }
    }];
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
