//
//  VAPostStore.h
//  vicinity-app
//
//  Created by Panda Systems on 2/17/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VAPostStore : NSObject

+ (instancetype)sharedPostStore;

- (void)saveFeedPosts:(NSArray *)posts;
- (void)readFeedPostsWithSuccess:(void (^)(NSArray *posts))success
                         failure:(void (^)(NSError *error))failure;

- (void)saveProfilePosts:(NSArray *)posts;
- (void)readProfilePostsWithSuccess:(void (^)(NSArray *posts))success
                            failure:(void (^)(NSError *error))failure;

- (void)saveLikedPosts:(NSArray *)posts;
- (void)readLikedPostsWithSuccess:(void (^)(NSArray *posts))success
                          failure:(void (^)(NSError *error))failure;

- (void)clearCache;

@end
