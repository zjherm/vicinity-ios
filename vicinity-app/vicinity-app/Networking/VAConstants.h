//
//  UILabel+VACustomFont.h
//  vicinity-app
//
//  Created by Evgeny Dedovets on 02/06/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

//API endpoints
#ifdef VICINITY
    #define API_BASE_URL @"http://45.55.15.229:80/api"
#elif STAGING
    #define API_BASE_URL @"http://45.55.134.111:3006/api"
#else
    #define API_BASE_URL @"http://45.55.134.111:3007/api"
//    #define API_BASE_URL @"http://192.168.1.2:3007/api" // local server (device)
//    #define API_BASE_URL @"http://127.0.0.1:3007/api" // local server (simulator)
#endif

#define API_GET_PROFILE @"/profile"
#define API_GET_LOCATIONS @"/locations"
#define API_GET_POSTS @"/posts"
#define API_GET_POST_LIKES @"/posts/likes"
#define API_GET_COMMENTS @"/comments"
#define API_GET_USER_SEARCH @"/users"
#define API_GET_LOGOUT @"/logout"
#define API_GET_NOTIFICATIONS @"/notifications"
#define API_GET_USER_PROFILE_BY_NICKNAME @"/users/nickname"
#define API_GET_USER_PROFILE_BY_ID @"/users"
#define API_GET_COMPANY @"/companies"
#define API_GET_DEALS @"/deals"
#define API_GET_FRIENDS @"/profile/friends"
#define API_GET_FACEBOOK_FRIENDS @"/profile/facebookFriends"
#define API_GET_RESTORE_PASSWORD @"/restore-password"
#define API_GET_CONNECTIONS @"/profile/friends"
#define API_GET_FOLLOWERS @"/profile/followers"

#define API_POST_LOGIN @"/login"
#define API_POST_POSTS @"/posts"
#define API_POST_PROFILE @"/profile"
#define API_POST_COMMENTS @"/comments"
#define API_POST_LIKE @"/like"
#define API_POST_REPORT @"/report"
#define API_POST_PUSH_TOKEN @"/pushToken"
#define API_POST_NOTIFICATION_SEEN @"/notifications/seen"
#define API_POST_PROFILE_CREATE @"/profile/create"
#define API_POST_PROFILE_FACEBOOK_TOKEN @"/profile/link-facebook"
#define API_POST_CONNECTIONS @"/profile/friends"

#define API_DELETE_LIKE @"/like"
#define API_DELETE_POST @"/posts"
#define API_DELETE_COMMENT @"/comments"
#define API_DELETE_CONNECTIONS @"/profile/friends"

#define API_PUT_EDIT_POST @"/posts"
#define API_PUT_EDIT_COMMENT @"/comments"



