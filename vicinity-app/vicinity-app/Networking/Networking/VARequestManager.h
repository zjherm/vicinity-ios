//
//  UILabel+VACustomFont.h
//  vicinity-app
//
//  Created by Evgeny Dedovets on 02/06/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VAModel.h"
#import <AFNetworking/AFURLRequestSerialization.h>

typedef NS_ENUM(NSUInteger, VAHttpRequestMethod) {
    VAHttpRequestTypeGet,
    VAHttpRequestTypePost,
    VAHttpRequestTypeDelete,
    VAHttpRequestTypePatch,
    VAHttpRequestTypePut,
    VAMultipartRequestTypePost,
    VAMultipartRequestTypePut
};

typedef void(^VAAPICompletionBlock)(NSError *error, VAModel *model);
typedef void(^VAAPICompletionBlockSimple)(NSError *error, id object);

extern NSString *const VACoreRequestManagerAcceptBodyHeader;
extern NSString *const VACoreResponseManagerAcceptJSONHeader;
typedef void(^VACoreRequestCompletionBlock)(NSError *error, id jsonResponseObject);
typedef void(^VAConstructingBodyBlock)(id<AFMultipartFormData> formData);

@protocol AFMultipartFormData;
@protocol VARequestManangerDelegate <NSObject>

@required
- (NSString *)authenticationHeaderString;

@end

@interface VARequestManager : NSObject

@property (nonatomic, weak) id<VARequestManangerDelegate> delegate;
@property (assign, nonatomic, readwrite) BOOL multipartEnabled;

+ (instancetype)sharedManager;

- (void)fetchURL:(NSURL *)url
      withMethod:(VAHttpRequestMethod)requestMethod
 withParamsOrNil:(NSDictionary *)params
    requiresAuth:(BOOL)shouldAuth
constructingBodyWithBlock:(VAConstructingBodyBlock)constructingBlock               //this block is used only in multipart/form-data requests. For application/json set this to nil
withCompletionBlock:(VACoreRequestCompletionBlock)completion;


@end
