//
//  UILabel+VACustomFont.h
//  vicinity-app
//
//  Created by Evgeny Dedovets on 02/06/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAUserApiManager.h"
#import "VAUser.h"
#import "VALoginResponse.h"
#import "VALocationResponse.h"
#import "NSMutableDictionary+Params.h"
#import "VAPost.h"
#import "VAPostResponse.h"
#import "VANewPostResponse.h"
#import "VALikesResponse.h"
#import "VACommentsResponse.h"
#import "VAUserSearchResponse.h"
#import "VANotificationsResponse.h"
#import "VAGetPostResponse.h"
#import "VANewCommentResponse.h"
#import "VAUserProfileResponse.h"
#import "VABusinessResponse.h"
#import "VADealsResponse.h"
#import "VAConnectionsResponse.h"
@import GoogleMaps;

NSString *const kErrorResponseStatus = @"error";
NSString *const kSuccessResponseStatus = @"success";

@implementation VAUserApiManager

#pragma mark - GET

- (void)getCurrentUserWithFacebookToken:(NSString *)tokenString
                          andCompletion:(VAAPICompletionBlock)completion {
    
    NSDictionary *params = @{@"token": tokenString};
    
    [self getCurrentUserWithParams:params completion:completion];
}

- (void)getCurrentUserWithLogin:(NSString *)login
                       passwoed:(NSString *)password
                  andCompletion:(VAAPICompletionBlock)completion {
    
    NSDictionary *params = @{
                             @"login": login,
                             @"password": password
                             };
    
    [self getCurrentUserWithParams:params completion:completion];
}

- (void)getCurrentUserWithLogin:(NSString *)login
                       passwoed:(NSString *)password
            changePasswordToken:(NSString*)token
                  andCompletion:(VAAPICompletionBlock)completion {
    
    NSDictionary *params = @{@"restorePasswordToken": token,
                             @"login": login,
                             @"password": password
                             };
    
    [self getCurrentUserWithParams:params completion:completion];
}

- (void)getCurrentUserWithParams:(NSDictionary *)params
                      completion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_POST_LOGIN]];
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypePost
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VALoginResponse* response = [MTLJSONAdapter modelOfClass:[VALoginResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                       
                                   }
                               }
                           }];

}

- (void)getLocationsWithParams:(NSDictionary *)params
                    completion:(VAAPICompletionBlock)completion {
    
    [self getLocationsFromServerWithParams:params completion:completion];
}

- (void)getLocationsFromServerWithParams:(NSDictionary *)params
                    completion:(VAAPICompletionBlock)completion {
    
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_GET_LOCATIONS]];
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VALocationResponse* response = [MTLJSONAdapter modelOfClass:[VALocationResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                       
                                   }
                               }
                           }];
}

- (void)getLocationsFromGooglePlacesWithParams:(NSDictionary *)params
                              completion:(VAAPICompletionBlock)completion {
    double lat = [[NSUserDefaults standardUserDefaults] doubleForKey:kLatValue];
    double lon = [[NSUserDefaults standardUserDefaults] doubleForKey:kLonValue];
    CLLocationCoordinate2D coord1 = CLLocationCoordinate2DMake(lat+0.05, lon+0.1);
    CLLocationCoordinate2D coord2 = CLLocationCoordinate2DMake(lat-0.05, lon-0.1);
    GMSCoordinateBounds* bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:coord1 coordinate:coord2];
    [[GMSPlacesClient sharedClient] autocompleteQuery:params[@"text"]
                              bounds:bounds
                              filter:nil
                            callback:^(NSArray *results, NSError *error) {
                                if (error != nil) {
                                    completion(error, nil);
                                    return;
                                }
                                __block NSUInteger numberOfPlaces = results.count;
                                
                                NSMutableArray* placesArray = [NSMutableArray array];
                                VALocationResponse* responce = [[VALocationResponse alloc] init];
                                responce.locations = placesArray;
                                responce.nextPageToken = @""
                                ;
                                if (numberOfPlaces==0) {
                                    completion(nil,responce);
                                    return;
                                }
                                
                                for (GMSAutocompletePrediction* result in results) {
                                    [[GMSPlacesClient sharedClient] lookUpPlaceID:result.placeID callback:^(GMSPlace *place, NSError *error) {
                                        if (error != nil) {
                                        } else {
                                            if (place != nil) {
                                                VALocation* location = [[VALocation alloc] initWithGMSPlace:place];
                                                [placesArray addObject:location];
                                            
                                            } else {
                                            }
                                        }
                                        numberOfPlaces--;
                                        if (numberOfPlaces<=0) {
                                            completion(nil,responce);
                                        }
                                    }];
                                }
                            }];
}


- (void)getPostsWithParams:(NSDictionary *)params
                completion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_GET_POSTS]];
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
//                               jsonResponseObject = [self testData];
//                               error = nil;
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VAPostResponse* response = [MTLJSONAdapter modelOfClass:[VAPostResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                       
                                   }
                               }
                           }];
}

- (void)getPostLikesForPostWithID:(NSString *)postID
                   withCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_GET_POST_LIKES]];
    
    NSDictionary *params = @{
                             @"postId": postID,
                             };
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VALikesResponse* response = [MTLJSONAdapter modelOfClass:[VALikesResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                   }
                               }
                           }];
}

- (void)getCommentsToPostWithPostID:(NSString *)postID
                     withCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/posts/%@%@", API_BASE_URL, postID, API_GET_COMMENTS]];
    
    if (!postID) {
        postID = @"";
    }
    
    NSDictionary *params = @{
                             @"postId": postID,
                             };
    
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VACommentsResponse* response = [MTLJSONAdapter modelOfClass:[VACommentsResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                   }
                               }
                           }];
    
    
}

- (void)getUserByAliasSearch:(NSString *)nickname
              withCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_GET_USER_SEARCH]];
    
    NSDictionary *params = @{
                             @"text": nickname,
                             };
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VAUserSearchResponse* response = [MTLJSONAdapter modelOfClass:[VAUserSearchResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                   }
                               }
                           }];
}

- (void)getLogoutWithDeviceToken:(NSString *)deviceToken
                   andCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_GET_LOGOUT]];
    
    NSDictionary *params = nil;
    
    if (deviceToken) {
        params = @{
                   @"token": deviceToken,
                   };
    }
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VAServerResponse* response = [MTLJSONAdapter modelOfClass:[VAServerResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                   }
                               }
                           }];
}

- (void)getNotificationsWithCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_GET_NOTIFICATIONS]];
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:nil
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VANotificationsResponse* response = [MTLJSONAdapter modelOfClass:[VANotificationsResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                   }
                               }
                           }];
}

- (void)getPostWithPostID:(NSString *)postID
            andCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@", API_BASE_URL, API_GET_POSTS, postID]];
    
    NSDictionary *params = @{
                             @"postId": postID,
                             @"lat": ((NSNumber *)[[NSUserDefaults standardUserDefaults] valueForKey:kLatValue]).stringValue,
                             @"lon": ((NSNumber *)[[NSUserDefaults standardUserDefaults] valueForKey:kLonValue]).stringValue,
                             };
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VAGetPostResponse* response = [MTLJSONAdapter modelOfClass:[VAGetPostResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                   }
                               }
                           }];
}

- (void)getUserWithNickname:(NSString *)nickname
              andCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@", API_BASE_URL, API_GET_USER_PROFILE_BY_NICKNAME, nickname]];
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:nil
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VAUserProfileResponse* response = [MTLJSONAdapter modelOfClass:[VAUserProfileResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                   }
                               }
                           }];


}

- (void)getUserWithID:(NSString *)userID
              andCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@", API_BASE_URL, API_GET_USER_PROFILE_BY_ID, userID]];
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:nil
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VAUserProfileResponse* response = [MTLJSONAdapter modelOfClass:[VAUserProfileResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                   }
                               }
                           }];
    
    
}

- (void)getCompanyWithID:(NSString *)companyID
           andCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@", API_BASE_URL, API_GET_COMPANY, companyID]];
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:nil
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VABusinessResponse* response = [MTLJSONAdapter modelOfClass:[VABusinessResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                   }
                               }
                           }];

}

- (void)getCompanyWithGooglePlaceID:(NSString *)googlePlaceID
           andCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_GET_COMPANY]];
    
    NSDictionary *params = @{
                             @"googlePlaceId": googlePlaceID,
                             };
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VABusinessResponse* response = [MTLJSONAdapter modelOfClass:[VABusinessResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                   }
                               }
                           }];
    
}

- (void)getPostsCreatedByUserWithID:(NSString *)userID
                    currentLatitude:(NSNumber *)lat
                   currentLongitude:(NSNumber *)lon
                          withLimit:(NSNumber *)limit
                             offset:(NSNumber *)offset
                andCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_GET_POSTS]];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                  @"createdBy": userID,
                                                                                  @"limit": limit,
                                                                                  @"offset": offset
                                                                                  }];
    
    if (lat) {
        [params addEntriesFromDictionary:@{@"lat": lat}];
    }
    
    if (lon) {
        [params addEntriesFromDictionary:@{@"lon": lon}];
    }
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VAPostResponse* response = [MTLJSONAdapter modelOfClass:[VAPostResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                       
                                   }
                               }
                           }];
}

- (void)getPostsLikedByUserWithID:(NSString *)userID
                  currentLatitude:(NSNumber *)lat
                 currentLongitude:(NSNumber *)lon
                        withLimit:(NSNumber *)limit
                           offset:(NSNumber *)offset
                      andCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_GET_POSTS]];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                 @"likedBy": userID,
                                                                                 @"limit": limit,
                                                                                 @"offset": offset
                                                                                 }];
    
    if (lat) {
        [params addEntriesFromDictionary:@{@"lat": lat}];
    }
    
    if (lon) {
        [params addEntriesFromDictionary:@{@"lon": lon}];
    }
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VAPostResponse* response = [MTLJSONAdapter modelOfClass:[VAPostResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                       
                                   }
                               }
                           }];
}

- (void)getDealsWithLimit:(NSNumber *)limit
                   offset:(NSNumber *)offset
                 latitude:(NSNumber *)lat
                longitude:(NSNumber *)lon
                 distance:(NSNumber *)distance
            andCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_GET_DEALS]];
    
    NSDictionary *params = @{
                             @"limit": limit,
                             @"offset": offset,
                             @"lat": lat,
                             @"lon": lon,
                             @"distance": distance
                             };
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VADealsResponse* response = [MTLJSONAdapter modelOfClass:[VADealsResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                       
                                   }
                               }
                           }];
}

- (void)getFacebookFriendsWithCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_GET_FACEBOOK_FRIENDS]];
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:nil
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VAUserSearchResponse* response = [MTLJSONAdapter modelOfClass:[VAUserSearchResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                       
                                   }
                               }
                           }];
}

- (void)getResetPasswordForUsernameOrEmail:(NSString *)usernameOrEmail
                            withCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_GET_RESTORE_PASSWORD]];
    
    NSDictionary *params = @{
                             @"login": usernameOrEmail,
                             };

    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VAServerResponse* response = [MTLJSONAdapter modelOfClass:[VAServerResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                       
                                   }
                               }
                           }];

}

- (void)getConnectionsWithCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_GET_CONNECTIONS]];
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:nil
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VAConnectionsResponse* response = [MTLJSONAdapter modelOfClass:[VAConnectionsResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                       
                                   }
                               }
                           }];
}

- (void)getFollowersWithCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_GET_FOLLOWERS]];
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:nil
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VAConnectionsResponse* response = [MTLJSONAdapter modelOfClass:[VAConnectionsResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                       
                                   }
                               }
                           }];
}

- (void)getUsersByEmailListSearch:(NSString *)emailList
              withCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_GET_USER_SEARCH]];
    
    NSDictionary *params = @{
                             @"email": emailList,
                             };
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VAUserSearchResponse* response = [MTLJSONAdapter modelOfClass:[VAUserSearchResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                   }
                               }
                           }];
}

- (void)addFriendsFromEmailList:(NSString *)emailList
                 withCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, @"/profile/friends/add"]];
    
    NSDictionary *params = @{
                             @"email": emailList,
                             };
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypePost
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VAConnectionsResponse* response = [MTLJSONAdapter modelOfClass:[VAConnectionsResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                       
                                   }
                               }
                           }];
}

- (void)getPostsForPlaceWithID:(NSString *)placeID
               currentLatitude:(NSNumber *)lat
              currentLongitude:(NSNumber *)lon
                     withLimit:(NSNumber *)limit
                        offset:(NSNumber *)offset
                 andCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_GET_POSTS]];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                  @"placeId": placeID,
                                                                                  @"limit": limit,
                                                                                  @"offset": offset
                                                                                  }];
    
    if (lat) {
        [params addEntriesFromDictionary:@{@"lat": lat}];
    }
    
    if (lon) {
        [params addEntriesFromDictionary:@{@"lon": lon}];
    }
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VAPostResponse* response = [MTLJSONAdapter modelOfClass:[VAPostResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                       
                                   }
                               }
                           }];
}


#pragma mark - POST

- (void)postUpdateCurrentUserWithParams:(NSDictionary *)params
                          imageData:(NSData *)imageData
                     withCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_POST_PROFILE]];
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAMultipartRequestTypePost
                               withParamsOrNil:nil
                                  requiresAuth:YES
                     constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                         if (imageData) {
                             [formData appendPartWithFileData:imageData name:@"avatar" fileName:@"avatar.jpg" mimeType:@"image/jpeg"];
                         }
                         [formData appendPartWithFormData:[[params valueForKey:@"fullname"] dataUsingEncoding:NSUTF8StringEncoding]
                                                     name:@"fullname"];
                         [formData appendPartWithFormData:[[params valueForKey:@"nickname"] dataUsingEncoding:NSUTF8StringEncoding]
                                                     name:@"nickname"];
                         [formData appendPartWithFormData:[[params valueForKey:@"email"] dataUsingEncoding:NSUTF8StringEncoding]
                                                     name:@"email"];
                         [formData appendPartWithFormData:[[params valueForKey:@"city"] dataUsingEncoding:NSUTF8StringEncoding]
                                                     name:@"city"];
                         [formData appendPartWithFormData:[[params valueForKey:@"state"] dataUsingEncoding:NSUTF8StringEncoding]
                                                     name:@"state"];
                         [formData appendPartWithFormData:[[params valueForKey:@"country"] dataUsingEncoding:NSUTF8StringEncoding]
                                                     name:@"country"];
                         [formData appendPartWithFormData:[[params valueForKey:@"gender"] dataUsingEncoding:NSUTF8StringEncoding]
                                                     name:@"gender"];
                         if ([params valueForKey:@"bio"]) {
                             [formData appendPartWithFormData:[[params valueForKey:@"bio"] dataUsingEncoding:NSUTF8StringEncoding]
                                                         name:@"bio"];
                         }

                     }
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VALoginResponse* response = [MTLJSONAdapter modelOfClass:[VALoginResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                       
                                   }
                               }
                           }];

}

- (void)postPostWithParams:(NSDictionary *)params
                 imageData:(NSData *)imageData
            withCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_POST_POSTS]];
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAMultipartRequestTypePost
                               withParamsOrNil:nil
                                  requiresAuth:YES
                     constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                         if (imageData) {
                             [formData appendPartWithFileData:imageData name:@"image" fileName:@"image.jpg" mimeType:@"image/jpeg"];
                         }
                         if ([params valueForKey:@"description"]) {
                             [formData appendPartWithFormData:[[params valueForKey:@"description"] dataUsingEncoding:NSUTF8StringEncoding]
                                                         name:@"description"];
                         }
                         if ([params valueForKey:@"placeId"]) {
                             [formData appendPartWithFormData:[[params valueForKey:@"placeId"] dataUsingEncoding:NSUTF8StringEncoding]
                                                         name:@"placeId"];
                         }
                         if ([params valueForKey:@"facebookPlaceId"]) {
                             [formData appendPartWithFormData:[[params valueForKey:@"facebookPlaceId"] dataUsingEncoding:NSUTF8StringEncoding]
                                                         name:@"facebookPlaceId"];
                         }
                         [formData appendPartWithFormData:[((NSNumber *)[params valueForKey:@"lat"]).stringValue dataUsingEncoding:NSUTF8StringEncoding]
                                                     name:@"lat"];
                         [formData appendPartWithFormData:[((NSNumber *)[params valueForKey:@"lon"]).stringValue dataUsingEncoding:NSUTF8StringEncoding]
                                                     name:@"lon"];
                         [formData appendPartWithFormData:[[params valueForKey:@"wallPost"] dataUsingEncoding:NSUTF8StringEncoding]
                                                     name:@"wallPost"];
                         [formData appendPartWithFormData:[[params valueForKey:@"isLandscapePhoto"] dataUsingEncoding:NSUTF8StringEncoding]
                                                     name:@"isLandscapePhoto"];
                     }
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VANewPostResponse* response = [MTLJSONAdapter modelOfClass:[VANewPostResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                       
                                   }
                               }
                           }];
}

- (void)postCommentToPostWithPostID:(NSString *)postID
                               text:(NSString *)text
                    andCurentUserID:(NSString *)userID
                     withCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/posts/%@%@", API_BASE_URL, postID, API_POST_COMMENTS]];
    
    if (!postID) {
        postID = @"";
    } else if (!text) {
        text = @"";
    } else if (!userID) {
        userID = @"";
    }
    
    NSDictionary *params = @{
                             @"postId": postID,
                             @"text": text,
                             @"userId": userID,
                             };
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypePost
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   
                                   NSError* serializationError = nil;
                                   
                                   VANewCommentResponse* response = [MTLJSONAdapter modelOfClass:[VANewCommentResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                   }
                               }
                           }];
    
}

- (void)postLikePostWithID:(NSString *)postID
            withCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_POST_LIKE]];
    NSDictionary *params = @{
                             @"postId": postID,
                             };
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypePost
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   
                                   NSError* serializationError = nil;
                                   
                                   VAServerResponse* response = [MTLJSONAdapter modelOfClass:[VAServerResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                   }
                               }
                           }];
    
}

- (void)postReportWithText:(NSString *)text
                deviceType:(NSString *)deviceType
           operationSystem:(NSString *)os
        applicationVersion:(NSString *)applicationVersion
                reportType:(NSString *)reportType
            withCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_POST_REPORT]];
    
    NSDictionary *params = @{
                             @"text": text,
                             @"deviceType": deviceType,
                             @"os": os,
                             @"version": applicationVersion,
                             @"reportType": reportType
                             };
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypePost
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   
                                   NSError* serializationError = nil;
                                   
                                   VAServerResponse* response = [MTLJSONAdapter modelOfClass:[VAServerResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                   }
                               }
                           }];
}

- (void)postDeviceToken:(NSString *)deviceToken
         withCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_POST_PUSH_TOKEN]];
    
    NSDictionary *params = nil;
    
    if (deviceToken) {
        params = @{
                   @"token": deviceToken,
                   };
    }
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypePost
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   
                                   NSError* serializationError = nil;
                                   
                                   VAServerResponse* response = [MTLJSONAdapter modelOfClass:[VAServerResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                   }
                               }
                           }];
}

- (void)postNotificationSeenWithCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_POST_NOTIFICATION_SEEN]];
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypePost
                               withParamsOrNil:nil
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   
                                   NSError* serializationError = nil;
                                   
                                   VAServerResponse* response = [MTLJSONAdapter modelOfClass:[VAServerResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                   }
                               }
                           }];
}

- (void)postCreateUserWithEmail:(NSString *)email
                       password:(NSString *)password
                       fullname:(NSString *)fullname
                       username:(NSString *)username
                           city:(NSString *)city
                          state:(NSString *)state
                        country:(NSString *)country
                         gender:(NSString *)gender
                    avatarImage:(UIImage *)image
                  andCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_POST_PROFILE_CREATE]];

    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAMultipartRequestTypePost
                               withParamsOrNil:nil
                                  requiresAuth:YES
                     constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                         if (image) {
                             NSData *imgData = UIImageJPEGRepresentation(image, 0.1);
                             [formData appendPartWithFileData:imgData name:@"image" fileName:@"image.jpg" mimeType:@"image/jpeg"];
                         }
                         if (email) {
                             [formData appendPartWithFormData:[email dataUsingEncoding:NSUTF8StringEncoding] name:@"email"];
                         }
                         if (password) {
                             [formData appendPartWithFormData:[password dataUsingEncoding:NSUTF8StringEncoding] name:@"password"];
                         }
                         if (username) {
                             [formData appendPartWithFormData:[username dataUsingEncoding:NSUTF8StringEncoding] name:@"nickname"];
                         }
                         if (state) {
                             [formData appendPartWithFormData:[state dataUsingEncoding:NSUTF8StringEncoding] name:@"state"];
                         }
                         if (gender) {
                             [formData appendPartWithFormData:[gender dataUsingEncoding:NSUTF8StringEncoding] name:@"gender"];
                         }
                         if (city) {
                             [formData appendPartWithFormData:[city dataUsingEncoding:NSUTF8StringEncoding] name:@"city"];
                         }
                         if (country) {
                             [formData appendPartWithFormData:[country dataUsingEncoding:NSUTF8StringEncoding] name:@"country"];
                         }
                         if (fullname) {
                             [formData appendPartWithFormData:[fullname dataUsingEncoding:NSUTF8StringEncoding] name:@"fullname"];
                         }
                     }
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VALoginResponse *response = [MTLJSONAdapter modelOfClass:[VALoginResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                       
                                   }
                               }
                           }];

}

- (void)postCurrentSessionAddLinkToFacebookToken:(NSString *)facebookToken
                                  withCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_POST_PROFILE_FACEBOOK_TOKEN]];

    NSDictionary *params = nil;
    
    if (facebookToken) {
        params = @{
                   @"token": facebookToken,
                   };
    }
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypePost
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   
                                   NSError* serializationError = nil;
                                   
                                   VALoginResponse* response = [MTLJSONAdapter modelOfClass:[VALoginResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                   }
                               }
                           }];
}

- (void)postChangeOldPassword:(NSString *)oldPassword
                toNewPassword:(NSString *)newPassword
               withCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_POST_PROFILE]];
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAMultipartRequestTypePost
                               withParamsOrNil:nil
                                  requiresAuth:YES
                     constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                        [formData appendPartWithFormData:[oldPassword dataUsingEncoding:NSUTF8StringEncoding]
                                                     name:@"oldPassword"];
                         [formData appendPartWithFormData:[newPassword dataUsingEncoding:NSUTF8StringEncoding]
                                                     name:@"newPassword"];
                     }
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VALoginResponse* response = [MTLJSONAdapter modelOfClass:[VALoginResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                       
                                   }
                               }
                           }];
}

- (void)postAddFriendWithID:(NSString *)connectionID
                  andCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@", API_BASE_URL, API_POST_CONNECTIONS, connectionID]];
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypePost
                               withParamsOrNil:nil
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   
                                   NSError* serializationError = nil;
                                   
                                   VAUserProfileResponse* response = [MTLJSONAdapter modelOfClass:[VAUserProfileResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       completion(nil, response);
                                   }
                               }
                           }];
}


#pragma mark - DELETE

- (void)deleteLikePostWithID:(NSString *)postID
              withCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_POST_LIKE]];
    NSDictionary *params = @{
                             @"postId": postID,
                             };
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeDelete
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   
                                   NSError* serializationError = nil;
                                   
                                   VAServerResponse* response = [MTLJSONAdapter modelOfClass:[VAServerResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                   }
                               }
                           }];
}

- (void)deletePostWithID:(NSString *)postID
          withCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@", API_BASE_URL, API_DELETE_POST, postID]];
    
    NSDictionary *params = @{
                             @"postId": postID,
                             };
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeDelete
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   
                                   NSError* serializationError = nil;
                                   
                                   VAServerResponse* response = [MTLJSONAdapter modelOfClass:[VAServerResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                   }
                               }
                           }];
}

- (void)deleteCommentWithID:(NSString *)commentID
              forPostWithID:(NSString *)postID
             withCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/posts/%@%@", API_BASE_URL, postID, API_DELETE_COMMENT]];
    
    NSDictionary *params = @{
                             @"commentId": commentID,
                             @"postId": postID,
                             };
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeDelete
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   
                                   NSError* serializationError = nil;
                                   
                                   VAServerResponse* response = [MTLJSONAdapter modelOfClass:[VAServerResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                   }
                               }
                           }];
    
}

- (void)deleteConnectionWithID:(NSString *)connectionID
                withCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@", API_BASE_URL, API_DELETE_CONNECTIONS, connectionID]];
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeDelete
                               withParamsOrNil:nil
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   
                                   NSError* serializationError = nil;
                                   
                                   VAUserProfileResponse* response = [MTLJSONAdapter modelOfClass:[VAUserProfileResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       completion(nil, response);
                                   }
                               }
                           }];
    
}

#pragma mark - PUT

- (void)putPostViewWithPostID:(NSString *)postID
            andCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@/view/%@", API_BASE_URL, API_GET_POSTS, postID]];
    
    NSDictionary *params = @{
                             @"postId": postID,
                             };
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypePut
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VAServerResponse* response = [MTLJSONAdapter modelOfClass:[VAServerResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                   
                                       completion(nil, response);
                                   }
                               }
                           }];
}

- (void)putEditCommentWithID:(NSString *)commentID
               forPostWithID:(NSString *)postID andNewCommentText:(NSString *)text
              withCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/posts/%@%@", API_BASE_URL, postID, API_PUT_EDIT_COMMENT]];
    
    NSDictionary *params = @{
                             @"commentId": commentID,
                             @"text": text,
                             };
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypePut
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VANewCommentResponse* response = [MTLJSONAdapter modelOfClass:[VANewCommentResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                       
                                   }
                               }
                           }];

}

- (void)putEditPostWithID:(NSString *)postID
           andNewPostText:(NSString *)text
                      lat:(NSNumber *)lat
                      lon:(NSNumber *)lon
                  placeID:(NSString *)placeID
                 postToFB:(BOOL)shouldPostToFB
                imageData:(NSData *)imageData
        shouldUpdatePhoto:(BOOL)shouldUpdatePhoto
           withCompletion:(VAAPICompletionBlock)completion {
    
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@", API_BASE_URL, API_PUT_EDIT_POST, postID]];
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAMultipartRequestTypePut
                               withParamsOrNil:nil
                                  requiresAuth:YES
                     constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                         if (shouldUpdatePhoto) {
                             [formData appendPartWithFileData:imageData name:@"image" fileName:@"image.jpg" mimeType:@"image/jpeg"];
                         }
                         
                         [formData appendPartWithFormData:[text dataUsingEncoding:NSUTF8StringEncoding]
                                                     name:@"description"];
                         
                         [formData appendPartWithFormData:[placeID dataUsingEncoding:NSUTF8StringEncoding]
                                                     name:@"placeId"];
                         
                         [formData appendPartWithFormData:[[lat stringValue] dataUsingEncoding:NSUTF8StringEncoding]
                                                     name:@"lat"];
                         [formData appendPartWithFormData:[[lon stringValue] dataUsingEncoding:NSUTF8StringEncoding]
                                                     name:@"lon"];
                         [formData appendPartWithFormData:[(shouldUpdatePhoto ? @"true" : @"false") dataUsingEncoding:NSUTF8StringEncoding]
                                                     name:@"wallPost"];
                         
                     }
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VAServerResponse* response = [MTLJSONAdapter modelOfClass:[VAServerResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                       
                                   }
                               }
                           }];
}

#pragma mark - Posts subscribe / unsubscribe

- (void)subscribePostWithID:(NSString *)postID
              andCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@/%@", API_BASE_URL, API_POST_POSTS, postID, @"subscribe"]];
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypePost
                               withParamsOrNil:nil
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   VANewPostResponse* response = [MTLJSONAdapter modelOfClass:[VANewPostResponse class]
                                                                           fromJSONDictionary:jsonResponseObject
                                                                                        error:&serializationError];
                                   
                                   if (serializationError) {
                                       completion(serializationError, nil);
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       completion(response.error, nil);
                                   } else {
                                       completion(nil, response.post);
                                   }
                               }
                           }];
}

- (void)unsubscribePostWithID:(NSString *)postID
                andCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@/%@", API_BASE_URL, API_POST_POSTS, postID, @"unsubscribe"]];
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypePost
                               withParamsOrNil:nil
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   VANewPostResponse* response = [MTLJSONAdapter modelOfClass:[VANewPostResponse class]
                                                                           fromJSONDictionary:jsonResponseObject
                                                                                        error:&serializationError];
                                   
                                   if (serializationError) {
                                       completion(serializationError, nil);
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       completion(response.error, nil);
                                   } else {
                                       completion(nil, response.post);
                                   }
                               }
                           }];
}

#pragma mark - Users block / unblock

- (void)blockUserWithID:(NSString *)userID
              andCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@/%@", API_BASE_URL, @"/users", userID, @"block"]];
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypePost
                               withParamsOrNil:nil
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   VAUserProfileResponse* response = [MTLJSONAdapter modelOfClass:[VAUserProfileResponse class]
                                                                               fromJSONDictionary:jsonResponseObject
                                                                                            error:&serializationError];
                                   
                                   if (serializationError) {
                                       completion(serializationError, nil);
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       completion(response.error, nil);
                                   } else {
                                       completion(nil, response);
                                   }
                               }
                           }];
}

- (void)unblockUserWithID:(NSString *)userID
          andCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@/%@", API_BASE_URL, @"/users", userID, @"unblock"]];
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypePost
                               withParamsOrNil:nil
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   VAUserProfileResponse* response = [MTLJSONAdapter modelOfClass:[VAUserProfileResponse class]
                                                                               fromJSONDictionary:jsonResponseObject
                                                                                            error:&serializationError];
                                   
                                   if (serializationError) {
                                       completion(serializationError, nil);
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       completion(response.error, nil);
                                   } else {
                                       completion(nil, response);
                                   }
                               }
                           }];
}

#pragma mark - Reports

- (void)sendReportAboutUserWithID:(NSString *)userID
                    andReportType:(NSString *)reportType
                        andTarget:(NSString *)target
                        andPostId:(NSString *)postId
                     andCommentId:(NSString *)commentId
                    andCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/reports", API_BASE_URL]];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:userID forKey:@"userId"];
    [params setObject:reportType forKey:@"reportType"];
    [params setObject:target forKey:@"target"];
    if (postId) {
        [params setObject:postId forKey:@"postId"];
    }
    if (commentId) {
        [params setObject:commentId forKey:@"commentId"];
    }
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypePost
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   VAServerResponse* response = [MTLJSONAdapter modelOfClass:[VAServerResponse class] fromJSONDictionary:jsonResponseObject
                                                                                       error:&serializationError];
                                   
                                   if (serializationError) {
                                       completion(serializationError, nil);
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       completion(response.error, nil);
                                   } else {
                                       completion(nil, response);
                                   }
                               }
                           }];
}

#pragma mark - Profile

- (void)getUserProfileWithCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, API_GET_PROFILE]];
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:nil
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   VALoginResponse* response = [MTLJSONAdapter modelOfClass:[VALoginResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];

                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       completion(response.error, nil);
                                   } else {
                                       completion(nil, response.loggedUser);
                                   }
                               }
                           }];
}

- (void)getViewedUsersWithCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, @"/users/viewed"]];
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:nil
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VAConnectionsResponse *response = [MTLJSONAdapter modelOfClass:[VAConnectionsResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       completion(response.error, nil);
                                   } else {
                                       completion(nil, response);
                                   }
                               }
                           }];
}

- (void)clearViewedUsersWithCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, @"/users/viewed/clear"]];
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypePost
                               withParamsOrNil:nil
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VALoginResponse* response = [MTLJSONAdapter modelOfClass:[VALoginResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       completion(response.error, nil);
                                   } else {
                                       completion(nil, response);
                                   }
                               }
                           }];
}

- (void)restoreOriginalEmailWithCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, @"/email-restore-original"]];
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:nil
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VALoginResponse* response = [MTLJSONAdapter modelOfClass:[VALoginResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       completion(response.error, nil);
                                   } else {
                                       completion(nil, response);
                                   }
                               }
                           }];
}

- (void)removeUserWithId:(NSString *)userId
          withCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, @"/remove-users"]];
    
    NSDictionary *params = @{
                             @"userIds": userId,
                             };
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypePost
                               withParamsOrNil:params
                                  requiresAuth:YES
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VAServerResponse* response = [MTLJSONAdapter modelOfClass:[VAServerResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                       
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       
                                       completion(response.error, nil);
                                       
                                   } else {
                                       
                                       completion(nil, response);
                                       
                                   }
                               }
                           }];
}

- (void)checkUsername:(NSString *)username
       withCompletion:(VAAPICompletionBlock)completion {
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_BASE_URL, @"/check-username"]];
    
    NSDictionary *params = @{
                             @"username": username,
                             };
    
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypePost
                               withParamsOrNil:params
                                  requiresAuth:NO
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   NSError* serializationError = nil;
                                   
                                   VAServerResponse* response = [MTLJSONAdapter modelOfClass:[VAServerResponse class] fromJSONDictionary:jsonResponseObject error:&serializationError];
                                   
                                   if (serializationError) {
                                       // return error
                                       completion(serializationError, nil);
                                   } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                                       completion(response.error, nil);
                                   } else {
                                       completion(nil, response);
                                   }
                               }
                           }];
}

@end
