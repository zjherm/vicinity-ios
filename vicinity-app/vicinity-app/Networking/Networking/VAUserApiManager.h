//
//  UILabel+VACustomFont.h
//  vicinity-app
//
//  Created by Evgeny Dedovets on 02/06/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VARequestManager.h"

extern NSString *const kErrorResponseStatus;
extern NSString *const kSuccessResponseStatus;

@class VAUser;

@interface VAUserApiManager : NSObject 

#pragma mark - GET

- (void)getCurrentUserWithFacebookToken:(NSString *)tokenString
                          andCompletion:(VAAPICompletionBlock)completion;

- (void)getCurrentUserWithLogin:(NSString *)login
                       passwoed:(NSString *)password
                  andCompletion:(VAAPICompletionBlock)completion;

- (void)getCurrentUserWithLogin:(NSString *)login
                       passwoed:(NSString *)password
            changePasswordToken:(NSString*)token
                  andCompletion:(VAAPICompletionBlock)completion;

- (void)getLocationsWithParams:(NSDictionary *)params
                    completion:(VAAPICompletionBlock)completion;

- (void)getLocationsFromGooglePlacesWithParams:(NSDictionary *)params
                                    completion:(VAAPICompletionBlock)completion;

- (void)getPostsWithParams:(NSDictionary *)params
                completion:(VAAPICompletionBlock)completion;

- (void)getPostLikesForPostWithID:(NSString *)postID
                   withCompletion:(VAAPICompletionBlock)completion;

- (void)getCommentsToPostWithPostID:(NSString *)postID
                     withCompletion:(VAAPICompletionBlock)completion;

- (void)getUserByAliasSearch:(NSString *)nickname
              withCompletion:(VAAPICompletionBlock)completion;

- (void)getLogoutWithDeviceToken:(NSString *)deviceToken
                   andCompletion:(VAAPICompletionBlock)completion;

- (void)getNotificationsWithCompletion:(VAAPICompletionBlock)completion;

- (void)getPostWithPostID:(NSString *)postID
            andCompletion:(VAAPICompletionBlock)completion;

- (void)getUserWithNickname:(NSString *)nickname
              andCompletion:(VAAPICompletionBlock)completion;

- (void)getUserWithID:(NSString *)userID
        andCompletion:(VAAPICompletionBlock)completion;

- (void)getCompanyWithID:(NSString *)companyID
           andCompletion:(VAAPICompletionBlock)completion;

- (void)getCompanyWithGooglePlaceID:(NSString *)googlePlaceID
                      andCompletion:(VAAPICompletionBlock)completion;

- (void)getPostsCreatedByUserWithID:(NSString *)userID
                    currentLatitude:(NSNumber *)lat
                   currentLongitude:(NSNumber *)lon
                          withLimit:(NSNumber *)limit
                             offset:(NSNumber *)offset
                      andCompletion:(VAAPICompletionBlock)completion;

- (void)getPostsLikedByUserWithID:(NSString *)userID
                  currentLatitude:(NSNumber *)lat
                 currentLongitude:(NSNumber *)lon
                        withLimit:(NSNumber *)limit
                           offset:(NSNumber *)offset
                    andCompletion:(VAAPICompletionBlock)completion;

- (void)getDealsWithLimit:(NSNumber *)limit
                   offset:(NSNumber *)offset
                 latitude:(NSNumber *)lat
                longitude:(NSNumber *)lon
                 distance:(NSNumber *)distance
            andCompletion:(VAAPICompletionBlock)completion;

- (void)getFacebookFriendsWithCompletion:(VAAPICompletionBlock)completion;

- (void)getResetPasswordForUsernameOrEmail:(NSString *)usernameOrEmail
                            withCompletion:(VAAPICompletionBlock)completion;

- (void)getConnectionsWithCompletion:(VAAPICompletionBlock)completion;

- (void)getFollowersWithCompletion:(VAAPICompletionBlock)completion;

- (void)getUsersByEmailListSearch:(NSString *)emailList
                   withCompletion:(VAAPICompletionBlock)completion;

- (void)getPostsForPlaceWithID:(NSString *)placeID
               currentLatitude:(NSNumber *)lat
              currentLongitude:(NSNumber *)lon
                     withLimit:(NSNumber *)limit
                        offset:(NSNumber *)offset
                 andCompletion:(VAAPICompletionBlock)completion;

#pragma mark - POST

- (void)postUpdateCurrentUserWithParams:(NSDictionary *)params
                              imageData:(NSData *)imageData
                         withCompletion:(VAAPICompletionBlock)completion;

- (void)postPostWithParams:(NSDictionary *)params
                 imageData:(NSData *)imageData
            withCompletion:(VAAPICompletionBlock)completion;

- (void)postCommentToPostWithPostID:(NSString *)postID
                               text:(NSString *)text
                    andCurentUserID:(NSString *)userID
                     withCompletion:(VAAPICompletionBlock)completion;

- (void)postLikePostWithID:(NSString *)postID
            withCompletion:(VAAPICompletionBlock)completion;

- (void)postReportWithText:(NSString *)text
                deviceType:(NSString *)deviceType
           operationSystem:(NSString *)os
        applicationVersion:(NSString *)applicationVersion
                reportType:(NSString *)reportType
            withCompletion:(VAAPICompletionBlock)completion;

- (void)postDeviceToken:(NSString *)deviceToken
         withCompletion:(VAAPICompletionBlock)completion;

- (void)postNotificationSeenWithCompletion:(VAAPICompletionBlock)completion;

- (void)postCreateUserWithEmail:(NSString *)email
                       password:(NSString *)password
                       fullname:(NSString *)fullname
                       username:(NSString *)username
                           city:(NSString *)city
                          state:(NSString *)state
                        country:(NSString *)country
                         gender:(NSString *)gender
                    avatarImage:(UIImage *)image
                  andCompletion:(VAAPICompletionBlock)completion;

- (void)postCurrentSessionAddLinkToFacebookToken:(NSString *)facebookToken
                                  withCompletion:(VAAPICompletionBlock)completion;

- (void)postChangeOldPassword:(NSString *)oldPassword
                toNewPassword:(NSString *)newPassword
               withCompletion:(VAAPICompletionBlock)completion;

- (void)postAddFriendWithID:(NSString *)connectionID
                  andCompletion:(VAAPICompletionBlock)completion;

#pragma mark - DELETE

- (void)deleteLikePostWithID:(NSString *)postID
              withCompletion:(VAAPICompletionBlock)completion;

- (void)deletePostWithID:(NSString *)postID
          withCompletion:(VAAPICompletionBlock)completion;

- (void)deleteCommentWithID:(NSString *)commentID
              forPostWithID:(NSString *)postID
             withCompletion:(VAAPICompletionBlock)completion;

- (void)deleteConnectionWithID:(NSString *)connectionID
                withCompletion:(VAAPICompletionBlock)completion;

#pragma mark - PUT

- (void)putPostViewWithPostID:(NSString *)postID
                andCompletion:(VAAPICompletionBlock)completion;


- (void)putEditCommentWithID:(NSString *)commentID
               forPostWithID:(NSString *)postID andNewCommentText:(NSString *)text
              withCompletion:(VAAPICompletionBlock)completion;

- (void)putEditPostWithID:(NSString *)postID
           andNewPostText:(NSString *)text
                      lat:(NSNumber *)lat
                      lon:(NSNumber *)lon
                  placeID:(NSString *)placeID
                 postToFB:(BOOL)shouldPostToFB
                imageData:(NSData *)imageData
        shouldUpdatePhoto:(BOOL)shouldUpdatePhoto
           withCompletion:(VAAPICompletionBlock)completion;


- (void)subscribePostWithID:(NSString *)postID
              andCompletion:(VAAPICompletionBlock)completion;
- (void)unsubscribePostWithID:(NSString *)postID
                andCompletion:(VAAPICompletionBlock)completion;


- (void)blockUserWithID:(NSString *)userID
          andCompletion:(VAAPICompletionBlock)completion;
- (void)unblockUserWithID:(NSString *)userID
            andCompletion:(VAAPICompletionBlock)completion;

- (void)sendReportAboutUserWithID:(NSString *)userID
                    andReportType:(NSString *)reportType
                        andTarget:(NSString *)target
                        andPostId:(NSString *)postId
                     andCommentId:(NSString *)commentId
                    andCompletion:(VAAPICompletionBlock)completion;

- (void)getUserProfileWithCompletion:(VAAPICompletionBlock)completion;
- (void)getViewedUsersWithCompletion:(VAAPICompletionBlock)completion;
- (void)clearViewedUsersWithCompletion:(VAAPICompletionBlock)completion;

- (void)addFriendsFromEmailList:(NSString *)emailList
                 withCompletion:(VAAPICompletionBlock)completion;

- (void)restoreOriginalEmailWithCompletion:(VAAPICompletionBlock)completion;
- (void)removeUserWithId:(NSString *)userId
          withCompletion:(VAAPICompletionBlock)completion;

- (void)checkUsername:(NSString *)username
       withCompletion:(VAAPICompletionBlock)completion;


@end
