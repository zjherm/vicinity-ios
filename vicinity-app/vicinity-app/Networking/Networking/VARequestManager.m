//
//  UILabel+VACustomFont.h
//  vicinity-app
//
//  Created by Evgeny Dedovets on 02/06/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VARequestManager.h"

#import "AFHTTPSessionManager.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "VAUserDefaultsHelper.h"
#import "AFHTTPRequestOperationManager.h"

NSString *const VACoreResponseManagerAcceptJSONHeader = @"application/json";
NSString *const VACoreRequestManagerAcceptBodyHeader = @"application/x-www-form-urlencoded";

NSInteger const VACoreRequestTimeoutInterval = 30.0f;

@interface VARequestManager ()

@property (nonatomic) AFHTTPSessionManager *httpRequestManager;
@property (nonatomic) AFHTTPRequestOperationManager *httpRequestOperationManager;

@end

@implementation VARequestManager

#pragma mark - Public API

+ (VARequestManager*)sharedManager {
    static dispatch_once_t pred;
    static VARequestManager *sharedInstance = nil;
    
    dispatch_once(&pred, ^{
        sharedInstance = [[VARequestManager alloc] init];
        sharedInstance.multipartEnabled = NO;
        [sharedInstance addObserver:sharedInstance forKeyPath:@"multipartEnabled" options:0 context:nil];
        [sharedInstance configureAFNetworking];
        
    });
    
    return sharedInstance;
}

- (void)fetchURL:(NSURL *)url withMethod:(VAHttpRequestMethod)requestMethod withParamsOrNil:(id)params requiresAuth:(BOOL)shouldAuth constructingBodyWithBlock:(VAConstructingBodyBlock)constructingBlock withCompletionBlock:(VACoreRequestCompletionBlock)completion {
    
    NSAssert([NSThread isMainThread], @"-fetchURL called using a thread other than main!");
    NSMutableDictionary* parameters = [params mutableCopy];
    if (shouldAuth) {
        [self setAuthorizationHttpHeader];
    }
    
    NSAssert(url, @"CPCoreRequestManager empty request method specified on model class.");
    
    switch (requestMethod) {
        case VAHttpRequestTypeGet:
            
            [self performGetOnEndpoint:[url absoluteString] withParamsOrNil:parameters withCompletionBlock:completion];
            break;
        case VAHttpRequestTypePost:
            
            [self performPostOnEndpoint:[url absoluteString] withParamsOrNil:parameters withCompletionBlock:completion];
            break;
        case VAHttpRequestTypeDelete:
            
            [self performDeleteOnEndpoint:[url absoluteString] withParamsOrNil:parameters withCompletionBlock:completion];
            break;
        case VAHttpRequestTypePatch:
            
            [self performPatchOnEndpoint:[url absoluteString] withParamsOrNil:parameters withCompletionBlock:completion];
            break;
        case VAHttpRequestTypePut:
            
            [self performPutOnEndpoint:[url absoluteString] withParamsOrNil:parameters withCompletionBlock:completion];
            break;
        case VAMultipartRequestTypePost:
            
            [self performMultipartPostOnEndpoint:[url absoluteString] withParamsOrNil:parameters constructingBodyWithBlock:constructingBlock withCompletionBlock:completion];
            break;
        case VAMultipartRequestTypePut:
            
            [self performMultipartPutOnEndpoint:[url absoluteString] withParamsOrNil:parameters constructingBodyWithBlock:constructingBlock withCompletionBlock:completion];
            break;
        default:
            
            NSAssert(NO, @"CPCoreRequestManager invalid request method specified on model class.");
            break;
    }

}

#pragma mark - Internal Request methods

- (void)performGetOnEndpoint:(NSString *)endpointString withParamsOrNil:(NSDictionary *)parameters withCompletionBlock:(VACoreRequestCompletionBlock)completion {
    
    [self.httpRequestManager GET:endpointString parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        [self handleSuccessResponse:responseObject task:task completionBlock:completion];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        // Failure
        [self handleFailureWithError:error withCompletionBlock:completion];
    }];
}

- (void)performPostOnEndpoint:(NSString *)endpointString withParamsOrNil:(NSDictionary *)parameters withCompletionBlock:(VACoreRequestCompletionBlock)completion {
    
    [self.httpRequestOperationManager POST:endpointString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self handleSuccessResponse:responseObject task:nil completionBlock:completion];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self handleFailureWithError:error withCompletionBlock:completion];
    }];
}

- (void)performDeleteOnEndpoint:(NSString *)endpointString withParamsOrNil:(NSDictionary *)parameters withCompletionBlock:(VACoreRequestCompletionBlock)completion {
    
    [self.httpRequestOperationManager DELETE:endpointString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self handleSuccessResponse:responseObject task:nil completionBlock:completion];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self handleFailureWithError:error withCompletionBlock:completion];
    }];
}

- (void)performPatchOnEndpoint:(NSString *)endpointString withParamsOrNil:(NSDictionary *)parameters withCompletionBlock:(VACoreRequestCompletionBlock)completion {
    
    [self.httpRequestManager PATCH:endpointString parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        [self handleSuccessResponse:responseObject task:task completionBlock:completion];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self handleFailureWithError:error withCompletionBlock:completion];
    }];
}

- (void)performPutOnEndpoint:(NSString *)endpointString withParamsOrNil:(NSDictionary *)parameters withCompletionBlock:(VACoreRequestCompletionBlock)completion {
    
    [self.httpRequestOperationManager PUT:endpointString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self handleSuccessResponse:responseObject task:nil completionBlock:completion];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self handleFailureWithError:error withCompletionBlock:completion];
    }];
}

- (void)performMultipartPostOnEndpoint:(NSString *)endpointString withParamsOrNil:(NSDictionary *)parameters constructingBodyWithBlock:(VAConstructingBodyBlock)constructingBlock withCompletionBlock:(VACoreRequestCompletionBlock)completion {
    
    [self.httpRequestOperationManager POST:endpointString parameters:parameters
                  constructingBodyWithBlock:constructingBlock
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              [self handleSuccessResponse:responseObject task:responseObject completionBlock:completion];
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              [self handleFailureWithError:error withCompletionBlock:completion];
    }];
}

- (void)performMultipartPutOnEndpoint:(NSString *)endpointString withParamsOrNil:(NSDictionary *)parameters constructingBodyWithBlock:(VAConstructingBodyBlock)constructingBlock withCompletionBlock:(VACoreRequestCompletionBlock)completion {
    
    NSError *error;
    NSURLRequest *request = [self.httpRequestOperationManager.requestSerializer multipartFormRequestWithMethod:@"PUT"
                                                                                                     URLString:endpointString
                                                                                                    parameters:parameters
                                                                                     constructingBodyWithBlock:constructingBlock
                                                                                                         error:&error];
    
    AFHTTPRequestOperation *operation = [self.httpRequestOperationManager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self handleSuccessResponse:responseObject task:responseObject completionBlock:completion];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self handleFailureWithError:error withCompletionBlock:completion];
    }];
    
    [self.httpRequestOperationManager.operationQueue addOperation:operation];
}

#pragma mark - Convenience methods

- (void)handleSuccessResponse:(id)responseObject task:(NSURLSessionDataTask *)task completionBlock:(VACoreRequestCompletionBlock)completion {
    
    [self callCompletion:nil response:responseObject completionBlock:completion];
}

- (void)handleFailureWithError:(NSError *)error withCompletionBlock:(VACoreRequestCompletionBlock)completion {
    
    [self callCompletion:error response:nil completionBlock:completion];
}

- (void)setAuthorizationHttpHeader {
    
    // Clear the previous header
    [self.httpRequestManager.requestSerializer clearAuthorizationHeader];
    
    NSString *authHeaderString = [VAUserDefaultsHelper getAuthToken];

    if ([authHeaderString length]) {
        [self.httpRequestManager.requestSerializer setValue:authHeaderString forHTTPHeaderField:@"Authorization"];
        [self.httpRequestOperationManager.requestSerializer setValue:authHeaderString forHTTPHeaderField:@"Authorization"];
    }
}

- (void)configureAFNetworking {
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = VACoreRequestTimeoutInterval;
    
    self.httpRequestManager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:sessionConfig];
    self.httpRequestManager.requestSerializer = [AFJSONRequestSerializer serializer];
    [self.httpRequestManager.requestSerializer setValue:VACoreRequestManagerAcceptBodyHeader forHTTPHeaderField:@"Accept"];
    self.httpRequestManager.requestSerializer.HTTPMethodsEncodingParametersInURI = [NSSet setWithObjects:@"GET", nil];
    self.httpRequestManager.responseSerializer = [AFJSONResponseSerializer serializer];
    self.httpRequestManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:VACoreResponseManagerAcceptJSONHeader];
    
    self.httpRequestOperationManager = [AFHTTPRequestOperationManager manager];
    self.httpRequestOperationManager.responseSerializer = [AFJSONResponseSerializer serializer];
    self.httpRequestOperationManager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
}

// Ensures that the completion block is always called on the main thread back to the UI

- (void)callCompletion:(NSError *)error response:(id)jsonResponse completionBlock:(VACoreRequestCompletionBlock)completion {
    
    if (completion) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(error,jsonResponse);
        });
    }
}

@end
