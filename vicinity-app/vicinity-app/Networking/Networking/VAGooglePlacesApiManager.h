//
//  VAGooglePlacesApiManager.h
//  vicinity-app
//
//  Created by Panda Systems on 10/27/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VAPlacePhoto;

@interface VAGooglePlacesApiManager : NSObject

+ (instancetype)sharedManager;

- (void)getDetailsForPlaceWithId:(NSString *)placeId
                      completion:(void (^)(NSError *error, id responseObject))completion;
- (void)getPlacesInBoundsWithCenterLatitude:(NSNumber *)latitude
                            centerLongitude:(NSNumber *)longitude
                                     radius:(NSNumber *)radius
                                 completion:(void (^)(NSError *error, id responseObject, NSString *nextPageToken))completion;
- (void)getPlacesWithPageToken:(NSString *)pageToken
                    completion:(void (^)(NSError *error, id responseObject, NSString *nextPageToken))completion;
- (void)getPlacesTipsWithCenterLatitude:(NSNumber *)latitude
                        centerLongitude:(NSNumber *)longitude
                                 radius:(NSNumber *)radius
                                keyword:(NSString *)keyword
                             completion:(void (^)(NSError *error, id responseObject))completion;

- (NSURL *)downloadUrlForPhoto:(VAPlacePhoto *)photo;

@end
