//
//  VAGooglePlacesApiManager.m
//  vicinity-app
//
//  Created by Panda Systems on 10/27/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAGooglePlacesApiManager.h"

#import "VARequestManager.h"

#import "VAPlaceDetails.h"
#import "VAPlacePhoto.h"

// API key
static NSString *const kGoogleApiKey = @"AIzaSyBmc-f8xb6Mhvnpc0uX5O_pfxKBC734pJo";
// Base URL
static NSString *const kGoogleApiBaseURL = @"https://maps.googleapis.com/maps/api";
// Endpoints
static NSString *const kPlaceDetailsURL = @"/place/details";
static NSString *const kNearbyPlacesURL = @"/place/nearbysearch";
static NSString *const kAutocompletePlacesURL = @"/place/autocomplete";
static NSString *const kPlacePhotoURL = @"/place/photo";
// Data format
static NSString *const kJsonResponseTypeURL = @"/json";

@interface VAGooglePlacesApiManager ()
@property (nonatomic, strong) NSString *googleApiKey;
@end

@implementation VAGooglePlacesApiManager

+ (instancetype)sharedManager {
    static dispatch_once_t pred;
    static VAGooglePlacesApiManager *sharedInstance = nil;
    
    dispatch_once(&pred, ^{
        sharedInstance = [[VAGooglePlacesApiManager alloc] init];
        if (sharedInstance) {
            sharedInstance.googleApiKey = kGoogleApiKey;
        }
    });
    
    return sharedInstance;
}

- (void)getDetailsForPlaceWithId:(NSString *)placeId
                      completion:(void (^)(NSError *error, id responseObject))completion {
    NSDictionary *params = @{@"placeid": placeId,
                             @"key": self.googleApiKey};
    
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", kGoogleApiBaseURL, kPlaceDetailsURL, kJsonResponseTypeURL]];
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:params
                                  requiresAuth:NO
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   if ([jsonResponseObject[@"status"] isEqualToString:@"OK"]) {
                                       NSError *serializationError = nil;
                                       VAPlaceDetails *placeDetails = [MTLJSONAdapter modelOfClass:[VAPlaceDetails class]
                                                                                fromJSONDictionary:jsonResponseObject[@"result"]
                                                                                             error:&serializationError];
                                       if (serializationError) {
                                           completion(serializationError, nil);
                                       } else {
                                           completion(nil, placeDetails);
                                       }
                                   } else {
                                       completion([NSError errorWithDomain:kGoogleApiBaseURL code:100 userInfo:@{}], nil);
                                   }
                               }
                           }];
}

- (void)getPlacesInBoundsWithCenterLatitude:(NSNumber *)latitude
                            centerLongitude:(NSNumber *)longitude
                                     radius:(NSNumber *)radius
                      completion:(void (^)(NSError *error, id responseObject, NSString *nextPageToken))completion {
    NSDictionary *params = @{@"location": [NSString stringWithFormat:@"%@,%@", latitude, longitude],
//                             @"radius": [radius stringValue],
                             @"rankby": @"distance",
                             @"types": [self placesTypesString],
                             @"key": self.googleApiKey};
    
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", kGoogleApiBaseURL, kNearbyPlacesURL, kJsonResponseTypeURL]];
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:params
                                  requiresAuth:NO
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil, nil);
                               } else {
                                   if ([jsonResponseObject[@"status"] isEqualToString:@"OK"]) {
                                       NSError *serializationError = nil;
                                       NSArray *places = [MTLJSONAdapter modelsOfClass:[VAPlaceDetails class]
                                                                         fromJSONArray:jsonResponseObject[@"results"]
                                                                                 error:&serializationError];
                                       if (serializationError) {
                                           completion(serializationError, nil, nil);
                                       } else {
                                           completion(nil, places, jsonResponseObject[@"next_page_token"]);
                                       }
                                   } else {
                                       completion([NSError errorWithDomain:kGoogleApiBaseURL code:100 userInfo:@{}], nil, nil);
                                   }
                               }
                           }];
}

- (void)getPlacesWithPageToken:(NSString *)pageToken
                    completion:(void (^)(NSError *error, id responseObject, NSString *nextPageToken))completion {
    NSDictionary *params = @{@"pagetoken": pageToken,
                             @"key": self.googleApiKey};
    
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", kGoogleApiBaseURL, kNearbyPlacesURL, kJsonResponseTypeURL]];
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:params
                                  requiresAuth:NO
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil, nil);
                               } else {
                                   if ([jsonResponseObject[@"status"] isEqualToString:@"OK"]) {
                                       NSError *serializationError = nil;
                                       NSArray *places = [MTLJSONAdapter modelsOfClass:[VAPlaceDetails class]
                                                                         fromJSONArray:jsonResponseObject[@"results"]
                                                                                 error:&serializationError];
                                       if (serializationError) {
                                           completion(serializationError, nil, nil);
                                       } else {
                                           completion(nil, places, jsonResponseObject[@"next_page_token"]);
                                       }
                                   } else {
                                       completion([NSError errorWithDomain:kGoogleApiBaseURL code:100 userInfo:@{}], nil, nil);
                                   }
                               }
                           }];
}

- (void)getPlacesTipsWithCenterLatitude:(NSNumber *)latitude
                        centerLongitude:(NSNumber *)longitude
                                 radius:(NSNumber *)radius
                                keyword:(NSString *)keyword
                             completion:(void (^)(NSError *error, id responseObject))completion {
    NSDictionary *params = @{@"location": [NSString stringWithFormat:@"%@,%@", latitude, longitude],
                             @"radius": [radius stringValue],
                             @"input": keyword,
                             @"key": self.googleApiKey};
    
    NSURL *request_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", kGoogleApiBaseURL, kAutocompletePlacesURL, kJsonResponseTypeURL]];
    [[VARequestManager sharedManager] fetchURL:request_url
                                    withMethod:VAHttpRequestTypeGet
                               withParamsOrNil:params
                                  requiresAuth:NO
                     constructingBodyWithBlock:nil
                           withCompletionBlock:^(NSError *error, id jsonResponseObject) {
                               if (error) {
                                   completion(error, nil);
                               } else {
                                   if ([jsonResponseObject[@"status"] isEqualToString:@"OK"]) {
                                       __block NSUInteger numberOfPlaces = [jsonResponseObject[@"predictions"] count];
                                       
                                       NSMutableArray* placesArray = [NSMutableArray array];
                                       if (numberOfPlaces == 0) {
                                           completion(nil, @[]);
                                           return;
                                       }
                                       
                                       for (NSDictionary* result in jsonResponseObject[@"predictions"]) {
                                           [self getDetailsForPlaceWithId:result[@"place_id"] completion:^(NSError *error, id responseObject) {
                                               if (error != nil) {
                                               } else {
                                                   if (responseObject != nil) {
                                                       [placesArray addObject:responseObject];
                                                       
                                                   } else {
                                                       
                                                   }
                                               }
                                               numberOfPlaces--;
                                               if (numberOfPlaces <= 0) {
                                                   completion(nil, placesArray);
                                               }
                                           }];
                                       }
                                   } else if ([jsonResponseObject[@"status"] isEqualToString:@"ZERO_RESULTS"]) {
                                       completion(nil, [NSArray array]);
                                   } else {
                                       completion([NSError errorWithDomain:kGoogleApiBaseURL code:100 userInfo:@{}], nil);
                                   }
                               }
                           }];
}

- (NSURL *)downloadUrlForPhoto:(VAPlacePhoto *)photo {
    NSString *photoUrlString = [NSString stringWithFormat:@"%@%@?key=%@&photoreference=%@&maxwidth=1000&maxheight=1000", kGoogleApiBaseURL, kPlacePhotoURL, self.googleApiKey, photo.photoReference];
    return [NSURL URLWithString:photoUrlString];
}

-(NSString *)placesTypesString {
    NSMutableString *typesString = [[NSMutableString alloc] initWithString:@""];
    for (int i = 0; i < [self placesTypes].count; i++) {
        if (i > 0) {
            [typesString appendString:@"|"];
        }
        [typesString appendString:[self placesTypes][i]];
    }
    return typesString;
}

- (NSArray *)placesTypes {
    return @[@"airport",
             @"art_gallery",
             @"atm",
             @"bakery",
             @"bank",
             @"bar",
             @"beauty_salon",
             @"book_store",
             @"cafe",
             @"campground",
             @"casino",
             @"cemetery",
             @"church",
             @"city_hall",
             @"clothing_store",
             @"courthouse",
             @"embassy",
             @"food",
             @"gym",
//             @"health",
             @"library",
             @"lodging",
             @"meal_delivery",
             @"meal_takeaway",
             @"movie_theater",
             @"museum",
             @"night_club",
             @"park",
             @"place_of_worship",
             @"restaurant",
             @"shopping_mall",
             @"spa",
             @"stadium",
             @"store",
             @"university",
             @"veterinary_care",
             @"zoo"];
}

@end
