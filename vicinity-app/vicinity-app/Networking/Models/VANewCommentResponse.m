//
//  VAEditCommentResponse.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 8/17/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VANewCommentResponse.h"
#import "VAComment.h"

@implementation VANewCommentResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    // model_property_name : json_field_name
    return @{
             @"comment": @"data",
             @"status": @"status",
             @"error": @"error",
             };
}

+ (NSValueTransformer *)commentJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:[VAComment class]];
}

@end
