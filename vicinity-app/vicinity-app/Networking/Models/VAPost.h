//
//  VAPost.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 09.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAModel.h"
#import "VALocation.h"
#import "VAUser.h"
#import "VACoordinate.h"

@class VALike;
@class VAComment;

@interface VAPost : VAModel
@property (strong, nonatomic) VALocation *checkIn;
@property (strong, nonatomic) VAUser *user;
@property (strong, nonatomic) VACoordinate *location;
@property (strong, nonatomic) NSString *postID;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSString *photoURL;
@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSNumber *likesAmount;
@property (strong, nonatomic) NSNumber *commentsAmount;
@property (strong, nonatomic) NSArray *commentsArray;
@property (strong, nonatomic) NSNumber *isLiked;
@property (strong, nonatomic) NSNumber *isViewed;
@property (strong, nonatomic) NSNumber *isCheckedIn;
@property (strong, nonatomic) NSNumber *isPostedOnFacebook;
@property (strong, nonatomic) NSNumber *isSubscribed;
@property (strong, nonatomic) NSArray *subscribers;
@property (strong, nonatomic) NSNumber *isLandscapePhoto;

@end
