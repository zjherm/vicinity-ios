//
//  VANotificationsResponse.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 10.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAServerResponse.h"

@interface VANotificationsResponse : VAServerResponse
@property (strong, nonatomic) NSArray *notifications;
@end
