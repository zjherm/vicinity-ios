//
//  VALoginResponse.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 02.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAServerResponse.h"
#import "VAUser.h"

@interface VALoginResponse : VAServerResponse
@property (nonatomic) VAUser *loggedUser;
@property (nonatomic) NSString *sessionId;
@end
