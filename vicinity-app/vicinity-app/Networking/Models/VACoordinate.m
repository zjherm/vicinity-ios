//
//  VACoordinate.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 04.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VACoordinate.h"

@implementation VACoordinate

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    // model_property_name : json_field_name
    return @{
             @"latitude": @"lat",
             @"longitude": @"lon",
             };
}

@end
