//
//  VADay.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 8/21/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAModel.h"

@interface VADay : VAModel
@property (strong, nonatomic) NSNumber *dayIndex;
@property (strong, nonatomic) NSString *openHours;
@property (strong, nonatomic) NSString *closeHours;
@property (strong, nonatomic) NSString *name;
@end
