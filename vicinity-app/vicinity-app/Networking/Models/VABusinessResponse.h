//
//  VACompanyResponse.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 8/21/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAServerResponse.h"

@class VABusiness;

@interface VABusinessResponse : VAServerResponse
@property (strong, nonatomic) NSArray *businesses;
@end
