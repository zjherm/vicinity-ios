//
//  VALikesResponse.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 23.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAServerResponse.h"

@interface VALikesResponse : VAServerResponse
@property (strong, nonatomic) NSArray *usersWhoLikes;
@end
