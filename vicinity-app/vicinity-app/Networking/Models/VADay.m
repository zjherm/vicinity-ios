//
//  VADay.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 8/21/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VADay.h"

@implementation VADay

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    // model_property_name : json_field_name
    return @{
             @"dayIndex": @"open.day",
             @"openHours": @"open.time",
             @"closeHours": @"close.time"
             };
}

@end
