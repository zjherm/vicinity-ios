//
//  VADealsResponse.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 8/31/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VADealsResponse.h"
#import "VADeal.h"

@implementation VADealsResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    // model_property_name : json_field_name
    return @{
             @"deals": @"data",
             @"status": @"status",
             @"error": @"error",
             };
}

+ (NSValueTransformer *)dealsJSONTransformer {
    return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:[VADeal class]];
}

@end
