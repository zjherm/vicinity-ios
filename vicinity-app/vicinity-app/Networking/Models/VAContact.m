//
//  VAContact.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/23/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAContact.h"

@implementation VAContact

- (instancetype)initWithDict:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        _firstName = dict[@"firstName"];
        _lastName = dict[@"lastName"];
        _companyName = dict[@"companyName"];
        _imageData = dict[@"avatar"];
        _phones = dict[@"phones"];
        _emails = dict[@"emails"];
    }
    return self;
}

@end
