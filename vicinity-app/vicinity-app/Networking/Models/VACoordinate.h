//
//  VACoordinate.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 04.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAModel.h"

@interface VACoordinate : VAModel
@property (strong, nonatomic) NSNumber *latitude;
@property (strong, nonatomic) NSNumber *longitude;
@end
