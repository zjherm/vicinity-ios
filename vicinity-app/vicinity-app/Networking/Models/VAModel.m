//
//  UILabel+VACustomFont.h
//  vicinity-app
//
//  Created by Evgeny Dedovets on 02/06/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAModel.h"

@implementation VAModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    
    NSAssert(NO, @"VMGrModel does not provide concrete implementation, child classes must implement protocol");
    return nil;
}

+ (NSDateFormatter *)dateFormatter {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:enUSPOSIXLocale];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZZ"];
    return dateFormatter;
}
@end
