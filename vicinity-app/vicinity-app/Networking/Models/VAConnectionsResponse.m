//
//  VAConnectionsResponse.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/22/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAConnectionsResponse.h"
#import "VAConnection.h"

@implementation VAConnectionsResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    // model_property_name : json_field_name
    return @{
             @"connections": @"data",
             @"status": @"status",
             @"error": @"error",
             };
}

+ (NSValueTransformer *)connectionsJSONTransformer {
    return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:[VAConnection class]];
}

@end
