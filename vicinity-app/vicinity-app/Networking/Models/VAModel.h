//
//  UILabel+VACustomFont.h
//  vicinity-app
//
//  Created by Evgeny Dedovets on 02/06/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface VAModel : MTLModel<MTLJSONSerializing>

+ (NSDateFormatter *)dateFormatter;

@end
