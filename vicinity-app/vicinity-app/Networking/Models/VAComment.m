//
//  VAComment.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 10.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAComment.h"
#import "VAUser.h"

@implementation VAComment

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"user": @"user",
             @"commentID": @"_id",
             @"text": @"text",
             @"date": @"ts",
             @"status": @"status"
             };
}

+ (NSValueTransformer *)userJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:[VAUser class]];
}

+ (NSValueTransformer *)dateJSONTransformer {
//    return [MTLValueTransformer reversibleTransformerWithBlock:^(NSString *string) {
//        NSDate *date = [NSDate dateWithTimeIntervalSince1970:[string integerValue]];
//        return date;
//    }];
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id(NSNumber *dateString) {
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:[dateString integerValue]];
        return date;
    } reverseBlock:^id(NSDate *date) {
        return [NSNumber numberWithDouble:[date timeIntervalSince1970]];
    }];
}

- (VACommentStatus)commentStatus {
    if ([self.status isEqualToString:@"Active"]) {
        return VACommentStatusActive;
    } else if ([self.status isEqualToString:@"Removed"]) {
        return VACommentStatusRemoved;
    }
    return VACommentStatusDeleted;
}

@end
