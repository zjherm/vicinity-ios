//
//  VAConnectionsResponse.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/22/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAServerResponse.h"

@interface VAConnectionsResponse : VAServerResponse
@property (strong, nonatomic) NSArray *connections;

@end
