//
//  VAPostResponse.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 10.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAServerResponse.h"

@interface VAPostResponse : VAServerResponse
@property (strong, nonatomic) NSArray *posts;
@end
