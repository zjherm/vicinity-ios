//
//  VADeal.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 01.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

typedef NS_ENUM(NSInteger, VADealType) {
    VADealTypePaid = 0,
    VADealTypeFree,
    VADealTypeAd
};

#import "VAModel.h"

@class VACoordinate;
@class VABusiness;

@interface VADeal : VAModel
@property (strong, nonatomic) VACoordinate *coordinates;
@property (strong, nonatomic) VABusiness *business;
@property (strong, nonatomic) NSString *dealID;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSString *shortDescription;
@property (strong, nonatomic) NSString *photoURL;
@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSDate *expirationDate;
@property (strong, nonatomic) NSNumber *likesAmount;
@property (strong, nonatomic) NSNumber *commentsAmount;
@property (strong, nonatomic) NSArray *commentsArray;
@property (strong, nonatomic) NSNumber *isLiked;
@property (strong, nonatomic) NSNumber *isPurchased;
@property (strong, nonatomic) NSNumber *distance;
@property (strong, nonatomic) NSString *serialNumber;
@property (strong, nonatomic) NSString *price;
@property (assign, nonatomic) VADealType dealType;
@property (assign, nonatomic) BOOL isInConfirmPurchaseMode;
@end
