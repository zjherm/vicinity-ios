//
//  VAEditCommentResponse.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 8/17/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAServerResponse.h"

@class VAComment;

@interface VANewCommentResponse : VAServerResponse
@property (strong, nonatomic) VAComment *comment;
@end
