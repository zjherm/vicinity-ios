//
//  VAUserProfileResponse.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 8/19/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAUserProfileResponse.h"
#import "VAUser.h"

@implementation VAUserProfileResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"user": @"data",
             @"status": @"status",
             @"error": @"error",
             };
}

+ (NSValueTransformer *)userJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:[VAUser class]];
}

@end
