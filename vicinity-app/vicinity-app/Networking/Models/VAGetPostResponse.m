//
//  VAGetPostResponse.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 13.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAGetPostResponse.h"
#import "VAPost.h"

@implementation VAGetPostResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"post": @"data",
             @"status": @"status",
             @"error": @"error",
             };
}

+ (NSValueTransformer *)postJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:[VAPost class]];
}

@end
