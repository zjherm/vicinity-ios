//
//  VAContact.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/23/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VAContact : NSObject
@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSString *companyName;
@property (strong, nonatomic) NSData *imageData;
@property (strong, nonatomic) NSArray *emails;
@property (strong, nonatomic) NSArray *phones;

- (instancetype)initWithDict:(NSDictionary *)dict;
@end
