//
//  VANotification.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 10.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VANotification.h"
#import "VAUser.h"

@implementation VANotification

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"user": @"actor",
             @"notificationID": @"_id",
             @"text": @"text",
             @"date": @"ts",
             @"postID": @"postId",
             @"eventType": @"notifType",
             @"eventText": @"eventText",
             };
}

+ (NSValueTransformer *)userJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:[VAUser class]];
}

+ (NSValueTransformer *)dateJSONTransformer {
    return [MTLValueTransformer reversibleTransformerWithBlock:^(NSString *string) {
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:[string integerValue]];
        return date;
    }];
}

@end