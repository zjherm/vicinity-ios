//
//  VAComment.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 10.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAModel.h"

typedef NS_ENUM(NSUInteger, VACommentStatus) {
    VACommentStatusActive,  // Normal state
    VACommentStatusRemoved, // Comment deleted
    VACommentStatusDeleted  // Author deleted
};

@class VAUser;

@interface VAComment : VAModel
@property (strong, nonatomic) VAUser *user;
@property (strong, nonatomic) NSString *commentID;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSString *status;

@property (nonatomic, readonly) VACommentStatus commentStatus;
@end
