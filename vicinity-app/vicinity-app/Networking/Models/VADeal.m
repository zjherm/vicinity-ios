//
//  VADeal.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 01.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VADeal.h"
#import "VABusiness.h"
#import "VALocation.h"

@implementation VADeal

- (instancetype)init
{
    self = [super init];
    if (self) {
        _isInConfirmPurchaseMode = NO;
    }
    return self;
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"shortDescription": @"title",
             @"text": @"description",
             @"date": @"startTime",
             @"expirationDate": @"endTime",
             @"photoURL": @"imageUrl",
             @"dealType": @"type",
             @"price": @"price",
             @"coordinates": @"location",
             @"business": @"_company",
             @"distance": @"distance",
             };
}

+ (NSValueTransformer *)locationJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:[VALocation class]];
}

+ (NSValueTransformer *)businessJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:[VABusiness class]];
}

+ (NSValueTransformer *)dateJSONTransformer {
    return [MTLValueTransformer reversibleTransformerWithBlock:^(NSString *string) {
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:[string integerValue]];
        return date;
    }];
}

+ (NSValueTransformer *)expirationDateJSONTransformer {
    return [MTLValueTransformer reversibleTransformerWithBlock:^(NSString *string) {
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:[string integerValue]];
        return date;
    }];
}

@end