//
//  VALocation.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 04.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VALocation.h"
@import GoogleMaps;
#import "VAPlaceDetails.h"
#import "VAGoogleLocation.h"

@implementation VALocation

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    // model_property_name : json_field_name
    return @{
             @"distance": @"distance",
             @"rating": @"rating",
             @"name": @"name",
             @"coordinate": @"location",
             @"placeID": @"placeId",
             @"facebookPlaceID": @"facebookPlaceId",
             @"type": @"type",
             @"address": @"address",
             @"cityName": @"land",
             @"companyID": @"companyId",
             };
}

+ (NSValueTransformer *)coordinateJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:[VACoordinate class]];
}

-(instancetype)initWithGMSPlace:(GMSPlace*)place{
    self = [self init];
    
    self.name = place.name;
    self.rating = [NSNumber numberWithFloat:place.rating];
    self.cityName = @"";
    double lat = [[NSUserDefaults standardUserDefaults] doubleForKey:kLatValue];
    double lon = [[NSUserDefaults standardUserDefaults] doubleForKey:kLonValue];
    CLLocation* currentLocation = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
    CLLocation* placeLocation = [[CLLocation alloc] initWithLatitude:place.coordinate.latitude longitude:place.coordinate.longitude];
    self.distance = [NSNumber numberWithDouble:[currentLocation distanceFromLocation:placeLocation]];
    VACoordinate* coordinate = [[VACoordinate alloc] init];
    coordinate.latitude = [NSNumber numberWithDouble:place.coordinate.latitude];
    coordinate.longitude = [NSNumber numberWithDouble:place.coordinate.longitude];
    self.coordinate = coordinate;
    self.placeID = place.placeID;
    self.companyID = nil;
    self.type = [place.types firstObject];
    self.address = place.formattedAddress;
    
    return self;
}

+ (VALocation *)locationFromPlaceDetails:(VAPlaceDetails *)placeDetails
                            withDistance:(NSNumber *)distance {
    VALocation *location = [[VALocation alloc] init];
    location.distance = distance;
    location.rating = placeDetails.rating;
    location.name = placeDetails.name;
    location.cityName = nil;
    location.coordinate = [[VACoordinate alloc] init];
    location.coordinate.latitude = placeDetails.location.latitude;
    location.coordinate.longitude = placeDetails.location.longitude;
    location.placeID = placeDetails.placeId;
    location.facebookPlaceID = nil;
    location.companyID = nil;
    location.type = placeDetails.types.firstObject;
    location.address = placeDetails.vicinity;
    return location;
}

@end
