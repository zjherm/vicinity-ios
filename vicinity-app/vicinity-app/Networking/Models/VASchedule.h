//
//  VASchedule.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 8/21/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAModel.h"

@interface VASchedule : VAModel
@property (strong, nonatomic) NSNumber *isOpenedNow;
@property (strong, nonatomic) NSArray *days;
@end
