//
//  VAPostResponse.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 10.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAPostResponse.h"
#import "VAPost.h"

@implementation VAPostResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    // model_property_name : json_field_name
    return @{
             @"posts": @"data",
             @"status": @"status",
             @"error": @"error",
             };
}

+ (NSValueTransformer *)postsJSONTransformer {
    return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:[VAPost class]];
}

@end
