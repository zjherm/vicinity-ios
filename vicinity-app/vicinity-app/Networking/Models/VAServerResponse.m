//
//  VAServerResponse.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 15.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAServerResponse.h"
#import "VAControlTool.h"

@implementation VAServerResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    // model_property_name : json_field_name
    return @{
             @"status": @"status",
             @"error": @"error",
             };
}

+ (NSValueTransformer *)errorJSONTransformer {
    return [MTLValueTransformer reversibleTransformerWithBlock:^(NSDictionary *errorDict) {
        if (errorDict) {
            NSInteger code = 400;
            NSString* errorDomain = @"ErrorResponse";
            NSString *description = [errorDict objectForKey:@"code"];
            //if error is BAD_SESSION (user is not authorized) we should logout user from the app;
            if ([description isEqual:@(100004)]) {
                [[VAControlTool defaultControl] logout];
            } else if ([description isEqual:@(100019)]) {
                [[VAControlTool defaultControl] logoutForBlockedUser];
            } else if([description isEqual:@(100020)]) {
                [[VAControlTool defaultControl] showWelcomeScreen];
            }
            
            NSDictionary *userInfo = [NSDictionary dictionaryWithObject:description forKey:NSLocalizedDescriptionKey];
            NSError* error = [NSError errorWithDomain:errorDomain code:code userInfo:userInfo];
            return error;
        } else {
            return (NSError *)nil;
        }
    }];
}

@end
