//
//  VAPost.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 09.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAPost.h"
#import "VAComment.h"

@implementation VAPost

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    // model_property_name : json_field_name
    return @{
             @"checkIn": @"place",
             @"user": @"author",
             @"location": @"location",
             @"postID": @"_id",
             @"text": @"description",
             @"photoURL": @"photo",
             @"date": @"ts",
             @"likesAmount": @"likes",
             @"commentsAmount": @"commentsNum",
             @"commentsArray": @"comments",
             @"isLiked": @"isLiked",
             @"isViewed": @"viewed",
             @"isCheckedIn": @"isCheckedIn",
             @"isPostedOnFacebook": @"isPostedOnFacebook",
             @"isSubscribed": @"subscribed",
             @"subscribers": @"subscribers",
             @"isLandscapePhoto": @"isLandscapePhoto"
             };
}

+ (NSValueTransformer *)checkInJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:[VALocation class]];
}

+ (NSValueTransformer *)userJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:[VAUser class]];
}

+ (NSValueTransformer *)locationJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:[VACoordinate class]];
}

+ (NSValueTransformer *)dateJSONTransformer {
//    return [MTLValueTransformer reversibleTransformerWithBlock:^(NSString *string) {
//        NSDate *date = [NSDate dateWithTimeIntervalSince1970:[string integerValue]];
//        return date;
//    }];
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id(NSNumber *dateString) {
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:[dateString integerValue]];
        return date;
    } reverseBlock:^id(NSDate *date) {
        return [NSNumber numberWithDouble:[date timeIntervalSince1970]];
    }];
}

+ (NSValueTransformer *)commentsArrayJSONTransformer {
    return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:[VAComment class]];
}

@end