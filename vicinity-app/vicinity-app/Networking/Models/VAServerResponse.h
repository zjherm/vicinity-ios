//
//  VAServerResponse.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 15.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAModel.h"

@interface VAServerResponse : VAModel
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSError *error;
@end
