//
//  VACompanyResponse.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 8/21/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VABusinessResponse.h"
#import "VABusiness.h"

@implementation VABusinessResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"businesses": @"data",
             @"status": @"status",
             @"error": @"error",
             };
}

+ (NSValueTransformer *)businessesJSONTransformer {
    return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:[VABusiness class]];
}

@end
