//
//  VACommentsResponse.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 23.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VACommentsResponse.h"
#import "VAComment.h"

@implementation VACommentsResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    // model_property_name : json_field_name
    return @{
             @"comments": @"data",
             @"status": @"status",
             @"error": @"error",
             };
}

+ (NSValueTransformer *)commentsJSONTransformer {
    return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:[VAComment class]];
}

@end
