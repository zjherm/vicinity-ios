//
//  UILabel+VACustomFont.h
//  vicinity-app
//
//  Created by Evgeny Dedovets on 02/06/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAUser.h"

@implementation VAUser

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    // model_property_name : json_field_name
    return @{
             @"userId": @"userId",
             @"gender": @"gender",
             @"fullname": @"fullname",
             @"nickname": @"nickname",
             @"email": @"email",
             @"pendingEmail": @"pendingEmail",
             @"city": @"city",
             @"state": @"state",
             @"country": @"country",
             @"stripeId": @"stripeId",
             @"avatarURL": @"avatarUrl",
             @"bio": @"bio",
             @"isEmailConfirmed": @"isEmailConfirmed",
             @"facebookID": @"facebook.id",
             @"isFriend": @"isFriend",
             @"isBlocked": @"isBlocked",
             @"userStatus": @"userStatus"
             };
}

- (BOOL)isEqual:(id)object {
    if ([object isKindOfClass:[VAUser class]]) {
        return [self.userId isEqualToString:((VAUser *)object).userId];
    } else {
        return NO;
    }
}

- (NSUInteger)hash {
    return self.userId.hash;
}

- (VAUserStatus)status {
    if ([self.userStatus isEqualToString:@"Deleted"]) {
        return VAUserStatusDeleted;
    } else if ([self.userStatus isEqualToString:@"Blocked"]) {
        return VAUserStatusBlocked;
    } else if ([self.userStatus isEqualToString:@"Locked"]) {
        return VAUserStatusLocked;
    }
    return VAUserStatusNormal;
}

@end

