//
//  VAUserProfileResponse.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 8/19/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAServerResponse.h"

@class VAUser;

@interface VAUserProfileResponse : VAServerResponse
@property (strong, nonatomic) VAUser *user;
@end
