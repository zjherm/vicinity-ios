//
//  VANewPostResponse.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 19.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VANewPostResponse.h"
#import "VAPost.h"

@implementation VANewPostResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    // model_property_name : json_field_name
    return @{
             @"post": @"data",
             @"status": @"status",
             @"error": @"error",
             };
}

+ (NSValueTransformer *)postJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:[VAPost class]];
}

@end
