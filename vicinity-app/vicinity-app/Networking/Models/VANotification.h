//
//  VANotification.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 10.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAModel.h"

@class VAUser;

@interface VANotification : VAModel
@property (strong, nonatomic) VAUser *user;
@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSString *notificationID;
@property (strong, nonatomic) NSString *postID;
@property (strong, nonatomic) NSString *eventType;
@property (strong, nonatomic) NSString *eventText;

@end
