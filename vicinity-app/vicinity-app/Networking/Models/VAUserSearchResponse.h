//
//  VAUserSearchResponse.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 24.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAServerResponse.h"

@interface VAUserSearchResponse : VAServerResponse
@property (strong, nonatomic) NSArray *users;
@end
