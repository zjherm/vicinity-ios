//
//  VAUserSearchResponse.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 24.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAUserSearchResponse.h"
#import "VAUser.h"

@implementation VAUserSearchResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    // model_property_name : json_field_name
    return @{
             @"users": @"data",
             @"status": @"status",
             @"error": @"error",
             };
}

+ (NSValueTransformer *)usersJSONTransformer {
    return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:[VAUser class]];
}

@end
