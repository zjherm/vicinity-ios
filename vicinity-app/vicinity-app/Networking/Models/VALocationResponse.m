//
//  VALocationResponse.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 05.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VALocationResponse.h"
#import "VALocation.h"

@implementation VALocationResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    // model_property_name : json_field_name
    return @{
             @"nextPageToken": @"pagination.next_page_token",
             @"locations": @"data",
             @"status": @"status",
             @"error": @"error",
             };
}

+ (NSValueTransformer *)locationsJSONTransformer {
    return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:[VALocation class]];
}

@end
