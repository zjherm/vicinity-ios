//
//  VANotificationsResponse.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 10.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VANotificationsResponse.h"
#import "VANotification.h"

@implementation VANotificationsResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    // model_property_name : json_field_name
    return @{
             @"notifications": @"data",
             @"status": @"status",
             @"error": @"error",
             };
}

+ (NSValueTransformer *)notificationsJSONTransformer {
    return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:[VANotification class]];
}

@end
