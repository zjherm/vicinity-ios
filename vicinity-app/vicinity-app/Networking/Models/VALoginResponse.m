//
//  VALoginResponse.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 02.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VALoginResponse.h"

@implementation VALoginResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    // model_property_name : json_field_name
    return @{
             @"sessionId": @"data.sessionId",
             @"loggedUser": @"data.user",
             @"status": @"status",
             @"error": @"error",
             };
}

+ (NSValueTransformer *)loggedUserJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:[VAUser class]];
}

@end
