//
//  VALocation.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 04.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAModel.h"
#import "VACoordinate.h"
@class GMSPlace;
@class VAPlaceDetails;

@interface VALocation : VAModel
@property (strong, nonatomic) NSNumber *distance;
@property (strong, nonatomic) NSNumber *rating;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *cityName;
@property (strong, nonatomic) VACoordinate *coordinate;
@property (strong, nonatomic) NSString *placeID;
@property (strong, nonatomic) NSString *facebookPlaceID;
@property (strong, nonatomic) NSString *companyID;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *address;

-(instancetype)initWithGMSPlace:(GMSPlace*)place;
+ (VALocation *)locationFromPlaceDetails:(VAPlaceDetails *)placeDetails
                            withDistance:(NSNumber *)distance;

@end