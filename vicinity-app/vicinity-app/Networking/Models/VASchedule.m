//
//  VASchedule.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 8/21/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VASchedule.h"
#import "VADay.h"

@implementation VASchedule

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    // model_property_name : json_field_name
    return @{
             @"days": @"periods",
             @"isOpenedNow": @"openNow"
             };
}

+ (NSValueTransformer *)daysJSONTransformer {
    return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:[VADay class]];
}
@end
