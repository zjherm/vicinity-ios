//
//  VALikesResponse.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 23.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VALikesResponse.h"
#import "VAUser.h"

@implementation VALikesResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    // model_property_name : json_field_name
    return @{
             @"usersWhoLikes": @"data",
             @"status": @"status",
             @"error": @"error",
             };
}

+ (NSValueTransformer *)usersWhoLikesJSONTransformer {
    return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:[VAUser class]];
}

@end
