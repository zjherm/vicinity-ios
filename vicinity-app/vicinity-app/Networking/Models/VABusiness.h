//
//  VABusiness.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 01.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAModel.h"

@class VACoordinate;
@class VASchedule;

@interface VABusiness : VAModel
@property (strong, nonatomic) NSString *businessID;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *avatarURL;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *webSiteURL;
@property (strong, nonatomic) NSNumber *priceLevel;
@property (strong, nonatomic) NSString *googlePlaceID;
@property (strong, nonatomic) NSString *regionName;
@property (strong, nonatomic) VACoordinate *coordinates;
@property (strong, nonatomic) NSNumber *raiting;
@property (strong, nonatomic) NSNumber *votesCount;
@property (strong, nonatomic) NSString *phoneNumber;
@property (strong, nonatomic) VASchedule *schedule;
@property (strong, nonatomic) NSArray *photoURLsArray;

@end
