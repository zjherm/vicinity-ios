//
//  VADealsResponse.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 8/31/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAServerResponse.h"

@interface VADealsResponse : VAServerResponse
@property (strong, nonatomic) NSArray *deals;
@end