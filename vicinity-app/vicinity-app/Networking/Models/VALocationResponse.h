//
//  VALocationResponse.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 05.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAServerResponse.h"

@interface VALocationResponse : VAServerResponse
@property (strong, nonatomic) NSString *nextPageToken;
@property (strong, nonatomic) NSArray *locations;
@end
