//
//  UILabel+VACustomFont.h
//  vicinity-app
//
//  Created by Evgeny Dedovets on 02/06/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAModel.h"

typedef NS_ENUM(NSUInteger, VAUserStatus) {
    VAUserStatusNormal,
    VAUserStatusDeleted,
    VAUserStatusBlocked,
    VAUserStatusLocked
};

@interface VAUser : VAModel

@property (nonatomic) NSString *userId;
@property (nonatomic) NSString *gender;
@property (nonatomic) NSString *fullname;
@property (nonatomic) NSString *nickname;
@property (nonatomic) NSString *email;
@property (nonatomic) NSString *pendingEmail;
@property (nonatomic) NSString *city;
@property (nonatomic) NSString *state;
@property (nonatomic) NSString *country;
@property (nonatomic) NSString *avatarURL;
@property (nonatomic) NSString *stripeId;
@property (nonatomic) NSString *bio;
@property (nonatomic) NSString *facebookID;
@property (nonatomic) NSNumber *isEmailConfirmed;
@property (nonatomic) NSNumber *isFriend;
@property (nonatomic) NSNumber *isBlocked;
@property (nonatomic) NSString *userStatus;

@property (nonatomic, readonly) VAUserStatus status;

@end
