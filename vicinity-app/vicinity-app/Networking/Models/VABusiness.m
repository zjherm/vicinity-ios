//
//  VABusiness.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 01.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VABusiness.h"
#import "VALocation.h"
#import "VASchedule.h"

@implementation VABusiness

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"name": @"companyName",
             @"businessID": @"_id",
             @"avatarURL": @"avatarUrl",
             @"email": @"email",
             @"address": @"address",
             @"priceLevel": @"priceLevel",
             @"googlePlaceID": @"googlePlaceId",
             @"coordinates": @"location",
             @"raiting": @"rating",
             @"votesCount": @"userRatingsTotal",
             @"phoneNumber": @"phone",
             @"schedule": @"openingHours",
             @"photoURLsArray": @"galleryArray",
             @"webSiteURL": @"webSiteUrl",
             @"regionName": @"regionName"
             };
}

+ (NSValueTransformer *)coordinatesJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:[VACoordinate class]];
}

+ (NSValueTransformer *)scheduleJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:[VASchedule class]];
}

@end
