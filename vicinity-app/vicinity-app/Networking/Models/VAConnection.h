//
//  VAConnection.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/22/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAUser.h"

@interface VAConnection : VAUser
@property (assign, nonatomic) BOOL isInDisconnectMode;
@end
