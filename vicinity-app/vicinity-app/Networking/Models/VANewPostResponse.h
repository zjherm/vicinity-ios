//
//  VANewPostResponse.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 19.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAServerResponse.h"

@class VAPost;

@interface VANewPostResponse : VAServerResponse
@property (strong, nonatomic) VAPost *post;
@end
