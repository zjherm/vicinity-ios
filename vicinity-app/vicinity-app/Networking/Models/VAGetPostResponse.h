//
//  VAGetPostResponse.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 13.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAServerResponse.h"

@class VAPost;

@interface VAGetPostResponse : VAServerResponse
@property (strong, nonatomic) VAPost *post;
@end
