//
//  VAConnectionTableViewCell.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/22/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VAConnectionsProfileDelegate;

@interface VAConnectionTableViewCell : UITableViewCell
@property (weak, nonatomic) id <VAConnectionsProfileDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *avatarView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *nicknameLabel;
@property (weak, nonatomic) IBOutlet UIView *separatorView;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIButton *disconnectButton;
@property (weak, nonatomic) IBOutlet UIImageView *disconnectView;

@property (strong, nonatomic) NSIndexPath *indexPath;
@end

@protocol VAConnectionsProfileDelegate <NSObject>
- (void)userDidTapCellAtIndexPath:(NSIndexPath *)path;
@end