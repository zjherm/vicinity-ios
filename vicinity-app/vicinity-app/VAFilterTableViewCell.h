//
//  VAFilterTableViewCell.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 25.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VAFilterTableViewCell : UITableViewCell
@property (assign, nonatomic) BOOL hasCheckmark;

@property (weak, nonatomic) IBOutlet UIImageView *checkmarkImageView;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@end
