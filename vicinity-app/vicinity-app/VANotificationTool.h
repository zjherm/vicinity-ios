//
//  VANotificationTool.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 22.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <Foundation/Foundation.h>
extern NSString *const kDidChangeLocationStatus;
extern NSString *const kDidChangeUserAvatar;
extern NSString *const kDidUpdateLocaion;
extern NSString *const kUserDidRegisterForPushNotifications;
extern NSString *const kUserDidCreateAlias;
extern NSString *const kShowBadgeForMenuIcon;
extern NSString *const kHideBadgeForMenuIcon;
extern NSString *const kShowInviteScreen;
extern NSString *const kDidDisableLocationServices;
@interface VANotificationTool : NSObject

@end
