//
//  VAScrollView.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/11/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VAScrollView : UIScrollView
@property (assign, nonatomic) BOOL avoidScrolling;
@property (assign, nonatomic) CGFloat scrollingOffset;
@end
