//
//  VASignUpViewController.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/7/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VASignUpViewController.h"
#import "GPUImage.h"
#import "VAPhotoTool.h"
#import "VANotificationMessage.h"
#import "UIButton+VACustomFont.h"
#import "VADesignTool.h"
#import "VAGenderTypeView.h"
#import <Google/Analytics.h>
#import "VAControlTool.h"
#import "VAUserApiManager.h"
#import "VALoginResponse.h"
#import "VAUserDefaultsHelper.h"
#import "VAScrollView.h"
#import "VAStatePickerView.h"
#import "VACountryPickerView.h"
#import <SDWebImage/UIImageView+WebCache.h>

#import "VAConfirmEmailViewController.h"

@interface VASignUpViewController () <VAPhotoToolDelegate, VAGenderChangeDelegate, VAStatePickerDelegate, VACountryPickerDelegate, VAConfirmEmailDelegate>
@property (strong, nonatomic) UITextField *activeField;
@property (strong, nonatomic) UIActivityIndicatorView *indicator;
@property (strong, nonatomic) UIButton *pushedButton;
@property (strong, nonatomic) UIImage *updatedImage;

@property (weak, nonatomic) IBOutlet VAScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *avatarContainerView;
@property (weak, nonatomic) IBOutlet UIView *bottomViewContainer;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIImageView *avatarBackgroundImageView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomScrollViewConstraint;
@property (weak, nonatomic) IBOutlet UIButton *createButton;
@property (weak, nonatomic) IBOutlet UIButton *termsButton;
@property (weak, nonatomic) IBOutlet UIButton *privacyButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *profileViewHeightConstraint;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *textFieldsCollection;
@property (weak, nonatomic) IBOutlet UIButton *securePasswordButton;
@property (weak, nonatomic) IBOutlet VAGenderTypeView *genderTypeView;
@property (weak, nonatomic) IBOutlet UIImageView *nameIcon;
@property (weak, nonatomic) IBOutlet UIImageView *emailIcon;
@property (weak, nonatomic) IBOutlet UIImageView *usernameIcon;
@property (weak, nonatomic) IBOutlet UIImageView *passwordIcon;
@property (weak, nonatomic) IBOutlet UIImageView *locationIcon;
@property (weak, nonatomic) IBOutlet UIImageView *countryIcon;
@property (weak, nonatomic) IBOutlet UIView *stateMaskView;
@property (weak, nonatomic) IBOutlet UIView *countryMaskView;

@property (weak, nonatomic) IBOutlet UIView *addressSeparatorView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addressViewTrailingSpaceConstraint;

@property (strong, nonatomic) UIView *stateTapView;
@property (strong, nonatomic) UIView *countryTapView;
@property (strong, nonatomic) VAStatePickerView *statePickerView;
@property (strong, nonatomic) VACountryPickerView *countryPickerView;
@end

#define kAvatarImageCornerRadius 51.f
#define kPickerContainerHeight 162.f

@implementation VASignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.activeField = nil;
    self.indicator = nil;
    self.pushedButton = nil;
    self.updatedImage = nil;
    
    self.view.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapBackground = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboardAndPicker)];
    [self.view addGestureRecognizer:tapBackground];
    
    self.avatarImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapAvatar = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeAvatar)];
    [self.avatarImageView addGestureRecognizer:tapAvatar];
    
    UITapGestureRecognizer *stateViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(stateViewTap:)];
    [self.stateMaskView addGestureRecognizer:stateViewTap];
    
    UITapGestureRecognizer *countryViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(countryViewTap:)];
    [self.countryMaskView addGestureRecognizer:countryViewTap];
    
    self.stateTapView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight([UIScreen mainScreen].bounds) - 122.f, CGRectGetWidth([UIScreen mainScreen].bounds), 28.f)];
    self.stateTapView.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *stateTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(stateViewTap:)];
    [self.stateTapView addGestureRecognizer:stateTap];
    
    self.countryTapView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight([UIScreen mainScreen].bounds) - 122.f, CGRectGetWidth([UIScreen mainScreen].bounds), 28.f)];
    self.countryTapView.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *countryTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(countryViewTap:)];
    [self.countryTapView addGestureRecognizer:countryTap];
    
    [self.editButton setHidden:YES];
    
    [self.createButton setEnabled:NO];
    
    [self setPlaceholdersForTextFieldsFromArray:self.textFieldsCollection];
    
    CGFloat screenHeight = CGRectGetHeight([UIScreen mainScreen].bounds);
    if (screenHeight < 667.f) {       //height of iphone 6
        self.containerViewHeightConstraint.constant = 667.f  - 64.f - 143.f;
    } else {
        self.containerViewHeightConstraint.constant = CGRectGetHeight(self.view.bounds) - 64.f - 143.f;     //nav bar height and profile header view heigth
    }
    
    self.avatarContainerView.layer.borderWidth = 1.5f;
    self.avatarContainerView.layer.borderColor = [[VADesignTool defaultDesign] vicinityDarkBlue].CGColor;
    
    self.avatarContainerView.layer.cornerRadius = kAvatarImageCornerRadius;
    [self.avatarContainerView setClipsToBounds:YES];
    
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBarTintColor:[[VADesignTool defaultDesign] vicinityBlue]];
    [self.navigationController setTitle:@"Create Account"];
    
    self.navigationItem.leftBarButtonItem = [self backButton];
    
    [self updateAvatarImage:[UIImage imageNamed:@"avatar-placeholder.png"]];
    
    self.createButton.layer.cornerRadius = 5.f;
    self.createButton.clipsToBounds = YES;
    
    [self.privacyButton setUnderlinedText:@"Privacy Policy" withColor:[UIColor darkGrayColor]];
    [self.termsButton setUnderlinedText:@"Terms of Service" withColor:[UIColor darkGrayColor]];
    
    [self.scrollView setContentInset:UIEdgeInsetsMake(142.0, 0.0, 0.0, 0.0)];
    [self.scrollView setContentOffset:CGPointMake(0.0, 142.0)];
    
    [self.scrollView setScrollingOffset:(self.scrollView.contentOffset.y + 64.f)];
    
    self.genderTypeView.delegate = self;
    
    [self.navigationController.navigationBar setTranslucent:NO];
    
    [self setupPickerView];
    
    if (self.pendingUser) {
        [self setupFieldsForPendingUser];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Sign up screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    if ([[VANotificationMessage currentMessageController] notificationMessageIsShown]) {
        [[VANotificationMessage currentMessageController] hideCurrentMessage];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup UI

- (void)setupPickerView {
    self.statePickerView = [[VAStatePickerView alloc] init];
    self.statePickerView.stateDelegate = self;
    ((UITextField *)self.textFieldsCollection[5]).inputView = self.statePickerView;
    
    self.countryPickerView = [[VACountryPickerView alloc] init];
    self.countryPickerView.countryDelegate = self;
    ((UITextField *)self.textFieldsCollection[6]).inputView = self.countryPickerView;

}

- (void)setupFieldsForPendingUser {
    ((UITextField *)self.textFieldsCollection[0]).text = self.pendingUser.fullname;
    ((UITextField *)self.textFieldsCollection[1]).text = self.pendingUser.email;
    ((UITextField *)self.textFieldsCollection[2]).text = self.pendingUser.nickname;
    ((UITextField *)self.textFieldsCollection[3]).text = @"";
    
    NSArray *cityWords = [self.pendingUser.city componentsSeparatedByString:@", "];
    if ([cityWords count] > 0) {
        ((UITextField *)self.textFieldsCollection[4]).text = [cityWords firstObject];
    }
    if ([cityWords count] > 1) {
        ((UITextField *)self.textFieldsCollection[5]).text = cityWords[1];
        self.statePickerView.currentState = cityWords[1];
        
        self.addressSeparatorView.hidden = NO;
        ((UITextField *)self.textFieldsCollection[5]).hidden = NO;
        self.stateMaskView.hidden = NO;
        ((UITextField*)self.textFieldsCollection[4]).returnKeyType = UIReturnKeyNext;

        self.addressViewTrailingSpaceConstraint.constant = 0;
    } else if ([self.pendingUser.state length]) {
        ((UITextField *)self.textFieldsCollection[5]).text = self.pendingUser.state;
        self.statePickerView.currentState = self.pendingUser.state;
        
        self.addressSeparatorView.hidden = NO;
        ((UITextField *)self.textFieldsCollection[5]).hidden = NO;
        self.stateMaskView.hidden = NO;
        ((UITextField*)self.textFieldsCollection[4]).returnKeyType = UIReturnKeyNext;

        self.addressViewTrailingSpaceConstraint.constant = 0;
    }
    
    ((UITextField *)self.textFieldsCollection[6]).text = self.pendingUser.country;
    if ([self.pendingUser.country length]) {
        self.countryPickerView.currentCountry = self.pendingUser.country;
    }
    
    if ([self.pendingUser.gender isEqualToString:@"male"]) {
        [self.genderTypeView setGenderType:VAGenderTypeMale];
    } else if ([self.pendingUser.gender isEqualToString:@"female"]) {
        [self.genderTypeView setGenderType:VAGenderTypeFemale];
    } else {
        [self.genderTypeView setGenderType:VAGenderTypeUnknown];
    }
    
    if ([self.pendingUser.avatarURL length]) {
        [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:self.pendingUser.avatarURL]
                                placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                           if (image) {
                                               [self photoHasBeenEdited:image landscape:NO];
                                           }
                                       }];
    }
}

#pragma mark - Keyboard actions

- (void)willKeyboardShow:(NSNotification*)notification {
    
    NSDictionary* info = [notification userInfo];
    CGRect keyRect;
    [[info objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyRect];
    CGFloat height = keyRect.size.height;
    
    self.bottomScrollViewConstraint.constant = height;
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
    
//    CGRect rc = [self.activeField bounds];
//    rc = [self.activeField convertRect:rc toView:self.scrollView];
//    [self.scrollView scrollRectToVisible:rc animated:YES];
}

- (void)willKeyboardHide:(NSNotification*)notification {
    
    self.bottomScrollViewConstraint.constant = 0.0;
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - Actions

- (void)goBack {
    [self hideKeyboard];
    if (self.navigationController.presentingViewController) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    } else {
        [[VAControlTool defaultControl] logout];
    }
}

- (IBAction)addPhoto:(UIButton *)sender {
    [self changeAvatar];
}

- (IBAction)editPhoto:(UIButton *)sender {
    [self changeAvatar];
}

- (IBAction)showTermsOfService:(UIButton *)sender {
    NSString *urlString = @"http://getvicinityapp.com/terms/";
    NSURL *url = [NSURL URLWithString:urlString];
    [[UIApplication sharedApplication] openURL:url];
}

- (IBAction)showPrivacyPolicy:(UIButton *)sender {
    NSString *urlString = @"http://getvicinityapp.com/policy/";
    NSURL *url = [NSURL URLWithString:urlString];
    [[UIApplication sharedApplication] openURL:url];
}

- (IBAction)secureButtonPushed:(UIButton *)sender {
    UITextField *passwordField = self.textFieldsCollection[3];
    if (passwordField.isEditing) {
        [passwordField resignFirstResponder];
    } else {
        if (passwordField.secureTextEntry) {
            [passwordField setFont:[[VADesignTool defaultDesign] vicinityRegularFontOfSize:14.f]];
            [passwordField setSecureTextEntry:NO];
            [self.securePasswordButton setImage:[UIImage imageNamed:@"eye-icon-black"] forState:UIControlStateNormal];
        } else {
            [passwordField setSecureTextEntry:YES];
            [self.securePasswordButton setImage:[UIImage imageNamed:@"eye-icon-gray"] forState:UIControlStateNormal];
        }
    }
}

- (IBAction)createButtonPushed:(UIButton *)sender {
    self.pushedButton = sender;
    
    [self hideKeyboard];
    
    if (!self.updatedImage) {
        [VANotificationMessage showSetAvatarMessageOnController:self withDuration:5.f];
    } else {
        [self showLoaderFor:sender];
        [self sendCreateUserRequest];
    }
}

#pragma mark - Methods

- (void)changeAvatar {
    [self hideKeyboard];
    [self editAvatar];
}

- (void)updateAvatarImage:(UIImage *)image {
    self.avatarImageView.image = image;
    [self setBluredAvatarImage:image];
}

- (void)setBluredAvatarImage:(UIImage *)avatarImage {
    UIImage *bluredImage = [self gpuBlurApplyDarkEffect:avatarImage];
    
    self.avatarBackgroundImageView.image = bluredImage;
}

- (UIImage *)gpuBlurApplyDarkEffect:(UIImage*)image {
    UIImage *result = nil;
    
    GPUImageiOSBlurFilter *blurFilter = [[GPUImageiOSBlurFilter alloc] init];
    blurFilter.blurRadiusInPixels = 10;
    blurFilter.saturation = 1.2;
    result = [blurFilter imageByFilteringImage:image];
    
    GPUImageBrightnessFilter * brightnessFilter = [[GPUImageBrightnessFilter alloc] init];
    brightnessFilter.brightness = -0.1;
    result = [brightnessFilter imageByFilteringImage:result];
    
    return result;
}

- (void)hideKeyboard {
    [self.activeField resignFirstResponder];
}

- (void)hideKeyboardAndPicker {
    [self hideKeyboard];
}

- (void)editAvatar {
    self.navigationItem.leftBarButtonItem = nil;
    [VAPhotoTool defaultPhoto].delegate = self;
    [[VAPhotoTool defaultPhoto] showPhotoActionViewOnController:self];
}

- (void)setPlaceholdersForTextFieldsFromArray:(NSArray *)array {
    
    for (UITextField *textField in array) {
        NSAttributedString *loginPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{ NSForegroundColorAttributeName : [[UIColor whiteColor] colorWithAlphaComponent:0.7f]}];
        textField.attributedPlaceholder = loginPlaceholder;
    }
}

- (void)disableCreateButton {
    self.createButton.enabled = NO;
    self.createButton.backgroundColor = [[VADesignTool defaultDesign] separatorColor];
}

- (void)enableCreateButton {
    self.createButton.enabled = YES;
    self.createButton.backgroundColor = [[VADesignTool defaultDesign] vicinityBlue];
}

- (BOOL)allFieldsAreValid {
    if ([[self.textFieldsCollection firstObject] text].length < 3) {
        return NO;
    } else if ([self.textFieldsCollection[4] text].length < 3) {
        return NO;
    } else if ([[self.textFieldsCollection[6] text] isEqualToString:@""]) {
        return NO;
    } else if ([[self.textFieldsCollection[5] text] isEqualToString:@""] && [[self.textFieldsCollection[6] text] isEqualToString:@"United States"]) {
        return NO;
    } else if ([self.textFieldsCollection[2] text].length < 3) {
        return NO;
    }  else if (![[VAControlTool defaultControl] validEmailInTextField:self.textFieldsCollection[1]]) {
        return NO;
    } else if (![[VAControlTool defaultControl] validPasswordInTextField:self.textFieldsCollection[3]]) {
        return NO;
    } else {
        return YES;
    }
}

- (void)sendCreateUserRequest {
    VAUserApiManager *manager = [VAUserApiManager new];
    [manager postCreateUserWithEmail:[self.textFieldsCollection[1] text]
                            password:[self.textFieldsCollection[3] text]
                            fullname:[self.textFieldsCollection[0] text]
                            username:[self.textFieldsCollection[2] text]
                                city:[self.textFieldsCollection[4] text]
                               state:[self.textFieldsCollection[5] text]
                             country:[self.textFieldsCollection[6] text]
                              gender:nil//self.genderTypeView.genderType == VAGenderTypeFemale ? @"female" : @"male"
                         avatarImage:self.avatarImageView.image
                       andCompletion:^(NSError *error, VAModel *model) {
                           self.view.userInteractionEnabled = YES;
                           [self restoreButton:self.pushedButton];
                           
                           if (model) {
                               VALoginResponse *response = (VALoginResponse *)model;
                               VAUser *user = response.loggedUser;
                               [VAUserDefaultsHelper saveModel:user forKey:VASaveKeys_CurrentUser];
                               [VAUserDefaultsHelper setAuthToken:response.sessionId];
                               
                               VAConfirmEmailViewController *vc = [[UIStoryboard storyboardWithName:@"Vicinity" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"confirmEmailViewController"];
                               vc.isEmailUpdated = NO;
                               vc.delegate = self;
                               [self.navigationController pushViewController:vc animated:YES];
                               
                           } else if (error) {

                               if ([error.localizedDescription isEqual:@(100001)]) {
                                   [VANotificationMessage showEmailInUseMessageOnController:self forEmail:[self.textFieldsCollection[1] text] withDuration:5.f];
                                   [self.emailIcon setImage:[UIImage imageNamed:@"profile-email-icon-red"]];
                               } else if ([error.localizedDescription isEqual:@(100002)]) {
                                   [VANotificationMessage showUsernameInUseMessageOnController:self forUsername:[self.textFieldsCollection[2] text] withDuration:5.f];
                                   [self.usernameIcon setImage:[UIImage imageNamed:@"profile-username-icon-red"]];
                               } else {
                                   [VANotificationMessage showCreateAccountErrorMessageOnController:self withDuration:5.f];
                               }
                           }
                       }];
}

- (void)restoreButton:(UIButton *)button {
    self.view.userInteractionEnabled = YES;
    [self hideLoader];
    [UIView animateWithDuration:0.3f animations:^{
        button.alpha = 1.f;
    }];
}

- (UITextField *)closestClearTextFieldFrom:(UITextField *)textField {
    NSInteger index = [self.textFieldsCollection indexOfObject:textField];
    NSInteger nextIndex = index + 1 < [self.textFieldsCollection count] ? index + 1 : [self.textFieldsCollection count] - index - 1;
    if (index == 3) {
        nextIndex = 6;
    } else if (index == 6) {
        nextIndex = 4;
    }
    if (nextIndex == 5 && self.stateMaskView.hidden) {
        nextIndex = 0;
    }
    
    for (NSInteger i = 0; i < [self.textFieldsCollection count]; i++) {
        NSInteger nextIdx = nextIndex + i < [self.textFieldsCollection count] ? nextIndex + i : nextIndex + i - [self.textFieldsCollection count];
        if (nextIndex == 4) {
            nextIndex = 6;
        } else if (nextIndex == 7) {
            nextIndex = 4;
        }
        UITextField *nextField = self.textFieldsCollection[nextIdx];
        if ([nextField.text isEqualToString:@""]) {
            return nextField;
        }
    }
    return self.textFieldsCollection[nextIndex];
}

#pragma mark - BarButtonItems

- (UIBarButtonItem *)backButton {
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackButton"] style:UIBarButtonItemStylePlain target:self action:@selector(goBack)];
    
    return backButton;
}

#pragma mark - Loader

- (void)showLoaderFor:(UIButton *)button {
    self.view.userInteractionEnabled = NO;
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [activityIndicator setFrame:CGRectMake((CGRectGetWidth(button.frame) - CGRectGetWidth(activityIndicator.frame)) / 2, (CGRectGetHeight(button.frame) - CGRectGetHeight(activityIndicator.frame)) / 2, CGRectGetWidth(activityIndicator.frame), CGRectGetHeight(activityIndicator.frame))];
    activityIndicator.hidesWhenStopped = YES;
    self.indicator = activityIndicator;
    
    self.pushedButton = button;
    [UIView animateWithDuration:0.3f animations:^{
        button.alpha = 0.4f;
    }];
    
    [self.createButton addSubview:activityIndicator];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [activityIndicator startAnimating];
    });
}

- (void)hideLoader {
    [self.indicator stopAnimating];
    [self.indicator removeFromSuperview];
    self.indicator = nil;
}

#pragma mark - VAStatePickerDelegate methods

- (void)didSelectState:(NSString *)state withAbbreviation:(NSString *)abbreviation {
    UITextField *stateField = self.textFieldsCollection[5];
    stateField.text = abbreviation;
    
    if ([self allFieldsAreValid]) {
        [self enableCreateButton];
    } else {
        [self disableCreateButton];
    }
}

#pragma mark - VACountryPickerDelegate methods

- (void)didSelectCountry:(NSString *)country {
    UITextField *countryField = self.textFieldsCollection[6];
    countryField.text = country;
    
    if ([country isEqualToString:@"United States"]) {
        self.addressSeparatorView.hidden = NO;
        ((UITextField *)self.textFieldsCollection[5]).hidden = NO;
        self.stateMaskView.hidden = NO;
        ((UITextField*)self.textFieldsCollection[4]).returnKeyType = UIReturnKeyNext;

        self.addressViewTrailingSpaceConstraint.constant = 0;
        [UIView animateWithDuration:0.3f animations:^{
            [self.view layoutIfNeeded];
        }];
    } else {
        UITextField *stateField = self.textFieldsCollection[5];
        stateField.text = @"";
        self.statePickerView.currentState = nil;
        
        self.addressViewTrailingSpaceConstraint.constant = -([UIScreen mainScreen].bounds.size.width - 66.0f + 32.0f);
        
        [UIView animateWithDuration:0.3f animations:^{
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            self.addressSeparatorView.hidden = YES;
            ((UITextField *)self.textFieldsCollection[5]).hidden = YES;
            self.stateMaskView.hidden = YES;
            ((UITextField*)self.textFieldsCollection[4]).returnKeyType = UIReturnKeyDone;

        }];
    }
    
    if ([self allFieldsAreValid]) {
        [self enableCreateButton];
    } else {
        [self disableCreateButton];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (![textField isEqual:self.textFieldsCollection[5]]) {
        [self.stateTapView removeFromSuperview];
    } else if (![self.statePickerView.currentState length]) {
        self.statePickerView.currentState = @"";
    }
    
    if (![textField isEqual:self.textFieldsCollection[6]]) {
        [self.countryTapView removeFromSuperview];
    } else if (![self.countryPickerView.currentCountry length]) {
        self.countryPickerView.currentCountry = @"";
    }

    if (textField == self.textFieldsCollection[6] && !self.countryTapView.superview) {

        UIWindow *window = [UIApplication sharedApplication].windows.lastObject;
        [window addSubview:self.countryTapView];
        [window bringSubviewToFront:self.countryTapView];

    }

    if (textField == self.textFieldsCollection[5] && !self.stateTapView.superview) {

        UIWindow *window = [UIApplication sharedApplication].windows.lastObject;
        [window addSubview:self.stateTapView];
        [window bringSubviewToFront:self.stateTapView];
        
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (!self.updatedImage && [textField isEqual:self.textFieldsCollection[0]]) {
        self.avatarContainerView.layer.borderColor = [UIColor redColor].CGColor;
    }
    
    self.activeField = textField;
    
    if (textField.secureTextEntry) {
        [textField setFont:[[VADesignTool defaultDesign] vicinityRegularFontOfSize:14.f]];
    }
    
    if ([textField isEqual:[self.textFieldsCollection firstObject]]) {
        if (textField.text.length < 3  && [self.nameIcon.image isEqual:[UIImage imageNamed:@"profile-name-icon-red"]]) {
            [VANotificationMessage showFillInNameMessageOnController:self withDuration:5.f];
        }
    } else if ([textField isEqual:self.textFieldsCollection[1]]) {
        if (![[VAControlTool defaultControl] validEmailInTextField:self.textFieldsCollection[1]] && [self.emailIcon.image isEqual:[UIImage imageNamed:@"profile-email-icon-red"]]) {
            [VANotificationMessage showFillInEmailMessageOnController:self withDuration:5.f];
        }
    } else if ([textField isEqual:self.textFieldsCollection[2]] && [self.usernameIcon.image isEqual:[UIImage imageNamed:@"profile-username-icon-red"]]) {
        if ([self.textFieldsCollection[2] text].length == 0) {
            [VANotificationMessage showFillInAliasMessageOnController:self withDuration:5.f];
        }
    } else if ([textField isEqual:self.textFieldsCollection[3]] && [self.passwordIcon.image isEqual:[UIImage imageNamed:@"profile-password-red"]]) {
        if (![[VAControlTool defaultControl] validPasswordInTextField:self.textFieldsCollection[3]]) {
            [VANotificationMessage showInvalidPasswordMessageOnController:self withDuration:5.f];
        }
    } else if ([textField isEqual:self.textFieldsCollection[4]] && [self.locationIcon.image isEqual:[UIImage imageNamed:@"profile-location-red"]]) {
        if (textField.text.length < 3) {
            [VANotificationMessage showFillInCityMessageOnController:self withDuration:5.f];
        }
    } else if ([textField isEqual:self.textFieldsCollection[6]] && [self.countryIcon.image isEqual:[UIImage imageNamed:@"profile-country-icon-red"]]) {
        if (textField.text.length < 2) {
            [VANotificationMessage showFillInCountryMessageOnController:self withDuration:5.f];
        }
    }

}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.secureTextEntry) {
        [textField setFont:[[VADesignTool defaultDesign] vicinityRegularFontOfSize:14.f]];
    }
    
    if ([textField isEqual:[self.textFieldsCollection firstObject]]) {
        if (textField.text.length < 3) {
            [self.nameIcon setImage:[UIImage imageNamed:@"profile-name-icon-red"]];
        } else {
            [self.nameIcon setImage:[UIImage imageNamed:@"profile-name-icon-green"]];
        }
    } else if ([textField isEqual:self.textFieldsCollection[1]]) {
        if (![[VAControlTool defaultControl] validEmailInTextField:self.textFieldsCollection[1]]) {
            [self.emailIcon setImage:[UIImage imageNamed:@"profile-email-icon-red"]];
        } else {
            [self.emailIcon setImage:[UIImage imageNamed:@"profile-email-icon-green"]];
        }
    } else if ([textField isEqual:self.textFieldsCollection[2]]) {
        if ([self.textFieldsCollection[2] text].length == 0) {
            [self.usernameIcon setImage:[UIImage imageNamed:@"profile-username-icon-red"]];
        } else {
            [self.usernameIcon setImage:[UIImage imageNamed:@"profile-username-icon-green"]];
        }
    } else if ([textField isEqual:self.textFieldsCollection[3]]) {
        if (![[VAControlTool defaultControl] validPasswordInTextField:self.textFieldsCollection[3]]) {
            [self.passwordIcon setImage:[UIImage imageNamed:@"profile-password-red"]];
        } else {
            [self.passwordIcon setImage:[UIImage imageNamed:@"profile-password-green"]];
        }
    } else if ([textField isEqual:self.textFieldsCollection[4]]) {
        if (textField.text.length < 3) {
            [self.locationIcon setImage:[UIImage imageNamed:@"profile-location-red"]];
        } else {
            [self.locationIcon setImage:[UIImage imageNamed:@"profile-location-green"]];
        }
    } else if ([textField isEqual:self.textFieldsCollection[6]]) {
        if (textField.text.length < 3) {
            [self.countryIcon setImage:[UIImage imageNamed:@"profile-country-icon-red"]];
        } else {
            [self.countryIcon setImage:[UIImage imageNamed:@"profile-country-icon-green"]];
        }
    }
    
    if ([self allFieldsAreValid]) {
        [self enableCreateButton];
    } else {
        [self disableCreateButton];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([self.textFieldsCollection indexOfObject:textField] == 4 && self.stateMaskView.hidden) {
        return [textField resignFirstResponder];
    }
    return [[self closestClearTextFieldFrom:textField] becomeFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {

    if ([textField isEqual:self.textFieldsCollection[1]]) {
//        return [[VAControlTool defaultControl] setEmailMaskForTextField:textField InRange:range replacementString:string];
        return YES;
    } else if ([textField isEqual:self.textFieldsCollection[0]] || [textField isEqual:self.textFieldsCollection[4]] || [textField isEqual:self.textFieldsCollection[5]]) {
        return [[VAControlTool defaultControl] setNameAndSurnameMaskForTextField:textField InRange:range replacementString:string];
    } else  if ([textField isEqual:self.textFieldsCollection[2]]) {
        return [[VAControlTool defaultControl] setAliasMaskForTextField:textField InRange:range replacementString:string];
    } else if ([textField isEqual:self.textFieldsCollection[3]]) {
        return [[VAControlTool defaultControl] setPasswordMaskForTextField:textField InRange:range replacementString:string];
    } else {
        return YES;
    }
}

- (IBAction)textfieldTextDidChange:(UITextField *)sender {
    if ([self allFieldsAreValid]) {
        [self enableCreateButton];
    } else {
        [self disableCreateButton];
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGPoint offset = scrollView.contentOffset;
    
    CGFloat len = -offset.y;
    
    if (len < 0) {
        len = 0;
    }
    
    self.profileViewHeightConstraint.constant = len;
}

#pragma mark - VAPhotoToolDelegate

- (void)photoHasBeenEdited:(UIImage *)photo landscape:(BOOL)isLandscape {
    self.avatarContainerView.layer.borderColor = [[VADesignTool defaultDesign] vicinityDarkBlue].CGColor;

    self.updatedImage = photo;
    [self updateAvatarImage:photo];
    
    [self.addButton setHidden:YES];
    [self.editButton setHidden:NO];
    
    self.navigationItem.leftBarButtonItem = [self backButton];
    
    if ([self allFieldsAreValid]) {
        [self enableCreateButton];
    } else {
        [self disableCreateButton];
    }
}

- (void)photoActionViewHasBeenDismissed {
    self.navigationItem.leftBarButtonItem = [self backButton];
}

- (void)userDidChangeGenderType:(VAGenderType)genderType {

}

#pragma mark - State view

- (void)stateViewTap:(id)sender {
    if ([self.textFieldsCollection[5] isFirstResponder]) {
        ((UITextField *)self.textFieldsCollection[5]).text = self.statePickerView.currentState;
        [self didSelectState:self.statePickerView.currentState withAbbreviation:self.statePickerView.currentState];
        [self hideKeyboard];
        [self.stateTapView removeFromSuperview];
    } else {
        [self.textFieldsCollection[5] becomeFirstResponder];
        UIWindow *window = [UIApplication sharedApplication].windows.lastObject;
        [window addSubview:self.stateTapView];
        [window bringSubviewToFront:self.stateTapView];
    }
}

- (void)countryViewTap:(id)sender {
    if ([self.textFieldsCollection[6] isFirstResponder]) {
        ((UITextField *)self.textFieldsCollection[6]).text = self.countryPickerView.currentCountry;
        [self didSelectCountry:self.countryPickerView.currentCountry];
        [self hideKeyboard];
        [self.countryTapView removeFromSuperview];
    } else {
        [self.textFieldsCollection[6] becomeFirstResponder];
        UIWindow *window = [UIApplication sharedApplication].windows.lastObject;
        [window addSubview:self.countryTapView];
        [window bringSubviewToFront:self.countryTapView];
    }
}

#pragma mark - Confirm email delegate

- (void)didRestoreOriginalEmailForUser:(VAUser *)user {
    self.pendingUser = nil;
}

@end
