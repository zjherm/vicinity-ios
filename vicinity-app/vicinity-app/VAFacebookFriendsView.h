//
//  VAFacebookFriendsView.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/21/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VAFacebookFriendsViewDelegate <NSObject>

- (void)didUpdateFacebookFriends;
- (void)didInviteFriends;
- (void)didAddUserAsConnection:(VAUser *)user;

@end

@interface VAFacebookFriendsView : UIView
@property (nonatomic, weak) id<VAFacebookFriendsViewDelegate> delegate;

- (void)getFacebookFriends;
@end
