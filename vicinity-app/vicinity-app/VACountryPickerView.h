//
//  VACountryPickerView.h
//  vicinity-app
//
//  Created by Panda Systems on 2/25/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VACountryPickerDelegate <NSObject>
- (void)didSelectCountry:(NSString *)country;
@end

@interface VACountryPickerView : UIPickerView
@property (nonatomic, weak) id<VACountryPickerDelegate> countryDelegate;
@property (nonatomic) NSString *currentCountry;
@end
