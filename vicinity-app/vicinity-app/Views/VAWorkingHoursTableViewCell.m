//
//  VAWorkingHoursTableViewCell.m
//  vicinity-app
//
//  Created by Panda Systems on 10/8/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAWorkingHoursTableViewCell.h"
#import "VADay.h"

#pragma mark - VADay+Today category

@interface VADay (Today)

- (BOOL)isToday;
- (NSString *)dayDescription;
- (NSString *)hoursDescription;

@end

@implementation VADay (Today)

- (BOOL)isToday {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorian components:NSWeekdayCalendarUnit fromDate:[NSDate date]];
    NSInteger weekday = [components weekday] - 1;
    
    return [self.dayIndex integerValue] == weekday;
}

- (NSString *)dayDescription {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"US"]];
    NSArray *weekdays = [dateFormatter weekdaySymbols];
    return weekdays[[self.dayIndex integerValue]];
}

- (NSString *)hoursDescription {
    if ([self.openHours isEqualToString:@"0000"] && [self.closeHours isEqualToString:@"2400"]) {
        return @"Open 24 hours";
    }
    return [NSString stringWithFormat:@"%@ - %@",
            [self timeStringFromString:self.openHours],
            [self timeStringFromString:self.closeHours]];
}

- (NSString *)timeStringFromString:(NSString *)string {
    //string will be in format of @"0900", for example
    
    NSString *hoursString = [string substringToIndex:2];
    NSString *minutesString = [string substringFromIndex:2];
    NSString *endString = @"AM";
    
    NSInteger hours = [hoursString integerValue];
    if (hours >= 12) {
        if (hours == 12) {
            hoursString = [NSString stringWithFormat:@"%ld", (long)hours];
            endString = @"PM";
        } else {
            NSInteger newHours = hours - 12;
            hoursString = [NSString stringWithFormat:@"%ld", (long)newHours];
            endString = @"PM";
        }
    } else {
        if (hours < 1) {
            NSInteger newHours = hours + 12;
            hoursString = [NSString stringWithFormat:@"%ld", (long)newHours];
            endString = @"AM";
        } else {
            hoursString = [NSString stringWithFormat:@"%ld", (long)hours];
            endString = @"AM";
        }
    }
    NSString *resultString = [NSString stringWithFormat:@"%@:%@ %@", hoursString, minutesString, endString];
    
    return resultString;
}

@end


#pragma mark - VAWorkingHoursTableViewCell class

@interface VAWorkingHoursTableViewCell ()

@property (nonatomic, weak) IBOutlet UILabel *dayLabel;
@property (nonatomic, weak) IBOutlet UILabel *hoursLabel;

@property (nonatomic, strong) IBOutletCollection(NSLayoutConstraint) NSArray *smallScreenConstraints;
@property (nonatomic, strong) IBOutletCollection(NSLayoutConstraint) NSArray *largeScreenConstraints;

@property (nonatomic) CGFloat prefferedFontSize;

@end

@implementation VAWorkingHoursTableViewCell

- (void)awakeFromNib {
    self.prefferedFontSize = CGRectGetWidth([UIScreen mainScreen].bounds) < 400.0f ? 10.0f : 13.0f;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    BOOL isSmallScreen = CGRectGetWidth([UIScreen mainScreen].bounds) < 350.0f;
    
    for (NSLayoutConstraint *small in self.smallScreenConstraints) {
        small.active = isSmallScreen;
    }
    for (NSLayoutConstraint *small in self.largeScreenConstraints) {
        small.active = !isSmallScreen;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureCellWithDay:(VADay *)day {
    if ([day isToday]) {
        [self boldLabels];
    } else {
        [self unboldLabels];
    }
    
    self.dayLabel.text = day.dayDescription;
    self.hoursLabel.text = day.hoursDescription;
}

#pragma mark - Helpers

- (void)boldLabels {
    self.dayLabel.font = [UIFont fontWithName:@"Montserrat-Bold" size:self.prefferedFontSize];
    self.hoursLabel.font = [UIFont fontWithName:@"Montserrat-Bold" size:self.prefferedFontSize];
}

- (void)unboldLabels {
    self.dayLabel.font = [UIFont fontWithName:@"Montserrat-Regular" size:self.prefferedFontSize];
    self.hoursLabel.font = [UIFont fontWithName:@"Montserrat-Regular" size:self.prefferedFontSize];
}

@end
