//
//  VAProfileUserInfoView.h
//  vicinity-app
//
//  Created by Panda Systems on 5/20/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VAGenderTypeView.h"

@protocol VAProfileChangesProtocol;
@class VAStatePickerView, VACountryPickerView;

@interface VAProfileUserInfoView : UIView
@property (weak, nonatomic) id <VAProfileChangesProtocol> delegate;

@property (nonatomic, weak) IBOutlet UITextField *nameTextField;
@property (nonatomic, weak) IBOutlet UITextField *aliasTextField;
@property (nonatomic, weak) IBOutlet UITextField *cityTextField;
@property (nonatomic, weak) IBOutlet UITextField *stateTextField;
@property (nonatomic, weak) IBOutlet UITextField *countryTextField;
@property (nonatomic, weak) IBOutlet UITextField *emailTextField;
@property (nonatomic, weak) IBOutlet UITextView *bioTextView;
@property (nonatomic, weak) IBOutlet UILabel *placeholderLabel;
@property (nonatomic, weak) IBOutlet VAGenderTypeView *genderView;
@property (nonatomic, weak) IBOutlet UIView *bioContainer;
@property (nonatomic, weak) IBOutlet UIView *cityContainer;
@property (nonatomic, weak) IBOutlet UIView *countryContainer;
@property (nonatomic, weak) IBOutlet UIView *nameContainer;
@property (nonatomic, weak) IBOutlet UIView *emailContainer;
@property (nonatomic, weak) IBOutlet UIView *passwordContainer;
@property (weak, nonatomic) IBOutlet UIView *privateHeaderView;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *cityHeightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *countryHeightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *nameHeightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *genderHeightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *passwordHeightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bioBlockHeightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *privateBlockHeaderHeightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *privateBlockHeightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *emailHeightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *cityViewTrailingConstraint;

@property (nonatomic, strong) VAStatePickerView *statePickerView;
@property (nonatomic, strong) VACountryPickerView *countryPickerView;

- (void)updateForCountry:(NSString *)country;
@end

@protocol VAProfileChangesProtocol <NSObject>
- (void)textFieldDidBecomeActive:(id)textField;
- (void)textFieldDidChange:(UITextField *)textField;
- (void)bioViewDidChange:(UITextView *)textView;

@optional
- (void)bioViewDidBeginEditing:(UITextView *)textView;
- (void)bioViewDidEndEditing:(UITextView *)textView;
@end