//
//  VAWorkingHoursTableViewCell.h
//  vicinity-app
//
//  Created by Panda Systems on 10/8/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VADay;

@interface VAWorkingHoursTableViewCell : UITableViewCell

- (void)configureCellWithDay:(VADay *)day;

@end
