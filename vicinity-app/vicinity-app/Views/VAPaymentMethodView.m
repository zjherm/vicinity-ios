//
//  VAPaymentMethodView.m
//  vicinity-app
//
//  Created by Panda Systems on 5/20/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAPaymentMethodView.h"

@interface VAPaymentMethodView ()

@property (nonatomic, weak) IBOutlet UIView *headerSeparatorView;

@property (nonatomic, weak) IBOutlet UIView *creditCardView;
@property (nonatomic, weak) IBOutlet UIView *paypalView;
@property (nonatomic, weak) IBOutlet UIView *creditCardStateView;
@property (nonatomic, weak) IBOutlet UIView *paypalStateView;
@property (nonatomic, weak) IBOutlet UIView *creditCardInfoView;
@property (nonatomic, weak) IBOutlet UIView *paypalInfoView;

@end

@implementation VAPaymentMethodView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [[VADesignTool defaultDesign] addShadowForSeparator:self.headerSeparatorView];
    
    UITapGestureRecognizer *creditCardTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectCreditCardMethod:)];
    [self.creditCardView addGestureRecognizer:creditCardTap];
    UITapGestureRecognizer *paypalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectPaypalMethod:)];
    [self.paypalView addGestureRecognizer:paypalTap];
    
    self.paymentMethodType = VAPaymentMethodTypeCreditCard;
}

#pragma mark - Properties

- (void)setPaymentMethodType:(VAPaymentMethodType)paymentMethodType {
    _paymentMethodType = paymentMethodType;
    
    self.creditCardInfoView.hidden = paymentMethodType != VAPaymentMethodTypeCreditCard;
    self.paypalInfoView.hidden = paymentMethodType != VAPaymentMethodTypePaypal;
    self.creditCardStateView.hidden = paymentMethodType != VAPaymentMethodTypeCreditCard;
    self.paypalStateView.hidden = paymentMethodType != VAPaymentMethodTypePaypal;
}

#pragma mark - Taps

- (void)didSelectCreditCardMethod:(id)sender {
    self.paymentMethodType = VAPaymentMethodTypeCreditCard;
}

- (void)didSelectPaypalMethod:(id)sender {
    self.paymentMethodType = VAPaymentMethodTypePaypal;
}

@end
