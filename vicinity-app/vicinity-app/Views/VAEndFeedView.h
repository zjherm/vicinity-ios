//
//  VAEndFeedView.h
//  vicinity-app
//
//  Created by Panda Systems on 1/15/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VAEndFeedViewProtocol <NSObject>
- (void)shouldShowShareSheet;
@end

@interface VAEndFeedView : UIView
@property (weak, nonatomic) id <VAEndFeedViewProtocol> delegate;
@end
