//
//  VAPaymentMethodView.h
//  vicinity-app
//
//  Created by Panda Systems on 5/20/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, VAPaymentMethodType) {
    VAPaymentMethodTypeCreditCard,
    VAPaymentMethodTypePaypal
};

@interface VAPaymentMethodView : UIView

@property (nonatomic) VAPaymentMethodType paymentMethodType;

@end
