//
//  VAProfileUserInfoView.m
//  vicinity-app
//
//  Created by Panda Systems on 5/20/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAProfileUserInfoView.h"
#import "VAControlTool.h"
#import "VAChangePasswordViewController.h"
#import "VAStatePickerView.h"
#import "VACountryPickerView.h"

@interface VAProfileUserInfoView () <VAGenderChangeDelegate, VAStatePickerDelegate, VACountryPickerDelegate>
@property (strong, nonatomic) NSArray *genderArray;

@property (nonatomic, weak) IBOutlet UIView *privateHeaderSeparatorView;
@property (nonatomic, weak) IBOutlet UIView *stateMaskView;
@property (nonatomic, weak) IBOutlet UIView *countryMaskView;
@property (strong, nonatomic) UIView *stateTapView;
@property (strong, nonatomic) UIView *countryTapView;
@end

@implementation VAProfileUserInfoView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.bioTextView.textContainerInset = UIEdgeInsetsMake(8.f, -4.f, 8.f, 21.f);
    
    [[VADesignTool defaultDesign] addShadowForSeparator:self.privateHeaderSeparatorView];
    
    self.genderView.delegate = self;
    [self setupPickerView];
    
    UITapGestureRecognizer *stateViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(stateMaskViewTap:)];
    [self.stateMaskView addGestureRecognizer:stateViewTap];
    
    UITapGestureRecognizer *countryViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(countryMaskViewTap:)];
    [self.countryMaskView addGestureRecognizer:countryViewTap];
    
    self.stateTapView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight([UIScreen mainScreen].bounds) - 122.f, CGRectGetWidth([UIScreen mainScreen].bounds), 28.f)];
    self.stateTapView.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *stateTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(stateMaskViewTap:)];
    [self.stateTapView addGestureRecognizer:stateTap];
    
    self.countryTapView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight([UIScreen mainScreen].bounds) - 122.f, CGRectGetWidth([UIScreen mainScreen].bounds), 28.f)];
    self.countryTapView.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *countryTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(countryMaskViewTap:)];
    [self.countryTapView addGestureRecognizer:countryTap];
}

- (void)setupPickerView {
    self.statePickerView = [[VAStatePickerView alloc] init];
    self.statePickerView.stateDelegate = self;
    self.stateTextField.inputView = self.statePickerView;
    
    self.countryPickerView = [[VACountryPickerView alloc] init];
    self.countryPickerView.countryDelegate = self;
    self.countryTextField.inputView = self.countryPickerView;
}

- (void)stateMaskViewTap:(id)sender {
    if ([self.stateTextField isFirstResponder]) {
        [self didSelectState:self.statePickerView.currentState withAbbreviation:self.statePickerView.currentState];
        [self.stateTextField resignFirstResponder];
        [self.stateTapView removeFromSuperview];
    } else {
        [self.stateTextField becomeFirstResponder];
        UIWindow *window = [UIApplication sharedApplication].windows.lastObject;
        [window addSubview:self.stateTapView];
        [window bringSubviewToFront:self.stateTapView];
    }
}

- (void)countryMaskViewTap:(id)sender {
    if ([self.countryTextField isFirstResponder]) {
        [self didSelectCountry:self.countryPickerView.currentCountry];
        [self.countryTextField resignFirstResponder];
        [self.countryTapView removeFromSuperview];
    } else {
        [self.countryTextField becomeFirstResponder];
        UIWindow *window = [UIApplication sharedApplication].windows.lastObject;
        [window addSubview:self.countryTapView];
        [window bringSubviewToFront:self.countryTapView];
    }
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self.delegate textFieldDidBecomeActive:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    return [textField resignFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    [self.delegate textFieldDidChange:textField];
    
    if ([textField isEqual:self.emailTextField]) {
//        return [[VAControlTool defaultControl] setEmailMaskForTextField:textField InRange:range replacementString:string];
        return YES;
    } else if ([textField isEqual:self.nameTextField] || [textField isEqual:self.cityTextField]) {
        return [[VAControlTool defaultControl] setNameAndSurnameMaskForTextField:textField InRange:range replacementString:string];
    } else  if ([textField isEqual:self.aliasTextField]) {
        return [[VAControlTool defaultControl] setAliasMaskForTextField:textField InRange:range replacementString:string];
    } else {
        return YES;
    }
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    [self.delegate textFieldDidChange:textField];
    return YES;
}

#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    [self.delegate textFieldDidBecomeActive:textView];
}

- (void)textViewDidChange:(UITextView *)textView {
    if ([self.bioTextView.text isEqualToString:@""]) {
        [self.placeholderLabel setHidden:NO];
        self.placeholderLabel.text = @"Create your Bio";
    } else {
        [self.placeholderLabel setHidden:YES];
        self.placeholderLabel.text = @"";
    }
    [self.delegate bioViewDidChange:textView];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    [self.delegate bioViewDidEndEditing:textView];
}

#pragma mark - Actions

- (IBAction)changePassword:(UIButton *)sender {
    UIViewController *topVC = [VAControlTool topScreenController];
    VAChangePasswordViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"VAChangePasswordViewController"];
    [topVC.navigationController pushViewController:vc animated:YES];
}

#pragma mark - VAGenderChangeDelegate

- (void)userDidChangeGenderType:(VAGenderType)genderType {
    [self.delegate textFieldDidChange:nil];
}

#pragma mark - VAStatePickerDelegate methods

- (void)didSelectState:(NSString *)state withAbbreviation:(NSString *)abbreviation {
    self.stateTextField.text = abbreviation;
    [self.delegate textFieldDidChange:self.stateTextField];
}

- (void)didSelectCountry:(NSString *)country {
    self.countryTextField.text = country;
    [self.delegate textFieldDidChange:self.countryTextField];
    
    [self updateForCountry:country];
}

- (void)updateForCountry:(NSString *)country {
    if ([country isEqualToString:@"United States"]) {
        self.stateTextField.hidden = NO;
        self.stateMaskView.hidden = NO;
        
        self.cityViewTrailingConstraint.constant = 0;
        [UIView animateWithDuration:0.3f animations:^{
            [self layoutIfNeeded];
        }];
    } else {
        self.stateTextField.text = @"";
        self.statePickerView.currentState = @"";
        
        self.cityViewTrailingConstraint.constant = -([UIScreen mainScreen].bounds.size.width - 66.0f + 32.0f);
        
        [UIView animateWithDuration:0.3f animations:^{
            [self layoutIfNeeded];
        } completion:^(BOOL finished) {
            self.stateTextField.hidden = YES;
            self.stateMaskView.hidden = YES;
        }];
    }
}

@end
