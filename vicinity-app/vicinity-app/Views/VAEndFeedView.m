//
//  VAEndFeedView.m
//  vicinity-app
//
//  Created by Panda Systems on 1/15/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import "VAEndFeedView.h"

@interface VAEndFeedView ()
@property (nonatomic, weak) IBOutlet UIView *contentView;
@end

@implementation VAEndFeedView

- (void)awakeFromNib {
    self.contentView.layer.cornerRadius = 4.0f;
    self.contentView.clipsToBounds = YES;
}

#pragma mark - Actions

- (IBAction)inviteButtonPushed:(UIButton *)sender {
    [self.delegate shouldShowShareSheet];
}

@end
