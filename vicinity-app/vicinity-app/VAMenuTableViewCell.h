//
//  VAMenuTableViewCell.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 30.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VAUser;

@interface VAMenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *rightArrowImageView;
@property (nonatomic, weak) IBOutlet UIView *countView;
@property (nonatomic, weak) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UIImageView *comingSoonImageView;
@property (nonatomic, weak) IBOutlet UIView *iconContainer;

@property (nonatomic, weak) IBOutlet UILabel *liveDealLabel;
@property (weak, nonatomic) IBOutlet UIImageView *liveDealImageView;
@property (weak, nonatomic) IBOutlet UIImageView *vicinityImageView;

- (void)setCellSelected:(BOOL)selected;
- (void)setNotificationsCount:(NSInteger)count;
- (void)loadAvatarForUser:(VAUser *)user;
@end
