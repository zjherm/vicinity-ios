//
//  VAFullPhotoCollectionViewCell.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 07.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VAFullPhotoCollectionViewCell : UICollectionViewCell
@property (nonatomic, weak) IBOutlet UIImageView *photoImageView;
@property (nonatomic, weak) IBOutlet UIImageView *photoBackgroundImageView;
@property (nonatomic, weak) IBOutlet UIView *bluredView;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@end
