//
//  VAAppDelegate.h
//  vicinity-app
//
//  Created by Panda Systems on 5/18/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (void)goToEventsViewController;

@end

