//
//  VABusinessPhotoViewController.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 07.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VABusinessPhotoViewController.h"
#import "VAFullPhotoCollectionViewCell.h"
#import "GPUImage.h"
#import <UIImage-ResizeMagick/UIImage+ResizeMagick.h>
#import "VAControlTool.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "VANotificationMessage.h"
#import <Google/Analytics.h>

#import "VAGooglePlacesApiManager.h"

@interface VABusinessPhotoViewController ()
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@end

@implementation VABusinessPhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackButton"] style:UIBarButtonItemStylePlain target:self.navigationController action:@selector(popViewControllerAnimated:)];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    [((UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout) setSectionInset:UIEdgeInsetsMake(-44.0f, 0, 0, 0)];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.view layoutIfNeeded];
    
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Fullscreen business photo screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setBackgroundImage:nil
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.translucent = NO;
    [[UIApplication sharedApplication] setStatusBarHidden:NO];

}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self.collectionView scrollToItemAtIndexPath:self.selectedIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];

}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.photoURLsArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"FullPhoto";
    VAFullPhotoCollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];

    [cell.indicator startAnimating];
    
    cell.photoImageView.alpha = 0.f;
    cell.photoBackgroundImageView.alpha = 0.f;
    __weak VAFullPhotoCollectionViewCell *weakCell = cell;
    [weakCell.photoImageView sd_setImageWithURL:[[VAGooglePlacesApiManager sharedManager] downloadUrlForPhoto:self.photoURLsArray[indexPath.row]]
                               placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                        options:SDWebImageRetryFailed | SDWebImageContinueInBackground | SDWebImageHandleCookies | SDWebImageHighPriority | SDWebImageTransformAnimatedImage | SDWebImageAvoidAutoSetImage
                                      completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                          if (image) {
                                              [cell.indicator stopAnimating];
                                         
                                              [self setImage:image forCell:cell];
                                          } else if (error) {
                                              [VANotificationMessage showLoadErrorMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
                                          }
                                      }];
    [cell layoutIfNeeded];
    return cell;
}

#pragma mark - UICollectionViewDelegate

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize size = CGSizeMake(CGRectGetWidth([UIScreen mainScreen].bounds), CGRectGetHeight([UIScreen mainScreen].bounds));
    return size;
}

#pragma mark - Methods

- (void)setImage:(UIImage *)image forCell:(VAFullPhotoCollectionViewCell *)cell {
    if (IOS8 || IOS9) {
        for (id subview in [cell.bluredView subviews]) {
            [subview removeFromSuperview];
        }
        cell.photoImageView.image = image;
        cell.photoBackgroundImageView.image = image;
        [UIView animateWithDuration:0.3f animations:^{
            cell.photoImageView.alpha = 1.0;
            cell.photoBackgroundImageView.alpha = 1.0;
        }];
        UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVibrancyEffect *vibrancy = [UIVibrancyEffect effectForBlurEffect:blur];
        
        UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
        effectView.frame = CGRectMake(0, 0, CGRectGetWidth([self.collectionView frame]), CGRectGetHeight([self.collectionView frame]) + 25);
        
        UIVisualEffectView *vibrantView = [[UIVisualEffectView alloc]initWithEffect:vibrancy];
        effectView.frame = CGRectMake(0, 0, CGRectGetWidth([self.collectionView frame]), CGRectGetHeight([self.collectionView frame]) + 25);
        
        [cell.bluredView addSubview:effectView];
        [cell.bluredView addSubview:vibrantView];

    } else if (IOS7) {
        [self setBluredPhotoImage:image forCell:cell];
    }
}

- (void)setBluredPhotoImage:(UIImage *)photoImage forCell:(VAFullPhotoCollectionViewCell *)cell {
    
    cell.photoBackgroundImageView.image = nil;
    cell.photoImageView.image = nil;
    UIActivityIndicatorView *ind = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    ind.center = self.collectionView.center;
    [cell addSubview:ind];
    [ind startAnimating];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        UIImage *resizedImage = [photoImage resizedImageWithMaximumSize:CGSizeMake(408, 408)];
        UIImage *bluredImage = [self gpuBlurApplyDarkEffect:resizedImage];
        dispatch_async(dispatch_get_main_queue(), ^{
            [ind stopAnimating];
            cell.photoBackgroundImageView.image = bluredImage;
            cell.photoImageView.image = photoImage;
        });
    });
    
//    UIImage *bluredImage = [self gpuBlurApplyDarkEffect:avatarImage];
//    cell.photoBackgroundImageView.image = bluredImage;
}

- (UIImage *)gpuBlurApplyDarkEffect:(UIImage*)image {
    
    GPUImageiOSBlurFilter *blurFilter = [[GPUImageiOSBlurFilter alloc] init];
    blurFilter.blurRadiusInPixels = 13;
    UIImage *result = [blurFilter imageByFilteringImage:image];
    
    GPUImageBrightnessFilter * brightnessFilter = [[GPUImageBrightnessFilter alloc] init];
    brightnessFilter.brightness = -0.2;
    result = [brightnessFilter imageByFilteringImage:result];
    
    return result;
}

@end
