//
//  VAInviteTypeView.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/21/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAInviteTypeView.h"

@interface VAInviteTypeView ()

@property (weak, nonatomic) IBOutlet UIView *stateView;

@property (weak, nonatomic) IBOutlet UIView *facebookView;
@property (weak, nonatomic) IBOutlet UIView *contactsView;
@property (weak, nonatomic) IBOutlet UIView *otherView;

@property (weak, nonatomic) IBOutlet UILabel *facebookLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactsLabel;
@property (weak, nonatomic) IBOutlet UILabel *otherLabel;

@end

@implementation VAInviteTypeView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setupGestures];
    [self setupConstraints];

    self.inviteType = VAInviteTypeFacebook;
}

#pragma mark - Setup UI

- (void)setupGestures {
    UITapGestureRecognizer *facebookTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectFacebookInvite:)];
    [self.facebookView addGestureRecognizer:facebookTap];
    self.facebookView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *contactsTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectContactsInvite:)];
    [self.contactsView addGestureRecognizer:contactsTap];
    self.contactsView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *otherTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectOtherInvite:)];
    [self.otherView addGestureRecognizer:otherTap];
    self.otherView.userInteractionEnabled = YES;
}

- (void)setupConstraints {
    self.facebookStateViewConstraints = @[[NSLayoutConstraint constraintWithItem:self.stateView
                                                                       attribute:NSLayoutAttributeLeading
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:self.facebookLabel
                                                                       attribute:NSLayoutAttributeLeading
                                                                      multiplier:1
                                                                        constant:0],
                                          [NSLayoutConstraint constraintWithItem:self.stateView
                                                                       attribute:NSLayoutAttributeTrailing
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:self.facebookLabel
                                                                       attribute:NSLayoutAttributeTrailing
                                                                      multiplier:1
                                                                        constant:0]];
    
    self.contactsStateViewConstraints = @[[NSLayoutConstraint constraintWithItem:self.stateView
                                                                       attribute:NSLayoutAttributeLeading
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:self.contactsLabel
                                                                       attribute:NSLayoutAttributeLeading
                                                                      multiplier:1
                                                                        constant:0],
                                          [NSLayoutConstraint constraintWithItem:self.stateView
                                                                       attribute:NSLayoutAttributeTrailing
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:self.contactsLabel
                                                                       attribute:NSLayoutAttributeTrailing
                                                                      multiplier:1
                                                                        constant:0]];
    
    self.otherStateViewConstraints = @[[NSLayoutConstraint constraintWithItem:self.stateView
                                                                       attribute:NSLayoutAttributeLeading
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:self.otherLabel
                                                                       attribute:NSLayoutAttributeLeading
                                                                      multiplier:1
                                                                        constant:0],
                                          [NSLayoutConstraint constraintWithItem:self.stateView
                                                                       attribute:NSLayoutAttributeTrailing
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:self.otherLabel
                                                                       attribute:NSLayoutAttributeTrailing
                                                                      multiplier:1
                                                                        constant:0]];
    
    [self addConstraints:self.facebookStateViewConstraints];
}

#pragma mark - Properties

- (void)setInviteType:(VAInviteType)inviteType {
    _inviteType = inviteType;
    
//    self.creditCardInfoView.hidden = paymentMethodType != VAPaymentMethodTypeCreditCard;
//    self.paypalInfoView.hidden = paymentMethodType != VAPaymentMethodTypePaypal;
//    self.facebookStateView.hidden = inviteType != VAInviteTypeFacebook;
//    self.contactsStateView.hidden = inviteType != VAInviteTypeContacts;
//    self.otherStateView.hidden = inviteType != VAInviteTypeOther;
}

#pragma mark - Taps

- (void)didSelectFacebookInvite:(id)sender {
    self.inviteType = VAInviteTypeFacebook;
    [self.delegate userDidSelectInviteType:VAInviteTypeFacebook];
}

- (void)didSelectContactsInvite:(id)sender {
    self.inviteType = VAInviteTypeContacts;
    [self.delegate userDidSelectInviteType:VAInviteTypeContacts];
}

- (void)didSelectOtherInvite:(id)sender {
    self.inviteType = VAInviteTypeOther;
    [self.delegate userDidSelectInviteType:VAInviteTypeOther];
}

-(void)selectType:(VAInviteType)type{
    if (type==VAInviteTypeContacts) {
        [self didSelectContactsInvite:nil];
    }
    if (type==VAInviteTypeFacebook) {
        [self didSelectFacebookInvite:nil];
    }
    if (type==VAInviteTypeOther) {
        [self didSelectOtherInvite:nil];
    }
}
@end
