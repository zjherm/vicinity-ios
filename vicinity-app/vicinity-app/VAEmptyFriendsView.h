//
//  VAEmptyFriendsView.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/1/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VAFriendsEmptyViewDelegate;

@interface VAEmptyFriendsView : UIView
@property (weak, nonatomic) id <VAFriendsEmptyViewDelegate> delegate;
@end

@protocol VAFriendsEmptyViewDelegate <NSObject>
- (void)userDidPushInviteFacebookFriendsButton:(UIButton *)button;
@end