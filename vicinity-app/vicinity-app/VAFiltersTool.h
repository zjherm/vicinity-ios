//
//  VAFiltersTool.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 26.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString *kDistanceFilter = @"kDistanceFilter";
static NSString *kcategoriesFilter = @"kcategoriesFilter";

@interface VAFiltersTool : NSObject

+ (VAFiltersTool *)currentFilters;

- (void)saveDistance:(NSString *)distance;
- (NSString *)loadDistance;

- (void)saveCategories:(NSArray *)categories;
- (NSArray *)loadCategories;

@end
