//
//  VAStatePickerView.m
//  vicinity-app
//
//  Created by Panda Systems on 10/21/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAStatePickerView.h"

@interface VAStatePickerView () <UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) NSArray *states;
@property (nonatomic, strong) NSArray *abbreviations;

@end

@implementation VAStatePickerView

- (instancetype)init {
    self = [super init];
    if (self) {
        self.tintColor = [[VADesignTool defaultDesign] middleGrayColor];
        self.backgroundColor = [UIColor whiteColor];
        self.dataSource = self;
        self.delegate = self;
        [self selectRow:5 inComponent:0 animated:NO];
        
        _states = [self statesArray];
        _abbreviations = [self abbreviationsArray];
    }
    return self;
}

- (void)setCurrentState:(NSString *)currentState {
    _currentState = currentState;
    
    NSInteger index = [self.abbreviations indexOfObject:currentState];
    if (index != NSNotFound) {
        [self reloadAllComponents];
        [self selectRow:index inComponent:0 animated:NO];
    } else {
        index = [self.states indexOfObject:currentState];
        if (index != NSNotFound) {
            [self reloadAllComponents];
            [self selectRow:index inComponent:0 animated:NO];
        } else {
            [self reloadAllComponents];
            [self selectRow:0 inComponent:0 animated:YES];
            _currentState = _abbreviations[0];
        }
    }
}

#pragma mark - Data source

- (NSArray *)statesArray {
    return @[@"Alabama", @"Alaska", @"American Samoa", @"Arizona", @"Arkansas", @"California",
             @"Colorado", @"Connecticut", @"Delaware", @"District of Columbia", @"Florida", @"Georgia", @"Guam",
             @"Hawaii", @"Idaho", @"Illinois", @"Indiana", @"Iowa",
             @"Kansas", @"Kentucky", @"Louisiana", @"Maine", @"Maryland",
             @"Massachusetts", @"Michigan", @"Minnesota", @"Mississippi", @"Missouri",
             @"Montana", @"Nebraska", @"Nevada", @"New Hampshire", @"New Jersey",
             @"New Mexico", @"New York", @"North Carolina", @"North Dakota", @"Ohio",
             @"Oklahoma", @"Oregon", @"Pennsylvania", @"Puerto Rico", @"Rhode Island", @"South Carolina",
             @"South Dakota", @"Tennessee", @"Texas", @"Utah", @"Vermont",
             @"Virginia", @"Virgin Islands", @"Washington", @"West Virginia", @"Wisconsin", @"Wyoming"];
}

- (NSArray *)abbreviationsArray {
    return @[@"AL", @"AK", @"AS", @"AZ", @"AR", @"CA",
             @"CO", @"CT", @"DE", @"DC", @"FL", @"GA", @"GU",
             @"HI", @"ID", @"IL", @"IN", @"IA",
             @"KS", @"KY", @"LA", @"ME", @"MD",
             @"MA", @"MI", @"MN", @"MS", @"MO",
             @"MT", @"NE", @"NV", @"NH", @"NJ",
             @"NM", @"NY", @"NC", @"ND", @"OH",
             @"OK", @"OR", @"PA", @"PR", @"RI", @"SC",
             @"SD", @"TN", @"TX", @"UT", @"VT",
             @"VA", @"VI", @"WA", @"WV", @"WI", @"WY"];
}

#pragma mark - UIPickerViewDelegate methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.states.count;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    UILabel *tView = (UILabel*)view;
    if (!tView) {
        tView = [[UILabel alloc] init];
        [tView setFont:[[VADesignTool defaultDesign] vicinityRegularFontOfSize:14.f]];
        [tView setTextAlignment:NSTextAlignmentCenter];
        tView.numberOfLines = 3;
    }
    
    tView.text = [NSString stringWithFormat:@"%@ - %@", self.abbreviations[row], self.states[row]];
    return tView;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    _currentState = self.abbreviations[row];
    if (self.stateDelegate) {
        [self.stateDelegate didSelectState:self.states[row] withAbbreviation:self.abbreviations[row]];
    }
}

@end
