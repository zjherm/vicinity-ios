//
//  VAFacebookFriendsView.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/21/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAFacebookFriendsView.h"
#import "VALoaderView.h"
#import "VAUserApiManager.h"
#import "VANotificationMessage.h"
#import "VAControlTool.h"
#import "VAUserSearchResponse.h"
#import "VAEmptyFriendsView.h"
#import <FBSDKLoginKit.h>
#import <FBSDKCoreKit.h>
#import <FBSDKShareKit.h>
#import "VAUserDefaultsHelper.h"
#import "VAUser.h"
#import "VAFriendsTableViewCell.h"
#import "VAProfileViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UITableViewCell+VAParentCell.h"

@interface VAFacebookFriendsView () <VAFriendsEmptyViewDelegate, FBSDKAppInviteDialogDelegate, VAFacebookFriendsDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray *friendsArray;
@property (strong, nonatomic) VALoaderView *headerLoader;
@property (strong, nonatomic) VALoaderView *refresh;
@property (assign, nonatomic) BOOL isRefreshing;
@property (strong, nonatomic) UIBarButtonItem *leftItem;
@property (assign, nonatomic) CGFloat lastContentOffset;

@property (weak, nonatomic) IBOutlet UIButton *inviteButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;
@end

@implementation VAFacebookFriendsView

- (void)awakeFromNib {
    self.friendsArray = [NSArray array];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 120.f, 0);
    
    self.isRefreshing = NO;
    [self setupRefreshControl];
    
    self.inviteButton.layer.cornerRadius = 5.f;
    self.inviteButton.clipsToBounds = YES;
    
    self.heightConstraint.constant = 0.f;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.friendsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Friends";
    VAFriendsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell.delegate = self;
    
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.f;
}

#pragma mark - Actions

- (IBAction)addFriendButtonPushed:(UIButton *)sender {
    VAUser *user = [self userForCellWithPushedButton:sender];
    VAFriendsTableViewCell *cell = (VAFriendsTableViewCell *)[UITableViewCell findParentCellOfView:sender];
    
    if ([user.isFriend boolValue] == 1) {
        [cell.addButton setImage:[UIImage imageNamed:@"add-contact-icon"] forState:UIControlStateNormal];
        user.isFriend = @(NO);
        [self deleteFriend:user];
    } else {
        [cell.addButton setImage:[UIImage imageNamed:@"Checkmark"] forState:UIControlStateNormal];
        user.isFriend = @(YES);
        [self addFriend:user];
    }
}

- (IBAction)inviteFacebookFriends:(id)sender {
    [self inviteFriends];
}

#pragma mark - Methods

- (void)configureCell:(VAFriendsTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    cell.indexPath = indexPath;
    
    VAUser *user = [self.friendsArray objectAtIndex:indexPath.row];
    
    if (user.avatarURL) {
        NSURL *imageURL = [NSURL URLWithString:user.avatarURL];
        
        [cell.avatarView sd_setImageWithURL:imageURL
                           placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                      if (image) {
                                          cell.avatarView.image = image;
                                      }
                                      if (error) {
                                          [VANotificationMessage showAvatarErrorMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
                                      }
                                  }];
    } else {
        cell.avatarView.image = [UIImage imageNamed:@"placeholder.png"];
    }

    cell.nameLabel.text = user.fullname;
    cell.nicknameLabel.text = user.nickname;
    
    if (indexPath.row == [self.friendsArray count] - 1) {
        [cell.separatorView setHidden:YES];
    } else {
        [cell.separatorView setHidden:NO];
    }
    
    if ([user.isFriend boolValue] == 1) {
        [cell.addButton setImage:[UIImage imageNamed:@"Checkmark"] forState:UIControlStateNormal];
    } else {
        [cell.addButton setImage:[UIImage imageNamed:@"add-contact-icon"] forState:UIControlStateNormal];
    }
}

- (void)showProfileForUserWithAlias:(NSString *)alias {
    VAProfileViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"profileViewController"];
    vc.searchAlias = alias;
    vc.isUsersAccountMode = YES;
    UIViewController *topController = [VAControlTool topScreenController];
    [topController.navigationController pushViewController:vc animated:YES];
}

- (void)getFacebookFriends {
    self.tableView.scrollEnabled = NO;
    if (!self.isRefreshing) {
        self.heightConstraint.constant = 0.f;
        [self showLoader];
        [[self mutableArrayValueForKey:@"friendsArray"] removeAllObjects];
        [self.tableView reloadData];
    }
    VAUserApiManager *manager = [VAUserApiManager new];
    [manager getFacebookFriendsWithCompletion:^(NSError *error, VAModel *model) {
        self.tableView.scrollEnabled = YES;
        if (!self.isRefreshing) {
            [self.headerLoader stopAnimating];
        } else {
            [self.refresh stopRefreshing];
            self.isRefreshing = NO;
        }
        
        [self hideLoader];
        
        if (error) {
            [VANotificationMessage showLoadErrorMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
        } else if (model) {
            [[self mutableArrayValueForKey:@"friendsArray"] removeAllObjects];
            
            VAUserSearchResponse *response = (VAUserSearchResponse *)model;
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userStatus == %@", @"Normal"];
            
            [[self mutableArrayValueForKey:@"friendsArray"] addObjectsFromArray:[response.users filteredArrayUsingPredicate:predicate]];
            
            if ([self.friendsArray count] == 0) {
                [self showNoFriendsView];
            } else {
                self.tableView.tableHeaderView = [self headerViewForFriends];
                self.heightConstraint.constant = 120.f;
                self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 120.f, 0);
                [self layoutIfNeeded];
            }
            
            [self.tableView reloadData];
        }
    }];
}

- (void)showNoFriendsView {
    VAEmptyFriendsView *view = [[[NSBundle mainBundle] loadNibNamed:@"VAEmptyFriendsView" owner:self options:nil] firstObject];
    view.delegate = self;
    [view setFrame:CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), 232.f)];
    self.tableView.tableHeaderView = view;
}

- (void)hideNoFriendsView {
    self.tableView.tableHeaderView = nil;
}

- (void)showLoader {
    VALoaderView *loader = [VALoaderView initLoader];
    loader.frame = CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), kLoaderHeight);
    [loader startAnimating];
    self.tableView.tableHeaderView = loader;
    self.headerLoader = loader;
}

- (void)hideLoader {
    self.tableView.tableHeaderView = nil;
}

- (void)inviteFriends {
    FBSDKAppInviteContent *content = [[FBSDKAppInviteContent alloc] init];
    content.appLinkURL = [NSURL URLWithString:@"https://fb.me/1642207479328843"];
    
    [FBSDKAppInviteDialog showWithContent:content delegate:self];
}

- (void)setupRefreshControl {
    VALoaderView *refresh = [VALoaderView initLoader];
    self.refresh = refresh;
    [refresh setFrame:[self frameForCustomRefreshControl]];
    refresh.alpha = 0.f;
    
    [refresh addTarget:self
                action:@selector(refreshData)
      forControlEvents:UIControlEventValueChanged];
    
    [self.tableView addSubview:refresh];
}

- (CGRect)frameForCustomRefreshControl {
    CGFloat customRefreshControlHeight = kLoaderHeight;
    CGFloat customRefreshControlWidth = CGRectGetWidth(self.tableView.bounds);
    CGRect customRefreshControlFrame = CGRectMake(0.0f,
                                                  -customRefreshControlHeight,
                                                  customRefreshControlWidth,
                                                  customRefreshControlHeight);
    return customRefreshControlFrame;
}

- (void)refreshData {
    self.isRefreshing = YES;
    [self.refresh startRefreshing];
    [self getFacebookFriends];
}

- (VAUser *)userForCellWithPushedButton:(UIButton *)button {
    VAFriendsTableViewCell *cell = (VAFriendsTableViewCell *)[UITableViewCell findParentCellOfView:button];
    NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
    VAUser *user = [self.friendsArray objectAtIndex:cellIndexPath.row];
    return user;
}

- (void)addFriend:(VAUser *)user {
    [[VAUserApiManager new] postAddFriendWithID:user.userId andCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self.friendsArray indexOfObject:user] inSection:0];
            VAFriendsTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            [cell.addButton setImage:[UIImage imageNamed:@"add-contact-icon"] forState:UIControlStateNormal];
            user.isFriend = @(NO);
        } else {
            [self.delegate didUpdateFacebookFriends];
            [self.delegate didAddUserAsConnection:user];
        }
    }];
}

- (void)deleteFriend:(VAUser *)user {
    [[VAUserApiManager new] deleteConnectionWithID:user.userId withCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self.friendsArray indexOfObject:user] inSection:0];
            VAFriendsTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            [cell.addButton setImage:[UIImage imageNamed:@"Checkmark"] forState:UIControlStateNormal];
            user.isFriend = @(YES);
        } else {
            [self.delegate didUpdateFacebookFriends];
        }
    }];
}

- (UIView *)headerViewForFriends {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.bounds), 40)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, CGRectGetWidth(self.bounds) - 16 * 2, 40)];
    [label setFont:[[VADesignTool defaultDesign] vicinityRegularFontOfSize:12.f]];
    [label setText:@"Friends on Vicinity"];
    [label setTextColor:[UIColor lightGrayColor]];
    [view addSubview:label];
    UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(0, 34.f, CGRectGetWidth(view.bounds), 1.f)];
    separator.backgroundColor = [[[VADesignTool defaultDesign] clearGray] colorWithAlphaComponent:0.5f];
    [view addSubview:separator];
    return view;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self.refresh containingScrollViewDidEndDragging:scrollView];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.refresh containingScrollViewDidScroll:scrollView];
    
    if (self.lastContentOffset > scrollView.contentOffset.y && [self.friendsArray count] > 0) {    //scrolling up && table is not empty
        if (self.bottomConstraint.constant == -120.f) {
            [UIView animateWithDuration:0.5f animations:^{
                self.bottomConstraint.constant = 0.f;
                [self layoutIfNeeded];
            }];
        }
    } else if (self.lastContentOffset < scrollView.contentOffset.y && scrollView.contentOffset.y > 0 && [self.friendsArray count] > 0) {    //scrolling down && not pull to refresh && table is not empty
        if (self.bottomConstraint.constant == 0.f) {
            [UIView animateWithDuration:0.5f animations:^{
                self.bottomConstraint.constant = -120.f;
                [self layoutIfNeeded];
            }];
        }
    }
    
    
    self.lastContentOffset = scrollView.contentOffset.y;
}

#pragma mark - VAFriendsEmptyViewDelegate

- (void)userDidPushInviteFacebookFriendsButton:(UIButton *)button {
    [self inviteFriends];
}

#pragma mark - FBSDKAppInviteDialogDelegate

- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didCompleteWithResults:(NSDictionary *)results {
    NSLog(@"success");
    if (results && [results[@"didComplete"] intValue] == 1 && ![results[@"completionGesture"] isEqualToString:@"cancel"]) {
        [self.delegate didInviteFriends];
    }
}

- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didFailWithError:(NSError *)error {
    NSLog(@"fail");
}

#pragma mark - VAFacebookFriendsDelegate

- (void)userDidTapCellAtIndexPath:(NSIndexPath *)path {
    VAUser *user = [self.friendsArray objectAtIndex:path.row];
    [self showProfileForUserWithAlias:user.nickname];
}

@end
