//
//  VAMenuTableViewController.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 30.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MFSideMenu.h>

@interface VAMenuTableViewController : UIViewController

@property (nonatomic) NSInteger selectedMenuItemIndex;

@property (strong, nonatomic) MFSideMenuContainerViewController *containerViewController;

@end
