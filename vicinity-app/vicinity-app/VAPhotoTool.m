//
//  VAPhotoTool.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 29.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAPhotoTool.h"
#import "VAPhotoViewController.h"
#import "VAPhotoEditingViewController.h"
#import <MWPhotoBrowser.h>
#import "VAControlTool.h"

@interface VAPhotoTool () <VAPhotoViewProtocol, UINavigationControllerDelegate, UIImagePickerControllerDelegate, VAPhotoProtocol, MWPhotoBrowserDelegate>
@property (strong, nonatomic) UIViewController *topController;
@property (strong, nonatomic) NSArray *photos;
@end

@implementation VAPhotoTool

+ (VAPhotoTool *)defaultPhoto {
    static VAPhotoTool *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [VAPhotoTool new];
    });
    return shared;
}

- (void)showPhotoActionViewOnController:(UIViewController *)controller {
    self.topController = controller;
    VAPhotoViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"VAPhotoViewController"];
    vc.delegate = self;
    [controller addChildViewController:vc];
    [controller.view addSubview:vc.view];
    [vc didMoveToParentViewController:controller];
    
    vc.view.alpha = 0.0f;
    [UIView animateWithDuration:0.3f animations:^{
        vc.view.alpha = 1.0f;
    }];
}

- (void)openPhotoBrowserFor:(UIImage *)image {
    NSMutableArray *photos = [NSMutableArray array];
    [photos addObject:[MWPhoto photoWithImage:image]];
    self.photos = photos;
    
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    UIViewController *vc = [VAControlTool topScreenController];
    
    vc.navigationItem.title = @"";

    [[[VAControlTool topScreenController] navigationController] pushViewController:browser animated:YES];
}

#pragma mark - VAPhotoViewProtocol

- (void)dismissPhotoView {
    
    UIViewController *vc = [self.topController.childViewControllers lastObject];
    while ([vc.childViewControllers count] > 0) {
        vc = [vc.childViewControllers lastObject];
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        vc.view.alpha = 0.0f;
    }];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.31 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [vc willMoveToParentViewController:nil];
        [vc.view removeFromSuperview];
        [vc removeFromParentViewController];
    });
    
    if ([self.delegate respondsToSelector:@selector(photoActionViewHasBeenDismissed)]) {
        [self.delegate photoActionViewHasBeenDismissed];
    }
}

- (void)openCamera {
    [self dismissPhotoView];
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.delegate = self;
    picker.modalPresentationStyle = UIModalTransitionStyleCrossDissolve;
    [self.topController presentViewController:picker animated:YES completion:nil];
}

- (void)openPhotoLibrary {
    [self dismissPhotoView];
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.delegate = self;
    picker.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self.topController presentViewController:picker animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {

    VAPhotoEditingViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"VAPhotoEditingViewController"];
    vc.flowController = self.topController;
    vc.photo = image;
    vc.delegate = self;
    [picker pushViewController:vc animated:YES];
    
//    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    [picker presentViewController:vc animated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self.topController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - VAPhotoProtocol

- (void)photoHasBeenTaken:(UIImage *)photo landscape:(BOOL)isLandscape {
    [self.delegate photoHasBeenEdited:photo landscape:isLandscape];
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return [self.photos count];
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.photos.count) {
        return [self.photos objectAtIndex:index];
    }
    return nil;
}

@end
