//
//  VACommentTableViewCell.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 28.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VACommentTableViewCell.h"
#import "VAUser.h"
#import "VAPost.h"
#import <OpenInGoogleMapsController.h>
#import "VAPostTableViewCell.h"
#import "UILabel+VACustomFont.h"
#import "VAEventsViewController.h"
#import "VAProfileViewController.h"
#import "VAControlTool.h"
#import "VACommentsViewController.h"
#import "VAUserDefaultsHelper.h"
#import "VAPhotoTool.h"
#import "VABusinessProfileViewController.h"
#import <Google/Analytics.h>

@interface VACommentTableViewCell ()
@property (assign, nonatomic) BOOL showCheckIn;
@end

CGFloat postCellWidth;

@implementation VACommentTableViewCell

- (void)awakeFromNib {
    self.imageContainerHeight.constant = 0.f;
    self.textContainerHeight.constant = 0.f;
    self.likesViewContainerHeight.constant = 0.f;
    
    UITapGestureRecognizer* tapAvatar = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showProfileForUserWithAlias)];
    [tapAvatar setNumberOfTapsRequired:1];
    [self.avatarImageView addGestureRecognizer:tapAvatar];
    self.avatarImageView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer* tapPhoto = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openPhotoBrowser)];
    [self.postImageView addGestureRecognizer:tapPhoto];
    self.postImageView.userInteractionEnabled = YES;
    
    self.nameLabel.userInteractionEnabled = YES;
    self.postAndCommentLabel.userInteractionEnabled = YES;
    
    [self setupAvatarImageView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Setup

- (void)setupAvatarImageView {
    self.avatarImageView.layer.borderWidth = 1.5/[UIScreen mainScreen].scale;
    self.avatarImageView.layer.borderColor = [UIColor colorWithRed:151.0f/255.0f
                                                             green:151.0f/255.0f
                                                              blue:151.0f/255.0f
                                                             alpha:1].CGColor;
    self.avatarImageView.layer.cornerRadius = 22.5f;
    self.avatarImageView.clipsToBounds = YES;
}

#pragma mark - Methods

- (void)showProfileForUserWithAlias {
    [self.delegate shouldShowProfileForUser:self.user];
}

- (void)showLocationForPost:(VAPost *)post {
    GoogleMapDefinition *mapDefinition = [[GoogleMapDefinition alloc] init];
    double lat;
    double lon;
    if (self.showCheckIn) {
        lat = [post.checkIn.coordinate.latitude doubleValue];
        lon = [post.checkIn.coordinate.longitude doubleValue];
        if ([OpenInGoogleMapsController sharedInstance].isGoogleMapsInstalled) {
            mapDefinition.queryString = [NSString stringWithFormat:@"%@ %@", post.checkIn.name, post.checkIn.address];
        } else {
            mapDefinition.queryString = [NSString stringWithFormat:@"%f %f", lat, lon];
        }
    } else {
        lat = [post.location.latitude doubleValue];
        lon = [post.location.longitude doubleValue];
        mapDefinition.queryString = [NSString stringWithFormat:@"%f %f", lat, lon];
    }
    [[OpenInGoogleMapsController sharedInstance] openMap:mapDefinition];
}

- (VALinkType)linkTypeForUrl:(NSURL *)url {
    if ([url.absoluteString isEqualToString:@"Author"]) {
        return VALinkTypeAuthor;
    } else if ([url.absoluteString isEqualToString:@"Location"]) {
        return VALinkTypeLocation;
    } else if ([url.absoluteString rangeOfString:@"http://"].location != NSNotFound) {
        return VALinkTypeURL;
    } else {
        return -1;
    }
}

- (void)showProfileForUserWithAlias:(NSString *)alias {
    VAProfileViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"profileViewController"];
    vc.searchAlias = alias;
    vc.isUsersAccountMode = YES;
    VACommentsViewController *currentVC = (VACommentsViewController *)[VAControlTool topScreenController];
    [currentVC.navigationController pushViewController:vc animated:YES];
}

- (void)showPostCheckIn {
    self.showCheckIn = YES;
    [self showLocationForPost:self.post];
    
    //code for google analytics
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Location"
                                                          action:@"User clicked the check in"
                                                           label:@"From comments screen"
                                                           value:nil] build]];
}

- (void)showBusinessProfile {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VABusinessProfileViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"VABusinessProfileViewController"];
    vc.isBusinessSearchModeMode = YES;
    vc.googlePlaceID = self.post.checkIn.placeID;
    VACommentsViewController *currentVC = (VACommentsViewController *)[VAControlTool topScreenController];
    [currentVC.navigationController pushViewController:vc animated:YES];
    
    //code for google analytics
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Business profile"
                                                          action:@"User viewed the profile"
                                                           label:@"Google place profile"
                                                           value:nil] build]];
}

- (void)showPostLocation {
    self.showCheckIn = NO;
    [self showLocationForPost:self.post];
    
    //code for google analytics
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Location"
                                                          action:@"User clicked post's location"
                                                           label:@"From comments screen"
                                                           value:nil] build]];
}

- (void)openPhotoBrowser {
    [[VAPhotoTool defaultPhoto] openPhotoBrowserFor:self.postImageView.image];
    
    //code for google analytics
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Picture"
                                                          action:@"User clicked the picture"
                                                           label:@"From comments screen"
                                                           value:nil] build]];
}

#pragma mark - TTTAttributedLabelDelegate

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    switch ([self linkTypeForUrl:url]) {
        case VALinkTypeAuthor:
            [self showProfileForUserWithAlias];
            break;
        case VALinkTypeLocation:
            [self showBusinessProfile];
            break;
        case VALinkTypeURL:
            [[UIApplication sharedApplication] openURL:url];
            break;
        default:
            break;
    }
}

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithTransitInformation:(NSDictionary *)components {
    [self performActionToWordType:components[kType] wordString:components[kWord]];
}

#pragma mark - TTTAttributedLabel Actions

- (void)performActionToWordType:(NSString *)wordType wordString:(NSString *)string {
    if ([wordType isEqualToString:kHashtag]) {
        NSString *hashtag = [string substringFromIndex:1];              //string here shows #hashtag with #
        
        VAEventsViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"VAEventsViewController"];
        vc.isHashTagMode = YES;
        vc.hashtagString = hashtag;
        VACommentsViewController *currentVC = (VACommentsViewController *)[VAControlTool topScreenController];
        [currentVC.navigationController pushViewController:vc animated:YES];
    } else if ([wordType isEqualToString:kMention]) {
        NSString *nickname = [string substringFromIndex:1];
        [self showProfileForUserWithAlias:nickname];
    } else if ([wordType isEqualToString:kAlias]) {
        [self showProfileForUserWithAlias:string];
    }
}

#pragma mark - Height methods

- (CGFloat)heightForCellWithPost:(VAPost *)post {
    postCellWidth = CGRectGetWidth([[UIScreen mainScreen] bounds]);// - 2 * 16.f;
    
    CGFloat baseHeight = 80.f;
    CGFloat postContainerHeight = [self heightForPostContainerWithPost:post];
    CGFloat imageContainerHeight = [self heightForImageContainerWithURLString:post.photoURL landscape:[post.isLandscapePhoto boolValue]];
    CGFloat likesContainerHeight = [self heightForLikesContainerForPost:post];
    
    return baseHeight + postContainerHeight + imageContainerHeight + likesContainerHeight;
}

- (CGFloat)heightForPostContainerWithPost:(VAPost *)post {
    CGFloat postLabelIndentLeft = 8.f;
    CGFloat postLabelIndentRight = 18.f;
    CGFloat bottomPostLabelContraint = 10.f;
    
    CGFloat nameLabelHeight = [self.nameLabel heightForNameAndCheckInLabelWithWidth:CGRectGetWidth([[UIScreen mainScreen] bounds]) - 125.f andPost:post];   //if > 20 - label is multiline
    
    VAUser *loggedUser = (VAUser *)[VAUserDefaultsHelper getModelForKey:VASaveKeys_CurrentUser];
    
    if (!post.text && nameLabelHeight > 20) {
        return 19.f;
    }
    
    if (!post.text) {
        return 0;
    } else {
        self.postAndCommentLabel.text = post.text;
        return [self.postAndCommentLabel heightForPostLabelWithWidth:postCellWidth - postLabelIndentLeft - postLabelIndentRight] + bottomPostLabelContraint;
    }
}

- (CGFloat)heightForImageContainerWithURLString:(NSString *)urlString landscape:(BOOL)isLandscape {
    CGFloat bottomImageViewContraint = 12.f;
    
    if (!urlString) {
        return 0;
    } else {
        CGFloat k = isLandscape ? 9.0f/16.0f : 1.0f;
        return postCellWidth*k + bottomImageViewContraint;        //imageView is always square, so height equals cell's width
    };
}

- (CGFloat)heightForLikesContainerForPost:(VAPost *)post {
    if (post.likesAmount.integerValue > 0) {
            return 24.f;
        } else {
            return 0.f;
        }
    }

@end

