//
//  VASignUpViewController.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/7/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VAUser;

@interface VASignUpViewController : UIViewController
@property (nonatomic, strong) VAUser *pendingUser;
@end
