//
//  VADealTableViewCell.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 01.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VAVoucherView.h"
#import <TTTAttributedLabel.h>

@protocol VADealDelegate;
@class VABusiness;
@class VADeal;

@interface VADealTableViewCell : UITableViewCell
@property (weak, nonatomic) id <VADealDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *avatarView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *dealLabel;
@property (weak, nonatomic) IBOutlet UILabel *likeLabel;
@property (weak, nonatomic) IBOutlet UIButton *viewAllCommentsButton;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *firstCommentLabel;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *secondCommentLabel;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UILabel *likeButtonLabel;
@property (weak, nonatomic) IBOutlet UIImageView *likeImageView;
@property (weak, nonatomic) IBOutlet UILabel *commentButtonLabel;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UIImageView *dealImageView;
@property (weak, nonatomic) IBOutlet UIView *likeView;
@property (weak, nonatomic) IBOutlet UIButton *getDealButton;
@property (weak, nonatomic) IBOutlet UILabel *shortDealDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *purchaseButton;
@property (weak, nonatomic) IBOutlet UIView *confirmationView;
@property (weak, nonatomic) IBOutlet UIButton *editCardButton;
@property (weak, nonatomic) IBOutlet UIView *cellBlock;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *likesAndCommentsContainerHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageContainerHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textContainerHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *confirmationViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomCommentConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *getDealHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentBlockHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionContainerHeightConstraint;

@property (strong, nonatomic) NSIndexPath *path;
@property (strong, nonatomic) NSLayoutConstraint *bottomConfirmationViewConstraint;

- (void)configureCellWithDeal:(VADeal *)deal;
- (CGFloat)heightForCellWithDeal:(VADeal *)deal;
@end

@protocol VADealDelegate <NSObject>

- (void)shouldShowProfileForBusiness:(VABusiness *)business;

@end
