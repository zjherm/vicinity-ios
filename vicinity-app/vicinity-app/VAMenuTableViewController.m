//
//  VAMenuTableViewController.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 30.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAMenuTableViewController.h"
#import "VANotificationTool.h"
#import "VAMenuTableViewCell.h"
#import "VAUser.h"
#import "VAUserDefaultsHelper.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "VAUserApiManager.h"
#import "VAControlTool.h"
#import "DEMONavigationController.h"
#import "VAProfileViewController.h"
#import "VANotificationTableViewCell.h"
#import "VABugViewController.h"
#import "VAEventsViewController.h"
#import "VANotificationsTableViewController.h"
#import "VALoginViewController.h"
#import "VALiveDealsViewController.h"
#import "VAVoucherViewController.h"
#import "VAInviteViewController.h"
#import "VAConnectionsViewController.h"
#import "VAYourConnectionsViewController.h"
#import "VAConnectViewController.h"
#import <LaunchKit/LaunchKit.h>

//TemporaryHide
#import "VANotificationMessage.h"

@interface VAMenuTableViewController ()
@property (weak, nonatomic) IBOutlet UITableView *menuTableView;

@property (strong, nonatomic) NSArray *cellIdentifiers;
@property (strong, nonatomic) NSArray *cellHeights;
@end

@implementation VAMenuTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.selectedMenuItemIndex = 1;
    
    self.cellIdentifiers = @[@"Profile", @"Vicinity", @"LiveDeals", @"MyDeals", @"Connections", @"Notifications", @"Invite", @"Feedback", @"Logout"];
    CGFloat myDealsCellHeight;
#ifdef VICINITY
    myDealsCellHeight = 0.f;
#elif STAGING
    myDealsCellHeight = 0.f;
#else
    myDealsCellHeight = 75.0;
#endif
    self.cellHeights = @[@(94), @(75), @(0), @(myDealsCellHeight), @(75), @(75), @(75), @(75), @(75)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateAvatar) name:kDidChangeUserAvatar object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showCellBadge) name:kShowBadgeForMenuIcon object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideCellBadge) name:kHideBadgeForMenuIcon object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(navigateToInviteController) name:kShowInviteScreen object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 9;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VAMenuTableViewCell *cell = (VAMenuTableViewCell *)[tableView dequeueReusableCellWithIdentifier:self.cellIdentifiers[indexPath.row]];
    if (self.selectedMenuItemIndex == indexPath.row) {
        [cell setCellSelected:YES];
    } else {
        [cell setCellSelected:NO];
    }
    
    if (indexPath.row == 1) {
        cell.vicinityImageView.image = [UIImage imageNamed:self.selectedMenuItemIndex == indexPath.row ? @"v-menu-icon" : @"v-menu-icon-not-selected"];
    }
    
    if (indexPath.row == 3) {
#ifdef VICINITY
        [cell.iconContainer setHidden:YES];
#elif STAGING
        [cell.iconContainer setHidden:YES];
#else
        [cell.iconContainer setHidden:NO];
#endif
    }
    
#ifdef VICINITY
    [cell.comingSoonImageView setHidden:NO];
    [cell.liveDealImageView setImage:[UIImage imageNamed:@"live_deals_icon_gray"]];
    cell.liveDealLabel.textColor = [UIColor lightGrayColor];
#elif STAGING
    [cell.comingSoonImageView setHidden:NO];
    [cell.liveDealImageView setImage:[UIImage imageNamed:@"live_deals_icon_gray"]];
    cell.liveDealLabel.textColor = [UIColor lightGrayColor];
#else
    [cell.comingSoonImageView setHidden:YES];
    [cell.liveDealImageView setImage:[UIImage imageNamed:@"live_deals_icon"]];
    cell.liveDealLabel.textColor = [UIColor whiteColor];
#endif
    
    if (indexPath.row == 0) {
        VAUser *user = (VAUser *)[VAUserDefaultsHelper getModelForKey:VASaveKeys_CurrentUser];
        [cell loadAvatarForUser:user];
    }
    
    if (indexPath.row == 5) {
        [cell setNotificationsCount:[UIApplication sharedApplication].applicationIconBadgeNumber];
    }
    return cell;
}
    
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.selectedMenuItemIndex == indexPath.row && indexPath.row != 6) {
        // Selected active tab
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:^{}];
        
        return;
    }
#ifdef VICINITY
    if (indexPath.row == 2 || indexPath.row == 3) {
        return;
    }
#endif
#ifdef STAGING
    if (indexPath.row == 2 || indexPath.row == 3) {
        return;
    }
#endif
    
    if (indexPath.row == 1) {
        VAMenuTableViewCell *cell = (VAMenuTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.vicinityImageView.image = [UIImage imageNamed:@"v-menu-icon"];
    } else if (self.selectedMenuItemIndex == 1) {
        BOOL TemporaryHide = YES;
        if (TemporaryHide && indexPath.row != 6){
            VAMenuTableViewCell *cell = (VAMenuTableViewCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedMenuItemIndex inSection:0]];
            cell.vicinityImageView.image = [UIImage imageNamed:@"v-menu-icon-not-selected"];
        }
    }
    
    if (indexPath.row != 8) { //this cell can't be selected
        BOOL TemporaryHide = YES;
        if (TemporaryHide && indexPath.row != 6){
            VAMenuTableViewCell *cell = (VAMenuTableViewCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedMenuItemIndex inSection:0]];
            [cell setCellSelected:NO];
            self.selectedMenuItemIndex = indexPath.row;
            cell = (VAMenuTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
            [cell setCellSelected:YES];
        }
    }
    

    
    if (indexPath.row == 0) {
        [self goToProfileViewController];
    } else if (indexPath.row == 1) {
        [self goToEventsViewController];
    } else if (indexPath.row == 2) {
        [self goToLiveDealsViewController];
    } else if (indexPath.row == 3) {
        [self goToMyDealsViewController];
    } else if (indexPath.row == 4) {
        [self goToConnectionsController];
    } else if (indexPath.row == 5) {
        [self goToNotificationsViewController];
    } else if (indexPath.row == 6) {
        [self goToInviteController];
    } else if (indexPath.row == 7) {
        [self goToFeedbackViewController];
    } else if (indexPath.row == 8) {
        [self showLogoutAlert];
    }

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    BOOL TemporaryHide = YES;
    if (TemporaryHide && indexPath.row == 6){
        
    }
    else {
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:^{}];
    }

}

#pragma mark - UITableViewDelegate
    
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self.cellHeights[indexPath.row] integerValue];
}

#pragma mark - Methods

- (void)updateAvatar {
    VAUser *user = (VAUser *)[VAUserDefaultsHelper getModelForKey:VASaveKeys_CurrentUser];
    VAMenuTableViewCell *profileCell = (VAMenuTableViewCell *)[self.menuTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [profileCell loadAvatarForUser:user];
}

- (void)showCellBadge {
    VAMenuTableViewCell *cell = (VAMenuTableViewCell *)[self.menuTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:5 inSection:0]];
    [cell setNotificationsCount:[UIApplication sharedApplication].applicationIconBadgeNumber];
    [self.menuTableView reloadData];
}

- (void)hideCellBadge {
    VAMenuTableViewCell *cell = (VAMenuTableViewCell *)[self.menuTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:5 inSection:0]];
    [cell setNotificationsCount:0];
    [self.menuTableView reloadData];
}

- (void)clearDeviceTokenOnServer {
    [[VAUserApiManager alloc] getLogoutWithDeviceToken:[VAUserDefaultsHelper getDeviceToken] andCompletion:^(NSError *error, VAModel *model) {
        
    }];
}

- (void)showLogoutAlert {
    if (IOS8 || IOS9) {
        UIAlertController *ac = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"alert.confirm.logout", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:NSLocalizedString(@"alert.logout.button", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [[VAControlTool defaultControl] logout];
        }];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"alert.cancel.button", nil) style:UIAlertActionStyleCancel handler:nil];
        [ac addAction:actionOk];
        [ac addAction:cancel];
        [self presentViewController:ac animated:YES completion:nil];
    } else if (IOS7) {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alert.confirm.logout", nil)
                                    message:nil
                                   delegate:self
                          cancelButtonTitle:NSLocalizedString(@"alert.cancel.button", nil)
                          otherButtonTitles:NSLocalizedString(@"alert.logout.button", nil), nil] show];
    }
    
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [[VAControlTool defaultControl] logout];
    }
}

#pragma mark - Navigation

- (void)goToProfileViewController {
    VAProfileViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"profileViewController"];
    DEMONavigationController *nav = (DEMONavigationController *)self.containerViewController.centerViewController;
    [nav setViewControllers:@[vc] animated:NO];
}

- (void)goToEventsViewController {
    VAEventsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"VAEventsViewController"];
    DEMONavigationController *nav = (DEMONavigationController *)self.containerViewController.centerViewController;
    [nav setViewControllers:@[vc] animated:NO];
}

- (void)goToNotificationsViewController {
    VANotificationsTableViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"VANotificationsTableViewController"];
    DEMONavigationController *nav = (DEMONavigationController *)self.containerViewController.centerViewController;
    [nav setViewControllers:@[vc] animated:NO];
}

- (void)goToLiveDealsViewController {
    VALiveDealsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"VALiveDealsViewController"];
    DEMONavigationController *nav = (DEMONavigationController *)self.containerViewController.centerViewController;
    [nav setViewControllers:@[vc] animated:NO];
}

- (void)goToMyDealsViewController {
    VAVoucherViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"VAVoucherViewController"];
    DEMONavigationController *nav = (DEMONavigationController *)self.containerViewController.centerViewController;
    [nav setViewControllers:@[vc] animated:NO];
}

- (void)goToFeedbackViewController {
    VABugViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"VABugViewController"];
    DEMONavigationController *nav = (DEMONavigationController *)self.containerViewController.centerViewController;
    [nav setViewControllers:@[vc] animated:NO];
}

- (void)goToConnectionsController {
    VAConnectViewController *vc = [[UIStoryboard storyboardWithName:@"Vicinity" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"ConnectViewController"];
    DEMONavigationController *nav = (DEMONavigationController *)self.containerViewController.centerViewController;
    [nav setViewControllers:@[vc] animated:NO];
}

- (void)goToInviteController {
    BOOL TemporaryHide = YES;
    if (TemporaryHide) {
        [[VAControlTool defaultControl] showShareSheetWithCompletion:^(NSString * _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError) {
            if (completed) {
                [VANotificationMessage showShareSuccessMessageOnController:[VAControlTool topScreenController]];
            }
        }];
    } else {
        VAConnectViewController *vc = [[UIStoryboard storyboardWithName:@"Vicinity" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"ConnectViewController"];
        vc.isInviteTabDefault = YES;
        DEMONavigationController *nav = (DEMONavigationController *)self.containerViewController.centerViewController;
        [nav setViewControllers:@[vc] animated:NO];
    }
}

- (void)navigateToInviteController{
    NSIndexPath* ip = [NSIndexPath indexPathForItem:6 inSection:0];
    [self tableView:self.menuTableView didSelectRowAtIndexPath:ip];
}

@end
