//
//  VAFilterTableViewFooter.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 25.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VAFilterVIewProtocol;

@interface VAFilterTableViewFooterView : UIView
@property (weak, nonatomic) id <VAFilterVIewProtocol> delegate;
@end

@protocol VAFilterVIewProtocol <NSObject>
- (void)cancelFilters;
- (void)applyFilters;
@end
