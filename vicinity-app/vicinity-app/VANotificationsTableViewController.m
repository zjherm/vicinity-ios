//
//  VANotificationsTableViewController.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 17.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VANotificationsTableViewController.h"
#import "VANotificationTableViewCell.h"
#import "REFrostedViewController.h"
#import "DEMONavigationController.h"
#import "VAUserDefaultsHelper.h"
#import "UIBarButtonItem+Badge.h"
#import "VANotificationTool.h"
#import "VALoaderView.h"
#import "VAUser.h"
#import "VAUserApiManager.h"
#import "VANotificationMessage.h"
#import "VANotificationsResponse.h"
#import "VANotification.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSDate+VAString.h"
#import "VACommentsViewController.h"
#import "VALoaderView.h"
#import "VAProfileViewController.h"
#import "VAControlTool.h"
#import "VAEventsViewController.h"
#import "UILabel+VACustomFont.h"
#import <Google/Analytics.h>

typedef NS_ENUM(NSInteger, VANotificationType) {
    VANotificationTypeComment = 0,
    VANotificationTypeLike,
    VANotificationTypeMention,
    VANotificationTypeConnection,
    VANotificationTypeCommentPublish
};

@interface VANotificationsTableViewController () <TTTAttributedLabelDelegate>
@property (strong, nonatomic) NSArray *notificationsArray;
@property (strong, nonatomic) UIBarButtonItem *leftItem;
@property (strong, nonatomic) VALoaderView *headerLoader;
@property (assign, nonatomic) BOOL newNotification;
@property (assign, nonatomic) BOOL isRefreshing;
@property (assign, nonatomic) NSInteger lastNotificationsArrayCount;
@property (strong, nonatomic) VALoaderView *refresh;

@end

CGFloat kAvatarHeight = 36.f;

@implementation VANotificationsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.notificationsArray = [NSArray array];
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Menu Icon"] style:UIBarButtonItemStylePlain target:self.navigationController action:@selector(showMenu)];
    self.navigationItem.leftBarButtonItem = leftItem;
    self.leftItem = leftItem;
    
    if ([VAUserDefaultsHelper shouldShowNotificationBadge]) {
        [leftItem showBadge];
    }
    
    self.newNotification = NO;
    self.isRefreshing = NO;
    
    VALoaderView *loader = [VALoaderView initLoader];
    loader.frame = CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), kLoaderHeight);
    [loader startAnimating];
    self.tableView.tableHeaderView = loader;
    self.headerLoader = loader;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.scrollEnabled = NO;
    
    [self getNotificationsForCurrentUser];
    
    [self setupRefreshControl];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [(DEMONavigationController *)self.navigationController setDefaultNavigationBar];
    
    self.navigationController.title = @"Notifications";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showButtonBadge) name:kShowBadgeForMenuIcon object:nil];
    
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Notifications screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.notificationsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *likeIdentifier = @"Like";
    static NSString *mentionIdentifier = @"Mention";
    static NSString *commentIdentifier = @"Comment";
    static NSString *connectionIdentifier = @"Connection";
    
    VANotification *notification = [self.notificationsArray objectAtIndex:indexPath.row];
    
    NSString *identifier = nil;
    
    if ([self notificationTypeForString:notification.eventType] == VANotificationTypeMention) {
        identifier = mentionIdentifier;
    } else if ([self notificationTypeForString:notification.eventType] == VANotificationTypeLike) {
        identifier = likeIdentifier;
    } else if ([self notificationTypeForString:notification.eventType] == VANotificationTypeComment) {
        if (!notification.text || [notification.text isEqualToString:@""]) {
            identifier = mentionIdentifier;
        } else {
            identifier = commentIdentifier;
        }
    } else if ([self notificationTypeForString:notification.eventType] == VANotificationTypeConnection) {
        identifier = connectionIdentifier;
    } else if ([self notificationTypeForString:notification.eventType] == VANotificationTypeCommentPublish) {
        identifier = commentIdentifier;
    }
    
    VANotificationTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    VANotification *notification = [self.notificationsArray objectAtIndex:indexPath.row];
    
    return [self heightForNotification:notification];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    VANotification *notification = [self.notificationsArray objectAtIndex:indexPath.row];
    if ([self notificationTypeForString:notification.eventType] == VANotificationTypeConnection) {
        //open connections is not logical, do smth here
        [self showProfileForUserWithAlias:notification.user.nickname];
    } else {
        NSString *postID = notification.postID;
        
        VACommentsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"VACommentsViewController"];
        vc.postID = postID;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - Methods

- (void)configureCell:(VANotificationTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    
    VANotification *notification = [self.notificationsArray objectAtIndex:indexPath.row];
    VAUser *user = notification.user;
    
    if (user.avatarURL) {
        NSURL *imageURL = [NSURL URLWithString:user.avatarURL];
        
        [cell.avatarView sd_setImageWithURL:imageURL
                           placeholderImage:[UIImage imageNamed:@"avatar-placeholder"]
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                      if (image) {
                                          cell.avatarView.image = image;
                                      }
                                      if (error) {
                                          [VANotificationMessage showAvatarErrorMessageOnController:self withDuration:5.f];
                                      }
                                  }];
    } else {
        cell.avatarView.image = [UIImage imageNamed:@"avatar-placeholder"];
    }
    
    NSString *text = notification.eventText;
    NSString *alias = user.nickname;
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:text
                                                                                attributes:@{
                                                                                             NSFontAttributeName : [[VADesignTool defaultDesign] vicinityRegularFontOfSize:12.f],
                                                                                             NSForegroundColorAttributeName: [UIColor blackColor]
                                                                                             }];
                                                    
    [attrStr addAttributes:@{
                             NSFontAttributeName: [[VADesignTool defaultDesign] vicinityBoldFontOfSize:12.f],
                             NSForegroundColorAttributeName: [[VADesignTool defaultDesign] vicinityDarkBlue]
                             }
                     range:[text rangeOfString:alias]];
    cell.generalInfoLabel.attributedText = attrStr;
    
    cell.generalInfoLabel.activeLinkAttributes = @{NSForegroundColorAttributeName: [[VADesignTool defaultDesign] vicinityDarkBlue]};
    cell.generalInfoLabel.linkAttributes = @{NSForegroundColorAttributeName: [[VADesignTool defaultDesign] vicinityDarkBlue]};
    
    [cell.generalInfoLabel addLinkToURL:[NSURL URLWithString:@"name"] withRange:[cell.generalInfoLabel.text rangeOfString:user.nickname]];
    
    if ([self notificationTypeForString:notification.eventType] != VANotificationTypeMention && [self notificationTypeForString:notification.eventType] != VANotificationTypeConnection && notification.text && ![notification.text isEqualToString:@""]) {
        NSMutableAttributedString *detailStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\"%@\"", notification.text ? notification.text : @""]
                                                                                    attributes:@{
                                                                                                 NSFontAttributeName : [[VADesignTool defaultDesign] vicinityRegularFontOfSize:12.f],
                                                                                                 NSForegroundColorAttributeName: [UIColor blackColor]
                                                                                                 }];
        cell.detailInfoLabel.attributedText = detailStr;

        [cell.detailInfoLabel addPostAtrributes];
        
        
        
    } else {
        cell.detailInfoLabel.text = @"";
    }
    
    cell.dateLabel.text = [notification.date messageTimeForDate];
    
    cell.avatarView.layer.cornerRadius = kAvatarHeight / 2;
    cell.avatarView.clipsToBounds = YES;
    
    cell.user = user;
    
    if (indexPath.row == [self.notificationsArray count] - 1) {
        [cell.separatorView setHidden:YES];
    } else {
        [cell.separatorView setHidden:NO];
    }
}

- (void)showButtonBadge {
    [self.leftItem showBadge];
    self.newNotification = YES;
    [self getNotificationsForCurrentUser];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{       //2 secs message will be shown
        [self.leftItem hideBadge];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kShouldShowNotificationBadge];
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    });
}

- (void)getNotificationsForCurrentUser {
    VAUserApiManager *manager = [VAUserApiManager new];
    [manager getNotificationsWithCompletion:^(NSError *error, VAModel *model) {
        if (!self.isRefreshing) {
            [self.headerLoader stopAnimating];
        } else {
            [self.refresh stopRefreshing];
            self.isRefreshing = NO;
        }

        self.tableView.tableHeaderView = nil;
        self.tableView.scrollEnabled = YES;

        if (error) {
            [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
        } else if (model) {
            [[self mutableArrayValueForKey:@"notificationsArray"] removeAllObjects];

            VANotificationsResponse *response = (VANotificationsResponse *)model;
            if ([response.status isEqualToString:kSuccessResponseStatus]) {
                [[self mutableArrayValueForKey:@"notificationsArray"] addObjectsFromArray:response.notifications];
                
                if (self.newNotification) {
                    NSMutableArray *indexPsths = [NSMutableArray array];
                    for (int i = 0; i < [self.notificationsArray count] - self.lastNotificationsArrayCount; i++) {
                        [indexPsths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                    }
                    
                    [self.tableView beginUpdates];
                    [self.tableView insertRowsAtIndexPaths:indexPsths withRowAnimation:UITableViewRowAnimationTop];
                    [self.tableView endUpdates];
                    
                    [self clearMenuNotifications];
                    
                    self.newNotification = NO;

                } else {
                    [self clearMenuNotifications];
                    [self.tableView reloadData];
                }
                
                if ([self.notificationsArray count] == 0) {
                    UIView *noNotificationsView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 135.f)];
                    UIImageView *noNotificationImageView = [[UIImageView alloc] initWithFrame:CGRectMake((CGRectGetWidth(self.view.bounds) - 196.f) / 2.f, 30.f, 196.f, 105.f)];
                    noNotificationImageView.image = [UIImage imageNamed:@"no_notifications_icon.png"];
                    [noNotificationsView addSubview:noNotificationImageView];
                    self.tableView.tableHeaderView = noNotificationsView;
                } else {
                    self.tableView.tableHeaderView = nil;
                }
            } else if ([response.status isEqualToString:kErrorResponseStatus]) {

                [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
            }
        }
        self.lastNotificationsArrayCount = [self.notificationsArray count];
    }];
}

- (VANotificationType)notificationTypeForString:(NSString *)notificationTypeString {
    if ([notificationTypeString isEqualToString:@"mention.post"] || [notificationTypeString isEqualToString:@"mention.comment"]) {
        return VANotificationTypeMention;
    } else if ([notificationTypeString isEqualToString:@"comment"]) {
        return VANotificationTypeComment;
    } else if ([notificationTypeString isEqualToString:@"like"]) {
        return VANotificationTypeLike;
    } else if ([notificationTypeString isEqualToString:@"connection"]) {
        return VANotificationTypeConnection;
    } else if ([notificationTypeString isEqualToString:@"comment.publish"]) {
        return VANotificationTypeCommentPublish;
    } else {
        return NSIntegerMax;
    }
}

- (void)setupRefreshControl {
    VALoaderView *refresh = [VALoaderView initLoader];
    self.refresh = refresh;
    [refresh setFrame:[self frameForCustomRefreshControl]];
    refresh.alpha = 0.f;
    
    [refresh addTarget:self
                action:@selector(refreshData)
      forControlEvents:UIControlEventValueChanged];

    [self.tableView addSubview:refresh];
}

- (CGRect)frameForCustomRefreshControl {
    CGFloat customRefreshControlHeight = kLoaderHeight;
    CGFloat customRefreshControlWidth = CGRectGetWidth([self.tableView frame]);
    CGRect customRefreshControlFrame = CGRectMake(0.0f,
                                                  -customRefreshControlHeight,
                                                  customRefreshControlWidth,
                                                  customRefreshControlHeight);
    return customRefreshControlFrame;
}

- (void)refreshData {
    self.isRefreshing = YES;
    [self.refresh startRefreshing];
    [self getNotificationsForCurrentUser];
}

- (void)clearMenuNotifications {
    [self postNotificationSeenToServer];
}

- (void)postNotificationSeenToServer {
    VAUserApiManager *manager = [VAUserApiManager new];
    [manager postNotificationSeenWithCompletion:^(NSError *error, VAModel *model) {
        if (model) {
            [self.leftItem hideBadge];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kShouldShowNotificationBadge];
            [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
            [[NSNotificationCenter defaultCenter] postNotificationName:kHideBadgeForMenuIcon object:nil];
        }
    }];
}

- (void)showProfileForUserWithAlias:(NSString *)alias {
    VAProfileViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"profileViewController"];
    vc.searchAlias = alias;
    vc.isUsersAccountMode = YES;
    VANotificationsTableViewController *currentVC = (VANotificationsTableViewController *)[VAControlTool topScreenController];
    [currentVC.navigationController pushViewController:vc animated:YES];
}

- (CGFloat)heightForNotification:(VANotification *)notification {
    
    UILabel *label = [UILabel new];
    
    CGFloat baseHeight = 60.f;
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width - 85.f;

    CGFloat firstLabelHeight = [label heightForNotificationEventLabellWithWidth:width andNotification:notification];
    
    CGFloat secondHeight = [label heightForNotificationTextLabelWithWidth:width andNotification:notification];
    
    return baseHeight + firstLabelHeight + secondHeight;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self.refresh containingScrollViewDidEndDragging:scrollView];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.refresh containingScrollViewDidScroll:scrollView];
}


@end
