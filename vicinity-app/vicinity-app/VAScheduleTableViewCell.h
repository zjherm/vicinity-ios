//
//  VAScheduleTableViewCell.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 14.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VAScheduleTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@end
