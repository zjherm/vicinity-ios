//
//  main.m
//  vicinity-app
//
//  Created by Panda Systems on 5/18/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VAAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([VAAppDelegate class]));
    }
}
