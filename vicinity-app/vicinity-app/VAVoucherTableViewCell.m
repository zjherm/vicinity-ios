//
//  VAVoucherTableViewCell.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 04.08.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAVoucherTableViewCell.h"
#import "VABusiness.h"
#import "VADeal.h"
#import "UILabel+VACustomFont.h"

@interface VAVoucherTableViewCell ()
@property (strong, nonatomic) VADeal *deal;
@property (strong, nonatomic) VABusiness *business;
@end

CGFloat voucherCellWidth;

@implementation VAVoucherTableViewCell

- (void)awakeFromNib {
    self.layer.cornerRadius = 5.f;
    self.clipsToBounds = YES;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)configureCellWithDeal:(VADeal *)deal {
    VABusiness *business = deal.business;
    
    self.business = business;
    self.deal = deal;
    
    [self setAvatarForBusiness:business];
    
    [self setNameForBusiness:business];
    
    [self setDescriptionTextForDeal:deal];
        
    [self setImageForDeal:deal];
    
    [self setVoucherExpireDateForDeal:deal];
    
    [self setVoucherNumberForDeal:deal];
}

- (void)setAvatarForBusiness:(VABusiness *)business {
    //    if (business.avatarURL) {
    //        NSURL *imageURL = [NSURL URLWithString:business.avatarURL];
    //
    //        [self.avatarImageView sd_setImageWithURL:imageURL
    //                           placeholderImage:[UIImage imageNamed:@"placeholder.png"]
    //                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    //                                      if (image) {
    //                                          self.avatarImageView.image = image;
    //                                      }
    //                                      if (error) {
    //                                          VALiveDealsViewController *currentVC = (VALiveDealsViewController *)[VAControlTool topScreenController];
    //                                          [VANotificationMessage showAvatarErrorMessageOnController:currentVC withDuration:5.f];
    //                                      }
    //                                  }];
    //    } else {
    //        self.avatarView.image = [UIImage imageNamed:@"placeholder.png"];
    //    }
    
    self.avatarImageView.image = [UIImage imageNamed:business.avatarURL];
}

- (void)setNameForBusiness:(VABusiness *)business {
    self.nameLabel.text = business.name;
}

- (void)setDescriptionTextForDeal:(VADeal *)deal {
    self.descriptionLabel.text = deal.shortDescription;
}

- (void)setImageForDeal:(VADeal *)deal {
    
    //    if (deal.photoURL) {
    //        NSURL *imageURL = [NSURL URLWithString:deal.photoURL];
    //
    //        [self.dealImageView sd_setImageWithURL:imageURL
    //                              placeholderImage:[UIImage imageNamed:@"placeholder.png"]
    //                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    //                                         if (image) {
    //                                             self.dealImageView.image = image;
    //                                         }
    //                                         if (error) {
    //                                             VALiveDealsViewController *currentVC = (VALiveDealsViewController *)[VAControlTool topScreenController];
    //                                             [VANotificationMessage showAvatarErrorMessageOnController:currentVC withDuration:5.f];
    //                                         }
    //                                     }];
    //
    //        self.imageContainerHeight.priority = 250.f;
    //    } else {
    //        self.dealImageView.image = nil;
    //        self.imageContainerHeight.priority = 900.f;
    //    }
    
    self.dealImageView.image = [UIImage imageNamed:deal.photoURL];
}

- (void)setVoucherExpireDateForDeal:(VADeal *)deal {
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"dd/MM/yy"];
    self.expirationLabel.text = [formatter stringFromDate:deal.expirationDate];
}

-(void)setVoucherNumberForDeal:(VADeal *)deal {
    self.voucherNumberLabel.text = deal.serialNumber;
}

#pragma mark - Height methods

- (CGFloat)heightForCellWithDeal:(VADeal *)deal {
    voucherCellWidth = CGRectGetWidth([[UIScreen mainScreen] bounds]) - 2 * 16.f;
    
    CGFloat baseHeight = 93.f;
    CGFloat nameLabelHeight = [self heightForNameLabelWithBusiness:deal.business];
    CGFloat descriptionLabelHeight = [self heightForDescriptionLabelWithDeal:deal];
    CGFloat imageHeight = voucherCellWidth;
    
    return baseHeight + nameLabelHeight + descriptionLabelHeight +imageHeight;
}

- (CGFloat)heightForNameLabelWithBusiness:(VABusiness *)business {
    CGFloat nameLabelLeadingConstraint = 75.f;
    CGFloat nameLabelTrailingConstraint = 16.f;
    return [self.nameLabel heightForLabelWithWidth:voucherCellWidth - nameLabelLeadingConstraint - nameLabelTrailingConstraint font:[[VADesignTool defaultDesign] vicinityRegularFontOfSize:13.f] andTitile:business.name];
}

- (CGFloat)heightForDescriptionLabelWithDeal:(VADeal *)deal {
    CGFloat descriptionLabelLeadingConstraint = 75.f;
    CGFloat descriptionLabelTrailingConstraint = 16.f;
    return [self.descriptionLabel heightForLabelWithWidth:voucherCellWidth - descriptionLabelLeadingConstraint - descriptionLabelTrailingConstraint font:[[VADesignTool defaultDesign] vicinityRegularFontOfSize:13.f] andTitile:deal.shortDescription];
}

@end
