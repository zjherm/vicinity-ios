//
//  VAFiltersTool.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 26.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAFiltersTool.h"

@implementation VAFiltersTool

+ (VAFiltersTool *)currentFilters {
    static VAFiltersTool *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [VAFiltersTool new];
    });
    return shared;
}

- (void)saveDistance:(NSString *)distance {
    [[NSUserDefaults standardUserDefaults] setObject:distance forKey:kDistanceFilter];
}

- (NSString *)loadDistance {
    NSString *distance = [[NSUserDefaults standardUserDefaults] objectForKey:kDistanceFilter];
    return distance;
}

- (void)saveCategories:(NSArray *)categories {
    [[NSUserDefaults standardUserDefaults] setObject:categories forKey:kcategoriesFilter];
}

- (NSArray *)loadCategories {
    NSArray *categories = [[NSUserDefaults standardUserDefaults] objectForKey:kcategoriesFilter];
    return categories;
}

@end
