//
//  VAFacebookPlaceTableViewCell.m
//  vicinity-app
//
//  Created by Panda Systems on 10/12/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAFacebookPlaceTableViewCell.h"
#import "NSNumber+VAMilesDistance.h"

@interface VAFacebookPlaceTableViewCell ()

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *addressLabel;
@property (nonatomic, weak) IBOutlet UILabel *distanceLabel;

@end

@implementation VAFacebookPlaceTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureWithFacebookPlace:(NSDictionary *)place {
    self.nameLabel.text = place[@"name"];
    self.addressLabel.text = place[@"location"][@"street"];
    self.distanceLabel.text = [place[@"distance"] milesStringFromNSNumberMeters:YES];
}

- (void)setDefaultProperties {
    self.backgroundColor = [UIColor whiteColor];
    self.nameLabel.textColor = [UIColor blackColor];
    self.addressLabel.textColor = [self labelColor];
    self.distanceLabel.textColor = [self labelColor];
}

- (void)setSelectedProperties {
    self.backgroundColor = [[VADesignTool defaultDesign] vicinityBlue];
    self.nameLabel.textColor = [UIColor whiteColor];
    self.addressLabel.textColor = [UIColor whiteColor];
    self.distanceLabel.textColor = [UIColor whiteColor];
}

- (UIColor *)labelColor {
    return [UIColor colorWithRed:99.0/255.0
                           green:107.0/255.0
                            blue:128.0/255.0
                           alpha:1.0];
}

@end
