//
//  VACommentsViewController.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 28.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VACommentsViewController.h"
#import "VACommentTableViewCell.h"
#import "UILabel+VACustomFont.h"
#import "VADesignTool.h"
#import "VAUserApiManager.h"
#import "VAUserDefaultsHelper.h"
#import "VAUser.h"
#import "VAPost.h"
#import "VAComment.h"
#import "VAServerResponse.h"
#import "VACommentsResponse.h"
#import "VANotificationMessage.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSDate+VAString.h"
#import "VALoaderView.h"
#import "VAUserSearchResponse.h"
#import "VAEventsViewController.h"
#import "VAProfileViewController.h"
#import "VAUserSearchTableViewCell.h"
#import "NSNumber+VAMilesDistance.h"
#import "VAControlTool.h"
#import "VAGetPostResponse.h"
#import <TTTAttributedLabel.h>
#import <OpenInGoogleMapsController.h>
#import "VAPostTableViewCell.h"
#import "UILabel+VACustomFont.h"
#import "DEMONavigationController.h"
#import "VALikeListTableViewController.h"
#import "UITableViewCell+VAParentCell.h"
#import <LGAlertView.h>
#import "VANewCommentResponse.h"
#import "TTTAttributedLabel+VALinks.h"
#import <Google/Analytics.h>

#import "VACommentsTableViewCell.h"
#import "VAPostActionsView.h"
#import "VAPostActionsService.h"
#import "VAReportViewController.h"

@interface VACommentsViewController () <UITableViewDataSource, UITableViewDelegate, VACommentToProfileDelegate, TTTAttributedLabelDelegate, UIActionSheetDelegate, VACommentsInteractionDelegate, VAReportSendingDelegate, VAPostActionsDelegate>
@property (strong, nonatomic) NSArray *commentsArray;
@property (strong, nonatomic) NSArray *usersArray;
@property (strong, nonatomic) NSIndexPath *bottomVisiblePath;
@property (strong, nonatomic) VALoaderView *footerLoader;      //loader for footer on post/comment loading
@property (assign, nonatomic) BOOL shouldSearchUser;
@property (assign, nonatomic) BOOL isSearchTableShown;
@property (assign, nonatomic) BOOL isRefreshing;
@property (assign, nonatomic) BOOL connectionError;
@property (strong, nonatomic) UITableView *searchTable;
@property (strong, nonatomic) NSString *searchString;
@property (strong, nonatomic) NSTimer *searchTimer;
@property (strong, nonatomic) VALoaderView *loader;            //loader for tableView with mentioned users
@property (strong, nonatomic) VALoaderView *refresh;           //loader for pull to refresh control
@property (strong, nonatomic) UITapGestureRecognizer *tapBackground;           //tap for dismissing keyboard
@property (strong, nonatomic) id elementToEdit;
@property (strong, nonatomic) NSString *editTitle;
@property (strong, nonatomic) NSString *deleteTitle;

@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (weak, nonatomic) IBOutlet UIButton *postButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *commentVew;
@property (weak, nonatomic) IBOutlet UILabel *placeholderLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;

// Pop up view
@property (nonatomic, strong) UIView *popUpView;
@property (nonatomic, strong) VAPostActionsView *postActionsView;

//prevent multiple call of keyboardWillAppear while background/foreground
@property (nonatomic) BOOL isKeyboardShown;

- (IBAction)actionSendComment:(id)sender;
@end

CGFloat kPostAvatarHeight = 48.f;
CGFloat kCommentAvatarHeight = 37.f;

static NSString *postIdentifier = @"PostCell";
//static NSString *commentIdentifier = @"CommentCell";

@implementation VACommentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.navigationController.navigationBar.topItem.title = @"";
    self.navigationController.navigationBar.translucent = NO;
    
    self.bottomVisiblePath = nil;
    self.searchTimer = nil;
    self.loader = nil;
    self.shouldSearchUser = NO;
    self.isSearchTableShown = NO;
    self.isRefreshing = NO;
    self.isKeyboardShown = NO;
    
    UITapGestureRecognizer* tapBackground = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
    [tapBackground setNumberOfTapsRequired:1];
    self.tapBackground = tapBackground;                 //we can not add this gesture recognizer for tableView, becouse it will break touch detection for TTTAttributedLabel and STTweetLabel
    
    self.postButton.layer.cornerRadius = 5.f;
    self.postButton.clipsToBounds = YES;
    self.postButton.enabled = 0;
    self.postButton.backgroundColor = [UIColor lightGrayColor];

    self.commentsArray = [NSArray array];
    self.usersArray = [NSArray array];
    
    VALoaderView *loader = [VALoaderView initLoader];
    loader.frame = CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), kLoaderHeight);
    [loader startAnimating];
    self.footerLoader = loader;
    self.tableView.tableFooterView = loader;
    
    if (self.post) {
        [self loadCommentsFromServer];
    } else if (self.postID){
        [self loadPostWithPostID:self.postID];
    }
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackButton"] style:UIBarButtonItemStylePlain target:self.navigationController action:@selector(popViewControllerAnimated:)];
    self.navigationItem.leftBarButtonItem = leftButton;
    
    [self setupRefreshControl];
    
    UINib *commentCellNib = [UINib nibWithNibName:@"VACommentsTableViewCell" bundle:[NSBundle mainBundle]];
    [self.tableView registerNib:commentCellNib forCellReuseIdentifier:@"CommentsCell"];
    
    UINib *commentRemovedCellNib = [UINib nibWithNibName:@"VACommentsTableViewCell" bundle:[NSBundle mainBundle]];
    [self.tableView registerNib:commentRemovedCellNib forCellReuseIdentifier:@"CommentsRemovedCell"];
    
    UINib *commentDeletedCellNib = [UINib nibWithNibName:@"VACommentsTableViewCell" bundle:[NSBundle mainBundle]];
    [self.tableView registerNib:commentDeletedCellNib forCellReuseIdentifier:@"CommentsDeletedCell"];
    [self setupActionsPopUp];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    self.navigationController.title = @"Post";
    
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Comments screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [(DEMONavigationController *)self.navigationController setDefaultNavigationBar];

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (!self.isSearchTableShown) {
        if (self.post) {
            return [self.commentsArray count] + 1;
        } else {
            return [self.commentsArray count];
        }
    } else {
        return [self.usersArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!self.isSearchTableShown) {
        if (indexPath.row == 0) {
            return [self basicCellForIndexPath:indexPath];
        } else {
            VAComment *comment = self.commentsArray[indexPath.row - 1];
            NSString *cellIdentifier = @"CommentsCell";
            switch (comment.commentStatus) {
                case VACommentStatusActive:
                    cellIdentifier = @"CommentsCell";
                    break;
                case VACommentStatusRemoved:
                    cellIdentifier = @"CommentsRemovedCell";
                    break;
                case VACommentStatusDeleted:
                    cellIdentifier = @"CommentsDeletedCell";
                    break;
                default:
                    break;
            }
            VACommentsTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
            cell.commentDelegate = self;
            [cell configureCellWithComment:comment];
            return cell;
        }
    } else {
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        return [self searchTableCellForIndexPath:indexPath];
    }
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!self.isSearchTableShown) {
        return [self heightForBasicCellAtIndexPath:indexPath];
    } else {
        return 44.f;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.isSearchTableShown) {
        VAUser *selectedUser = [self.usersArray objectAtIndex:indexPath.row];
        NSString *nickname = selectedUser.nickname;
        NSMutableArray *words = [NSMutableArray arrayWithArray:[self.commentTextView.text componentsSeparatedByString:@" "]];
        [words replaceObjectAtIndex:[words count] - 1 withObject:[NSString stringWithFormat:@"@%@", nickname]];
        NSString *commentString = [words componentsJoinedByString:@" "];
        self.commentTextView.text = [commentString stringByAppendingString:@" "];
        [self hideSearchTable];
    }
}

#pragma mark - Keyboard actions

- (void)willKeyboardShow:(NSNotification*)notification {
    NSDictionary* info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    CGFloat height = kbSize.height;
    /*self.bottomVisiblePath = [[self.tableView indexPathsForVisibleRows] lastObject];
    [self.tableView scrollToRowAtIndexPath:self.bottomVisiblePath atScrollPosition:UITableViewScrollPositionTop animated:YES];*/
    self.bottomConstraint.constant = height;
    //[self.tableView reloadInputViews];
    
    if (!self.isKeyboardShown) {
        self.isKeyboardShown = YES;
        [UIView animateWithDuration:0.3f animations:^{
            [self.tableView setFrame:CGRectMake(CGRectGetMinX(self.tableView.frame), CGRectGetMinY(self.tableView.frame), CGRectGetWidth(self.tableView.frame), CGRectGetHeight(self.tableView.frame) - height)];
            
            [self.commentVew setFrame:CGRectMake(CGRectGetMinX(self.commentVew.frame), CGRectGetMinY(self.commentVew.frame) - height, CGRectGetWidth(self.commentVew.frame), CGRectGetHeight(self.commentVew.frame))];
        }];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.commentsArray count] inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    });

   /* [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.commentsArray count] inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];*/
    [self.tableView addGestureRecognizer:self.tapBackground];

}

- (void)willKeyboardHide:(NSNotification*)notification {
    self.isKeyboardShown = NO;
    NSDictionary* info = [notification userInfo];
    CGRect keyRect;
    [[info objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyRect];
    CGFloat height = keyRect.size.height;
    
    self.bottomConstraint.constant = 0.0;
    
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.tableView setFrame:CGRectMake(CGRectGetMinX(self.tableView.frame), CGRectGetMinY(self.tableView.frame), CGRectGetWidth(self.tableView.frame), CGRectGetHeight(self.tableView.frame) + height)];
        
        
        [self.commentVew setFrame:CGRectMake(CGRectGetMinX(self.commentVew.frame), CGRectGetMinY(self.commentVew.frame) + height, CGRectGetWidth(self.commentVew.frame), CGRectGetHeight(self.commentVew.frame))];
    }];
    
    [self.tableView removeGestureRecognizer:self.tapBackground];
}

#pragma mark - Methods

- (VACommentTableViewCell *)basicCellForIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = nil;
//    if (indexPath.row == 0) {
        identifier = postIdentifier;
//    } else {
//        identifier = commentIdentifier;
//    }
    VACommentTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier];
    cell.post = self.post;
//    if (indexPath.row == 0) {
        [self configurePostCell:cell atIndexPath:indexPath];
//    } else {
//        [self configureCell:cell atIndexPath:indexPath];
//    }
    return cell;
}

- (VAUserSearchTableViewCell *)searchTableCellForIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"UserCell";
    VAUserSearchTableViewCell *cell = [self.searchTable dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        [self.searchTable registerNib:[UINib nibWithNibName:@"VAUserSearchTableViewCell" bundle:nil] forCellReuseIdentifier:identifier];
        cell = [self.searchTable dequeueReusableCellWithIdentifier:identifier];
    }
    VAUser *user = nil;
    if ([self.usersArray count] > 0) {
        user = [self.usersArray objectAtIndex:indexPath.row];
    }
    
    cell.aliasLabel.text = user.nickname;
    cell.fullNameLabel.text = user.fullname;
    
    cell.avatarView.image = nil;

    if (user.avatarURL) {
        NSURL *imageURL = [NSURL URLWithString:user.avatarURL];
        
        [cell.avatarView sd_setImageWithURL:imageURL
                                placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                           if (image) {
                                               cell.avatarView.image = image;
                                           }
                                           if (error) {
                                               [VANotificationMessage showAvatarErrorMessageOnController:self withDuration:5.f];
                                           }
                                       }];
    } else {
        cell.avatarView.image = [UIImage imageNamed:@"placeholder.png"];
    }
    
    cell.avatarView.layer.cornerRadius = 36.f / 2;
    cell.avatarView.clipsToBounds = YES;
    
    if (indexPath.row == [self.usersArray count] - 1) {
        [cell.separatorView setHidden:YES];
    } else {
        [cell.separatorView setHidden:NO];
    }
    
    return cell;
}

- (CGFloat)heightForBasicCellAtIndexPath:(NSIndexPath *)indexPath {
    static VACommentTableViewCell *commentCell = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        commentCell = [self.tableView dequeueReusableCellWithIdentifier:postIdentifier];
    });

    if (indexPath.row == 0) {
        return [commentCell heightForCellWithPost:self.post];            //it doesn't metter what cell is here. We just count height for post content
    } else {
        return UITableViewAutomaticDimension;
//        VAComment *comment = [self.commentsArray objectAtIndex:indexPath.row - 1];
//        
//        commentCell.postAndCommentLabel.text = comment.text;
//        
//        CGFloat restCellHeight = 53.f;
//        
//        CGFloat cellWidth = CGRectGetWidth([[UIScreen mainScreen] bounds]);
//        CGFloat commentLabelWidth = cellWidth - 71 - 37;
//                        
//        CGFloat height = [commentCell.postAndCommentLabel heightForCommentLabelWithWidth:commentLabelWidth] + restCellHeight;
//        
//        return height;
    }
}

- (CGFloat)heightForCommentCellAtIndexPath:(NSIndexPath *)indexPath {
    static VACommentsTableViewCell *commentCell = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        commentCell = [self.tableView dequeueReusableCellWithIdentifier:@"CommentsCell"];
    });
    
    VAComment *comment = self.commentsArray[indexPath.row - 1];
    [commentCell configureCellWithComment:comment];
    
    return [commentCell getCellHeight];
}

- (void)configurePostCell:(VACommentTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.delegate = self;
    
    VAPost *post = self.post;
    VAUser *user = post.user;
    VALocation *location = post.checkIn;
    
    cell.user = user;
    
    cell.nameLabel.userInteractionEnabled = YES;
    
    if (location.address) {
        cell.nameLabel.attributedText = [self attributedTextForUserName:user.nickname andLocation:location.name];
    } else {
        cell.nameLabel.text = user.nickname;
    }
    
    cell.nameLabel.activeLinkAttributes = @{NSForegroundColorAttributeName: [[VADesignTool defaultDesign] vicinityDarkBlue]};
    cell.nameLabel.linkAttributes = @{NSForegroundColorAttributeName: [[VADesignTool defaultDesign] vicinityDarkBlue]};
    
    [cell.nameLabel addLinkToURL:[self authorUrl] withRange:[cell.nameLabel.text rangeOfString:user.nickname]];
    
    if (location.address) {
        [cell.nameLabel addLinkToURL:[self locationUrl] withRange:[cell.nameLabel.text rangeOfString:location.name]];
    }
    
    cell.timeLabel.text = [post.date messageTimeForDate];
    cell.locationLabel.text = location.cityName;
    
    if (location.distance) {
        cell.distanceLabel.text = [location.distance milesStringFromNSNumberMeters:NO];
    } else {
        cell.distanceLabel.text = @"< 0.5 mi";
    }
    
    cell.postAndCommentLabel.attributedText = [self attributedStringForPost:post.text];
    
    CGFloat nameLabelHeight = [cell.nameLabel heightForNameAndCheckInLabelWithWidth:CGRectGetWidth([[UIScreen mainScreen] bounds]) - 125.f andPost:post];   //if > 19 - label is multiline
    
    VAUser *loggedUser = (VAUser *)[VAUserDefaultsHelper getModelForKey:VASaveKeys_CurrentUser];
    
    if (!post.text && nameLabelHeight > 19) {
        cell.textContainerHeight.constant = 19.f;
    } else {
        cell.textContainerHeight.constant = 0.f;
    }
    
    if (post.text) {
        cell.textContainerHeight.priority = 250.f;
    } else {
        cell.textContainerHeight.priority = 900.f;
    }
    
    [cell.postAndCommentLabel addPostAtrributes];
    
    if (post.photoURL) {
        NSURL *imageURL = [NSURL URLWithString:post.photoURL];
        
        [cell.postImageView sd_setImageWithURL:imageURL
                              placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                         if (image) {
                                             cell.postImageView.image = image;
                                         }
                                         if (error) {
                                             [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
                                         }
                                     }];
        CGFloat k = [post.isLandscapePhoto boolValue] ? 9.0f/16.0f : 1.0f;
        cell.imageContainerHeight.constant = CGRectGetWidth([[UIScreen mainScreen] bounds])*k + 12.0f;
    } else {
        cell.postImageView.image = nil;
        cell.imageContainerHeight.constant = 0.0f;
    }
    
    if (user.avatarURL) {
        NSURL *imageURL = [NSURL URLWithString:user.avatarURL];
        
        [cell.avatarImageView sd_setImageWithURL:imageURL
                                placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                           if (image) {
                                               cell.avatarImageView.image = image;
                                           }
                                           if (error) {
                                               [VANotificationMessage showAvatarErrorMessageOnController:self withDuration:5.f];
                                           }
                                       }];
    } else {
        cell.avatarImageView.image = [UIImage imageNamed:@"placeholder.png"];
    }
    if (post.likesAmount.integerValue > 0) {
        cell.likesViewContainerHeight.priority = 250.f;
        [cell.likesContainer setHidden:NO];
    } else {
        cell.likesViewContainerHeight.priority = 900.f;
        [cell.likesContainer setHidden:YES];
    }
    
    [cell.editButton setHidden:YES];
    
    cell.likesCountLabel.text = [NSString stringWithFormat:@"%@", post.likesAmount];
    
    if ([self.commentsArray count] == 0) {
        [cell.separatorView setHidden:YES];
    } else {
        [cell.separatorView setHidden:NO];
    }
}

- (void)dismissKeyboard:(id)sender {
    [self.commentTextView resignFirstResponder];
}

- (void)addComment {
    [self.commentTextView resignFirstResponder];
    VAUser *currentUser = (VAUser *)[VAUserDefaultsHelper getModelForKey:VASaveKeys_CurrentUser];
    
    VAComment *comment = [VAComment new];
    comment.text = self.commentTextView.text;
    comment.date = [NSDate date];
    comment.user = currentUser;
    comment.status = @"Active";
    [[self mutableArrayValueForKey:@"commentsArray"] addObject:comment];
    // Add comment to table view
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:[self.commentsArray count] inSection:0]] withRowAnimation:UITableViewRowAnimationBottom];
    [self.tableView endUpdates];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.commentsArray count] inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    });
    
    [self postCommentToServer:comment];
    
    [self updatePostWithAddedComment:comment];
    
    self.commentTextView.text = @"";
    [self.placeholderLabel setHidden:NO];
    self.postButton.enabled = NO;
    self.postButton.backgroundColor = [UIColor lightGrayColor];

    VACommentTableViewCell *postCell = (VACommentTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [postCell.separatorView setHidden:NO];
    
    //code for google analytics
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Comments"
                                                          action:@"Add comment"
                                                           label:nil
                                                           value:nil] build]];
}

- (void)postCommentToServer:(VAComment *)comment {
    VAUserApiManager *manager = [VAUserApiManager new];
    
    VAUser *user = (VAUser *)[VAUserDefaultsHelper getModelForKey:VASaveKeys_CurrentUser];
    [manager postCommentToPostWithPostID:self.post.postID text:self.commentTextView.text andCurentUserID:user.userId withCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            [VANotificationMessage showAddCommentErrorMessageOnController:self withDuration:5.f];
            
            // Reload table view to clear optimistically added comment
            [[self mutableArrayValueForKey:@"commentsArray"] removeObject:comment];
            [self.tableView reloadData];
        } else if (model) {
            VANewCommentResponse *response = (VANewCommentResponse *)model;
            if ([response.status isEqualToString:kSuccessResponseStatus]) {
                VAComment *newComment = response.comment;
                NSInteger indexOfComment = [self.commentsArray indexOfObject:comment];
                if (indexOfComment != NSNotFound) {
                    [[self mutableArrayValueForKey:@"commentsArray"] replaceObjectAtIndex:[self.commentsArray indexOfObject:comment] withObject:newComment];
                    //[self.tableView reloadData];
                }
                // Subscribe post
                if (![user.userId isEqualToString:self.post.user.userId] && ![self.post.isSubscribed boolValue]) {
                    [manager subscribePostWithID:self.post.postID andCompletion:^(NSError *error, VAModel *model) {
                        self.post.isSubscribed = ((VAPost *)model).isSubscribed;
                        self.post.subscribers = ((VAPost *)model).subscribers;
                        [self.delegate userDidEditPost:self.post];
                        
//                        [self.tableView reloadData];
                    }];
                }
            } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                [VANotificationMessage showAddCommentErrorMessageOnController:self withDuration:5.f];
                
                // Reload table view to clear optimistically added comment
                [[self mutableArrayValueForKey:@"commentsArray"] removeObject:comment];
                [self.tableView reloadData];
            }
        }
    }];
}

- (void)loadCommentsFromServer {
    self.tableView.scrollEnabled = NO;
    VAUserApiManager *manager = [VAUserApiManager new];
    
    [manager getCommentsToPostWithPostID:self.post.postID withCompletion:^(NSError *error, VAModel *model) {
        self.tableView.scrollEnabled = YES;
        if (!self.isRefreshing) {
            [self.footerLoader stopAnimating];
        } else {
            [self.refresh stopRefreshing];
            self.isRefreshing = NO;
        }
        self.tableView.tableFooterView = nil;
        if (error) {
            [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
        } else if (model) {
            [[self mutableArrayValueForKey:@"commentsArray"] removeAllObjects];

            VACommentsResponse *response = (VACommentsResponse *)model;
            if ([response.status isEqualToString:kSuccessResponseStatus]) {
                [[self mutableArrayValueForKey:@"commentsArray"] addObjectsFromArray:response.comments];
                [self.tableView reloadData];
                NSIndexPath *lastIndex = [NSIndexPath indexPathForRow:[self.tableView numberOfRowsInSection:self.tableView.numberOfSections - 1] - 1 inSection:self.tableView.numberOfSections - 1];
                [self.tableView scrollToRowAtIndexPath:lastIndex atScrollPosition:UITableViewScrollPositionBottom animated:NO];
            } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
            }
        }
    }];
}

- (void)updatePostWithAddedComment:(VAComment *)comment {
    [[self.post mutableArrayValueForKey:@"commentsArray"] addObject:comment];
    if ([self.commentsArray count] > 2) {
        [[self.post mutableArrayValueForKey:@"commentsArray"] removeObjectAtIndex:0];
    }
    NSInteger commentCount = [self.post.commentsAmount integerValue];
    self.post.commentsAmount = [NSNumber numberWithInteger:++commentCount];
    [self.delegate userDidAddCommentToPost:self.post];
}

- (void)updatePostAfterRemovingComment {
    [self refillPostWithLastComments];

    NSInteger commentCount = [self.post.commentsAmount integerValue];
    self.post.commentsAmount = [NSNumber numberWithInteger:--commentCount];
    [self.delegate userDidDeleteCommentToPost:self.post];
}

- (void)updatePostAfterEditingComment {
    [self refillPostWithLastComments];
    
    [self.delegate userDidEditCommentToPost:self.post];
}

- (void)refillPostWithLastComments {
    [[self.post mutableArrayValueForKey:@"commentsArray"] removeAllObjects];
    
    if ([self.commentsArray count] >= 2) {           //after deleting comment
        for (NSInteger i = 0; i < 2; i++) {
            VAComment *comment = [self.commentsArray objectAtIndex:[self.commentsArray count] - 1 - i];          //it will be two last comments
            [[self.post mutableArrayValueForKey:@"commentsArray"] insertObject:comment atIndex:0];
        }
    } else if ([self.commentsArray count] == 1){
        [[self.post mutableArrayValueForKey:@"commentsArray"] addObject:[self.commentsArray objectAtIndex:0]];
    }
}

- (NSString*)isMentionEntred:(NSString*)comment {
    NSError *error;
    NSRegularExpression *regExp = [[NSRegularExpression alloc]initWithPattern:@"(?<=@)[a-zA-Z0-9_]+$" options:NSRegularExpressionCaseInsensitive error:&error];
    
    if(!error)
    {
        NSArray *matches=[regExp matchesInString:comment options:0 range:NSMakeRange(0, [comment length])];
        
        for(NSTextCheckingResult *match in matches)
        {
            return [comment substringWithRange:[match range]];
        }
    }
    return nil;
}

- (void)showSearchTable {
    self.isSearchTableShown = YES;
    UITableView *searchTable = [[UITableView alloc] initWithFrame:self.tableView.frame style:UITableViewStylePlain];
    searchTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    searchTable.delegate = self;
    searchTable.dataSource = self;
    [self.view addSubview:searchTable];
    NSLayoutConstraint *bottomConstraint =[NSLayoutConstraint
                                       constraintWithItem:self.tableView
                                       attribute:NSLayoutAttributeBottom
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:searchTable
                                       attribute:NSLayoutAttributeBottom
                                       multiplier:1.0
                                       constant:0];
    
    NSLayoutConstraint *topConstraint =[NSLayoutConstraint
                                           constraintWithItem:self.tableView
                                           attribute:NSLayoutAttributeTop
                                           relatedBy:NSLayoutRelationEqual
                                           toItem:searchTable
                                           attribute:NSLayoutAttributeTop
                                           multiplier:1.0
                                           constant:0];
    
    NSLayoutConstraint *leftConstraint =[NSLayoutConstraint
                                           constraintWithItem:self.tableView
                                           attribute:NSLayoutAttributeLeft
                                           relatedBy:NSLayoutRelationEqual
                                           toItem:searchTable
                                           attribute:NSLayoutAttributeLeft
                                           multiplier:1.0
                                           constant:0];
    
    NSLayoutConstraint *rightConstraint =[NSLayoutConstraint
                                           constraintWithItem:self.tableView
                                           attribute:NSLayoutAttributeRight
                                           relatedBy:NSLayoutRelationEqual
                                           toItem:searchTable
                                           attribute:NSLayoutAttributeRight
                                           multiplier:1.0
                                           constant:0];
    
    [searchTable setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [self.view addConstraint:topConstraint];
    [self.view addConstraint:bottomConstraint];
    [self.view addConstraint:leftConstraint];
    [self.view addConstraint:rightConstraint];

    self.searchTable = searchTable;
}

- (void)hideSearchTable {
    self.isSearchTableShown = NO;
    [self.searchTable removeFromSuperview];
}

- (void)makeUserSearchRequestWithAlias:(NSString *)alias {

    [self showActivityIndicator];
    [[VAUserApiManager alloc] getUserByAliasSearch:alias withCompletion:^(NSError *error, VAModel *model) {
        if (model) {
            VAUserSearchResponse *response = (VAUserSearchResponse *)model;
            if ([response.status isEqualToString:kSuccessResponseStatus]) {
                NSArray *usersArray = response.users;
                [[self mutableArrayValueForKey:@"usersArray"] addObjectsFromArray:usersArray];
                [self.searchTable reloadData];
            } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                [VANotificationMessage showLoadUserErrorMessageOnController:self withDuration:5.f];
            }
        } else if (error) {
            [VANotificationMessage showLoadUserErrorMessageOnController:self withDuration:5.f];
        }
        [self hideActivityIndicator];
    }];
}

- (void)makeSearchRequest {
    self.searchTimer = nil;
    [self makeUserSearchRequestWithAlias:self.searchString];
}

- (void)showProfileForUserWithAlias:(NSString *)alias {
    VAProfileViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"profileViewController"];
    vc.searchAlias = alias;
    vc.isUsersAccountMode = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showActivityIndicator {
    if (self.loader) {
        [self hideActivityIndicator];
    }
    VALoaderView *loader = [VALoaderView initLoader];
    loader.center = CGPointMake(CGRectGetWidth(self.tableView.bounds) / 2, CGRectGetHeight(loader.frame) /2);
    [self.searchTable addSubview:loader];
    [loader startAnimating];
    self.loader = loader;
}

- (void)hideActivityIndicator {
    [self.loader stopAnimating];
    for (id subview in [self.searchTable subviews]) {
        if ([subview isKindOfClass:[VALoaderView class]]) {
            [subview removeFromSuperview];
        }
    }
    self.loader = nil;
}

- (NSMutableAttributedString *)attributedTextForUserName:(NSString *)name andLocation:(NSString *)location {
    NSRange nameBoldedRange = NSMakeRange(0, name.length);
    NSMutableAttributedString *checkInAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ checked in at %@", name, location]
                                                                                          attributes:@{
                                                                                                       NSFontAttributeName : [[VADesignTool defaultDesign] vicinityLightFontOfSize:13.f],
                                                                                                       NSForegroundColorAttributeName: [UIColor blackColor]
                                                                                                       }];
    [checkInAttrString addAttributes:@{
                                       NSFontAttributeName: [[VADesignTool defaultDesign] vicinityBoldFontOfSize:13.f],
                                       NSForegroundColorAttributeName: [[VADesignTool defaultDesign] vicinityDarkBlue]
                                       }
                               range:nameBoldedRange];
    
    NSInteger preCheckInLength = [[NSString stringWithFormat:@"%@ checked in at ", name] length];
    NSRange checkInRange = NSMakeRange(preCheckInLength, location.length);
    
    [checkInAttrString addAttributes:@{
                                       NSFontAttributeName: [[VADesignTool defaultDesign] vicinityLightFontOfSize:13.f],
                                       NSForegroundColorAttributeName: [[VADesignTool defaultDesign] vicinityDarkBlue]
                                       }
                               range:checkInRange];
    return checkInAttrString;
}

- (NSMutableAttributedString *)attributedStringForPost:(NSString *)text {
    NSMutableAttributedString *postString = nil;
    
    if (text) {
        postString = [[NSMutableAttributedString alloc] initWithString:text
                                                            attributes:@{
                                                                         NSFontAttributeName : [[VADesignTool defaultDesign] vicinityLightFontOfSize:15.f],
                                                                         NSForegroundColorAttributeName: [UIColor blackColor]
                                                                         }];
    }
    
    return postString;
    
}

- (NSMutableAttributedString *)attributedStringForComment:(NSString *)text {
    NSMutableAttributedString *postString = nil;
    
    if (text) {
        postString = [[NSMutableAttributedString alloc] initWithString:text
                                                            attributes:@{
                                                                         NSFontAttributeName : [[VADesignTool defaultDesign] vicinityLightFontOfSize:12.f],
                                                                         NSForegroundColorAttributeName: [UIColor blackColor]
                                                                         }];
    }
    
    return postString;
    
}

- (void)loadPostWithPostID:(NSString *)postID {
    self.tableView.scrollEnabled = NO;
    VAUserApiManager *manager = [VAUserApiManager new];
    [manager getPostWithPostID:postID andCompletion:^(NSError *error, VAModel *model) {
        self.tableView.scrollEnabled = YES;
        
        [self.footerLoader stopAnimating];
        self.tableView.tableFooterView = nil;
        
        if (error) {
            [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
        } else if (model) {
            VAGetPostResponse *response = (VAGetPostResponse *)model;
            if ([response.status isEqualToString:kSuccessResponseStatus]) {
                self.post = response.post;
                [[self mutableArrayValueForKey:@"commentsArray"] addObjectsFromArray:self.post.commentsArray];
                [self.tableView reloadData];
                NSIndexPath *lastIndex = [NSIndexPath indexPathForRow:[self.tableView numberOfRowsInSection:self.tableView.numberOfSections - 1] - 1 inSection:self.tableView.numberOfSections - 1];
                [self.tableView scrollToRowAtIndexPath:lastIndex atScrollPosition:UITableViewScrollPositionBottom animated:YES];
            } else if ([response.status isEqualToString:kErrorResponseStatus]) {
                [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
            }
        }
    }];
}

- (void)setupRefreshControl {
    VALoaderView *refresh = [VALoaderView initLoader];
    self.refresh = refresh;
    [refresh setFrame:[self frameForCustomRefreshControl]];
    refresh.alpha = 0.f;
    
    [refresh addTarget:self
                action:@selector(refreshData)
      forControlEvents:UIControlEventValueChanged];
    
    [self.tableView addSubview:refresh];
}

- (CGRect)frameForCustomRefreshControl {
    CGFloat customRefreshControlHeight = kLoaderHeight;
    CGFloat customRefreshControlWidth = CGRectGetWidth([self.tableView frame]);
    CGRect customRefreshControlFrame = CGRectMake(0.0f,
                                                  -customRefreshControlHeight,
                                                  customRefreshControlWidth,
                                                  customRefreshControlHeight);
    return customRefreshControlFrame;
}

- (void)refreshData {
    self.isRefreshing = YES;
    [self.refresh startRefreshing];
    [self loadCommentsFromServer];
}

- (NSURL *)authorUrl {
    return [NSURL URLWithString:@"Author"];
}

- (NSURL *)locationUrl {
    return [NSURL URLWithString:@"Location"];
}

- (VAComment *)commentForCellWithPushedButton:(UIButton *)button {
    VACommentTableViewCell *cell = (VACommentTableViewCell *)[UITableViewCell findParentCellOfView:button];
    NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
    VAComment *comment = [self.commentsArray objectAtIndex:cellIndexPath.row - 1];
    return comment;
}

- (void)showEditActionSheetWithEditButtonTitle:(NSString *)editTitle andDeleteButtonTitle:(NSString *)deleteTitle {
    if(IOS8 || IOS9) {
        UIAlertController *ac = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];

        if ((self.post.text && ![self.post.text isEqualToString:@""]) || [editTitle rangeOfString:@"Comment"].location != NSNotFound) {     //if post with text or if comment
            UIAlertAction *edit = [UIAlertAction actionWithTitle:editTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self showEditViewWithTitle:editTitle];
            }];
            [ac addAction:edit];
        }
        UIAlertAction *delete = [UIAlertAction actionWithTitle:deleteTitle style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
            [self showDeleteConfirmationAlertViewWithTitle:deleteTitle];
        }];
        [ac addAction:delete];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        [ac addAction:cancel];
        [self presentViewController:ac animated:YES completion:nil];
    } else if (IOS7) {
        if (([self.post.text isEqualToString:@""] || !self.post.text) && [editTitle rangeOfString:@"Comment"].location == NSNotFound) {
            editTitle = nil;
        }
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:deleteTitle otherButtonTitles:editTitle, nil];
        [actionSheet showInView:self.view];
    }
}

- (void)showEditViewWithTitle:(NSString *)title {
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 276.f, 80.f)];
    textView.backgroundColor = [UIColor clearColor];
    
    [textView setFont:[[VADesignTool defaultDesign] vicinityRegularFontOfSize:14.f]];
    
    if ([self.elementToEdit isKindOfClass:[VAPost class]]) {
        textView.text = self.post.text;
    } else if ([self.elementToEdit isKindOfClass:[VAComment class]]) {
        textView.text = ((VAComment *)self.elementToEdit).text;
    }
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithViewStyleWithTitle:title
                                                                     message:nil
                                                                        view:textView
                                                                buttonTitles:@[@"Save"]
                                                           cancelButtonTitle:nil
                                                      destructiveButtonTitle:@"Cancel"
                                                               actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                   if ([title isEqualToString:@"Save"]) {
                                                                       [self editElement:self.elementToEdit withText:textView.text];
                                                                   }
                                                               }
                                                               cancelHandler:nil
                                                          destructiveHandler:nil];
    alertView.cancelOnTouch = NO;
    alertView.destructiveButtonTitleColor = [UIColor blackColor];
    alertView.destructiveButtonTitleColorHighlighted = [UIColor blackColor];
    alertView.destructiveButtonBackgroundColorHighlighted = [UIColor clearColor];
    alertView.buttonsFont = [UIFont boldSystemFontOfSize:18.f];
    [alertView showAnimated:YES completionHandler:nil];
    [textView becomeFirstResponder];
}

- (void)deleteElement:(id)element {
    if ([self.elementToEdit isKindOfClass:[VAPost class]]) {
        [self deletePost:(VAPost *)self.elementToEdit];
    } else if ([self.elementToEdit isKindOfClass:[VAComment class]]) {
        [self deleteComment:(VAComment *)self.elementToEdit];
    }
}

- (void)deletePost:(VAPost *)post {
    VAUserApiManager *manager = [VAUserApiManager new];
    [manager deletePostWithID:post.postID withCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            [VANotificationMessage showDeletePostErrorMessageOnController:self withDuration:5.f];
        } else if (model) {
            
        }
    }];
    
    [self.delegate userDidDeletePost:post];
    [self.navigationController popViewControllerAnimated:YES];
    
    //code for google analytics
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Posts"
                                                          action:@"Delete post"
                                                           label:@"From comments screen"
                                                           value:nil] build]];
}

- (void)deleteComment:(VAComment *)comment {
    NSInteger commentIndex = [self.commentsArray indexOfObject:comment];

    VAUserApiManager *manager = [VAUserApiManager new];
    [manager deleteCommentWithID:comment.commentID
                   forPostWithID:self.post.postID
                  withCompletion:^(NSError *error, VAModel *model) {
                      if (error) {
                          [VANotificationMessage showDeleteCommentErrorMessageOnController:self withDuration:5.f];
                      } else if (model) {
                          
                      }
                   }];
    
    [[self mutableArrayValueForKey:@"commentsArray"] removeObject:comment];

    [self updatePostAfterRemovingComment];
    
    if (!self.isSearchTableShown) {
        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:commentIndex + 1 inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
        [self.tableView endUpdates];

        NSIndexPath *lastIndex = [NSIndexPath indexPathForRow:[self.tableView numberOfRowsInSection:self.tableView.numberOfSections - 1] - 1 inSection:self.tableView.numberOfSections - 1];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.tableView scrollToRowAtIndexPath:lastIndex atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        });

    }
    
    //code for google analytics
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Comments"
                                                          action:@"Delete comment"
                                                           label:nil
                                                           value:nil] build]];
}

- (void)editElement:(id)element withText:(NSString *)text {
    if ([self.elementToEdit isKindOfClass:[VAPost class]]) {
        [self editPost:(VAPost *)self.elementToEdit withText:text];
    } else if ([self.elementToEdit isKindOfClass:[VAComment class]]) {
        [self editComment:(VAComment *)self.elementToEdit withText:text];
    }
}

- (void)editPost:(VAPost *)post withText:(NSString *)text {
    VAUserApiManager *manager = [VAUserApiManager new];
    [manager putEditPostWithID:post.postID
                andNewPostText:text
                           lat:post.location.latitude
                           lon:post.location.longitude
                       placeID:post.checkIn.placeID
                      postToFB:[post.isPostedOnFacebook boolValue]
                     imageData:nil
             shouldUpdatePhoto:NO
                withCompletion:^(NSError *error, VAModel *model) {
                    if (error) {
                        [VANotificationMessage showEditPostErrorMessageOnController:self withDuration:5.f];
                    } else if (model) {
                        
                    }
                }];

    post.text = text;
    
    [self.tableView reloadData];
    
    [self.delegate userDidEditPost:self.post];
    
    //code for google analytics
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Posts"
                                                          action:@"Edit post"
                                                           label:@"From comments screen"
                                                           value:nil] build]];
}

- (void)editComment:(VAComment *)comment withText:(NSString *)text {
    VAUserApiManager *manager = [VAUserApiManager new];
    [manager putEditCommentWithID:comment.commentID
                    forPostWithID:self.post.postID
                andNewCommentText:text
                   withCompletion:^(NSError *error, VAModel *model) {
                       if (model) {

                       } else if (error) {
                           [VANotificationMessage showEditCommentErrorMessageOnController:self withDuration:5.f];
                       }
                   }];

    comment.text = text;
    
    [self.tableView reloadData];
    
    [self updatePostAfterEditingComment];
    
    //code for google analytics
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Comments"
                                                          action:@"Edit comment"
                                                           label:nil
                                                           value:nil] build]];
}

- (void)showDeleteConfirmationAlertViewWithTitle:(NSString *)title {
    NSString *type = [[[title componentsSeparatedByString:@" "] lastObject] lowercaseString];      //it will be "post" or "comment"
    
    if (IOS8 || IOS9) {
        UIAlertController *ac = [UIAlertController alertControllerWithTitle:title message:[NSString stringWithFormat:NSLocalizedString(@"alert.delete.text", nil), type] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionDelete = [UIAlertAction actionWithTitle:NSLocalizedString(@"alert.delete.button", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self deleteElement:self.elementToEdit];
        }];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"alert.cancel.button", nil) style:UIAlertActionStyleCancel handler:nil];
        [ac addAction:actionDelete];
        [ac addAction:cancel];
        [self presentViewController:ac animated:YES completion:nil];
    } else if (IOS7) {
        [[[UIAlertView alloc] initWithTitle:title
                                    message:[NSString stringWithFormat:NSLocalizedString(@"alert.delete.text", nil), type]
                                   delegate:self
                          cancelButtonTitle:NSLocalizedString(@"alert.cancel.button", nil)
                          otherButtonTitles:NSLocalizedString(@"alert.delete.button", nil), nil] show];
    }
}

#pragma mark - Actions

- (IBAction)actionSendComment:(id)sender {
    [self hideSearchTable];
    [self addComment];
}

- (IBAction)editButtonPushed:(UIButton *)sender {
    [self dismissKeyboard:nil];
    
    VACommentTableViewCell *cell = (VACommentTableViewCell *)[UITableViewCell findParentCellOfView:sender];
    NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
    
    NSString *editButtonTitle = nil;
    self.editTitle = editButtonTitle;
    NSString *deleteButtonTitle = nil;
    self.deleteTitle = deleteButtonTitle;

    if (cellIndexPath.row == 0) {
        VAPost  *post = self.post;
        self.elementToEdit = post;
        
        editButtonTitle = @"Edit Post";
        deleteButtonTitle = @"Delete Post";
        
        VAUser *loggedUser = (VAUser *)[VAUserDefaultsHelper getModelForKey:VASaveKeys_CurrentUser];
        if (![self.post.user.userId isEqualToString:loggedUser.userId]) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                                     message:nil
                                                                              preferredStyle:UIAlertControllerStyleActionSheet];
            
            if ([self.post.isSubscribed boolValue]) {
                UIAlertAction *unsubscribe = [UIAlertAction actionWithTitle:@"Unsubscribe" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    VAUserApiManager *manager = [VAUserApiManager new];
                    [manager unsubscribePostWithID:self.post.postID andCompletion:^(NSError *error, VAModel *model) {
                        self.post.isSubscribed = ((VAPost *)model).isSubscribed;
                        self.post.subscribers = ((VAPost *)model).subscribers;
                        [self.delegate userDidEditPost:self.post];
                        
                        [self.tableView reloadData];
                    }];
                }];
                [alertController addAction:unsubscribe];
            } else {
                UIAlertAction *subscribe = [UIAlertAction actionWithTitle:@"Subscribe" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    VAUserApiManager *manager = [VAUserApiManager new];
                    [manager subscribePostWithID:self.post.postID andCompletion:^(NSError *error, VAModel *model) {
                        self.post.isSubscribed = ((VAPost *)model).isSubscribed;
                        self.post.subscribers = ((VAPost *)model).subscribers;
                        [self.delegate userDidEditPost:self.post];
                        
                        [self.tableView reloadData];
                    }];
                }];
                [alertController addAction:subscribe];
            }

            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
            [alertController addAction:cancel];
            [self presentViewController:alertController animated:YES completion:nil];
            
            return;
        }
    } else {
        VAComment *comment = [self commentForCellWithPushedButton:sender];
        self.elementToEdit = comment;

        editButtonTitle = @"Edit Comment";
        deleteButtonTitle = @"Delete Comment";
    }
    
    [self showEditActionSheetWithEditButtonTitle:editButtonTitle andDeleteButtonTitle:deleteButtonTitle];
}

#pragma mark - Post Actions

- (IBAction)showLikesToPostWithButton:(UIButton *)sender {
    VALikeListTableViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"VALikeListTableViewController"];
    vc.postID = self.post.postID;
    [self.navigationController pushViewController:vc animated:YES];
    
    //code for google analytics
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Likes"
                                                          action:@"Show likes for post"
                                                           label:@"From comments screen"
                                                           value:nil] build]];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    NSString *resultString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if ([resultString isEqualToString:@""]) {
        self.postButton.enabled = 0;
        self.postButton.backgroundColor = [UIColor lightGrayColor];
        [self.placeholderLabel setHidden:NO];
    } else {
        self.postButton.enabled = 1;
        self.postButton.backgroundColor = [[VADesignTool defaultDesign] vicinityBlue];
        [self.placeholderLabel setHidden:YES];
    }
    
    NSString *search = [self isMentionEntred:resultString];
    
    if (search) {
        if (!self.isSearchTableShown) {
            [self showSearchTable];
        }
        [[self mutableArrayValueForKey:@"usersArray"] removeAllObjects];
        self.searchTable.separatorStyle = UITableViewCellSeparatorStyleNone;
        if (!self.loader) {
            [self showActivityIndicator];
        }
        [self.searchTable reloadData];
        if (self.searchTimer) {
            [self.searchTimer invalidate];
        }
        
        NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:1.2f
                                                          target:self
                                                        selector:@selector(makeSearchRequest)
                                                        userInfo:nil
                                                         repeats:NO];
        self.searchTimer = timer;
        
    } else {
        if (self.isSearchTableShown) {
            [self hideSearchTable];
            [self.searchTimer invalidate];
            self.searchTimer = nil;
        }
    }
    
    self.searchString = search;
    
    if ([text isEqualToString:@"\n"]) {
        [self hideSearchTable];
    }
    
    return YES;

}

#pragma mark - UIScrollViewDelegate

/*- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self.refresh containingScrollViewDidEndDragging:scrollView];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.refresh containingScrollViewDidScroll:scrollView];
    self.bottomVisiblePath = [[self.tableView indexPathsForVisibleRows] lastObject];
}*/

#pragma mark - VACommentDelegate

- (void)shouldShowProfileForUser:(VAUser *)user {
    [self showProfileForUserWithAlias:user.nickname];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self showDeleteConfirmationAlertViewWithTitle:self.deleteTitle];
    } else if (buttonIndex == 1) {
        [self showEditViewWithTitle:self.editTitle];
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [self deleteElement:self.elementToEdit];
    }
}

#pragma mark - Comments Interaction delegate

- (void)didSelectAuthorForComment:(VAComment *)comment {
    [self showProfileForUserWithAlias:comment.user.nickname];
}

- (void)didSelectHashtag:(NSString *)hashtag {
    [self showTagsScreenWithHashtag:hashtag];
}

- (void)didSelectMention:(NSString *)mention {
    [self showProfileForUserWithAlias:mention];
}

- (void)didSelectReportComment:(VAComment *)comment {
    [self showReportScreenWithComment:comment];
}

- (void)didSelectCopyComment:(VAComment *)comment {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = comment.text ? comment.text : @"";
}

- (void)didSelectDeleteComment:(VAComment *)comment {
    // if comment created on server
    if (comment.commentID) {
        [self deleteComment:comment];
    }
}
- (void)didSelectEditComment:(VAComment *)comment{
    // if comment created on server
    if (comment.commentID) {
        self.elementToEdit = comment;
        [self showEditViewWithTitle:@"Edit Comment"];
    }
}

#pragma mark - Navigation

- (void)showTagsScreenWithHashtag:(NSString *)hashtag {
    VAEventsViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"VAEventsViewController"];
    vc.isHashTagMode = YES;
    vc.hashtagString = hashtag;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Pop Up

- (void)setupActionsPopUp {
    self.popUpView = [[UIView alloc] initWithFrame:self.navigationController.view.bounds];
    self.popUpView.backgroundColor = [UIColor clearColor];
    self.popUpView.hidden = YES;
    
    UIView *overlayView = [[UIView alloc] initWithFrame:self.popUpView.bounds];
    overlayView.backgroundColor = [UIColor blackColor];
    overlayView.alpha = 0.4f;
    UITapGestureRecognizer *overlayTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideActionsPopUp)];
    [overlayView addGestureRecognizer:overlayTap];
    [self.popUpView addSubview:overlayView];
    
    self.postActionsView = [[VAPostActionsView alloc] init];
    self.postActionsView.backgroundColor = [UIColor clearColor];
    self.postActionsView.delegate = self;
    [self.popUpView addSubview:self.postActionsView];
    
    [self.navigationController.view addSubview:self.popUpView];
}

- (void)showActionsPopUpForCell:(VACommentsTableViewCell *)cell {
    CGRect cellFrame = [cell convertRect:cell.contentView.frame toView:self.navigationController.view];
    CGRect targetFrame = CGRectMake(cellFrame.origin.x, cellFrame.origin.y + 27.0f, cellFrame.size.width, [VAPostActionsService postActionsForPost:self.post].count*37.0f + 2.0f);
    CGFloat spaceToBottom = CGRectGetMaxY(self.navigationController.view.frame) - CGRectGetMaxY(targetFrame);
    CGFloat tableOffset = 0.0f;
    if (spaceToBottom < 0) {
        tableOffset = -spaceToBottom;
        targetFrame = CGRectMake(targetFrame.origin.x, targetFrame.origin.y + spaceToBottom, targetFrame.size.width, targetFrame.size.height);
    }
    self.postActionsView.frame = targetFrame;
    self.postActionsView.postActions = [VAPostActionsService postActionsForPost:self.post];
    
    self.popUpView.alpha = 0.0f;
    self.popUpView.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        self.popUpView.alpha = 1.0f;
        self.tableView.contentOffset = CGPointMake(self.tableView.contentOffset.x, self.tableView.contentOffset.y + tableOffset);
    }];
}

- (void)hideActionsPopUp {
    [UIView animateWithDuration:0.3f animations:^{
        self.popUpView.alpha = 0.0f;
    } completion:^(BOOL finished) {
        self.popUpView.hidden = YES;
    }];
}

- (IBAction)showActionsButtonTouchUp:(id)sender {
    [self showActionsPopUpForCell:[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]]];
}

#pragma mark - Post Actions Delegate

- (void)editPostSelected {
    self.elementToEdit = self.post;
    [self showEditViewWithTitle:@"Edit Post"];
    [self hideActionsPopUp];
}

- (void)deletePostSelected {
    self.elementToEdit = self.post;
    [self showDeleteConfirmationAlertViewWithTitle:@"Delete Post"];
    [self hideActionsPopUp];
}

- (void)blockUserSelected {
    [self blockUserSelected:self.post.user];
    [self hideActionsPopUp];
}

- (void)reportUserSelected {
    [self reportSelected:self.post.user];
    [self hideActionsPopUp];
}

- (void)turnOffNotificationsSelected {
    [self turnOffNotificationsSelected:self.post];
    [self hideActionsPopUp];
}

- (void)copyPostSelected {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.post.text ? self.post.text : @"";
    [self hideActionsPopUp];
}

- (void)reportPostSelected {
    [self showReportScreenWithPost:self.post];
    [self hideActionsPopUp];
}

- (void)turnOnNotificationsSelected {
    [self subscibePost:self.post];
    [self hideActionsPopUp];
}

- (void)sharePostSelected{
    VACommentTableViewCell* cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [[VAControlTool defaultControl] showSharePostSheetWithPost:self.post image:cell.postImageView.image completion:^(NSString * _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError) {
        if (completed) {
            [VANotificationMessage showSharePostSuccessMessageOnController:self];
        }
    }];
    [self hideActionsPopUp];
}
#pragma mark - Report Sending delegate

- (void)didSendReportAboutUser:(VAUser *)user {
    [VANotificationMessage showReportSuccessMessageOnController:self withDuration:5.f];
}

#pragma mark - Helpers

- (void)blockUserSelected:(VAUser *)user {
    [self askConfirmationToBlockUser:user];
}

- (void)reportSelected:(VAUser *)user {
    [self showReportScreenWithUser:user];
}

- (void)turnOffNotificationsSelected:(VAPost *)post {
    [self unsubscibePost:post];
}

- (void)askConfirmationToBlockUser:(VAUser *)user {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"Block %@", user.fullname]
                                                                             message:[NSString stringWithFormat:@"Are you sure you want to block %@?", user.fullname]
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"No"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * _Nonnull action) { }];
    [alertController addAction:noAction];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"Yes"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * _Nonnull action) {
                                                          [self blockUser:user];
                                                      }];
    [alertController addAction:yesAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)blockUser:(VAUser *)user {
    user.isBlocked = @(YES);
    
    [self showActivityIndicator];
    
    [[VAUserApiManager new] blockUserWithID:user.userId andCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            user.isBlocked = @(NO);
            [VANotificationMessage showNetworkErrorMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
        } else {
            [VANotificationsService didBlockUser:user];
        }
    }];
}

- (void)showReportScreenWithUser:(VAUser *)user {
    VAReportViewController *vc = [[UIStoryboard storyboardWithName:@"Vicinity" bundle:nil] instantiateViewControllerWithIdentifier:@"reportViewController"];
    vc.reportedUser = user;
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showReportScreenWithPost:(VAPost *)post {
    VAReportViewController *vc = [[UIStoryboard storyboardWithName:@"Vicinity" bundle:nil] instantiateViewControllerWithIdentifier:@"reportViewController"];
    vc.reportedPost = post;
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showReportScreenWithComment:(VAComment *)comment {
    VAReportViewController *vc = [[UIStoryboard storyboardWithName:@"Vicinity" bundle:nil] instantiateViewControllerWithIdentifier:@"reportViewController"];
    vc.reportedPost = self.post;
    vc.reportedComment = comment;
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)unsubscibePost:(VAPost *)post {
    post.isSubscribed = [NSNumber numberWithBool:NO];
    
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    
    VAUserApiManager *manager = [VAUserApiManager new];
    [manager unsubscribePostWithID:post.postID andCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            post.isSubscribed = [NSNumber numberWithBool:YES];
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        }
    }];
}

- (void)subscibePost:(VAPost *)post {
    post.isSubscribed = [NSNumber numberWithBool:YES];
    
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    
    VAUserApiManager *manager = [VAUserApiManager new];
    [manager unsubscribePostWithID:post.postID andCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            post.isSubscribed = [NSNumber numberWithBool:NO];
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        }
    }];
}

@end
