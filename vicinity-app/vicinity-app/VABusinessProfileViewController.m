//
//  VABusinessProfileViewController.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 07.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VABusinessProfileViewController.h"
#import "VAPhotoPreviewCollectionViewCell.h"
#import "UILabel+VACustomFont.h"
#import "VAAllPhotosViewController.h"
#import "VABusinessPhotoViewController.h"
#import "VAScheduleTableViewCell.h"
#import <OpenInGoogleMapsController.h>
#import "DEMONavigationController.h"
#import "VABusiness.h"
#import "VAUserApiManager.h"
#import "VABusinessResponse.h"
#import "VASchedule.h" 
#import "VADay.h"
#import "VACoordinate.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "VANotificationMessage.h"
#import "VAControlTool.h"
#import <Google/Analytics.h>
#import "VAEventsViewController.h"

#import "VAWorkingHoursTableViewCell.h"

#import "VAGooglePlacesApiManager.h"
#import "VAPlaceDetails.h"
#import "VAPlacePhoto.h"
#import "VAGoogleLocation.h"

@interface VABusinessProfileViewController ()

@property (nonatomic, strong) VAPlaceDetails *placeDetails;

@property (strong, nonatomic) NSArray *photoURLsArray;
@property (assign, nonatomic) BOOL isInScheduleMode;
@property (strong, nonatomic) NSArray *indexPaths;
@property (strong, nonatomic) NSArray *daysInOrder;        //starting from today and going to i-1 day from today
@property (strong, nonatomic) NSArray *daysSchedule;
@property (strong, nonatomic) UIActivityIndicatorView *indicator;
@property (strong, nonatomic) UIBarButtonItem *indicatiorItem;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIView *infoBlock;
@property (weak, nonatomic) IBOutlet UIView *photosBlock;
@property (weak, nonatomic) IBOutlet UILabel *photosLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *webSiteLabeLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingVotesAmmountLabel;
@property (weak, nonatomic) IBOutlet UIButton *scheduleButton;
@property (weak, nonatomic) IBOutlet UIButton *viewAllPhotosButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ratingBlockHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addressBlockHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phoneBlockHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *priceBlockHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webSiteBlockHeightConstraint;
@property (strong, nonatomic) NSLayoutConstraint *photosBlockHeightConstraint;
@property (strong, nonatomic) NSLayoutConstraint *photosCollectionViewAspectRatioConstraint;

// Hours view
@property (nonatomic, strong) NSLayoutConstraint *hoursViewHeightConstraint;
@property (nonatomic, weak) IBOutlet UIView *hoursView;
@property (nonatomic, weak) IBOutlet UILabel *todayHoursLabel;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *todayHoursLabelHeightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *tableViewAlignBottomConstraint;

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *starsArray;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *priceArray;
@end

CGFloat smallScreenCellHeight = 30.f;
CGFloat bigScreenCellHeight = 20.f;

@implementation VABusinessProfileViewController

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        _isBusinessSearchModeMode = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupConstraints];
    
    self.photoURLsArray = [NSArray array];
    
    self.isInScheduleMode = NO;
    self.indexPaths = [NSArray array];
    self.daysInOrder = [NSArray array];
    
    [self.viewAllPhotosButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    
    self.indicatiorItem = [self indicatorBarButtonItem];
    
    [self clearAllFields];
    
    if (self.businessID && self.googlePlaceID) {                  //if user send both business ID and google place ID, search will use business ID
        self.googlePlaceID = nil;
    }
    
    if (self.isBusinessSearchModeMode) {
        [self showActivityIndicator];
        [self.scrollView setScrollEnabled:NO];
        [self.scheduleButton setEnabled:NO];
        
        if (self.businessID) {
            [self loadBusinessWithID:self.businessID];
        } else if (self.googlePlaceID) {
            [self loadBusinessWithGooglePlaceID:self.googlePlaceID];
        }
    } else {
        [self showProfileForBusiness:self.business];
    }
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackButton"] style:UIBarButtonItemStylePlain target:self.navigationController action:@selector(popViewControllerAnimated:)];
    self.navigationItem.leftBarButtonItem = leftItem;
        
    [self roundCornersForViews:@[self.infoBlock, self.photosBlock]];
    
    [self.photosLabel setLetterSpacingWithMultiplier:1.2f];
    
    UITapGestureRecognizer* tapAddress = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showMap)];
    [self.addressLabel addGestureRecognizer:tapAddress];
    self.addressLabel.userInteractionEnabled = YES;
    
    UITapGestureRecognizer* tapPhoneNumber = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dialPhoneNumber)];
    [self.phoneNumberLabel addGestureRecognizer:tapPhoneNumber];
    self.phoneNumberLabel.userInteractionEnabled = YES;
    
//    UITapGestureRecognizer* tapSchedule = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showSchedule:)];
//    [self.todayHoursLabel addGestureRecognizer:tapSchedule];
//    self.tableView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer* tapURL = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showSite)];
    [self.webSiteLabeLabel addGestureRecognizer:tapURL];
    self.webSiteLabeLabel.userInteractionEnabled = YES;
    
    UINib *workingHoursCellNib = [UINib nibWithNibName:@"VAWorkingHoursTableViewCell" bundle:[NSBundle mainBundle]];
    [self.tableView registerNib:workingHoursCellNib forCellReuseIdentifier:@"WorkingHoursCell"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.placeDetails) {
        self.navigationController.title = self.placeDetails.name;
    } else {
        self.navigationController.title = @"";
    }
    
    self.todayHoursLabelHeightConstraint.constant = [self isSmallScreen] ? smallScreenCellHeight : bigScreenCellHeight;
    
    [(DEMONavigationController *)self.navigationController setDefaultNavigationBar];
    
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Business' profile screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)setupConstraints {
    self.hoursViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.hoursView
                                                                  attribute:NSLayoutAttributeHeight
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:nil
                                                                  attribute:NSLayoutAttributeNotAnAttribute
                                                                 multiplier:1
                                                                   constant:0];
    self.photosBlockHeightConstraint = [NSLayoutConstraint constraintWithItem:self.photosBlock
                                                                    attribute:NSLayoutAttributeHeight
                                                                    relatedBy:NSLayoutRelationEqual
                                                                       toItem:nil
                                                                    attribute:NSLayoutAttributeNotAnAttribute
                                                                   multiplier:1
                                                                     constant:0];
    self.photosCollectionViewAspectRatioConstraint = [NSLayoutConstraint constraintWithItem:self.collectionView
                                                                                  attribute:NSLayoutAttributeWidth
                                                                                  relatedBy:NSLayoutRelationEqual
                                                                                     toItem:self.collectionView
                                                                                  attribute:NSLayoutAttributeHeight
                                                                                 multiplier:2.5f
                                                                                   constant:0];
    [self.collectionView addConstraint:self.photosCollectionViewAspectRatioConstraint];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    BOOL isSmallScreen = CGRectGetWidth([UIScreen mainScreen].bounds) < 350.0f;
    return isSmallScreen ? smallScreenCellHeight : bigScreenCellHeight;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.daysSchedule count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VAWorkingHoursTableViewCell *workingHoursCell = [tableView dequeueReusableCellWithIdentifier:@"WorkingHoursCell"
                                                                                    forIndexPath:indexPath];
    [workingHoursCell configureCellWithDay:[self.daysSchedule objectAtIndex:indexPath.row]];
    return workingHoursCell;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.photoURLsArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Photo";
    VAPhotoPreviewCollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    [cell.indicator startAnimating];
    
    cell.imageView.alpha = 0.f;
    __weak VAPhotoPreviewCollectionViewCell *weakCell = cell;
    NSURL *imageURL = [[VAGooglePlacesApiManager sharedManager] downloadUrlForPhoto:self.photoURLsArray[indexPath.row]];// [NSURL URLWithString:[self.photoURLsArray objectAtIndex:indexPath.row]];
    [weakCell.imageView sd_setImageWithURL:imageURL
                          placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                   options:SDWebImageRetryFailed | SDWebImageContinueInBackground | SDWebImageHandleCookies | SDWebImageHighPriority | SDWebImageTransformAnimatedImage | SDWebImageAvoidAutoSetImage
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                     if (image) {
                                         weakCell.imageView.image = image;
                                         [UIView animateWithDuration:0.3f animations:^{
                                             weakCell.imageView.alpha = 1.0;
                                         }];
                                         
                                         [cell.indicator stopAnimating];
                                     } else if (error) {
                                         [VANotificationMessage showLoadErrorMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
                                     }
                                 }];
    
    cell.layer.cornerRadius = 5.f;
    cell.clipsToBounds = YES;
    
    [cell layoutIfNeeded];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    VABusinessPhotoViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"VABusinessPhotoViewController"];
    vc.selectedIndexPath = indexPath;
    vc.photoURLsArray = self.photoURLsArray;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat width = ([UIScreen mainScreen].bounds.size.width - 16.f*5)/2;
    CGSize size = CGSizeMake(width, width);
    return size;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (self.photoURLsArray.count == 1) {
        CGFloat width = ([UIScreen mainScreen].bounds.size.width - 16.f*5)/2;
        CGFloat edge = [UIScreen mainScreen].bounds.size.width/2 - width/2;
        return UIEdgeInsetsMake(0, edge, 0, edge);
    }
    return UIEdgeInsetsMake(0, 32, 0, 32);
}

#pragma mark - Methods

- (void)roundCornersForViews:(NSArray *)views {
    for (id view in views) {
        if ([view isKindOfClass:[UIView class]]) {
            ((UIView *)view).layer.cornerRadius = 5.f;
            ((UIView *)view).clipsToBounds = YES;
        }
    }
}

- (void)placeDaysInOrder {
    NSMutableArray *temp = [NSMutableArray array];
    
    NSArray *days = [self valueForKeyPath:@"daysSchedule.@unionOfObjects.name"];
    
    NSArray *orderedDays = @[@"Monday", @"Tuesday", @"Wednesday", @"Thursday", @"Friday", @"Saturday", @"Sunday"];
    // HACK: change order to show Moday first
    NSInteger dayOrder = 0;
    
    for (NSInteger i = 0; i < [days count]; i++) {
        NSInteger indexOfDay = [days indexOfObject:orderedDays[dayOrder]];
        if (indexOfDay == NSNotFound) {                //schedule does not contains today
            dayOrder++;
            if (dayOrder >= orderedDays.count) {
                break;
            }
            i--;
            continue;
        }
        NSInteger resultIndex = indexOfDay + i < [self.daysSchedule count] ? indexOfDay + i : indexOfDay + i - [self.daysSchedule count];
        [temp addObject:[self.daysSchedule objectAtIndex:resultIndex]];
    }
    
    self.daysSchedule = temp;
}

- (void)addIndexPaths {
    for (NSInteger i = 0; i < [self.daysSchedule count]; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        [[self mutableArrayValueForKey:@"indexPaths"] addObject:indexPath];
    }
}

- (void)showMap {
    GoogleMapDefinition *mapDefinition = [[GoogleMapDefinition alloc] init];
    
    double lat = [self.placeDetails.location.latitude doubleValue];
    double lon = [self.placeDetails.location.longitude doubleValue];
    if ([OpenInGoogleMapsController sharedInstance].isGoogleMapsInstalled) {
        mapDefinition.queryString = [NSString stringWithFormat:@"%@ %@", self.placeDetails.name, self.placeDetails.shortAddressStreet];
    } else {
        mapDefinition.queryString = [NSString stringWithFormat:@"%f %f", lat, lon];
    }

    [[OpenInGoogleMapsController sharedInstance] openMap:mapDefinition];
}

- (void)dialPhoneNumber {
    NSString *phoneNumber = [self.phoneNumberLabel.text stringByReplacingOccurrencesOfString:@" " withString:@"-"];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", phoneNumber]];
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
}

- (void)showSite {
    NSString *webURLString = self.webSiteLabeLabel.text;
    if ([webURLString rangeOfString:@"http://"].location == NSNotFound) {
        webURLString = [@"http://" stringByAppendingString:webURLString];
    }
    NSURL *webURL = [NSURL URLWithString:webURLString];
    [[UIApplication sharedApplication] openURL:webURL];
}

- (void)showStarsForRating:(CGFloat)rating andVotesAmmount:(NSInteger)votesAmount {
    if (votesAmount == 0 && self.business) {      //it means that we have loaded data from server
        self.ratingBlockHeightConstraint.constant = 0.f;
        return;
    }
    self.ratingVotesAmmountLabel.text = [NSString stringWithFormat:@"%ld %@", votesAmount, votesAmount == 1 ? @"Review" : @"Reviews"];
    NSInteger integralPart = (NSInteger)rating;
    double fractionalPart = rating - integralPart;
    if (fractionalPart > 0.5) {
        integralPart +=1;
    }
    
    for (NSInteger j = 0; j < self.starsArray.count; j++) {
        UIImageView *star = [self.starsArray objectAtIndex:j];
        star.image = j < integralPart ? [UIImage imageNamed:@"big-selected-star"] : [UIImage imageNamed:@"big-unselected-star"];
    }
}

- (void)showBagsForPriceLevel:(CGFloat)priceLevel {
    if (self.googlePlaceID && self.business) {
        priceLevel += 1;             //google returns price level from 0 to 4
    }
    NSInteger integralPart = (NSInteger)priceLevel;
    double fractionalPart = priceLevel - integralPart;
    if (fractionalPart > 0.5) {
        integralPart +=1;
    }
    for (NSInteger j = 0; j < integralPart; j++) {
        UIImageView *bag = [self.priceArray objectAtIndex:j];
        bag.image = [UIImage imageNamed:@"price_icon_enabled.png"];
    }
}

- (void)showScheduleForBusiness:(VABusiness *)business {
    [self.tableView setHidden:NO];
    [self.scheduleButton setEnabled:NO];
    self.daysSchedule = business.schedule.days;
    [self addIndexPaths];
    [self setNamesForDaysInArray:self.daysSchedule];
    [self placeDaysInOrder];
    [self.tableView reloadData];
    
    VADay *day = nil;
    
    if ([self.daysSchedule count] > 0) {
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = [gregorian components:NSWeekdayCalendarUnit fromDate:[NSDate date]];
        NSInteger weekday = [components weekday] - 2;
        if (weekday == -1) {
            weekday = 6;
        }
        if (weekday < self.daysSchedule.count) {
            day = [self.daysSchedule objectAtIndex:weekday];
        } else {
            VADay *today = [VADay new];
            today.name = @"Today";
            today.openHours = @"0000";
            today.closeHours = @"0000";
            day = today;
        }
    }
    
    NSString *status;
    
    BOOL isSmallScreen = CGRectGetWidth([UIScreen mainScreen].bounds) < 350.0f;
    if ([day.openHours isEqualToString:@"0000"] && [day.closeHours isEqualToString:@"0000"]) {
        status = @"Closed";
    } else {
        status = @"Open";
    }
    
    if ([status isEqualToString:@"Open"]) {
        if ([day.openHours isEqualToString:@"0000"] && [day.closeHours isEqualToString:@"2400"]) {
            self.todayHoursLabel.text = @"Open 24 hours";
        } else {
            NSString *hoursText = [NSString stringWithFormat:@"%@ - %@", [self timeStringFromJSONString:day.openHours], [self timeStringFromJSONString:day.closeHours]];
            self.todayHoursLabel.text = [NSString stringWithFormat:@"%@ today: %@%@", status, isSmallScreen ? @"\n" : @"", hoursText];
        }
    } else {
        self.todayHoursLabel.text = [NSString stringWithFormat:@"%@ today", status];
    }
    
    BOOL isNotLargeScreen = CGRectGetWidth([UIScreen mainScreen].bounds) < 400.0f;
    self.todayHoursLabel.font = [UIFont fontWithName:@"Montserrat-Regular" size:isNotLargeScreen ? 10.0f : 13.0f];
}

- (void)loadGaleryForBusiness:(VABusiness *)business {
    self.photoURLsArray = business.photoURLsArray;
    if ([self.photoURLsArray count] > 0) {
        [self.viewAllPhotosButton setEnabled:YES];
        self.viewAllPhotosButton.hidden = self.photoURLsArray.count < 3;
    } else {
        [self.photosBlock removeConstraint:self.photosCollectionViewAspectRatioConstraint];
        [self.photosBlock addConstraint:self.photosBlockHeightConstraint];
        self.photosBlock.hidden = YES;
        self.collectionView.hidden = YES;
    }
    [self.collectionView reloadData];
}

- (void)showWebsiteForBusiness:(VABusiness *)business {
    if ([business.webSiteURL rangeOfString:@"http://"].location != NSNotFound) {
        business.webSiteURL = [business.webSiteURL substringFromIndex:7];          //the length of http:// is 7 chars
    }
    if ([business.webSiteURL rangeOfString:@"www."].location != NSNotFound) {
        business.webSiteURL = [business.webSiteURL substringFromIndex:4];          //the length of www. is 4 chars
    }
    if ([business.webSiteURL rangeOfString:@"?"].location != NSNotFound) {
        business.webSiteURL = [business.webSiteURL substringToIndex:[business.webSiteURL rangeOfString:@"?"].location];
    }
    if ([business.webSiteURL rangeOfString:@"/" options:NSBackwardsSearch].location != NSNotFound) {
        if ([business.webSiteURL rangeOfString:@"/" options:NSBackwardsSearch].location == [business.webSiteURL length] - 1) {
            business.webSiteURL = [business.webSiteURL substringToIndex:business.webSiteURL.length - 1];
        }
    }
    
    self.webSiteLabeLabel.text = business.webSiteURL;

    if (!business.webSiteURL || [business.webSiteURL isEqualToString:@""]) {
        self.webSiteBlockHeightConstraint.constant = 0;
    }
}

- (void)showProfileForBusiness:(VABusiness *)business {
    [self showStarsForRating:[business.raiting floatValue] andVotesAmmount:[business.votesCount integerValue]];
    [self showAddressForBusiness:business];
    [self showPhoneNumberForBusiness:business];
    [self showBagsForPriceLevel:[business.priceLevel floatValue]];
    if ([business.schedule.days count] > 0) {
        
        if ([business.schedule.days count] == 1) {
            [self.scheduleButton setHidden:YES];
            VADay *day = business.schedule.days[0];
            if (!day.closeHours && [day.openHours isEqualToString:@"0000"] && [day.dayIndex integerValue] == 0) {
                NSLog(@"Open always");
                for (int i = 0; i < 7; i++) {
                    VADay *d = [[VADay alloc] init];
                    d.openHours = @"0000";
                    d.closeHours = @"2400";
                    d.dayIndex = @(i);
                }
            }
        }
        [self showScheduleForBusiness:business];
    } else {
        [self.hoursView addConstraint:self.hoursViewHeightConstraint];
        self.hoursView.hidden = YES;
    }
    [self showWebsiteForBusiness:business];
    [self loadGaleryForBusiness:business];
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)clearAllFields {
    [self showStarsForRating:0 andVotesAmmount:0];
    self.addressLabel.text = @"";
    self.phoneNumberLabel.text = @"";
    [self showBagsForPriceLevel:0.f];
    [self.tableView setHidden:YES];
    self.webSiteLabeLabel.text = @"";
    [self.viewAllPhotosButton setEnabled:NO];
}

- (void)showActivityIndicator {
    self.navigationItem.rightBarButtonItem = self.indicatiorItem;;
    [self.indicator startAnimating];
}

- (void)hideActivityIndicator {
    [self.indicator stopAnimating];
    
    self.navigationItem.rightBarButtonItem = nil;
}

- (UIBarButtonItem *)indicatorBarButtonItem {
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    indicator.frame = CGRectMake(0.0, 0.0, 20.0, 20.0);
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:indicator];
    self.indicator = indicator;
    return rightItem;
}

- (void)setNamesForDaysInArray:(NSArray *)daysArray {
    NSArray *days = @[@"Sunday", @"Monday", @"Tuesday", @"Wednesday", @"Thursday", @"Friday", @"Saturday"];
    for (VADay *day in daysArray) {
        if (![day isKindOfClass:[VADay class]]) {
            break;
        }
        
        day.name = days[[day.dayIndex integerValue]];
    }
}

- (void)loadBusinessWithID:(NSString *)businessID {
    VAUserApiManager *manager = [VAUserApiManager new];
    [manager getCompanyWithID:businessID andCompletion:^(NSError *error, VAModel *model) {
        [self performComplitionForLoadedModel:model orError:error];
    }];
}

- (void)loadBusinessWithGooglePlaceID:(NSString *)googlePlaceID {
    [[VAGooglePlacesApiManager sharedManager] getDetailsForPlaceWithId:googlePlaceID
                                                            completion:^(NSError *error, id responseObject) {
                                                                [self performCompletionForModel:responseObject
                                                                                        orError:error];
    }];
    
//    VAUserApiManager *manager = [VAUserApiManager new];
//    [manager getCompanyWithGooglePlaceID:googlePlaceID andCompletion:^(NSError *error, VAModel *model) {
//        [self performComplitionForLoadedModel:model orError:error];
//    }];
}

- (void)performCompletionForModel:(VAPlaceDetails *)model orError:(NSError *)error {
    [self hideActivityIndicator];
    
    if (error) {
        [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
    } else if (model) {
        [self.scrollView setScrollEnabled:YES];
        self.placeDetails = model;
        [self updateUI];
        
        self.navigationController.title = self.placeDetails.name;
    }
}

- (void)performComplitionForLoadedModel:(VAModel *)model orError:(NSError *)error {
    [self hideActivityIndicator];
    
    if (error) {
        [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
    } else if (model) {
        [self.scrollView setScrollEnabled:YES];
        VABusinessResponse *response = (VABusinessResponse *)model;
        VABusiness *business = [response.businesses firstObject];
        self.business = business;
        [self showProfileForBusiness:business];
        
        self.navigationController.title = business.name;
    }
}

- (NSString *)timeStringFromJSONString:(NSString *)string {
    //string will be in format of @"0900", for example
    
    NSString *hoursString = [string substringToIndex:2];
    NSString *minutesString = [string substringFromIndex:2];
    NSString *endString = @"AM";
    
    NSInteger hours = [hoursString integerValue];
    if (hours >= 12) {
        if (hours == 12) {
            hoursString = [NSString stringWithFormat:@"%ld", (long)hours];
            endString = @"PM";
        } else {
            NSInteger newHours = hours - 12;
            hoursString = [NSString stringWithFormat:@"%ld", (long)newHours];
            endString = @"PM";
        }
    } else {
        if (hours < 1) {
            NSInteger newHours = hours + 12;
            hoursString = [NSString stringWithFormat:@"%ld", (long)newHours];
            endString = @"AM";
        } else {
            hoursString = [NSString stringWithFormat:@"%ld", (long)hours];
            endString = @"AM";
        }
    }
    NSString *resultString = [NSString stringWithFormat:@"%@:%@ %@", hoursString, minutesString, endString];
    
    return resultString;
}

- (void)showAddressForBusiness:(VABusiness *)business {
    self.addressLabel.text = business.address;
    if (!business.address || [business.address isEqualToString:@""]) {
        self.addressBlockHeightConstraint.constant = 0;
    }
}

- (void)showPhoneNumberForBusiness:(VABusiness *)business {
    if ([business.phoneNumber rangeOfString:@"+1"].location != NSNotFound && [business.phoneNumber rangeOfString:@"+1"].location == 0) {
        business.phoneNumber = [business.phoneNumber substringFromIndex:2];          //the length of http:// is 7 chars
    }
    self.phoneNumberLabel.text = business.phoneNumber;
    if (!business.phoneNumber || [business.phoneNumber isEqualToString:@""] || business.phoneNumber.length < 4) {
        self.phoneBlockHeightConstraint.constant = 0;
    }
}

#pragma mark - Actions

- (IBAction)viewAllPhotos:(UIButton *)sender {
    VAAllPhotosViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"VAAllPhotosViewController"];
    vc.photoURLsArray = self.photoURLsArray;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)showSchedule:(UIButton *)sender {
    BOOL isSmallScreen = CGRectGetWidth([UIScreen mainScreen].bounds) < 350.0f;
    
    self.tableViewHeightConstraint.constant = (isSmallScreen ? smallScreenCellHeight : bigScreenCellHeight) * [self.daysSchedule count];
    
    VADay *day = nil;
    
    if ([self.daysSchedule count] > 0) {
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = [gregorian components:NSWeekdayCalendarUnit fromDate:[NSDate date]];
        NSInteger weekday = [components weekday] - 2;
        if (weekday == -1) {
            weekday = 6;
        }
        if (weekday < self.daysSchedule.count) {
            day = [self.daysSchedule objectAtIndex:weekday];
        } else {
            VADay *today = [VADay new];
            today.name = @"Today";
            today.openHours = @"0000";
            today.closeHours = @"0000";
            day = today;
        }
    }
    
    NSString *status;
    
    if ([day.openHours isEqualToString:@"0000"] && [day.closeHours isEqualToString:@"0000"]) {
        status = @"Closed";
    } else {
        status = @"Open";
    }
    
    if (!self.isInScheduleMode) {
        self.isInScheduleMode = YES;
        if ([day.openHours isEqualToString:@"0000"] && [day.closeHours isEqualToString:@"2400"]) {
            self.todayHoursLabel.text = @"Open 24 hours";
        } else {
            self.todayHoursLabel.text = [NSString stringWithFormat:@"%@ today", status];
        }
        
        self.tableViewAlignBottomConstraint.constant = self.tableViewHeightConstraint.constant;
        
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
            self.scheduleButton.transform = CGAffineTransformRotate(self.scheduleButton.transform, M_PI);
        }];
    } else {
        self.isInScheduleMode = NO;
        
        if ([status isEqualToString:@"Open"]) {
            if ([day.openHours isEqualToString:@"0000"] && [day.closeHours isEqualToString:@"2400"]) {
                self.todayHoursLabel.text = @"Open 24 hours";
            } else {
                NSString *hoursText = [NSString stringWithFormat:@"%@ - %@", [self timeStringFromJSONString:day.openHours], [self timeStringFromJSONString:day.closeHours]];
                self.todayHoursLabel.text = [NSString stringWithFormat:@"%@ today: %@%@", status, isSmallScreen ? @"\n" : @"", hoursText];
            }
        } else {
            self.todayHoursLabel.text = [NSString stringWithFormat:@"%@ today", status];
        }
        
        self.tableViewAlignBottomConstraint.constant = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
            self.scheduleButton.transform = CGAffineTransformRotate(self.scheduleButton.transform, -M_PI + 0.00001);
        }];
    }
}

- (IBAction)viewCheckIns:(UIButton *)sender {
    VAEventsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"VAEventsViewController"];
    vc.isCheckInMode = YES;
    vc.checkInName = self.placeDetails.name;
    vc.googlePlaceID = self.placeDetails.placeId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (BOOL)isSmallScreen {
    return CGRectGetWidth([UIScreen mainScreen].bounds) < 350.0f;
}

#pragma mark - Update UI

- (void)updateUI {
    [self showStarsForRating:self.placeDetails.rating ? [self.placeDetails.rating floatValue] : 0.f
             andVotesAmmount:[self.placeDetails.userRatingsTotal integerValue]];
    [self updateAddressLabel];
    [self updatePhoneNumberLabel];
    if (self.placeDetails.priceLevel) {
        if ([self.placeDetails.priceLevel intValue] == 0) {
            self.priceBlockHeightConstraint.constant = 0;
        } else {
            [self showBagsForPriceLevel:[self.placeDetails.priceLevel floatValue]];
        }        
    } else {
        self.priceBlockHeightConstraint.constant = 0;
    }
    if ([self.placeDetails.openingHours count] > 0) {
        if ([self.placeDetails.openingHours count] == 1) {
            [self.scheduleButton setHidden:YES];
            VADay *day = self.placeDetails.openingHours[0];
            if (!day.closeHours && [day.openHours isEqualToString:@"0000"] && [day.dayIndex integerValue] == 0) {
                NSLog(@"Open always");
                NSMutableArray *hours = [NSMutableArray array];
                for (int i = 0; i < 7; i++) {
                    VADay *d = [[VADay alloc] init];
                    d.openHours = @"0000";
                    d.closeHours = @"2400";
                    d.dayIndex = @(i);
                    [hours addObject:d];
                }
                self.placeDetails.openingHours = hours;
                [self.scheduleButton setHidden:NO];
            }
        } else {
            for (VADay *day in self.placeDetails.openingHours) {
                if ([day.openHours isEqualToString:@"0000"] && [day.closeHours isEqualToString:@"2359"]) {
                    day.closeHours = @"2400";
                }
            }
        }
        [self updateWorkingSchedule];
    } else {
        [self.hoursView addConstraint:self.hoursViewHeightConstraint];
        self.hoursView.hidden = YES;
    }
    [self updateWebsiteLabel];
    [self updatePhotos];
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)updateAddressLabel {
    if (!self.placeDetails.shortAddressStreet || [self.placeDetails.shortAddressStreet isEqualToString:@""]) {
        self.addressLabel.text = @"";
        self.addressBlockHeightConstraint.constant = 0;
    } else {
        self.addressLabel.text = self.placeDetails.shortAddressStreet;
    }
}

- (void)updatePhoneNumberLabel {
    if (!self.placeDetails.formattedPhoneNumber || [self.placeDetails.formattedPhoneNumber isEqualToString:@""] || self.placeDetails.formattedPhoneNumber.length < 4) {
        self.phoneNumberLabel.text = @"";
        self.phoneBlockHeightConstraint.constant = 0;
    } else {
        self.phoneNumberLabel.text = self.placeDetails.formattedPhoneNumber;
    }
}

- (void)updateWebsiteLabel {
    NSString *websiteString = self.placeDetails.website;
    if ([websiteString rangeOfString:@"http://"].location != NSNotFound) {
        websiteString = [websiteString substringFromIndex:7];          //the length of http:// is 7 chars
    }
    if ([websiteString rangeOfString:@"www."].location != NSNotFound) {
        websiteString = [websiteString substringFromIndex:4];          //the length of www. is 4 chars
    }
    if ([websiteString rangeOfString:@"?"].location != NSNotFound) {
        websiteString = [websiteString substringToIndex:[websiteString rangeOfString:@"?"].location];
    }
    if ([websiteString rangeOfString:@"/" options:NSBackwardsSearch].location != NSNotFound) {
        if ([websiteString rangeOfString:@"/" options:NSBackwardsSearch].location == [websiteString length] - 1) {
            websiteString = [websiteString substringToIndex:websiteString.length - 1];
        }
    }
    
    if (!websiteString || [websiteString isEqualToString:@""]) {
        self.webSiteLabeLabel.text = @"";
        self.webSiteBlockHeightConstraint.constant = 0;
    } else {
        self.webSiteLabeLabel.text = websiteString;
    }
}

- (void)updatePhotos {
    self.photoURLsArray = self.placeDetails.photos;
    if ([self.photoURLsArray count] > 0) {
        [self.viewAllPhotosButton setEnabled:YES];
        self.viewAllPhotosButton.hidden = self.photoURLsArray.count < 3;
    } else {
        [self.photosBlock removeConstraint:self.photosCollectionViewAspectRatioConstraint];
        [self.photosBlock addConstraint:self.photosBlockHeightConstraint];
        self.photosBlock.hidden = YES;
        self.collectionView.hidden = YES;
    }
    [self.collectionView reloadData];
}

- (void)updateWorkingSchedule {
    [self.tableView setHidden:NO];
    [self.scheduleButton setEnabled:NO];
    self.daysSchedule = self.placeDetails.openingHours;
    [self addIndexPaths];
    [self setNamesForDaysInArray:self.daysSchedule];
    [self placeDaysInOrder];
    [self.tableView reloadData];
    
    VADay *day = nil;
    
    if ([self.daysSchedule count] > 0) {
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = [gregorian components:NSWeekdayCalendarUnit fromDate:[NSDate date]];
        NSInteger weekday = [components weekday] - 2;
        if (weekday == -1) {
            weekday = 6;
        }
        if (weekday < self.daysSchedule.count) {
            day = [self.daysSchedule objectAtIndex:weekday];
        } else {
            VADay *today = [VADay new];
            today.name = @"Today";
            today.openHours = @"0000";
            today.closeHours = @"0000";
            day = today;
        }
    }
    
    NSString *status;
    
    BOOL isSmallScreen = CGRectGetWidth([UIScreen mainScreen].bounds) < 350.0f;
    if ([day.openHours isEqualToString:@"0000"] && [day.closeHours isEqualToString:@"0000"]) {
        status = @"Closed";
    } else {
        status = @"Open";
    }
    
    if ([status isEqualToString:@"Open"]) {
        if ([day.openHours isEqualToString:@"0000"] && [day.closeHours isEqualToString:@"2400"]) {
            self.todayHoursLabel.text = @"Open 24 hours";
        } else {
            NSString *hoursText = [NSString stringWithFormat:@"%@ - %@", [self timeStringFromJSONString:day.openHours], [self timeStringFromJSONString:day.closeHours]];
            self.todayHoursLabel.text = [NSString stringWithFormat:@"%@ today: %@%@", status, isSmallScreen ? @"\n" : @"", hoursText];
        }
    } else {
        self.todayHoursLabel.text = [NSString stringWithFormat:@"%@ today", status];
    }
    
    BOOL isNotLargeScreen = CGRectGetWidth([UIScreen mainScreen].bounds) < 400.0f;
    self.todayHoursLabel.font = [UIFont fontWithName:@"Montserrat-Regular" size:isNotLargeScreen ? 10.0f : 13.0f];
}

@end
