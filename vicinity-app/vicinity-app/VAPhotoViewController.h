//
//  VAPhotoViewController.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 26.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VAPhotoViewProtocol;

@interface VAPhotoViewController : UIViewController
@property (weak, nonatomic) id <VAPhotoViewProtocol> delegate;
@end

@protocol VAPhotoViewProtocol <NSObject>
- (void)dismissPhotoView;
- (void)openCamera;
- (void)openPhotoLibrary;
@end
