//
//  VAContactsView.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/22/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VAContactsViewDelegate <NSObject>

- (void)didUpdateContacts;
- (void)didInviteFriends;
- (void)didAddUserAsConnection:(VAUser *)user;

@end

@interface VAContactsView : UIView
@property (nonatomic, weak) id<VAContactsViewDelegate> delegate;

- (void)sortAllContacts;

@end
