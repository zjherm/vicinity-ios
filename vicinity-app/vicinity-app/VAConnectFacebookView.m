//
//  VAConnectFacebookView.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/9/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAConnectFacebookView.h"
#import "VADesignTool.h"

@interface VAConnectFacebookView ()
@property (weak, nonatomic) IBOutlet UIButton *fbButton;
@end

@implementation VAConnectFacebookView

- (void)awakeFromNib {
    self.fbButton.layer.cornerRadius = 5.f;
    self.fbButton.clipsToBounds = YES;
    self.fbButton.backgroundColor = [[VADesignTool defaultDesign] facebookBlue];
}

#pragma mark - Actions

- (IBAction)fbButtonPushed:(UIButton *)sender {
    [self.delegate userDidPushFbButton:sender];
}

@end
