//
//  VAChangePasswordViewController.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/10/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAChangePasswordViewController.h"
#import <Google/Analytics.h>
#import "VAUserApiManager.h"
#import "VALoaderView.h"
#import "VANotificationMessage.h"
#import "VAControlTool.h"

@interface VAChangePasswordViewController ()
@property (strong, nonatomic) UITextField *activeField;
@property (strong, nonatomic) UIView *backgroundView;
@property (strong, nonatomic) VALoaderView *loader;

@property (weak, nonatomic) IBOutlet UITextField *oldPasswordField;
@property (weak, nonatomic) IBOutlet UITextField *nPasswordField;
@property (weak, nonatomic) IBOutlet UIButton *oldPassVisibleButton;
@property (weak, nonatomic) IBOutlet UIButton *nPassVisibleButton;
@end

@implementation VAChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [self cancelButton];
    self.navigationItem.rightBarButtonItem = [self saveButtonDisabled];
    
    NSAttributedString *oldPlaceholder = [[NSAttributedString alloc] initWithString:self.oldPasswordField.placeholder attributes:@{ NSForegroundColorAttributeName : [[UIColor grayColor] colorWithAlphaComponent:0.7f]}];
    self.oldPasswordField.attributedPlaceholder = oldPlaceholder;
    
    NSAttributedString *newPlaceholder = [[NSAttributedString alloc] initWithString:self.nPasswordField.placeholder attributes:@{ NSForegroundColorAttributeName : [[UIColor grayColor] colorWithAlphaComponent:0.7f]}];
    self.nPasswordField.attributedPlaceholder = newPlaceholder;
    
    self.view.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapBackground = [[UITapGestureRecognizer alloc] initWithTarget:self.activeField action:@selector(resignFirstResponder)];
    [self.view addGestureRecognizer:tapBackground];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setTitle:@"Password"];
    
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Change password screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

#pragma mark - UIBarButtonItems

- (UIBarButtonItem *)cancelButton {
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelChanges)];
    [cancelButton setTitleTextAttributes:@{
                                           NSFontAttributeName: [[VADesignTool defaultDesign] vicinityRegularFontOfSize:15.f],
                                           NSForegroundColorAttributeName: [UIColor whiteColor]
                                           } forState:UIControlStateNormal];
    
    return cancelButton;
}

- (UIBarButtonItem *)saveButtonDisabled {
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(applyChanges)];
    saveButton.enabled = NO;
    [saveButton setTitleTextAttributes:@{
                                         NSFontAttributeName: [[VADesignTool defaultDesign] vicinityRegularFontOfSize:15.f],
                                         NSForegroundColorAttributeName: [[UIColor whiteColor] colorWithAlphaComponent:0.5f]
                                         } forState:UIControlStateNormal];
    
    return saveButton;
}

- (UIBarButtonItem *)saveButtonEnabled {
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(applyChanges)];
    [saveButton setTitleTextAttributes:@{
                                         NSFontAttributeName: [[VADesignTool defaultDesign] vicinityRegularFontOfSize:15.f],
                                         NSForegroundColorAttributeName: [UIColor whiteColor]
                                         } forState:UIControlStateNormal];
    
    return saveButton;
}

- (UIBarButtonItem *)backButton {
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackButton.png"] style:UIBarButtonItemStylePlain target:self.navigationController action:@selector(popViewControllerAnimated:)];
    
    return backItem;
}

#pragma mark - Methods 

- (void)cancelChanges {
    [self.activeField resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)applyChanges {
    [self.activeField resignFirstResponder];
    
    if ([self allFieldAreValid]) {
        [self showLoader];
        
        [[VAUserApiManager new] postChangeOldPassword:self.oldPasswordField.text
                                        toNewPassword:self.nPasswordField.text
                                       withCompletion:^(NSError *error, VAModel *model) {
                                           [self hideLoader];
                                           if (model) {
                                               [VANotificationMessage showSuccessUpdatePasswordMessageOnController:self withDuration:5.f];
                                               self.navigationItem.leftBarButtonItem = [self backButton];
                                               self.navigationItem.rightBarButtonItem = nil;
                                           } else if (error) {
                                               if ([error.localizedDescription isEqual:@(100011)]) {
                                                   [VANotificationMessage showIncorrectOldPasswordMessageOnController:self withDuration:5.f];
                                               } else {
                                                   [VANotificationMessage showConnectionErrorMessageOnController:self withDuration:5.f];
                                               }
                                           }
                                       }];
    }

}

- (void)darkenBackground {
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds))];
    backgroundView.backgroundColor = [UIColor blackColor];
    backgroundView.alpha = 0.f;
    self.backgroundView = backgroundView;
    [self.view addSubview:backgroundView];
    [UIView animateWithDuration:0.3f animations:^{
        backgroundView.alpha = 0.7f;
    }];
}

- (void)lightenBackground {
    [UIView animateWithDuration:0.3f animations:^{
        self.backgroundView.alpha = 0.0f;
    }];
    [self.backgroundView removeFromSuperview];
}

- (void)showActivityIndicator {
    VALoaderView *loader = [VALoaderView initWhiteLoader];
    loader.frame = CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), kLoaderHeight);
    [loader setCenter:CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds))];
    [self.backgroundView addSubview:loader];
    self.loader = loader;
    [loader startAnimating];
}

- (void)hideActivityIndicator {
    if (self.loader) {
        [self.loader stopAnimating];
        [self.loader removeFromSuperview];
        self.loader = nil;
    }
}

- (void)showLoader {
    [self darkenBackground];
    [self showActivityIndicator];
}

- (void)hideLoader {
    [self hideActivityIndicator];
    [self lightenBackground];
}

- (BOOL)allFieldAreValid {
    if (![[VAControlTool defaultControl] validPasswordInTextField:self.nPasswordField]) {
        [VANotificationMessage showInvalidPasswordMessageOnController:self withDuration:8.f];
        return NO;
    } else {
        return YES;
    }
}


#pragma mark - Actions 

- (IBAction)oldSecureButtonPushed:(UIButton *)sender {
    if (self.oldPasswordField.isEditing) {
        [self.oldPasswordField resignFirstResponder];
    } else {
        if (self.oldPasswordField.secureTextEntry) {
            [self.oldPasswordField setFont:[[VADesignTool defaultDesign] vicinityRegularFontOfSize:14.f]];
            [self.oldPasswordField setSecureTextEntry:NO];
            [self.oldPassVisibleButton setImage:[UIImage imageNamed:@"eye-icon-black"] forState:UIControlStateNormal];
        } else {
            [self.oldPasswordField setSecureTextEntry:YES];
            [self.oldPassVisibleButton setImage:[UIImage imageNamed:@"eye-icon-gray"] forState:UIControlStateNormal];
        }
    }
}

- (IBAction)newSecureButtonPushed:(UIButton *)sender {
    if (self.nPasswordField.isEditing) {
        [self.nPasswordField resignFirstResponder];
    } else {
        if (self.nPasswordField.secureTextEntry) {
            [self.nPasswordField setFont:[[VADesignTool defaultDesign] vicinityRegularFontOfSize:14.f]];
            [self.nPasswordField setSecureTextEntry:NO];
            [self.nPassVisibleButton setImage:[UIImage imageNamed:@"eye-icon-black"] forState:UIControlStateNormal];
        } else {
            [self.nPasswordField setSecureTextEntry:YES];
            [self.nPassVisibleButton setImage:[UIImage imageNamed:@"eye-icon-gray"] forState:UIControlStateNormal];
        }
    }
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.activeField = textField;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *result = [textField.text stringByReplacingCharactersInRange:range withString:string];
    UITextField *anotherField = nil;
    
    if ([textField isEqual:self.nPasswordField]) {
        anotherField = self.oldPasswordField;
    } else {
        anotherField = self.nPasswordField;
    }
    
    if (![result isEqualToString:@""] && ![anotherField.text isEqualToString:@""]) {
        self.navigationItem.rightBarButtonItem = [self saveButtonEnabled];
    } else {
        self.navigationItem.rightBarButtonItem = [self saveButtonDisabled];
    }
    
    return [[VAControlTool defaultControl] setPasswordMaskForTextField:textField InRange:range replacementString:string];
}
@end
