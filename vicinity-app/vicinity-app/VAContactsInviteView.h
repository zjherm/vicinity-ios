//
//  VAContactsInviteView.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/22/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VAContactsInviteDelegate;

@interface VAContactsInviteView : UIView
@property (weak, nonatomic) id <VAContactsInviteDelegate> delegate;
@end

@protocol VAContactsInviteDelegate <NSObject>
- (void)userDidPushAllowContactsButton:(UIButton *)button;
@end