//
//  VAProfileSegmentedView.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 8/27/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VAPRrofileInfoDelegate;

typedef NS_ENUM(NSUInteger, VAProfileInfoType) {
    VAProfileInfoTypeGeneral,
    VAProfileInfoTypePosts,
    VAProfileInfoTypeLikes
};

@interface VAProfileSegmentedView : UIView
@property (weak, nonatomic) id <VAPRrofileInfoDelegate> delegate;
@property (assign, nonatomic) VAProfileInfoType profileInfoType;

// Constraints
@property (nonatomic, strong) NSArray *generalStateViewConstraints;
@property (nonatomic, strong) NSArray *postsStateViewConstraints;
@property (nonatomic, strong) NSArray *likesStateViewConstraints;
@property (nonatomic, strong) NSArray *centralStateViewConstraints;
@end

@protocol VAPRrofileInfoDelegate <NSObject>
- (void)didSelectGeneralInfo;
- (void)didSelectPostsInfo;
- (void)didSelectLikesInfo;
@end
