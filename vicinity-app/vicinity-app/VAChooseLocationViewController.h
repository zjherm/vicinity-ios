//
//  VAChooseLocationViewController.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 04.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VAPlaceDetails;
@class VALocation;
@protocol VALocationDelegate;

@interface VAChooseLocationViewController : UIViewController
@property (strong, nonatomic) VAPlaceDetails *currentLocation;

@property (weak, nonatomic) id <VALocationDelegate> delegate;
@end

@protocol VALocationDelegate <NSObject>
- (void)userDidChooseLocation:(VALocation *)location;
@end