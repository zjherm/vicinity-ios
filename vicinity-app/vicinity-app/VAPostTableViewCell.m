//
//  VAPostTableViewCell.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 18.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAPostTableViewCell.h"
#import "VAUser.h"
#import "UILabel+VACustomFont.h"
#import "VAPost.h"
#import "VALocation.h"
#import "VACoordinate.h"
#import "VAComment.h"
#import "NSDate+VAString.h"
#import "NSNumber+VAMilesDistance.h"
#import "VAControlTool.h"
#import "VAEventsViewController.h"
#import "VANotificationMessage.h"
#import "VAProfileViewController.h"
#import <OpenInGoogleMapsController.h>
#import "VAUserDefaultsHelper.h"
#import "VACommentsViewController.h"
#import "VAPhotoTool.h"
#import <UIImageView+AFNetworking.h>
#import "TTTAttributedLabel+VALinks.h"
#import "VABusinessProfileViewController.h"
#import <Google/Analytics.h>
#import "VAPostActionsView.h"
#import "VAPostActionsService.h"
#import <objc/runtime.h>

CGFloat const kPostActionsItemHeight = 37.0f;
CGFloat const kPostActionsTopArrowHeight = 2.0f;

@interface VAPostTableViewCell () <TTTAttributedLabelDelegate, VAPostActionsDelegate>

@property (strong, nonatomic) VAUser *user;
@property (assign, nonatomic) BOOL showCheckIn;

@property (nonatomic, weak) IBOutlet UIView *tapView;
@property (nonatomic, weak) IBOutlet VAPostActionsView *postActionsView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *postActionsViewHeightConstraint;

@property (nonatomic, weak) IBOutlet UIView *disableActionsView;

@end

CGFloat cellWidth;

@implementation VAPostTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.imageContainerHeight.constant = 0.f;
    self.textContainerHeight.constant = 0.f;
    self.bottomCommentConstraint.constant = 0.f;
    self.firstCommentContainerHeightConstraint.constant = 0.f;
    self.secondCommentContainerHeightConstraint.constant = 0.f;
    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showActionsTap:)];
//    [self.tapView addGestureRecognizer:tap];
    
    UITapGestureRecognizer* tapAvatar = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showProfileForUser)];
    [tapAvatar setNumberOfTapsRequired:1];
    [self.avatarView addGestureRecognizer:tapAvatar];
    self.nameLabel.userInteractionEnabled = YES;
    
    UITapGestureRecognizer* tapPhoto = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openPhotoBrowser)];
    [self.postImageView addGestureRecognizer:tapPhoto];
    self.postImageView.userInteractionEnabled = YES;
    
    self.postLabel.userInteractionEnabled = YES;

    self.avatarView.layer.cornerRadius = 24.5f;
    self.avatarView.clipsToBounds = YES;
    self.avatarView.userInteractionEnabled = YES;
    
    self.userInteractionEnabled = YES;
    self.layer.cornerRadius = 5.f;
    self.clipsToBounds = YES;
    
    [self.commentButtonLabel setLetterSpacingWithMultiplier:1.1f];
    [self.likeButtonLabel setLetterSpacingWithMultiplier:1.2f];
    
    self.postActionsView.delegate = self;
}

#pragma mark - Methods

- (void)showProfileForUser {
    [self showProfileForUserWithID:self.user.userId];
}

- (void)configureCellWithPost:(VAPost *)post {
    VAUser *user = post.user;
    VALocation *location = post.checkIn;
    
    self.user = user;
    self.post = post;
    
    [self setAvatarForUser:user];
    
    [self setNameForUser:user andCheckInForLocation:location];
    
    [self setDistanceForLocation:location];
    
    [self setDateTextForPost:post];
    
    [self setLocationTextForLocation:location];
    
    [self setPostTextForPost:post];
    
    [self setImageForPost:post];
    
    [self setLikesCountForPost:post];
    
    [self setCommentsButtonForPost:post];
    
    [self setCommentsForPost:post];
    
    [self setLikeButtonForPost:post];
    
    [self setEditButtonForPost:post];
    
    self.postActionsView.hidden = YES;
    self.postActionsView.postActions = [VAPostActionsService postActionsForPost:post];
    self.postActionsViewHeightConstraint.constant = kPostActionsTopArrowHeight + kPostActionsItemHeight*self.postActionsView.postActions.count;
    if (objc_getAssociatedObject(post, @"isPostTextTruncated")) {
        self.moreFakeButton.userInteractionEnabled = YES;
    } else {
        self.moreFakeButton.userInteractionEnabled = NO;
    }
    self.disableActionsView.hidden = post.user.status != VAUserStatusLocked;
}

- (void)setAvatarForUser:(VAUser *)user {
    if (user.avatarURL) {
        self.avatarView.image = [UIImage imageNamed:@"avatar-placeholder"];
        [[VAImageCache sharedImageCache] getImageWithURL:user.avatarURL success:^(UIImage *image, NSString *imageURL) {
            if ([self.user.avatarURL isEqualToString:imageURL]) {
                self.avatarView.image = image;
            }
        } failure:^(NSError *error) {
            NSLog(@"Failed to get image.");
        }];
    } else {
        self.avatarView.image = [UIImage imageNamed:@"avatar-placeholder"];
    }
}

- (void)setNameForUser:(VAUser *)user andCheckInForLocation:(VALocation *)location {
    
    [((TTTAttributedLabel *)self.nameLabel) setLinkModels:[NSArray array]];

    if (location.address) {
        self.nameLabel.attributedText = [self attributedTextForUserName:user.nickname andLocation:location.name];
    } else {
        self.nameLabel.text = user.nickname;
    }
    
    self.nameLabel.activeLinkAttributes = @{NSForegroundColorAttributeName: [[VADesignTool defaultDesign] vicinityDarkBlue]};
    self.nameLabel.linkAttributes = @{NSForegroundColorAttributeName: [[VADesignTool defaultDesign] vicinityDarkBlue]};
    
    [self.nameLabel addLinkToURL:[self authorUrl] withRange:[self.nameLabel.text rangeOfString:user.nickname]];

    if (location.address) {
        [self.nameLabel addLinkToURL:[self locationUrl] withRange:[self.nameLabel.text rangeOfString:location.name]];
    }
}

- (void)setDateTextForPost:(VAPost *)post {
    self.dateLabel.text = [post.date messageTimeForDate];
}

- (void)setDistanceForLocation:(VALocation *)location {
    if (location.distance) {
        self.distanceLabel.text = [location.distance milesStringFromNSNumberMeters:NO];
    } else {
        self.distanceLabel.text = @"< 0.5 mi";
    }
}

- (void)setLocationTextForLocation:(VALocation *)location {
    self.locationLabel.text = location.cityName;
}

- (void)setPostTextForPost:(VAPost *)post {
    NSMutableAttributedString *attributedToken = [[NSMutableAttributedString alloc] initWithString:@"... " attributes:@{
                                                                                                                        NSFontAttributeName : [[VADesignTool defaultDesign] vicinityLightFontOfSize:15.f],
                                                                                                                        NSForegroundColorAttributeName: [UIColor blackColor],
//                                                                                                                        NSLinkAttributeName: [NSURL URLWithString:@"More"]
                                                                                                                        }];
    [attributedToken appendAttributedString:[[NSAttributedString alloc] initWithString:@"more" attributes:@{
                                                                                                            NSFontAttributeName : [[VADesignTool defaultDesign] vicinityLightFontOfSize:15.f],
                                                                                                            NSForegroundColorAttributeName: [[VADesignTool defaultDesign] vicinityDarkBlue],
//                                                                                                            NSLinkAttributeName: [NSURL URLWithString:@"More"]
                                                                                                            }]];
    self.postLabel.attributedTruncationToken = attributedToken;
    self.postLabel.attributedText = [self attributedStringForPost:post];
    self.postLabel.numberOfLines = 4;
    self.postLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    
    self.postLabel.delegate = self;
    
    CGFloat nameLabelHeight = [self.nameLabel heightForNameAndCheckInLabelWithWidth:CGRectGetWidth([[UIScreen mainScreen] bounds]) - 125.f andPost:post];   //if > 19 - label is multiline
    
    if (!post.text && nameLabelHeight > 19) {
        self.textContainerHeight.constant = 19.f;
    } else {
        self.textContainerHeight.constant = 0.f;
    }
    
    if (post.text) {
        self.textContainerHeight.priority = 250.f;
    } else {
        self.textContainerHeight.priority = 900.f;
    }
    
    [self.postLabel addPostAtrributes];
}

- (void)setImageForPost:(VAPost *)post {

    if (post.photoURL) {
        self.postImageView.image = nil;
        [[VAImageCache sharedImageCache] getImageWithURL:post.photoURL success:^(UIImage *image, NSString *imageURL) {
            if ([self.post.photoURL isEqualToString:imageURL]) {
                self.postImageView.image = image;
            }
        } failure:^(NSError *error) {
            NSLog(@"Failed to get image.");
        }];
        
        CGFloat k = [post.isLandscapePhoto boolValue] ? 9.0f/16.0f : 1.0f;
        self.imageContainerHeight.constant = cellWidth*k + 12.0f;
    } else {
        self.postImageView.image = nil;
        self.imageContainerHeight.constant = 0.0f;
    }
}

- (void)setLikesCountForPost:(VAPost *)post {
    self.likeLabel.text = [NSString stringWithFormat:@"%ld", (long)[post.likesAmount integerValue]];
    if ([post.likesAmount integerValue] == 0) {
        self.likesAndCommentsContainerHeight.constant = 0.f;
        [self.likeView setHidden:YES];
    } else {
        self.likesAndCommentsContainerHeight.constant = 24.f;
        [self.likeView setHidden:NO];
    }
}

- (void)setCommentsButtonForPost:(VAPost *)post {
    if ([post.commentsAmount integerValue] > 2) {
        [self.viewAllCommentsButton setHidden:NO];
        [self.viewAllCommentsButton setTitle:[NSString stringWithFormat:@"View all %ld comments", (unsigned long)[post.commentsAmount integerValue]] forState:UIControlStateNormal];
        self.viewAllCommentsButton.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
        self.likesAndCommentsContainerHeight.constant = 24.f;
    } else {
        [self.viewAllCommentsButton setHidden:YES];
    }
}

- (void)setCommentsForPost:(VAPost *)post {
    CGFloat commentLabelLeadingConstraint = 18.f;
    CGFloat commentLabelTrailngConstraint = 11.f;
    
    self.firstCommentLabel.text = @"";
    self.secondCommentLabel.text = @"";
    
    self.firstCommentLabel.delegate = self;
    self.secondCommentLabel.delegate = self;
    
    if ([post.commentsArray count] > 0) {
        for (int i = 0; i < [post.commentsArray count]; i++) {
            VAComment *comment = [post.commentsArray objectAtIndex:i];
            
            if (i == 0) {
                self.firstCommentLabel.attributedText = [self attributedStringForComment:comment];
                [self.firstCommentLabel addCommentAtrributes];
                self.firstCommentContainerHeightConstraint.constant = [self.firstCommentLabel heightForCommentLabelWithWidth:cellWidth - commentLabelTrailngConstraint - commentLabelLeadingConstraint andComment:comment];
            } else if (i == 1) {
                self.secondCommentLabel.attributedText = [self attributedStringForComment:comment];
                [self.secondCommentLabel addCommentAtrributes];
                self.secondCommentContainerHeightConstraint.constant = [self.firstCommentLabel heightForCommentLabelWithWidth:cellWidth - commentLabelTrailngConstraint - commentLabelLeadingConstraint andComment:comment];
            }
        }
        self.bottomCommentConstraint.constant = 7.f;
        self.bottomCommentConstraint.priority = 899;

    } else {
        self.bottomCommentConstraint.constant = 0.f;
        self.bottomCommentConstraint.priority = 900;
        self.firstCommentContainerHeightConstraint.constant = 0.f;
        self.secondCommentContainerHeightConstraint.constant = 0.f;
    }
    
    if ([post.commentsArray count] == 1) {
        self.secondCommentContainerHeightConstraint.constant = 0.f;
    }
}

- (void)setLikeButtonForPost:(VAPost *)post {
    if ([post.isLiked boolValue] == YES) {
        self.likeImageView.image = [UIImage imageNamed:@"isLiked-red.png"];
    } else {
        self.likeImageView.image = [UIImage imageNamed:@"isUnliked.png"];
    }
}

- (void)setEditButtonForPost:(VAPost *)post {
//    VAUser *loggedUser = (VAUser *)[VAUserDefaultsHelper getModelForKey:VASaveKeys_CurrentUser];
//    if ([loggedUser.userId isEqualToString:post.user.userId] || [post.isSubscribed boolValue]) {
//        [self.editButton setHidden:NO];
//    } else {
        [self.editButton setHidden:YES];
//    }
}

- (NSMutableAttributedString *)attributedTextForUserName:(NSString *)name andLocation:(NSString *)location {
    NSRange nameBoldedRange = NSMakeRange(0, name.length);
    NSMutableAttributedString *checkInAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ checked in at %@", name, location]
                                                                                          attributes:@{
                                                                                                       NSFontAttributeName : [[VADesignTool defaultDesign] vicinityLightFontOfSize:13.f],
                                                                                                       NSForegroundColorAttributeName: [UIColor blackColor]
                                                                                                       }];
    [checkInAttrString addAttributes:@{
                                       NSFontAttributeName: [[VADesignTool defaultDesign] vicinityBoldFontOfSize:13.f],
                                       NSForegroundColorAttributeName: [[VADesignTool defaultDesign] vicinityDarkBlue]
                                       }
                               range:nameBoldedRange];
    
    NSInteger preCheckInLength = [[NSString stringWithFormat:@"%@ checked in at ", name] length];
    NSRange checkInRange = NSMakeRange(preCheckInLength, location.length);
    
    [checkInAttrString addAttributes:@{
                                       NSFontAttributeName: [[VADesignTool defaultDesign] vicinityLightFontOfSize:13.f],
                                       NSForegroundColorAttributeName: [[VADesignTool defaultDesign] vicinityDarkBlue]
                                       }
                               range:checkInRange];
    return checkInAttrString;
}

- (NSMutableAttributedString *)attributedStringForComment:(VAComment *)comment {
    NSString *commentText = comment.text;
    NSString *userNickname = comment.user.nickname;
    NSString *fullComment = [NSString stringWithFormat:@"%@ %@",userNickname, commentText];
    NSMutableAttributedString *commentAttrString = [[NSMutableAttributedString alloc] initWithString:fullComment attributes:@{NSFontAttributeName : [[VADesignTool defaultDesign] vicinityLightFontOfSize:13.5f]}];
    
    NSRange nameBoldedRange = NSMakeRange(0, userNickname.length);
    
    
    [commentAttrString addAttributes:@{
                                       NSFontAttributeName: [[VADesignTool defaultDesign] vicinityRegularFontOfSize:13.5f],
                                       NSForegroundColorAttributeName: [[VADesignTool defaultDesign] vicinityDarkBlue]
                                       }
                               range:nameBoldedRange];
    
    return commentAttrString;
}

- (NSMutableAttributedString *)attributedStringForPost:(VAPost *)post {
    NSMutableAttributedString *postString = nil;
    
    if (post.text) {
        postString = [[NSMutableAttributedString alloc] initWithString:post.text
                                                            attributes:@{
                                                                         NSFontAttributeName : [[VADesignTool defaultDesign] vicinityLightFontOfSize:15.f],
                                                                         NSForegroundColorAttributeName: [UIColor blackColor]
                                                                         }];
    }
    
    return postString;

}

- (void)showProfileForUserWithAlias:(NSString *)alias {
    VAProfileViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"profileViewController"];
    vc.searchAlias = alias;
    vc.isUsersAccountMode = YES;
    VAEventsViewController *currentVC = (VAEventsViewController *)[VAControlTool topScreenController];
    [currentVC.navigationController pushViewController:vc animated:YES];
}

- (void)showProfileForUserWithID:(NSString *)ID {
    VAProfileViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"profileViewController"];
    vc.userId = ID;
    vc.isUsersAccountMode = YES;
    VAEventsViewController *currentVC = (VAEventsViewController *)[VAControlTool topScreenController];
    [currentVC.navigationController pushViewController:vc animated:YES];
}

- (void)showPostCheckIn {
    self.showCheckIn = YES;
    [self showLocationForPost:self.post];
    
    //code for google analytics
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Location"
                                                          action:@"User clicked post's check in"
                                                           label:@"From comments screen"
                                                           value:nil] build]];
}

- (void)showBusinessProfile {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VABusinessProfileViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"VABusinessProfileViewController"];
    vc.isBusinessSearchModeMode = YES;
    vc.googlePlaceID = self.post.checkIn.placeID;
    VAEventsViewController *currentVC = (VAEventsViewController *)[VAControlTool topScreenController];
    [currentVC.navigationController pushViewController:vc animated:YES];
}

- (void)showPostLocation {
    self.showCheckIn = NO;
    [self showLocationForPost:self.post];
    
    //code for google analytics
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Location"
                                                          action:@"User clicked post's location"
                                                           label:@"From comments screen"
                                                           value:nil] build]];
}

- (void)showLocationForPost:(VAPost *)post {
    GoogleMapDefinition *mapDefinition = [[GoogleMapDefinition alloc] init];
    double lat;
    double lon;
    if (self.showCheckIn) {
        lat = [post.checkIn.coordinate.latitude doubleValue];
        lon = [post.checkIn.coordinate.longitude doubleValue];
        if ([OpenInGoogleMapsController sharedInstance].isGoogleMapsInstalled) {
            mapDefinition.queryString = [NSString stringWithFormat:@"%@ %@", post.checkIn.name, post.checkIn.address];
        } else {
            mapDefinition.queryString = [NSString stringWithFormat:@"%f %f", lat, lon];
        }
    } else {
        lat = [post.location.latitude doubleValue];
        lon = [post.location.longitude doubleValue];
        mapDefinition.queryString = [NSString stringWithFormat:@"%f %f", lat, lon];
    }
    [[OpenInGoogleMapsController sharedInstance] openMap:mapDefinition];
}

- (void)openPhotoBrowser {
    [[VAPhotoTool defaultPhoto] openPhotoBrowserFor:self.postImageView.image];
    
    //code for google analytics
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Picture"
                                                          action:@"User clicked the picture"
                                                           label:@"From Vicinity screen"
                                                           value:nil] build]];
}

- (NSURL *)authorUrl {
    return [NSURL URLWithString:@"Author"];
}

- (NSURL *)locationUrl {
    return [NSURL URLWithString:@"Location"];
}

- (NSURL *)mentionUrl {
    return [NSURL URLWithString:@"Mention"];
}

- (NSURL *)hashtagUrl {
    return [NSURL URLWithString:@"Hashtag"];
}

- (VALinkType)linkTypeForUrl:(NSURL *)url {
    if ([url.absoluteString isEqualToString:@"Author"]) {
        return VALinkTypeAuthor;
    } else if ([url.absoluteString isEqualToString:@"Location"]) {
        return VALinkTypeLocation;
    } else if ([url.absoluteString isEqualToString:@"Mention"]) {
        return VALinkTypeMention;
    } else if ([url.absoluteString isEqualToString:@"Hashtag"]) {
        return VALinkTypeHashtag;
    } else if ([url.absoluteString isEqualToString:@"More"]) {
        return VALinkTypeMore;
    } else if ([url.absoluteString rangeOfString:@"http://"].location != NSNotFound) {
        return VALinkTypeURL;
    } else {
        return -1;
    }
}

- (void)showProfileForFirstCommentAuthor {
    VAComment *firstComment = [self.post.commentsArray firstObject];
    [self showProfileForUserWithID:firstComment.user.userId];
}

- (void)showProfileForSecondCommentAuthor {
    VAComment *secondComment = [self.post.commentsArray lastObject];
    [self showProfileForUserWithID:secondComment.user.userId];}

#pragma mark - TTTAttributedLabelDelegate

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    switch ([self linkTypeForUrl:url]) {
        case VALinkTypeAuthor:
            [self showProfileForUser];
            break;
        case VALinkTypeLocation:
//#ifdef VICINITY
//            [self showPostCheckIn];
//#else
//            [self showBusinessProfile];
//#endif
            [self showBusinessProfile];
            break;
        case VALinkTypeMore:
            self.postLabel.attributedText = [self attributedStringForPost:self.post];
            if (self.delegate && [self.delegate respondsToSelector:@selector(shouldShowCommentsForPost:)]) {
                [self.delegate shouldShowCommentsForPost:self.post];
            }
            break;
        case VALinkTypeURL:
            [[UIApplication sharedApplication] openURL:url];
            break;
        default:
            break;
    }
}

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithTransitInformation:(NSDictionary *)components {
    [self performActionToWordType:components[kType] wordString:components[kWord]];
}

#pragma mark - TTTAttributedLabel Actions

- (void)performActionToWordType:(NSString *)wordType wordString:(NSString *)string {
    if ([wordType isEqualToString:kHashtag]) {
        NSString *hashtag = [string substringFromIndex:1];              //string here shows #hashtag with #
        
        VAEventsViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"VAEventsViewController"];
        vc.isHashTagMode = YES;
        vc.hashtagString = hashtag;
        VAEventsViewController *currentVC = (VAEventsViewController *)[VAControlTool topScreenController];
        [currentVC.navigationController pushViewController:vc animated:YES];
    } else if ([wordType isEqualToString:kMention]) {
        NSString *nickname = [string substringFromIndex:1];
        [self showProfileForUserWithAlias:nickname];
    } else if ([wordType isEqualToString:kAlias]) {
        [self showProfileForUserWithAlias:string];
    } else if ([wordType isEqualToString:kCommentWord]) {
        
    }
}
    
#pragma mark - Height methods

- (CGFloat)heightForCellWithPost:(VAPost *)post {
    cellWidth = CGRectGetWidth([[UIScreen mainScreen] bounds]) - 2 * 5.f;
    
    CGFloat baseHeight = 116.f;
    CGFloat postContainerHeight = [self heightForPostContainerWithPost:post];
    CGFloat imageContainerHeight = [self heightForImageContainerWithURLString:post.photoURL landscape:[post.isLandscapePhoto boolValue]];
    CGFloat commentsContainerHeight = [self heightForCommentsContainerWithComments:post.commentsArray];
    CGFloat likeViewHeight = [self heightForLikeViewForPost:post];
    
    return baseHeight + postContainerHeight + imageContainerHeight + commentsContainerHeight + likeViewHeight;
}

- (CGFloat)heightForPostContainerWithPost:(VAPost *)post {
    CGFloat postLabelIndentLeft = 18.f;
    CGFloat postLabelIndentRight = 28.f;
    CGFloat bottomPostLabelContraint = 10.f;
    
    CGFloat nameLabelHeight = [self.nameLabel heightForNameAndCheckInLabelWithWidth:CGRectGetWidth([[UIScreen mainScreen] bounds]) - 125.f andPost:post];   //if > 20 - label is multiline
    
    if (!post.text && nameLabelHeight > 20) {
        return 19.f;
    }
    
    if (!post.text) {
        return 0;
    } else {
        self.postLabel.text = post.text;
        CGFloat postLabelHeight = [self.postLabel heightForPostLabelWithWidth:cellWidth - postLabelIndentLeft - postLabelIndentRight];
        if (postLabelHeight < 72.0f) {
            objc_setAssociatedObject(post, @"isPostTextTruncated", nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        } else {
            postLabelHeight = 72.0f;
            objc_setAssociatedObject(post, @"isPostTextTruncated", @(YES), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        }
        //postLabelHeight = postLabelHeight < 72.0f ? postLabelHeight : 72.0f;
        return postLabelHeight + bottomPostLabelContraint;
    }
}

- (CGFloat)heightForImageContainerWithURLString:(NSString *)urlString landscape:(BOOL)isLandscape {
    CGFloat bottomImageViewContraint = 12.f;
    
    if (!urlString) {
        return 0;
    } else {
        CGFloat k = isLandscape ? 9.0f/16.0f : 1.0f;
        return cellWidth*k + bottomImageViewContraint;        //imageView is always square, so height equals cell's width
    };
}

- (CGFloat)heightForCommentsContainerWithComments:(NSArray *)commentsArray {
    CGFloat commentsVerticalConstraint = 1.f;               //between first and second comment
    CGFloat bottomCommentsContraint = 7.f;
    
    CGFloat commentLabelLeadingConstraint = 18.f;
    CGFloat commentLabelTrailngConstraint = 11.f;
    
    VAComment *firstComment = [commentsArray count] > 0 ? [commentsArray objectAtIndex:0] : nil;
    VAComment *secondComment = [commentsArray count] > 1 ? [commentsArray objectAtIndex:1] : nil;
    
    self.firstCommentLabel.text = firstComment.text;
    self.secondCommentLabel.text = secondComment.text;
    
    CGFloat firstCommentHeight = firstComment ? [self.firstCommentLabel heightForCommentLabelWithWidth:cellWidth - commentLabelTrailngConstraint - commentLabelLeadingConstraint andComment:firstComment] : 0.f;
    CGFloat secondCommentHeight = secondComment ? [self.secondCommentLabel heightForCommentLabelWithWidth:cellWidth - commentLabelTrailngConstraint - commentLabelLeadingConstraint andComment:secondComment] : 0.f;

    if ([commentsArray count] == 0) {
        return commentsVerticalConstraint;
    } else {
        return  firstCommentHeight + commentsVerticalConstraint + secondCommentHeight + bottomCommentsContraint;
    }
}

- (CGFloat)heightForLikeViewForPost:(VAPost *)post {
    if ([post.likesAmount integerValue] == 0 && [post.commentsAmount integerValue] < 3) {
        return 0.f;
    } else {
        return 24.f;
    }
}

#pragma mark - Actions

- (IBAction)showActionsTap:(id)sender {
    if (self.delegate) {
        [self.delegate didSelectShowActions:self];
    }
//    if (self.postActionsView.hidden) {
//        [self showPostActions];
//    } else {
//        [self hidePostActions];
//    }
}

#pragma mark - Helpers

- (void)showPostActions {
    self.postActionsView.alpha = 0;
    self.postActionsView.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        self.postActionsView.alpha = 1;
    }];
}

- (void)hidePostActions {
    [UIView animateWithDuration:0.3f animations:^{
        self.postActionsView.alpha = 0;
    } completion:^(BOOL finished) {
        self.postActionsView.hidden = YES;
    }];
}

#pragma mark - Post Actions delegate

- (void)editPostSelected {
    if (self.delegate) {
        [self.delegate editPostSelected:self.post];
    }
    [self hidePostActions];
}

- (void)deletePostSelected {
    if (self.delegate) {
        [self.delegate deletePostSelected:self.post];
    }
    [self hidePostActions];
}

- (void)blockUserSelected {
    if (self.delegate) {
        [self.delegate blockUserSelected:self.post.user];
    }
    [self hidePostActions];
}

- (void)reportSelected {
    if (self.delegate) {
        [self.delegate reportSelected:self.post.user];
    }
    [self hidePostActions];
}

- (void)turnOffNotificationsSelected {
    if (self.delegate) {
        [self.delegate turnOffNotificationsSelected:self.post];
    }
    [self hidePostActions];
}
- (IBAction)moreButtonTapped:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(shouldShowCommentsForPost:)]) {
        [self.delegate shouldShowCommentsForPost:self.post];
    }

}

@end
