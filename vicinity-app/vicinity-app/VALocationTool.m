//
//  VALocationTool.m
//  vicinity-app
//
//  Created by Panda Systems on 5/21/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VALocationTool.h"
#import "VANotificationTool.h"
#import "VAUserDefaultsHelper.h"
#import "VANotificationMessage.h"

NSString * const kLatValue = @"kLatValue";
NSString * const kLonValue = @"kLonValue";

@interface VALocationTool () <UIAlertViewDelegate, CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, readwrite, getter=isServicesEnabled) BOOL servicesEnabled;
@property (copy, nonatomic) VALocationCompletionBlock complitionBlock;
@property (nonatomic, assign) NSInteger startApplicationStatus;              //when we launch the app, didChangeAuthorizationStatus method is called. Here we set the curren status on launch

@property (assign, nonatomic) BOOL isLocationUpdated;

@end

@implementation VALocationTool

+ (instancetype)sharedInstance {
    static VALocationTool *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [VALocationTool new];
        shared.startApplicationStatus = -1;            //here we set nonexistent status
    });
    return shared;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _servicesEnabled = NO;
    }
    return self;
}

#pragma mark - Methods

- (void)requestInUseAuthorization {
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    if (status == kCLAuthorizationStatusDenied) {
        self.servicesEnabled = NO;
    } else if (status == kCLAuthorizationStatusNotDetermined) {
        // The user has not enabled any location services. Request background authorization.
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            // For iOS 8
            [self.locationManager requestWhenInUseAuthorization];
        }
        [self.locationManager startUpdatingLocation];
    } else if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusAuthorizedAlways) {
        self.servicesEnabled = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:kDidChangeLocationStatus object:nil];
    }
}

- (void)getCurrentDeviceLocationWithComplition:(VALocationCompletionBlock)complitionBlock {
    self.complitionBlock = complitionBlock;
    self.isLocationUpdated = NO;
//    if (self.servicesEnabled) {
        [self updateCurrentLocation];
//    }
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (self.startApplicationStatus == -1) {          //if self.startApplicationStatus == -1 it means that we just launch the app and this method has been called authomaticly
        self.startApplicationStatus = status;
    } else {
        if (status == kCLAuthorizationStatusNotDetermined || status == kCLAuthorizationStatusRestricted) {
            self.servicesEnabled = NO;
        } else if (status == kCLAuthorizationStatusDenied) {
            self.servicesEnabled = NO;
            [[NSNotificationCenter defaultCenter] postNotificationName:kDidChangeLocationStatus object:nil];
        } else if (!self.isServicesEnabled){
            self.servicesEnabled = YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:kDidChangeLocationStatus object:nil];
            [self updateCurrentLocation];
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {      //this method can be called more then 1 time (even if you perform update location only once)
    [self.locationManager stopUpdatingLocation];
    CLLocation *location = [locations lastObject];
    if (!self.isLocationUpdated) {           //this variable is used to send data to block only once
        self.isLocationUpdated = YES;
        if (self.complitionBlock) {
            self.complitionBlock(nil, location);
        } else {
            double lat = location.coordinate.latitude;
            double lon = location.coordinate.longitude;
            [VAUserDefaultsHelper savelatitude:lat andLongitude:lon];
            
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    self.isLocationUpdated = NO;
    if (self.complitionBlock) {
        self.complitionBlock(error, nil);
    }
}

#pragma mark - Methods

- (void)updateCurrentLocation {

//    if (TARGET_IPHONE_SIMULATOR) {
//        CLLocation *location = [[CLLocation alloc] initWithLatitude:53.911641 longitude:27.595961];
//        self.isLocationUpdated = YES;
//        if (self.complitionBlock) {
//            self.complitionBlock(nil, location);
//        } else {
//            double lat = location.coordinate.latitude;
//            double lon = location.coordinate.longitude;
//            if (TARGET_IPHONE_SIMULATOR) {
//                lat = 53.911641;
//                lon = 27.595961;
//            }
//            [VAUserDefaultsHelper savelatitude:lat andLongitude:lon];
//        }
//    } else {
        [self.locationManager startUpdatingLocation];
//    }
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        if (!self.isLocationUpdated) {
//            NSInteger code = 1000;
//            NSString* errorDomain = @"VALocationTool";
//            NSString *description = NSLocalizedString(@"update.location.error", nil);
//            NSDictionary *userInfo = [NSDictionary dictionaryWithObject:description forKey:NSLocalizedDescriptionKey];
//            NSError* error = [NSError errorWithDomain:errorDomain code:code userInfo:userInfo];
//            self.complitionBlock(error, nil);
//        } else {
//            self.isLocationUpdated = NO;
//        }
//    });
}

@end
