//
//  VAPhotoViewController.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 26.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAPhotoViewController.h"
#import "UILabel+VACustomFont.h"
#import "VAControlTool.h"
#import "DEMONavigationController.h"

@interface VAPhotoViewController ()
@property (weak, nonatomic) IBOutlet UILabel *takePhotoLabel;
@property (weak, nonatomic) IBOutlet UILabel *choosePhotoLabel;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalAlignment;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *middleConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alignmentCenterYConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewConstraint;


- (IBAction)actionTakePhoto:(id)sender;
- (IBAction)actionChooseExistingPhoto:(id)sender;
- (IBAction)actionCancel:(id)sender;
@end

BOOL isTranslucent;

@implementation VAPhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.takePhotoLabel setLetterSpacingWithMultiplier:1.2f];
    [self.choosePhotoLabel setLetterSpacingWithMultiplier:1.2f];
    
    self.backgroundView.backgroundColor = [UIColor clearColor];
    if (IOS8 || IOS9) {
        UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        
        UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
        effectView.frame = self.view.frame;
        
        [self.view insertSubview:effectView atIndex:0];
    } else if (IOS7) {
        self.backgroundView.backgroundColor = [UIColor blackColor];
        self.backgroundView.alpha = 0.85f;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    isTranslucent = self.navigationController.navigationBar.translucent;

    if (isTranslucent) {
        self.topViewConstraint.constant = 64.f;
    } else {
        self.bottomConstraint.constant += 64.f;        //navBar height
        self.verticalAlignment.constant += 32.f;       //navBar height / 2
    }
    
    if (SMALL_HEIGHT_DEVICE) {
        self.middleConstraint.constant = 30.f;
        if (isTranslucent) {
            self.alignmentCenterYConstraint.constant = 28.f;     //(60 - 32)
        } else {
            self.alignmentCenterYConstraint.constant = 60.f;
        }
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];

    if (!isTranslucent) {
        self.bottomConstraint.constant -= 64.f;        //navBar height
        self.verticalAlignment.constant -= 32.f;       //navBar height / 2
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)actionTakePhoto:(id)sender {
    [self.delegate openCamera];
}

- (IBAction)actionChooseExistingPhoto:(id)sender {
    [self.delegate openPhotoLibrary];
}

- (IBAction)actionCancel:(id)sender {
    [self.delegate dismissPhotoView];
}

@end
