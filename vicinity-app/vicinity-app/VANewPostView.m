//
//  VANewPostView.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 29.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VANewPostView.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "VANotificationMessage.h"
#import "VAControlTool.h"
#import "VAUserApiManager.h"
#import "VALoginResponse.h"
#import "VAUserDefaultsHelper.h"

@interface VANewPostView ()

- (IBAction)locationButtonPushed:(id)sender;
- (IBAction)photoButtonPushed:(id)sender;
- (IBAction)postButtonPushed:(id)sender;

@end

@implementation VANewPostView

- (void)awakeFromNib {
    self.facebookImageView.layer.cornerRadius = 2.f;
    self.facebookImageView.clipsToBounds = YES;

    self.avatarView.layer.cornerRadius = 49.f / 2;
    self.avatarView.clipsToBounds = YES;
    
    self.locationLabel.text = @"";
    [self.locationLabel sizeToFit];
    
    self.boundsView.layer.cornerRadius = 5.f;
    self.boundsView.clipsToBounds = YES;
    
    self.postButton.enabled = 0;
    [self.postButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    [self.postButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    UITapGestureRecognizer *tapBackground = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
    [tapBackground setNumberOfTapsRequired:1];
    [self addGestureRecognizer:tapBackground];
    [self.postButton removeGestureRecognizer:tapBackground];
    [self.postImageView removeGestureRecognizer:tapBackground];
    [self.locationLabel removeGestureRecognizer:tapBackground];
    
    UITapGestureRecognizer *tapLocation = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showLocationActionSheet:)];
    [tapLocation setNumberOfTapsRequired:1];
    [self.locationLabel addGestureRecognizer:tapLocation];
    [self.locationLabel setUserInteractionEnabled:YES];
    self.isLocationSet = NO;
    self.isPhotoSet = NO;
    
    UITapGestureRecognizer *tapPhoto = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showPhotoActionSheet:)];
    [tapPhoto setNumberOfTapsRequired:1];
    [self.postImageView addGestureRecognizer:tapPhoto];
    [self.postImageView setUserInteractionEnabled:YES];
    
    self.imageContainerHeight.constant = 0.f;
    
    self.textView.textContainerInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);

    self.locationLabel.textColor = [UIColor blackColor];
    self.locationLabel.numberOfLines = 2;
    
    [self.popoverArrow setHidden:YES];
}

#pragma mark - Actions

- (IBAction)locationButtonPushed:(id)sender {
    [self.delegate locationButtonPushed];
}

- (IBAction)photoButtonPushed:(id)sender {
    [self.delegate photoButtonPushed];
}

- (IBAction)postButtonPushed:(id)sender {
    [self.delegate postButtonPushed];
}

- (IBAction)fbSwitchDidChange:(UISwitch *)sender {
    if (sender.isOn) {
        self.facebookImageView.image = [UIImage imageNamed:@"facebook-icon-enabled.png"];
        if (![FBSDKAccessToken currentAccessToken]) {
            [self getPublishAccessToken];
        } else if (![[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_actions"]) {
            [self askForPublishPermissions];
        } else {
            [self addTokenToCurrentUser:[[FBSDKAccessToken currentAccessToken] tokenString]];
        }
    } else {
        self.facebookImageView.image = [UIImage imageNamed:@"facebook-icon-disabled.png"];
    }
}

#pragma mark - Methods

- (void) dismissKeyboard:(id)sender {
    [self.textView resignFirstResponder];
}

- (void)showLocationActionSheet:(id)sender {
    [self.delegate showLocationActions];
}

- (void)showPhotoActionSheet:(id)sender {
    [self.delegate showPhotoActions];

}

- (void)askForPublishPermissions {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    NSArray *permissions = @[@"publish_actions"];
    [login logInWithPublishPermissions:permissions handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            [VANotificationMessage showLoginCancelledMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
            [self.facebookSwitch setOn:NO animated:YES];
            self.facebookImageView.image = [UIImage imageNamed:@"facebook-icon-disabled.png"];
        } else if ([[result declinedPermissions] containsObject:@"publish_actions"]) {
            [self.facebookSwitch setOn:NO animated:YES];
            self.facebookImageView.image = [UIImage imageNamed:@"facebook-icon-disabled.png"];
        } else {
            [self addTokenToCurrentUser:[[FBSDKAccessToken currentAccessToken] tokenString]];
        }
    }];
}

- (void)getPublishAccessToken {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    NSArray *permissions = @[@"publish_actions"];
    [login logInWithPublishPermissions:permissions handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            [VANotificationMessage showLoginCancelledMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
            [self.facebookSwitch setOn:NO animated:YES];
        } else {
            FBSDKAccessToken *accessToken = [FBSDKAccessToken currentAccessToken];
            NSString *tokenString = [accessToken tokenString];
            
            [self addTokenToCurrentUser:tokenString];
            
            if ([[result declinedPermissions] containsObject:@"publish_actions"]) {
                [self.facebookSwitch setOn:NO animated:YES];
                self.facebookImageView.image = [UIImage imageNamed:@"facebook-icon-disabled.png"];

            } else {
                
            }
        }
    }];
}


- (void)addTokenToCurrentUser:(NSString *)tokenString {
    [[VAUserApiManager new] postCurrentSessionAddLinkToFacebookToken:tokenString
                                                      withCompletion:^(NSError *error, VAModel *model) {
                                                          if (model) {
                                                              VALoginResponse *response = (VALoginResponse *)model;
                                                              VAUser *user = response.loggedUser;
                                                              [VAUserDefaultsHelper saveModel:user forKey:VASaveKeys_CurrentUser];
                                                              [VAUserDefaultsHelper setAuthToken:response.sessionId];
                                                          } else if ([error.localizedDescription isEqual:@(100015)]) {
                                                              [VANotificationMessage showFacebookInUseMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
                                                              [self.facebookSwitch setOn:NO animated:YES];
                                                              self.facebookImageView.image = [UIImage imageNamed:@"facebook-icon-disabled.png"];
                                                          }
                                                      }];
}

@end
