//
//  VALoader.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 09.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

extern CGFloat const kLoaderHeight;

@interface VALoaderView : UIControl
@property (assign, nonatomic) BOOL hidesWhenStopped;              //default is YES
@property (strong, nonatomic) NSString *text;                     //if not set text is LOADING

+ (VALoaderView *)initLoader;
+ (VALoaderView *)initWhiteLoader;

- (void)startAnimating;             //for loader
- (void)stopAnimating;              //for loader
- (void)startRefreshing;            //for pull to refresh cusom view
- (void)stopRefreshing;             //for pull to refresh cusom view
- (void)containingScrollViewDidEndDragging:(UIScrollView *)containingScrollView;
- (void)containingScrollViewDidScroll:(UIScrollView *)containingScrollView;
@end

