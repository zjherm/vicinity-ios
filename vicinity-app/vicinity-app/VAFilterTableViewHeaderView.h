//
//  VAFilterTableViewHeaderView.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 25.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VADistanceFilterProtocol;

@interface VAFilterTableViewHeaderView : UIView
@property (weak, nonatomic) id <VADistanceFilterProtocol> delegate;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topContraint;

@property (strong, nonatomic) NSString *currentDistance;
@property (assign, nonatomic) BOOL hideCategories;          //default is NO (if YES only categories label is hidden)

- (void)updateText;
- (void)checkButtonsAvailability;

@end

@protocol VADistanceFilterProtocol <NSObject>
- (void)distanceFilterChangedToDistance:(NSString *)distance;
@end
