//
//  VAControlTool.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 02.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, VALoginMethod) {
    VALoginMethodFacebook,
    VALoginMethodEmail
};

#define IOS9 ([[[UIDevice currentDevice] systemVersion] hasPrefix:@"9"])
#define IOS8 ([[[UIDevice currentDevice] systemVersion] hasPrefix:@"8"])
#define IOS7 ([[[UIDevice currentDevice] systemVersion] hasPrefix:@"7"])

#define SMALL_HEIGHT_DEVICE ((CGRectGetHeight([UIScreen mainScreen].bounds) / CGRectGetWidth([UIScreen mainScreen].bounds)) <= 1.5) ? YES : NO

@class VAPost;

@interface VAControlTool : NSObject

+ (VAControlTool *)defaultControl;

- (BOOL)setNameAndSurnameMaskForTextField:(UITextField *)textField InRange:(NSRange)range replacementString:(NSString *)string;
- (BOOL)setEmailMaskForTextField:(UITextField *)textField InRange:(NSRange)range replacementString:(NSString *)string;
- (BOOL)setAliasMaskForTextField:(UITextField *)textField InRange:(NSRange)range replacementString:(NSString *)string;
- (BOOL)setPasswordMaskForTextField:(UITextField *)textField InRange:(NSRange)range replacementString:(NSString *)string;

- (BOOL)validEmailInTextField:(UITextField *)textField;
- (BOOL)validPasswordInTextField:(UITextField *)textField;

- (NSString *)platformString;

+ (NSString *)appVersion;

+ (NSString *)build;

+ (UIViewController *)topScreenController;

- (BOOL)userHasGivenNotificationsPermission;

- (void)registerApplicationNotificationSettings;

- (void)showShareSheet;
- (void)showShareSheetWithCompletion:(UIActivityViewControllerCompletionWithItemsHandler)handler;
- (void)showSharePostSheetWithPost:(VAPost*)post image:(UIImage*)image completion:(UIActivityViewControllerCompletionWithItemsHandler)handler;

- (void)setFacebookLoginMethod;
- (void)setEmailLoginMethod;
- (VALoginMethod)currentLoginMethod;

- (void)logout;

- (void)logoutForBlockedUser;

- (void)showWelcomeScreen;

- (void)openAppSettings;

@end

