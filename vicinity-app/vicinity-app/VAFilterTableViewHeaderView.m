//
//  VAFilterTableViewHeaderView.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 25.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAFilterTableViewHeaderView.h"
#import "UILabel+VACustomFont.h"

#define distanceElementsHeight 41.f

@interface VAFilterTableViewHeaderView ()
@property (weak, nonatomic) IBOutlet UIButton *minusButton;
@property (weak, nonatomic) IBOutlet UIButton *plusButton;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *radiusLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoriesLabel;
@property (weak, nonatomic) IBOutlet UIView *separatorView;

- (IBAction)minusButtonPushed:(id)sender;
- (IBAction)plusButtonPushed:(id)sender;

@property (strong, nonatomic) NSArray *distanceArray;
@property (assign, nonatomic) BOOL userHasChangeDistance;
@end

NSInteger currentIndex = 6;

@implementation VAFilterTableViewHeaderView

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        _hideCategories = NO;
    }
    return self;
}

- (void)awakeFromNib {
    self.plusButton.layer.cornerRadius = distanceElementsHeight / 2;
    self.plusButton.clipsToBounds = YES;
    self.plusButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.plusButton.layer.borderWidth = 1.f;
    
    self.minusButton.layer.cornerRadius = distanceElementsHeight / 2;
    self.minusButton.clipsToBounds = YES;
    self.minusButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.minusButton.layer.borderWidth = 1.f;
    
    self.distanceLabel.layer.cornerRadius = distanceElementsHeight / 2;
    self.distanceLabel.clipsToBounds = YES;
    self.distanceLabel.layer.borderColor = [UIColor whiteColor].CGColor;
    self.distanceLabel.layer.borderWidth = 1.f;
    
    [self.radiusLabel setLetterSpacingWithMultiplier:2.0];
    [self.categoriesLabel setLetterSpacingWithMultiplier:2.0];
    
    self.distanceArray = @[@"Few blocks", @"1/4 mile", @"1/2 mile", @"1 mile", @"2 miles", @"5 miles", @"10 miles", @"25 miles", @"50 miles"];
    
    self.userHasChangeDistance = NO;
    
    self.hideCategories = NO;
    
    [self updateText];
        
    [self addObserver:self forKeyPath:@"hideCategories" options:NSKeyValueObservingOptionNew context:nil];
}

-(void)dealloc {
    [self removeObserver:self forKeyPath:@"hideCategories"];
}

#pragma mark - Observing

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:@"hideCategories"]) {
        if ([[change valueForKey:@"new"] boolValue] == YES) {
            [self.categoriesLabel setHidden:YES];
            [self.separatorView setHidden:YES];
        } else {
            [self.categoriesLabel setHidden:NO];
            [self.separatorView setHidden:NO];
        }
    }
}

#pragma mark - Actions 

- (IBAction)minusButtonPushed:(UIButton *)sender {
    currentIndex--;
    self.userHasChangeDistance = YES;
    [self updateText];
    if (currentIndex == 0) {
        sender.enabled = NO;
    }
    self.plusButton.enabled = YES;
    [self.delegate distanceFilterChangedToDistance:[self.distanceArray objectAtIndex:currentIndex]];
}

- (IBAction)plusButtonPushed:(UIButton *)sender {
    currentIndex++;
    self.userHasChangeDistance = YES;
    [self updateText];
    if (currentIndex == self.distanceArray.count - 1) {
        sender.enabled = NO;
    }
    self.minusButton.enabled = YES;
    [self.delegate distanceFilterChangedToDistance:[self.distanceArray objectAtIndex:currentIndex]];
}

- (void)updateText {
    if (self.currentDistance && !self.userHasChangeDistance) {
        currentIndex = [self.distanceArray indexOfObject:self.currentDistance];
    }
    
    self.distanceLabel.text = [self.distanceArray objectAtIndex:currentIndex];
}

- (void)checkButtonsAvailability {
    if (currentIndex == 0) {
        self.minusButton.enabled = NO;
    } else if (currentIndex == [self.distanceArray count] - 1) {
        self.plusButton.enabled = NO;
    }
}

@end
