//
//  VAPostTableActions.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 8/28/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VAPost;
@class VAPostTableViewCell;

@protocol VAPostActionsDelegate;

@interface VAPostTableActions : NSObject

@property (weak, nonatomic) id <VAPostActionsDelegate> delegate;

- (void)tableView:(UITableView *)tableView performLikePost:(VAPost *)post inCell:(VAPostTableViewCell *)cell;
- (void)tableView:(UITableView *)tableView showLikesForPost:(VAPost *)post;
- (void)tableView:(UITableView *)tableView showCommentsForPost:(VAPost *)post;
- (void)tableView:(UITableView *)tableView editPost:(VAPost *)post;


- (void)showEditViewForPost:(VAPost *)post inTableView:(UITableView *)tableView;
- (void)showDeleteConfirmationAlertViewForPost:(VAPost *)post inTableView:(UITableView *)tableView;

@end

@protocol VAPostActionsDelegate <NSObject>

- (void)didPerformDeleteActionForPost:(VAPost *)post;

@end