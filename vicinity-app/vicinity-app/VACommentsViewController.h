//
//  VACommentsViewController.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 28.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VAPost;
@protocol VACommentDelegate;

@interface VACommentsViewController : UIViewController 
@property (weak, nonatomic) id <VACommentDelegate> delegate;

@property (strong, nonatomic) VAPost *post;      //is used when we want to see comments from My Vicinity screen
@property (strong, nonatomic) NSString *postID;  //is used when we want to load post from notification screen
@end

@protocol VACommentDelegate <NSObject>
- (void)userDidAddCommentToPost:(VAPost *)post;
- (void)userDidDeleteCommentToPost:(VAPost *)post;
- (void)userDidDeletePost:(VAPost *)post;
- (void)userDidEditPost:(VAPost *)post;
- (void)userDidEditCommentToPost:(VAPost *)post;
@end