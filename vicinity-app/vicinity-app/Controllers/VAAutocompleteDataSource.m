//
//  VAAutocompleteDataSource.m
//  vicinity-app
//
//  Created by Panda Systems on 3/31/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import "VAAutocompleteDataSource.h"
#import "VAAutoCompleteObject.h"

@interface VAAutocompleteDataSource ()

@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) NSArray *names;

@end

@implementation VAAutocompleteDataSource

- (id)initWithNames:(NSArray *)names {
    self = [super init];
    if (self) {
        _names = names;
    }
    return self;
}

#pragma mark - MLPAutoCompleteTextField data source

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
 possibleCompletionsForString:(NSString *)string
            completionHandler:(void (^)(NSArray *))handler {
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_async(queue, ^{
        NSArray *completions;
        completions = [self names];
        
        handler(completions);
    });
}

- (NSArray *)allCountryObjects {
    if (!self.items){
        NSArray *names = [self names];
        NSMutableArray *mutableNames = [NSMutableArray new];
        for(NSString *name in names){
            VAAutoCompleteObject *nameItem = [[VAAutoCompleteObject alloc] initWithName:name];
            [mutableNames addObject:nameItem];
        }
        
        [self setItems:[NSArray arrayWithArray:mutableNames]];
    }
    
    return self.items;
}

- (NSArray *)names {
    if (_names) {
        return _names;
    }
    return @[];
}

+ (NSArray *)countries {
    return @[@"United States", @"Afghanistan", @"Åland Islands", @"Albania", @"Algeria", @"American Samoa", @"Andorra", @"Angola",
             @"Anguilla", @"Antarctica", @"Antigua & Barbuda", @"Argentina", @"Armenia", @"Aruba", @"Australia", @"Austria",
             @"Azerbaijan", @"Bahamas", @"Bahrain", @"Bangladesh", @"Barbados", @"Belarus", @"Belgium", @"Belize", @"Benin",
             @"Bermuda", @"Bhutan", @"Bolivia", @"Bosnia & Herzegovina", @"Botswana", @"Bouvet Island", @"Brazil", @"British Indian Ocean Territory",
             @"British Virgin Islands", @"Brunei", @"Bulgaria", @"Burkina Faso", @"Burundi", @"Cambodia", @"Cameroon", @"Canada", @"Cape Verde",
             @"Caribbean Netherlands", @"Cayman Islands", @"Central African Republic", @"Chad", @"Chile", @"China", @"Christmas Island",
             @"Cocos (Keeling) Islands", @"Colombia", @"Comoros", @"Congo - Brazzaville", @"Congo - Kinshasa", @"Cook Islands", @"Costa Rica",
             @"Côte d’Ivoire", @"Croatia", @"Cuba", @"Curaçao", @"Cyprus", @"Czech Republic", @"Denmark", @"Djibouti", @"Dominica", @"Dominican Republic",
             @"Ecuador", @"Egypt", @"El Salvador", @"Equatorial Guinea", @"Eritrea", @"Estonia", @"Ethiopia", @"Falkland Islands", @"Faroe Islands",
             @"Fiji", @"Finland", @"France", @"French Guiana", @"French Polynesia", @"French Southern Territories", @"Gabon", @"Gambia", @"Georgia",
             @"Germany", @"Ghana", @"Gibraltar", @"Greece", @"Greenland", @"Grenada", @"Guadeloupe", @"Guam", @"Guatemala", @"Guernsey", @"Guinea",
             @"Guinea-Bissau", @"Guyana", @"Haiti", @"Heard & McDonald Islands", @"Honduras", @"Hong Kong SAR China", @"Hungary", @"Iceland", @"India",
             @"Indonesia", @"Iran", @"Iraq", @"Ireland", @"Isle of Man", @"Israel", @"Italy", @"Jamaica", @"Japan", @"Jersey", @"Jordan", @"Kazakhstan",
             @"Kenya", @"Kiribati", @"Kuwait", @"Kyrgyzstan", @"Laos", @"Latvia", @"Lebanon", @"Lesotho", @"Liberia", @"Libya", @"Liechtenstein",
             @"Lithuania", @"Luxembourg", @"Macau SAR China", @"Macedonia", @"Madagascar", @"Malawi", @"Malaysia", @"Maldives", @"Mali", @"Malta",
             @"Marshall Islands", @"Martinique", @"Mauritania", @"Mauritius", @"Mayotte", @"Mexico", @"Micronesia", @"Moldova", @"Monaco", @"Mongolia",
             @"Montenegro", @"Montserrat", @"Morocco", @"Mozambique", @"Myanmar (Burma)", @"Namibia", @"Nauru", @"Nepal", @"Netherlands",
             @"New Caledonia", @"New Zealand", @"Nicaragua", @"Niger", @"Nigeria", @"Niue", @"Norfolk Island", @"North Korea", @"Northern Mariana Islands",
             @"Norway", @"Oman", @"Pakistan", @"Palau", @"Palestinian Territories", @"Panama", @"Papua New Guinea", @"Paraguay", @"Peru", @"Philippines",
             @"Pitcairn Islands", @"Poland", @"Portugal", @"Puerto Rico", @"Qatar", @"Réunion", @"Romania", @"Russia", @"Rwanda", @"Samoa", @"San Marino",
             @"São Tomé & Príncipe", @"Saudi Arabia", @"Senegal", @"Serbia", @"Seychelles", @"Sierra Leone", @"Singapore", @"Sint Maarten", @"Slovakia",
             @"Slovenia", @"So. Georgia & So. Sandwich Isl.", @"Solomon Islands", @"Somalia", @"South Africa", @"South Korea", @"South Sudan", @"Spain",
             @"Sri Lanka", @"St. Barthélemy", @"St. Helena", @"St. Kitts & Nevis", @"St. Lucia", @"St. Martin", @"St. Pierre & Miquelon",
             @"St. Vincent & Grenadines", @"Sudan", @"Suriname", @"Svalbard & Jan Mayen", @"Swaziland", @"Sweden", @"Switzerland", @"Syria",
             @"Taiwan", @"Tajikistan", @"Tanzania", @"Thailand", @"Timor-Leste", @"Togo", @"Tokelau", @"Tonga", @"Trinidad & Tobago", @"Tunisia",
             @"Turkey", @"Turkmenistan", @"Turks & Caicos Islands", @"Tuvalu", @"U.S. Outlying Islands", @"U.S. Virgin Islands", @"Uganda",
             @"Ukraine", @"United Arab Emirates", @"United Kingdom", @"Uruguay", @"Uzbekistan", @"Vanuatu", @"Vatican City", @"Venezuela",
             @"Vietnam", @"Wallis & Futuna", @"Western Sahara", @"Yemen", @"Zambia", @"Zimbabwe"];
}

+ (NSArray *)states {
    return @[@"AL", @"AK", @"AS", @"AZ", @"AR", @"CA",
             @"CO", @"CT", @"DE", @"DC", @"FL", @"GA", @"GU",
             @"HI", @"ID", @"IL", @"IN", @"IA",
             @"KS", @"KY", @"LA", @"ME", @"MD",
             @"MA", @"MI", @"MN", @"MS", @"MO",
             @"MT", @"NE", @"NV", @"NH", @"NJ",
             @"NM", @"NY", @"NC", @"ND", @"OH",
             @"OK", @"OR", @"PA", @"PR", @"RI", @"SC",
             @"SD", @"TN", @"TX", @"UT", @"VT",
             @"VA", @"VI", @"WA", @"WV", @"WI", @"WY"];
}

@end
