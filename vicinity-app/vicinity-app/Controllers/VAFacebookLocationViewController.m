//
//  VAFacebookLocationViewController.m
//  vicinity-app
//
//  Created by Panda Systems on 10/12/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAFacebookLocationViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "VALocation.h"
#import "VAFacebookPlaceTableViewCell.h"
#import "VANotificationMessage.h"
#import "VAFiltersTool.h"
#import "NSString+VADistance.h"

@interface VAFacebookLocationViewController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
@property (nonatomic, weak) IBOutlet UIView *blurView;
@property (nonatomic, weak) IBOutlet UIView *containerView;
@property (nonatomic, weak) IBOutlet UIView *buttonsView;
@property (nonatomic, weak) IBOutlet UIView *searchView;
@property (nonatomic, weak) IBOutlet UITextField *searchTextField;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (nonatomic, weak) IBOutlet UILabel *noMatchesLabel;

@property (nonatomic, strong) IBOutletCollection(NSLayoutConstraint) NSArray *noMatchesLabelMarginConstraints;

@property (nonatomic, strong) NSMutableArray *matchedFacebookPlaces;
@property (nonatomic, strong) NSMutableArray *vicityFacebookPlaces;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;

@property (nonatomic, strong, readonly) NSArray *facebookPlaces;
@end

@implementation VAFacebookLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.matchedFacebookPlaces = [NSMutableArray array];
    
    // Container view rounded corners
    self.containerView.layer.cornerRadius = 5.0f;
    self.containerView.clipsToBounds = YES;
    
    // Buttons view shadow
    self.buttonsView.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.buttonsView.layer.shadowRadius = 2.0f;
    self.buttonsView.layer.shadowOpacity = 0.5f;
    self.buttonsView.layer.shadowOffset = CGSizeMake(0, 2);
    self.buttonsView.layer.masksToBounds = NO;
    
    // Search view shadow
    self.searchView.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.searchView.layer.shadowRadius = 1.0f;
    self.searchView.layer.shadowOpacity = 0.3f;
    self.searchView.layer.shadowOffset = CGSizeMake(0, 1);
    self.searchView.layer.masksToBounds = NO;
    
    UINib *placeCellNib = [UINib nibWithNibName:@"VAFacebookPlaceTableViewCell" bundle:[NSBundle mainBundle]];
    [self.tableView registerNib:placeCellNib forCellReuseIdentifier:@"PlaceCell"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.searchTextField.text = self.targetLocation.name;
    
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:blur];
    effectView.frame = [UIScreen mainScreen].bounds;
    [self.blurView addSubview:effectView];
    
    [self loadNearbyFacebookPlaces];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Properties

- (NSArray *)facebookPlaces {
    if (self.matchedFacebookPlaces.count) {
        return self.matchedFacebookPlaces;
    } else if (self.vicityFacebookPlaces) {
        return self.vicityFacebookPlaces;
    }
    return [NSArray array];
}

#pragma mark - UI setup

- (void)updateUI {
    if (self.matchedFacebookPlaces.count) {
        self.noMatchesLabel.text = @"";
    } else if (self.vicityFacebookPlaces.count) {
        self.noMatchesLabel.text = [NSString stringWithFormat:@"No matches for %@\nBelow are places in your Vicinity", self.searchTextField.text];
    } else {
        self.noMatchesLabel.text = @"No matches found";
    }
    for (NSLayoutConstraint *constraint in self.noMatchesLabelMarginConstraints) {
        constraint.constant = [self.noMatchesLabel.text length] ? 10 : 0;
    }
    [self.view layoutIfNeeded];
    [self.tableView reloadData];
}

#pragma mark - Actions

- (IBAction)cancelButtonTouchUp:(id)sender {
    if (self.delegate) {
        [self.delegate didSelectCancel];
    }
}

- (IBAction)saveButtonTouchUp:(id)sender {
    if (self.selectedIndexPath) {
        if (self.delegate) {
            [self.delegate didSelectPostWithFacebookPlaceId:self.facebookPlaces[self.selectedIndexPath.row][@"id"]];
        }
    }
}

- (IBAction)postWithoutCheckInButtonTouchUp:(id)sender {
    if (self.delegate) {
        [self.delegate didSelectPostWithoutFacebookPlaceId];
    }
}

- (IBAction)searchTextFieldEditingChanged:(id)sender {
    [self loadFacebookPlaces];
}

#pragma mark - Navigation

- (void)dismissController {
    if (self.presentingViewController) {
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - UITableView methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.facebookPlaces.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VAFacebookPlaceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PlaceCell" forIndexPath:indexPath];
    if (self.facebookPlaces.count > indexPath.row) {
        [cell configureWithFacebookPlace:self.facebookPlaces[indexPath.row]];
    } else {
        [cell configureWithFacebookPlace:nil];
    }
    if (self.selectedIndexPath && self.selectedIndexPath.row == indexPath.row) {
        [cell setSelectedProperties];
    } else {
        [cell setDefaultProperties];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.selectedIndexPath) {
        VAFacebookPlaceTableViewCell *oldCell = (VAFacebookPlaceTableViewCell *)[tableView cellForRowAtIndexPath:self.selectedIndexPath];
        [oldCell setDefaultProperties];
    }
    VAFacebookPlaceTableViewCell *newCell = (VAFacebookPlaceTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    [newCell setSelectedProperties];
    self.selectedIndexPath = indexPath;
}

#pragma mark - UITextFieldDelegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self loadFacebookPlaces];
    return NO;
}

#pragma mark - Helpers

- (NSNumber *)distanceFromLocation:(VACoordinate *)oldCoordinate
                        toLocation:(VACoordinate *)newCoordinate {
    CLLocation *oldLocation = [[CLLocation alloc] initWithLatitude:[oldCoordinate.latitude doubleValue] longitude:[oldCoordinate.longitude doubleValue]];
    CLLocation *newLocation = [[CLLocation alloc] initWithLatitude:[newCoordinate.latitude doubleValue] longitude:[newCoordinate.longitude doubleValue]];
    CLLocationDistance distance = [newLocation distanceFromLocation:oldLocation];
    return @(distance);
}

- (void)loadFacebookPlaces {
    [self.activityIndicatorView startAnimating];
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                  initWithGraphPath:@"search"
                                  parameters:@{
                                               @"q": self.searchTextField.text,
                                               @"type": @"place",
                                               @"center": [NSString stringWithFormat:@"%@,%@", self.targetLocation.coordinate.latitude, self.targetLocation.coordinate.longitude],
                                               @"distance": @"1000"
                                               }
                                  HTTPMethod:@"GET"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                          id result,
                                          NSError *error) {
        [self.activityIndicatorView stopAnimating];
        if (error) {
            NSLog(@"%@", error);
            [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
        } else {
            NSLog(@"%@", result);
            [self handleMatchedFacebookPlacesResponse:result];
        }
    }];
}

- (void)loadNearbyFacebookPlaces {
    [self.activityIndicatorView startAnimating];
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                  initWithGraphPath:@"search"
                                  parameters:@{
                                               @"q": @"",
                                               @"type": @"place",
                                               @"center": [NSString stringWithFormat:@"%@,%@", self.targetLocation.coordinate.latitude, self.targetLocation.coordinate.longitude],
                                               @"distance": [NSString stringWithFormat:@"%.0f", [self getVicinityRadius]],
                                               @"limit": @"100"
                                               }
                                  HTTPMethod:@"GET"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                          id result,
                                          NSError *error) {
        if (error) {
            NSLog(@"%@", error);
            [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
        } else {
            NSLog(@"%@", result);
            [self handleNearbyFacebookPlacesResponse:result];
            [self loadFacebookPlaces];
        }
    }];
}

- (void)handleMatchedFacebookPlacesResponse:(NSDictionary *)response {
    NSMutableArray *places = [response[@"data"] mutableCopy];
    
    [self handleFacebookPlaces:places];
    self.matchedFacebookPlaces = places;
    
    [self updateUI];
}

- (void)handleNearbyFacebookPlacesResponse:(NSDictionary *)response {
    NSMutableArray *places = [response[@"data"] mutableCopy];
    
    [self handleFacebookPlaces:places];
    self.vicityFacebookPlaces = places;
}

- (void)handleFacebookPlaces:(NSMutableArray *)places {
    [self addDistanceValuesToFacebookPlaces:places];
    [self sortFacebookPlacesByDistance:places];
    [self distinctFacebookPlaces:places];
}

- (void)addDistanceValuesToFacebookPlaces:(NSMutableArray *)places {
    VACoordinate *oldCordinate = self.targetLocation.coordinate;
    for (int i = 0; i < places.count; i++) {
        NSMutableDictionary *place = [places[i] mutableCopy];
        VACoordinate *newCordinate = [[VACoordinate alloc] init];
        newCordinate.latitude = place[@"location"][@"latitude"];
        newCordinate.longitude = place[@"location"][@"longitude"];
        
        NSNumber *distance = [self distanceFromLocation:oldCordinate toLocation:newCordinate];
        [place setValue:distance forKey:@"distance"];
        [places replaceObjectAtIndex:i withObject:place];
    }
}

- (void)sortFacebookPlacesByDistance:(NSMutableArray *)places {
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"distance" ascending:YES];
    [places sortUsingDescriptors:@[sortDescriptor]];
}

- (void)distinctFacebookPlaces:(NSMutableArray *)places {
    NSMutableArray *resultArray = [NSMutableArray array];
    
    NSMutableArray *names = [[places valueForKeyPath:@"@distinctUnionOfObjects.name"] mutableCopy];
    for (NSString *name in names) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name ==[c] %@", name];
        NSArray *result = [places filteredArrayUsingPredicate:predicate];
        
        NSInteger maxPriority = 0;
        NSInteger maxPriorityIndex = 0;
        for (int i = 0; i < result.count; i++) {
            NSInteger priority= 0;
            if (result[i][@"location"][@"street"] && [result[i][@"location"][@"street"] length]) {
                priority++;
            }
            if (result[i][@"location"][@"zip"] && [result[i][@"location"][@"zip"] length]) {
                priority++;
            }
            if (priority > maxPriority) {
                maxPriority = priority;
                maxPriorityIndex = i;
            }
        }
        
        [resultArray addObject:result[maxPriorityIndex]];
    }
    places = resultArray;
}

- (CGFloat)getVicinityRadius {
    NSString *milesString = [[VAFiltersTool currentFilters] loadDistance];
    if (!milesString) {
        milesString = @"10 miles";
    }
    CGFloat distance = [milesString metersFromMilesString];
    return distance;
}

@end
