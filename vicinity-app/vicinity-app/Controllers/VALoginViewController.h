//
//  ViewController.h
//  vicinity-app
//
//  Created by Panda Systems on 5/18/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VALoginViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *fbButton;

@property (nonatomic) BOOL showUserBlockedMessage;

@end

