//
//  DEMONavigationController.m
//  REFrostedViewControllerExample
//
//  Created by Roman Efimov on 9/18/13.
//  Copyright (c) 2013 Roman Efimov. All rights reserved.
//

#import "DEMONavigationController.h"
#import "UIViewController+REFrostedViewController.h"
#import "VAFilterViewController.h"
#import "VAFilterTableViewFooterView.h"
#import "VAEventsViewController.h"
#import "VALiveDealsViewController.h"

CGPoint startPoint;

@interface DEMONavigationController ()

@end

@implementation DEMONavigationController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.filtersAreShown = NO;
}

- (void)showMenu {
    if (self.containerViewController.menuState == MFSideMenuStateClosed) {
        [self.containerViewController setMenuState:MFSideMenuStateLeftMenuOpen completion:^{}];
    } else {
        [self.containerViewController setMenuState:MFSideMenuStateClosed completion:^{}];
    }
}

- (UIColor *)colorFromHex:(NSString *)hex {
    unsigned int c;
    if ([hex characterAtIndex:0] == '#') {
        [[NSScanner scannerWithString:[hex substringFromIndex:1]] scanHexInt:&c];
    } else {
        [[NSScanner scannerWithString:hex] scanHexInt:&c];
    }
    return [UIColor colorWithRed:((c & 0xff0000) >> 16)/255.0
                           green:((c & 0xff00) >> 8)/255.0
                            blue:(c & 0xff)/255.0 alpha:1.0];
}

- (void)setDefaultNavigationBar {
    [self.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    self.navigationBar.shadowImage = nil;
    self.navigationBar.translucent = NO;
    self.navigationBar.barTintColor = [[VADesignTool defaultDesign] vicinityBlue];
}

- (void)setTitle:(NSString *)title {
    CGFloat sideIndent = 40.f;
    CGFloat maxLabelWidth = CGRectGetWidth(self.view.bounds) - sideIndent * 2;
    CGFloat startFontSize;
    
    if ([self.topViewController isKindOfClass:[VAEventsViewController class]] || [self.topViewController isKindOfClass:[VALiveDealsViewController class]]) {
        if ([self.topViewController isKindOfClass:[VAEventsViewController class]] &&
            (((VAEventsViewController *)self.topViewController).isCheckInMode || ((VAEventsViewController *)self.topViewController).isHashTagMode)) {
            startFontSize = 18.f;
        } else {
            startFontSize = 31.f;
        }
    } else {
        startFontSize = 18.f;
    }
    
    CGFloat minimumFontSize = 17.f;
    
    UIFont *baseFont = [[VADesignTool defaultDesign] vicinityNavigationFontOfSize:startFontSize];
    
    UILabel *sizeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGFLOAT_MAX, CGFLOAT_MAX)];
    [sizeLabel setFont:baseFont];
    [sizeLabel setText:title];
    [sizeLabel sizeToFit];
    
    CGFloat labelWidth = CGRectGetWidth([sizeLabel frame]);
    
    CGFloat optimumFontSize = startFontSize;
    
    while (labelWidth > maxLabelWidth) {
        optimumFontSize -= 1;
        baseFont = [[VADesignTool defaultDesign] vicinityNavigationFontOfSize:optimumFontSize];
        
        if (optimumFontSize == minimumFontSize) {
            break;
        }
        
        [sizeLabel setFont:baseFont];
        [sizeLabel setText:title];
        [sizeLabel sizeToFit];
        labelWidth = CGRectGetWidth([sizeLabel frame]);
        
    }
    
    [self.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
                              NSFontAttributeName: [[VADesignTool defaultDesign] vicinityNavigationFontOfSize:optimumFontSize]}];
    
    CGFloat verticalPosition = 5 - (31 - optimumFontSize) / 2;
    [self.navigationBar setTitleVerticalPositionAdjustment:verticalPosition forBarMetrics:UIBarMetricsDefault];
    
    self.topViewController.navigationItem.title = title;
}



@end
