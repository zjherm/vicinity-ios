//
//  VAAutocompleteDataSource.h
//  vicinity-app
//
//  Created by Panda Systems on 3/31/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MLPAutoCompleteTextFieldDataSource.h"

@interface VAAutocompleteDataSource : NSObject <MLPAutoCompleteTextFieldDataSource>

- (id)initWithNames:(NSArray *)names;
+ (NSArray *)countries;
+ (NSArray *)states;

@end
