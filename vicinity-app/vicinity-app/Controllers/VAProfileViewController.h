//
//  VAProfileViewController.h
//  vicinity-app
//
//  Created by Panda Systems on 5/19/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VAProfileProtocol;

@interface VAProfileViewController : UIViewController <UIScrollViewDelegate>
@property (weak, nonatomic) id <VAProfileProtocol> delegate;

@property (assign, nonatomic) BOOL isUsersAccountMode;                 //mode for displaying another users profiles (default is NO)
@property (assign, nonatomic) BOOL isCreateAliasMode;                 //mode for creating alias on first sign up (default is NO)
@property (strong, nonatomic) NSString *searchAlias;
@property (strong, nonatomic) NSString *userId;
@end

@protocol VAProfileProtocol <NSObject>
- (void)userDidGoBackToLoginScreen;
@end
