//
//  DEMONavigationController.h
//  REFrostedViewControllerExample
//
//  Created by Roman Efimov on 9/18/13.
//  Copyright (c) 2013 Roman Efimov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MFSideMenu.h>

@interface DEMONavigationController : UINavigationController
@property (assign, nonatomic) BOOL filtersAreShown;
@property (strong, readwrite, nonatomic) MFSideMenuContainerViewController *containerViewController;

- (void)showMenu;

- (void)setDefaultNavigationBar;

@end
