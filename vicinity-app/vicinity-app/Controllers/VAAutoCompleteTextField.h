//
//  VAAutoCompleteTextField.h
//  vicinity-app
//
//  Created by Panda Systems on 3/31/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import <MLPAutoCompleteTextField/MLPAutoCompleteTextField.h>

@interface VAAutoCompleteTextField : MLPAutoCompleteTextField

+ (CGRect)autoCompleteTableViewFrameForTextField:(MLPAutoCompleteTextField *)textField
                                 forNumberOfRows:(NSInteger)numberOfRows;

+ (CGFloat)autoCompleteTableHeightForTextField:(MLPAutoCompleteTextField *)textField
                              withNumberOfRows:(NSInteger)numberOfRows;

+ (CGRect)autoCompleteTableViewFrameForTextField:(MLPAutoCompleteTextField *)textField;
+ (UITableView *)newAutoCompleteTableViewForTextField:(MLPAutoCompleteTextField *)textField;

@end
