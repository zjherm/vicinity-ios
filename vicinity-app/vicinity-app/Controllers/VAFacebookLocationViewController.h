//
//  VAFacebookLocationViewController.h
//  vicinity-app
//
//  Created by Panda Systems on 10/12/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VALocation;

@protocol FacebookLocationDelegate <NSObject>

- (void)didSelectPostWithFacebookPlaceId:(NSString *)placeId;
- (void)didSelectPostWithoutFacebookPlaceId;
- (void)didSelectCancel;

@end

@interface VAFacebookLocationViewController : UIViewController

@property (nonatomic, weak) id<FacebookLocationDelegate> delegate;
@property (nonatomic, strong) VALocation *targetLocation;

@end
