//
//  VAAutoCompleteObject.m
//  vicinity-app
//
//  Created by Panda Systems on 3/31/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import "VAAutoCompleteObject.h"

@interface VAAutoCompleteObject ()

@property (nonatomic, strong) NSString *name;

@end

@implementation VAAutoCompleteObject

- (id)initWithName:(NSString *)name {
    self = [super init];
    if (self) {
        _name = name;
    }
    return self;
}

#pragma mark - MLPAutoCompletionObject protocol

- (NSString *)autocompleteString {
    return self.name;
}

@end
