//
//  VAAutoCompleteTextField.m
//  vicinity-app
//
//  Created by Panda Systems on 3/31/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import "VAAutoCompleteTextField.h"

@implementation VAAutoCompleteTextField

+ (CGRect)autoCompleteTableViewFrameForTextField:(MLPAutoCompleteTextField *)textField
{
    CGRect frame = textField.frame;
    frame.origin.y += textField.frame.size.height;
    frame.origin.x += textField.autoCompleteTableOriginOffset.width;
    frame.origin.y += textField.autoCompleteTableOriginOffset.height;
    frame = CGRectInset(frame, 1, 0);
    
    return frame;
}

+ (CGRect)autoCompleteTableViewFrameForTextField:(MLPAutoCompleteTextField *)textField
                                 forNumberOfRows:(NSInteger)numberOfRows
{
    CGRect newTableViewFrame = [[self class] autoCompleteTableViewFrameForTextField:textField];
    
    CGFloat height = [[self class] autoCompleteTableHeightForTextField:textField
                                                      withNumberOfRows:numberOfRows];
    newTableViewFrame.size.height = height;
    newTableViewFrame.origin.x = newTableViewFrame.origin.x - 15.0f;
    newTableViewFrame.size.width = newTableViewFrame.size.width + 16.0f;
    
    if(!textField.autoCompleteTableAppearsAsKeyboardAccessory){
        newTableViewFrame.size.height += textField.autoCompleteTableView.contentInset.top;
    }
    
    return newTableViewFrame;
}

+ (CGFloat)autoCompleteTableHeightForTextField:(MLPAutoCompleteTextField *)textField
                              withNumberOfRows:(NSInteger)numberOfRows
{
    CGFloat maximumHeightMultiplier = textField.maximumNumberOfAutoCompleteRows;
    CGFloat heightMultiplier;
    if(numberOfRows >= textField.maximumNumberOfAutoCompleteRows){
        heightMultiplier = maximumHeightMultiplier;
    } else {
        heightMultiplier = numberOfRows;
    }
    
    CGFloat height = textField.autoCompleteRowHeight * heightMultiplier;
    return height;
}

+ (UITableView *)newAutoCompleteTableViewForTextField:(MLPAutoCompleteTextField *)textField
{
    CGRect dropDownTableFrame = [[self class] autoCompleteTableViewFrameForTextField:textField];
    
    UITableView *newTableView = [[UITableView alloc] initWithFrame:dropDownTableFrame
                                                             style:UITableViewStylePlain];
    [newTableView setDelegate:textField];
    [newTableView setDataSource:textField];
    [newTableView setScrollEnabled:YES];
    [newTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    newTableView.layer.shadowColor = [UIColor blackColor].CGColor;
    newTableView.layer.shadowOpacity = 0.24f;
    newTableView.layer.shadowOffset = CGSizeMake(0, 1.0f);
    newTableView.layer.shadowRadius = 1.0f;
    newTableView.layer.masksToBounds = YES;
    
    return newTableView;
}

@end
