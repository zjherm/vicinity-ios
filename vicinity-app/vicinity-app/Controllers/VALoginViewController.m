//
//  ViewController.m
//  vicinity-app
//
//  Created by Panda Systems on 5/18/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VALoginViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "VAEventsViewController.h"
#import "REFrostedViewController.h"
#import "DEMONavigationController.h"
#import "VANotificationTool.h"
#import "VAUserTool.h"
#import "VANotificationMessage.h"
#import "VAUserApiManager.h"
#import "VAUserDefaultsHelper.h"
#import "VALoginResponse.h"
#import "VAUser.h"
#import "VAControlTool.h"
#import "VAProfileViewController.h"
#import "VAUserDefaultsHelper.h"
#import <MFSideMenu.h>
#import "VAMenuTableViewController.h"
#import <Google/Analytics.h>
#import "UILabel+VACustomFont.h"
#import "UIButton+VACustomFont.h"
#import "VASignUpViewController.h"
#import <LGAlertView.h>
#import "VAScrollView.h"
#import "VAWelcomeViewController.h"
#import <LaunchKit/LaunchKit.h>

#import "VAConfirmEmailViewController.h"
#import "VAEnterUsernameViewController.h"

static NSString *const kUserBlockedMessage = @"Your account is currently locked. Check your e-mail with instructions on how to proceed.";

@interface VALoginViewController () <VAProfileProtocol>
@property (strong, nonatomic) UIActivityIndicatorView *facebookIndicator;
@property (strong, nonatomic) UIButton *pushedButton;
@property (strong, nonatomic) UITextField *activeField;

@property (weak, nonatomic) IBOutlet UIButton *signUpButton;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordButton;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@property (weak, nonatomic) IBOutlet UILabel *noAccountLabel;
@property (weak, nonatomic) IBOutlet UILabel *orLabel;

@property (weak, nonatomic) IBOutlet UITextField *loginField;
@property (weak, nonatomic) IBOutlet UIView *loginFieldView;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UIView *passwordFieldView;
@property (weak, nonatomic) IBOutlet UIView *signUpView;

@property (weak, nonatomic) IBOutlet UIImageView *backgroundView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIView *loaderView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loginIndicator;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topMarginToLogoViewConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoAndFieldsVSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loginButtonAndLabelVSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *facebookButtonAndLabelVSpaceConstraint;



- (IBAction)loginWithFacebook:(UIButton *)button;
- (IBAction)loginWithFields:(id)sender;
@end

#define kIncorrectLoginData @(100005)

@implementation VALoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.fbButton.layer.cornerRadius = 5.f;
    self.fbButton.clipsToBounds = YES;
    self.fbButton.contentEdgeInsets = UIEdgeInsetsMake(3.0, 0.0, 0.0, 0.0);
    
    self.backgroundView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapBackground = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.backgroundView addGestureRecognizer:tapBackground];
    
    
    [self setUpViewsAlpha];
    [self setUpVerticalSpacingConstraints];
    [self setUpFontSizes];
    [self setUpPlaceholders];
    [self disableLoginButton];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [UIApplication sharedApplication].keyWindow.backgroundColor = [UIColor colorWithRed:67.0/255.0 green:207.0/255.0 blue:206.0/255.0 alpha:1.0f];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToEventsViewController) name:kDidChangeLocationStatus object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideNotificationsMessage) name:kUserDidRegisterForPushNotifications object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestLocationPermissions) name:kUserDidCreateAlias object:nil];

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Login screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.showUserBlockedMessage) {
        [self showAlertViewWithMessage:kUserBlockedMessage];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    if ([[VANotificationMessage currentMessageController] notificationMessageIsShown]) {
        [[VANotificationMessage currentMessageController] hideCurrentMessage];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Actions

- (IBAction)loginWithFacebook:(UIButton *)button {
    [[VAControlTool defaultControl] setFacebookLoginMethod];

    if ([[VANotificationMessage currentMessageController] notificationMessageIsShown]) {
        [[VANotificationMessage currentMessageController] hideCurrentMessage];
    }
    [self showFacebookLoaderFor:button];
    [self loginWithFB];
}

- (IBAction)loginWithFields:(id)sender {
    [self loginWithFields];
}

- (void)loginWithFields {
    if ([self.loginField.text isEqualToString:@""]) {
        [self showAlertViewWithMessage:NSLocalizedString(@"error.enter.username", nil)];
    } else if ([self.passwordField.text isEqualToString:@""]) {
        [self showAlertViewWithMessage:NSLocalizedString(@"error.enter.password", nil)];
    } else {
        [[VAControlTool defaultControl] setEmailLoginMethod];
        [self loginWithLogin:self.loginField.text password:self.passwordField.text];
    }
}

- (IBAction)resetPasswordButtonPushed:(UIButton *)sender {
    [self.activeField resignFirstResponder];
    
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 258.f, 30.f)];
    textField.borderStyle = UITextBorderStyleLine;
    textField.spellCheckingType = UITextSpellCheckingTypeNo;
    textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    [textField setFont:[[VADesignTool defaultDesign] vicinityRegularFontOfSize:14.f]];
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithViewStyleWithTitle:@"Forgot Password"
                                                                     message:@"Enter your username or email below to reset your password"
                                                                        view:textField
                                                                buttonTitles:@[@"Submit"]
                                                           cancelButtonTitle:nil
                                                      destructiveButtonTitle:@"Cancel"
                                                               actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                   if ([title isEqualToString:@"Submit"]) {
                                                                       [self sendResetPasswordRequestForUsernameOrEmail:textField.text];
                                                                   }
                                                               }
                                                               cancelHandler:nil
                                                          destructiveHandler:nil];
    alertView.cancelOnTouch = NO;
    alertView.destructiveButtonTitleColor = [UIColor blackColor];
    alertView.destructiveButtonTitleColorHighlighted = [UIColor blackColor];
    alertView.destructiveButtonBackgroundColorHighlighted = [UIColor clearColor];
    alertView.buttonsFont = [UIFont boldSystemFontOfSize:18.f];
    [alertView showAnimated:YES completionHandler:nil];
    [textField becomeFirstResponder];
    self.activeField = textField;
}


/*- (IBAction)signUpButtonPushed:(UIButton *)sender {
    [[VAControlTool defaultControl] setEmailLoginMethod];
    [self gotoSignUpController];
}
*/
#pragma mark - Keyboard actions

- (void)willKeyboardShow:(NSNotification*)notification {
    NSDictionary* info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;

    
    CGPoint originInSuperview = [self.scrollView convertPoint:CGPointZero fromView:self.loginButton];
    CGFloat yOffset = (originInSuperview.y + self.loginButton.frame.size.height + 15) - (self.scrollView.frame.size.height - kbSize.height);
    if (yOffset > 0) {
        self.scrollView.contentOffset = CGPointMake(0, yOffset);
    }
}

- (void)willKeyboardHide:(NSNotification*)notification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    self.scrollView.contentOffset = CGPointZero;
}

#pragma mark - Methods

- (void)loginWithFB {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    NSArray *permissions = @[@"public_profile", @"user_location", @"email", @"user_friends"];
    [login logInWithReadPermissions:permissions handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            [self showErrorMessage];
        } else if (result.isCancelled) {
            [self showErrorMessage];
        } else {
            FBSDKAccessToken *accessToken = [FBSDKAccessToken currentAccessToken];
            NSString *tokenString = [accessToken tokenString];
            [[VAUserApiManager alloc] getCurrentUserWithFacebookToken:tokenString
                                                        andCompletion:^(NSError *error, VAModel *model)
            {
                if (model) {
                    VALoginResponse *response = (VALoginResponse *)model;
                    VAUser *user = response.loggedUser;
                    if (user.status == VAUserStatusNormal || user.status == VAUserStatusLocked) {
                        [VAUserDefaultsHelper saveModel:user forKey:VASaveKeys_CurrentUser];
                        [VAUserDefaultsHelper setAuthToken:response.sessionId];
                    }
                    NSLog(@"%@", response.sessionId);
                    NSLog(@"%@", [accessToken tokenString]);
                    
                    if ((user.status == VAUserStatusNormal || user.status == VAUserStatusLocked) && (!user.nickname || [user.nickname isEqualToString:@""])) {
                        [self showProfileSetup];
                    } else if ((user.status == VAUserStatusNormal || user.status == VAUserStatusLocked) && ![user.isEmailConfirmed boolValue]) {
                        if ([user.pendingEmail length]) {
                            [self showConfirmEmailAfterUpdateViewController];
                        } else {
                            [self showConfirmEmailAfterSignUpViewControllerWithUser:user];
                        }
                    } else {
                        if (user.status == VAUserStatusBlocked) {
                            [self showAlertViewWithMessage:kUserBlockedMessage];
                            [self restoreButton:self.pushedButton];
                        } else {
                            [self requestLocationPermissions];
                        }
                    }
                } else {
                    //error or response status = error
                    [self restoreButton:self.pushedButton];
                    [self showAlertViewWithMessage:NSLocalizedString(@"connection.facebook.failed" , nil)];
                }
            }];
        }
    }];
}

- (void)loginWithLogin:(NSString *)login
              password:(NSString *)password {
    
    [self showLoginLoader];
    
    [[VAUserApiManager alloc] getCurrentUserWithLogin:login
                                             passwoed:password
                                        andCompletion:^(NSError *error, VAModel *model)
    {
        if (model) {
            VALoginResponse *response = (VALoginResponse *)model;
            VAUser *user = response.loggedUser;
            if (user.status == VAUserStatusNormal || user.status == VAUserStatusLocked) {
                [VAUserDefaultsHelper saveModel:user forKey:VASaveKeys_CurrentUser];
                [VAUserDefaultsHelper setAuthToken:response.sessionId];
            }
            
            if ((user.status == VAUserStatusNormal || user.status == VAUserStatusLocked) && ![user.isEmailConfirmed boolValue]) {
                if ([user.pendingEmail length]) {
                    [self showConfirmEmailAfterUpdateViewController];
                } else {
                    [self showConfirmEmailAfterSignUpViewControllerWithUser:user];
                }
            } else {
                if (user.status == VAUserStatusBlocked) {
                    [self showAlertViewWithMessage:kUserBlockedMessage];
                    [self hideLoginLoader];
                } else {
                    [self requestLocationPermissions];
                }
            }
        } else {
            [self hideLoginLoader];
            if ([[error localizedDescription] isEqual:kIncorrectLoginData]) {
                [self showAlertViewWithMessage:NSLocalizedString(@"error.incorrect.data", nil)];
            } else {
                [self showAlertViewWithMessage:NSLocalizedString(@"connection.error", nil)];
            }
        }
    }];
}

- (void)goToEventsViewController {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Vicinity" bundle:nil];
    
    VAWelcomeViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"welcomeViewController"];
    [[UIApplication sharedApplication] keyWindow].rootViewController = vc;
    [[[UIApplication sharedApplication] keyWindow] makeKeyAndVisible];
}

- (void)showProfileSetup {
    //To profile view Controller
    /*VAProfileViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"profileViewController"];
*/
    
    //To EnterUserNameViewController
    VAEnterUsernameViewController *vc = [[UIStoryboard storyboardWithName:@"LoginFlow" bundle:nil] instantiateViewControllerWithIdentifier:@"VAEnterUsernameViewController"];
    
    vc.delegate = self;
    DEMONavigationController *nav = [[DEMONavigationController alloc] initWithRootViewController:vc];
    nav.navigationBar.translucent = NO;
    nav.navigationBar.tintColor = [UIColor whiteColor];
    nav.navigationBar.barTintColor = [[VADesignTool defaultDesign] vicinityBlue];
    nav.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:nav animated:YES completion:nil];
}

- (void)requestLocationPermissions {
    [self hideFacebookLoader];
    [self hideAllButtons];
    [[VALocationTool sharedInstance] requestInUseAuthorization];
}

- (void)hideAllButtons {
    for (id subview in self.view.subviews) {
        if ([subview isKindOfClass:[UIButton class]]) {
            [self hideButton:(UIButton *)subview];
        }
    }
}

- (void)hideButton:(UIButton *)button {
    for (id subview in self.view.subviews) {
        if ([subview isKindOfClass:[UIImageView class]]) {
            UIImageView *imageView = (UIImageView *)subview;
            if (CGRectContainsRect(button.bounds, imageView.bounds)) {
                [self hideImageView:imageView];
                break;
            }
        }
    }
    [UIView animateWithDuration:0.3f animations:^{
        [button setHidden:YES];
    }];
}

- (void)hideImageView:(UIImageView *)buttonIcon {
    [UIView animateWithDuration:0.3f animations:^{
        [buttonIcon setHidden:YES];
    }];
}

- (void)restoreButton:(UIButton *)button {
    [self hideFacebookLoader];
    [UIView animateWithDuration:0.3f animations:^{
        button.alpha = 1.f;
    }];
}

- (void)showErrorMessage {
    [self restoreButton:self.pushedButton];
    [self showAlertViewWithMessage:NSLocalizedString(@"login.error", nil)];
}

- (void)hideNotificationsMessage {
    if ([[VANotificationMessage currentMessageController] notificationMessageIsShown]) {
        [[VANotificationMessage currentMessageController] hideCurrentMessage];
    }
}

- (void)hideKeyboard {
    [self.activeField resignFirstResponder];
}

- (void)goToVerificationController {
    [self.navigationController setNavigationBarHidden:NO];
    VAConfirmEmailViewController *vc = [[UIStoryboard storyboardWithName:@"Vicinity" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"confirmEmailViewController"];
    vc.isEmailUpdated = YES;
    DEMONavigationController *nav = [[DEMONavigationController alloc] initWithRootViewController:vc];
    nav.navigationBar.translucent = NO;
    nav.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:nav animated:YES completion:nil];}

- (void)sendResetPasswordRequestForUsernameOrEmail:(NSString *)usernameOrEmail {
    [self showLoginLoader];
    [[VAUserApiManager new] getResetPasswordForUsernameOrEmail:usernameOrEmail
                                                withCompletion:^(NSError *error, VAModel *model)
    {
        [self hideLoginLoader];
        if (model) {
            [self showAlertViewWithMessage:NSLocalizedString(@"check.email.password", nil)];
        } else {
            [self showAlertViewWithMessage:NSLocalizedString(@"error.incorrect.usernameOrEmail", nil)];
        }
    }];
}

#pragma mark - setup

- (void)setUpPlaceholders {
    [self setUpPlaceholderForTextField:self.loginField];
    [self setUpPlaceholderForTextField:self.passwordField];
    
}

- (void)setUpPlaceholderForTextField:(UITextField *)textField{
    NSAttributedString *placeholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{ NSForegroundColorAttributeName : [[UIColor whiteColor] colorWithAlphaComponent:1.0f]}];
    textField.attributedPlaceholder = placeholder;
}

- (void)setUpViewsAlpha {
    self.loginFieldView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.27];
    self.passwordFieldView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.27];
    self.signUpView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.38];
    
}

- (void)setUpVerticalSpacingConstraints {
    CGFloat mainViewHeight = self.view.frame.size.height;
    self.topMarginToLogoViewConstraint.constant = 0.0863f * mainViewHeight;
    self.logoAndFieldsVSpaceConstraint.constant = 0.1f * mainViewHeight;
    self.loginButtonAndLabelVSpaceConstraint.constant = 0.0537f * mainViewHeight;
    self.facebookButtonAndLabelVSpaceConstraint.constant = 0.047f * mainViewHeight;
}

- (void)setUpFontSizes {
    if (self.view.frame.size.height < 660) {
        //iphone 5 and smaller
        CGFloat fontSizeDiff = 2.0f;
        if (self.view.frame.size.height < 560) {
            //iphone 4 and smaller
            fontSizeDiff = 4.0f;
        }
        self.loginButton.titleLabel.font = [self.loginButton.titleLabel.font fontWithSize:(self.loginButton.titleLabel.font.pointSize - fontSizeDiff)];
        self.loginField.font = [self.loginField.font fontWithSize:(self.loginField.font.pointSize - fontSizeDiff)];
        self.passwordField.font = [self.passwordField.font fontWithSize:(self.passwordField.font.pointSize - fontSizeDiff)];
        self.signUpButton.titleLabel.font = [self.signUpButton.titleLabel.font fontWithSize:(self.signUpButton.titleLabel.font.pointSize - fontSizeDiff)];
        self.noAccountLabel.font = [self.noAccountLabel.font fontWithSize:(self.noAccountLabel.font.pointSize - fontSizeDiff)];
        self.fbButton.titleLabel.font = [self.fbButton.titleLabel.font fontWithSize:(self.fbButton.titleLabel.font.pointSize - fontSizeDiff)];
        self.orLabel.font = [self.orLabel.font fontWithSize:(self.orLabel.font.pointSize - fontSizeDiff)];
    }
}


#pragma mark - Loader

- (void)showFacebookLoaderFor:(UIButton *)button {
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = CGPointMake(CGRectGetMidX(button.frame), CGRectGetMidY(button.frame));
    self.facebookIndicator = activityIndicator;
    
    self.pushedButton = button;
    [UIView animateWithDuration:0.3f animations:^{
        button.alpha = 0.4f;
    }];
    
    [self.view addSubview:activityIndicator];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [activityIndicator startAnimating];
    });
}

- (void)hideFacebookLoader {
    [self.facebookIndicator stopAnimating];
}

- (void)showLoginLoader {
    [UIView animateWithDuration:0.3f animations:^{
        self.loaderView.alpha = 0.5f;
    }];
    [self.loginIndicator startAnimating];
}

- (void)hideLoginLoader {
    [UIView animateWithDuration:0.3f animations:^{
        self.loaderView.alpha = 0.0f;
    }];
    [self.loginIndicator stopAnimating];
}

#pragma mark - VAProfileProtocol

- (void)userDidGoBackToLoginScreen {
    [self restoreButton:self.pushedButton];
    [self hideLoginLoader];
}

#pragma mark - VAVerificationDelegate

- (void)userDidGoBackToLoginScreenFromVerification {
    [self restoreButton:self.pushedButton];
    [self hideLoginLoader];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.activeField = textField;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(UITextFieldTextDidChange:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UITextFieldTextDidChangeNotification
                                                  object:textField];
    if ([self allFieldsAreValid]) {
        [self enableLoginButton];
    } else {
        [self disableLoginButton];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField isEqual:self.loginField]) {
        BOOL change = [[VAControlTool defaultControl] setEmailMaskForTextField:textField InRange:range replacementString:string];
        return change ;
    } else if ([textField isEqual:self.passwordField]) {
        BOOL sould = [[VAControlTool defaultControl] setPasswordMaskForTextField:textField InRange:range replacementString:string];
        return sould;
    } else {
        return YES;
    }
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    [self disableLoginButton];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.loginField]) {
        [self.passwordField becomeFirstResponder];
    } else if ([textField isEqual:self.passwordField]) {
        [self hideKeyboard];
        [self loginWithFields];
    }
    
    return YES;
}
- (void) UITextFieldTextDidChange:(NSNotification*)notification
{
    if ([self allFieldsAreValid]) {
        [self enableLoginButton];
    } else {
        [self disableLoginButton];
    }
}

#pragma mark - enable/disable login button
- (void)disableLoginButton {
    self.loginButton.enabled = NO;
    self.loginButton.backgroundColor = [self.loginButton.backgroundColor colorWithAlphaComponent:0.5];
}

- (void)enableLoginButton {
    self.loginButton.enabled = YES;
    self.loginButton.backgroundColor = [self.loginButton.backgroundColor colorWithAlphaComponent:1.0];
}

- (BOOL)allFieldsAreValid {
    return (self.loginField.text.length != 0) && (self.passwordField.text.length != 0);
}


#pragma mark - Alerts

- (void)showAlertViewWithMessage:(NSString *)message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Helpers

- (void)showConfirmEmailAfterUpdateViewController {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    VAProfileViewController *profileVC = [storyboard instantiateViewControllerWithIdentifier:@"profileViewController"];
    VAConfirmEmailViewController *confirmEmailVC = [[UIStoryboard storyboardWithName:@"Vicinity" bundle:nil] instantiateViewControllerWithIdentifier:@"confirmEmailViewController"];
    confirmEmailVC.isEmailUpdated = YES;
    confirmEmailVC.delegate = (id<VAConfirmEmailDelegate>)profileVC;
    DEMONavigationController *nav = (DEMONavigationController*)[storyboard instantiateViewControllerWithIdentifier:@"MyVicinityNavigationController"];
    [nav setViewControllers:@[profileVC, confirmEmailVC] animated:NO];
    VAMenuTableViewController *leftMenuController = [storyboard instantiateViewControllerWithIdentifier:@"VAMenuTableViewController"];;
    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:nav
                                                    leftMenuViewController:leftMenuController
                                                    rightMenuViewController:nil];
    container.leftMenuWidth = 125.f;
    container.menuAnimationMaxDuration = container.menuAnimationDefaultDuration;
    leftMenuController.containerViewController = container;
    nav.containerViewController = container;
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    window.rootViewController = container;
    [window makeKeyAndVisible];
}

- (void)showConfirmEmailAfterSignUpViewControllerWithUser:(VAUser *)user {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    VASignUpViewController *signUpVC = [storyboard instantiateViewControllerWithIdentifier:@"VASignUpViewController"];
    signUpVC.pendingUser = user;
    VAConfirmEmailViewController *confirmEmailVC = [[UIStoryboard storyboardWithName:@"Vicinity" bundle:nil] instantiateViewControllerWithIdentifier:@"confirmEmailViewController"];
    confirmEmailVC.isEmailUpdated = NO;
    confirmEmailVC.delegate = (id<VAConfirmEmailDelegate>)signUpVC;
    DEMONavigationController *nav = (DEMONavigationController*)[storyboard instantiateViewControllerWithIdentifier:@"MyVicinityNavigationController"];
    [nav setViewControllers:@[signUpVC, confirmEmailVC] animated:NO];
    VAMenuTableViewController *leftMenuController = [storyboard instantiateViewControllerWithIdentifier:@"VAMenuTableViewController"];;
    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:nav
                                                    leftMenuViewController:leftMenuController
                                                    rightMenuViewController:nil];
    container.leftMenuWidth = 125.f;
    container.menuAnimationMaxDuration = container.menuAnimationDefaultDuration;
    leftMenuController.containerViewController = container;
    nav.containerViewController = container;
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    window.rootViewController = container;
    [window makeKeyAndVisible];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [self hideKeyboard];
}

@end
