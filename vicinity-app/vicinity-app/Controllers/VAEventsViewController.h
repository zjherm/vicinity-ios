//
//  VAEventsViewController.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 18.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VANewPostView.h"
#import "VACommentsViewController.h"

@interface VAEventsViewController : UIViewController <VACommentDelegate>
@property (assign, nonatomic) BOOL isHashTagMode;
@property (strong, nonatomic) NSString *hashtagString;
@property (assign, nonatomic) BOOL isCheckInMode;
@property (strong, nonatomic) NSString *googlePlaceID;
@property (strong, nonatomic) NSString *checkInName;
@end
