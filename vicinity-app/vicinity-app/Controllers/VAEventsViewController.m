//
//  VAEventsViewController.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 18.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAEventsViewController.h"
#import "DEMONavigationController.h"
#import "VANotificationMessage.h"
#import "VALikeListTableViewController.h"
#import "VACommentsViewController.h"
#import "UILabel+VACustomFont.h"
#import "VANewPostView.h"
#import "VAPhotoTool.h"
#import "VAControlTool.h"
#import "VAChooseLocationViewController.h"
#import "VALocation.h"
#import "VAUserApiManager.h"
#import "VAFiltersTool.h"
#import "VAPostResponse.h"
#import "VAPost.h"
#import "VAComment.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "VALoaderView.h"
#import "NSDate+VAString.h"
#import "VANotificationTool.h"
#import "VAFilterViewController.h"
#import "VAUserDefaultsHelper.h"
#import "NSNumber+VAMilesDistance.h"
#import "VAPostTableViewCell.h"
#import "VANewPostResponse.h"
#import "VAProfileViewController.h"
#import "UITableViewCell+VAParentCell.h"
#import "UIBarButtonItem+Badge.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "UIImage+VAFixOrientation.h"
#import "VAEmptyVicinityView.h"
#import <Google/Analytics.h>
#import "VAPostTableActions.h"
#import "NSString+VADistance.h"
#import <FBSDKShareKit.h>
#import <FBSDKLoginKit.h>
#import <FBSDKCoreKit.h>
#import "VAFacebookLocationViewController.h"
#import "VAReportViewController.h"
#import "VAPostActionsView.h"
#import "VAPostActionsService.h"
#import "VAControlTool.h"
#import "VAPostsTableViewCell.h"
#import <LaunchKit/LaunchKit.h>
#import "VAEndFeedView.h"

#import "VAPostStore.h"

#define kLocationTag 999
#define kPhotoTag 1000

#define postLimit 20

@interface VAEventsViewController () <UITableViewDelegate, UITableViewDataSource, VANewPostActionsProtocol, VAPhotoToolDelegate, VALocationDelegate, UIActionSheetDelegate, VAFilterDelegate, VAVicinityEmptyViewProtocol, VAPostActionsDelegate, FacebookLocationDelegate, VAPostDelegate, VAReportSendingDelegate, VAPostActionsDelegate, VAEndFeedViewProtocol>
@property (assign, nonatomic) BOOL isPostWriting;
@property (assign, nonatomic) BOOL postsHaveBeenLoaded;
@property (assign, nonatomic) BOOL isRefreshing;                 //refreshing all posts after pull to refresh
@property (assign, nonatomic) BOOL isLoadingNewPostBatch;        //loading another batch of posts
@property (assign, nonatomic) BOOL firstBatchHasBeenLoaded;
@property (assign, nonatomic) BOOL allPostsHasBeenLoaded;
@property (strong, nonatomic) VALocation *currentLocation;
@property (strong, nonatomic) NSArray *postsArray;
@property (strong, nonatomic) VALoaderView *loader;              //activity indicator for data loading
@property (assign, nonatomic) CGFloat lastContentOffset;
@property (assign, nonatomic) BOOL addPostButtonVisible;

@property (strong, nonatomic) VALoaderView *footerLoader;        //loader in footer for loading more posts
@property (strong, nonatomic) VALoaderView *refresh;             //loader for refresh control
@property (strong, nonatomic) UIImage *currentImage;
@property (strong, nonatomic) NSIndexPath *indexPathOfSelectedPost;
@property (strong, nonatomic) UIView *navigationAlphaView;
@property (strong, nonatomic) UIView *navigationBlurView;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *addPostButton;
@property (weak, nonatomic) IBOutlet UIImageView *addPostButtonBackground;
@property (weak, nonatomic) IBOutlet VANewPostView *userPostView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userPostContainerHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addPostButtonBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addPostButtonBottomBackgroundConstraint;
@property (weak, nonatomic) IBOutlet UIView *blurView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menuButton;

// Pop up view
@property (nonatomic, strong) UIView *popUpView;
@property (nonatomic, strong) VAPostActionsView *postActionsView;
@property (nonatomic, strong) VAPost *activePost;
@property (nonatomic, strong) UIImage *activePostImage;

@property (nonatomic, weak) IBOutlet UIView* noPostsView;
@property (nonatomic, strong) NSMutableDictionary *postTitleStrings;
//@property (nonatomic, strong) NSMutableDictionary *postTextStrings;

- (IBAction)writePost:(UIButton *)sender;

- (IBAction)showLikesToPostWithButton:(UIButton *)sender;

- (IBAction)showCommentsToPostWithButton:(UIButton *)sender;

@property (nonatomic) BOOL isViewAppear;
@property (nonatomic) BOOL isCompletePostsRequest;
@end

CGFloat fullNewPostViewHeight = 356.f;
CGFloat imageContainerHeight = 176.f;

@implementation VAEventsViewController

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        _isHashTagMode = NO;
        _isCheckInMode = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    self.navigationItem.leftBarButtonItem = [self getMenuButtonWithBadge:NO];
    
    _isViewAppear = NO;
    _isCompletePostsRequest = NO;
    
    self.postTitleStrings = [NSMutableDictionary dictionary];
//    self.postTextStrings = [NSMutableDictionary dictionary];
    
    if (!IOS7) {
        [self addBlurNavigationViews];
    }
    
    self.postsArray = [NSArray new];
    
    self.postsHaveBeenLoaded = NO;
    
    if (!self.hashtagString) {
        self.hashtagString = @"";
    }
    
    if (!self.checkInName) {
        self.checkInName = @"";
    }
    
    if (!self.googlePlaceID) {
        self.googlePlaceID = @"";
    }
    
    [self reloadPostInfo];
    
    self.userPostContainerHeight.constant = 0.f;
    
    self.isPostWriting = NO;
    self.currentImage = nil;
    self.isRefreshing = NO;
    self.isLoadingNewPostBatch = NO;
    self.firstBatchHasBeenLoaded = NO;
    self.allPostsHasBeenLoaded = NO;
    self.indexPathOfSelectedPost = nil;
    self.addPostButtonVisible = YES;
    
    self.tableView.clipsToBounds = NO;
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(7.f, 0.f, 0.f, -8.f);       //7 is an top inset for heaeder height = 7.f
    
    [self setupRefreshControl];
    
    self.addPostButton.layer.cornerRadius = 24.5f;
    self.addPostButton.clipsToBounds = YES;
    
    if (IOS8 || IOS9) {
        UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVibrancyEffect *vibrancy = [UIVibrancyEffect effectForBlurEffect:blur];
        
        UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
        effectView.frame = self.view.frame;
        
        UIVisualEffectView *vibrantView = [[UIVisualEffectView alloc]initWithEffect:vibrancy];
        effectView.frame = self.view.frame;
        
        [self.blurView addSubview:effectView];
        [self.blurView addSubview:vibrantView];
    }
    
    if (self.isHashTagMode) {
        [self.addPostButton setHidden:YES];
        [self.addPostButtonBackground setHidden:YES];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackButton"] style:UIBarButtonItemStylePlain target:self.navigationController action:@selector(popViewControllerAnimated:)];
        self.navigationItem.rightBarButtonItem = nil;
        
        //code for google analytics
        id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Hashtag"
                                                              action:@"Hashtag click"
                                                               label:@"Posts with hashtag viewed"
                                                               value:@1] build]];
    }
    
    if (self.isCheckInMode) {
        [self.addPostButton setHidden:YES];
        [self.addPostButtonBackground setHidden:YES];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackButton"] style:UIBarButtonItemStylePlain target:self.navigationController action:@selector(popViewControllerAnimated:)];
        self.navigationItem.rightBarButtonItem = nil;
        
        //code for google analytics
        id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Check In"
                                                              action:@"\"View recent check ins\" click"
                                                               label:@"Posts for check in viewed"
                                                               value:@1] build]];
    }
    
    [self askForNotificationPermissions];
    
    self.blurView.alpha = 0.f;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updatePosts)
                                                 name:VAUserDidBlockedNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updatePosts)
                                                 name:VAUserDidUnblockedNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(profileUpdated)
                                                 name:kDidChangeUserAvatar
                                               object:nil];
    
    [self setupActionsPopUp];
    
    UINib *postCellNib = [UINib nibWithNibName:@"VAPostsTableViewCell" bundle:[NSBundle mainBundle]];
    [self.tableView registerNib:postCellNib forCellReuseIdentifier:@"PostsCell"];

    if (!self.isHashTagMode && !self.isCheckInMode) {
        [[VAPostStore sharedPostStore] readFeedPostsWithSuccess:^(NSArray *posts) {
            if (!self.isCompletePostsRequest) {
                [[self mutableArrayValueForKey:@"postsArray"] addObjectsFromArray:posts];
                [self.tableView reloadData];
            }
        } failure:^(NSError *error) {
            NSLog(@"failure");
        }];
    }
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    [self.view bringSubviewToFront:self.navigationBlurView];
    [self.view bringSubviewToFront:self.navigationAlphaView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.title = @"Vicinity";
    
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Vicinity screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatePosts) name:kDidUpdateLocaion object:nil];        //this notification is posted in applicationWillEnterForeground: method of App Delegate after updating location
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationServicesDisabled) name:kDidDisableLocationServices object:nil];

    [self addBadgeObservers];
    
    if (self.isHashTagMode) {
        self.navigationController.title = [NSString stringWithFormat:@"#%@", self.hashtagString];
    }
    
    if (self.isCheckInMode) {
        self.navigationController.title = @"Recent Check-ins";
    }
    
    if ([VAUserDefaultsHelper shouldShowNotificationBadge]) {
        self.navigationItem.leftBarButtonItem = [self getMenuButtonWithBadge:YES];
    }
    
    if (!IOS7) {
        [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                      forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [UIImage new];
        self.navigationController.navigationBar.translucent = YES;
    } else {
        [(DEMONavigationController *)self.navigationController setDefaultNavigationBar];
    }
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    _isViewAppear = YES;
    if (![[VALocationTool sharedInstance] isServicesEnabled]) {
        self.tableView.tableHeaderView = nil;
        self.tableView.tableFooterView = nil;
        [self showNoLocationMessage];
        self.addPostButton.userInteractionEnabled = NO;
    } else {
        self.addPostButton.userInteractionEnabled = YES;
    }
    [[LaunchKit sharedInstance] presentAppReleaseNotesFromViewController:self completion:^(BOOL didPresent) {
        if (didPresent) {

        }
    }];

}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kDidUpdateLocaion object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kDidDisableLocationServices object:nil];
    
    [self removeBadgeObservers];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Action

- (IBAction)showMenu:(id)sender {
    [((DEMONavigationController*)self.navigationController) showMenu];
}

- (IBAction)showFilters:(UIBarButtonItem *)sender {
    VAFilterViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"VAFilterViewController"];
    vc.hideCategories = YES;
    vc.delegate = self;
    if (IOS7) {
        vc.modalPresentationStyle = UIModalPresentationCurrentContext;
    } else if (IOS8 || IOS9) {
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    }
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)writePost:(UIButton *)sender {
    if (!self.isPostWriting) {
        if (self.tableView.contentOffset.y == 0.f) {
            [self showPost];
        } else {
            [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
            [self showPost];
            //then scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView method of UIScrollVIewDelegate will be called and we will call showPost there
        }
    } else {
        [self hidePost];
        [self clearPostView];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([[VALocationTool sharedInstance] isServicesEnabled]) {
        return [self.postsArray count];
    } else {
        return 0;
    }
//    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
//    return self.postsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Post";
    
//    VAPost *post = self.postsArray[indexPath.row];
//    VAPostsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PostsCell" forIndexPath:indexPath];
//    NSAttributedString *titleString = self.postTitleStrings[post.postID];
//    if (!titleString) {
//        self.postTitleStrings[post.postID] = [self decoratedNameStringForPost:post];
//        titleString = self.postTitleStrings[post.postID];
//    }
//    NSAttributedString *textString = self.postTextStrings[post.postID];
//    if (!textString) {
//        self.postTextStrings[post.postID] = [self decoratedPostStringFromString:post.text];
//        textString = self.postTextStrings[post.postID];
//    }
//    [cell configureCellWithPost:post postNameString:titleString postTextString:textString];
//    return cell;
    
    
    VAPost *post = nil;
    if ([self.postsArray count] > 0) {
        post = [self.postsArray objectAtIndex:indexPath.section];
    }
    
    VAPostTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    cell.delegate = self;
    
    cell.path = indexPath;
    [cell configureCellWithPost:post];
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];

    return cell;
}

- (CGFloat)calculateHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Post";
    
    static VAPostTableViewCell *postCell = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        postCell = [self.tableView dequeueReusableCellWithIdentifier:identifier];
    });
    postCell.path = indexPath;
    
    VAPost *post = [self.postsArray objectAtIndex:indexPath.section];

    CGFloat h = [postCell heightForCellWithPost:post];
    self.postTitleStrings[post.postID] = @(h);
    return h;
}

- (CGFloat)getRowHeightAtIndexPath:(NSIndexPath *)indexPath {
    VAPost *post = [self.postsArray objectAtIndex:indexPath.section];
    NSNumber *height = self.postTitleStrings[post.postID];
    if (height) {
        NSLog(@"Height ready");
        return [height floatValue];
    } else {
        NSLog(@"Height calculated");
        CGFloat h = [self calculateHeightForRowAtIndexPath:indexPath];
        self.postTitleStrings[post.postID] = @(h);
        return h;
    }
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 5.f;
}

//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return 200.0f;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return UITableViewAutomaticDimension;
//    static NSString *identifier = @"Post";
//    
//    static VAPostTableViewCell *postCell = nil;
//    
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        postCell = [self.tableView dequeueReusableCellWithIdentifier:identifier];
//    });
//    postCell.path = indexPath;
//    
//    VAPost *post = [self.postsArray objectAtIndex:indexPath.section];
//    NSNumber *height = self.postTitleStrings[post.postID];
//    if (height) {
//        NSLog(@"Height ready");
//        return [height floatValue];
//    } else {
//        NSLog(@"Height calculated");
//        CGFloat h = [postCell heightForCellWithPost:post];
//        self.postTitleStrings[post.postID] = @(h);
//        return h;
//    }
    
    
    return [self getRowHeightAtIndexPath:indexPath];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 7)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSIndexPath *startRequestIndexPath = [NSIndexPath indexPathForRow:0 inSection:[self.postsArray count] - 5];
    
    if ([indexPath isEqual:startRequestIndexPath]  && self.firstBatchHasBeenLoaded && !self.allPostsHasBeenLoaded) {
        self.isLoadingNewPostBatch = YES;
        [self makePostRequest];
    }
    
    VAPost *post = nil;
    if ([self.postsArray count]) {
        post = [self.postsArray objectAtIndex:indexPath.section];
    }
    if (post && ![post.isViewed boolValue]) {
        VAUserApiManager *manager = [VAUserApiManager new];
        [manager putPostViewWithPostID:post.postID andCompletion:^(NSError *error, VAModel *model) {
            if (model) {
                VAServerResponse *response = (VAServerResponse *)model;
                if ([response.status isEqualToString:kSuccessResponseStatus]) {
                    post.isViewed = [NSNumber numberWithBool:YES];
                }
            }
        }];
    }
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath  {
    self.firstBatchHasBeenLoaded = YES;
}

#pragma mark - Methods

- (void)showNoLocationMessage {
    [VANotificationMessage showTurnOnLocationMessageOnController:self withDuration:0.f];
}

- (void)reloadPostInfo {
    self.tableView.scrollEnabled = YES;
    if ([[VANotificationMessage currentMessageController] notificationMessageIsShown]) {
        [[VANotificationMessage currentMessageController] hideCurrentMessage];
    }
    if (![[VALocationTool sharedInstance] isServicesEnabled] && _isViewAppear) {
        self.tableView.scrollEnabled = NO;
        self.tableView.tableHeaderView = nil;
        self.tableView.tableFooterView = nil;
        [self.tableView reloadData];
        [self showNoLocationMessage];
    } else {
        [[VALocationTool sharedInstance] getCurrentDeviceLocationWithComplition:^(NSError *error, CLLocation *location) {
            if (error) {
//                [[VANotificationMessage currentMessage] showTopNotificationMessageWithTitle:error.localizedDescription onController:self];
//                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                    [[VANotificationMessage currentMessage] hideCurrentMessage];
//                });
                [self makePostRequest];
            } else if (location) {
                double lat = location.coordinate.latitude;
                double lon = location.coordinate.longitude;
                [VAUserDefaultsHelper savelatitude:lat andLongitude:lon];
                
                if ([VANotificationMessage currentMessageController].isLocationMessage) {
                    [[VANotificationMessage currentMessageController] hideCurrentMessage];
                }
                [self makePostRequest];
            }
        }];
    }
}

- (void)showPost {
    VAUser *user = (VAUser *)[VAUserDefaultsHelper getModelForKey:VASaveKeys_CurrentUser];
    if (user.avatarURL) {
        NSURL *imageURL = [NSURL URLWithString:user.avatarURL];
        
        [self.userPostView.avatarView sd_setImageWithURL:imageURL
                           placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                      if (image) {
                                          self.userPostView.avatarView.image = image;
                                      }
                                      if (error) {
                                          [VANotificationMessage showAvatarErrorMessageOnController:self withDuration:5.f];
                                      }
                                  }];
    } else {
        self.userPostView.avatarView.image = [UIImage imageNamed:@"placeholder.png"];
    }
    [self.userPostView.placeholderLabel setHidden:NO];
    [self.userPostView.popoverArrow setHidden:NO];
    self.tableView.scrollEnabled = NO;
    [self rotateAddPostButton];
    VANewPostView *newPostView = self.userPostView;
    newPostView.delegate = self;
    self.userPostContainerHeight.priority = 250.f;
    
    if (!self.userPostView.isLocationSet && self.userPostView.isPhotoSet) {
        self.userPostView.imageContainerHeight.constant = imageContainerHeight;
    } else if (self.userPostView.isLocationSet) {
        [self changeNewPostViewsPriority];
        self.userPostView.imageContainerHeight.constant = imageContainerHeight;
    }
    
    [UIView animateWithDuration:0.3f animations:^{
        self.blurView.alpha = 1.f;
        self.tableView.alpha = 0.5f;
        [self.view layoutIfNeeded];
    }];
    self.isPostWriting = YES;
}

- (void)hidePost {
    [self.userPostView.textView resignFirstResponder];
    [self.userPostView.popoverArrow setHidden:YES];
    self.tableView.scrollEnabled = YES;
    
    self.userPostContainerHeight.priority = 900.f;
    self.userPostContainerHeight.constant = 0.f;

    [UIView animateWithDuration:0.3f animations:^{
        self.tableView.alpha = 1.f;
        self.blurView.alpha = 0.0f;
        [self.view layoutIfNeeded];
    }];
    [self rotateAddPostButton];
    self.isPostWriting = NO;
}

- (void)clearPostView {
    self.userPostView.imageContainerHeight.priority = 750.f;
    self.userPostView.imageContainerHeight.constant = 0.f;
    self.currentImage = nil;
    self.currentLocation = nil;
    
    self.userPostView.textView.text = @"";
    self.userPostView.locationLabel.text = @"";
    self.userPostView.facebookSwitch.on = NO;
    self.userPostView.postImageView.image = nil;
    [self.userPostView.facebookImageView setImage:[UIImage imageNamed:@"facebook-icon-disabled.png"]];
    self.userPostView.isLocationSet = NO;
    self.userPostView.isPhotoSet = NO;
    [self setPostButtonAttributes];
}

- (void)setPostButtonAttributes {

    if ([self.userPostView.textView.text isEqualToString:@""]) {
        [self.userPostView.placeholderLabel setHidden:NO];
        self.userPostView.placeholderLabel.text = @"Write your message...";

    } else {
        [self.userPostView.placeholderLabel setHidden:YES];
        self.userPostView.placeholderLabel.text = @"";
    }
    
    if ([self.userPostView.textView.text isEqualToString:@""] && !self.currentLocation && !self.userPostView.postImageView.image) {
        self.userPostView.postButton.enabled = 0;
        self.userPostView.postButton.backgroundColor = [UIColor whiteColor];
    } else {
        self.userPostView.postButton.enabled = 1;
        self.userPostView.postButton.backgroundColor = [[VADesignTool defaultDesign] vicinityBlue];
    }
}

- (void)goToLocationsController {
    VAChooseLocationViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"VAChooseLocationViewController"];
    vc.delegate = self;
    if (self.currentLocation) {
//        vc.currentLocation = self.currentLocation;
    }
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)removeLocation {
    self.userPostView.locationLabel.text = @"";
    self.userPostView.isLocationSet = NO;
    self.currentLocation = nil;
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
    
    if (self.userPostView.isPhotoSet) {
        [self setNewPostViewsPriorityToDefault];
    }
    
    //code for google analytics
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Check In"
                                                          action:@"User removed check in"
                                                           label:nil
                                                           value:nil] build]];
}

- (void)retakePicture {
    [[VAPhotoTool defaultPhoto] setDelegate:self];
    [[VAPhotoTool defaultPhoto] showPhotoActionViewOnController:self];
}

- (void)removePicture {
    self.currentImage = nil;
    
    self.userPostView.postImageView.image = nil;
    self.userPostView.imageContainerHeight.constant = 0;
    self.userPostView.photoButton.enabled = YES;
    self.userPostView.isPhotoSet = NO;
    
    if (self.userPostView.isLocationSet) {
        [self setNewPostViewsPriorityToDefault];
    }
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
    
    //code for google analytics
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Photo"
                                                          action:@"User removed photo from post"
                                                           label:nil
                                                           value:nil] build]];
}

- (void)showActivityIndicator {
    if (self.loader) {
        [self hideActivityIndicator];
    }
    VALoaderView *loader = [VALoaderView initLoader];
    loader.frame = CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), kLoaderHeight);
    [loader startAnimating];
    self.tableView.tableHeaderView = loader;
    self.loader = loader;
    self.tableView.userInteractionEnabled = NO;
}

- (void)hideActivityIndicator {
    [self.loader stopAnimating];
    self.tableView.tableHeaderView = nil;
    self.loader = nil;
    self.tableView.userInteractionEnabled = YES;
}

- (void)makePostRequest {
    
    self.tableView.scrollEnabled = YES;
    
    if (!self.isRefreshing && !self.isLoadingNewPostBatch) {      //first post batch loading
        [self showActivityIndicator];
    } else if (self.isLoadingNewPostBatch) {
        VALoaderView *loader = [VALoaderView initLoader];
        self.footerLoader = loader;
        loader.frame = CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), kLoaderHeight);
        [loader startAnimating];
        self.tableView.tableFooterView = loader;
    }
    
    VAUserApiManager *manager = [VAUserApiManager new];
    NSString *milesString = [[VAFiltersTool currentFilters] loadDistance];
    if (!milesString) {
        milesString = @"10 miles";
    }
    CGFloat distance = [milesString metersFromMilesString];
    
    NSNumber *batchOffset = nil;
    if (self.isRefreshing || !self.isCompletePostsRequest) {
        batchOffset = [NSNumber numberWithInteger:0];
    } else {
        batchOffset = [NSNumber numberWithInteger:[self.postsArray count]];
    }
    
    NSNumber *lat = ((NSNumber *)[[NSUserDefaults standardUserDefaults] valueForKey:kLatValue]);
    NSNumber *lon = ((NSNumber *)[[NSUserDefaults standardUserDefaults] valueForKey:kLonValue]);
    
    NSDictionary *params = nil;
    
    if (self.isHashTagMode) {
        params = @{
                   @"hashtag": self.hashtagString,
                   @"lat": lat,
                   @"lon": lon
                   };
        
        [manager getPostsWithParams:params completion:^(NSError *error, VAModel *model) {
            if (model) {
                if (!self.isRefreshing && !self.isLoadingNewPostBatch) {           //first batch loading
                    [self hideActivityIndicator];
                } else if (self.isLoadingNewPostBatch) {                           //second, third etc... batch loading
                    [self.footerLoader stopAnimating];
                    self.tableView.tableFooterView = nil;
                    self.isLoadingNewPostBatch = NO;
                } else {                                                           //pull to refresh have been used
                    [self.refresh stopRefreshing];
                    self.isRefreshing = NO;
                    [[self mutableArrayValueForKey:@"postsArray"] removeAllObjects];
                    self.allPostsHasBeenLoaded = NO;
                }
                VAPostResponse *response = (VAPostResponse *)model;
                if ([response.posts count] < postLimit) {                          //all post have been loaded
                    self.allPostsHasBeenLoaded = YES;
                }
                
                [[self mutableArrayValueForKey:@"postsArray"] addObjectsFromArray:response.posts];
                
                for (int i = 0; i < self.postsArray.count; i++) {
                    [self calculateHeightForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:i]];
                }
                [self.tableView reloadData];
            } else if (error) {
                if (!self.isRefreshing && !self.isLoadingNewPostBatch) {
                    [self hideActivityIndicator];
                } else if (self.isLoadingNewPostBatch) {
                    [self.footerLoader stopAnimating];
                    self.tableView.tableFooterView = nil;
                    self.isLoadingNewPostBatch = NO;
                } else {
                    [self.refresh stopRefreshing];
                    self.isRefreshing = NO;
                }
                
                [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
            }
        }];

    } else if (self.isCheckInMode) {
        
        [manager getPostsForPlaceWithID:self.googlePlaceID
                        currentLatitude:lat
                       currentLongitude:lon
                              withLimit:@(100000)
                                 offset:@(0)
                          andCompletion:^(NSError *error, VAModel *model) {
                              if (model) {
                                  if (!self.isRefreshing) {
                                      [self hideActivityIndicator];
                                  } else {
                                      [self.refresh stopRefreshing];
                                      self.isRefreshing = NO;
                                      [[self mutableArrayValueForKey:@"postsArray"] removeAllObjects];
                                  }
                                  VAPostResponse *response = (VAPostResponse *)model;
                                  
                                  [[self mutableArrayValueForKey:@"postsArray"] addObjectsFromArray:response.posts];
                                  for (int i = 0; i < self.postsArray.count; i++) {
                                      [self calculateHeightForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:i]];
                                  }
                                  [self.tableView reloadData];
                              } else if (error) {
                                  [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
                              }
        }];
        
    } else {
        if (lat && lon) {
            params = @{
                       @"lat": lat.stringValue,
                       @"lon": lon.stringValue,
//                       @"lat": @"53.881678",
//                       @"lon": @"27.586670",
                       @"offset": batchOffset,
                       @"limit": [NSNumber numberWithInteger:postLimit],
                       @"distance": @(distance),
                       };
            
            [manager getPostsWithParams:params completion:^(NSError *error, VAModel *model) {
                // Complete posts request
                if (model) {
                    if (!self.isRefreshing && !self.isLoadingNewPostBatch) {           //first batch loading
                        [self hideActivityIndicator];
                    } else if (self.isLoadingNewPostBatch) {                           //second, third etc... batch loading
                        [self.footerLoader stopAnimating];
                        self.tableView.tableFooterView = nil;
                        self.isLoadingNewPostBatch = NO;
                    } else {                                                           //pull to refresh have been used
                        [self.refresh stopRefreshing];
                        self.isRefreshing = NO;
                        [[self mutableArrayValueForKey:@"postsArray"] removeAllObjects];
                        self.allPostsHasBeenLoaded = NO;
                    }
                    VAPostResponse *response = (VAPostResponse *)model;
                    if ([response.status isEqualToString:kErrorResponseStatus]) {
                        
                    } else {
                        if (!self.isCompletePostsRequest) {
                            [[self mutableArrayValueForKey:@"postsArray"] removeAllObjects];
                        }
                        self.isCompletePostsRequest = YES;
                        [[self mutableArrayValueForKey:@"postsArray"] addObjectsFromArray:response.posts];
                        
                        [[VAPostStore sharedPostStore] saveFeedPosts:self.postsArray];
                        
                        for (int i = 0; i < self.postsArray.count; i++) {
                            [self calculateHeightForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:i]];
                        }
                        [self.tableView reloadData];
                        
                        if ([self.postsArray count] == 0) {
                            [self showNoPostsView];
                        } else {
                            [self hideNoPostsView];
                        }
                    }
                    
                    if ([response.posts count] < postLimit) {                          //all post have been loaded
                        self.allPostsHasBeenLoaded = YES;
                        
                        if ([self.postsArray count] != 0 && [[VALocationTool sharedInstance] isServicesEnabled]) {
                            VAEndFeedView *view = [[[NSBundle mainBundle] loadNibNamed:@"VAEndFeedView" owner:self options:nil] firstObject];
                            view.delegate = self;
                            [view setFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.bounds), 360.0f)];
                            self.tableView.tableFooterView = view;
                        }
                    }
                } else if (error) {
                    if (!self.isRefreshing && !self.isLoadingNewPostBatch) {
                        [self hideActivityIndicator];
                    } else if (self.isLoadingNewPostBatch) {
                        [self.footerLoader stopAnimating];
                        self.tableView.tableFooterView = nil;
                        self.isLoadingNewPostBatch = NO;
                    } else {
                        [self.refresh stopRefreshing];
                        self.isRefreshing = NO;
                    }
                    
                    [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
                }
                if ([VAControlTool topScreenController] == self) {
                    [[LaunchKit sharedInstance] presentAppReleaseNotesFromViewController:self completion:^(BOOL didPresent) {
                        if (didPresent) {

                        }
                    }];
                }

            }];
        } else {
            if (![[VALocationTool sharedInstance] isServicesEnabled]) {
                [self showNoLocationMessage];
            } else {
                [VANotificationMessage showCantUpdateLocationMessageOnController:self withDuration:5.f];
            }
        }
    }
}

- (void)sendPostToServer {
    NSNumber *lat = [[NSUserDefaults standardUserDefaults] valueForKey:kLatValue];
    NSNumber *lon = [[NSUserDefaults standardUserDefaults] valueForKey:kLonValue];
    VALocation *postLocation = nil;
    
    if (!lat || !lon) {
        [VANotificationMessage showTurnOnLocationMessageOnController:self withDuration:5.f];
        return;
    }
    
    NSDictionary *params = @{
                             @"lat": lat,
                             @"lon": lon,
                             @"wallPost": self.userPostView.facebookSwitch.isOn ? @"true" : @"false",
                             @"isLandscapePhoto": self.userPostView.isLandscapePhoto ? @"true" : @"false"
                             };
    
    NSMutableDictionary *temp = [NSMutableDictionary dictionaryWithDictionary:params];
    if (![self.userPostView.textView.text isEqualToString:@""]) {
        [temp setObject:self.userPostView.textView.text forKey:@"description"];
    }
    if (self.currentLocation.placeID && ![self.currentLocation.placeID isEqualToString:@" "]) {
        [temp setObject:self.currentLocation.placeID forKey:@"placeId"];
        [temp setObject:self.currentLocation.coordinate.latitude forKey:@"lat"];
        [temp setObject:self.currentLocation.coordinate.longitude forKey:@"lon"];
        postLocation = [self.currentLocation copy];
    } else if (self.currentLocation.companyID) {
        [temp setObject:self.currentLocation.companyID forKey:@"companyId"];
    }
    
    if (self.currentLocation.facebookPlaceID) {
        [temp setObject:self.currentLocation.facebookPlaceID forKey:@"facebookPlaceId"];
    }
    
    params = temp;
    VAUserApiManager *manager = [VAUserApiManager new];
    NSData *imgData = UIImageJPEGRepresentation(self.userPostView.postImageView.image, 0.1);
    
    [manager postPostWithParams:params imageData:imgData withCompletion:^(NSError *error, VAModel *model) {
        [self hideActivityIndicator];
        if (model) {
            VANewPostResponse *postResponse = (VANewPostResponse *)model;
            if (postLocation) {
                postResponse.post.checkIn.distance = postLocation.distance;
            }
            [self addNewPost:postResponse.post];
            [self clearPostView];
        } else if (error) {
            [VANotificationMessage showAddingPostErrorMessageOnController:self withDuration:5.f];
            [self showPost];
        }
    }];
}

- (void)addNewPost:(VAPost *)post {
    NSString *milesString = [[VAFiltersTool currentFilters] loadDistance];
    if (!milesString) {
        milesString = @"10 miles";
    }
    CGFloat distance = [milesString metersFromMilesString];
    if (!post.checkIn || [post.checkIn.distance floatValue] < distance) {
        [[self mutableArrayValueForKey:@"postsArray"] insertObject:post atIndex:0];
        
        [[VAPostStore sharedPostStore] saveFeedPosts:self.postsArray];
        
        [self.tableView insertSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationTop];
        [self.refresh setFrame:[self frameForCustomRefreshControl]];
    }
}

- (CGFloat)heightForImageToFitCellWidth:(UICollectionViewCell *)cell forImage:(UIImage *)image {
    CGSize imgSize = image.size;
    CGFloat height = imgSize.height / imgSize.width * (CGRectGetWidth([UIScreen mainScreen].bounds) - 32.f);
    return height;
}

- (void)setupRefreshControl {
    VALoaderView *refresh = [VALoaderView initLoader];
    self.refresh = refresh;
    [refresh setFrame:[self frameForCustomRefreshControl]];
    refresh.alpha = 0.f;
    
    [refresh addTarget:self
                action:@selector(refreshData)
      forControlEvents:UIControlEventValueChanged];
    
    [self.tableView addSubview:refresh];
}

- (CGRect)frameForCustomRefreshControl {
    CGFloat customRefreshControlHeight = kLoaderHeight;
    CGFloat customRefreshControlWidth = CGRectGetWidth([self.tableView frame]);
    CGRect customRefreshControlFrame = CGRectMake(0.0f,
                                                  -customRefreshControlHeight,
                                                  customRefreshControlWidth,
                                                  customRefreshControlHeight);
    return customRefreshControlFrame;
}

- (void)locationServicesDisabled {
    [[self mutableArrayValueForKey:@"postsArray"] removeAllObjects];
    self.allPostsHasBeenLoaded = NO;
    self.tableView.tableFooterView = nil;
    self.tableView.tableHeaderView = nil;
    [self.tableView reloadData];
    [self showNoLocationMessage];
}

- (void)refreshData {
    [self.refresh startRefreshing];
    self.isRefreshing = YES;
    [self reloadPostInfo];
}

- (void)updatePosts {
    if ([[VANotificationMessage currentMessageController] notificationMessageIsShown]) {
        [[VANotificationMessage currentMessageController] hideCurrentMessage];
    }
    self.allPostsHasBeenLoaded = NO;
    self.tableView.tableFooterView = nil;
    [self.tableView reloadData];
    [self makePostRequest];
}

- (void)changeNewPostViewsPriority {
    self.userPostContainerHeight.constant = fullNewPostViewHeight;
    self.userPostContainerHeight.priority = 900.f;
    self.userPostView.imageContainerHeight.priority = 250.f;
}

- (void)setNewPostViewsPriorityToDefault {
    self.userPostContainerHeight.constant = 0.f;
    self.userPostContainerHeight.priority = 250.f;
    self.userPostView.imageContainerHeight.priority = 750.f;
}

- (NSMutableAttributedString *)attributedTextForUserName:(NSString *)name andLocation:(NSString *)location {
    NSRange nameBoldedRange = NSMakeRange(0, name.length);
    NSMutableAttributedString *checkInAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ checked in at %@", name, location]
                                                                                          attributes:@{
                                                                                                       NSFontAttributeName : [[VADesignTool defaultDesign] vicinityRegularFontOfSize:13.f],
                                                                                                       NSForegroundColorAttributeName: [UIColor blackColor]
                                                                                                       }];
    [checkInAttrString addAttributes:@{
                                       NSFontAttributeName: [[VADesignTool defaultDesign] vicinityBoldFontOfSize:15.f],
                                       NSForegroundColorAttributeName: [[VADesignTool defaultDesign] vicinityDarkBlue]
                                       }
                               range:nameBoldedRange];
    
    NSInteger preCheckInLength = [[NSString stringWithFormat:@"%@ checked in at ", name] length];
    NSRange checkInRange = NSMakeRange(preCheckInLength, location.length);
    
    [checkInAttrString addAttributes:@{
                                       NSFontAttributeName: [[VADesignTool defaultDesign] vicinityRegularFontOfSize:13.f],
                                       NSForegroundColorAttributeName: [[VADesignTool defaultDesign] vicinityDarkBlue]
                                       }
                               range:checkInRange];
    return checkInAttrString;
}

- (VAPost *)postForCellWithPushedButton:(UIButton *)button {
    VAPostTableViewCell *cell = (VAPostTableViewCell *)[UITableViewCell findParentCellOfView:button];
    NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
    self.indexPathOfSelectedPost = cellIndexPath;
    VAPost *post = [self.postsArray objectAtIndex:cellIndexPath.section];
    return post;
}

- (void)showButtonBadge {
    self.navigationItem.leftBarButtonItem = [self getMenuButtonWithBadge:YES];
}

- (void)hideButtonBadge {
    self.navigationItem.leftBarButtonItem = [self getMenuButtonWithBadge:NO];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kShouldShowNotificationBadge];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void)rotateAddPostButton {
    if (self.isPostWriting) {
        [UIView animateWithDuration:0.3f
                              delay:0.f
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                             self.addPostButton.backgroundColor = [[VADesignTool defaultDesign] vicinityBlue];
                             [self.addPostButton setImage:[UIImage imageNamed:@"plus_icon"] forState:UIControlStateNormal];
                             self.addPostButton.transform = CGAffineTransformMakeRotation(0);
                         }
                         completion:nil];
    } else {
        [UIView animateWithDuration:0.3f
                              delay:0.f
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                             self.addPostButton.backgroundColor = [UIColor colorWithRed:197.f/255.f green:99.f/255.f blue:92.f/255.f alpha:1.f];
                             [self.addPostButton setImage:[UIImage imageNamed:@"cross_icon"] forState:UIControlStateNormal];
                             self.addPostButton.transform = CGAffineTransformMakeRotation(M_PI_4);
                         }
                         completion:nil];
    }
}

- (void)askForNotificationPermissions {
    if ([VAUserDefaultsHelper hasPromptedForUserNotification] && ![[VAControlTool defaultControl] userHasGivenNotificationsPermission]) {
#if TARGET_IPHONE_SIMULATOR
        NSLog(@"Push notifications unavailable in simulator.");
#else
        [VANotificationMessage showTurnOnNotificationsMessageOnController:self withDuration:5.f];
#endif
    } else {
        [[VAControlTool defaultControl] registerApplicationNotificationSettings];
    }
}

- (void)addBlurNavigationViews {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 64)];
    view.backgroundColor = [[VADesignTool defaultDesign] vicinityBlue];
    [self.view addSubview:view];
    self.navigationAlphaView = view;
    
    UIView *blurView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 64)];
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
    effectView.frame = blurView.bounds;
    [blurView addSubview:effectView];
    
    self.navigationBlurView = blurView;
    
    [self.view addSubview:blurView];
}

- (void)showNoPostsView {
    VAEmptyVicinityView *view = [[[NSBundle mainBundle] loadNibNamed:@"VAEmptyVicinityView" owner:self options:nil] firstObject];
    view.delegate = self;
    [view setFrame:self.tableView.bounds];
    self.tableView.tableHeaderView = view;
    self.tableView.tableFooterView = nil;
}

- (void)hideNoPostsView {
    self.tableView.tableHeaderView = nil;
}

#pragma mark - Post Actions

- (IBAction)showLikesToPostWithButton:(UIButton *)sender {
    VAPost *post = [self postForCellWithPushedButton:sender];

    VAPostTableActions *action = [VAPostTableActions new];
    [action tableView:self.tableView showLikesForPost:post];
    
    //code for google analytics
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Likes"
                                                          action:@"Show likes for post"
                                                           label:@"From Vicinity screen"
                                                           value:nil] build]];
}

- (void)shouldShowCommentsForPost:(VAPost *)post {
    VAPostTableActions *action = [VAPostTableActions new];
    [action tableView:self.tableView showCommentsForPost:post];
}

- (IBAction)showCommentsToPostWithButton:(UIButton *)sender {
    VAPost *post = [self postForCellWithPushedButton:sender];

    VAPostTableActions *action = [VAPostTableActions new];
    [action tableView:self.tableView showCommentsForPost:post];
}

- (IBAction)likeThisPost:(UIButton *)sender {
    VAPost *post = [self postForCellWithPushedButton:sender];
    [self.postTitleStrings  removeObjectForKey:post.postID];
    VAPostTableViewCell *cell = (VAPostTableViewCell *)[UITableViewCell findParentCellOfView:sender];
    
    VAPostTableActions *action = [VAPostTableActions new];
    [action tableView:self.tableView performLikePost:post inCell:cell];
}

- (IBAction)editPost:(UIButton *)sender {
    VAPost *post = [self postForCellWithPushedButton:sender];
    [self.postTitleStrings  removeObjectForKey:post.postID];
    VAUser *loggedUser = (VAUser *)[VAUserDefaultsHelper getModelForKey:VASaveKeys_CurrentUser];
    if ([post.user.userId isEqualToString:loggedUser.userId]) {
        VAPostTableActions *action = [VAPostTableActions new];
        action.delegate = self;
        
        [action tableView:self.tableView editPost:post];
    } else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                                 message:nil
                                                                          preferredStyle:UIAlertControllerStyleActionSheet];
        
        if ([post.isSubscribed boolValue]) {
            UIAlertAction *unsubscribe = [UIAlertAction actionWithTitle:@"Unsubscribe" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                post.isSubscribed = [NSNumber numberWithBool:NO];
                VAUserApiManager *manager = [VAUserApiManager new];
                [manager unsubscribePostWithID:post.postID andCompletion:^(NSError *error, VAModel *model) {
                    if (error) {
                        post.isSubscribed = [NSNumber numberWithBool:YES];
                    }
                }];
            }];
            [alertController addAction:unsubscribe];
        } else {
            UIAlertAction *subscribe = [UIAlertAction actionWithTitle:@"Subscribe" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                post.isSubscribed = [NSNumber numberWithBool:YES];
                VAUserApiManager *manager = [VAUserApiManager new];
                [manager subscribePostWithID:post.postID andCompletion:^(NSError *error, VAModel *model) {
                    if (error) {
                        post.isSubscribed = [NSNumber numberWithBool:NO];
                    }
                }];
            }];
            [alertController addAction:subscribe];
        }
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        [alertController addAction:cancel];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

#pragma mark - VANewPostActionsProtocol

- (void)locationButtonPushed {
    if (self.userPostView.isLocationSet) {
        [self showLocationActionSheet];
    } else {
        [self.userPostView.textView resignFirstResponder];
        [self goToLocationsController];
    }
}

- (void)photoButtonPushed {
    if (self.userPostView.isPhotoSet) {
        [self showPhotoActionSheet];
    } else {
        [self.userPostView.textView resignFirstResponder];
        [self retakePicture];
    }
}

- (void)postButtonPushed {
    if (self.userPostView.facebookSwitch.isOn && self.currentLocation.placeID && ![self.currentLocation.placeID isEqualToString:@" "]) {
        [self tryToFindFacebookPlaceForPost];
    } else {
        [self sendPost];
    }
}

- (void)sendPost {
    //code for google analytics
    for (NSString *word in [self.userPostView.textView.text componentsSeparatedByString:@" "]) {
        if ([word rangeOfString:@"#"].location != NSNotFound) {
            id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Hashtag"
                                                                  action:@"Hashtag created"
                                                                   label:nil
                                                                   value:@1] build]];
        }
    }
    
    if (self.userPostView.facebookSwitch.isOn) {
        //code for google analytics
        id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Facebook"
                                                              action:@"Repost to Facebook"
                                                               label:nil
                                                               value:nil] build]];
    }
    
    [self sendPostToServer];
    [self hidePost];
    [self showActivityIndicator];
    
    //code for google analytics
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Posts"
                                                          action:@"Add post"
                                                           label:@"From Vicinity screen"
                                                           value:nil] build]];
}

- (void)showLocationActions {
    [self showLocationActionSheet];
}

- (void)showPhotoActions {
    [self showPhotoActionSheet];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    [self setPostButtonAttributes];
}

- (void)textViewDidChange:(UITextView *)textView {
    [self setPostButtonAttributes];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

#pragma mark - VAPhotoToolDelegate

- (void)photoHasBeenEdited:(UIImage *)photo landscape:(BOOL)isLandscape {
    if (self.currentImage) {
        //code for google analytics
        id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Photo"
                                                              action:@"User changed photo to post"
                                                               label:nil
                                                               value:nil] build]];
    } else {
        //code for google analytics
        id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Photo"
                                                              action:@"User added photo to post"
                                                               label:nil
                                                               value:nil] build]];
    }
    
    self.currentImage = photo;
    self.userPostView.postImageView.image = photo;
    
    if (!self.userPostView.isLocationSet) {
        self.userPostView.postImageView.image = photo;
        self.userPostView.imageContainerHeight.constant = imageContainerHeight;
    } else {
        [self changeNewPostViewsPriority];
        self.userPostView.imageContainerHeight.constant = imageContainerHeight;
    }

    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
    [self setPostButtonAttributes];
    self.userPostView.isPhotoSet = YES;
    self.userPostView.isLandscapePhoto = isLandscape;
}

#pragma mark - Action sheet methods

- (void)showLocationActionSheet {
    if(IOS8 || IOS9) {
        UIAlertController *ac = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *change = [UIAlertAction actionWithTitle:@"Change location" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self goToLocationsController];
        }];
        UIAlertAction *delete = [UIAlertAction actionWithTitle:@"Remove location" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self removeLocation];
            [self setPostButtonAttributes];
        }];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        [ac addAction:change];
        [ac addAction:delete];
        [ac addAction:cancel];
        [self presentViewController:ac animated:YES completion:nil];
    } else if (IOS7) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Change location", @"Remove location", nil];
        actionSheet.tag = kLocationTag;
        [actionSheet showInView:self.view];
    }
}

- (void)showPhotoActionSheet {
    if (IOS8 || IOS9) {
        UIAlertController *ac = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *change = [UIAlertAction actionWithTitle:@"Retake picture" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self retakePicture];
        }];
        UIAlertAction *delete = [UIAlertAction actionWithTitle:@"Remove picture" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self removePicture];
            [self setPostButtonAttributes];
        }];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        [ac addAction:change];
        [ac addAction:delete];
        [ac addAction:cancel];
        [self presentViewController:ac animated:YES completion:nil];
    } else if (IOS7) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Retake picture", @"Remove picture", nil];
        actionSheet.tag = kPhotoTag;
        [actionSheet showInView:self.view];
    }
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        if (actionSheet.tag == kLocationTag) {
            [self goToLocationsController];
        } else if (actionSheet.tag == kPhotoTag) {
            [self retakePicture];
        }
    } else if (buttonIndex == 1) {
        if (actionSheet.tag == kLocationTag) {
            [self removeLocation];
            [self setPostButtonAttributes];
        } else if (actionSheet.tag == kPhotoTag) {
            [self removePicture];
            [self setPostButtonAttributes];
        }
    }
}

#pragma mark - VALocationDelegate

- (void)userDidChooseLocation:(VALocation *)location {
    self.currentLocation = location;
    [self setPostButtonAttributes];
    NSString *locationText = [NSString stringWithFormat:@"You checked in at %@", location.name];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:locationText];
    [attrStr addAttributes:@{
                             NSForegroundColorAttributeName: [[VADesignTool defaultDesign] vicinityDarkBlue]
                            }
                    range:[locationText rangeOfString:location.name]];
    self.userPostView.locationLabel.attributedText = attrStr;
    self.userPostView.isLocationSet = YES;
    if (self.userPostView.isPhotoSet) {
        [self changeNewPostViewsPriority];
    }
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self.refresh containingScrollViewDidEndDragging:scrollView];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.refresh containingScrollViewDidScroll:scrollView];
    
    if (self.lastContentOffset > scrollView.contentOffset.y) {    //scrolling up
        if (!self.addPostButtonVisible) {
            self.addPostButtonBottomConstraint.constant = 12.f;
            self.addPostButtonBottomBackgroundConstraint.constant = 9.f;
            [UIView animateWithDuration:0.5f animations:^{
                [self.view layoutIfNeeded];
            }];
            self.addPostButtonVisible = YES;
        }
    } else if (self.lastContentOffset < scrollView.contentOffset.y && scrollView.contentOffset.y > 0) {    //scrolling down && not pull to refresh
        if (self.addPostButtonVisible) {
            self.addPostButtonBottomConstraint.constant = 0 - CGRectGetHeight([self.addPostButtonBackground frame]);
            self.addPostButtonBottomBackgroundConstraint.constant = 0 - CGRectGetHeight([self.addPostButtonBackground frame]);
            [UIView animateWithDuration:0.5f animations:^{
                [self.view layoutIfNeeded];
            }];
            self.addPostButtonVisible = NO;
        }
    }
    
    
    self.lastContentOffset = scrollView.contentOffset.y;
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    if (scrollView.contentOffset.y > 0) {
        [UIView animateWithDuration:0.5f animations:^{
            self.navigationAlphaView.alpha = 0.55f;
        }];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [UIView animateWithDuration:0.5f animations:^{
        self.navigationAlphaView.alpha = 1.f;
    }];

}

#pragma mark - VaFilterDelegate

- (void)userDidChangeFilters {
    [[self mutableArrayValueForKey:@"postsArray"] removeAllObjects];
    self.allPostsHasBeenLoaded = NO;
    self.tableView.tableFooterView = nil;
    [self.tableView reloadData];
    [self makePostRequest];
}

#pragma mark - VACommentDelegate

- (void)userDidAddCommentToPost:(VAPost *)post {
    NSInteger indexOfSelectedPost = [self.postsArray indexOfObject:post];
    if (indexOfSelectedPost != NSNotFound) {
        [[self mutableArrayValueForKey:@"postsArray"] replaceObjectAtIndex:indexOfSelectedPost withObject:post];
        for (int i = 0; i < self.postsArray.count; i++) {
            [self calculateHeightForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:i]];
        }
        [self.tableView reloadData];
    }
}

- (void)userDidDeleteCommentToPost:(VAPost *)post {
    NSInteger indexOfSelectedPost = [self.postsArray indexOfObject:post];
    if (indexOfSelectedPost != NSNotFound) {
        [[self mutableArrayValueForKey:@"postsArray"] replaceObjectAtIndex:indexOfSelectedPost withObject:post];
        for (int i = 0; i < self.postsArray.count; i++) {
            [self calculateHeightForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:i]];
        }
        [self.tableView reloadData];
    }
}

- (void)userDidDeletePost:(VAPost *)post {
    [[self mutableArrayValueForKey:@"postsArray"] removeObject:post];
    for (int i = 0; i < self.postsArray.count; i++) {
        [self calculateHeightForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:i]];
    }
    [self.tableView reloadData];
}

- (void)userDidEditCommentToPost:(VAPost *)post {
    NSInteger indexOfSelectedPost = [self.postsArray indexOfObject:post];
    if (indexOfSelectedPost != NSNotFound) {
        [[self mutableArrayValueForKey:@"postsArray"] replaceObjectAtIndex:indexOfSelectedPost withObject:post];
        for (int i = 0; i < self.postsArray.count; i++) {
            [self calculateHeightForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:i]];
        }
        [self.tableView reloadData];
    }
}

- (void)userDidEditPost:(VAPost *)post {
    NSInteger indexOfSelectedPost = [self.postsArray indexOfObject:post];
    if (indexOfSelectedPost != NSNotFound) {
        [[self mutableArrayValueForKey:@"postsArray"] replaceObjectAtIndex:indexOfSelectedPost withObject:post];
        for (int i = 0; i < self.postsArray.count; i++) {
            [self calculateHeightForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:i]];
        }
        [self.tableView reloadData];
    }
}

#pragma mark - VAVicinityEmptyViewProtocol

- (void)shouldShowFilters {
    [self showFilters:nil];
}

- (void)shouldShowShareSheet {
    //[[VAControlTool defaultControl] showShareSheet];
    [[NSNotificationCenter defaultCenter] postNotificationName:kShowInviteScreen object:nil];
}

#pragma mark - VAPostActionsDelegate

- (void)didPerformDeleteActionForPost:(VAPost *)post {
    NSInteger postIndex = [self.postsArray indexOfObject:post];
    
    [[self mutableArrayValueForKey:@"postsArray"] removeObject:post];
    
    [self.tableView beginUpdates];
    [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:postIndex] withRowAnimation:UITableViewRowAnimationLeft];
    [self.tableView endUpdates];
}

#pragma mark - Navigation

- (void)showFacebookLocationView {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Second" bundle:nil];
    VAFacebookLocationViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"facebookLocationVC"];
    self.providesPresentationContextTransitionStyle = YES;
    self.definesPresentationContext = YES;
    [vc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    vc.targetLocation = self.currentLocation;
    vc.delegate = self;
    [self.navigationController presentViewController:vc animated:YES completion:nil];
}

#pragma mark - FacebookLocationDelegate methods

- (void)didSelectPostWithFacebookPlaceId:(NSString *)placeId {
    self.currentLocation.facebookPlaceID = placeId;
    if (self.presentedViewController) {
        [self dismissViewControllerAnimated:YES completion:^{
            [self sendPost];
        }];
    }
}

- (void)didSelectPostWithoutFacebookPlaceId {
    if (self.presentedViewController) {
        [self dismissViewControllerAnimated:YES completion:^{
            [self sendPost];
        }];
    }
}

- (void)didSelectCancel {
    if (self.presentedViewController) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - Post delegate

- (void)didSelectShowActions:(VAPostTableViewCell *)cell {
    [self showActionsPopUpForCell:cell];
}

- (void)editPostSelected:(VAPost *)post {
    VAPostTableActions *action = [VAPostTableActions new];
    action.delegate = self;
    [action showEditViewForPost:post inTableView:self.tableView];
}

- (void)deletePostSelected:(VAPost *)post {
    VAPostTableActions *action = [VAPostTableActions new];
    action.delegate = self;
    [action showDeleteConfirmationAlertViewForPost:post inTableView:self.tableView];
}

- (void)blockUserSelected:(VAUser *)user {
    [self askConfirmationToBlockUser:user];
}

- (void)reportSelected:(VAUser *)user {
    [self showReportScreenWithUser:user];
}

- (void)turnOffNotificationsSelected:(VAPost *)post {
    [self unsubscibePost:post];
}

#pragma mark - Helpers

- (void)showReportScreenWithUser:(VAUser *)user {
    VAReportViewController *vc = [[UIStoryboard storyboardWithName:@"Vicinity" bundle:nil] instantiateViewControllerWithIdentifier:@"reportViewController"];
    vc.reportedUser = user;
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showReportScreenWithPost:(VAPost *)post {
    VAReportViewController *vc = [[UIStoryboard storyboardWithName:@"Vicinity" bundle:nil] instantiateViewControllerWithIdentifier:@"reportViewController"];
    vc.reportedPost = post;
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)subscibePost:(VAPost *)post {
    post.isSubscribed = [NSNumber numberWithBool:YES];
    
    NSInteger index = [self.postsArray indexOfObject:post];
    [self calculateHeightForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:index]];
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:index]] withRowAnimation:UITableViewRowAnimationNone];
    
    VAUserApiManager *manager = [VAUserApiManager new];
    [manager subscribePostWithID:post.postID andCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            post.isSubscribed = [NSNumber numberWithBool:NO];
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:index]] withRowAnimation:UITableViewRowAnimationNone];
        }
    }];
}

- (void)unsubscibePost:(VAPost *)post {
    post.isSubscribed = [NSNumber numberWithBool:NO];

    NSInteger index = [self.postsArray indexOfObject:post];
    [self calculateHeightForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:index]];
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:index]] withRowAnimation:UITableViewRowAnimationNone];
    
    VAUserApiManager *manager = [VAUserApiManager new];
    [manager unsubscribePostWithID:post.postID andCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            post.isSubscribed = [NSNumber numberWithBool:YES];
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:index]] withRowAnimation:UITableViewRowAnimationNone];
        }
    }];
}

- (void)askConfirmationToBlockUser:(VAUser *)user {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"Block %@", user.fullname]
                                                                             message:[NSString stringWithFormat:@"Are you sure you want to block %@?", user.fullname]
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"No"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * _Nonnull action) { }];
    [alertController addAction:noAction];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"Yes"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * _Nonnull action) {
                                                          [self blockUser:user];
                                                      }];
    [alertController addAction:yesAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)blockUser:(VAUser *)user {
    user.isBlocked = @(YES);
    
    [self showActivityIndicator];
    
    [[VAUserApiManager new] blockUserWithID:user.userId andCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            user.isBlocked = @(NO);
            [VANotificationMessage showNetworkErrorMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
        } else {
            [VANotificationsService didBlockUser:user];
        }
    }];
}

- (void)tryToFindFacebookPlaceForPost {
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                  initWithGraphPath:@"search"
                                  parameters:@{
                                               @"q": self.currentLocation.name,
                                               @"type": @"place",
                                               @"center": [NSString stringWithFormat:@"%@,%@", self.currentLocation.coordinate.latitude, self.currentLocation.coordinate.longitude],
                                               @"distance": @"1000"
                                               }
                                  HTTPMethod:@"GET"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                          id result,
                                          NSError *error) {
        if (error) {
            NSLog(@"%@", error);
            [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
        } else {
            NSLog(@"%@", result);
            NSMutableArray *facebookPlaces = [result[@"data"] mutableCopy];
            if (facebookPlaces.count == 1) {
                self.currentLocation.facebookPlaceID = facebookPlaces[0][@"id"];
                [self sendPost];
            } else {
                [self showFacebookLocationView];
            }
        }
    }];
}

#pragma mark - Report Sending delegate

- (void)didSendReportAboutUser:(VAUser *)user {
    [VANotificationMessage showReportSuccessMessageOnController:self withDuration:5.f];
}

#pragma mark - Pop Up

- (void)setupActionsPopUp {
    self.popUpView = [[UIView alloc] initWithFrame:self.navigationController.view.bounds];
    self.popUpView.backgroundColor = [UIColor clearColor];
    self.popUpView.hidden = YES;
    
    UIView *overlayView = [[UIView alloc] initWithFrame:self.popUpView.bounds];
    overlayView.backgroundColor = [UIColor blackColor];
    overlayView.alpha = 0.4f;
    UITapGestureRecognizer *overlayTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideActionsPopUp)];
    [overlayView addGestureRecognizer:overlayTap];
    [self.popUpView addSubview:overlayView];
    
    self.postActionsView = [[VAPostActionsView alloc] init];
    self.postActionsView.backgroundColor = [UIColor clearColor];
    self.postActionsView.delegate = self;
    [self.popUpView addSubview:self.postActionsView];
    
    [self.navigationController.view addSubview:self.popUpView];
}

- (void)showActionsPopUpForCell:(VAPostTableViewCell *)cell {
    CGRect cellFrame = [cell convertRect:cell.contentView.frame toView:self.navigationController.view];
    CGRect targetFrame = CGRectMake(cellFrame.origin.x, cellFrame.origin.y + 21.0f, cellFrame.size.width, [VAPostActionsService postActionsForPost:cell.post].count*37.0f + 2.0f);
    CGFloat spaceToBottom = CGRectGetMaxY(self.navigationController.view.frame) - CGRectGetMaxY(targetFrame);
    CGFloat tableOffset = 0.0f;
    if (spaceToBottom < 0) {
        tableOffset = -spaceToBottom;
        targetFrame = CGRectMake(targetFrame.origin.x, targetFrame.origin.y + spaceToBottom, targetFrame.size.width, targetFrame.size.height);
    }
    self.postActionsView.frame = targetFrame;
    self.postActionsView.postActions = [VAPostActionsService postActionsForPost:cell.post];
    self.activePost = cell.post;
    self.activePostImage = cell.postImageView.image;
    self.popUpView.alpha = 0.0f;
    self.popUpView.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        self.popUpView.alpha = 1.0f;
        self.tableView.contentOffset = CGPointMake(self.tableView.contentOffset.x, self.tableView.contentOffset.y + tableOffset);
    }];
}

- (void)hideActionsPopUp {
    [UIView animateWithDuration:0.3f animations:^{
        self.popUpView.alpha = 0.0f;
    } completion:^(BOOL finished) {
        self.popUpView.hidden = YES;
    }];
}

#pragma mark - Post Actions Delegate 

- (void)editPostSelected {
    [self.postTitleStrings  removeObjectForKey:self.activePost.postID];
    [self editPostSelected:self.activePost];
    [self hideActionsPopUp];
}

- (void)deletePostSelected {
    [self.postTitleStrings  removeObjectForKey:self.activePost.postID];
    [self deletePostSelected:self.activePost];
    [self hideActionsPopUp];
}

- (void)blockUserSelected {
    [self blockUserSelected:self.activePost.user];
    [self hideActionsPopUp];
}

- (void)reportUserSelected {
    [self reportSelected:self.activePost.user];
    [self hideActionsPopUp];
}

- (void)turnOffNotificationsSelected {
    [self turnOffNotificationsSelected:self.activePost];
    [self hideActionsPopUp];
}

- (void)copyPostSelected {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.activePost.text ? self.activePost.text : @"";
    [self hideActionsPopUp];
}

- (void)reportPostSelected {
    [self showReportScreenWithPost:self.activePost];
    [self hideActionsPopUp];
}

- (void)turnOnNotificationsSelected {
    [self subscibePost:self.activePost];
    [self hideActionsPopUp];
}

- (void)sharePostSelected {
    [[VAControlTool defaultControl] showSharePostSheetWithPost:self.activePost image:self.activePostImage completion:^(NSString * _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError) {
        if (completed) {
            [VANotificationMessage showSharePostSuccessMessageOnController:self];
        }
    }];
    [self hideActionsPopUp];
}

#pragma mark - Helpers

- (NSMutableAttributedString *)decoratedNameStringForPost:(VAPost *)post {
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] init];
    NSAttributedString *nameAttrString = [[NSAttributedString alloc] initWithString:post.user.nickname
                                                                         attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:58.0f/255.0f
                                                                                                                                      green:110.0f/255.0f
                                                                                                                                       blue:162.0f/255.0f
                                                                                                                                      alpha:1],
                                                                                      @"Author": @(YES)}];
    [attrString appendAttributedString:nameAttrString];
    
    if (post.checkIn.address) {
        NSAttributedString *checkInAttrString = [[NSAttributedString alloc] initWithString:@" checked in at "
                                                                                attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:74.0f/255.0f
                                                                                                                                             green:74.0f/255.0f
                                                                                                                                              blue:74.0f/255.0f
                                                                                                                                             alpha:1]}];
        [attrString appendAttributedString:checkInAttrString];
        
        NSAttributedString *locationAttrString = [[NSAttributedString alloc] initWithString:post.checkIn.name
                                                                                 attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:58.0f/255.0f
                                                                                                                                              green:110.0f/255.0f
                                                                                                                                               blue:162.0f/255.0f
                                                                                                                                              alpha:1],
                                                                                              @"CheckIn": @(YES)}];
        [attrString appendAttributedString:locationAttrString];
    }
    
    [attrString addAttribute:NSFontAttributeName
                       value:[UIFont fontWithName:@"Montserrat-Light" size:15.0f]
                       range:NSMakeRange(0, attrString.length)];
    
    return attrString;
}

- (NSMutableAttributedString *)decoratedPostStringFromString:(NSString *)stringWithTags {
    if (!stringWithTags) {
        return [[NSMutableAttributedString alloc] initWithString:@""];
    }
    NSError *error = nil;
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[#@](\\w+)" options:0 error:&error];
    
    NSArray *matches = [regex matchesInString:stringWithTags
                                      options:0
                                        range:NSMakeRange(0, stringWithTags.length)];
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:stringWithTags];
    [attString addAttribute:NSForegroundColorAttributeName
                      value:[UIColor colorWithRed:74.0f/255.0f
                                            green:74.0f/255.0f
                                             blue:74.0f/255.0f
                                            alpha:1]
                      range:NSMakeRange(0, stringWithTags.length)];
    [attString addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:@"Montserrat-Light" size:15.0f]
                      range:NSMakeRange(0, stringWithTags.length)];
    
    for (NSTextCheckingResult *match in matches) {
        NSRange wordRange = [match rangeAtIndex:0];
        
        //Set Foreground Color
        UIColor *foregroundColor = [UIColor colorWithRed:58.0f/255.0f
                                                   green:110.0f/255.0f
                                                    blue:162.0f/255.0f
                                                   alpha:1];
        [attString addAttribute:NSForegroundColorAttributeName
                          value:foregroundColor
                          range:wordRange];
        [attString addAttribute:@"Highlighted"
                          value:@(YES)
                          range:wordRange];
    }
    
    return attString;
}

- (void)profileUpdated {
    [self refreshData];
}

@end
