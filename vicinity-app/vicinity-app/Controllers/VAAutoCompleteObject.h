//
//  VAAutoCompleteObject.h
//  vicinity-app
//
//  Created by Panda Systems on 3/31/16.
//  Copyright © 2016 Vicinity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MLPAutoCompletionObject.h"

@interface VAAutoCompleteObject : NSObject <MLPAutoCompletionObject>

- (id)initWithName:(NSString *)name;

@end
