//
//  VAFriendsTableViewCell.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/1/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAFriendsTableViewCell.h"

@implementation VAFriendsTableViewCell

- (void)awakeFromNib {
    self.avatarView.layer.cornerRadius = 18.f;
    self.avatarView.clipsToBounds = YES;
    
    UITapGestureRecognizer* tapAvatar = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showProfileForUser)];
    [self.avatarView addGestureRecognizer:tapAvatar];
    self.avatarView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer* tapName = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showProfileForUser)];
    [self.nicknameLabel addGestureRecognizer:tapName];
    self.nicknameLabel.userInteractionEnabled = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

#pragma mark - Methods

- (void)showProfileForUser {
    [self.delegate userDidTapCellAtIndexPath:self.indexPath];
}

@end
