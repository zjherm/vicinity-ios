//
//  VAPostTableViewCell.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 18.06.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TTTAttributedLabel.h>

typedef NS_ENUM(NSUInteger, VALinkType) {
    VALinkTypeAuthor,
    VALinkTypeLocation,
    VALinkTypeMention,
    VALinkTypeHashtag,
    VALinkTypeMore,
    VALinkTypeURL
};

@class VAUser;
@class VAPost;

@protocol VAPostDelegate;

@interface VAPostTableViewCell : UITableViewCell
@property (strong, nonatomic) VAPost *post;
@property (weak, nonatomic) id <VAPostDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *avatarView;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *locationImage;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *dateImage;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *postLabel;
@property (weak, nonatomic) IBOutlet UIImageView *likeImageView;
@property (weak, nonatomic) IBOutlet UILabel *likeLabel;
@property (weak, nonatomic) IBOutlet UIButton *viewAllCommentsButton;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *firstCommentLabel;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *secondCommentLabel;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UILabel *likeButtonLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentButtonLabel;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UIImageView *postImageView;
@property (weak, nonatomic) IBOutlet UIView *likeView;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *likesAndCommentsContainerHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageContainerHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textContainerHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomCommentConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *firstCommentContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *secondCommentContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet UIButton *moreFakeButton;

@property (strong, nonatomic) NSIndexPath *path;

- (void)configureCellWithPost:(VAPost *)post;
- (CGFloat)heightForCellWithPost:(VAPost *)post;
@end

@protocol VAPostDelegate <NSObject>
@optional
- (void)shouldShowCommentsForPost:(VAPost *)post;
- (void)didSelectShowActions:(VAPostTableViewCell *)cell;
- (void)editPostSelected:(VAPost *)post;
- (void)deletePostSelected:(VAPost *)post;
- (void)blockUserSelected:(VAUser *)user;
- (void)reportSelected:(VAUser *)user;
- (void)turnOffNotificationsSelected:(VAPost *)post;

@end
