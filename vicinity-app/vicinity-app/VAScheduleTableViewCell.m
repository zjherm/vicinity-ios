//
//  VAScheduleTableViewCell.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 14.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAScheduleTableViewCell.h"

@implementation VAScheduleTableViewCell

- (void)awakeFromNib {
    BOOL isSmallScreen = CGRectGetWidth([UIScreen mainScreen].bounds) < 400.0f;
    self.dayLabel.font = [UIFont fontWithName:@"Montserrat-Regular" size:isSmallScreen ? 11.0f : 13.0f];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
