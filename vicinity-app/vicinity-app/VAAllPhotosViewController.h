//
//  VAAllPhotosViewController.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 07.07.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VAAllPhotosViewController : UIViewController
@property (strong, nonatomic) NSArray *photoURLsArray;
@end
