//
//  VAContactManager.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/23/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAContactManager.h"

@implementation VAContactManager

+ (void)askContactsPermissionsWithComplition:(void (^)(BOOL granted))complition {
    CFErrorRef *createError = nil;
    
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, createError);
    
    __block BOOL accessGranted = NO;
    if (&ABAddressBookRequestAccessWithCompletion != NULL) {
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            complition(accessGranted);
            dispatch_semaphore_signal(sema);
        });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    } else {
        accessGranted = YES;
        complition(accessGranted);
    }
}

+ (NSMutableArray *)getAllContacts {
    NSMutableArray *allContacts = [[NSMutableArray alloc] init];
    CFErrorRef *createError = nil;
    
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, createError);
    
    
    addressBook = ABAddressBookCreateWithOptions(NULL, createError);
    
    ABRecordRef source = ABAddressBookCopyDefaultSource(addressBook);
#warning  CHECK IF AUTORELEASE WORKS WELL
    CFAutorelease(source);
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeopleInSourceWithSortOrdering(addressBook, source, kABPersonSortByFirstName);
    
    CFAutorelease(addressBook);
    CFIndex peopleCount = CFArrayGetCount(allPeople);
    
    for (int i = 0; i < peopleCount; i++) {
        NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
        
        ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);
        
        // First name
        [userInfo setValue:(__bridge NSString*)ABRecordCopyValue(person, kABPersonFirstNameProperty) forKey:@"firstName"];
        // Last Name
        [userInfo setValue:(__bridge NSString*)ABRecordCopyValue(person, kABPersonLastNameProperty) forKey:@"lastName"];
        // Company
        [userInfo setValue:(__bridge NSString*)ABRecordCopyValue(person, kABPersonOrganizationProperty)forKey:@"companyName"];
        
        
        // Phone numbers
        NSMutableArray *userPhones = [[NSMutableArray alloc] init];
        ABMultiValueRef phones = ABRecordCopyValue(person, kABPersonPhoneProperty);
        for(CFIndex j = 0; j < ABMultiValueGetCount(phones); j++)
        {
            CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(phones, j);
            [userPhones addObject:(__bridge NSString*) ABAddressBookCopyLocalizedLabel(phoneNumberRef)];
        }
        
        [userInfo setObject:userPhones forKey:@"phones"];
        
        
        // Emails
        NSMutableArray *userEmails = [[NSMutableArray alloc] init];
        ABMultiValueRef multiEmails = ABRecordCopyValue(person, kABPersonEmailProperty);
        for(CFIndex j = 0; j < ABMultiValueGetCount(multiEmails); j++)
        {
            CFStringRef contactEmailRef = ABMultiValueCopyValueAtIndex(multiEmails, j);
            [userEmails addObject:(__bridge NSString*) ABAddressBookCopyLocalizedLabel(contactEmailRef)];
        }
        [userInfo setObject:userEmails forKey:@"emails"];
        
        if (ABPersonHasImageData(person)) {
            [userInfo setObject:(__bridge id)(ABPersonCopyImageData(person)) forKey:@"avatar"];
        }
        
        if (([userInfo valueForKey:@"firstName"] || [userInfo valueForKey:@"lastName"]) && (ABMultiValueGetCount(phones) > 0 || ABMultiValueGetCount(multiEmails) > 0)) {
            [allContacts addObject:userInfo];
        }
        CFAutorelease(person);
        
    }
    
    return allContacts;
    

}

@end
