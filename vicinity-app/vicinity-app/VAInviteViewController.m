//
//  VAInviteViewController.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/21/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAInviteViewController.h"
#import "VAInviteTypeView.h"
#import "DEMONavigationController.h"
#import <Google/Analytics.h>
#import "VANotificationTool.h"
#import "UIBarButtonItem+Badge.h"
#import "VAUserDefaultsHelper.h"
#import "VAFacebookInviteView.h"
#import "VAControlTool.h"
#import "VAUser.h"
#import <FBSDKLoginKit.h>
#import <FBSDKCoreKit.h>
#import "VANotificationMessage.h"
#import "VAUserApiManager.h"
#import "VALoginResponse.h"
#import "VAUserSearchResponse.h"
#import "VAFacebookFriendsView.h"
#import <AddressBook/AddressBook.h>
#import "VAContactsView.h"
#import "VAContactsInviteView.h"
#import "VAContactManager.h"

@interface VAInviteViewController () <VAFacebookInviteDelegate, VAInviteDelegate, VAContactsInviteDelegate, UIGestureRecognizerDelegate, VAFacebookFriendsViewDelegate, VAContactsViewDelegate>
@property (strong, nonatomic) UIBarButtonItem *leftItem;
@property (assign, nonatomic) VAInviteType currentInviteType;
@property (assign, nonatomic) VAInviteType lastInviteType;

@property (weak, nonatomic) IBOutlet VAInviteTypeView *inviteTypeView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *facebookGeneralView;
@property (weak, nonatomic) IBOutlet UIView *contactsGeneralView;
@property (weak, nonatomic) IBOutlet VAFacebookInviteView *facebookInviteView;
@property (weak, nonatomic) IBOutlet VAFacebookFriendsView *facebookFriendsView;
@property (weak, nonatomic) IBOutlet VAContactsInviteView *contactsInviteView;
@property (weak, nonatomic) IBOutlet VAContactsView *contactsView;


@end

@implementation VAInviteViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [(DEMONavigationController *)self.navigationController setDefaultNavigationBar];
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Menu Icon"] style:UIBarButtonItemStylePlain target:self.navigationController action:@selector(showMenu)];
    self.navigationItem.leftBarButtonItem = leftItem;
    self.leftItem = leftItem;
    
    self.facebookInviteView.delegate = self;
    self.inviteTypeView.delegate = self;
    self.contactsInviteView.delegate = self;
    
    [self loadViewsContent];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    switch (self.currentInviteType) {
        case VAInviteTypeFacebook:
            [self setFacebookInviteMethod];
            break;
        case VAInviteTypeContacts:
            [self setContactsInviteMethod];
            break;
        case VAInviteTypeOther:
            [self setOtherInviteMethod];
            break;
    }
}

- (void)loadViewsContent {
    [self loadFacebookFriendsContent];
    [self loadContactsContent];
}

- (void)loadFacebookFriendsContent {
    // Facebook view
    VAUser *user = (VAUser *)[VAUserDefaultsHelper getModelForKey:VASaveKeys_CurrentUser];
    FBSDKAccessToken *accessToken = [FBSDKAccessToken currentAccessToken];
    
    if (!user.facebookID || ![[accessToken permissions] containsObject:@"user_friends"]) {
        [self.facebookInviteView setHidden:NO];
        [self.facebookFriendsView setHidden:YES];
    } else {
        [self.facebookInviteView setHidden:YES];
        [self.facebookFriendsView setHidden:NO];
        self.facebookFriendsView.delegate = self;
        
        [self.facebookFriendsView getFacebookFriends];
    }
}

- (void)loadContactsContent {
    // Contacts view
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
        [self.contactsView setHidden:NO];
        self.contactsView.delegate = self;
        [self.contactsInviteView setHidden:YES];
        [self.contactsView sortAllContacts];
    } else {
        [self.contactsView setHidden:YES];
        [self.contactsInviteView setHidden:NO];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationController.title = @"Invite";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showButtonBadge) name:kShowBadgeForMenuIcon object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideButtonBadge) name:kHideBadgeForMenuIcon object:nil];
    
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Invite screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    switch (self.currentInviteType) {
        case VAInviteTypeFacebook:
            self.scrollView.contentOffset = CGPointMake(0, 0);
            break;
        case VAInviteTypeContacts:
            self.scrollView.contentOffset = CGPointMake(self.scrollView.frame.size.width, 0);
            break;
        case VAInviteTypeOther:
            if (self.lastInviteType == VAInviteTypeFacebook) {
                self.inviteTypeView.inviteType = VAInviteTypeFacebook;
                self.currentInviteType = VAInviteTypeFacebook;
                CGPointMake(0, 0);
            } else if (self.lastInviteType == VAInviteTypeContacts) {
                self.inviteTypeView.inviteType = VAInviteTypeContacts;
                self.currentInviteType = VAInviteTypeContacts;
                CGPointMake(self.scrollView.frame.size.width, 0);
            }
            break;
        default:
            break;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    if ([VAUserDefaultsHelper shouldShowNotificationBadge]) {
        [self.leftItem showBadge];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Methods

- (void)showButtonBadge {
    [self.leftItem showBadge];
}

- (void)hideButtonBadge {
    [self.leftItem hideBadge];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kShouldShowNotificationBadge];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void)setFacebookInviteMethod {
    self.currentInviteType = VAInviteTypeFacebook;
    self.lastInviteType = VAInviteTypeFacebook;
    
    [self.inviteTypeView removeConstraints:self.inviteTypeView.contactsStateViewConstraints];
    [self.inviteTypeView removeConstraints:self.inviteTypeView.otherStateViewConstraints];
    [self.inviteTypeView addConstraints:self.inviteTypeView.facebookStateViewConstraints];
    
    [UIView animateWithDuration:0.3f animations:^{
        self.scrollView.contentOffset = CGPointMake(0, 0);
        [self.inviteTypeView layoutIfNeeded];
    }];
}

- (void)setContactsInviteMethod {
    self.currentInviteType = VAInviteTypeContacts;
    self.lastInviteType = VAInviteTypeContacts;
    
    [self.inviteTypeView removeConstraints:self.inviteTypeView.facebookStateViewConstraints];
    [self.inviteTypeView removeConstraints:self.inviteTypeView.otherStateViewConstraints];
    [self.inviteTypeView addConstraints:self.inviteTypeView.contactsStateViewConstraints];
    
    [UIView animateWithDuration:0.3f animations:^{
        self.scrollView.contentOffset = CGPointMake(self.scrollView.frame.size.width, 0);
        [self.inviteTypeView layoutIfNeeded];
    }];
}

- (void)setOtherInviteMethod {
    self.currentInviteType = VAInviteTypeOther;

    [self.inviteTypeView removeConstraints:self.inviteTypeView.facebookStateViewConstraints];
    [self.inviteTypeView removeConstraints:self.inviteTypeView.contactsStateViewConstraints];
    [self.inviteTypeView addConstraints:self.inviteTypeView.otherStateViewConstraints];
    
    [[VAControlTool defaultControl] showShareSheetWithCompletion:^(NSString * _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError) {
        if (completed) {
            [VANotificationMessage showShareSuccessMessageOnController:self];
            [self setOtherInviteMethod];
        } else {
            [self shareControllerDidClose];
        }
    }];
    
    [UIView animateWithDuration:0.3f animations:^{
        self.scrollView.contentOffset = CGPointMake(self.scrollView.frame.size.width*2, 0);
        [self.inviteTypeView layoutIfNeeded];
    }];
}

- (void)connectAccountToFacebook {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    NSArray *permissions = @[@"public_profile", @"user_location", @"email", @"user_friends"];
    [login logInWithReadPermissions:permissions handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            [VANotificationMessage showLoginCancelledMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
        } else {
            
            if ([[result declinedPermissions] containsObject:@"user_friends"]) {
                
            } else {
                FBSDKAccessToken *accessToken = [FBSDKAccessToken currentAccessToken];
                NSString *tokenString = [accessToken tokenString];
                
                [[VAUserApiManager new] postCurrentSessionAddLinkToFacebookToken:tokenString
                                                                  withCompletion:^(NSError *error, VAModel *model) {
                                                                      if (model) {
                                                                          VALoginResponse *response = (VALoginResponse *)model;
                                                                          VAUser *user = response.loggedUser;
                                                                          [VAUserDefaultsHelper saveModel:user forKey:VASaveKeys_CurrentUser];
                                                                          [VAUserDefaultsHelper setAuthToken:response.sessionId];
                                                                          
                                                                          [self.facebookInviteView setHidden:YES];
                                                                          [self.facebookFriendsView setHidden:NO];
                                                                          
                                                                          [self.facebookFriendsView getFacebookFriends];
                                                                      } else if ([error.localizedDescription isEqual:@(100015)]) {
                                                                          [VANotificationMessage showFacebookInUseMessageOnController:self withDuration:5.f];
                                                                      }
                                                                  }];
                
                
            }
        }
    }];
}

- (void)askFriendsPermissions {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    NSArray *permissions = @[@"user_friends"];
    [login logInWithReadPermissions:permissions handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            [VANotificationMessage showLoginCancelledMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
        } else {
            
            if ([[result declinedPermissions] containsObject:@"user_friends"]) {
                
            } else {
                [self.facebookInviteView setHidden:YES];
                [self.facebookFriendsView setHidden:NO];
                
                [self.facebookFriendsView getFacebookFriends];
            }
        }
    }];
}

- (void)showGoToSettingsMessage {
    if (IOS8 || IOS9) {
        UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"Allow access" message:NSLocalizedString(@"contacts.permissions.denied", nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
        }];
        [ac addAction:actionOk];
        [self presentViewController:ac animated:YES completion:nil];
    } else if (IOS7) {
        [[[UIAlertView alloc] initWithTitle:@"Allow access"
                                    message:NSLocalizedString(@"contacts.permissions.denied", nil)
                                   delegate:self
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
}

- (void)askContactsPermissions {
    [VAContactManager askContactsPermissionsWithComplition:^(BOOL granted) {
        if (granted) {
            [self performSelectorOnMainThread:@selector(showContacts) withObject:nil waitUntilDone:NO];
        }
    }];
}

- (void)showContacts {
    [self.contactsInviteView setHidden:YES];
    [self.contactsView setHidden:NO];
    [self.contactsView sortAllContacts];
}

- (IBAction)rightSwipeRecognized:(id)sender {
    switch (self.currentInviteType) {
        case VAInviteTypeFacebook:
            break;
            
        case VAInviteTypeContacts:
            [self setFacebookInviteMethod];
            self.inviteTypeView.inviteType = VAInviteTypeFacebook;
            break;
            
        case VAInviteTypeOther:
            [self setContactsInviteMethod];
            self.inviteTypeView.inviteType = VAInviteTypeContacts;
            break;
            
        default:
            break;
    }
}
- (IBAction)leftSwipeRecognized:(id)sender {
    switch (self.currentInviteType) {
        case VAInviteTypeFacebook:
            [self setContactsInviteMethod];
            self.inviteTypeView.inviteType = VAInviteTypeContacts;
            break;
            
        case VAInviteTypeContacts:
            [self setOtherInviteMethod];
            self.inviteTypeView.inviteType = VAInviteTypeOther;
            break;
            
        case VAInviteTypeOther:
            break;
            
        default:
            break;
    }
}

#pragma mark - VAFacebookInviteDelegate

- (void)userDidPushAllowButton:(UIButton *)button {
    VAUser *user = (VAUser *)[VAUserDefaultsHelper getModelForKey:VASaveKeys_CurrentUser];
    FBSDKAccessToken *accessToken = [FBSDKAccessToken currentAccessToken];

    if (!user.facebookID) {
        [self connectAccountToFacebook];
    } else if (!accessToken || ![[accessToken permissions] containsObject:@"user_friends"]) {
        [self askFriendsPermissions];
    }
}

#pragma mark - VAInviteDelegate

- (void)userDidSelectInviteType:(VAInviteType)inviteType {
    if (inviteType == VAInviteTypeFacebook && self.currentInviteType != VAInviteTypeFacebook) {
        [self setFacebookInviteMethod];
    } else if (inviteType == VAInviteTypeContacts && self.currentInviteType != VAInviteTypeContacts) {
        [self setContactsInviteMethod];
    } else if (inviteType == VAInviteTypeOther && self.currentInviteType != VAInviteTypeOther) {
        [self setOtherInviteMethod];
    }
}

#pragma mark - VAContactsInviteDelegate

- (void)userDidPushAllowContactsButton:(UIButton *)button {
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied || ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusRestricted) {
        [self showGoToSettingsMessage];
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        [self askContactsPermissions];
    }
}

- (void)shareControllerDidClose {
    if (self.lastInviteType == VAInviteTypeFacebook) {
        self.inviteTypeView.inviteType = VAInviteTypeFacebook;
        [self setFacebookInviteMethod];
    } else if (self.lastInviteType == VAInviteTypeContacts) {
        self.inviteTypeView.inviteType = VAInviteTypeContacts;
        [self setContactsInviteMethod];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

#pragma mark - VAFacebookFriendsViewDelegate

- (void)didUpdateFacebookFriends {
    [self loadContactsContent];
}

- (void)didInviteFriends {
    [VANotificationMessage showShareSuccessMessageOnController:self];
}

- (void)didAddUserAsConnection:(VAUser *)user {
    [VANotificationMessage showConnectUserSuccessMessageOnController:self withDuration:5.0f nickname:user.nickname];
}

#pragma mark - VAContactsViewDelegate

- (void)didUpdateContacts {
    [self loadFacebookFriendsContent];
}

@end
