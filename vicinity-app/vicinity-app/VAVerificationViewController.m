//
//  VAVerificationViewController.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/8/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAVerificationViewController.h"
#import "VADesignTool.h"
#import "VALoaderView.h"
#import "VAUserApiManager.h"
#import "VAUserDefaultsHelper.h"
#import "VAUser.h"
#import "VALocationTool.h"
#import "VAUserProfileResponse.h"
#import "VANotificationMessage.h"
#import "VANotificationTool.h"
#import "VAEventsViewController.h"
#import "DEMONavigationController.h"
#import "VAMenuTableViewController.h"
#import "VAControlTool.h"

@interface VAVerificationViewController ()
@property (assign, nonatomic) BOOL viewDidDisappear;
@property (strong, nonatomic) UIView *backgroundView;
@property (strong, nonatomic) VALoaderView *loader;

@property (weak, nonatomic) IBOutlet UITextView *textView;
@end

@implementation VAVerificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.viewDidDisappear = NO;
    
    [self.navigationController.navigationBar setBarTintColor:[[VADesignTool defaultDesign] vicinityBlue]];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackButton"] style:UIBarButtonItemStylePlain target:self action:@selector(goBack)];

    self.navigationItem.leftBarButtonItem = backButton;
    
    self.textView.text = @"Please open the email from us in your inbox.\nClick on the link to verify";
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setTitle:@"Verify Info"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendVerificationRequest) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToEventsViewController) name:kDidChangeLocationStatus object:nil];

}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    if ([[VANotificationMessage currentMessageController] notificationMessageIsShown]) {
        [[VANotificationMessage currentMessageController] hideCurrentMessage];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions 

- (void)goBack {
    [VAUserDefaultsHelper setAuthToken:nil];

    [self.delegate userDidGoBackToLoginScreenFromVerification];
    
    if (self.navigationController.presentingViewController) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    } else {
        [[VAControlTool defaultControl] logout];
    }
}

#pragma mark - Methods

- (void)darkenBackground {
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds))];
    backgroundView.backgroundColor = [UIColor blackColor];
    backgroundView.alpha = 0.f;
    self.backgroundView = backgroundView;
    [self.view addSubview:backgroundView];
    [UIView animateWithDuration:0.3f animations:^{
        backgroundView.alpha = 0.7f;
    }];
}

- (void)lightenBackground {
    [UIView animateWithDuration:0.3f animations:^{
        self.backgroundView.alpha = 0.0f;
    }];
    [self.backgroundView removeFromSuperview];
}

- (void)showActivityIndicator {
    VALoaderView *loader = [VALoaderView initWhiteLoader];
    loader.frame = CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), kLoaderHeight);
    [loader setCenter:CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds))];
    loader.text = @"VERIFYING";
    [self.backgroundView addSubview:loader];
    self.loader = loader;
    [loader startAnimating];
}

- (void)hideActivityIndicator {
    if (self.loader) {
        [self.loader stopAnimating];
        [self.loader removeFromSuperview];
        self.loader = nil;
    }
}

- (void)showLoader {
    [self darkenBackground];
    [self showActivityIndicator];
}

- (void)hideLoader {
    [self hideActivityIndicator];
    [self lightenBackground];
}

- (void)sendVerificationRequest {
    [self showLoader];
    VAUser *user = (VAUser *)[VAUserDefaultsHelper getModelForKey:VASaveKeys_CurrentUser];

    [[VAUserApiManager new] getUserWithID:user.userId andCompletion:^(NSError *error, VAModel *model) {
        [self hideLoader];
        if (model) {
            VAUserProfileResponse *response = (VAUserProfileResponse *)model;
            VAUser *user = response.user;
            [VAUserDefaultsHelper saveModel:user forKey:VASaveKeys_CurrentUser];

            if ([user.isEmailConfirmed boolValue]) {
                [[VALocationTool sharedInstance] requestInUseAuthorization];
            } else {
                [VANotificationMessage showNoVerificationMessageOnController:self withDuration:5.f];
            }
        } else {
            [VANotificationMessage showNoVerificationMessageOnController:self withDuration:5.f];
        }
    }];
}

- (void)goToEventsViewController {
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    VAEventsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"VAEventsViewController"];
    DEMONavigationController *nav = (DEMONavigationController*)[self.storyboard instantiateViewControllerWithIdentifier:@"MyVicinityNavigationController"];
    [nav setViewControllers:@[vc] animated:NO];
    VAMenuTableViewController *leftMenuController = [self.storyboard instantiateViewControllerWithIdentifier:@"VAMenuTableViewController"];;
    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:nav
                                                    leftMenuViewController:leftMenuController
                                                    rightMenuViewController:nil];
    nav.containerViewController = container;
    
    container.leftMenuWidth = 125.f;
    leftMenuController.containerViewController = container;
    nav.containerViewController = container;
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];

    window.rootViewController = container;
    
    for (UIView *subview in window.subviews) {
        if ([subview isKindOfClass:NSClassFromString(@"UITransitionView")]) {
            [subview removeFromSuperview];
        }
    }
}

@end
