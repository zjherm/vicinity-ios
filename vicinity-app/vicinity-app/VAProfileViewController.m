//
//  VAProfileViewController.m
//  vicinity-app
//
//  Created by Panda Systems on 5/19/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAProfileViewController.h"
#import "GPUImage.h"
#import "DEMONavigationController.h"
#import "VAUserTool.h"
#import "VAPhotoTool.h"
#import "VAUser.h"
#import "VALoginResponse.h"
#import "VAUserDefaultsHelper.h"
#import "VAProfileUserInfoView.h"
#import "VAUserApiManager.h"
#import <UIImageView+AFNetworking.h>
#import "VANotificationTool.h"
#import "VANotificationMessage.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "VAControlTool.h"
#import "VAUserProfileResponse.h"
#import "UIBarButtonItem+Badge.h"
#import "UIImage+VAFixOrientation.h"
#import <Google/Analytics.h>
#import "VAProfileSegmentedView.h"
#import "VAPostTableViewCell.h"
#import "VAPost.h"
#import "VAPostResponse.h"
#import "VALoaderView.h"
#import "UITableViewCell+VAParentCell.h"
#import "VAPostTableActions.h"
#import "VACommentsViewController.h"
#import "VAUserSearchResponse.h"
#import "VAScrollView.h"
#import "VAStatePickerView.h"
#import "VAProfileActionsView.h"
#import "VAReportViewController.h"
#import "VALoginViewController.h"
#import "VAPostActionsView.h"
#import "VAPostActionsService.h"
#import "VAPostStore.h"
#import "VACountryPickerView.h"

#import "VAConfirmEmailViewController.h"

static NSString *const kAliasAlreadyExist = @"NICKNAME_ALREADY_EXISTS";

static CGFloat const kAvatarImageCornerRadius = 51.f;

@interface VAProfileViewController () <VAPhotoToolDelegate, VAProfileChangesProtocol, VAPRrofileInfoDelegate, VAPostActionsDelegate, VACommentDelegate, VAUserActionsDelegate, VAReportSendingDelegate, VAPostDelegate, VAPostActionsDelegate, VAConfirmEmailDelegate>

@property (weak, nonatomic) IBOutlet UIView *swipeView;
@property (strong, nonatomic) UITextField *activeField;
@property (strong, nonatomic) UIImage *updatedImage;
@property (strong, nonatomic) VAUser *currentUser;
@property (strong, nonatomic) UINavigationController *navController;
@property (strong, nonatomic) UIActivityIndicatorView *indicator;
@property (strong, nonatomic) UIBarButtonItem *indicatiorItem;
@property (assign, nonatomic) BOOL accountHaveBeenChanged;
@property (strong, nonatomic) NSArray *postsArray;
@property (strong, nonatomic) NSArray *likesArray;
@property (strong, nonatomic) VALoaderView *postsLoader;              //activity indicator for data loading
@property (strong, nonatomic) VALoaderView *likesLoader;
@property (strong, nonatomic) UITapGestureRecognizer *tapBackground;           //tap for dismissing keyboard
@property (assign, nonatomic) CGPoint offset;
@property (strong, nonatomic) NSIndexPath *indexPathOfSelectedPost;
@property (nonatomic, strong) UIImage *activePostImage;

@property (nonatomic, weak) IBOutlet UIScrollView *generalScrollView;
@property (nonatomic, weak) IBOutlet UIImageView *avatarImageView;
@property (nonatomic, weak) IBOutlet UIImageView *avatarBackgroundImageView;
@property (weak, nonatomic) IBOutlet UIView *privateInfoBlock;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UITextField *cardNumberField;
@property (weak, nonatomic) IBOutlet UITextField *expiresField;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *cardCodeField;
@property (weak, nonatomic) IBOutlet VAScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITableView *likesTableView;
@property (weak, nonatomic) IBOutlet VAProfileUserInfoView *profileView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *avatarContainerView;

@property (weak, nonatomic) IBOutlet UIView *paymentBlock;
@property (weak, nonatomic) IBOutlet VAProfileSegmentedView *segmentedView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundView;

// Actions View
@property (nonatomic, weak) IBOutlet VAProfileActionsView *actionsView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *actionsViewHeightConstraint;

// Pop up view
@property (nonatomic, strong) UIView *popUpView;
@property (nonatomic, strong) VAPostActionsView *postActionsView;
@property (nonatomic, strong) VAPost *activePost;

// ------------------------------------------------------------------------------------
// Constraints
// Height of top view with avatar
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *profileViewHeightConstraint;
// Constraint to change scroll view height on keyboard appearing
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewBottomConstraint;
// To hide payment block for production builds
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *paymentBlockHeightConstraint;
// To hide segmented if first profile setup after sign up
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *segmentedControlHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomBlurViewConstraint;
// Height of header depends on visibility of segmented control
@property (nonatomic) CGFloat currentHeaderHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *segmentedControlTrailingConstraint;
// ------------------------------------------------------------------------------------

@property (nonatomic, weak) IBOutlet UIBarButtonItem *leftButton;

@property (nonatomic, weak) IBOutlet UIView *statusView;
@property (nonatomic, weak) IBOutlet UILabel *statusLabel;

@property (nonatomic) CGFloat topOffset;

@property (nonatomic) CGFloat headerLastHeigth;
@property (nonatomic) CGFloat generalLastTopOffset;
@property (nonatomic) CGFloat postsLastTopOffset;
@property (nonatomic) CGFloat likesLastTopOffset;
@property (nonatomic) BOOL needToSetTopOffset;

@property (nonatomic) BOOL isGeneralInfoHeaderAnimating;
@property (nonatomic) BOOL isScrollViewDecelerating;

@property (nonatomic) BOOL isPostsLoaded;
@property (nonatomic) BOOL isLikesLoaded;

@property (strong, nonatomic) IBOutlet UIView *profileLoaderContainerView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *profileLoader;

@property (strong, nonatomic) VALoaderView *loader;
@property (weak, nonatomic) UIView *loaderBackground;

- (IBAction)editButtonPushed:(id)sender;

@end

BOOL facebookPhoto = YES;

@implementation VAProfileViewController

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        _isUsersAccountMode = NO;
        _isCreateAliasMode = NO;
        _needToSetTopOffset = NO;
        _isGeneralInfoHeaderAnimating = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.avatarBackgroundImageView.alpha = 0;
    
    // Setup loader
    [self.profileLoader stopAnimating];
    self.swipeView.hidden = NO;
    self.profileLoaderContainerView.hidden = YES;
    
    self.currentHeaderHeight = 235.0f;
    self.swipeView.userInteractionEnabled = NO;
    self.segmentedControlTrailingConstraint.constant = 0.0f;
    
    self.postsArray = [NSArray array];
    self.likesArray = [NSArray array];
    
    self.actionsView.delegate = self;
    self.statusView.hidden = YES;
    
    VAUser *user = [VAUserDefaultsHelper me];
    
    if (!self.isUsersAccountMode || [self.searchAlias isEqualToString:user.nickname] || [self.userId isEqualToString:user.userId]) {
        self.currentUser = user;
        self.actionsView.hidden = YES;
        self.actionsViewHeightConstraint.constant = 0.0f;
        
//        [self updateAvatarImage:[UIImage imageNamed:@"avatar-placeholder.png"]];
        [self loadAvatarForUser:user];
        
        if (self.isUsersAccountMode) {
            [self addBackButtonForUserSearchMode];
        }
        self.isUsersAccountMode = NO;
        self.searchAlias = nil;
        self.userId = nil;
        
        [self fillInDataFromUser:user];

        [self hideBigActivityIndicator];
    }
    
    self.updatedImage = nil;
    self.indexPathOfSelectedPost = nil;
    
    self.indicatiorItem = [self indicatorBarButtonItem];
    
    self.avatarContainerView.layer.borderWidth = 1.5f;
    self.avatarContainerView.layer.borderColor = [[VADesignTool defaultDesign] vicinityDarkBlue].CGColor;
    
    self.avatarContainerView.layer.cornerRadius = kAvatarImageCornerRadius;
    [self.avatarContainerView setClipsToBounds:YES];
    
    UITapGestureRecognizer* tapBackground = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [tapBackground setNumberOfTapsRequired:1];
    self.tapBackground = tapBackground;                 //we can not add this gesture recognizer for tableView, becouse it will break touch detection for TTTAttributedLabel and STTweetLabel
    
    self.avatarImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer* tapAvatar = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(editAvatar)];
    [tapAvatar setNumberOfTapsRequired:1];
    [self.avatarImageView addGestureRecognizer:tapAvatar];
    
    self.profileView.delegate = self;
    
    self.accountHaveBeenChanged = NO;
    
#ifdef VICINITY
    self.paymentBlock.hidden = YES;
    self.paymentBlockHeightConstraint.constant = 0.f;
#elif STAGING
    self.paymentBlock.hidden = YES;
    self.paymentBlockHeightConstraint.constant = 0.f;
#else
    self.paymentBlock.hidden = NO;
    self.paymentBlockHeightConstraint.constant = 252.f;
#endif
    
    if ([[VAControlTool defaultControl] currentLoginMethod] == VALoginMethodFacebook) {
        self.profileView.passwordHeightConstraint.constant = 0.f;
        [self.profileView.passwordContainer setHidden:YES];
    }
    
    [self.scrollView setContentInset:UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)];
    [self.scrollView setContentOffset:CGPointMake(0.0, 0.0)];
    
    [self.tableView setContentInset:UIEdgeInsetsMake(self.currentHeaderHeight, 0.0, 0.0, 0.0)];
    [self.likesTableView setContentInset:UIEdgeInsetsMake(self.currentHeaderHeight, 0.0, 0.0, 0.0)];
    
    self.topOffset = self.currentHeaderHeight;
    
    if (self.isUsersAccountMode) {
        [self setProfileSettingsForUserSearchMode];
        [self addBackButtonForUserSearchMode];
        
        if (self.searchAlias) {
            [self loadUser];
        } else if (self.userId) {
            [self loadUserByUserId];
        }
        
        //code for google analytics
        id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"User's profile"
                                                              action:@"Show user's profile"
                                                               label:@"From Vicinity screen"
                                                               value:nil] build]];
        
    } else if (self.isCreateAliasMode) {
        [self setProfileSettingsForCreateAliasMode];
    }
    
//    [self hideTableView];

    self.segmentedView.delegate = self;

    [self setupBarButtons];
    [self setupActionsPopUp];

    if ([self.currentUser.userId isEqualToString:user.userId]) {
        [[VAPostStore sharedPostStore] readProfilePostsWithSuccess:^(NSArray *posts) {
            if (!self.isPostsLoaded) {
                [[self mutableArrayValueForKey:@"postsArray"] addObjectsFromArray:posts];
                [self.tableView reloadData];
            }
        } failure:^(NSError *error) {
            NSLog(@"failure");
        }];
        
        [[VAPostStore sharedPostStore] readLikedPostsWithSuccess:^(NSArray *posts) {
            if (!self.isLikesLoaded) {
                [[self mutableArrayValueForKey:@"likesArray"] addObjectsFromArray:posts];
                [self.likesTableView reloadData];
            }
        } failure:^(NSError *error) {
            NSLog(@"failure");
        }];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    if (!self.currentUser) {
//        [self showBigActivityIndicator];
//    }
    
    self.navigationController.title = @"My Profile";
    if (!self.accountHaveBeenChanged) {
        [self setupBarButtons];
    }

    if (!self.isCreateAliasMode) {
        [(DEMONavigationController *)self.navigationController setDefaultNavigationBar];
    }
    
    if (self.isCreateAliasMode) {
        [self.navigationController.navigationBar
         setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
                                  NSFontAttributeName: [[VADesignTool defaultDesign] vicinityNavigationFontOfSize:21.f]}];
        self.navigationItem.title = @"Profile Setup";
    }
    
    if (self.isUsersAccountMode) {
        if (self.searchAlias) {
            self.navigationController.title = [NSString stringWithFormat:@"@%@", self.searchAlias];
        } else {
            if (self.currentUser && self.currentUser.nickname) {
                self.navigationController.title = [NSString stringWithFormat:@"@%@", self.currentUser.nickname];
            } else {
                self.navigationController.title = @"";
            }
        }
    }
    
    self.navController = self.navigationController;  //after selection another menu cell in side menu navigation controller will change its root controller and the acces to navigation controller in viewWillDissapear method will be lost
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    [self addBadgeObservers];
    if ([VAUserDefaultsHelper shouldShowNotificationBadge] && !self.isUsersAccountMode && ! self.isCreateAliasMode) {
        [self showButtonBadge];
    }
    
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"User's profile screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    switch (self.segmentedView.profileInfoType) {
        case VAProfileInfoTypeGeneral:
            self.generalScrollView.contentOffset = CGPointMake(0, 0);
            break;
        case VAProfileInfoTypePosts:
            self.generalScrollView.contentOffset = CGPointMake(self.generalScrollView.frame.size.width, 0);
            break;
        case VAProfileInfoTypeLikes:
            self.generalScrollView.contentOffset = CGPointMake(self.generalScrollView.frame.size.width*2, 0);
            break;
        default:
            break;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.segmentedView.profileInfoType = self.segmentedView.profileInfoType;
    if (self.currentUser && !self.accountHaveBeenChanged) {
        [self fillInDataFromUser:self.currentUser];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
                              NSFontAttributeName: [[VADesignTool defaultDesign] vicinityNavigationFontOfSize:31.f]}];
    //[self.navigationController.navigationBar setTitleVerticalPositionAdjustment:5.f forBarMetrics:UIBarMetricsDefault];

    self.headerLastHeigth = self.profileViewHeightConstraint.constant + self.currentHeaderHeight;
    self.generalLastTopOffset = self.scrollView.contentOffset.y;
    self.postsLastTopOffset = self.tableView.contentOffset.y;
    self.likesLastTopOffset = self.likesTableView.contentOffset.y;
    self.needToSetTopOffset = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - property
- (void)setSearchAlias:(NSString *)searchAlias
{
    if ([searchAlias hasSuffix:@"."]) {
        NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:@"."];
        _searchAlias = [searchAlias stringByTrimmingCharactersInSet:set];
    } else {
        _searchAlias = searchAlias;
    }
}


#pragma mark - Setup

- (void)setupBarButtons {
    if (self.navigationController.viewControllers.count == 1
        && self.currentUser
        && [self.currentUser.userId isEqualToString:[VAUserDefaultsHelper me].userId]
        && self.currentUser.status != VAUserStatusLocked) {
        BOOL showBadge = [VAUserDefaultsHelper shouldShowNotificationBadge];
        self.navigationItem.leftBarButtonItem = [self getMenuButtonWithBadge:showBadge];
    } else {
        self.navigationItem.leftBarButtonItem = [self backBarButton];
    }
    
    self.navigationItem.rightBarButtonItem = nil;
}

#pragma mark - IBActions

- (IBAction)closeButtonTouchUp:(id)sender {
    [((DEMONavigationController*)self.navigationController) showMenu];
}

- (IBAction)editButtonPushed:(id)sender {
    [self.activeField resignFirstResponder];
    [self editAvatar];
}

#pragma mark - Helpers

- (void)setBluredAvatarImage:(UIImage *)avatarImage {
    if (avatarImage) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            UIImage *bluredImage = [self gpuBlurApplyDarkEffect:avatarImage];
            dispatch_sync(dispatch_get_main_queue(), ^{
                self.avatarBackgroundImageView.image = bluredImage;
                [UIView animateWithDuration:0.3f animations:^{
                    self.avatarBackgroundImageView.alpha = 1.0f;
                }];
            });
        });
    } else {
        self.avatarBackgroundImageView.image = avatarImage;
        [UIView animateWithDuration:0.3f animations:^{
            self.avatarBackgroundImageView.alpha = 1.0f;
        }];
    }
}

- (IBAction)rightSwipeRecognized:(id)sender {
    if (![self.currentUser.isBlocked boolValue]) {
        switch (self.segmentedView.profileInfoType) {
            case VAProfileInfoTypeGeneral:
                break;

            case VAProfileInfoTypePosts:
                self.segmentedView.profileInfoType = VAProfileInfoTypeGeneral;
                break;

            case VAProfileInfoTypeLikes:
                self.segmentedView.profileInfoType = VAProfileInfoTypePosts;
                break;

            default:
                break;
        }
    }
}

- (IBAction)leftSwipeRecognized:(id)sender {
    if (![self.currentUser.isBlocked boolValue]) {
        switch (self.segmentedView.profileInfoType) {
            case VAProfileInfoTypeGeneral:
                self.segmentedView.profileInfoType = VAProfileInfoTypePosts;
                break;

            case VAProfileInfoTypePosts:
                if (self.currentUser.status == VAUserStatusNormal) {
                    self.segmentedView.profileInfoType = VAProfileInfoTypeLikes;
                }
                break;

            case VAProfileInfoTypeLikes:
                break;

            default:
                break;
        }
    }
}

#pragma mark - Keyboard actions

- (void)willKeyboardShow:(NSNotification*)notification {
    
    NSDictionary* info = [notification userInfo];
    CGRect keyRect;
    [[info objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyRect];
    CGFloat height = keyRect.size.height;
    
    self.scrollViewBottomConstraint.constant = height;
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
    
    [self.view addGestureRecognizer:self.tapBackground];
}

- (void)willKeyboardHide:(NSNotification*)notification {
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.05f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{         //it's better to make delay, because changing constraint couses scroll view scrolling and editing field blinks on the name field
        self.scrollViewBottomConstraint.constant = 0.0;
        
        [UIView animateWithDuration:0.3f animations:^{
            [self.view layoutIfNeeded];
            switch (self.segmentedView.profileInfoType) {
                case VAProfileInfoTypeGeneral:
                    self.generalScrollView.contentOffset = CGPointMake(0, 0);
                    break;
                case VAProfileInfoTypePosts:
                    self.generalScrollView.contentOffset = CGPointMake(self.generalScrollView.frame.size.width, 0);
                    break;
                case VAProfileInfoTypeLikes:
                    self.generalScrollView.contentOffset = CGPointMake(self.generalScrollView.frame.size.width*2, 0);
                    break;
                default:
                    break;
            }
        }];
    });
    
    [self.view removeGestureRecognizer:self.tapBackground];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([tableView isEqual:self.tableView]) {
        return [self.postsArray count];
    } else if ([tableView isEqual:self.likesTableView]) {
        return [self.likesArray count];
    } else {
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Post";
    
    VAPostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    VAPost *post = nil;
    
    if ([tableView isEqual:self.tableView]) {
        if ([self.postsArray count] > 0) {
            post = [self.postsArray objectAtIndex:indexPath.section];
        }
    } else if ([tableView isEqual:self.likesTableView]) {
        if ([self.likesArray count] > 0) {
            post = [self.likesArray objectAtIndex:indexPath.section];
        }
    }
    
    cell.path = indexPath;
    [cell configureCellWithPost:post];
    cell.delegate = self;
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 5.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Post";
    
    static VAPostTableViewCell *postCell = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        postCell = [tableView dequeueReusableCellWithIdentifier:identifier];
    });
    postCell.path = indexPath;
    
    VAPost *post = nil;
    
    if ([tableView isEqual:self.tableView]) {
        if ([self.postsArray count] > 0) {
            post = [self.postsArray objectAtIndex:indexPath.section];
        }
    } else if ([tableView isEqual:self.likesTableView]) {
        if ([self.likesArray count] > 0) {
            post = [self.likesArray objectAtIndex:indexPath.section];
        }
    }
    
    return [postCell heightForCellWithPost:post];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 7)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    VAPost *post = nil;
    if ([tableView isEqual:self.tableView]) {
        post = [self.postsArray objectAtIndex:indexPath.section];
    } else if ([tableView isEqual:self.likesTableView]) {
        post = [self.likesArray objectAtIndex:indexPath.section];
    }
    if (post && ![post.isViewed boolValue]) {
        VAUserApiManager *manager = [VAUserApiManager new];
        [manager putPostViewWithPostID:post.postID andCompletion:^(NSError *error, VAModel *model) {
            if (model) {
                VAServerResponse *response = (VAServerResponse *)model;
                if ([response.status isEqualToString:kSuccessResponseStatus]) {
                    post.isViewed = [NSNumber numberWithBool:YES];
                }
            }
        }];
    }
}

#pragma mark - Methods

- (void)applyChanges {
    if ([self allFieldsAreValid]) {
        [self updateUserData];
    }
    self.accountHaveBeenChanged = NO;
}

- (void)cancelChanges {
    [self dismissKeyboard];
    VAUser *user = (VAUser *)[VAUserDefaultsHelper getModelForKey:VASaveKeys_CurrentUser];
    self.currentUser = user;
    [self fillInDataFromUser:user];
    [self loadAvatarForUser:user];
    self.accountHaveBeenChanged = NO;
//    [self showDefaultNavigationButtons];
    [self setupBarButtons];
}

- (void)dismissKeyboard {
    [self.activeField resignFirstResponder];
}

- (UIImage *)gpuBlurApplyDarkEffect:(UIImage*)image
{
    UIImage *result = nil;
    
    if (!facebookPhoto) {
        GPUImageiOSBlurFilter *blurFilter = [[GPUImageiOSBlurFilter alloc] init];
        blurFilter.blurRadiusInPixels = 10;
        blurFilter.saturation = 1.2;
        result = [blurFilter imageByFilteringImage:image];
    } else {
        GPUImageGaussianBlurFilter *blurFilter = [GPUImageGaussianBlurFilter new];
        blurFilter.blurRadiusInPixels = 10;
        result = [blurFilter imageByFilteringImage:image];
    }
    
    GPUImageBrightnessFilter * brightnessFilter = [[GPUImageBrightnessFilter alloc] init];
    brightnessFilter.brightness = -0.1;
    result = [brightnessFilter imageByFilteringImage:result];
    
    return result;
}

- (void)fillInDataFromUser:(VAUser *)user {
    self.actionsView.user = user;
    
    if (user.status == VAUserStatusNormal || [user.userId isEqualToString:[VAUserDefaultsHelper me].userId]) {
        self.profileView.nameTextField.text = [user.fullname capitalizedString];
        self.profileView.aliasTextField.text = [user.nickname lowercaseString];
        self.profileView.emailTextField.text = [user.email lowercaseString];
        if ([user.gender isEqualToString:@"male"]) {
            [self.profileView.genderView setGenderType:VAGenderTypeMale];
        } else if ([user.gender isEqualToString:@"female"]) {
            [self.profileView.genderView setGenderType:VAGenderTypeFemale];
        } else {
            [self.profileView.genderView setGenderType:VAGenderTypeUnknown];
        }
        
        self.profileView.bioTextView.text = user.bio;
        if (!user.bio || [user.bio isEqualToString:@""]) {
            if (self.isUsersAccountMode) {
                [self.profileView.bioContainer setHidden:YES];
                self.profileView.bioTextView.text = @" ";
            } else {
                [self.profileView.placeholderLabel setHidden:NO];
            }
        } else {
            [self.profileView.placeholderLabel setHidden:YES];
        }
        
        NSArray *cityWords = [user.city componentsSeparatedByString:@", "];

        if ([cityWords count] > 0) {
            self.profileView.cityTextField.text = [cityWords firstObject];
        }

        if ([cityWords count] > 1) {
            self.profileView.stateTextField.text = cityWords[1];
            self.profileView.statePickerView.currentState = cityWords[1];
        } else if (user.state) {
            self.profileView.stateTextField.text = user.state;
            self.profileView.statePickerView.currentState = user.state;
        } else {
            self.profileView.stateTextField.text = @"";
            self.profileView.statePickerView.currentState = @"";
        }
        
        if ([user.country length]) {
            self.profileView.countryTextField.text = user.country;
            self.profileView.countryPickerView.currentCountry = user.country;
            [self.profileView updateForCountry:user.country];
            
            if (![user.userId isEqualToString:[VAUserDefaultsHelper me].userId]) {
                self.profileView.countryContainer.userInteractionEnabled = NO;
            }
        } else {
            [self.profileView updateForCountry:@""];
            
            if (![user.userId isEqualToString:[VAUserDefaultsHelper me].userId]) {
                [self.profileView.countryContainer setHidden:YES];
                self.profileView.countryHeightConstraint.constant = 0.f;
            }
        }
    }
    
    self.segmentedControlHeightConstraint.constant = 60.0f;
    self.currentHeaderHeight = 235.0f;
//    self.profileViewHeightConstraint.constant = self.currentHeaderHeight;
    if ([user.isBlocked boolValue] || user.status == VAUserStatusBlocked || user.status == VAUserStatusDeleted || (![user.userId isEqualToString:[VAUserDefaultsHelper me].userId] && user.status == VAUserStatusLocked)) {
        self.segmentedControlTrailingConstraint.constant = -[UIScreen mainScreen].bounds.size.width*2;
    } else if (user.status == VAUserStatusLocked && [user.userId isEqualToString:[VAUserDefaultsHelper me].userId]) {
        self.segmentedControlTrailingConstraint.constant = -[UIScreen mainScreen].bounds.size.width*0.5f;
    } else {
        self.segmentedControlTrailingConstraint.constant = 0.0f;
    }
    self.bottomBlurViewConstraint.constant = 100.0f;
    self.swipeView.userInteractionEnabled = YES;
    
    self.statusView.hidden = YES;
    if (user.status != VAUserStatusNormal) {
        self.actionsView.hidden = YES;
        self.actionsViewHeightConstraint.constant = 0.0f;
        if ([[VAUserDefaultsHelper me].userId isEqualToString:user.userId]) {
//            self.actionsViewHeightConstraint.constant = 0.0f;
        } else {
//            self.statusView.hidden = NO;
            if (user.status == VAUserStatusDeleted) {
                [VANotificationMessage showUserDeletedMessageOnController:self];
//                self.statusLabel.text = @"DELETED USER";
//                self.statusLabel.textColor = [UIColor redColor];
            } else if (user.status == VAUserStatusBlocked) {
                [VANotificationMessage showUserBlockedMessageOnController:self];
//                self.statusLabel.text = @"BLOCKED USER";
//                self.statusLabel.textColor = [UIColor orangeColor];
            } else  if (user.status == VAUserStatusLocked) {
                self.statusLabel.text = @"LOCKED USER";
                self.statusLabel.textColor = [UIColor orangeColor];
            }
        }
    }
    if (self.segmentedView.profileInfoType != VAProfileInfoTypeGeneral && (self.currentUser.status != VAUserStatusNormal || [self.currentUser.isBlocked boolValue])) {
        self.segmentedView.profileInfoType = VAProfileInfoTypeGeneral;
//        [self didSelectGeneralInfo];
    } else {
        if (![user.userId isEqualToString:[VAUserDefaultsHelper me].userId]) {
            BOOL isOnlyGeneral = ([self.currentUser.isBlocked boolValue] || self.currentUser.status == VAUserStatusBlocked || self.currentUser.status == VAUserStatusDeleted || (![self.currentUser.userId isEqualToString:[VAUserDefaultsHelper me].userId] && self.currentUser.status == VAUserStatusLocked));
            if (isOnlyGeneral) {
                [self.segmentedView removeConstraints:self.segmentedView.generalStateViewConstraints];
                [self.segmentedView removeConstraints:self.segmentedView.postsStateViewConstraints];
                [self.segmentedView removeConstraints:self.segmentedView.likesStateViewConstraints];
                [self.segmentedView addConstraints:self.segmentedView.centralStateViewConstraints];
            } else {
                [self.segmentedView removeConstraints:self.segmentedView.generalStateViewConstraints];
                [self.segmentedView removeConstraints:self.segmentedView.postsStateViewConstraints];
                [self.segmentedView removeConstraints:self.segmentedView.likesStateViewConstraints];
                [self.segmentedView removeConstraints:self.segmentedView.centralStateViewConstraints];
                
                if (self.segmentedView.profileInfoType == VAProfileInfoTypeGeneral) {
                    [self.segmentedView addConstraints:self.segmentedView.generalStateViewConstraints];
                }
                if (self.segmentedView.profileInfoType == VAProfileInfoTypePosts) {
                    [self.segmentedView addConstraints:self.segmentedView.postsStateViewConstraints];
                }
                if (self.segmentedView.profileInfoType == VAProfileInfoTypeLikes) {
                    [self.segmentedView addConstraints:self.segmentedView.likesStateViewConstraints];
                }
            }
        }
        [UIView animateWithDuration:0.3f animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}

- (void)clearAllFields {
    self.profileView.nameTextField.text = @"";
    self.profileView.aliasTextField.text = @"";
    self.profileView.cityTextField.text = @"";
    self.profileView.bioTextView.text = @"";
}

// any offset changes

- (void)editAvatar {
    if (self.isUsersAccountMode) {
        [[VAPhotoTool defaultPhoto] openPhotoBrowserFor:self.avatarImageView.image];
    } else {
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.rightBarButtonItem = nil;
        [VAPhotoTool defaultPhoto].delegate = self;
        [[VAPhotoTool defaultPhoto] showPhotoActionViewOnController:self];
    }
}

- (void)updateAvatarImage:(UIImage *)image {
    self.avatarImageView.image = image;
    [self setBluredAvatarImage:image];
}

- (NSData *)dataForResizedImage:(UIImage *)image {
    NSData *imgData = UIImageJPEGRepresentation(image, 0.1);     //data to send to server
    return imgData;
}

- (void)showActivityIndicator {
    self.navigationItem.rightBarButtonItem = self.indicatiorItem;;
    [self.indicator startAnimating];
}

- (void)hideActivityIndicator {
    [self.indicator stopAnimating];
    
    self.navigationItem.rightBarButtonItem = nil;

}

- (void)loadAvatarForUser:(VAUser *)user {

    if (user.status == VAUserStatusNormal || [user.userId isEqualToString:[VAUserDefaultsHelper me].userId]) {
        if ([user.avatarURL rangeOfString:@"facebook"].location == NSNotFound) {      //that means that photo is on our server, not facebook
            facebookPhoto = NO;             //the quality of uploaded photos from device is better then quality of photos from facebook. So we can make them more blury
        } else {
            facebookPhoto = YES;
        }
        
        if (user.avatarURL) {
            
            NSURL *imageURL = [NSURL URLWithString:user.avatarURL];
            
            [self.avatarImageView sd_setImageWithURL:imageURL
                              placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                         if (image) {
                                             [self updateAvatarImage:image];
                                         }
                                         if (error) {
                                             [VANotificationMessage showAvatarErrorMessageOnController:self withDuration:5.f];
                                         }
                                     }];
            
        } else {
            self.avatarImageView.image = [UIImage imageNamed:@"avatar-placeholder.png"];
            [self updateAvatarImage:self.avatarImageView.image];
        }
    }
}

- (BOOL)allFieldsAreValid {
    if (self.profileView.nameTextField.text.length < 3) {
        [VANotificationMessage showFillInNameMessageOnController:self withDuration:5.f];
        return NO;
    } else if (self.profileView.cityTextField.text.length < 3 && !self.isCreateAliasMode) {
        [VANotificationMessage showFillInCityMessageOnController:self withDuration:5.f];
        return NO;
    } else if (self.profileView.aliasTextField.text.length < 3) {
        [VANotificationMessage showFillInAliasMessageOnController:self withDuration:5.f];
        return NO;
    } else if ([self.profileView.aliasTextField.text hasSuffix:@"."]) {
        [VANotificationMessage showUsernameCannotEndWithPeriodMessageOnController:self withDuration:5.f];
        return NO;
    } else if (![[VAControlTool defaultControl] validEmailInTextField:self.profileView.emailTextField] && !self.isCreateAliasMode) {
        [VANotificationMessage showFillInEmailMessageOnController:self withDuration:5.f];
        return NO;
    } else {
        return YES;
    }
}

- (void)postUpdateAvatarNotification {
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidChangeUserAvatar object:nil];
}

- (void)updateUserData {
    [self.activeField resignFirstResponder];
    [self showActivityIndicator];
    NSData *imageData = nil;
    if (self.updatedImage) {
        imageData = [self dataForResizedImage:self.updatedImage];
    }
    NSDictionary *params = @{
                             @"fullname": [self.profileView.nameTextField.text capitalizedString],
                             @"nickname": [self.profileView.aliasTextField.text lowercaseString],
                             @"email": [self.profileView.emailTextField.text lowercaseString],
                             @"city": self.profileView.cityTextField.text,
                             @"state": self.profileView.stateTextField.text,
                             @"country": self.profileView.countryTextField.text,
                             @"gender": self.profileView.genderView.genderType == VAGenderTypeMale ? @"male" : @"female",
                             @"bio": self.profileView.bioTextView.text
                             };
    
    [[VAUserApiManager alloc] postUpdateCurrentUserWithParams:params imageData:imageData withCompletion:^(NSError *error, VAModel *model) {
        [self hideActivityIndicator];
        if (model) {
            VALoginResponse *response = (VALoginResponse *)model;
            VAUser *user = response.loggedUser;
            [VAUserDefaultsHelper saveModel:user forKey:VASaveKeys_CurrentUser];
            [self postUpdateAvatarNotification];
            for (VAPost* post in self.postsArray) {
                if ([post.user.userId isEqualToString:user.userId]) {
                    post.user = user;
                }
            }
            for (VAPost* post in self.likesArray) {
                if ([post.user.userId isEqualToString:user.userId]) {
                    post.user = user;
                }
            }
            [self.tableView reloadData];
            [self.likesTableView reloadData];
            if (![user.isEmailConfirmed boolValue]) {
                VAConfirmEmailViewController *confirmEmailViewController = [[UIStoryboard storyboardWithName:@"Vicinity" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"confirmEmailViewController"];
                confirmEmailViewController.isEmailUpdated = YES;
                confirmEmailViewController.delegate = self;
                [self.navigationController pushViewController:confirmEmailViewController animated:YES];

                
            } else if (self.isCreateAliasMode) {
                [self.navigationController dismissViewControllerAnimated:YES completion:^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:kUserDidCreateAlias object:nil];
                }];
            } else {
                [VANotificationMessage showSuccessMessageOnController:self withDuration:5.f];
//                [self showDefaultNavigationButtons];
                [self setupBarButtons];
            }

        } else {
            if (error) {
                if ([error.userInfo[@"NSLocalizedDescription"] isKindOfClass:[NSString class]] && [error.localizedDescription isEqualToString:kAliasAlreadyExist]) {
                    [VANotificationMessage showAliasExistsErrorMessageOnController:self withDuration:5.f];
                } else {
                    [VANotificationMessage showUpdateErrorMessageOnController:self withDuration:5.f];
                }
            }
        }
    }];
}

- (void)loadUser {    
    [self showActivityIndicator];
    self.scrollView.scrollEnabled = NO;
    [[VAUserApiManager alloc] getUserWithNickname:self.searchAlias andCompletion:^(NSError *error, VAModel *model) {
        [self hideActivityIndicator];
        self.scrollView.scrollEnabled = YES;
        if (model) {
            VAUserProfileResponse *response = (VAUserProfileResponse *)model;
            
            VAUser *user = response.user;
            if (user) {
                if (user.status == VAUserStatusNormal) {
                    self.currentUser = user;
                    if ([user.isBlocked boolValue]) {
                        [VANotificationMessage showYouBlockedUserMessageOnController:self];
                    }
                    [self loadAvatarForUser:user];
                    [self fillInDataFromUser:user];
                } else if (user.status == VAUserStatusDeleted) {
                    [VANotificationMessage showUserDeletedMessageOnController:self];
                    self.avatarImageView.image = [UIImage imageNamed:@"avatar-placeholder.png"];
                    self.actionsViewHeightConstraint.constant = 0.0f;
                    self.actionsView.hidden = YES;
                    self.segmentedView.hidden = YES;
                    self.generalScrollView.hidden = YES;
                    [self.view layoutIfNeeded];
                    [self.profileView.bioContainer setHidden:YES];
                } else if (user.status == VAUserStatusBlocked) {
                    [VANotificationMessage showUserBlockedMessageOnController:self];
                    self.avatarImageView.image = [UIImage imageNamed:@"avatar-placeholder.png"];
                    self.actionsViewHeightConstraint.constant = 0.0f;
                    self.actionsView.hidden = YES;
                    [self.view layoutIfNeeded];
                    [self.profileView.bioContainer setHidden:YES];
                }
            } else {
                [VANotificationMessage showUserNotFoundMessageOnController:self];
                self.avatarImageView.image = [UIImage imageNamed:@"avatar-placeholder.png"];
                self.actionsViewHeightConstraint.constant = 0.0f;
                self.actionsView.hidden = YES;
                [self.view layoutIfNeeded];
                [self.profileView.bioContainer setHidden:YES];
            }
        } else if (error) {
            [VANotificationMessage showUserNotFoundMessageOnController:self];
            self.avatarImageView.image = [UIImage imageNamed:@"avatar-placeholder.png"];
            self.actionsViewHeightConstraint.constant = 0.0f;
            self.actionsView.hidden = YES;
            [self.view layoutIfNeeded];
            [self.profileView.bioContainer setHidden:YES];
        }
        
        [self hideBigActivityIndicator];
    }];
}

- (void)loadUserByUserId {
    [self showActivityIndicator];
    self.scrollView.scrollEnabled = NO;
    [[VAUserApiManager alloc] getUserWithID:self.userId andCompletion:^(NSError *error, VAModel *model) {
        [self hideActivityIndicator];
        self.scrollView.scrollEnabled = YES;
        if (model) {
            VAUserProfileResponse *response = (VAUserProfileResponse *)model;
            
            VAUser *user = response.user;
            self.currentUser = user;
            if (user) {
                if (user.status == VAUserStatusNormal) {
                    self.currentUser = user;
                    if ([user.isBlocked boolValue]) {
                        [VANotificationMessage showYouBlockedUserMessageOnController:self];
                    }
                    [self loadAvatarForUser:user];
                    [self fillInDataFromUser:user];
                } else if (user.status == VAUserStatusDeleted) {
                    [VANotificationMessage showUserDeletedMessageOnController:self];
                    self.actionsViewHeightConstraint.constant = 0.0f;
                    self.actionsView.hidden = YES;
                    self.segmentedView.hidden = YES;
                    self.generalScrollView.hidden = YES;
                    [self.view layoutIfNeeded];
                    [self.profileView.bioContainer setHidden:YES];
                } else if (user.status == VAUserStatusBlocked) {
                    [VANotificationMessage showUserBlockedMessageOnController:self];
                    self.actionsViewHeightConstraint.constant = 0.0f;
                    self.actionsView.hidden = YES;
                    [self.view layoutIfNeeded];
                    [self.profileView.bioContainer setHidden:YES];
                }
                self.navigationController.title = [NSString stringWithFormat:@"@%@", user.nickname];
            } else {
                [VANotificationMessage showUserNotFoundMessageOnController:self];
                self.actionsViewHeightConstraint.constant = 0.0f;
                self.actionsView.hidden = YES;
                [self.view layoutIfNeeded];
                [self.profileView.bioContainer setHidden:YES];
            }
        } else if (error) {
            [VANotificationMessage showUserNotFoundMessageOnController:self];
            self.actionsViewHeightConstraint.constant = 0.0f;
            self.actionsView.hidden = YES;
            [self.view layoutIfNeeded];
            [self.profileView.bioContainer setHidden:YES];
        }

        [self hideBigActivityIndicator];
    }];
}

- (void)setProfileSettingsForUserSearchMode {
    [self.privateInfoBlock setHidden:YES];
    [self.paymentBlock setHidden:YES];
    
    self.paymentBlockHeightConstraint.constant = 0.f;
    
    self.profileView.nameTextField.userInteractionEnabled = NO;
    self.profileView.aliasTextField.userInteractionEnabled = NO;
    self.profileView.cityTextField.userInteractionEnabled = NO;
    self.profileView.stateTextField.userInteractionEnabled = NO;
    self.profileView.bioTextView.userInteractionEnabled = NO;
    
//    self.navigationItem.leftBarButtonItem = nil;

    [self.editButton setHidden:YES];
    
    self.avatarImageView.userInteractionEnabled = YES;
    
//    [self updateAvatarImage:[UIImage imageNamed:@"avatar-placeholder.png"]];
    
    self.profileView.nameTextField.text = @" ";
    self.profileView.aliasTextField.text = @" ";
    self.profileView.cityTextField.text = @" ";
    self.profileView.bioTextView.text  = @" ";
    self.profileView.stateTextField.text  = @" ";
    
    self.profileView.genderHeightConstraint.constant = 0.f;
    self.profileView.passwordHeightConstraint.constant = 0.f;
    self.profileView.privateBlockHeaderHeightConstraint.constant = 0.f;
    self.profileView.emailHeightConstraint.constant = 0.f;
}

- (void)addBackButtonForUserSearchMode {
//    self.navigationItem.leftBarButtonItem = [self backButton];
}

- (void)setProfileSettingsForCreateAliasMode {
    self.privateInfoBlock.hidden = NO;
    self.paymentBlock.hidden = YES;
    
    self.paymentBlockHeightConstraint.constant = 0.f;
    
    [self.editButton setHidden:NO];
    
    self.avatarImageView.userInteractionEnabled = YES;
    
    self.profileView.aliasTextField.text = @"";
    
//    if ([self.profileView.cityTextField.text isEqualToString:@""]) {
//        self.profileView.cityTextField.text = @"Undefined City";
//    }

    [self.profileView.bioContainer setHidden:YES];
    self.profileView.bioBlockHeightConstraint.priority = 900.f;

    [self.profileView.cityContainer setHidden:YES];
    self.profileView.cityHeightConstraint.constant = 0.f;
    
    [self.profileView.countryContainer setHidden:YES];
    self.profileView.countryHeightConstraint.constant = 0.f;
    
    [self.profileView.nameContainer setHidden:YES];
    self.profileView.nameHeightConstraint.constant = 0.f;
    
    [self.segmentedView setHidden:YES];
    self.segmentedControlHeightConstraint.constant = 0.f;
    self.bottomBlurViewConstraint.constant -= 60.f;
    
    [self.profileView.genderView setHidden:YES];
    self.profileView.genderHeightConstraint.constant = 0.f;
    
    [self.profileView.passwordContainer setHidden:YES];
    self.profileView.passwordHeightConstraint.constant = 0.f;
    
    [self.profileView.privateHeaderView setHidden:YES];
    self.profileView.privateBlockHeaderHeightConstraint.constant = 0.f;
    
    if (self.currentUser.email || ![self.currentUser.email isEqualToString:@""]) {
        [self.profileView.emailContainer setHidden:YES];
        self.profileView.emailHeightConstraint.constant = 0.f;
    }
    
    [self.scrollView setContentInset:UIEdgeInsetsMake(175.0, 0.0, 0.0, 0.0)];           //175 is header height without segmented control
//    [self.scrollView setContentOffset:CGPointMake(0.0, 175.0)];
    
//    self.navigationItem.leftBarButtonItem = [self backButton];
    self.navigationItem.rightBarButtonItem = nil;
}

- (void)showButtonBadge {
    if (self.navigationController.viewControllers.count == 1
        && self.currentUser
        && [self.currentUser.userId isEqualToString:[VAUserDefaultsHelper me].userId]
        && self.currentUser.status != VAUserStatusLocked) {
        self.navigationItem.leftBarButtonItem = [self getMenuButtonWithBadge:YES];
    }
}

- (void)hideButtonBadge {
    if (self.navigationController.viewControllers.count == 1
        && self.currentUser
        && [self.currentUser.userId isEqualToString:[VAUserDefaultsHelper me].userId]
        && self.currentUser.status != VAUserStatusLocked) {
        self.navigationItem.leftBarButtonItem = [self getMenuButtonWithBadge:NO];
    }
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kShouldShowNotificationBadge];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void)makeAliasFieldVisibleWithKeyboardHeight:(CGFloat)height {
    CGFloat screenHeight = CGRectGetHeight([self.view bounds]);
    CGFloat maxVisibleY = screenHeight - height;
    CGRect aliasFieldFrame = [self.profileView.aliasTextField convertRect:[self.profileView.aliasTextField frame] toView:self.view];
    CGFloat aliasFieldMaxY = CGRectGetMaxY(aliasFieldFrame);
    
    if (maxVisibleY < aliasFieldMaxY) {
        CGFloat difference = aliasFieldMaxY - maxVisibleY;
        [self.scrollView setContentInset:UIEdgeInsetsMake(175.0 - difference, 0.0, 0.0, 0.0)];
        CGPoint contentOffset = self.scrollView.contentOffset;
//        [self.scrollView setContentOffset:CGPointMake(contentOffset.x, contentOffset.y - difference)];
    }
}

- (UIBarButtonItem *)indicatorBarButtonItem {
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    indicator.frame = CGRectMake(0.0, 0.0, 20.0, 20.0);
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:indicator];
    self.indicator = indicator;
    return rightItem;
}

- (void)showActionNavigationButtons {
    if (!self.accountHaveBeenChanged) {
        
        if (!self.isCreateAliasMode) {
            self.navigationItem.leftBarButtonItem = [self cancelButton];
            
            self.navigationItem.rightBarButtonItem = [self saveButton];
        } else {
            self.navigationItem.rightBarButtonItem = [self verifyButton];
        }
    }
}

- (void)showDefaultNavigationButtons {
//    self.navigationItem.leftBarButtonItem = [self menuButton];
    
    self.navigationItem.rightBarButtonItem = nil;
}

- (void)hideTableView {
    [self.tableView setHidden:YES];
    [self.likesTableView setHidden:YES];
    [self.backgroundView setHidden:YES];
}

- (void)showTableView {
    [self.tableView setHidden:NO];
    [self.likesTableView setHidden:NO];
    [self.backgroundView setHidden:NO];
}

- (void)setDefaultTableViewSettings {
    [self dismissKeyboard];
    
//    CGPoint offset;
//    if (self.segmentedView.profileInfoType == VAProfileInfoTypeGeneral) {
//        offset = self.scrollView.contentOffset;
//    } else if (self.segmentedView.profileInfoType == VAProfileInfoTypePosts) {
//        offset = self.tableView.contentOffset;
//    } else {
//        offset = self.likesTableView.contentOffset;
//    }
//    
//    if (offset.y > -60) {
//        offset.y = -60.f;
//    }
//    self.offset = offset;
    
    [self showTableView];
//    [self.tableView setContentOffset:offset];
//    [self.likesTableView setContentOffset:offset];
}

- (void)showPostsLoader {
    if (self.postsLoader) {
        [self hideActivityIndicator];
    }
    VALoaderView *loader = [VALoaderView initLoader];
    loader.frame = CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), kLoaderHeight);
    [loader startAnimating];
    self.tableView.tableHeaderView = loader;
    self.postsLoader = loader;
    self.tableView.userInteractionEnabled = NO;
}

- (void)hidePostsLoader {
    [self.postsLoader stopAnimating];
    self.tableView.tableHeaderView = nil;
    self.postsLoader = nil;
    self.tableView.userInteractionEnabled = YES;
}

- (void)showLikesLoader {
    if (self.likesLoader) {
        [self hideActivityIndicator];
    }
    VALoaderView *loader = [VALoaderView initLoader];
    loader.frame = CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), kLoaderHeight);
    [loader startAnimating];
    self.likesTableView.tableHeaderView = loader;
    self.likesLoader = loader;
    self.likesTableView.userInteractionEnabled = NO;
}

- (void)hideLikesLoader {
    [self.likesLoader stopAnimating];
    self.likesTableView.tableHeaderView = nil;
    self.likesLoader = nil;
    self.likesTableView.userInteractionEnabled = YES;
}

- (void)makePostRequest {
    self.scrollView.scrollEnabled = NO;
    
//    [self.tableView setContentOffset:self.offset];
//    [self.likesTableView setContentOffset:self.offset];
    
    NSNumber *limit = @(1000);
    NSNumber *offset = @(0);
    
    NSNumber *lat = [[NSUserDefaults standardUserDefaults] valueForKey:kLatValue];
    NSNumber *lon = [[NSUserDefaults standardUserDefaults] valueForKey:kLonValue];
    
    if (self.segmentedView.profileInfoType == VAProfileInfoTypePosts) {
        if (!self.isPostsLoaded) {
            [self showPostsLoader];
            [self getPostsCreatedByUserWithID:self.currentUser.userId
                              currentLatitude:lat
                             currentLongitude:lon
                                    withLimit:limit
                                    andOffset:offset];
        }
    } else if (self.segmentedView.profileInfoType == VAProfileInfoTypeLikes) {
        if (!self.isLikesLoaded) {
            [self showLikesLoader];
            [self getPostsLikedByUserWithID:self.currentUser.userId
                            currentLatitude:lat
                           currentLongitude:lon
                                  withLimit:limit
                                  andOffset:offset];
        }
    }

}

- (void)getPostsCreatedByUserWithID:(NSString *)userID
                    currentLatitude:(NSNumber *)lat
                   currentLongitude:(NSNumber *)lon
                          withLimit:(NSNumber *)limit
                          andOffset:(NSNumber *)offset {
    VAUserApiManager *manager = [VAUserApiManager new];
    [manager getPostsCreatedByUserWithID:userID
                         currentLatitude:lat
                        currentLongitude:lon
                               withLimit:limit
                                  offset:offset
                           andCompletion:^(NSError *error, VAModel *model) {
                               [self performCompletionForPostsResponseWithError:error model:model];
                           }];
}

- (void)getPostsLikedByUserWithID:(NSString *)userID
                  currentLatitude:(NSNumber *)lat
                 currentLongitude:(NSNumber *)lon
                        withLimit:(NSNumber *)limit
                        andOffset:(NSNumber *)offset {
    VAUserApiManager *manager = [VAUserApiManager new];
    [manager getPostsLikedByUserWithID:userID
                       currentLatitude:lat
                      currentLongitude:lon
                             withLimit:limit
                                offset:offset
                         andCompletion:^(NSError *error, VAModel *model) {
                             [self performCompletionForLikesResponseWithError:error model:model];
                         }];
}

- (void)performCompletionForPostsResponseWithError:(NSError *)error
                                             model:(VAModel *)model {
    [self hidePostsLoader];
    self.scrollView.scrollEnabled = YES;
    
    if (model) {
        VAPostResponse *response = (VAPostResponse *)model;
        
        [[self mutableArrayValueForKey:@"postsArray"] removeAllObjects];
        [[self mutableArrayValueForKey:@"postsArray"] addObjectsFromArray:response.posts];
        
        if ([self.currentUser.userId isEqualToString:[VAUserDefaultsHelper me].userId]) {
            [[VAPostStore sharedPostStore] saveProfilePosts:self.postsArray];
        }
        
        [self.tableView reloadData];
        self.isPostsLoaded = YES;
    } else if (error) {
        [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
    }
}

- (void)performCompletionForLikesResponseWithError:(NSError *)error
                                             model:(VAModel *)model {
    [self hideLikesLoader];
    self.scrollView.scrollEnabled = YES;
    
    if (model) {
        VAPostResponse *response = (VAPostResponse *)model;
        
        [[self mutableArrayValueForKey:@"likesArray"] removeAllObjects];
        [[self mutableArrayValueForKey:@"likesArray"] addObjectsFromArray:response.posts];
        
        if ([self.currentUser.userId isEqualToString:[VAUserDefaultsHelper me].userId]) {
            [[VAPostStore sharedPostStore] saveLikedPosts:self.postsArray];
        }
        
        [self.likesTableView reloadData];
        self.isLikesLoaded = YES;
    } else if (error) {
        [VANotificationMessage showLoadErrorMessageOnController:self withDuration:5.f];
    }
}

- (VAPost *)postForCellWithPushedButton:(UIButton *)button {
    VAPost *post = nil;
    VAPostTableViewCell *cell = (VAPostTableViewCell *)[UITableViewCell findParentCellOfView:button];
    if ([self.postsArray count] && self.segmentedView.profileInfoType == VAProfileInfoTypePosts) {
        NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
        self.indexPathOfSelectedPost = cellIndexPath;
        post = [self.postsArray objectAtIndex:cellIndexPath.section];
    } else if ([self.likesArray count] && self.segmentedView.profileInfoType == VAProfileInfoTypeLikes) {
        NSIndexPath *cellIndexPath = [self.likesTableView indexPathForCell:cell];
        self.indexPathOfSelectedPost = cellIndexPath;
        post = [self.likesArray objectAtIndex:cellIndexPath.section];
    }
    return post;
}

- (NSString *)nameOfCurrentArrayInUse {
    if (self.segmentedView.profileInfoType == VAProfileInfoTypePosts) {
        return @"postsArray";
    } else if (self.segmentedView.profileInfoType == VAProfileInfoTypeLikes) {
        return @"likesArray";
    } else {
        return nil;
    }
}

#pragma mark - Post Actions

- (IBAction)showLikesToPostWithButton:(UIButton *)sender {
    VAPost *post = [self postForCellWithPushedButton:sender];
    
    VAPostTableActions *action = [VAPostTableActions new];
    [action tableView:self.tableView showLikesForPost:post];
    
    //code for google analytics
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Likes"
                                                          action:@"Show likes for post"
                                                           label:@"From Profile screen"
                                                           value:nil] build]];
}

- (void)shouldShowCommentsForPost:(VAPost *)post {
    VAPostTableActions *action = [VAPostTableActions new];
    [action tableView:self.tableView showCommentsForPost:post];
}

- (IBAction)showCommentsToPostWithButton:(UIButton *)sender {
    VAPost *post = [self postForCellWithPushedButton:sender];
    
    VAPostTableActions *action = [VAPostTableActions new];
    [action tableView:self.tableView showCommentsForPost:post];
}

- (IBAction)likeThisPost:(UIButton *)sender {
    VAPost *post = [self postForCellWithPushedButton:sender];
    VAPostTableViewCell *cell = (VAPostTableViewCell *)[UITableViewCell findParentCellOfView:sender];
    
    VAPostTableActions *action = [VAPostTableActions new];
    [action tableView:self.tableView performLikePost:post inCell:cell];
}

- (IBAction)editPost:(UIButton *)sender {
    VAPost *post = [self postForCellWithPushedButton:sender];
        
    VAPostTableActions *action = [VAPostTableActions new];
    action.delegate = self;
    
    [action tableView:self.tableView editPost:post];
}

#pragma mark - UIBarButtonItems

- (UIBarButtonItem *)menuButton {
    UIBarButtonItem *menuItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Menu Icon"] style:UIBarButtonItemStylePlain target:self.navigationController action:@selector(showMenu)];

    return menuItem;
}

- (UIBarButtonItem *)cancelButton {
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelChanges)];
    [cancelButton setTitleTextAttributes:@{
                                           NSFontAttributeName: [[VADesignTool defaultDesign] vicinityRegularFontOfSize:15.f],
                                           NSForegroundColorAttributeName: [UIColor whiteColor]
                                           } forState:UIControlStateNormal];

    return cancelButton;
}

- (UIBarButtonItem *)backButton {
    UIBarButtonItem *backItem = nil;
    if (self.isCreateAliasMode) {
        backItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackButton.png"] style:UIBarButtonItemStylePlain target:self action:@selector(goBackToLoginScreen)];
    } else {
        backItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackButton.png"] style:UIBarButtonItemStylePlain target:self.navigationController action:@selector(popViewControllerAnimated:)];
    }
    
    return backItem;
}

- (UIBarButtonItem *)saveButton {
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(applyChanges)];
    [saveButton setTitleTextAttributes:@{
                                         NSFontAttributeName: [[VADesignTool defaultDesign] vicinityRegularFontOfSize:15.f],
                                         NSForegroundColorAttributeName: [UIColor whiteColor]
                                         } forState:UIControlStateNormal];
    
    return saveButton;
}

- (UIBarButtonItem *)verifyButton {
    UIBarButtonItem *verifyItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"VerifyButton"] style:UIBarButtonItemStylePlain target:self action:@selector(applyChanges)];
    
    return verifyItem;
}

- (void)goBackToLoginScreen {
    [self dismissKeyboard];
    [self.delegate userDidGoBackToLoginScreen];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - VAPhotoToolDelegate

- (void)photoHasBeenEdited:(UIImage *)photo landscape:(BOOL)isLandscape {
    self.updatedImage = photo;
    facebookPhoto = NO;
    [self updateAvatarImage:photo];
    
    [self showActionNavigationButtons];
    self.accountHaveBeenChanged = YES;
}

- (void)photoActionViewHasBeenDismissed {
    if (self.accountHaveBeenChanged) {
        self.navigationItem.leftBarButtonItem = [self cancelButton];
        self.navigationItem.rightBarButtonItem = [self saveButton];
    } else {
        self.navigationItem.leftBarButtonItem = [self menuButton];
    }
}

#pragma mark - VAProfileChangesProtocol

- (void)textFieldDidBecomeActive:(UITextField *)textField {
    self.activeField = textField;
}

- (void)bioViewDidChange:(UITextView *)textView {
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
    
    [self showActionNavigationButtons];
    self.accountHaveBeenChanged = YES;

    self.scrollView.avoidScrolling = YES;
}

- (void)textFieldDidChange:(UITextField *)textField {
    [self showActionNavigationButtons];
    self.accountHaveBeenChanged = YES;
}

- (void)bioViewDidEndEditing:(UITextView *)textView {
    self.scrollView.avoidScrolling = NO;
}

#pragma mark - UITextFieldDelegate 

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.activeField = textField;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (!self.isGeneralInfoHeaderAnimating) {
        NSLog(@"Handle scrolling.");
        CGFloat topInset = scrollView.contentInset.top + ([scrollView isKindOfClass:[UITableView class]] ? 0 : self.currentHeaderHeight);
        CGPoint offset = CGPointMake(0, scrollView.contentOffset.y - ([scrollView isKindOfClass:[UITableView class]] ? 0 : self.currentHeaderHeight));
        CGFloat currentOffset = topInset + offset.y;
    //    NSLog(@"Content inset top: %f \n Content offset y: %f", scrollView.contentInset.top, scrollView.contentOffset.y);
        CGFloat topOffset;
        if ((topInset - currentOffset) <= 60.f) {
            topOffset = 60.f;
        } else {
            topOffset = topInset - currentOffset;
        }
        
        self.topOffset = topOffset;
        if ([scrollView isKindOfClass:[UITableView class]]) {
            scrollView.contentInset = UIEdgeInsetsMake(topOffset > self.currentHeaderHeight ? self.currentHeaderHeight : topOffset, 0, 0, 0);
            self.profileViewHeightConstraint.constant = topOffset;
        } else {
            topOffset = topOffset + self.currentHeaderHeight;
            scrollView.contentInset = UIEdgeInsetsMake(topOffset - self.currentHeaderHeight > 0.f ? 0.f : topOffset - self.currentHeaderHeight, 0, 0, 0);
            self.profileViewHeightConstraint.constant = topOffset - self.currentHeaderHeight;
        }
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    NSLog(@"Scroll view begin scroll.");
    self.isScrollViewDecelerating = YES;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    NSLog(@"Scroll view end scroll.");
    self.isScrollViewDecelerating = NO;
    self.isGeneralInfoHeaderAnimating = NO;
}

#pragma mark - VAPRrofileInfoDelegate

- (void)didSelectGeneralInfo {
    self.scrollView.contentInset = UIEdgeInsetsMake(self.topOffset - self.currentHeaderHeight, 0, 0, 0);
    self.scrollView.contentOffset = CGPointMake(0, -self.topOffset + self.currentHeaderHeight);
    
    CGFloat topOffset;
    
    if (self.needToSetTopOffset) {
        topOffset = self.headerLastHeigth;
    } else {
        CGFloat contentHeight = self.scrollView.contentSize.height;
        CGFloat headerHeight = CGRectGetHeight(self.view.bounds) - contentHeight;
        
        if (headerHeight < self.topOffset - self.currentHeaderHeight) {
            topOffset = self.topOffset;
        } else {
            topOffset = self.currentHeaderHeight;
        }
    }
    
    [self.segmentedView removeConstraints:self.segmentedView.postsStateViewConstraints];
    [self.segmentedView removeConstraints:self.segmentedView.likesStateViewConstraints];
    if ([self.currentUser.isBlocked boolValue] || self.currentUser.status == VAUserStatusBlocked || self.currentUser.status == VAUserStatusDeleted || (![self.currentUser.userId isEqualToString:[VAUserDefaultsHelper me].userId] && self.currentUser.status == VAUserStatusLocked)) {
        [self.segmentedView removeConstraints:self.segmentedView.generalStateViewConstraints];
        [self.segmentedView addConstraints:self.segmentedView.centralStateViewConstraints];
    } else {
        [self.segmentedView removeConstraints:self.segmentedView.centralStateViewConstraints];
        [self.segmentedView addConstraints:self.segmentedView.generalStateViewConstraints];
    }
    
//    for (NSLayoutConstraint *c in self.segmentedView.postsStateViewConstraints) {
//        c.active = NO;
//    }
//    for (NSLayoutConstraint *c in self.segmentedView.likesStateViewConstraints) {
//        c.active = NO;
//    }
//    for (NSLayoutConstraint *c in self.segmentedView.centralStateViewConstraints) {
//        c.active = ([self.currentUser.isBlocked boolValue] || self.currentUser.status == VAUserStatusBlocked || self.currentUser.status == VAUserStatusDeleted || (![self.currentUser.userId isEqualToString:[VAUserDefaultsHelper me].userId] && self.currentUser.status == VAUserStatusLocked));
//    }
//    for (NSLayoutConstraint *c in self.segmentedView.generalStateViewConstraints) {
//        c.active = !([self.currentUser.isBlocked boolValue] || self.currentUser.status == VAUserStatusBlocked || self.currentUser.status == VAUserStatusDeleted || (![self.currentUser.userId isEqualToString:[VAUserDefaultsHelper me].userId] && self.currentUser.status == VAUserStatusLocked));
//    }
    
    NSLog(@"Start animating general info header.");
    if (self.isScrollViewDecelerating) {
        self.isGeneralInfoHeaderAnimating = YES;
    }
    [UIView animateWithDuration:0.3f animations:^{
        self.generalScrollView.contentOffset = CGPointMake(0, 0);
        [self.segmentedView layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.profileViewHeightConstraint.constant = !self.needToSetTopOffset ? topOffset : topOffset - self.currentHeaderHeight;
        [UIView animateWithDuration:0.3f animations:^{
            self.scrollView.contentInset = UIEdgeInsetsMake(topOffset - self.currentHeaderHeight > 0.f ? 0.f : topOffset - self.currentHeaderHeight, 0, 0, 0);
            if (self.needToSetTopOffset) {
                self.scrollView.contentOffset = CGPointMake(0, self.generalLastTopOffset);
                self.needToSetTopOffset = NO;
            } else {
                self.scrollView.contentOffset = CGPointMake(0, -(topOffset - self.currentHeaderHeight > 0.f ? 0.f : topOffset - self.currentHeaderHeight));
            }
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            NSLog(@"Stop animating general info header.");
        }];
    }];
}

- (void)didSelectPostsInfo {
    [self setDefaultTableViewSettings];
    
    CGFloat headerOffset = self.profileViewHeightConstraint.constant;
    self.tableView.contentInset = UIEdgeInsetsMake(headerOffset, 0, 0, 0);
    if (!self.needToSetTopOffset) {
        self.tableView.contentOffset = CGPointMake(0, -headerOffset);
    }
    
    [self.tableView reloadData];
    if (!self.isPostsLoaded) {
        [self makePostRequest];
    }
    
    [self.segmentedView removeConstraints:self.segmentedView.generalStateViewConstraints];
    [self.segmentedView removeConstraints:self.segmentedView.likesStateViewConstraints];
    [self.segmentedView removeConstraints:self.segmentedView.centralStateViewConstraints];
    [self.segmentedView addConstraints:self.segmentedView.postsStateViewConstraints];
    
    for (NSLayoutConstraint *c in self.segmentedView.generalStateViewConstraints) {
        c.active = NO;
    }
    for (NSLayoutConstraint *c in self.segmentedView.likesStateViewConstraints) {
        c.active = NO;
    }
    for (NSLayoutConstraint *c in self.segmentedView.centralStateViewConstraints) {
        c.active = NO;
    }
    for (NSLayoutConstraint *c in self.segmentedView.postsStateViewConstraints) {
        c.active = YES;
    }
    
    [UIView animateWithDuration:0.3f animations:^{
        self.generalScrollView.contentOffset = CGPointMake(self.generalScrollView.frame.size.width, 0);
        [self.segmentedView layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.profileViewHeightConstraint.constant = self.topOffset;
        [UIView animateWithDuration:0.3f animations:^{
            self.tableView.contentInset = UIEdgeInsetsMake(self.topOffset, 0, 0, 0);
            if (self.needToSetTopOffset) {
                self.tableView.contentOffset = CGPointMake(0, self.postsLastTopOffset);
                self.needToSetTopOffset = NO;
            } else {
                self.tableView.contentOffset = CGPointMake(0, -self.topOffset);
            }
            [self.view layoutIfNeeded];
        }];
    }];
}

- (void)didSelectLikesInfo {
    [self setDefaultTableViewSettings];

    CGFloat headerOffset = self.profileViewHeightConstraint.constant;
    self.likesTableView.contentInset = UIEdgeInsetsMake(headerOffset, 0, 0, 0);
    if (!self.needToSetTopOffset) {
        self.likesTableView.contentOffset = CGPointMake(0, -headerOffset);
    }
    
    [self.likesTableView reloadData];
    if (!self.isLikesLoaded) {
        [self makePostRequest];
    }
    
    [self.segmentedView removeConstraints:self.segmentedView.generalStateViewConstraints];
    [self.segmentedView removeConstraints:self.segmentedView.postsStateViewConstraints];
    [self.segmentedView removeConstraints:self.segmentedView.centralStateViewConstraints];
    [self.segmentedView addConstraints:self.segmentedView.likesStateViewConstraints];
    
    [UIView animateWithDuration:0.3f animations:^{
        self.generalScrollView.contentOffset = CGPointMake(self.generalScrollView.frame.size.width*2, 0);
        [self.segmentedView layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.profileViewHeightConstraint.constant = self.topOffset;
        [UIView animateWithDuration:0.3f animations:^{
            self.likesTableView.contentInset = UIEdgeInsetsMake(self.topOffset, 0, 0, 0);
            if (self.needToSetTopOffset) {
                self.likesTableView.contentOffset = CGPointMake(0, self.likesLastTopOffset);
                self.needToSetTopOffset = NO;
            } else {
                self.likesTableView.contentOffset = CGPointMake(0, -self.topOffset);
            }
            [self.view layoutIfNeeded];
        }];
    }];
}

#pragma mark - VACommentDelegate

- (void)userDidAddCommentToPost:(VAPost *)post {
    [[self mutableArrayValueForKey:[self nameOfCurrentArrayInUse]] replaceObjectAtIndex:self.indexPathOfSelectedPost.section withObject:post];
    [self.tableView reloadData];
}

- (void)userDidDeleteCommentToPost:(VAPost *)post {
    [[self mutableArrayValueForKey:[self nameOfCurrentArrayInUse]] replaceObjectAtIndex:self.indexPathOfSelectedPost.section withObject:post];
    [self.tableView reloadData];
}

- (void)userDidDeletePost:(VAPost *)post {
    [[self mutableArrayValueForKey:[self nameOfCurrentArrayInUse]] removeObject:post];
    [self.tableView reloadData];
}

- (void)userDidEditCommentToPost:(VAPost *)post {
    [[self mutableArrayValueForKey:[self nameOfCurrentArrayInUse]] replaceObjectAtIndex:self.indexPathOfSelectedPost.section withObject:post];
    [self.tableView reloadData];
}

- (void)userDidEditPost:(VAPost *)post {
    [[self mutableArrayValueForKey:[self nameOfCurrentArrayInUse]] replaceObjectAtIndex:self.indexPathOfSelectedPost.section withObject:post];
    [self.tableView reloadData];
}

#pragma mark - VAPostActionsDelegate

- (void)didPerformDeleteActionForPost:(VAPost *)post {
    NSArray *currentArray = nil;
    UITableView* currentTableView;
    if (self.segmentedView.profileInfoType == VAProfileInfoTypePosts) {
        currentArray = self.postsArray;
        currentTableView = self.tableView;
    } else if (self.segmentedView.profileInfoType == VAProfileInfoTypeLikes) {
        currentArray = self.likesArray;
        currentTableView = self.likesTableView;
    }
    
    NSInteger postIndex = [currentArray indexOfObject:post];
    
    [[self mutableArrayValueForKey:[self nameOfCurrentArrayInUse]] removeObject:post];
    
    [currentTableView beginUpdates];
    [currentTableView deleteSections:[NSIndexSet indexSetWithIndex:postIndex] withRowAnimation:UITableViewRowAnimationLeft];
    [currentTableView endUpdates];
}

#pragma mark - VAVerificationDelegate

- (void)userDidGoBackToLoginScreenFromVerification {
    [self.delegate userDidGoBackToLoginScreen];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return ![self.currentUser.isBlocked boolValue] && (self.currentUser.status == VAUserStatusNormal || self.currentUser.status == VAUserStatusLocked);
}

#pragma mark - User Actions delegate

- (void)connectUserSelected:(VAUser *)user {
    [self addUserToConnections];
}

- (void)disconnectUserSelected:(VAUser *)user {
    [self deleteUserFromConnections];
}

- (void)messageToUserSelected:(VAUser *)user {
    
}

- (void)reportAboutUserSelected:(VAUser *)user {
    [self showReportScreen];
}

- (void)blockUserSelected:(VAUser *)user {
    [self askConfirmationToBlockUser];
}

- (void)unblockUserSelected:(VAUser *)user {
    [self unblockUser];
}

#pragma mark - Helpers

- (void)addUserToConnections {
    self.currentUser.isFriend = @(YES);
    self.actionsView.user = self.currentUser;
    [[VAUserApiManager new] postAddFriendWithID:self.currentUser.userId andCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            self.currentUser.isFriend = @(NO);
            self.actionsView.user = self.currentUser;

            [VANotificationMessage showNetworkErrorMessageOnController:self withDuration:5.f];
        } else {
            [VANotificationMessage showConnectUserSuccessMessageOnController:self withDuration:5.0f nickname:self.currentUser.nickname];
            [VANotificationsService didConnectUser:self.currentUser];
        }
    }];
}

- (void)deleteUserFromConnections {
    self.currentUser.isFriend = @(NO);
    self.actionsView.user = self.currentUser;
    [[VAUserApiManager new] deleteConnectionWithID:self.currentUser.userId withCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            self.currentUser.isFriend = @(YES);
            self.actionsView.user = self.currentUser;
            
            [VANotificationMessage showNetworkErrorMessageOnController:self withDuration:5.f];
        } else {
            [VANotificationsService didDisconnectUser:self.currentUser];
        }
    }];
}

- (void)subscibePost:(VAPost *)post {
    post.isSubscribed = [NSNumber numberWithBool:YES];
    
//    NSInteger index = [self.postsArray indexOfObject:post];
//    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:index]] withRowAnimation:UITableViewRowAnimationNone];
    
    VAUserApiManager *manager = [VAUserApiManager new];
    [manager subscribePostWithID:post.postID andCompletion:^(NSError *error, VAModel *model) {
        if (error) {
//            post.isSubscribed = [NSNumber numberWithBool:NO];
//            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:index]] withRowAnimation:UITableViewRowAnimationNone];
        }
    }];
}

- (void)unsubscibePost:(VAPost *)post {
    post.isSubscribed = [NSNumber numberWithBool:NO];
    
//    NSInteger index = [self.postsArray indexOfObject:post];
//    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:index]] withRowAnimation:UITableViewRowAnimationNone];
    
    VAUserApiManager *manager = [VAUserApiManager new];
    [manager unsubscribePostWithID:post.postID andCompletion:^(NSError *error, VAModel *model) {
        if (error) {
//            post.isSubscribed = [NSNumber numberWithBool:YES];
//            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:index]] withRowAnimation:UITableViewRowAnimationNone];
        }
    }];
}

- (void)askConfirmationToBlockUser {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"Block %@", self.currentUser.fullname]
                                                                             message:[NSString stringWithFormat:@"Are you sure you want to block %@?", self.currentUser.fullname]
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"No"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * _Nonnull action) { }];
    [alertController addAction:noAction];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"Yes"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * _Nonnull action) {
                                                          [self blockUser];
                                                      }];
    [alertController addAction:yesAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)blockUser {
    self.currentUser.isBlocked = @(YES);
    self.actionsView.user = self.currentUser;
    [self fillInDataFromUser:self.currentUser];
    
    [[VAUserApiManager new] blockUserWithID:self.currentUser.userId andCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            self.currentUser.isBlocked = @(NO);
            self.actionsView.user = self.currentUser;
            [self fillInDataFromUser:self.currentUser];
            
            [VANotificationMessage showNetworkErrorMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
        } else {
            [VANotificationsService didBlockUser:self.currentUser];
            
            [VANotificationMessage showBlockUserSuccessMessageOnController:[VAControlTool topScreenController] withDuration:5.f nickname:self.currentUser.nickname];
        }
    }];
}

- (void)unblockUser {
    self.currentUser.isBlocked = @(NO);
    self.actionsView.user = self.currentUser;
    [self fillInDataFromUser:self.currentUser];
    
    [[VAUserApiManager new] unblockUserWithID:self.currentUser.userId andCompletion:^(NSError *error, VAModel *model) {
        if (error) {
            self.currentUser.isBlocked = @(YES);
            self.actionsView.user = self.currentUser;
            [self fillInDataFromUser:self.currentUser];
            
            [VANotificationMessage showNetworkErrorMessageOnController:[VAControlTool topScreenController] withDuration:5.f];
        } else {
            [VANotificationsService didUnblockUser:self.currentUser];
            
            [VANotificationMessage showUnblockUserSuccessMessageOnController:[VAControlTool topScreenController] withDuration:5.f nickname:self.currentUser.nickname];
        }
    }];
}

#pragma mark - Navigation

- (void)showReportScreen {
    VAReportViewController *vc = [[UIStoryboard storyboardWithName:@"Vicinity" bundle:nil] instantiateViewControllerWithIdentifier:@"reportViewController"];
    vc.reportedUser = self.currentUser;
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showReportScreenWithPost:(VAPost *)post {
    VAReportViewController *vc = [[UIStoryboard storyboardWithName:@"Vicinity" bundle:nil] instantiateViewControllerWithIdentifier:@"reportViewController"];
    vc.reportedPost = post;
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showLoginScreen {
    VALoginViewController *vc = [[UIStoryboard storyboardWithName:@"LoginFlow" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"VALoginViewController"];
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)showMenu:(id)sender {
    [((DEMONavigationController*)self.navigationController) showMenu];
}

#pragma mark - Report Sending delegate

- (void)didSendReportAboutUser:(VAUser *)user {
    [VANotificationMessage showReportSuccessMessageOnController:self withDuration:5.f];
}

#pragma mark - Actions

- (void)menuButtonTouchUp:(id)sender {
    [self showMenu:sender];
}

- (void)backButtonTouchUp:(id)sender {
    if ([self.currentUser.userId isEqualToString:[VAUserDefaultsHelper me].userId] && self.currentUser.status == VAUserStatusLocked) {
        [self showLoginScreen];
    } else if (self.isCreateAliasMode) {
        [self goBackToLoginScreen];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - Navigation Bar items

- (UIBarButtonItem *)menuBarButton {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Menu Icon"]
                                            style:UIBarButtonItemStylePlain
                                           target:self
                                           action:@selector(menuButtonTouchUp:)];
}

- (UIBarButtonItem *)backBarButton {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackButton"]
                                            style:UIBarButtonItemStylePlain
                                           target:self
                                           action:@selector(backButtonTouchUp:)];
}

#pragma mark - Post delegate

- (void)didSelectShowActions:(VAPostTableViewCell *)cell {
    [self showActionsPopUpForCell:cell];
}

#pragma mark - Pop Up

- (void)setupActionsPopUp {
    self.popUpView = [[UIView alloc] initWithFrame:self.navigationController.view.bounds];
    self.popUpView.backgroundColor = [UIColor clearColor];
    self.popUpView.hidden = YES;
    
    UIView *overlayView = [[UIView alloc] initWithFrame:self.popUpView.bounds];
    overlayView.backgroundColor = [UIColor blackColor];
    overlayView.alpha = 0.4f;
    UITapGestureRecognizer *overlayTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideActionsPopUp)];
    [overlayView addGestureRecognizer:overlayTap];
    [self.popUpView addSubview:overlayView];
    
    self.postActionsView = [[VAPostActionsView alloc] init];
    self.postActionsView.backgroundColor = [UIColor clearColor];
    self.postActionsView.delegate = self;
    [self.popUpView addSubview:self.postActionsView];
    
    [self.navigationController.view addSubview:self.popUpView];
}

- (void)showActionsPopUpForCell:(VAPostTableViewCell *)cell {
    CGRect cellFrame = [cell convertRect:cell.contentView.frame toView:self.navigationController.view];
    CGRect targetFrame = CGRectMake(cellFrame.origin.x, cellFrame.origin.y + 21.0f, cellFrame.size.width, [VAPostActionsService postActionsForPost:cell.post].count*37.0f + 2.0f);
    CGFloat spaceToBottom = CGRectGetMaxY(self.navigationController.view.frame) - CGRectGetMaxY(targetFrame);
    CGFloat tableOffset = 0.0f;
    if (spaceToBottom < 0) {
        tableOffset = -spaceToBottom;
        targetFrame = CGRectMake(targetFrame.origin.x, targetFrame.origin.y + spaceToBottom, targetFrame.size.width, targetFrame.size.height);
    }
    self.postActionsView.frame = targetFrame;
    self.postActionsView.postActions = [VAPostActionsService postActionsForPost:cell.post];
    self.activePost = cell.post;
    self.activePostImage = cell.postImageView.image;
    self.popUpView.alpha = 0.0f;
    self.popUpView.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        self.popUpView.alpha = 1.0f;
        self.tableView.contentOffset = CGPointMake(self.tableView.contentOffset.x, self.tableView.contentOffset.y + tableOffset);
    }];
}

- (void)hideActionsPopUp {
    [UIView animateWithDuration:0.3f animations:^{
        self.popUpView.alpha = 0.0f;
    } completion:^(BOOL finished) {
        self.popUpView.hidden = YES;
    }];
}

#pragma mark - Post Actions Delegate

- (void)editPostSelected {
    [self editPostSelected:self.activePost];
    [self hideActionsPopUp];
}

- (void)deletePostSelected {
    [self deletePostSelected:self.activePost];
    [self hideActionsPopUp];
}

- (void)blockUserSelected {
    [self blockUserSelected:self.activePost.user];
    [self hideActionsPopUp];
}

- (void)reportUserSelected {
    [self reportSelected:self.activePost.user];
    [self hideActionsPopUp];
}

- (void)turnOffNotificationsSelected {
    [self unsubscibePost:self.activePost];
    [self hideActionsPopUp];
}

- (void)copyPostSelected {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.activePost.text ? self.activePost.text : @"";
    [self hideActionsPopUp];
}

- (void)reportPostSelected {
    [self showReportScreenWithPost:self.activePost];
    [self hideActionsPopUp];
}

- (void)turnOnNotificationsSelected {
    [self subscibePost:self.activePost];
    [self hideActionsPopUp];
}

- (void)sharePostSelected{
    [[VAControlTool defaultControl] showSharePostSheetWithPost:self.activePost image:self.activePostImage completion:^(NSString * _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError) {
        if (completed) {
            [VANotificationMessage showSharePostSuccessMessageOnController:self];
        }
    }];
    [self hideActionsPopUp];
}

#pragma mark - Helpers

- (void)editPostSelected:(VAPost *)post {
    VAPostTableActions *action = [VAPostTableActions new];
    action.delegate = self;
    [action showEditViewForPost:post inTableView:self.tableView];
}

- (void)deletePostSelected:(VAPost *)post {
    VAPostTableActions *action = [VAPostTableActions new];
    action.delegate = self;
    [action showDeleteConfirmationAlertViewForPost:post inTableView:self.tableView];
}

- (void)reportSelected:(VAUser *)user {
    [self showReportScreen];
}

- (void)turnOffNotificationsSelected:(VAPost *)post {
    
}

#pragma mark - Confirm email delegate

- (void)didRestoreOriginalEmailForUser:(VAUser *)user {
    self.currentUser = user;
    [self setupBarButtons];
}

- (void)didConfirmEmailForUser:(VAUser *)user {
    self.currentUser = user;
    [self setupBarButtons];
}

#pragma mark - Loading indicator

- (void)darkenBackground {
    UIView *backgroundView = [[UIView alloc] initWithFrame:self.view.bounds];
    backgroundView.backgroundColor = [UIColor blackColor];
    backgroundView.alpha = 0.f;
    self.loaderBackground = backgroundView;
    [self.view addSubview:backgroundView];
    [UIView animateWithDuration:0.3f animations:^{
        backgroundView.alpha = 0.7f;
    }];
}

- (void)lightenBackground {
    [UIView animateWithDuration:0.3f animations:^{
        self.loaderBackground.alpha = 0.0f;
    }];
    [self.loaderBackground removeFromSuperview];
}

- (void)showBigActivityIndicator {
    [self darkenBackground];
    VALoaderView *loader = [VALoaderView initWhiteLoader];
    loader.frame = CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), kLoaderHeight);
    [loader setCenter:CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds))];
    [self.loaderBackground addSubview:loader];
    self.loader = loader;
    [loader startAnimating];
}

- (void)hideBigActivityIndicator {
    [self lightenBackground];
    if (self.loader) {
        [self.loader stopAnimating];
        [self.loader removeFromSuperview];
        self.loader = nil;
    }
}

@end
