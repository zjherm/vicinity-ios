//
//  VAContactTableViewCell.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/23/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAContactTableViewCell.h"

@implementation VAContactTableViewCell

- (void)awakeFromNib {
    self.avatarView.layer.cornerRadius = 18.f;
    self.avatarView.clipsToBounds = YES;
    
    self.avatarPlaceholderLabel.layer.cornerRadius = 18.f;
    self.avatarPlaceholderLabel.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
