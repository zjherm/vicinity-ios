//
//  VAContactsInviteView.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/22/15.
//  Copyright © 2015 Vicinity. All rights reserved.
//

#import "VAContactsInviteView.h"

@interface VAContactsInviteView ()
@property (weak, nonatomic) IBOutlet UIButton *allowButton;
@end

@implementation VAContactsInviteView

- (void)awakeFromNib {
    self.allowButton.layer.cornerRadius = 5.f;
    self.allowButton.clipsToBounds = YES;
}

#pragma mark - Actions

- (IBAction)actionAllowContactsButtonDidPush:(UIButton *)sender {
    [self.delegate userDidPushAllowContactsButton:sender];
}

@end
