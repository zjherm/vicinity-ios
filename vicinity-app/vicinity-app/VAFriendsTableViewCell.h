//
//  VAFriendsTableViewCell.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 9/1/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VAFacebookFriendsDelegate;

@interface VAFriendsTableViewCell : UITableViewCell
@property (weak, nonatomic) id <VAFacebookFriendsDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *avatarView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *nicknameLabel;
@property (weak, nonatomic) IBOutlet UIView *separatorView;
@property (weak, nonatomic) IBOutlet UIButton *addButton;

@property (strong, nonatomic) NSIndexPath *indexPath;
@end

@protocol VAFacebookFriendsDelegate <NSObject>
- (void)userDidTapCellAtIndexPath:(NSIndexPath *)path;
@end