//
//  VAEmptyVicinityView.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 12.08.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VAVicinityEmptyViewProtocol;

@interface VAEmptyVicinityView : UIView
@property (weak, nonatomic) id <VAVicinityEmptyViewProtocol> delegate;
@end

@protocol VAVicinityEmptyViewProtocol <NSObject>
- (void)shouldShowFilters;
- (void)shouldShowShareSheet;
@end