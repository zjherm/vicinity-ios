//
//  VAVoucherViewController.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 04.08.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VAVoucherViewController.h"
#import "VALoaderView.h"
#import "VAVoucherTableViewCell.h"
#import "DEMONavigationController.h"
#import "VALocation.h"
#import "VABusiness.h"
#import "UIBarButtonItem+Badge.h"
#import "VAUserDefaultsHelper.h"
#import "VANotificationTool.h"
#import <Google/Analytics.h>

@interface VAVoucherViewController ()
@property (strong, nonatomic) NSArray *vouchersArray;
@property (strong, nonatomic) VALoaderView *loader;              //activity indicator for data loading
@property (strong, nonatomic) UIBarButtonItem *leftItem;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation VAVoucherViewController

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        _isViewVoucherMode = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.vouchersArray = [NSArray new];

    if (self.isViewVoucherMode) {
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackButton"] style:UIBarButtonItemStylePlain target:self.navigationController action:@selector(popViewControllerAnimated:)];
        self.navigationItem.leftBarButtonItem = leftItem;
    } else {
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Menu Icon"] style:UIBarButtonItemStylePlain target:self.navigationController action:@selector(showMenu)];
        self.navigationItem.leftBarButtonItem = leftItem;
        self.leftItem = leftItem;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showButtonBadge) name:kShowBadgeForMenuIcon object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideButtonBadge) name:kHideBadgeForMenuIcon object:nil];
    
    if (!self.isViewVoucherMode) {
        [self showActivityIndicator];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [self createDeals];
            [self hideActivityIndicator];
            [self.tableView reloadData];
        });
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationController.title = self.isViewVoucherMode ? @"View Deal" : @"View Deals";
    
//    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName value:@"Vouchers screen"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [(DEMONavigationController *)self.navigationController setDefaultNavigationBar];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.isViewVoucherMode) {
        return 1;
    } else {
        return [self.vouchersArray count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Voucher";
    
    VAVoucherTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier];
    VADeal *deal = nil;
    if ([self.vouchersArray count] > 0 && !self.isViewVoucherMode) {
        deal = [self.vouchersArray objectAtIndex:indexPath.section];
    }
    
    if (self.isViewVoucherMode) {
        deal = self.deal;
    }
    
    cell.path = indexPath;
    [cell configureCellWithDeal:deal];
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 7.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Voucher";
    
    static VAVoucherTableViewCell *dealCell = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dealCell = [self.tableView dequeueReusableCellWithIdentifier:identifier];
    });
    dealCell.path = indexPath;
    
    VADeal *deal = nil;
    if (self.isViewVoucherMode) {
        deal = self.deal;
    } else {
        deal = [self.vouchersArray objectAtIndex:indexPath.section];
    }
    return [dealCell heightForCellWithDeal:deal];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 7)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

#pragma mark - Methods

- (void)createDeals {
    
    VADeal *deal1 = [VADeal new];
    VALocation *location1 = [VALocation new];
    location1.cityName = @"La Jolla";
    location1.distance = [NSNumber numberWithInteger:160];
//    deal1.location = location1;
    VABusiness *business1 = [VABusiness new];
    business1.name = @"Bonfide Restaurant";
    business1.avatarURL = @"deal0";
    deal1.business = business1;
    deal1.text = @"Dinner is served. $20 for $45 dollars tonight. Grib deal fast!";
    deal1.shortDescription = @"$20 for $45 at Bonfide Restaurant";
    deal1.date = [NSDate dateWithTimeIntervalSinceNow:-25 * 60];
    deal1.photoURL = @"profile-avatar";
    deal1.likesAmount = [NSNumber numberWithInteger:0];
    deal1.commentsAmount = [NSNumber numberWithInteger:0];
    deal1.commentsArray = @[];
    deal1.isLiked = [NSNumber numberWithBool:NO];
    deal1.isPurchased = [NSNumber numberWithBool:NO];
    deal1.dealType = VADealTypePaid;
    deal1.serialNumber = @"32872-01";
    deal1.expirationDate = [NSDate dateWithTimeIntervalSinceNow:4 * 24 * 60 * 60];
    
    VADeal *deal3 = [VADeal new];
    VALocation *location3 = [VALocation new];
    location3.cityName = @"La Jolla";
    location3.distance = [NSNumber numberWithInteger:1050];
//    deal3.location = location3;
    VABusiness *business3 = [VABusiness new];
    business3.name = @"Joe's Bar";
    business3.avatarURL = @"deal2";
    deal3.business = business3;
    deal3.text = @"Come stop by! For the next two hours we are doing half off all";
    deal3.date = [NSDate dateWithTimeIntervalSinceNow:-5 * 60 * 60];
    deal3.photoURL = @"image2";
    deal3.likesAmount = [NSNumber numberWithInteger:22];
    deal3.commentsAmount = [NSNumber numberWithInteger:0];
    deal3.commentsArray = @[];
    deal3.isLiked = [NSNumber numberWithBool:YES];
    deal3.isPurchased = [NSNumber numberWithBool:NO];
    deal3.dealType = VADealTypeFree;
    deal3.shortDescription = @"$20 for 30 minutes massage until 4pm";
    deal3.serialNumber = @"12467-09";
    deal3.expirationDate = [NSDate dateWithTimeIntervalSinceNow:2 * 24 * 60 * 60];

    [[self mutableArrayValueForKey:@"vouchersArray"] addObjectsFromArray:@[deal1, deal3]];
    
}

- (void)showButtonBadge {
    [self.leftItem showBadge];
}

- (void)hideButtonBadge {
    [self.leftItem hideBadge];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kShouldShowNotificationBadge];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void)showActivityIndicator {
    if (self.loader) {
        [self hideActivityIndicator];
    }
    VALoaderView *loader = [VALoaderView initLoader];
    loader.frame = CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), kLoaderHeight);
    [loader startAnimating];
    self.tableView.tableHeaderView = loader;
    self.loader = loader;
    self.tableView.userInteractionEnabled = NO;
}

- (void)hideActivityIndicator {
    [self.loader stopAnimating];
    self.tableView.tableHeaderView = nil;
    self.loader = nil;
    self.tableView.userInteractionEnabled = YES;
}



@end
