//
//  VADesignTool.h
//  vicinity-app
//
//  Created by Hopreeeeenjust on 18.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VADesignTool : NSObject

+ (instancetype)defaultDesign;

// Colors
- (UIColor *)facebookBlue;
- (UIColor *)vicinityBlue;
- (UIColor *)vicinityDarkBlue;
- (UIColor *)notificationMessageBrown;
- (UIColor *)separatorColor;
- (UIColor *)middleGrayColor;
- (UIColor *)veryLightGrayColor;
- (UIColor *)vicinityNavBlue;
- (UIColor*)notificationMessageBlue;
- (UIColor *)vicinityOrange;
- (UIColor *)clearGray;
- (UIColor*)errorMessageRed;
- (UIColor*)successMessageGreen;

// Fonts
- (UIFont *)notificationMessageFontOfSize:(CGFloat)size;
- (UIFont *)vicinityBoldFontOfSize:(CGFloat)size;
- (UIFont *)vicinityRegularFontOfSize:(CGFloat)size;
- (UIFont *)vicinityLightFontOfSize:(CGFloat)size;
- (UIFont *)vicinityNavigationFontOfSize:(CGFloat)size;
- (UIFont *)faceebookButtonFontOfSize:(CGFloat)size;

// Helpers
- (void)addShadowForSeparator:(UIView *)separatorView;

- (void)setDefaultSettingsForMenuButton:(id)button;

@end
