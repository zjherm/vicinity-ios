//
//  VALocationTool.h
//  vicinity-app
//
//  Created by Panda Systems on 5/21/15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

extern NSString * const kLatValue;
extern NSString * const kLonValue;

typedef void(^VALocationCompletionBlock)(NSError *error, CLLocation *location);

@interface VALocationTool : NSObject

@property (nonatomic, readonly, getter=isServicesEnabled) BOOL servicesEnabled;

+ (instancetype)sharedInstance;

- (void)requestInUseAuthorization;

- (void)getCurrentDeviceLocationWithComplition:(VALocationCompletionBlock)complitionBlock;

@end
