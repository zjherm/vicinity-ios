//
//  VADesignTool.m
//  vicinity-app
//
//  Created by Hopreeeeenjust on 18.05.15.
//  Copyright (c) 2015 Vicinity. All rights reserved.
//

#import "VADesignTool.h"
#import "UIBarButtonItem+Badge.h"
#import "UIButton+Badge.h"

#define kNotificationBlue 0x21ABAA
#define kClearGray 0x666666

@implementation VADesignTool

+ (instancetype)defaultDesign {
    static VADesignTool *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [VADesignTool new];
    });
    return shared;
}

#pragma mark - Colors

- (UIColor *)facebookBlue {
    return [UIColor colorWithRed:59.0/255.0
                           green:87.0/255.0
                            blue:158.0/255.0
                           alpha:1.0];
}

- (UIColor *)vicinityBlue {
    return [UIColor colorWithRed:67.0/255.0
                           green:207.0/255.0
                            blue:206.0/255.0
                           alpha:1.0];
}

- (UIColor *)notificationMessageBrown {
    return [UIColor colorWithRed:152.0/255
                           green:101.0/255
                            blue:56.0/255
                           alpha:1.0];
}

- (UIColor *)separatorColor {
    return [UIColor colorWithRed:202.0/255
                           green:202.0/255
                            blue:202.0/255
                           alpha:1.0];
}

- (UIColor *)veryLightGrayColor {
    return [UIColor colorWithRed:231.0/255
                           green:231.0/255
                            blue:231.0/255
                           alpha:1.0];
}

- (UIColor *)middleGrayColor {
    return [UIColor colorWithRed:121.0/255
                           green:119.0/255
                            blue:128.0/255
                           alpha:1.0];
}

- (UIColor *)vicinityDarkBlue {
    return [UIColor colorWithRed:42.0/255
                           green:101.0/255
                            blue:160.0/255
                           alpha:1.0];
}

- (UIColor *)vicinityNavBlue {
    return [UIColor colorWithRed:73.0/255
                           green:128.0/255
                            blue:240.0/255
                           alpha:1.0];
}

- (UIColor *)vicinityOrange {
    return [UIColor colorWithRed:254.0/255
                           green:153.0/255
                            blue:51.0/255
                           alpha:1.0];
}

- (UIColor*)notificationMessageBlue {
    return [self colorWithHex:kNotificationBlue];
}

- (UIColor*)errorMessageRed {
    return [self colorWithHex:0xFC4D53];
}

- (UIColor*)successMessageGreen {
    return [self colorWithHex:0x128B02];
}

- (UIColor *)clearGray {
    return [self colorWithHex:kClearGray];
}

- (UIColor *)colorWithHex:(UInt32)hex {
    return [UIColor colorWithRed:((CGFloat)((hex & 0xFF0000) >> 16))/255.0f green:((CGFloat)((hex & 0xFF00) >> 8))/255.0f blue:((CGFloat)(hex & 0xFF))/255.0f alpha:1.0f];
}

#pragma mark - Fonts

- (UIFont*)notificationMessageFontOfSize:(CGFloat)size {
    //return [UIFont fontWithName:@"JosefinSlab-Light" size:size];
    return [UIFont fontWithName:@"Montserrat-Regular" size:size];
}

- (UIFont *)vicinityBoldFontOfSize:(CGFloat)size {
    return [UIFont fontWithName:@"Montserrat-Bold" size:size];
}

- (UIFont *)vicinityRegularFontOfSize:(CGFloat)size {
    return [UIFont fontWithName:@"Montserrat-Regular" size:size];
}

- (UIFont *)vicinityLightFontOfSize:(CGFloat)size {
    return [UIFont fontWithName:@"Montserrat-Light" size:size];
}

- (UIFont *)vicinityNavigationFontOfSize:(CGFloat)size {
    return [UIFont fontWithName:@"Blenda Script" size:size];
}

- (UIFont *)faceebookButtonFontOfSize:(CGFloat)size {
    return [UIFont fontWithName:@"Josefin Sans" size:size];
}

#pragma mark - Helpers

- (void)addShadowForSeparator:(UIView *)separatorView {
    separatorView.layer.shadowColor = [[UIColor blackColor] CGColor];
    separatorView.layer.shadowRadius = 1.5f;
    separatorView.layer.shadowOpacity = 0.5f;
    separatorView.layer.shadowOffset = CGSizeMake(0, 3);
    separatorView.layer.masksToBounds = NO;
}

- (void)setDefaultSettingsForMenuButton:(id)button {
    if ([button isKindOfClass:[UIBarButtonItem class]]) {
        [self setDefaultSettingsForBarButtonItem:button];
    } else if ([button isKindOfClass:[UIButton class]]) {
        [self setDefaultSettingsForButton:button];
    }
}

- (void)setDefaultSettingsForBarButtonItem:(UIBarButtonItem *)button {
    button.badgeBGColor = [self vicinityOrange];
    button.badgeMinSize = 1.f;
    button.badgePadding = 11.f;
    button.badgeOriginX = 22.f;
    button.badgeOriginY = 1.f;
    button.badgeFont = [UIFont systemFontOfSize:1.f];
    button.badgeBGColor = [self vicinityOrange];
}

- (void)setDefaultSettingsForButton:(UIButton *)button {
    button.badgeValue = @" ";
    button.badgeMinSize = 1.f;
    button.badgePadding = 11.f;
    button.badgeOriginX = 15.f;
    button.badgeOriginY = 1.f;
    button.badgeFont = [UIFont systemFontOfSize:1.f];
    button.badgeBGColor = [self vicinityOrange];
}

@end
